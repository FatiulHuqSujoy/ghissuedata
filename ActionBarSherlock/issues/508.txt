{"id":"508", "title":"Input queue can get blocked on ActionBar on pre-3.0 devices", "body":"On my 2.3.4 android device, it is possible that the input queue can get blocked.
This is using Sherlock version 4.1.0

To reproduce:
1. Create a fresh Android project.
2. In the Manifest make sure minimum sdk version is 10, target sdk of 11, and the activity has configChanges of \"orientation\" as well as split action bar. As so:
## 

   <uses-sdk android:minSdkVersion=\"10\" android:targetSdkVersion=\"11\"/>

```
<application
    android:icon=\"@drawable/ic_launcher\"
    android:label=\"@string/app_name\" >
    <activity
        android:name=\".BlockedActionBarBugActivity\"
        android:label=\"@string/app_name\"
        android:theme=\"@style/Theme.Sherlock\"
        android:configChanges=\"orientation|keyboardHidden\"
        android:uiOptions=\"splitActionBarWhenNarrow\" >
        <intent-filter>
            <action android:name=\"android.intent.action.MAIN\" />
            <category android:name=\"android.intent.category.LAUNCHER\" />
        </intent-filter>
    </activity>
</application>
```
## 

 In the activity itself make sure there is at least one menu entry with MenuItem.SHOW_AS_ACTION_ALWAYS. As so:
## 

public class BlockedActionBarBugActivity extends SherlockActivity implements
 OnCreateOptionsMenuListener, OnPrepareOptionsMenuListener, OnOptionsItemSelectedListener {

```
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
}

@Override
public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu)
{
    return true;
}

@Override
public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item)
{
    Log.d(\"DEBUG\", \"ITEM EXECUTED : \" + item.getItemId());
    return true;
}

@Override
public boolean onPrepareOptionsMenu(com.actionbarsherlock.view.Menu menu)
{
    menu.clear();
    MenuItem menuItem;
    menuItem = menu.add(0, 0, 0, \"MENU\");
    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    menuItem = menu.add(0, 1, 1, \"TOOLBAR\");
    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    return true;
}
```

}

The rest of reproduction scenario assumes the above code was installed. But the same behavior will reproduce the problem on all APPs with buttons that have the MenuItem.SHOW_AS_ACTION_ALWAYS property set.
1. Bring up the APP on a pre 3.0 phone. The phone I reproduced it on was running 2.3.4.
    It was a Verizon phone, MODEL ADR6400L.
2. Touch the \"TOOLBAR\" button on the bottom. Notice that it flashes. Also notice in the logcat an \"ITEM EXECUTED\" message.
   This is working behavior.
3. Now rotate the phone to landscape mode then rotate back into portrait mode.
4. At this point, touching the same \"TOOLBAR\" button will do nothing. The input queue is blocked against anything found in the action bar.  You can hit the menu button and see the \"MENU 2\" button. So it's only against the action bar itself that the input queue is blocked for some reason.
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/508","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/508/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/508/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/508/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/508","id":4918564,"node_id":"MDU6SXNzdWU0OTE4NTY0","number":508,"title":"Input queue can get blocked on ActionBar on pre-3.0 devices","user":{"login":"douglasselph","id":347472,"node_id":"MDQ6VXNlcjM0NzQ3Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/347472?v=4","gravatar_id":"","url":"https://api.github.com/users/douglasselph","html_url":"https://github.com/douglasselph","followers_url":"https://api.github.com/users/douglasselph/followers","following_url":"https://api.github.com/users/douglasselph/following{/other_user}","gists_url":"https://api.github.com/users/douglasselph/gists{/gist_id}","starred_url":"https://api.github.com/users/douglasselph/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/douglasselph/subscriptions","organizations_url":"https://api.github.com/users/douglasselph/orgs","repos_url":"https://api.github.com/users/douglasselph/repos","events_url":"https://api.github.com/users/douglasselph/events{/privacy}","received_events_url":"https://api.github.com/users/douglasselph/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2012-06-05T21:47:59Z","updated_at":"2012-06-05T22:37:22Z","closed_at":"2012-06-05T22:37:22Z","author_association":"NONE","body":"On my 2.3.4 android device, it is possible that the input queue can get blocked.\nThis is using Sherlock version 4.1.0\n\nTo reproduce:\n1. Create a fresh Android project.\n2. In the Manifest make sure minimum sdk version is 10, target sdk of 11, and the activity has configChanges of \"orientation\" as well as split action bar. As so:\n## \n\n   <uses-sdk android:minSdkVersion=\"10\" android:targetSdkVersion=\"11\"/>\n\n```\n<application\n    android:icon=\"@drawable/ic_launcher\"\n    android:label=\"@string/app_name\" >\n    <activity\n        android:name=\".BlockedActionBarBugActivity\"\n        android:label=\"@string/app_name\"\n        android:theme=\"@style/Theme.Sherlock\"\n        android:configChanges=\"orientation|keyboardHidden\"\n        android:uiOptions=\"splitActionBarWhenNarrow\" >\n        <intent-filter>\n            <action android:name=\"android.intent.action.MAIN\" />\n            <category android:name=\"android.intent.category.LAUNCHER\" />\n        </intent-filter>\n    </activity>\n</application>\n```\n## \n\n In the activity itself make sure there is at least one menu entry with MenuItem.SHOW_AS_ACTION_ALWAYS. As so:\n## \n\npublic class BlockedActionBarBugActivity extends SherlockActivity implements\n OnCreateOptionsMenuListener, OnPrepareOptionsMenuListener, OnOptionsItemSelectedListener {\n\n```\n@Override\npublic void onCreate(Bundle savedInstanceState) {\n    super.onCreate(savedInstanceState);\n    setContentView(R.layout.main);\n}\n\n@Override\npublic boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu)\n{\n    return true;\n}\n\n@Override\npublic boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item)\n{\n    Log.d(\"DEBUG\", \"ITEM EXECUTED : \" + item.getItemId());\n    return true;\n}\n\n@Override\npublic boolean onPrepareOptionsMenu(com.actionbarsherlock.view.Menu menu)\n{\n    menu.clear();\n    MenuItem menuItem;\n    menuItem = menu.add(0, 0, 0, \"MENU\");\n    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);\n    menuItem = menu.add(0, 1, 1, \"TOOLBAR\");\n    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);\n    return true;\n}\n```\n\n}\n\nThe rest of reproduction scenario assumes the above code was installed. But the same behavior will reproduce the problem on all APPs with buttons that have the MenuItem.SHOW_AS_ACTION_ALWAYS property set.\n1. Bring up the APP on a pre 3.0 phone. The phone I reproduced it on was running 2.3.4.\n    It was a Verizon phone, MODEL ADR6400L.\n2. Touch the \"TOOLBAR\" button on the bottom. Notice that it flashes. Also notice in the logcat an \"ITEM EXECUTED\" message.\n   This is working behavior.\n3. Now rotate the phone to landscape mode then rotate back into portrait mode.\n4. At this point, touching the same \"TOOLBAR\" button will do nothing. The input queue is blocked against anything found in the action bar.  You can hit the menu button and see the \"MENU 2\" button. So it's only against the action bar itself that the input queue is blocked for some reason.\n","closed_by":{"login":"JakeWharton","id":66577,"node_id":"MDQ6VXNlcjY2NTc3","avatar_url":"https://avatars0.githubusercontent.com/u/66577?v=4","gravatar_id":"","url":"https://api.github.com/users/JakeWharton","html_url":"https://github.com/JakeWharton","followers_url":"https://api.github.com/users/JakeWharton/followers","following_url":"https://api.github.com/users/JakeWharton/following{/other_user}","gists_url":"https://api.github.com/users/JakeWharton/gists{/gist_id}","starred_url":"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JakeWharton/subscriptions","organizations_url":"https://api.github.com/users/JakeWharton/orgs","repos_url":"https://api.github.com/users/JakeWharton/repos","events_url":"https://api.github.com/users/JakeWharton/events{/privacy}","received_events_url":"https://api.github.com/users/JakeWharton/received_events","type":"User","site_admin":false}}", "commentIds":["6137890","6138740"], "labels":[]}