{"id":"110", "title":"Native ActionBar does not show up", "body":"On Android 3.x+ (on the emulator) the native action bar does not show up. This is what I get:

![Screenshot](http://localhostr.com/file/hRnqelD/5554ICS.png)

This is just a bug-tracker repost of the email I sent you on Nov 1st. In case you want them here handy, here's the details:

I am targeting API 13 and using a custom theme with `parent=\"Theme.Sherlock.Light\"`. It only overrides some attributes, it's not even a complete overhaul.
This is the project.prop file:

```
# Project target.
target=android-13
android.library.reference.3=libs/GreenDroid
android.library.reference.1=libs/ActionBarSherlock
android.library.reference.2=libs/ViewPagerIndicator
```

This is the `uses-sdk` node of the manifest:

```
<uses-sdk android:minSdkVersion=\"8\" android:targetSdkVersion=\"13\"/>
```

The email thread has the full project code attached. I'm sending a newer build soon so you can check it out too. The problem is not solved as of yet.
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/110","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/110/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/110/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/110/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/110","id":2222489,"node_id":"MDU6SXNzdWUyMjIyNDg5","number":110,"title":"Native ActionBar does not show up","user":{"login":"rock3r","id":153802,"node_id":"MDQ6VXNlcjE1MzgwMg==","avatar_url":"https://avatars2.githubusercontent.com/u/153802?v=4","gravatar_id":"","url":"https://api.github.com/users/rock3r","html_url":"https://github.com/rock3r","followers_url":"https://api.github.com/users/rock3r/followers","following_url":"https://api.github.com/users/rock3r/following{/other_user}","gists_url":"https://api.github.com/users/rock3r/gists{/gist_id}","starred_url":"https://api.github.com/users/rock3r/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rock3r/subscriptions","organizations_url":"https://api.github.com/users/rock3r/orgs","repos_url":"https://api.github.com/users/rock3r/repos","events_url":"https://api.github.com/users/rock3r/events{/privacy}","received_events_url":"https://api.github.com/users/rock3r/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2011-11-13T18:40:28Z","updated_at":"2011-11-14T20:42:02Z","closed_at":"2011-11-13T20:34:21Z","author_association":"NONE","body":"On Android 3.x+ (on the emulator) the native action bar does not show up. This is what I get:\n\n![Screenshot](http://localhostr.com/file/hRnqelD/5554ICS.png)\n\nThis is just a bug-tracker repost of the email I sent you on Nov 1st. In case you want them here handy, here's the details:\n\nI am targeting API 13 and using a custom theme with `parent=\"Theme.Sherlock.Light\"`. It only overrides some attributes, it's not even a complete overhaul.\nThis is the project.prop file:\n\n```\n# Project target.\ntarget=android-13\nandroid.library.reference.3=libs/GreenDroid\nandroid.library.reference.1=libs/ActionBarSherlock\nandroid.library.reference.2=libs/ViewPagerIndicator\n```\n\nThis is the `uses-sdk` node of the manifest:\n\n```\n<uses-sdk android:minSdkVersion=\"8\" android:targetSdkVersion=\"13\"/>\n```\n\nThe email thread has the full project code attached. I'm sending a newer build soon so you can check it out too. The problem is not solved as of yet.\n","closed_by":{"login":"rock3r","id":153802,"node_id":"MDQ6VXNlcjE1MzgwMg==","avatar_url":"https://avatars2.githubusercontent.com/u/153802?v=4","gravatar_id":"","url":"https://api.github.com/users/rock3r","html_url":"https://github.com/rock3r","followers_url":"https://api.github.com/users/rock3r/followers","following_url":"https://api.github.com/users/rock3r/following{/other_user}","gists_url":"https://api.github.com/users/rock3r/gists{/gist_id}","starred_url":"https://api.github.com/users/rock3r/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rock3r/subscriptions","organizations_url":"https://api.github.com/users/rock3r/orgs","repos_url":"https://api.github.com/users/rock3r/repos","events_url":"https://api.github.com/users/rock3r/events{/privacy}","received_events_url":"https://api.github.com/users/rock3r/received_events","type":"User","site_admin":false}}", "commentIds":["2724239","2726606","2736218","2736241"], "labels":[]}