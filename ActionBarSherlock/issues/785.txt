{"id":"785", "title":"I use SearchView, but unlike the system of SearchView easy to use", "body":"I view the document, but there are some problems.I clicked on the actionbar on searchbutton, not performed onOptionsItemSelected, click on the keyboard searchButton no reaction.Here is my code。

``` java
<menu xmlns:android=\"http://schemas.android.com/apk/res/android\" >
    <item
        android:id=\"@+id/m_search\"
        android:icon=\"@drawable/action_search\"
        android:showAsAction=\"ifRoom\"
        android:actionViewClass=\"com.actionbarsherlock.widget.SearchView\"
        android:title=\"search\"/>
</menu>
```

``` java
<searchable xmlns:android=\"http://schemas.android.com/apk/res/android\"
    android:hint=\"3333\"
    android:label=\"@string/app_name\"
    android:searchSuggestAuthority=\"com.yaming.test.actionbar\"
    android:searchSuggestSelection=\" ?\"
    android:voiceSearchMode=\"showVoiceSearchButton|launchRecognizer\" />
```

``` java
 <activity
            android:name=\"com.example.actionbartest.MainActivity\"
            android:hardwareAccelerated=\"true\"
            android:label=\"@string/app_name\" >
            <intent-filter>
                <action android:name=\"android.intent.action.MAIN\" />
                <category android:name=\"android.intent.category.LAUNCHER\" />
            </intent-filter>
            <meta-data
                android:name=\"android.app.default_searchable\"
                android:value=\"com.example.actionbartest.SecondActivity\" />
        </activity>
        <activity
            android:name=\"com.example.actionbartest.SecondActivity\"
            android:hardwareAccelerated=\"true\"
            android:launchMode=\"singleTop\"
            android:label=\"@string/app_name\" >
            <intent-filter>
                <action android:name=\"android.intent.action.SEARCH\" />
            </intent-filter>
            <meta-data
                android:name=\"android.app.searchable\"
                android:resource=\"@xml/test\" />
        </activity>

        <provider
            android:name=\".TestSearchProvider\"
            android:authorities=\"com.yaming.test.actionbar\"
            android:exported=\"false\" />
```

``` java
public class TestSearchProvider extends SearchRecentSuggestionsProvider {
    private static final String AUTHORITY = \"com.yaming.test.actionbar\";

    /**
     * Save query to history
     * 
     * @param context
     * @param query
     */
    public static void save(Context context, String query) {
        suggestions(context).saveRecentQuery(query, null);
    }

    /**
     * Clear query history
     * 
     * @param context
     */
    public static void clear(Context context) {
        suggestions(context).clearHistory();
    }

    private static SearchRecentSuggestions suggestions(Context context) {
        return new SearchRecentSuggestions(context, AUTHORITY,
                DATABASE_MODE_QUERIES);
    }

    /**
     * Create suggestions provider for searched for repository queries
     */
    public TestSearchProvider() {
        setupSuggestions(AUTHORITY, DATABASE_MODE_QUERIES);
    }
}
```

``` java
public class SecondActivity extends SherlockFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        handleIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu optionMenu) {
         getSupportMenuInflater().inflate(R.menu.activity_main, optionMenu);
         return super.onCreateOptionsMenu(optionMenu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.m_search:
            onSearchRequested();
            return true;
        case android.R.id.home:
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
     @Override
        protected void onNewIntent(Intent intent) {
            setIntent(intent);
            handleIntent(intent);
        }

        private void handleIntent(Intent intent) {
            if (ACTION_SEARCH.equals(intent.getAction()))
                search(intent.getStringExtra(QUERY));
        }
        private void search(final String query) {
            getSupportActionBar().setTitle(query);
            TestSearchProvider.save(this, query);
        }
}
```

``` java
public class MainActivity extends SherlockFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ActionBar ab = getSupportActionBar();
        // set defaults for logo & home up
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu optionMenu) {
        MenuInflater inflate = getSupportMenuInflater();
        inflate.inflate(R.menu.activity_main, optionMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(\"test search\", \"click\");

        switch (item.getItemId()) {
        case R.id.m_search:
            onSearchRequested();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

}
```
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/785","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/785/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/785/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/785/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/785","id":10042755,"node_id":"MDU6SXNzdWUxMDA0Mjc1NQ==","number":785,"title":"I use SearchView, but unlike the system of SearchView easy to use","user":{"login":"yaming116","id":2171071,"node_id":"MDQ6VXNlcjIxNzEwNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/2171071?v=4","gravatar_id":"","url":"https://api.github.com/users/yaming116","html_url":"https://github.com/yaming116","followers_url":"https://api.github.com/users/yaming116/followers","following_url":"https://api.github.com/users/yaming116/following{/other_user}","gists_url":"https://api.github.com/users/yaming116/gists{/gist_id}","starred_url":"https://api.github.com/users/yaming116/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yaming116/subscriptions","organizations_url":"https://api.github.com/users/yaming116/orgs","repos_url":"https://api.github.com/users/yaming116/repos","events_url":"https://api.github.com/users/yaming116/events{/privacy}","received_events_url":"https://api.github.com/users/yaming116/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-01-17T01:11:14Z","updated_at":"2013-01-22T05:21:18Z","closed_at":"2013-01-22T05:21:18Z","author_association":"NONE","body":"I view the document, but there are some problems.I clicked on the actionbar on searchbutton, not performed onOptionsItemSelected, click on the keyboard searchButton no reaction.Here is my code。\n\n``` java\n<menu xmlns:android=\"http://schemas.android.com/apk/res/android\" >\n    <item\n        android:id=\"@+id/m_search\"\n        android:icon=\"@drawable/action_search\"\n        android:showAsAction=\"ifRoom\"\n        android:actionViewClass=\"com.actionbarsherlock.widget.SearchView\"\n        android:title=\"search\"/>\n</menu>\n```\n\n``` java\n<searchable xmlns:android=\"http://schemas.android.com/apk/res/android\"\n    android:hint=\"3333\"\n    android:label=\"@string/app_name\"\n    android:searchSuggestAuthority=\"com.yaming.test.actionbar\"\n    android:searchSuggestSelection=\" ?\"\n    android:voiceSearchMode=\"showVoiceSearchButton|launchRecognizer\" />\n```\n\n``` java\n <activity\n            android:name=\"com.example.actionbartest.MainActivity\"\n            android:hardwareAccelerated=\"true\"\n            android:label=\"@string/app_name\" >\n            <intent-filter>\n                <action android:name=\"android.intent.action.MAIN\" />\n                <category android:name=\"android.intent.category.LAUNCHER\" />\n            </intent-filter>\n            <meta-data\n                android:name=\"android.app.default_searchable\"\n                android:value=\"com.example.actionbartest.SecondActivity\" />\n        </activity>\n        <activity\n            android:name=\"com.example.actionbartest.SecondActivity\"\n            android:hardwareAccelerated=\"true\"\n            android:launchMode=\"singleTop\"\n            android:label=\"@string/app_name\" >\n            <intent-filter>\n                <action android:name=\"android.intent.action.SEARCH\" />\n            </intent-filter>\n            <meta-data\n                android:name=\"android.app.searchable\"\n                android:resource=\"@xml/test\" />\n        </activity>\n\n        <provider\n            android:name=\".TestSearchProvider\"\n            android:authorities=\"com.yaming.test.actionbar\"\n            android:exported=\"false\" />\n```\n\n``` java\npublic class TestSearchProvider extends SearchRecentSuggestionsProvider {\n    private static final String AUTHORITY = \"com.yaming.test.actionbar\";\n\n    /**\n     * Save query to history\n     * \n     * @param context\n     * @param query\n     */\n    public static void save(Context context, String query) {\n        suggestions(context).saveRecentQuery(query, null);\n    }\n\n    /**\n     * Clear query history\n     * \n     * @param context\n     */\n    public static void clear(Context context) {\n        suggestions(context).clearHistory();\n    }\n\n    private static SearchRecentSuggestions suggestions(Context context) {\n        return new SearchRecentSuggestions(context, AUTHORITY,\n                DATABASE_MODE_QUERIES);\n    }\n\n    /**\n     * Create suggestions provider for searched for repository queries\n     */\n    public TestSearchProvider() {\n        setupSuggestions(AUTHORITY, DATABASE_MODE_QUERIES);\n    }\n}\n```\n\n``` java\npublic class SecondActivity extends SherlockFragmentActivity {\n    @Override\n    protected void onCreate(Bundle savedInstanceState) {\n        super.onCreate(savedInstanceState);\n        setContentView(R.layout.activity_main);\n        final ActionBar ab = getSupportActionBar();\n        ab.setDisplayHomeAsUpEnabled(true);\n        handleIntent(getIntent());\n    }\n\n    @Override\n    public boolean onCreateOptionsMenu(Menu optionMenu) {\n         getSupportMenuInflater().inflate(R.menu.activity_main, optionMenu);\n         return super.onCreateOptionsMenu(optionMenu);\n    }\n    @Override\n    public boolean onOptionsItemSelected(MenuItem item) {\n        switch (item.getItemId()) {\n        case R.id.m_search:\n            onSearchRequested();\n            return true;\n        case android.R.id.home:\n            return true;\n        default:\n            return super.onOptionsItemSelected(item);\n        }\n    }\n     @Override\n        protected void onNewIntent(Intent intent) {\n            setIntent(intent);\n            handleIntent(intent);\n        }\n\n        private void handleIntent(Intent intent) {\n            if (ACTION_SEARCH.equals(intent.getAction()))\n                search(intent.getStringExtra(QUERY));\n        }\n        private void search(final String query) {\n            getSupportActionBar().setTitle(query);\n            TestSearchProvider.save(this, query);\n        }\n}\n```\n\n``` java\npublic class MainActivity extends SherlockFragmentActivity {\n\n    @Override\n    protected void onCreate(Bundle savedInstanceState) {\n        super.onCreate(savedInstanceState);\n        setContentView(R.layout.activity_main);\n\n        final ActionBar ab = getSupportActionBar();\n        // set defaults for logo & home up\n        ab.setDisplayHomeAsUpEnabled(true);\n        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);\n\n    }\n\n    @Override\n    public boolean onCreateOptionsMenu(Menu optionMenu) {\n        MenuInflater inflate = getSupportMenuInflater();\n        inflate.inflate(R.menu.activity_main, optionMenu);\n        return true;\n    }\n\n    @Override\n    public boolean onOptionsItemSelected(MenuItem item) {\n        Log.v(\"test search\", \"click\");\n\n        switch (item.getItemId()) {\n        case R.id.m_search:\n            onSearchRequested();\n            return true;\n        default:\n            return super.onOptionsItemSelected(item);\n        }\n    }\n\n}\n```\n","closed_by":{"login":"yaming116","id":2171071,"node_id":"MDQ6VXNlcjIxNzEwNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/2171071?v=4","gravatar_id":"","url":"https://api.github.com/users/yaming116","html_url":"https://github.com/yaming116","followers_url":"https://api.github.com/users/yaming116/followers","following_url":"https://api.github.com/users/yaming116/following{/other_user}","gists_url":"https://api.github.com/users/yaming116/gists{/gist_id}","starred_url":"https://api.github.com/users/yaming116/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yaming116/subscriptions","organizations_url":"https://api.github.com/users/yaming116/orgs","repos_url":"https://api.github.com/users/yaming116/repos","events_url":"https://api.github.com/users/yaming116/events{/privacy}","received_events_url":"https://api.github.com/users/yaming116/received_events","type":"User","site_admin":false}}", "commentIds":["12515185"], "labels":[]}