{"id":"1078", "title":"IcsLinearLayout to support dividers for horizontal orientation", "body":"Hello,

I've used this cool library for a long time (used version 4.2.0), and I've even used IcsLinearLayout just for adding the support of the dividers for certain layouts across the app.

Lately I've tried it while using an RTL language (Hebrew in my case) for the device by setting the device language to Hebrew and adding supportsRtl=\"true\" to the manifest. 
I've noticed that the dividers are gone when using a horizontal orientation.

I've downloaded the newest version (4.4.0) , and not only that this problem still exists, but it occurs for LTR languages (English in my case) too.

Here's a sample to show the problem. note that when I use vertical orientation (or when using the normal LinearLayout on newest Android versions) , it works just fine . 

divider.xml

```
<?xml version=\"1.0\" encoding=\"utf-8\"?>
<shape xmlns:android=\"http://schemas.android.com/apk/res/android\" >

<size
    android:height=\"1dp\"
    android:width=\"1dp\" />

<solid android:color=\"#FF000000\" />

</shape>
```

activity_main.xml 

```
<com.actionbarsherlock.internal.widget.IcsLinearLayout xmlns:android=\"http://schemas.android.com/apk/res/android\"
xmlns:tools=\"http://schemas.android.com/tools\"
android:layout_width=\"match_parent\"
android:layout_height=\"wrap_content\"
android:divider=\"@drawable/divider\"
android:gravity=\"center_horizontal\"
android:measureWithLargestChild=\"true\"
android:orientation=\"horizontal\"
android:showDividers=\"middle\"
tools:context=\".MainActivity\" >

<View
    android:layout_width=\"0px\"
    android:layout_height=\"wrap_content\"
    android:layout_weight=\"1\"
    android:background=\"#FFff0000\" />

<View
    android:layout_width=\"0px\"
    android:layout_height=\"wrap_content\"
    android:layout_weight=\"1\"
    android:background=\"#FFffff00\" />

<View
    android:layout_width=\"0px\"
    android:layout_height=\"wrap_content\"
    android:layout_weight=\"1\"
    android:background=\"#FFff00ff\" />

</com.actionbarsherlock.internal.widget.IcsLinearLayout>
```

Please help...
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/1078","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/1078/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/1078/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/1078/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/1078","id":24678701,"node_id":"MDU6SXNzdWUyNDY3ODcwMQ==","number":1078,"title":"IcsLinearLayout to support dividers for horizontal orientation","user":{"login":"AndroidDeveloperLB","id":5357526,"node_id":"MDQ6VXNlcjUzNTc1MjY=","avatar_url":"https://avatars0.githubusercontent.com/u/5357526?v=4","gravatar_id":"","url":"https://api.github.com/users/AndroidDeveloperLB","html_url":"https://github.com/AndroidDeveloperLB","followers_url":"https://api.github.com/users/AndroidDeveloperLB/followers","following_url":"https://api.github.com/users/AndroidDeveloperLB/following{/other_user}","gists_url":"https://api.github.com/users/AndroidDeveloperLB/gists{/gist_id}","starred_url":"https://api.github.com/users/AndroidDeveloperLB/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/AndroidDeveloperLB/subscriptions","organizations_url":"https://api.github.com/users/AndroidDeveloperLB/orgs","repos_url":"https://api.github.com/users/AndroidDeveloperLB/repos","events_url":"https://api.github.com/users/AndroidDeveloperLB/events{/privacy}","received_events_url":"https://api.github.com/users/AndroidDeveloperLB/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2013-12-22T10:56:34Z","updated_at":"2013-12-22T11:14:07Z","closed_at":"2013-12-22T10:58:18Z","author_association":"NONE","body":"Hello,\n\nI've used this cool library for a long time (used version 4.2.0), and I've even used IcsLinearLayout just for adding the support of the dividers for certain layouts across the app.\n\nLately I've tried it while using an RTL language (Hebrew in my case) for the device by setting the device language to Hebrew and adding supportsRtl=\"true\" to the manifest. \nI've noticed that the dividers are gone when using a horizontal orientation.\n\nI've downloaded the newest version (4.4.0) , and not only that this problem still exists, but it occurs for LTR languages (English in my case) too.\n\nHere's a sample to show the problem. note that when I use vertical orientation (or when using the normal LinearLayout on newest Android versions) , it works just fine . \n\ndivider.xml\n\n```\n<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<shape xmlns:android=\"http://schemas.android.com/apk/res/android\" >\n\n<size\n    android:height=\"1dp\"\n    android:width=\"1dp\" />\n\n<solid android:color=\"#FF000000\" />\n\n</shape>\n```\n\nactivity_main.xml \n\n```\n<com.actionbarsherlock.internal.widget.IcsLinearLayout xmlns:android=\"http://schemas.android.com/apk/res/android\"\nxmlns:tools=\"http://schemas.android.com/tools\"\nandroid:layout_width=\"match_parent\"\nandroid:layout_height=\"wrap_content\"\nandroid:divider=\"@drawable/divider\"\nandroid:gravity=\"center_horizontal\"\nandroid:measureWithLargestChild=\"true\"\nandroid:orientation=\"horizontal\"\nandroid:showDividers=\"middle\"\ntools:context=\".MainActivity\" >\n\n<View\n    android:layout_width=\"0px\"\n    android:layout_height=\"wrap_content\"\n    android:layout_weight=\"1\"\n    android:background=\"#FFff0000\" />\n\n<View\n    android:layout_width=\"0px\"\n    android:layout_height=\"wrap_content\"\n    android:layout_weight=\"1\"\n    android:background=\"#FFffff00\" />\n\n<View\n    android:layout_width=\"0px\"\n    android:layout_height=\"wrap_content\"\n    android:layout_weight=\"1\"\n    android:background=\"#FFff00ff\" />\n\n</com.actionbarsherlock.internal.widget.IcsLinearLayout>\n```\n\nPlease help...\n","closed_by":{"login":"JakeWharton","id":66577,"node_id":"MDQ6VXNlcjY2NTc3","avatar_url":"https://avatars0.githubusercontent.com/u/66577?v=4","gravatar_id":"","url":"https://api.github.com/users/JakeWharton","html_url":"https://github.com/JakeWharton","followers_url":"https://api.github.com/users/JakeWharton/followers","following_url":"https://api.github.com/users/JakeWharton/following{/other_user}","gists_url":"https://api.github.com/users/JakeWharton/gists{/gist_id}","starred_url":"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JakeWharton/subscriptions","organizations_url":"https://api.github.com/users/JakeWharton/orgs","repos_url":"https://api.github.com/users/JakeWharton/repos","events_url":"https://api.github.com/users/JakeWharton/events{/privacy}","received_events_url":"https://api.github.com/users/JakeWharton/received_events","type":"User","site_admin":false}}", "commentIds":["31082921","31082947"], "labels":[]}