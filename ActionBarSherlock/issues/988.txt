{"id":"988", "title":"Menu Items display in action bar in 2.1, but not in 4.0.3, 4.2 (nexus 4)using ActionBarSherlock", "body":"I am trying to implement ActionBarSherlock in my project.Adding menu/action items to the action bar is working on 2.1 but not in my nexus4, not in 4.0.3 emulator too. The problem is the function onCreateOptionsMenu(Menu menu) never gets called in my device but gets called in 2.1 and hence no action items are shown in action bar in my phone.

I have included ActionBarSherlock as library in my project.
I have set appropriate theme in application in manifest file.
I have extended my activity class with SherlockActivity.
I have overriden appropriate class from sherlock library ( so far i know, if not, menu would not have been shown in action bar in 2.1 too)
Following is a part of manifest where theme is set.

``` <application
        android:allowBackup=\"true\"
        android:icon=\"@drawable/ic_launcher\"
        android:label=\"@string/app_name\"
        android:theme=\"@style/Theme.Sherlock.Light.DarkActionBar\" >`

```

Following is menu.xml file:

```
<?xml version=\"1.0\" encoding=\"utf-8\"?>
<menu xmlns:android=\"http://schemas.android.com/apk/res/android\" >

    <item
        android:id=\"@+id/menu_item_call\"
        android:showAsAction=\"always\"
        android:title=\"Call\"/>
    <item
        android:id=\"@+id/menu_item_share\"
        android:showAsAction=\"always\"
        android:title=\"Share\"/>
    <item
        android:id=\"@+id/menu_item_save\"
        android:showAsAction=\"always\"
        android:title=\"Save\"/>

</menu>
```

Following is relevant part of the activity.

```
    //other imports
        import com.actionbarsherlock.app.SherlockActivity;
        import com.actionbarsherlock.view.Menu;
        import com.actionbarsherlock.view.MenuInflater;
        import com.actionbarsherlock.view.Window;

        public class SelectCategories extends SherlockActivity implements
                OnClickListener {


        //other code


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // TODO Auto-generated method stub
            MenuInflater inflater = getSupportMenuInflater();
            inflater.inflate(R.menu.menu, menu);
            return super.onCreateOptionsMenu(menu);
//return true; also has no effect
        }
        }
```

I have been trying to solve this issue and it has been several hours but couldnt. Please help me. Thank you in advance.
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/988","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/988/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/988/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/988/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/988","id":16443866,"node_id":"MDU6SXNzdWUxNjQ0Mzg2Ng==","number":988,"title":"Menu Items display in action bar in 2.1, but not in 4.0.3, 4.2 (nexus 4)using ActionBarSherlock","user":{"login":"dipendrapkrl","id":2265698,"node_id":"MDQ6VXNlcjIyNjU2OTg=","avatar_url":"https://avatars2.githubusercontent.com/u/2265698?v=4","gravatar_id":"","url":"https://api.github.com/users/dipendrapkrl","html_url":"https://github.com/dipendrapkrl","followers_url":"https://api.github.com/users/dipendrapkrl/followers","following_url":"https://api.github.com/users/dipendrapkrl/following{/other_user}","gists_url":"https://api.github.com/users/dipendrapkrl/gists{/gist_id}","starred_url":"https://api.github.com/users/dipendrapkrl/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dipendrapkrl/subscriptions","organizations_url":"https://api.github.com/users/dipendrapkrl/orgs","repos_url":"https://api.github.com/users/dipendrapkrl/repos","events_url":"https://api.github.com/users/dipendrapkrl/events{/privacy}","received_events_url":"https://api.github.com/users/dipendrapkrl/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-07-07T14:48:37Z","updated_at":"2013-07-07T15:11:51Z","closed_at":"2013-07-07T15:11:51Z","author_association":"NONE","body":"I am trying to implement ActionBarSherlock in my project.Adding menu/action items to the action bar is working on 2.1 but not in my nexus4, not in 4.0.3 emulator too. The problem is the function onCreateOptionsMenu(Menu menu) never gets called in my device but gets called in 2.1 and hence no action items are shown in action bar in my phone.\n\nI have included ActionBarSherlock as library in my project.\nI have set appropriate theme in application in manifest file.\nI have extended my activity class with SherlockActivity.\nI have overriden appropriate class from sherlock library ( so far i know, if not, menu would not have been shown in action bar in 2.1 too)\nFollowing is a part of manifest where theme is set.\n\n``` <application\n        android:allowBackup=\"true\"\n        android:icon=\"@drawable/ic_launcher\"\n        android:label=\"@string/app_name\"\n        android:theme=\"@style/Theme.Sherlock.Light.DarkActionBar\" >`\n\n```\n\nFollowing is menu.xml file:\n\n```\n<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<menu xmlns:android=\"http://schemas.android.com/apk/res/android\" >\n\n    <item\n        android:id=\"@+id/menu_item_call\"\n        android:showAsAction=\"always\"\n        android:title=\"Call\"/>\n    <item\n        android:id=\"@+id/menu_item_share\"\n        android:showAsAction=\"always\"\n        android:title=\"Share\"/>\n    <item\n        android:id=\"@+id/menu_item_save\"\n        android:showAsAction=\"always\"\n        android:title=\"Save\"/>\n\n</menu>\n```\n\nFollowing is relevant part of the activity.\n\n```\n    //other imports\n        import com.actionbarsherlock.app.SherlockActivity;\n        import com.actionbarsherlock.view.Menu;\n        import com.actionbarsherlock.view.MenuInflater;\n        import com.actionbarsherlock.view.Window;\n\n        public class SelectCategories extends SherlockActivity implements\n                OnClickListener {\n\n\n        //other code\n\n\n        @Override\n        public boolean onCreateOptionsMenu(Menu menu) {\n            // TODO Auto-generated method stub\n            MenuInflater inflater = getSupportMenuInflater();\n            inflater.inflate(R.menu.menu, menu);\n            return super.onCreateOptionsMenu(menu);\n//return true; also has no effect\n        }\n        }\n```\n\nI have been trying to solve this issue and it has been several hours but couldnt. Please help me. Thank you in advance.\n","closed_by":{"login":"JakeWharton","id":66577,"node_id":"MDQ6VXNlcjY2NTc3","avatar_url":"https://avatars0.githubusercontent.com/u/66577?v=4","gravatar_id":"","url":"https://api.github.com/users/JakeWharton","html_url":"https://github.com/JakeWharton","followers_url":"https://api.github.com/users/JakeWharton/followers","following_url":"https://api.github.com/users/JakeWharton/following{/other_user}","gists_url":"https://api.github.com/users/JakeWharton/gists{/gist_id}","starred_url":"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JakeWharton/subscriptions","organizations_url":"https://api.github.com/users/JakeWharton/orgs","repos_url":"https://api.github.com/users/JakeWharton/repos","events_url":"https://api.github.com/users/JakeWharton/events{/privacy}","received_events_url":"https://api.github.com/users/JakeWharton/received_events","type":"User","site_admin":false}}", "commentIds":["20572111"], "labels":[]}