{"id":"588", "title":"Action mode issues on orientation change when using actionbar", "body":"When starting action mode in landscape on a 2.3.x device (probably on all, just tested on these) while using an actionbar and you rotate the screen the buttons are not redrawn.  This is just one example of many orientation change issues that I have found when using an actionbar and actionmode together.  I also get overdraw of the different menus and unresponsive buttons.

Tested with 4.1 and dev branch, same issue.

To duplicate this particular issue:

start activity, rotate to landscape, press start button, rotate to portrait.

This is the sample code modified to show the problem.  Add this to manifest also for splitactiomodes: 

 android:configChanges=\"keyboard|keyboardHidden|orientation|screenSize\"

/*
- Copyright (C) 2011 Jake Wharton
  *
- Licensed under the Apache License, Version 2.0 (the \"License\");
- you may not use this file except in compliance with the License.
- You may obtain a copy of the License at
  *
-      http://www.apache.org/licenses/LICENSE-2.0
  *
- Unless required by applicable law or agreed to in writing, software
- distributed under the License is distributed on an \"AS IS\" BASIS,
- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
- See the License for the specific language governing permissions and
- limitations under the License.
  */
  package com.actionbarsherlock.sample.demos;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ActionModes extends SherlockActivity {
    ActionMode mMode;

```
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    //Used to put dark icons on light action bar
    boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;

    menu.add(\"Save\")
        .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

    menu.add(\"Search\")
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

    menu.add(\"Refresh\")
        .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

    return true;
}


@Override
public boolean onOptionsItemSelected(MenuItem item) 
{
    boolean eventConsumed = false;

    Toast.makeText(this, \"Item selected \"+item.getItemId(), Toast.LENGTH_LONG).show();

    return eventConsumed;
}


@Override
public void onConfigurationChanged(Configuration newConfig) 
{
    super.onConfigurationChanged(newConfig);
    android.util.Log.i(\"AM\", \"onConfigurationChanged\");
}


@Override
protected void onCreate(Bundle savedInstanceState) {
    setTheme(SampleList.THEME); //Used for theme switching in samples
    super.onCreate(savedInstanceState);
    setContentView(R.layout.action_modes);

    ((Button)findViewById(R.id.start)).setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {
            mMode = startActionMode(new AnActionModeOfEpicProportions());
        }
    });
    ((Button)findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {
            if (mMode != null) {
                mMode.finish();
            }
        }
    });
}

private final class AnActionModeOfEpicProportions implements ActionMode.Callback {

    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        //Used to put dark icons on light action bar
        boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;

        menu.add(\"Save\")
            .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(\"Search\")
            .setIcon(isLight ? R.drawable.ic_search_inverse : R.drawable.ic_search)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(\"Refresh\")
            .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(\"Save\")
            .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(\"Search\")
            .setIcon(isLight ? R.drawable.ic_search_inverse : R.drawable.ic_search)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(\"Refresh\")
            .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }


    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }


    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        Toast.makeText(ActionModes.this, \"Got click: \" + item, Toast.LENGTH_SHORT).show();
        mode.finish();
        return true;
    }


    public void onDestroyActionMode(ActionMode mode) {
    }
}
```

}
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/588","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/588/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/588/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/588/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/588","id":6438418,"node_id":"MDU6SXNzdWU2NDM4NDE4","number":588,"title":"Action mode issues on orientation change when using actionbar","user":{"login":"rayomvox","id":2205826,"node_id":"MDQ6VXNlcjIyMDU4MjY=","avatar_url":"https://avatars3.githubusercontent.com/u/2205826?v=4","gravatar_id":"","url":"https://api.github.com/users/rayomvox","html_url":"https://github.com/rayomvox","followers_url":"https://api.github.com/users/rayomvox/followers","following_url":"https://api.github.com/users/rayomvox/following{/other_user}","gists_url":"https://api.github.com/users/rayomvox/gists{/gist_id}","starred_url":"https://api.github.com/users/rayomvox/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rayomvox/subscriptions","organizations_url":"https://api.github.com/users/rayomvox/orgs","repos_url":"https://api.github.com/users/rayomvox/repos","events_url":"https://api.github.com/users/rayomvox/events{/privacy}","received_events_url":"https://api.github.com/users/rayomvox/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2012-08-24T16:28:35Z","updated_at":"2012-08-24T16:36:50Z","closed_at":"2012-08-24T16:29:59Z","author_association":"NONE","body":"When starting action mode in landscape on a 2.3.x device (probably on all, just tested on these) while using an actionbar and you rotate the screen the buttons are not redrawn.  This is just one example of many orientation change issues that I have found when using an actionbar and actionmode together.  I also get overdraw of the different menus and unresponsive buttons.\n\nTested with 4.1 and dev branch, same issue.\n\nTo duplicate this particular issue:\n\nstart activity, rotate to landscape, press start button, rotate to portrait.\n\nThis is the sample code modified to show the problem.  Add this to manifest also for splitactiomodes: \n\n android:configChanges=\"keyboard|keyboardHidden|orientation|screenSize\"\n\n/*\n- Copyright (C) 2011 Jake Wharton\n  *\n- Licensed under the Apache License, Version 2.0 (the \"License\");\n- you may not use this file except in compliance with the License.\n- You may obtain a copy of the License at\n  *\n-      http://www.apache.org/licenses/LICENSE-2.0\n  *\n- Unless required by applicable law or agreed to in writing, software\n- distributed under the License is distributed on an \"AS IS\" BASIS,\n- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n- See the License for the specific language governing permissions and\n- limitations under the License.\n  */\n  package com.actionbarsherlock.sample.demos;\n\nimport android.content.res.Configuration;\nimport android.os.Bundle;\nimport android.view.View;\nimport android.widget.Button;\nimport android.widget.TextView;\nimport android.widget.Toast;\n\nimport com.actionbarsherlock.app.SherlockActivity;\nimport com.actionbarsherlock.view.ActionMode;\nimport com.actionbarsherlock.view.Menu;\nimport com.actionbarsherlock.view.MenuItem;\n\npublic class ActionModes extends SherlockActivity {\n    ActionMode mMode;\n\n```\n@Override\npublic boolean onCreateOptionsMenu(Menu menu) {\n    //Used to put dark icons on light action bar\n    boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;\n\n    menu.add(\"Save\")\n        .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)\n        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n    menu.add(\"Search\")\n        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);\n\n    menu.add(\"Refresh\")\n        .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)\n        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);\n\n    return true;\n}\n\n\n@Override\npublic boolean onOptionsItemSelected(MenuItem item) \n{\n    boolean eventConsumed = false;\n\n    Toast.makeText(this, \"Item selected \"+item.getItemId(), Toast.LENGTH_LONG).show();\n\n    return eventConsumed;\n}\n\n\n@Override\npublic void onConfigurationChanged(Configuration newConfig) \n{\n    super.onConfigurationChanged(newConfig);\n    android.util.Log.i(\"AM\", \"onConfigurationChanged\");\n}\n\n\n@Override\nprotected void onCreate(Bundle savedInstanceState) {\n    setTheme(SampleList.THEME); //Used for theme switching in samples\n    super.onCreate(savedInstanceState);\n    setContentView(R.layout.action_modes);\n\n    ((Button)findViewById(R.id.start)).setOnClickListener(new View.OnClickListener() {\n\n        public void onClick(View v) {\n            mMode = startActionMode(new AnActionModeOfEpicProportions());\n        }\n    });\n    ((Button)findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {\n\n        public void onClick(View v) {\n            if (mMode != null) {\n                mMode.finish();\n            }\n        }\n    });\n}\n\nprivate final class AnActionModeOfEpicProportions implements ActionMode.Callback {\n\n    public boolean onCreateActionMode(ActionMode mode, Menu menu) {\n        //Used to put dark icons on light action bar\n        boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;\n\n        menu.add(\"Save\")\n            .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        menu.add(\"Search\")\n            .setIcon(isLight ? R.drawable.ic_search_inverse : R.drawable.ic_search)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        menu.add(\"Refresh\")\n            .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        menu.add(\"Save\")\n            .setIcon(isLight ? R.drawable.ic_compose_inverse : R.drawable.ic_compose)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        menu.add(\"Search\")\n            .setIcon(isLight ? R.drawable.ic_search_inverse : R.drawable.ic_search)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        menu.add(\"Refresh\")\n            .setIcon(isLight ? R.drawable.ic_refresh_inverse : R.drawable.ic_refresh)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n\n        return true;\n    }\n\n\n    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {\n        return false;\n    }\n\n\n    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {\n        Toast.makeText(ActionModes.this, \"Got click: \" + item, Toast.LENGTH_SHORT).show();\n        mode.finish();\n        return true;\n    }\n\n\n    public void onDestroyActionMode(ActionMode mode) {\n    }\n}\n```\n\n}\n","closed_by":{"login":"JakeWharton","id":66577,"node_id":"MDQ6VXNlcjY2NTc3","avatar_url":"https://avatars0.githubusercontent.com/u/66577?v=4","gravatar_id":"","url":"https://api.github.com/users/JakeWharton","html_url":"https://github.com/JakeWharton","followers_url":"https://api.github.com/users/JakeWharton/followers","following_url":"https://api.github.com/users/JakeWharton/following{/other_user}","gists_url":"https://api.github.com/users/JakeWharton/gists{/gist_id}","starred_url":"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JakeWharton/subscriptions","organizations_url":"https://api.github.com/users/JakeWharton/orgs","repos_url":"https://api.github.com/users/JakeWharton/repos","events_url":"https://api.github.com/users/JakeWharton/events{/privacy}","received_events_url":"https://api.github.com/users/JakeWharton/received_events","type":"User","site_admin":false}}", "commentIds":["8007543","8007701","8007733"], "labels":[]}