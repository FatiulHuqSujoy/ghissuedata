{"id":"811", "title":"Menu Item Not Show In Action Bar In Sherlock?", "body":"i create action menu from action bar sherlock sample like below

`````` java
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    //Used to put dark icons on light action bar


    //Create the search view
    SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
    searchView.setQueryHint(\"Search for countries…\");
    menu.add(\"Save\")
    .setIcon(R.drawable.abs__ic_menu_share_holo_light)
    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    menu.add(\"Search\")
        .setIcon( R.drawable.abs__ic_search)
        .setActionView(searchView)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

    return true;
}
```java
and my application theme theme is :

<style name=\"Theme.actor\" parent=\"@style/Theme.Sherlock.Light\">
</style>

but menu show in buttom of activity page not in actionbar?
``````
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/811","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/811/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/811/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/811/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/811","id":10338506,"node_id":"MDU6SXNzdWUxMDMzODUwNg==","number":811,"title":"Menu Item Not Show In Action Bar In Sherlock?","user":{"login":"atilazz","id":420355,"node_id":"MDQ6VXNlcjQyMDM1NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/420355?v=4","gravatar_id":"","url":"https://api.github.com/users/atilazz","html_url":"https://github.com/atilazz","followers_url":"https://api.github.com/users/atilazz/followers","following_url":"https://api.github.com/users/atilazz/following{/other_user}","gists_url":"https://api.github.com/users/atilazz/gists{/gist_id}","starred_url":"https://api.github.com/users/atilazz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/atilazz/subscriptions","organizations_url":"https://api.github.com/users/atilazz/orgs","repos_url":"https://api.github.com/users/atilazz/repos","events_url":"https://api.github.com/users/atilazz/events{/privacy}","received_events_url":"https://api.github.com/users/atilazz/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-01-26T14:24:42Z","updated_at":"2013-01-27T08:13:47Z","closed_at":"2013-01-27T08:13:47Z","author_association":"NONE","body":"i create action menu from action bar sherlock sample like below\n\n`````` java\n@Override\npublic boolean onCreateOptionsMenu(Menu menu) {\n    //Used to put dark icons on light action bar\n\n\n    //Create the search view\n    SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());\n    searchView.setQueryHint(\"Search for countries…\");\n    menu.add(\"Save\")\n    .setIcon(R.drawable.abs__ic_menu_share_holo_light)\n    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);\n    menu.add(\"Search\")\n        .setIcon( R.drawable.abs__ic_search)\n        .setActionView(searchView)\n        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);\n\n    return true;\n}\n```java\nand my application theme theme is :\n\n<style name=\"Theme.actor\" parent=\"@style/Theme.Sherlock.Light\">\n</style>\n\nbut menu show in buttom of activity page not in actionbar?\n``````\n","closed_by":{"login":"atilazz","id":420355,"node_id":"MDQ6VXNlcjQyMDM1NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/420355?v=4","gravatar_id":"","url":"https://api.github.com/users/atilazz","html_url":"https://github.com/atilazz","followers_url":"https://api.github.com/users/atilazz/followers","following_url":"https://api.github.com/users/atilazz/following{/other_user}","gists_url":"https://api.github.com/users/atilazz/gists{/gist_id}","starred_url":"https://api.github.com/users/atilazz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/atilazz/subscriptions","organizations_url":"https://api.github.com/users/atilazz/orgs","repos_url":"https://api.github.com/users/atilazz/repos","events_url":"https://api.github.com/users/atilazz/events{/privacy}","received_events_url":"https://api.github.com/users/atilazz/received_events","type":"User","site_admin":false}}", "commentIds":["12750628"], "labels":[]}