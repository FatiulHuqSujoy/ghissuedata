{"id":"781", "title":"Creating SearchView is failure.", "body":"I'm using ActionBarSherlock 4.2.0.

On Android 2.3 or older, a creating SearchView instance shows the following error message.

```
Could not find class 'com.actionbarsherlock.widget.SearchView$11', referenced from method com.actionbarsherlock.widget.SearchView.<init>
```

But, this message is not shown on Android 4.1 device.

Since `android.view.View.OnLayoutChangeListener` is available on Android 3.0 or later, the above problem is occurred.

SearchView.java, line 338:

```
if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
    mDropDownAnchor.addOnLayoutChangeListener(new OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                                             int oldLeft, int oldTop, int oldRight, int oldBottom) {
            adjustDropDownSizeAndPosition();
        }
    });
} else {
```

In this case, I think that ClassLoader or lazy class loading should be used.
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/781","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/781/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/781/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/781/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/781","id":10006654,"node_id":"MDU6SXNzdWUxMDAwNjY1NA==","number":781,"title":"Creating SearchView is failure.","user":{"login":"trancebeat","id":121032,"node_id":"MDQ6VXNlcjEyMTAzMg==","avatar_url":"https://avatars3.githubusercontent.com/u/121032?v=4","gravatar_id":"","url":"https://api.github.com/users/trancebeat","html_url":"https://github.com/trancebeat","followers_url":"https://api.github.com/users/trancebeat/followers","following_url":"https://api.github.com/users/trancebeat/following{/other_user}","gists_url":"https://api.github.com/users/trancebeat/gists{/gist_id}","starred_url":"https://api.github.com/users/trancebeat/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trancebeat/subscriptions","organizations_url":"https://api.github.com/users/trancebeat/orgs","repos_url":"https://api.github.com/users/trancebeat/repos","events_url":"https://api.github.com/users/trancebeat/events{/privacy}","received_events_url":"https://api.github.com/users/trancebeat/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2013-01-16T03:56:24Z","updated_at":"2013-01-17T19:33:48Z","closed_at":"2013-01-17T19:33:48Z","author_association":"NONE","body":"I'm using ActionBarSherlock 4.2.0.\n\nOn Android 2.3 or older, a creating SearchView instance shows the following error message.\n\n```\nCould not find class 'com.actionbarsherlock.widget.SearchView$11', referenced from method com.actionbarsherlock.widget.SearchView.<init>\n```\n\nBut, this message is not shown on Android 4.1 device.\n\nSince `android.view.View.OnLayoutChangeListener` is available on Android 3.0 or later, the above problem is occurred.\n\nSearchView.java, line 338:\n\n```\nif (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {\n    mDropDownAnchor.addOnLayoutChangeListener(new OnLayoutChangeListener() {\n        @Override\n        public void onLayoutChange(View v, int left, int top, int right, int bottom,\n                                                             int oldLeft, int oldTop, int oldRight, int oldBottom) {\n            adjustDropDownSizeAndPosition();\n        }\n    });\n} else {\n```\n\nIn this case, I think that ClassLoader or lazy class loading should be used.\n","closed_by":{"login":"SimonVT","id":549365,"node_id":"MDQ6VXNlcjU0OTM2NQ==","avatar_url":"https://avatars0.githubusercontent.com/u/549365?v=4","gravatar_id":"","url":"https://api.github.com/users/SimonVT","html_url":"https://github.com/SimonVT","followers_url":"https://api.github.com/users/SimonVT/followers","following_url":"https://api.github.com/users/SimonVT/following{/other_user}","gists_url":"https://api.github.com/users/SimonVT/gists{/gist_id}","starred_url":"https://api.github.com/users/SimonVT/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SimonVT/subscriptions","organizations_url":"https://api.github.com/users/SimonVT/orgs","repos_url":"https://api.github.com/users/SimonVT/repos","events_url":"https://api.github.com/users/SimonVT/events{/privacy}","received_events_url":"https://api.github.com/users/SimonVT/received_events","type":"User","site_admin":false}}", "commentIds":["12305876","12360831","12385038"], "labels":[]}