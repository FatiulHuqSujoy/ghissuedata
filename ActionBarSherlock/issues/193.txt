{"id":"193", "title":"Null background activity can not draw action bar items properly with invalidateOptionsMenu()", "body":"I found a drawing bug  on the API 9 phone with ABS v4.0, beta 5.
Action bar Items are not  drawn properly  by calling the invalidateOptionsMenu().
This bug does not occur on the API 15 emulator without ABS.

Sample code is like this.

```
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    // Set activity background to null
    getWindow().setBackgroundDrawable(null); 

    Button button = (Button)findViewById(R.id.button);
    button.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            mItems++;
            if (mItems>= 8) {
                mItems = 0;
            }
            invalidateOptionsMenu();
        }
    });
}

public boolean onCreateOptionsMenu(Menu menu) {
    for (int i = 0; i < mItems; i++) {
        String title = String.format(\"Item: %d\", i);
        int iconId = (i % 2 == 0) ? R.drawable.ic_search : R.drawable.ic_refresh;
        menu.add(title)
            .setIcon(iconId)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }
    return super.onCreateOptionsMenu(menu);
}
```
", "json":"{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/193","repository_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/193/labels{/name}","comments_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/193/comments","events_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/issues/193/events","html_url":"https://github.com/JakeWharton/ActionBarSherlock/issues/193","id":2872101,"node_id":"MDU6SXNzdWUyODcyMTAx","number":193,"title":"Null background activity can not draw action bar items properly with invalidateOptionsMenu()","user":{"login":"naoak","id":1264530,"node_id":"MDQ6VXNlcjEyNjQ1MzA=","avatar_url":"https://avatars3.githubusercontent.com/u/1264530?v=4","gravatar_id":"","url":"https://api.github.com/users/naoak","html_url":"https://github.com/naoak","followers_url":"https://api.github.com/users/naoak/followers","following_url":"https://api.github.com/users/naoak/following{/other_user}","gists_url":"https://api.github.com/users/naoak/gists{/gist_id}","starred_url":"https://api.github.com/users/naoak/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/naoak/subscriptions","organizations_url":"https://api.github.com/users/naoak/orgs","repos_url":"https://api.github.com/users/naoak/repos","events_url":"https://api.github.com/users/naoak/events{/privacy}","received_events_url":"https://api.github.com/users/naoak/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/milestones/4","html_url":"https://github.com/JakeWharton/ActionBarSherlock/milestone/4","labels_url":"https://api.github.com/repos/JakeWharton/ActionBarSherlock/milestones/4/labels","id":20462,"node_id":"MDk6TWlsZXN0b25lMjA0NjI=","number":4,"title":"4.0.0","description":"Rewritten widget from the sources of Android Ice Cream Sandwich.","creator":{"login":"JakeWharton","id":66577,"node_id":"MDQ6VXNlcjY2NTc3","avatar_url":"https://avatars0.githubusercontent.com/u/66577?v=4","gravatar_id":"","url":"https://api.github.com/users/JakeWharton","html_url":"https://github.com/JakeWharton","followers_url":"https://api.github.com/users/JakeWharton/followers","following_url":"https://api.github.com/users/JakeWharton/following{/other_user}","gists_url":"https://api.github.com/users/JakeWharton/gists{/gist_id}","starred_url":"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JakeWharton/subscriptions","organizations_url":"https://api.github.com/users/JakeWharton/orgs","repos_url":"https://api.github.com/users/JakeWharton/repos","events_url":"https://api.github.com/users/JakeWharton/events{/privacy}","received_events_url":"https://api.github.com/users/JakeWharton/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":131,"state":"closed","created_at":"2011-07-07T02:42:53Z","updated_at":"2013-02-14T23:22:24Z","due_on":"2012-03-07T08:00:00Z","closed_at":"2012-03-08T06:15:03Z"},"comments":2,"created_at":"2012-01-17T17:50:27Z","updated_at":"2012-01-20T10:07:29Z","closed_at":"2012-01-20T10:07:29Z","author_association":"NONE","body":"I found a drawing bug  on the API 9 phone with ABS v4.0, beta 5.\nAction bar Items are not  drawn properly  by calling the invalidateOptionsMenu().\nThis bug does not occur on the API 15 emulator without ABS.\n\nSample code is like this.\n\n```\npublic void onCreate(Bundle savedInstanceState) {\n    super.onCreate(savedInstanceState);\n    setContentView(R.layout.main);\n\n    // Set activity background to null\n    getWindow().setBackgroundDrawable(null); \n\n    Button button = (Button)findViewById(R.id.button);\n    button.setOnClickListener(new OnClickListener() {\n        @Override\n        public void onClick(View v) {\n            mItems++;\n            if (mItems>= 8) {\n                mItems = 0;\n            }\n            invalidateOptionsMenu();\n        }\n    });\n}\n\npublic boolean onCreateOptionsMenu(Menu menu) {\n    for (int i = 0; i < mItems; i++) {\n        String title = String.format(\"Item: %d\", i);\n        int iconId = (i % 2 == 0) ? R.drawable.ic_search : R.drawable.ic_refresh;\n        menu.add(title)\n            .setIcon(iconId)\n            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);\n    }\n    return super.onCreateOptionsMenu(menu);\n}\n```\n","closed_by":{"login":"naoak","id":1264530,"node_id":"MDQ6VXNlcjEyNjQ1MzA=","avatar_url":"https://avatars3.githubusercontent.com/u/1264530?v=4","gravatar_id":"","url":"https://api.github.com/users/naoak","html_url":"https://github.com/naoak","followers_url":"https://api.github.com/users/naoak/followers","following_url":"https://api.github.com/users/naoak/following{/other_user}","gists_url":"https://api.github.com/users/naoak/gists{/gist_id}","starred_url":"https://api.github.com/users/naoak/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/naoak/subscriptions","organizations_url":"https://api.github.com/users/naoak/orgs","repos_url":"https://api.github.com/users/naoak/repos","events_url":"https://api.github.com/users/naoak/events{/privacy}","received_events_url":"https://api.github.com/users/naoak/received_events","type":"User","site_admin":false}}", "commentIds":["3579298","3581379"], "labels":[]}