{"id":"5635", "title":"Unable to set ImplicitWait in IE", "body":"## Meta
OS:  Windows 10
Selenium Version:  Selenium Server Standalone 3.11.0, IEDriverServer 3.9.0 (32-bit)
Browser:  Internet Explorer 11

## Expected Behavior
Should be able to set ImplicitWait.
This is working fine in v3.4 and earlier version of Selenium Hub and IEDriverServer.

## Actual Behavior
Following exception is thrown:
```
OpenQA.Selenium.WebDriverException: Invalid timeout type specified: ms
Build info: version: '3.11.0', revision: 'e59cfb3', time: '2018-03-11T20:33:15.31Z'
System info: host: 'DESKTOP-S0LVI9F', ip: '10.0.2.15', os.name: 'Windows 10', os.arch: 'amd64', os.version: '10.0', java.version: '1.8.0_144'
Driver info: driver.version: unknown
   at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)
   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   at OpenQA.Selenium.Remote.RemoteTimeouts.ExecuteSetTimeout(String timeoutType, TimeSpan timeToWait)
   at OpenQA.Selenium.Remote.RemoteTimeouts.set_ImplicitWait(TimeSpan value)
```


## Steps to reproduce

```cmd
java -Dwebdriver.ie.driver.logfile=iedriver.log -Dwebdriver.ie.driver.loglevel=TRACE -Dwebdriver.chrome.driver=chromedriver.exe -Dwebdriver.ie.driver=IEDriverServer.exe -Dwebdriver.gecko.driver=geckodriver.exe -jar selenium-server-standalone-3.11.0.jar -port 4444 -timeout 320 -browserTimeout 320
```

```C#
DesiredCapabilities caps = new DesiredCapabilities();
caps.SetCapability(\"browserName\", \"internet explorer\");
var driver = new RemoteWebDriver(new Uri(\"http://localhost:4444/wd/hub\"), caps, TimeSpan.FromMinutes(2));
driver.Manage().Window.Size = new Size(1600, 900);
driver.Navigate().GoToUrl(\"https://ynet.co.il\");
driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(2000);
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5635","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5635/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5635/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5635/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5635","id":306245231,"node_id":"MDU6SXNzdWUzMDYyNDUyMzE=","number":5635,"title":"Unable to set ImplicitWait in IE","user":{"login":"romovs","id":2727288,"node_id":"MDQ6VXNlcjI3MjcyODg=","avatar_url":"https://avatars1.githubusercontent.com/u/2727288?v=4","gravatar_id":"","url":"https://api.github.com/users/romovs","html_url":"https://github.com/romovs","followers_url":"https://api.github.com/users/romovs/followers","following_url":"https://api.github.com/users/romovs/following{/other_user}","gists_url":"https://api.github.com/users/romovs/gists{/gist_id}","starred_url":"https://api.github.com/users/romovs/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/romovs/subscriptions","organizations_url":"https://api.github.com/users/romovs/orgs","repos_url":"https://api.github.com/users/romovs/repos","events_url":"https://api.github.com/users/romovs/events{/privacy}","received_events_url":"https://api.github.com/users/romovs/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-03-18T14:31:57Z","updated_at":"2019-08-16T09:09:58Z","closed_at":"2018-03-19T07:48:52Z","author_association":"NONE","body":"## Meta\r\nOS:  Windows 10\r\nSelenium Version:  Selenium Server Standalone 3.11.0, IEDriverServer 3.9.0 (32-bit)\r\nBrowser:  Internet Explorer 11\r\n\r\n## Expected Behavior\r\nShould be able to set ImplicitWait.\r\nThis is working fine in v3.4 and earlier version of Selenium Hub and IEDriverServer.\r\n\r\n## Actual Behavior\r\nFollowing exception is thrown:\r\n```\r\nOpenQA.Selenium.WebDriverException: Invalid timeout type specified: ms\r\nBuild info: version: '3.11.0', revision: 'e59cfb3', time: '2018-03-11T20:33:15.31Z'\r\nSystem info: host: 'DESKTOP-S0LVI9F', ip: '10.0.2.15', os.name: 'Windows 10', os.arch: 'amd64', os.version: '10.0', java.version: '1.8.0_144'\r\nDriver info: driver.version: unknown\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   at OpenQA.Selenium.Remote.RemoteTimeouts.ExecuteSetTimeout(String timeoutType, TimeSpan timeToWait)\r\n   at OpenQA.Selenium.Remote.RemoteTimeouts.set_ImplicitWait(TimeSpan value)\r\n```\r\n\r\n\r\n## Steps to reproduce\r\n\r\n```cmd\r\njava -Dwebdriver.ie.driver.logfile=iedriver.log -Dwebdriver.ie.driver.loglevel=TRACE -Dwebdriver.chrome.driver=chromedriver.exe -Dwebdriver.ie.driver=IEDriverServer.exe -Dwebdriver.gecko.driver=geckodriver.exe -jar selenium-server-standalone-3.11.0.jar -port 4444 -timeout 320 -browserTimeout 320\r\n```\r\n\r\n```C#\r\nDesiredCapabilities caps = new DesiredCapabilities();\r\ncaps.SetCapability(\"browserName\", \"internet explorer\");\r\nvar driver = new RemoteWebDriver(new Uri(\"http://localhost:4444/wd/hub\"), caps, TimeSpan.FromMinutes(2));\r\ndriver.Manage().Window.Size = new Size(1600, 900);\r\ndriver.Navigate().GoToUrl(\"https://ynet.co.il\");\r\ndriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(2000);\r\n```\r\n","closed_by":{"login":"romovs","id":2727288,"node_id":"MDQ6VXNlcjI3MjcyODg=","avatar_url":"https://avatars1.githubusercontent.com/u/2727288?v=4","gravatar_id":"","url":"https://api.github.com/users/romovs","html_url":"https://github.com/romovs","followers_url":"https://api.github.com/users/romovs/followers","following_url":"https://api.github.com/users/romovs/following{/other_user}","gists_url":"https://api.github.com/users/romovs/gists{/gist_id}","starred_url":"https://api.github.com/users/romovs/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/romovs/subscriptions","organizations_url":"https://api.github.com/users/romovs/orgs","repos_url":"https://api.github.com/users/romovs/repos","events_url":"https://api.github.com/users/romovs/events{/privacy}","received_events_url":"https://api.github.com/users/romovs/received_events","type":"User","site_admin":false}}", "commentIds":["374120036","374126437"], "labels":["R-awaiting answer"]}