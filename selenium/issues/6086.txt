{"id":"6086", "title":"IEDriver ignores noProxy settings", "body":"## Meta -
OS:  Windows Server 2012 R2 
Selenium Version:  3.13 (Java)
Browser:  Internet Explorer
Browser Version:  11.0.9600.18978

## Expected Behavior -
Specifying a list of addresses to bypass the configured proxy with setNoProxy() should work with IEDriver, but it does not.

## Actual Behavior -
IEDriver does not acknowledge noProxy, but the ChromeDriver works as expected.

## Steps to reproduce -
```
String myProxy = \"myproxy.example.com:80\";
String myNoProxy = \".example.com, .xyz.com\";

Proxy proxy = new Proxy();
proxy.setHttpProxy(myProxy);
proxy.setSslProxy(myProxy);
proxy.setNoProxy(myNoProxy);

InternetExplorerOptions options = new InternetExplorerOptions();
options.usePerProcessProxy();
options.setProxy(proxy);

WebDriver driver = new InternetExplorerDriver(options);
driver.get(\"https://example.com\");
driver.quit();
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6086","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6086/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6086/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6086/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6086","id":336323026,"node_id":"MDU6SXNzdWUzMzYzMjMwMjY=","number":6086,"title":"IEDriver ignores noProxy settings","user":{"login":"goldeelox","id":1199770,"node_id":"MDQ6VXNlcjExOTk3NzA=","avatar_url":"https://avatars3.githubusercontent.com/u/1199770?v=4","gravatar_id":"","url":"https://api.github.com/users/goldeelox","html_url":"https://github.com/goldeelox","followers_url":"https://api.github.com/users/goldeelox/followers","following_url":"https://api.github.com/users/goldeelox/following{/other_user}","gists_url":"https://api.github.com/users/goldeelox/gists{/gist_id}","starred_url":"https://api.github.com/users/goldeelox/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/goldeelox/subscriptions","organizations_url":"https://api.github.com/users/goldeelox/orgs","repos_url":"https://api.github.com/users/goldeelox/repos","events_url":"https://api.github.com/users/goldeelox/events{/privacy}","received_events_url":"https://api.github.com/users/goldeelox/received_events","type":"User","site_admin":false},"labels":[{"id":182505367,"node_id":"MDU6TGFiZWwxODI1MDUzNjc=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-IE","name":"D-IE","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-06-27T17:34:07Z","updated_at":"2019-08-14T21:09:42Z","closed_at":"2019-02-25T19:38:19Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows Server 2012 R2 \r\nSelenium Version:  3.13 (Java)\r\nBrowser:  Internet Explorer\r\nBrowser Version:  11.0.9600.18978\r\n\r\n## Expected Behavior -\r\nSpecifying a list of addresses to bypass the configured proxy with setNoProxy() should work with IEDriver, but it does not.\r\n\r\n## Actual Behavior -\r\nIEDriver does not acknowledge noProxy, but the ChromeDriver works as expected.\r\n\r\n## Steps to reproduce -\r\n```\r\nString myProxy = \"myproxy.example.com:80\";\r\nString myNoProxy = \".example.com, .xyz.com\";\r\n\r\nProxy proxy = new Proxy();\r\nproxy.setHttpProxy(myProxy);\r\nproxy.setSslProxy(myProxy);\r\nproxy.setNoProxy(myNoProxy);\r\n\r\nInternetExplorerOptions options = new InternetExplorerOptions();\r\noptions.usePerProcessProxy();\r\noptions.setProxy(proxy);\r\n\r\nWebDriver driver = new InternetExplorerDriver(options);\r\ndriver.get(\"https://example.com\");\r\ndriver.quit();\r\n```\r\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["427191150","467152386"], "labels":["D-IE"]}