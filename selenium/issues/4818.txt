{"id":"4818", "title":"[Facebook\WebDriver\Exception\SessionNotCreatedException] Unable to create new service: ChromeDriverService", "body":"## Meta -
OS:  MAC 
<!-- Windows 10? OSX? -->
Selenium Version:  3.6
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome, Firefox
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
Chrome Version - 61.0.3163.100
<!-- e.g.: 49.0.2623.87 (64-bit) -->
Firefox Version - 56.0 (64-bit)
## Expected Behavior -
It should open the browser and script should run
## Actual Behavior -
Script is not running.

Check the log here - 
Selenium Log -
```
11:52:26.038 INFO - Selenium build info: version: '3.6.0', revision: '6fbf3ec767'
11:52:26.038 INFO - Launching a standalone Selenium Server
2017-10-06 11:52:26.069:INFO::main: Logging initialized @333ms to org.seleniumhq.jetty9.util.log.StdErrLog
11:52:26.135 INFO - Driver class not found: com.opera.core.systems.OperaDriver
11:52:26.172 INFO - Driver provider class org.openqa.selenium.ie.InternetExplorerDriver registration is skipped:
 registration capabilities Capabilities [{ensureCleanSession=true, browserName=internet explorer, version=, platform=WINDOWS}] does not match the current platform MAC
11:52:26.172 INFO - Driver provider class org.openqa.selenium.edge.EdgeDriver registration is skipped:
 registration capabilities Capabilities [{browserName=MicrosoftEdge, version=, platform=WINDOWS}] does not match the current platform MAC
11:52:26.227 INFO - Using the passthrough mode handler
2017-10-06 11:52:26.270:INFO:osjs.Server:main: jetty-9.4.5.v20170502
2017-10-06 11:52:26.303:WARN:osjs.SecurityHandler:main: ServletContext@o.s.j.s.ServletContextHandler@eec5a4a{/,null,STARTING} has uncovered http methods for path: /
2017-10-06 11:52:26.310:INFO:osjsh.ContextHandler:main: Started o.s.j.s.ServletContextHandler@eec5a4a{/,null,AVAILABLE}
2017-10-06 11:52:26.354:INFO:osjs.AbstractConnector:main: Started ServerConnector@9660f4e{HTTP/1.1,[http/1.1]}{0.0.0.0:4444}
2017-10-06 11:52:26.356:INFO:osjs.Server:main: Started @620ms
11:52:26.356 INFO - Selenium Server is up and running
2017-10-06 11:52:38.203:INFO:osjshC.ROOT:qtp971848845-11: org.openqa.selenium.remote.server.WebDriverServlet-28d25987: Initialising WebDriverServlet
11:52:38.254 INFO - Binding default provider to: org.openqa.selenium.chrome.ChromeDriverService
11:52:38.255 INFO - Found handler: org.openqa.selenium.remote.server.commandhandler.BeginSession@124a951f
11:52:38.260 INFO - /session: Executing POST on /session (handler: BeginSession)
11:52:38.326 INFO - Capabilities are: Capabilities {browserName=chrome}
11:52:38.327 INFO - Capabilities {browserName=chrome} matched class org.openqa.selenium.remote.server.ServicedSession$Factory (provider: org.openqa.selenium.chrome.ChromeDriverService)
11:52:38.328 INFO - Capabilities {browserName=chrome} matched class org.openqa.selenium.remote.server.ServicedSession$Factory (provider: org.openqa.selenium.chrome.ChromeDriverService)
```
Script Log - 

```
Test  tests/acceptance/testCept.php
                                                                                                                                                                                                                                                                                                                                                                                           
  [Facebook\\WebDriver\\Exception\\SessionNotCreatedException] Unable to create new service: ChromeDriverService
Build info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T16:15:40.131Z'
System info: host: 'krupa.local', ip: '192.168.0.100', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.6', java.version: '1.8.0_111'
Driver info: driver.version: unknown  
```
## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
I am running my script for codeception test. Once I run the script it thorws the exception shown above. 

Please help me to fix this issue.", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4818","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4818/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4818/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4818/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4818","id":263347533,"node_id":"MDU6SXNzdWUyNjMzNDc1MzM=","number":4818,"title":"[Facebook\\WebDriver\\Exception\\SessionNotCreatedException] Unable to create new service: ChromeDriverService","user":{"login":"kjnanda","id":15173772,"node_id":"MDQ6VXNlcjE1MTczNzcy","avatar_url":"https://avatars3.githubusercontent.com/u/15173772?v=4","gravatar_id":"","url":"https://api.github.com/users/kjnanda","html_url":"https://github.com/kjnanda","followers_url":"https://api.github.com/users/kjnanda/followers","following_url":"https://api.github.com/users/kjnanda/following{/other_user}","gists_url":"https://api.github.com/users/kjnanda/gists{/gist_id}","starred_url":"https://api.github.com/users/kjnanda/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kjnanda/subscriptions","organizations_url":"https://api.github.com/users/kjnanda/orgs","repos_url":"https://api.github.com/users/kjnanda/repos","events_url":"https://api.github.com/users/kjnanda/events{/privacy}","received_events_url":"https://api.github.com/users/kjnanda/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":19,"created_at":"2017-10-06T06:29:00Z","updated_at":"2019-09-28T20:09:35Z","closed_at":"2017-10-09T06:59:47Z","author_association":"NONE","body":"## Meta -\r\nOS:  MAC \r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.6\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome, Firefox\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\nChrome Version - 61.0.3163.100\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\nFirefox Version - 56.0 (64-bit)\r\n## Expected Behavior -\r\nIt should open the browser and script should run\r\n## Actual Behavior -\r\nScript is not running.\r\n\r\nCheck the log here - \r\nSelenium Log -\r\n```\r\n11:52:26.038 INFO - Selenium build info: version: '3.6.0', revision: '6fbf3ec767'\r\n11:52:26.038 INFO - Launching a standalone Selenium Server\r\n2017-10-06 11:52:26.069:INFO::main: Logging initialized @333ms to org.seleniumhq.jetty9.util.log.StdErrLog\r\n11:52:26.135 INFO - Driver class not found: com.opera.core.systems.OperaDriver\r\n11:52:26.172 INFO - Driver provider class org.openqa.selenium.ie.InternetExplorerDriver registration is skipped:\r\n registration capabilities Capabilities [{ensureCleanSession=true, browserName=internet explorer, version=, platform=WINDOWS}] does not match the current platform MAC\r\n11:52:26.172 INFO - Driver provider class org.openqa.selenium.edge.EdgeDriver registration is skipped:\r\n registration capabilities Capabilities [{browserName=MicrosoftEdge, version=, platform=WINDOWS}] does not match the current platform MAC\r\n11:52:26.227 INFO - Using the passthrough mode handler\r\n2017-10-06 11:52:26.270:INFO:osjs.Server:main: jetty-9.4.5.v20170502\r\n2017-10-06 11:52:26.303:WARN:osjs.SecurityHandler:main: ServletContext@o.s.j.s.ServletContextHandler@eec5a4a{/,null,STARTING} has uncovered http methods for path: /\r\n2017-10-06 11:52:26.310:INFO:osjsh.ContextHandler:main: Started o.s.j.s.ServletContextHandler@eec5a4a{/,null,AVAILABLE}\r\n2017-10-06 11:52:26.354:INFO:osjs.AbstractConnector:main: Started ServerConnector@9660f4e{HTTP/1.1,[http/1.1]}{0.0.0.0:4444}\r\n2017-10-06 11:52:26.356:INFO:osjs.Server:main: Started @620ms\r\n11:52:26.356 INFO - Selenium Server is up and running\r\n2017-10-06 11:52:38.203:INFO:osjshC.ROOT:qtp971848845-11: org.openqa.selenium.remote.server.WebDriverServlet-28d25987: Initialising WebDriverServlet\r\n11:52:38.254 INFO - Binding default provider to: org.openqa.selenium.chrome.ChromeDriverService\r\n11:52:38.255 INFO - Found handler: org.openqa.selenium.remote.server.commandhandler.BeginSession@124a951f\r\n11:52:38.260 INFO - /session: Executing POST on /session (handler: BeginSession)\r\n11:52:38.326 INFO - Capabilities are: Capabilities {browserName=chrome}\r\n11:52:38.327 INFO - Capabilities {browserName=chrome} matched class org.openqa.selenium.remote.server.ServicedSession$Factory (provider: org.openqa.selenium.chrome.ChromeDriverService)\r\n11:52:38.328 INFO - Capabilities {browserName=chrome} matched class org.openqa.selenium.remote.server.ServicedSession$Factory (provider: org.openqa.selenium.chrome.ChromeDriverService)\r\n```\r\nScript Log - \r\n\r\n```\r\nTest  tests/acceptance/testCept.php\r\n                                                                                                                                                                                                                                                                                                                                                                                           \r\n  [Facebook\\WebDriver\\Exception\\SessionNotCreatedException] Unable to create new service: ChromeDriverService\r\nBuild info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T16:15:40.131Z'\r\nSystem info: host: 'krupa.local', ip: '192.168.0.100', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.6', java.version: '1.8.0_111'\r\nDriver info: driver.version: unknown  \r\n```\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\nI am running my script for codeception test. Once I run the script it thorws the exception shown above. \r\n\r\nPlease help me to fix this issue.","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["334681853","334682656","334683154","334707821","334709752","334710286","334711392","334712392","334714671","335078315","336186671","340933677","340954075","375087580","420970110","484820574","517818558","526339343","536221749"], "labels":["R-awaiting answer"]}