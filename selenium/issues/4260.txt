{"id":"4260", "title":"Radio button Always on not turn on /off , uninstalled plugin sevral times but not usefull ", "body":"## Meta -
OS:  Windows 10
Selenium Version:  2.9.1, IDE
Browser:  Firefox

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior - when i turn on then it should be like play/ stop test.  

## Actual Behavior - Radio button of record is not performing properly , click event is not working properly.

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4260","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4260/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4260/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4260/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4260","id":240433364,"node_id":"MDU6SXNzdWUyNDA0MzMzNjQ=","number":4260,"title":"Radio button Always on not turn on /off , uninstalled plugin sevral times but not usefull ","user":{"login":"akhileshsh026","id":2514351,"node_id":"MDQ6VXNlcjI1MTQzNTE=","avatar_url":"https://avatars1.githubusercontent.com/u/2514351?v=4","gravatar_id":"","url":"https://api.github.com/users/akhileshsh026","html_url":"https://github.com/akhileshsh026","followers_url":"https://api.github.com/users/akhileshsh026/followers","following_url":"https://api.github.com/users/akhileshsh026/following{/other_user}","gists_url":"https://api.github.com/users/akhileshsh026/gists{/gist_id}","starred_url":"https://api.github.com/users/akhileshsh026/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/akhileshsh026/subscriptions","organizations_url":"https://api.github.com/users/akhileshsh026/orgs","repos_url":"https://api.github.com/users/akhileshsh026/repos","events_url":"https://api.github.com/users/akhileshsh026/events{/privacy}","received_events_url":"https://api.github.com/users/akhileshsh026/received_events","type":"User","site_admin":false},"labels":[{"id":182506300,"node_id":"MDU6TGFiZWwxODI1MDYzMDA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-ide","name":"C-ide","color":"fef2c0","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-07-04T14:14:58Z","updated_at":"2019-08-18T03:09:49Z","closed_at":"2017-07-17T08:09:12Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\nSelenium Version:  2.9.1, IDE\r\nBrowser:  Firefox\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior - when i turn on then it should be like play/ stop test.  \r\n\r\n## Actual Behavior - Radio button of record is not performing properly , click event is not working properly.\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["315691103"], "labels":["C-ide"]}