{"id":"6135", "title":"Selenium(JAVA) - Handling notifications in mozilla firefox", "body":"Browser - Mozilla Firefox
Selenium Version - 3.12
Test NG - version 6.14.2
OS - Windows 10
Browser - Mozilla Firefox (59.02)

Problem - The website throws notification and navigation is blocked without user action.We want selenium to take the action when notification box opens up.
In the following code the method - openProfilePage() fails to execute because of selenium not taking action for the notification.

Code -
package testngDemo;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

public class TestNgDemo {

WebDriver driver;
  	  
  
@Test(dataProvider = \"dp\",priority=1)

public void login(String username, String password) {

	WebElement u_name = driver.findElement(By.id(\"email\"));
	u_name.sendKeys(username);
	
	WebElement p_word = driver.findElement(By.id(\"pass\"));
	p_word.sendKeys(password);
	
	driver.findElement(By.id(\"u_0_2\")).click();
		
}

@test (priority = 0)
public void openWebsite() {
System.setProperty(\"webdriver.gecko.driver\",\"C:\\\\Marionette\\\\geckodriver.exe\");
driver = new FirefoxDriver();
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
driver.get(\"https://www.facebook.com\");

}

@test(priority=2,dependsOnMethods=\"login\")
public void openProfilePage() {
driver.findElement(By.className(\"_1vp5\")).click();
}

@AfterMethod
public void afterMethod() {

}

@dataProvider
public Object[][] dp() {
Object[][] data = new Object[1][2];
data[0][0]=\"your_fb_username\";
data[0][1]=\"your_fb_password\";
return data;
}
}", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6135","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6135/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6135/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6135/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6135","id":339791681,"node_id":"MDU6SXNzdWUzMzk3OTE2ODE=","number":6135,"title":"Selenium(JAVA) - Handling notifications in mozilla firefox","user":{"login":"SudeeptMohan","id":32921253,"node_id":"MDQ6VXNlcjMyOTIxMjUz","avatar_url":"https://avatars1.githubusercontent.com/u/32921253?v=4","gravatar_id":"","url":"https://api.github.com/users/SudeeptMohan","html_url":"https://github.com/SudeeptMohan","followers_url":"https://api.github.com/users/SudeeptMohan/followers","following_url":"https://api.github.com/users/SudeeptMohan/following{/other_user}","gists_url":"https://api.github.com/users/SudeeptMohan/gists{/gist_id}","starred_url":"https://api.github.com/users/SudeeptMohan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SudeeptMohan/subscriptions","organizations_url":"https://api.github.com/users/SudeeptMohan/orgs","repos_url":"https://api.github.com/users/SudeeptMohan/repos","events_url":"https://api.github.com/users/SudeeptMohan/events{/privacy}","received_events_url":"https://api.github.com/users/SudeeptMohan/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-07-10T10:54:24Z","updated_at":"2019-08-14T07:09:52Z","closed_at":"2019-06-23T17:16:11Z","author_association":"NONE","body":"Browser - Mozilla Firefox\r\nSelenium Version - 3.12\r\nTest NG - version 6.14.2\r\nOS - Windows 10\r\nBrowser - Mozilla Firefox (59.02)\r\n\r\nProblem - The website throws notification and navigation is blocked without user action.We want selenium to take the action when notification box opens up.\r\nIn the following code the method - openProfilePage() fails to execute because of selenium not taking action for the notification.\r\n\r\nCode -\r\npackage testngDemo;\r\n\r\nimport org.testng.annotations.Test;\r\nimport org.testng.annotations.BeforeMethod;\r\nimport java.util.concurrent.TimeUnit;\r\nimport org.openqa.selenium.By;\r\nimport org.openqa.selenium.Capabilities;\r\nimport org.openqa.selenium.WebDriver;\r\nimport org.openqa.selenium.WebElement;\r\nimport org.openqa.selenium.firefox.FirefoxDriver;\r\nimport org.openqa.selenium.firefox.FirefoxProfile;\r\nimport org.openqa.selenium.firefox.internal.ProfilesIni;\r\nimport org.testng.annotations.AfterMethod;\r\nimport org.testng.annotations.DataProvider;\r\n\r\npublic class TestNgDemo {\r\n\r\nWebDriver driver;\r\n  \t  \r\n  \r\n@Test(dataProvider = \"dp\",priority=1)\r\n\r\npublic void login(String username, String password) {\r\n\r\n\tWebElement u_name = driver.findElement(By.id(\"email\"));\r\n\tu_name.sendKeys(username);\r\n\t\r\n\tWebElement p_word = driver.findElement(By.id(\"pass\"));\r\n\tp_word.sendKeys(password);\r\n\t\r\n\tdriver.findElement(By.id(\"u_0_2\")).click();\r\n\t\t\r\n}\r\n\r\n@test (priority = 0)\r\npublic void openWebsite() {\r\nSystem.setProperty(\"webdriver.gecko.driver\",\"C:\\\\Marionette\\\\geckodriver.exe\");\r\ndriver = new FirefoxDriver();\r\ndriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);\r\ndriver.get(\"https://www.facebook.com\");\r\n\r\n}\r\n\r\n@test(priority=2,dependsOnMethods=\"login\")\r\npublic void openProfilePage() {\r\ndriver.findElement(By.className(\"_1vp5\")).click();\r\n}\r\n\r\n@AfterMethod\r\npublic void afterMethod() {\r\n\r\n}\r\n\r\n@dataProvider\r\npublic Object[][] dp() {\r\nObject[][] data = new Object[1][2];\r\ndata[0][0]=\"your_fb_username\";\r\ndata[0][1]=\"your_fb_password\";\r\nreturn data;\r\n}\r\n}","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["484639734","504770525"], "labels":["D-firefox","R-awaiting answer"]}