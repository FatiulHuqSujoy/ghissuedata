{"id":"1351", "title":"keyDown() does not work with Internet Explorer Driver", "body":"I have tried to do it in different ways, but what works on Chrome (version 46.0.2490.86 m) and Firefox (version 42.0) won't work with IE 9.
Neither

``` javascript
browser.actions()
            .sendKeys(protractor.Key.chord(protractor.Key.CONTROL, '1'))
            .perform()
```

nor

``` javascript
browser.actions()
            .keyDown(protractor.Key.CONTROL)
            .sendKeys('1')
            .keyUp(protractor.Key.CONTROL)
            .perform();
```

work. I use protractor to send my tests to the selenium standalone server (which is on a VM which runs win7). Please tell me in case I overlooked something or if there is a workaround for this issue.

Full test:

``` javascript
describe('keyDown', function () {
    browser.ignoreSynchronization = true;
    browser.get('http://ci.seleniumhq.org:2310/common/keyboard_shortcut.html');

    it('should work with IE9', function () {
        browser.actions()
            .sendKeys(protractor.Key.chord(protractor.Key.CONTROL, '1'))
            .perform()

        expect(element(by.tagName('body')).getAttribute('style')).toContain('background: red');
    });
});
```

Thank you in advance,
Miro
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1351","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1351/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1351/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1351/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1351","id":120717990,"node_id":"MDU6SXNzdWUxMjA3MTc5OTA=","number":1351,"title":"keyDown() does not work with Internet Explorer Driver","user":{"login":"Miro-H","id":16030976,"node_id":"MDQ6VXNlcjE2MDMwOTc2","avatar_url":"https://avatars2.githubusercontent.com/u/16030976?v=4","gravatar_id":"","url":"https://api.github.com/users/Miro-H","html_url":"https://github.com/Miro-H","followers_url":"https://api.github.com/users/Miro-H/followers","following_url":"https://api.github.com/users/Miro-H/following{/other_user}","gists_url":"https://api.github.com/users/Miro-H/gists{/gist_id}","starred_url":"https://api.github.com/users/Miro-H/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Miro-H/subscriptions","organizations_url":"https://api.github.com/users/Miro-H/orgs","repos_url":"https://api.github.com/users/Miro-H/repos","events_url":"https://api.github.com/users/Miro-H/events{/privacy}","received_events_url":"https://api.github.com/users/Miro-H/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2015-12-07T07:49:13Z","updated_at":"2019-08-16T00:10:00Z","closed_at":"2015-12-07T19:21:33Z","author_association":"NONE","body":"I have tried to do it in different ways, but what works on Chrome (version 46.0.2490.86 m) and Firefox (version 42.0) won't work with IE 9.\nNeither\n\n``` javascript\nbrowser.actions()\n            .sendKeys(protractor.Key.chord(protractor.Key.CONTROL, '1'))\n            .perform()\n```\n\nnor\n\n``` javascript\nbrowser.actions()\n            .keyDown(protractor.Key.CONTROL)\n            .sendKeys('1')\n            .keyUp(protractor.Key.CONTROL)\n            .perform();\n```\n\nwork. I use protractor to send my tests to the selenium standalone server (which is on a VM which runs win7). Please tell me in case I overlooked something or if there is a workaround for this issue.\n\nFull test:\n\n``` javascript\ndescribe('keyDown', function () {\n    browser.ignoreSynchronization = true;\n    browser.get('http://ci.seleniumhq.org:2310/common/keyboard_shortcut.html');\n\n    it('should work with IE9', function () {\n        browser.actions()\n            .sendKeys(protractor.Key.chord(protractor.Key.CONTROL, '1'))\n            .perform()\n\n        expect(element(by.tagName('body')).getAttribute('style')).toContain('background: red');\n    });\n});\n```\n\nThank you in advance,\nMiro\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["162493834","162508363","162630585","163574765","391740750"], "labels":[]}