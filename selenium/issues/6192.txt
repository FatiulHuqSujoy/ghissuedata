{"id":"6192", "title":"IEDriverServer Crashing With Selenium 3.13 on Windows 7", "body":"## Meta -
OS:  Windows 7
<!-- Windows 10? OSX? -->
Selenium Version:  Selenium 3.13
<!-- 2.52.0, IDE, etc -->
Browser:  Internet Explorer

Browser Version:  11.0.9600.1908015 (64 Bit) and 10.0.9200.17609 (64 Bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

Driver : IEDriverServer 3.12; IEDriverServer 3.13

## Expected Behavior - 
Performing some steps on [this ](https://glip.com/hp_b5)url

## Actual Behavior -
While performing click IEDriver is getting crash.
Soetimes it is giving Element not clickable exception.

## Steps to reproduce -
Steps ::
1> open [This](https://glip.com/hp_b5)
2> Click on \"Product\"
3> Click on \"Facebook link Icon\" situated on the Bottom right corner on the page.
4> Exception occurs while clicking on the obove link and IEDriver crashing.


Please find below code to reproduce the problem::
```
DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(\"native events\", true);
		// capabilities.setCapability(\"requireWindowFocus\", true);
		capabilities.setCapability(\"ignoreProtectedModeSettings\", true);
		capabilities.setCapability(\"unexpectedAlertBehaviour\", \"ignore\");
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(\"handlesAlerts\", false);

		WebDriver driver = new InternetExplorerDriver(capabilities);

		driver.get(\"https://glip.com/hp_b5\");
		System.out.println(\"Title is as :: \" + driver.getTitle());

		// click on product
		driver.findElements(By.xpath(\"/html/body/div[2]/header/div/nav/ul/li[1]/a\")).get(0).click();
		if (args.length > 1)
			Thread.sleep(5000);

		// click on facebook 
               // Crashing At This Time
		// driver.findElements(By.xpath(\"/html/body/div[2]/footer/div/div[2]/ul/li[1]\")).get(0).click();
		driver.findElement(By.xpath(\"/html/body/div[2]/footer/div/div[2]/ul/li[1]\")).click();

		Thread.sleep(5000);

		// click on pricing
		List<WebElement> ele = driver.findElements(By.xpath(\"/html/body/div[2]/header/div/nav/ul/li[2]/a\"));
		System.out.println(ele.size());
		ele.get(0).click();

		// JavascriptExecutor jse = (JavascriptExecutor) driver;
		// jse.executeScript(\"arguments[0].click();\", ele.get(0));

		Thread.sleep(15000);
		driver.close();
		driver.quit();
```


**It is working fine with both driver on windows 10**
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6192","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6192/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6192/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6192/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6192","id":343581632,"node_id":"MDU6SXNzdWUzNDM1ODE2MzI=","number":6192,"title":"IEDriverServer Crashing With Selenium 3.13 on Windows 7","user":{"login":"TARIFABBAS","id":13521560,"node_id":"MDQ6VXNlcjEzNTIxNTYw","avatar_url":"https://avatars0.githubusercontent.com/u/13521560?v=4","gravatar_id":"","url":"https://api.github.com/users/TARIFABBAS","html_url":"https://github.com/TARIFABBAS","followers_url":"https://api.github.com/users/TARIFABBAS/followers","following_url":"https://api.github.com/users/TARIFABBAS/following{/other_user}","gists_url":"https://api.github.com/users/TARIFABBAS/gists{/gist_id}","starred_url":"https://api.github.com/users/TARIFABBAS/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TARIFABBAS/subscriptions","organizations_url":"https://api.github.com/users/TARIFABBAS/orgs","repos_url":"https://api.github.com/users/TARIFABBAS/repos","events_url":"https://api.github.com/users/TARIFABBAS/events{/privacy}","received_events_url":"https://api.github.com/users/TARIFABBAS/received_events","type":"User","site_admin":false},"labels":[{"id":182505367,"node_id":"MDU6TGFiZWwxODI1MDUzNjc=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-IE","name":"D-IE","color":"0052cc","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2018-07-23T10:47:08Z","updated_at":"2019-08-15T14:09:40Z","closed_at":"2018-08-26T18:43:47Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 7\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  Selenium 3.13\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Internet Explorer\r\n\r\nBrowser Version:  11.0.9600.1908015 (64 Bit) and 10.0.9200.17609 (64 Bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\nDriver : IEDriverServer 3.12; IEDriverServer 3.13\r\n\r\n## Expected Behavior - \r\nPerforming some steps on [this ](https://glip.com/hp_b5)url\r\n\r\n## Actual Behavior -\r\nWhile performing click IEDriver is getting crash.\r\nSoetimes it is giving Element not clickable exception.\r\n\r\n## Steps to reproduce -\r\nSteps ::\r\n1> open [This](https://glip.com/hp_b5)\r\n2> Click on \"Product\"\r\n3> Click on \"Facebook link Icon\" situated on the Bottom right corner on the page.\r\n4> Exception occurs while clicking on the obove link and IEDriver crashing.\r\n\r\n\r\nPlease find below code to reproduce the problem::\r\n```\r\nDesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();\r\n\t\tcapabilities.setCapability(\"native events\", true);\r\n\t\t// capabilities.setCapability(\"requireWindowFocus\", true);\r\n\t\tcapabilities.setCapability(\"ignoreProtectedModeSettings\", true);\r\n\t\tcapabilities.setCapability(\"unexpectedAlertBehaviour\", \"ignore\");\r\n\t\tcapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);\r\n\t\tcapabilities.setCapability(\"handlesAlerts\", false);\r\n\r\n\t\tWebDriver driver = new InternetExplorerDriver(capabilities);\r\n\r\n\t\tdriver.get(\"https://glip.com/hp_b5\");\r\n\t\tSystem.out.println(\"Title is as :: \" + driver.getTitle());\r\n\r\n\t\t// click on product\r\n\t\tdriver.findElements(By.xpath(\"/html/body/div[2]/header/div/nav/ul/li[1]/a\")).get(0).click();\r\n\t\tif (args.length > 1)\r\n\t\t\tThread.sleep(5000);\r\n\r\n\t\t// click on facebook \r\n               // Crashing At This Time\r\n\t\t// driver.findElements(By.xpath(\"/html/body/div[2]/footer/div/div[2]/ul/li[1]\")).get(0).click();\r\n\t\tdriver.findElement(By.xpath(\"/html/body/div[2]/footer/div/div[2]/ul/li[1]\")).click();\r\n\r\n\t\tThread.sleep(5000);\r\n\r\n\t\t// click on pricing\r\n\t\tList<WebElement> ele = driver.findElements(By.xpath(\"/html/body/div[2]/header/div/nav/ul/li[2]/a\"));\r\n\t\tSystem.out.println(ele.size());\r\n\t\tele.get(0).click();\r\n\r\n\t\t// JavascriptExecutor jse = (JavascriptExecutor) driver;\r\n\t\t// jse.executeScript(\"arguments[0].click();\", ele.get(0));\r\n\r\n\t\tThread.sleep(15000);\r\n\t\tdriver.close();\r\n\t\tdriver.quit();\r\n```\r\n\r\n\r\n**It is working fine with both driver on windows 10**\r\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["407218243","407285606","407360528","407681124","407702917","408517721","416059778"], "labels":["D-IE","R-awaiting answer"]}