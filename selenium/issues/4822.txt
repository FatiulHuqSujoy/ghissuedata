{"id":"4822", "title":"Rselenium in parallel: error when try to open page in a node", "body":"On following site: Run RSelenium in parallel, I have found out how to open multiply browsers (sessions) in parallel using parallel package:

```
 library(RSelenium)
library(rvest)
library(magrittr)
library(foreach)
library(doParallel)

# number of cores
(cl <- (detectCores() - 1) %>%  makeCluster) %>% registerDoParallel

# open a remoteDriver for each node on the cluster
# docker run -d -p 4445:4444 selenium/standalone-chrome:3.4.0
clusterEvalQ(cl, {
  library(RSelenium)
  remDr <- remoteDriver(remoteServerAddr = \"192.168.99.100\", port = 4445L,
                        browserName = \"chrome\")
  remDr$open()

})
```

But if I add only one additional line of code that open specific page:
```
clusterEvalQ(cl, {
  library(RSelenium)
  remDr <- remoteDriver(remoteServerAddr = \"192.168.99.100\", port = 4445L,
                        browserName = \"chrome\")
  remDr$open()
  Sys.sleep(3L)
  remDr$navigate(\"http://plovila.pomorstvo.hr/\")
})
```
it returns an error:
```
Error in checkForRemoteErrors(lapply(cl, recvResult)) : 
  6 nodes produced errors; first error:      Summary: UnknownError
     Detail: An unknown server-side error occurred while processing the command.
     class: org.openqa.selenium.WebDriverException
     Further Details: run errorDetails method
```
Why it returns an error with this additional line of code?

There is one more strange thing. It sometimes works at firs, but after if I try more times it returns an error all the time.

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4822","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4822/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4822/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4822/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4822","id":263545138,"node_id":"MDU6SXNzdWUyNjM1NDUxMzg=","number":4822,"title":"Rselenium in parallel: error when try to open page in a node","user":{"login":"mislav0207","id":15027982,"node_id":"MDQ6VXNlcjE1MDI3OTgy","avatar_url":"https://avatars0.githubusercontent.com/u/15027982?v=4","gravatar_id":"","url":"https://api.github.com/users/mislav0207","html_url":"https://github.com/mislav0207","followers_url":"https://api.github.com/users/mislav0207/followers","following_url":"https://api.github.com/users/mislav0207/following{/other_user}","gists_url":"https://api.github.com/users/mislav0207/gists{/gist_id}","starred_url":"https://api.github.com/users/mislav0207/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mislav0207/subscriptions","organizations_url":"https://api.github.com/users/mislav0207/orgs","repos_url":"https://api.github.com/users/mislav0207/repos","events_url":"https://api.github.com/users/mislav0207/events{/privacy}","received_events_url":"https://api.github.com/users/mislav0207/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2017-10-06T19:14:25Z","updated_at":"2019-08-17T05:09:39Z","closed_at":"2017-10-28T20:05:13Z","author_association":"NONE","body":"On following site: Run RSelenium in parallel, I have found out how to open multiply browsers (sessions) in parallel using parallel package:\r\n\r\n```\r\n library(RSelenium)\r\nlibrary(rvest)\r\nlibrary(magrittr)\r\nlibrary(foreach)\r\nlibrary(doParallel)\r\n\r\n# number of cores\r\n(cl <- (detectCores() - 1) %>%  makeCluster) %>% registerDoParallel\r\n\r\n# open a remoteDriver for each node on the cluster\r\n# docker run -d -p 4445:4444 selenium/standalone-chrome:3.4.0\r\nclusterEvalQ(cl, {\r\n  library(RSelenium)\r\n  remDr <- remoteDriver(remoteServerAddr = \"192.168.99.100\", port = 4445L,\r\n                        browserName = \"chrome\")\r\n  remDr$open()\r\n\r\n})\r\n```\r\n\r\nBut if I add only one additional line of code that open specific page:\r\n```\r\nclusterEvalQ(cl, {\r\n  library(RSelenium)\r\n  remDr <- remoteDriver(remoteServerAddr = \"192.168.99.100\", port = 4445L,\r\n                        browserName = \"chrome\")\r\n  remDr$open()\r\n  Sys.sleep(3L)\r\n  remDr$navigate(\"http://plovila.pomorstvo.hr/\")\r\n})\r\n```\r\nit returns an error:\r\n```\r\nError in checkForRemoteErrors(lapply(cl, recvResult)) : \r\n  6 nodes produced errors; first error:      Summary: UnknownError\r\n     Detail: An unknown server-side error occurred while processing the command.\r\n     class: org.openqa.selenium.WebDriverException\r\n     Further Details: run errorDetails method\r\n```\r\nWhy it returns an error with this additional line of code?\r\n\r\nThere is one more strange thing. It sometimes works at firs, but after if I try more times it returns an error all the time.\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["334909623","334921393","334922671","336243124","340216422"], "labels":["R-awaiting answer"]}