{"id":"4525", "title":"org.openqa.selenium.safari.SafariDriver#toString fails with org.openqa.selenium.WebDriverException", "body":"## Meta -
OS:  
MAC OS Sierra - 10.12.5 (16F73)

Selenium Version:  
3.5.1

Browser:  
Safari

Browser Version:  
 10.1.1

## Expected Behavior -
When one creates a new noargs SafariDriver and call toString() on it, the toString() should return a correct string representation of the driver.

## Actual Behavior -
When one creates a new noargs SafariDriver and calls toString() on it this fails with the exception 
```
org.openqa.selenium.WebDriverException: Unrecognized platform: macOS
Build info: version: '3.5.1', revision: '9c21bb67ef', time: '2017-08-17T15:26:08.955Z'
```

## Steps to reproduce -

```
import org.junit.Test;
import org.openqa.selenium.safari.SafariDriver;

public class SafariDriverTest {

    @Test
    public void testSafariDriverToString(){
        SafariDriver safariDriver = new SafariDriver();
        safariDriver.toString();
    }
}
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4525","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4525/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4525/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4525/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4525","id":251703422,"node_id":"MDU6SXNzdWUyNTE3MDM0MjI=","number":4525,"title":"org.openqa.selenium.safari.SafariDriver#toString fails with org.openqa.selenium.WebDriverException","user":{"login":"graysr89","id":1125980,"node_id":"MDQ6VXNlcjExMjU5ODA=","avatar_url":"https://avatars1.githubusercontent.com/u/1125980?v=4","gravatar_id":"","url":"https://api.github.com/users/graysr89","html_url":"https://github.com/graysr89","followers_url":"https://api.github.com/users/graysr89/followers","following_url":"https://api.github.com/users/graysr89/following{/other_user}","gists_url":"https://api.github.com/users/graysr89/gists{/gist_id}","starred_url":"https://api.github.com/users/graysr89/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/graysr89/subscriptions","organizations_url":"https://api.github.com/users/graysr89/orgs","repos_url":"https://api.github.com/users/graysr89/repos","events_url":"https://api.github.com/users/graysr89/events{/privacy}","received_events_url":"https://api.github.com/users/graysr89/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-08-21T16:00:22Z","updated_at":"2019-08-17T20:10:00Z","closed_at":"2017-08-22T06:54:13Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nMAC OS Sierra - 10.12.5 (16F73)\r\n\r\nSelenium Version:  \r\n3.5.1\r\n\r\nBrowser:  \r\nSafari\r\n\r\nBrowser Version:  \r\n 10.1.1\r\n\r\n## Expected Behavior -\r\nWhen one creates a new noargs SafariDriver and call toString() on it, the toString() should return a correct string representation of the driver.\r\n\r\n## Actual Behavior -\r\nWhen one creates a new noargs SafariDriver and calls toString() on it this fails with the exception \r\n```\r\norg.openqa.selenium.WebDriverException: Unrecognized platform: macOS\r\nBuild info: version: '3.5.1', revision: '9c21bb67ef', time: '2017-08-17T15:26:08.955Z'\r\n```\r\n\r\n## Steps to reproduce -\r\n\r\n```\r\nimport org.junit.Test;\r\nimport org.openqa.selenium.safari.SafariDriver;\r\n\r\npublic class SafariDriverTest {\r\n\r\n    @Test\r\n    public void testSafariDriverToString(){\r\n        SafariDriver safariDriver = new SafariDriver();\r\n        safariDriver.toString();\r\n    }\r\n}\r\n```\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["323810477","323936236"], "labels":[]}