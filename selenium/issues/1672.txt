{"id":"1672", "title":"IE browser doesn't close despite reporting that the session was deleted", "body":"## Expected Behavior -
1. Run the test
2. The test completes
3. The node reports:  
   `INFO - Executing: [delete session: 729e2...]`  
   `INFO - Done: [delete session: 729e2...]`
4. The browser closes
## Actual Behavior -
1. Run the test
2. The test completes
3. The node reports:  
   `INFO - Executing: [delete session: 729e2...]`  
   `INFO - Done: [delete session: 729e2...]`
4. The browser remains open
## Steps to reproduce -

I am running a Selenium grid. Here's my configuration:
- Hub
  - iMac9,1 - OS 10.11.3
  - selenium standalone server v2.52.0
    - started with `java -jar selenium-server-standalone-2.52.0.jar -role hub`
- Node
  - HP 11-d010nr - OS Windows 8.1
  - selenium standalone server v2.52.0
    - started with `java -jar selenium-server-standalone-2.52.0.jar -role node -hub http://the-imac.local:4444/grid/register -nodeConfig config.json`*
  - IEDriverServer (64 bit)
- Client
  - iMac7,1 - OS 10.10.5
  - JavaScript (Node) web driver installed via `npm install selenium-webdriver` (v2.52.0)

*Contents of config.json:

``` json
{
    \"capabilities\": [{
        \"browserName\": \"internet explorer\",
        \"platform\": \"WINDOWS\"
    }],
    \"configuration\": {}
}
```

The test is a simplified version of one of the included example tests:

``` javascript
var webdriver = require('selenium-webdriver'),

var driver = new webdriver.Builder()
    .forBrowser('internet explorer')
    .build();

driver.get('http://www.google.com/ncr');
driver.quit();
```

The same test run with `forBrowser('firefox')` behaves as expected.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1672","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1672/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1672/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1672/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1672","id":134943556,"node_id":"MDU6SXNzdWUxMzQ5NDM1NTY=","number":1672,"title":"IE browser doesn't close despite reporting that the session was deleted","user":{"login":"dustinpaluch","id":6547887,"node_id":"MDQ6VXNlcjY1NDc4ODc=","avatar_url":"https://avatars3.githubusercontent.com/u/6547887?v=4","gravatar_id":"","url":"https://api.github.com/users/dustinpaluch","html_url":"https://github.com/dustinpaluch","followers_url":"https://api.github.com/users/dustinpaluch/followers","following_url":"https://api.github.com/users/dustinpaluch/following{/other_user}","gists_url":"https://api.github.com/users/dustinpaluch/gists{/gist_id}","starred_url":"https://api.github.com/users/dustinpaluch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dustinpaluch/subscriptions","organizations_url":"https://api.github.com/users/dustinpaluch/orgs","repos_url":"https://api.github.com/users/dustinpaluch/repos","events_url":"https://api.github.com/users/dustinpaluch/events{/privacy}","received_events_url":"https://api.github.com/users/dustinpaluch/received_events","type":"User","site_admin":false},"labels":[{"id":182505367,"node_id":"MDU6TGFiZWwxODI1MDUzNjc=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-IE","name":"D-IE","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-02-19T18:42:22Z","updated_at":"2019-08-18T18:09:57Z","closed_at":"2017-03-19T00:25:59Z","author_association":"NONE","body":"## Expected Behavior -\n1. Run the test\n2. The test completes\n3. The node reports:  \n   `INFO - Executing: [delete session: 729e2...]`  \n   `INFO - Done: [delete session: 729e2...]`\n4. The browser closes\n## Actual Behavior -\n1. Run the test\n2. The test completes\n3. The node reports:  \n   `INFO - Executing: [delete session: 729e2...]`  \n   `INFO - Done: [delete session: 729e2...]`\n4. The browser remains open\n## Steps to reproduce -\n\nI am running a Selenium grid. Here's my configuration:\n- Hub\n  - iMac9,1 - OS 10.11.3\n  - selenium standalone server v2.52.0\n    - started with `java -jar selenium-server-standalone-2.52.0.jar -role hub`\n- Node\n  - HP 11-d010nr - OS Windows 8.1\n  - selenium standalone server v2.52.0\n    - started with `java -jar selenium-server-standalone-2.52.0.jar -role node -hub http://the-imac.local:4444/grid/register -nodeConfig config.json`*\n  - IEDriverServer (64 bit)\n- Client\n  - iMac7,1 - OS 10.10.5\n  - JavaScript (Node) web driver installed via `npm install selenium-webdriver` (v2.52.0)\n\n*Contents of config.json:\n\n``` json\n{\n    \"capabilities\": [{\n        \"browserName\": \"internet explorer\",\n        \"platform\": \"WINDOWS\"\n    }],\n    \"configuration\": {}\n}\n```\n\nThe test is a simplified version of one of the included example tests:\n\n``` javascript\nvar webdriver = require('selenium-webdriver'),\n\nvar driver = new webdriver.Builder()\n    .forBrowser('internet explorer')\n    .build();\n\ndriver.get('http://www.google.com/ncr');\ndriver.quit();\n```\n\nThe same test run with `forBrowser('firefox')` behaves as expected.\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["277464958"], "labels":["D-IE"]}