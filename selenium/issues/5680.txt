{"id":"5680", "title":"NodeJS: Support synchronous API", "body":"## Meta -
OS: Windows 10?
Selenium Version: 3.6.0

## Status Quo -

Currently [`selenium-webdriver`](https://www.npmjs.com/package/selenium-webdriver) works great with `async`/`await` but putting `await` at the beginning of each line can get a little annoying:

```js
const {Builder, By, Key, until} = require('selenium-webdriver');

(async function example() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('http://www.google.com/ncr');
    await driver.findElement(By.name('q'));.sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
  } finally {
    await driver.quit();
  }
})();
```

## Feature Request -

As many scripts don't actually need this asynchronous nature it would be great to provide a synchronous alternative.

One option would be to supply alternative methods by appending `Sync` to the end of each method name:

```js
const {Builder, By, Key, until} = require('selenium-webdriver');

let driver = new Builder().forBrowser('firefox').buildSync();
try {
  driver.getSync('http://www.google.com/ncr');
  driver.findElementSync(By.name('q'));.sendKeys('webdriver', Key.RETURN);
  driver.waitSync(until.titleIs('webdriver - Google Search'), 1000);
} finally {
  driver.quitSync();
}
```

This doesn't do much in the terms of savings. It is simply a trade-off between `await` and `Sync` but `Sync` is consistent with Node's `fs` module, etc. (e.g. `fs.readSync`).

Perhaps a better option would be to provide a synchronous API using a different module name. e.g. `selenium-webdriver/sync` instead of simply `selenium-webdriver`

```js
const {Builder, By, Key, until} = require('selenium-webdriver/sync');

let driver = new Builder().forBrowser('firefox').build();
try {
  driver.get('http://www.google.com/ncr');
  driver.findElement(By.name('q'));.sendKeys('webdriver', Key.RETURN);
  driver.wait(until.titleIs('webdriver - Google Search'), 1000);
} finally {
  driver.quit();
}
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5680","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5680/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5680/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5680/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5680","id":308612708,"node_id":"MDU6SXNzdWUzMDg2MTI3MDg=","number":5680,"title":"NodeJS: Support synchronous API","user":{"login":"mfulton26","id":5588957,"node_id":"MDQ6VXNlcjU1ODg5NTc=","avatar_url":"https://avatars0.githubusercontent.com/u/5588957?v=4","gravatar_id":"","url":"https://api.github.com/users/mfulton26","html_url":"https://github.com/mfulton26","followers_url":"https://api.github.com/users/mfulton26/followers","following_url":"https://api.github.com/users/mfulton26/following{/other_user}","gists_url":"https://api.github.com/users/mfulton26/gists{/gist_id}","starred_url":"https://api.github.com/users/mfulton26/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mfulton26/subscriptions","organizations_url":"https://api.github.com/users/mfulton26/orgs","repos_url":"https://api.github.com/users/mfulton26/repos","events_url":"https://api.github.com/users/mfulton26/events{/privacy}","received_events_url":"https://api.github.com/users/mfulton26/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-03-26T15:07:38Z","updated_at":"2019-08-16T08:09:58Z","closed_at":"2018-03-26T19:02:52Z","author_association":"NONE","body":"## Meta -\r\nOS: Windows 10?\r\nSelenium Version: 3.6.0\r\n\r\n## Status Quo -\r\n\r\nCurrently [`selenium-webdriver`](https://www.npmjs.com/package/selenium-webdriver) works great with `async`/`await` but putting `await` at the beginning of each line can get a little annoying:\r\n\r\n```js\r\nconst {Builder, By, Key, until} = require('selenium-webdriver');\r\n\r\n(async function example() {\r\n  let driver = await new Builder().forBrowser('firefox').build();\r\n  try {\r\n    await driver.get('http://www.google.com/ncr');\r\n    await driver.findElement(By.name('q'));.sendKeys('webdriver', Key.RETURN);\r\n    await driver.wait(until.titleIs('webdriver - Google Search'), 1000);\r\n  } finally {\r\n    await driver.quit();\r\n  }\r\n})();\r\n```\r\n\r\n## Feature Request -\r\n\r\nAs many scripts don't actually need this asynchronous nature it would be great to provide a synchronous alternative.\r\n\r\nOne option would be to supply alternative methods by appending `Sync` to the end of each method name:\r\n\r\n```js\r\nconst {Builder, By, Key, until} = require('selenium-webdriver');\r\n\r\nlet driver = new Builder().forBrowser('firefox').buildSync();\r\ntry {\r\n  driver.getSync('http://www.google.com/ncr');\r\n  driver.findElementSync(By.name('q'));.sendKeys('webdriver', Key.RETURN);\r\n  driver.waitSync(until.titleIs('webdriver - Google Search'), 1000);\r\n} finally {\r\n  driver.quitSync();\r\n}\r\n```\r\n\r\nThis doesn't do much in the terms of savings. It is simply a trade-off between `await` and `Sync` but `Sync` is consistent with Node's `fs` module, etc. (e.g. `fs.readSync`).\r\n\r\nPerhaps a better option would be to provide a synchronous API using a different module name. e.g. `selenium-webdriver/sync` instead of simply `selenium-webdriver`\r\n\r\n```js\r\nconst {Builder, By, Key, until} = require('selenium-webdriver/sync');\r\n\r\nlet driver = new Builder().forBrowser('firefox').build();\r\ntry {\r\n  driver.get('http://www.google.com/ncr');\r\n  driver.findElement(By.name('q'));.sendKeys('webdriver', Key.RETURN);\r\n  driver.wait(until.titleIs('webdriver - Google Search'), 1000);\r\n} finally {\r\n  driver.quit();\r\n}\r\n```","closed_by":{"login":"jleyba","id":254985,"node_id":"MDQ6VXNlcjI1NDk4NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/254985?v=4","gravatar_id":"","url":"https://api.github.com/users/jleyba","html_url":"https://github.com/jleyba","followers_url":"https://api.github.com/users/jleyba/followers","following_url":"https://api.github.com/users/jleyba/following{/other_user}","gists_url":"https://api.github.com/users/jleyba/gists{/gist_id}","starred_url":"https://api.github.com/users/jleyba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jleyba/subscriptions","organizations_url":"https://api.github.com/users/jleyba/orgs","repos_url":"https://api.github.com/users/jleyba/repos","events_url":"https://api.github.com/users/jleyba/events{/privacy}","received_events_url":"https://api.github.com/users/jleyba/received_events","type":"User","site_admin":false}}", "commentIds":["376276228","376285366","376301888"], "labels":["C-nodejs"]}