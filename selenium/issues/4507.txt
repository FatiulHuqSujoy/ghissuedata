{"id":"4507", "title":"Unable to execute Selenium (3.5.0) Grid when \"chrome_options=options\" is passed as an argument in \"self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\" method through Python PyDev (Eclipse) unittest module", "body":"## Meta - Unable to execute Selenium (3.5.0) Grid when \"chrome_options=options\" is passed as an argument in \"self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\" method through Python PyDev (Eclipse) unittest module

OS: Windows 8 Pro, 64 bit

Selenium Version: 3.5.0 (Python 3.6.1)

Selenium Binding: Python 3.6.1

GeckoDriver Version: 0.18.0

Browser: Firefox

Browser Version: Firefox 55.0.2 (64-bit)

## Expected Behavior -

As a result of #3884  and #685 while using Python 3.6.1  bindings we have been using the following configuration while initializing the `chromedriver`:

- Chrome:

        driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\\Utility\\BrowserDrivers\\chromedriver.exe')

While using unittest in Python 3.6.1 we used:

    self.driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\\Utility\\BrowserDrivers\\chromedriver.exe')

So when we use `self.driver = webdriver.Remote()` the following should work as:

    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)


## Actual Behavior -

But while trying to use `self.driver = webdriver.Remote()` as:

    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)

The following error is observed on the console:

	Finding files... done.
	Importing test modules ... done.

	======================================================================
	ERROR: test_search_in_python_org (readthedocs.SeleniumGridChrome.PythonOrgSearch)
	----------------------------------------------------------------------
	Traceback (most recent call last):
	  File \"C:\\Users\\AtechM_03\\LearnAutmation\\PythonProject\\readthedocs\\SeleniumGridChrome.py\", line 14, in setUp
	    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)
	TypeError: __init__() got an unexpected keyword argument 'chrome_options'

	----------------------------------------------------------------------
	Ran 1 test in 0.009s

	FAILED (errors=1)

## Steps to reproduce -
Minimal Code Block:

	import unittest
	from selenium import webdriver
	from selenium.webdriver.common.keys import Keys
	from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
	from selenium.webdriver.chrome.options import Options

	class PythonOrgSearch(unittest.TestCase):

	    def setUp(self):
		options = Options()
		options.add_argument(\"start-maximized\")
		options.add_argument(\"disable-infobars\")
		options.add_argument(\"--disable-extensions\")
		self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)

	    def test_search_in_python_org(self):
		driver = self.driver
		driver.get(\"http://www.python.org\")
		self.assertIn(\"Python\", driver.title)
		elem = driver.find_element_by_name(\"q\")
		elem.send_keys(\"pycon\")
		elem.send_keys(Keys.RETURN)
		assert \"No results found.\" not in driver.page_source


	    def tearDown(self):
		self.driver.quit()

	if __name__ == \"__main__\":
	    unittest.main()
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4507","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4507/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4507/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4507/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4507","id":251301336,"node_id":"MDU6SXNzdWUyNTEzMDEzMzY=","number":4507,"title":"Unable to execute Selenium (3.5.0) Grid when \"chrome_options=options\" is passed as an argument in \"self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\" method through Python PyDev (Eclipse) unittest module","user":{"login":"Debanjan-B","id":26503213,"node_id":"MDQ6VXNlcjI2NTAzMjEz","avatar_url":"https://avatars3.githubusercontent.com/u/26503213?v=4","gravatar_id":"","url":"https://api.github.com/users/Debanjan-B","html_url":"https://github.com/Debanjan-B","followers_url":"https://api.github.com/users/Debanjan-B/followers","following_url":"https://api.github.com/users/Debanjan-B/following{/other_user}","gists_url":"https://api.github.com/users/Debanjan-B/gists{/gist_id}","starred_url":"https://api.github.com/users/Debanjan-B/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Debanjan-B/subscriptions","organizations_url":"https://api.github.com/users/Debanjan-B/orgs","repos_url":"https://api.github.com/users/Debanjan-B/repos","events_url":"https://api.github.com/users/Debanjan-B/events{/privacy}","received_events_url":"https://api.github.com/users/Debanjan-B/received_events","type":"User","site_admin":false},"labels":[{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false},{"id":316341013,"node_id":"MDU6TGFiZWwzMTYzNDEwMTM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-chrome","name":"D-chrome","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2017-08-18T16:50:43Z","updated_at":"2019-08-15T09:10:00Z","closed_at":"2017-08-21T01:35:32Z","author_association":"NONE","body":"## Meta - Unable to execute Selenium (3.5.0) Grid when \"chrome_options=options\" is passed as an argument in \"self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\" method through Python PyDev (Eclipse) unittest module\r\n\r\nOS: Windows 8 Pro, 64 bit\r\n\r\nSelenium Version: 3.5.0 (Python 3.6.1)\r\n\r\nSelenium Binding: Python 3.6.1\r\n\r\nGeckoDriver Version: 0.18.0\r\n\r\nBrowser: Firefox\r\n\r\nBrowser Version: Firefox 55.0.2 (64-bit)\r\n\r\n## Expected Behavior -\r\n\r\nAs a result of #3884  and #685 while using Python 3.6.1  bindings we have been using the following configuration while initializing the `chromedriver`:\r\n\r\n- Chrome:\r\n\r\n        driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\\Utility\\BrowserDrivers\\chromedriver.exe')\r\n\r\nWhile using unittest in Python 3.6.1 we used:\r\n\r\n    self.driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\\Utility\\BrowserDrivers\\chromedriver.exe')\r\n\r\nSo when we use `self.driver = webdriver.Remote()` the following should work as:\r\n\r\n    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\r\n\r\n\r\n## Actual Behavior -\r\n\r\nBut while trying to use `self.driver = webdriver.Remote()` as:\r\n\r\n    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\r\n\r\nThe following error is observed on the console:\r\n\r\n\tFinding files... done.\r\n\tImporting test modules ... done.\r\n\r\n\t======================================================================\r\n\tERROR: test_search_in_python_org (readthedocs.SeleniumGridChrome.PythonOrgSearch)\r\n\t----------------------------------------------------------------------\r\n\tTraceback (most recent call last):\r\n\t  File \"C:\\Users\\AtechM_03\\LearnAutmation\\PythonProject\\readthedocs\\SeleniumGridChrome.py\", line 14, in setUp\r\n\t    self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\r\n\tTypeError: __init__() got an unexpected keyword argument 'chrome_options'\r\n\r\n\t----------------------------------------------------------------------\r\n\tRan 1 test in 0.009s\r\n\r\n\tFAILED (errors=1)\r\n\r\n## Steps to reproduce -\r\nMinimal Code Block:\r\n\r\n\timport unittest\r\n\tfrom selenium import webdriver\r\n\tfrom selenium.webdriver.common.keys import Keys\r\n\tfrom selenium.webdriver.common.desired_capabilities import DesiredCapabilities\r\n\tfrom selenium.webdriver.chrome.options import Options\r\n\r\n\tclass PythonOrgSearch(unittest.TestCase):\r\n\r\n\t    def setUp(self):\r\n\t\toptions = Options()\r\n\t\toptions.add_argument(\"start-maximized\")\r\n\t\toptions.add_argument(\"disable-infobars\")\r\n\t\toptions.add_argument(\"--disable-extensions\")\r\n\t\tself.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', chrome_options=options, desired_capabilities=DesiredCapabilities.CHROME)\r\n\r\n\t    def test_search_in_python_org(self):\r\n\t\tdriver = self.driver\r\n\t\tdriver.get(\"http://www.python.org\")\r\n\t\tself.assertIn(\"Python\", driver.title)\r\n\t\telem = driver.find_element_by_name(\"q\")\r\n\t\telem.send_keys(\"pycon\")\r\n\t\telem.send_keys(Keys.RETURN)\r\n\t\tassert \"No results found.\" not in driver.page_source\r\n\r\n\r\n\t    def tearDown(self):\r\n\t\tself.driver.quit()\r\n\r\n\tif __name__ == \"__main__\":\r\n\t    unittest.main()\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["323627046","323919758","403617334","424818592"], "labels":["C-py","D-chrome"]}