{"id":"6042", "title":"fullScreen with firefox", "body":"## Meta -
OS:  Raspberry pi 3 B+ armv7l, Raspbian ( Debian) 9 stretch

Selenium Version:  
3.12.0

Browser:  
Firefox-ESR with geckodriver v0.17.0

Browser Version:  
Mozilla Firefox 52.8.1

## Expected Behavior -
Open firefox in full screen
## Actual Behavior -
Firefox is opened but not in full screen

error log : 

```
Traceback (most recent call last):
  File \"fi.py\", line 4, in <module>
    browser.fullscreen_window()
  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/webdriver.py\", line 734, in fullscreen_window
    self.execute(Command.FULLSCREEN_WINDOW)
  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/webdriver.py\", line 314, in execute
    self.error_handler.check_response(response)
  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.WebDriverException: Message: fullscreenWindow
```


## Steps to reproduce -
With Python 3.5 and Python 2.7
http://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.fullscreen_window

```
from selenium import webdriver
browser = webdriver.Firefox()
browser.get('http://google.com')
browser.fullscreen_window()
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6042","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6042/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6042/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6042/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6042","id":333662437,"node_id":"MDU6SXNzdWUzMzM2NjI0Mzc=","number":6042,"title":"fullScreen with firefox","user":{"login":"well-oo","id":38964651,"node_id":"MDQ6VXNlcjM4OTY0NjUx","avatar_url":"https://avatars2.githubusercontent.com/u/38964651?v=4","gravatar_id":"","url":"https://api.github.com/users/well-oo","html_url":"https://github.com/well-oo","followers_url":"https://api.github.com/users/well-oo/followers","following_url":"https://api.github.com/users/well-oo/following{/other_user}","gists_url":"https://api.github.com/users/well-oo/gists{/gist_id}","starred_url":"https://api.github.com/users/well-oo/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/well-oo/subscriptions","organizations_url":"https://api.github.com/users/well-oo/orgs","repos_url":"https://api.github.com/users/well-oo/repos","events_url":"https://api.github.com/users/well-oo/events{/privacy}","received_events_url":"https://api.github.com/users/well-oo/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2018-06-19T13:04:51Z","updated_at":"2019-08-14T06:09:53Z","closed_at":"2018-06-19T18:17:38Z","author_association":"NONE","body":"## Meta -\r\nOS:  Raspberry pi 3 B+ armv7l, Raspbian ( Debian) 9 stretch\r\n\r\nSelenium Version:  \r\n3.12.0\r\n\r\nBrowser:  \r\nFirefox-ESR with geckodriver v0.17.0\r\n\r\nBrowser Version:  \r\nMozilla Firefox 52.8.1\r\n\r\n## Expected Behavior -\r\nOpen firefox in full screen\r\n## Actual Behavior -\r\nFirefox is opened but not in full screen\r\n\r\nerror log : \r\n\r\n```\r\nTraceback (most recent call last):\r\n  File \"fi.py\", line 4, in <module>\r\n    browser.fullscreen_window()\r\n  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/webdriver.py\", line 734, in fullscreen_window\r\n    self.execute(Command.FULLSCREEN_WINDOW)\r\n  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/webdriver.py\", line 314, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"/home/pi/.local/lib/python3.5/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.WebDriverException: Message: fullscreenWindow\r\n```\r\n\r\n\r\n## Steps to reproduce -\r\nWith Python 3.5 and Python 2.7\r\nhttp://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.fullscreen_window\r\n\r\n```\r\nfrom selenium import webdriver\r\nbrowser = webdriver.Firefox()\r\nbrowser.get('http://google.com')\r\nbrowser.fullscreen_window()\r\n```","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["398495807","398642810","398719013","508986962"], "labels":[]}