{"id":"6267", "title":"Devexpress SpinEdit", "body":"## Meta -
OS:  Windows 10
Selenium Version:  3.1.0, Chrome IDE
Browser:  Chrome

Browser Version:  Version 68.0.3440.106 (Official Build) (64-bit)

## Expected Behavior -
Go to \"https://demos.devexpress.com/aspxeditorsdemos/ASPxSpinEdit/Features.aspx\"
Do a Selenium Chrome IDE Test on the First and third Spin Editors
Start Recording:
Command \"Type\"
Target \"id=ContentHolder_ASPxSpinEdit1_I\" //This is the First Spin Editor
or
Target \"id=ContentHolder_ASPxSpinEdit3_I\" //This is the Third Spin Editor
Value \"11\"
Finish Recording
Run the Tests:
Expected Behavior is that the test fills the Spin Editors

## Actual Behavior -
Same as above 
But the Run the Tests:
Actual Behavior is that the test leaves the Spin Editors empty


## Steps to reproduce -
Follow the Expected Behavior Steps

Thanks and Best Regards,
Ivan
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6267","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6267/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6267/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6267/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6267","id":349413509,"node_id":"MDU6SXNzdWUzNDk0MTM1MDk=","number":6267,"title":"Devexpress SpinEdit","user":{"login":"ivanb000","id":42265237,"node_id":"MDQ6VXNlcjQyMjY1MjM3","avatar_url":"https://avatars2.githubusercontent.com/u/42265237?v=4","gravatar_id":"","url":"https://api.github.com/users/ivanb000","html_url":"https://github.com/ivanb000","followers_url":"https://api.github.com/users/ivanb000/followers","following_url":"https://api.github.com/users/ivanb000/following{/other_user}","gists_url":"https://api.github.com/users/ivanb000/gists{/gist_id}","starred_url":"https://api.github.com/users/ivanb000/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ivanb000/subscriptions","organizations_url":"https://api.github.com/users/ivanb000/orgs","repos_url":"https://api.github.com/users/ivanb000/repos","events_url":"https://api.github.com/users/ivanb000/events{/privacy}","received_events_url":"https://api.github.com/users/ivanb000/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-08-10T07:58:11Z","updated_at":"2019-08-15T16:09:41Z","closed_at":"2018-08-11T12:50:00Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\nSelenium Version:  3.1.0, Chrome IDE\r\nBrowser:  Chrome\r\n\r\nBrowser Version:  Version 68.0.3440.106 (Official Build) (64-bit)\r\n\r\n## Expected Behavior -\r\nGo to \"https://demos.devexpress.com/aspxeditorsdemos/ASPxSpinEdit/Features.aspx\"\r\nDo a Selenium Chrome IDE Test on the First and third Spin Editors\r\nStart Recording:\r\nCommand \"Type\"\r\nTarget \"id=ContentHolder_ASPxSpinEdit1_I\" //This is the First Spin Editor\r\nor\r\nTarget \"id=ContentHolder_ASPxSpinEdit3_I\" //This is the Third Spin Editor\r\nValue \"11\"\r\nFinish Recording\r\nRun the Tests:\r\nExpected Behavior is that the test fills the Spin Editors\r\n\r\n## Actual Behavior -\r\nSame as above \r\nBut the Run the Tests:\r\nActual Behavior is that the test leaves the Spin Editors empty\r\n\r\n\r\n## Steps to reproduce -\r\nFollow the Expected Behavior Steps\r\n\r\nThanks and Best Regards,\r\nIvan\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["412273061"], "labels":[]}