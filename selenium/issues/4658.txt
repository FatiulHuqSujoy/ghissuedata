{"id":"4658", "title":"Selenium IDE issue", "body":"## Meta -
OS:  
<!-- Windows 10? OSX? -->
Selenium Version:  
<!-- 2.52.0, IDE, etc -->
Browser:  
<!-- Internet Explorer?  Firefox?
Error 
There was an unexpected error. Msg: TypeError: window.editor.infoPanel is undefined
Url: chrome://selenium-ide/content/selenium-ide.xul, line: 1, column: 1
onclick@chrome://selenium-ide/content/selenium-ide.xul:1:1
any one could help me for this error issue




Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -

## Actual Behavior -

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4658","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4658/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4658/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4658/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4658","id":256047361,"node_id":"MDU6SXNzdWUyNTYwNDczNjE=","number":4658,"title":"Selenium IDE issue","user":{"login":"howardselenium","id":31744726,"node_id":"MDQ6VXNlcjMxNzQ0NzI2","avatar_url":"https://avatars2.githubusercontent.com/u/31744726?v=4","gravatar_id":"","url":"https://api.github.com/users/howardselenium","html_url":"https://github.com/howardselenium","followers_url":"https://api.github.com/users/howardselenium/followers","following_url":"https://api.github.com/users/howardselenium/following{/other_user}","gists_url":"https://api.github.com/users/howardselenium/gists{/gist_id}","starred_url":"https://api.github.com/users/howardselenium/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/howardselenium/subscriptions","organizations_url":"https://api.github.com/users/howardselenium/orgs","repos_url":"https://api.github.com/users/howardselenium/repos","events_url":"https://api.github.com/users/howardselenium/events{/privacy}","received_events_url":"https://api.github.com/users/howardselenium/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-09-07T19:39:12Z","updated_at":"2019-08-17T13:09:49Z","closed_at":"2017-09-07T20:39:46Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  \r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  \r\n<!-- Internet Explorer?  Firefox?\r\nError \r\nThere was an unexpected error. Msg: TypeError: window.editor.infoPanel is undefined\r\nUrl: chrome://selenium-ide/content/selenium-ide.xul, line: 1, column: 1\r\nonclick@chrome://selenium-ide/content/selenium-ide.xul:1:1\r\nany one could help me for this error issue\r\n\r\n\r\n\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\n\r\n## Actual Behavior -\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"ddavison","id":2972876,"node_id":"MDQ6VXNlcjI5NzI4NzY=","avatar_url":"https://avatars3.githubusercontent.com/u/2972876?v=4","gravatar_id":"","url":"https://api.github.com/users/ddavison","html_url":"https://github.com/ddavison","followers_url":"https://api.github.com/users/ddavison/followers","following_url":"https://api.github.com/users/ddavison/following{/other_user}","gists_url":"https://api.github.com/users/ddavison/gists{/gist_id}","starred_url":"https://api.github.com/users/ddavison/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ddavison/subscriptions","organizations_url":"https://api.github.com/users/ddavison/orgs","repos_url":"https://api.github.com/users/ddavison/repos","events_url":"https://api.github.com/users/ddavison/events{/privacy}","received_events_url":"https://api.github.com/users/ddavison/received_events","type":"User","site_admin":false}}", "commentIds":["327902624","327921368"], "labels":[]}