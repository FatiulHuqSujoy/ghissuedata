{"id":"4139", "title":"StaleElementReferenceException after update to newer version", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
3.1.0+ (.NET)
Browser:  
ChromeDriver
Browser Version:  
58.0.3029.110

## Actual Behavior -
Code is working in version 3.0.1 but updating to any newer version will cause StaleElementReference on Perform(). 
**Code producing error:** 
```// Element locating        
// [FindsBy(How = How.ClassName, Using = \"dot\")]
// private IList<IWebElement> gameDots { get; set; }        

for (int i = 0; i < levelIndicators.Count; i++)
{
    Browser.Action.ClickAndHold(gameDots[0]);
    foreach (IWebElement gameDot in gameDots)
    {
       Browser.Action.MoveToElement(gameDot);
    }
    Browser.Action.MoveToElement(gameDots[0])
       .Build()
       .Perform();
}
```
**Browser class:**
```public static Actions Action { get; set; }
public static IWebDriver Driver { get; set; }

static Browser()
{
   Action = new Actions(Driver);  
}
```
**Thrown error:**

>OpenQA.Selenium.StaleElementReferenceException : stale element reference: element is not attached to the page document (Session info: chrome=58.0.3029.110) (Driver info: chromedriver=2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform=Windows NT 10.0.15063 x86_64)

[Stackoverflow question](https://stackoverflow.com/questions/44392879/staleelementexception-after-selenium-update)

## Expected Behavior -
Actions functionality will be consistent between versions.

## Steps to reproduce -
1. Go to http://play-dot-to.com/
2. Start game
3. Use provided code to finish level
4. StaleElementReference is thrown", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4139","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4139/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4139/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4139/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4139","id":234130158,"node_id":"MDU6SXNzdWUyMzQxMzAxNTg=","number":4139,"title":"StaleElementReferenceException after update to newer version","user":{"login":"nekiscz","id":24873745,"node_id":"MDQ6VXNlcjI0ODczNzQ1","avatar_url":"https://avatars3.githubusercontent.com/u/24873745?v=4","gravatar_id":"","url":"https://api.github.com/users/nekiscz","html_url":"https://github.com/nekiscz","followers_url":"https://api.github.com/users/nekiscz/followers","following_url":"https://api.github.com/users/nekiscz/following{/other_user}","gists_url":"https://api.github.com/users/nekiscz/gists{/gist_id}","starred_url":"https://api.github.com/users/nekiscz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/nekiscz/subscriptions","organizations_url":"https://api.github.com/users/nekiscz/orgs","repos_url":"https://api.github.com/users/nekiscz/repos","events_url":"https://api.github.com/users/nekiscz/events{/privacy}","received_events_url":"https://api.github.com/users/nekiscz/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-06-07T08:26:00Z","updated_at":"2019-08-17T06:09:39Z","closed_at":"2017-06-07T09:52:12Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n3.1.0+ (.NET)\r\nBrowser:  \r\nChromeDriver\r\nBrowser Version:  \r\n58.0.3029.110\r\n\r\n## Actual Behavior -\r\nCode is working in version 3.0.1 but updating to any newer version will cause StaleElementReference on Perform(). \r\n**Code producing error:** \r\n```// Element locating        \r\n// [FindsBy(How = How.ClassName, Using = \"dot\")]\r\n// private IList<IWebElement> gameDots { get; set; }        \r\n\r\nfor (int i = 0; i < levelIndicators.Count; i++)\r\n{\r\n    Browser.Action.ClickAndHold(gameDots[0]);\r\n    foreach (IWebElement gameDot in gameDots)\r\n    {\r\n       Browser.Action.MoveToElement(gameDot);\r\n    }\r\n    Browser.Action.MoveToElement(gameDots[0])\r\n       .Build()\r\n       .Perform();\r\n}\r\n```\r\n**Browser class:**\r\n```public static Actions Action { get; set; }\r\npublic static IWebDriver Driver { get; set; }\r\n\r\nstatic Browser()\r\n{\r\n   Action = new Actions(Driver);  \r\n}\r\n```\r\n**Thrown error:**\r\n\r\n>OpenQA.Selenium.StaleElementReferenceException : stale element reference: element is not attached to the page document (Session info: chrome=58.0.3029.110) (Driver info: chromedriver=2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform=Windows NT 10.0.15063 x86_64)\r\n\r\n[Stackoverflow question](https://stackoverflow.com/questions/44392879/staleelementexception-after-selenium-update)\r\n\r\n## Expected Behavior -\r\nActions functionality will be consistent between versions.\r\n\r\n## Steps to reproduce -\r\n1. Go to http://play-dot-to.com/\r\n2. Start game\r\n3. Use provided code to finish level\r\n4. StaleElementReference is thrown","closed_by":{"login":"AutomatedTester","id":128518,"node_id":"MDQ6VXNlcjEyODUxOA==","avatar_url":"https://avatars1.githubusercontent.com/u/128518?v=4","gravatar_id":"","url":"https://api.github.com/users/AutomatedTester","html_url":"https://github.com/AutomatedTester","followers_url":"https://api.github.com/users/AutomatedTester/followers","following_url":"https://api.github.com/users/AutomatedTester/following{/other_user}","gists_url":"https://api.github.com/users/AutomatedTester/gists{/gist_id}","starred_url":"https://api.github.com/users/AutomatedTester/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/AutomatedTester/subscriptions","organizations_url":"https://api.github.com/users/AutomatedTester/orgs","repos_url":"https://api.github.com/users/AutomatedTester/repos","events_url":"https://api.github.com/users/AutomatedTester/events{/privacy}","received_events_url":"https://api.github.com/users/AutomatedTester/received_events","type":"User","site_admin":false}}", "commentIds":["306747369","320933518","338973088"], "labels":[]}