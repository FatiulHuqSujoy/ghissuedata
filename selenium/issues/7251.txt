{"id":"7251", "title":"Documentation on By.js is Incorrect", "body":"## 🐛 Bug Report

In Javascript an expression (eg. `1 + 1`) and a statement (eg. `const a = 1 + 1;` or `return a;`) are different things, but the documentation for By.js refers to it requiring an \"expression\" when it actually requires a statement (specifically a `return` statement).

## To Reproduce

1. Go to https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/by_exports_By.html#By.js
2. Try to follow the instructions: provide a \"a JavaScript expression\"
3. Receive a (difficult to debug) `TypeError: Custom locator did not return a WebElement`
4.  Change the \"Javascript expression\" to instead be the *statement* of: `return *whatever expression you had before*`
5. Get your expected element

## Expected behavior

The documentation should be changed from:

>Locates an elements by evaluating a JavaScript expression. The result of this expression must be an element or list of elements.

to:

>Locates elements by evaluating a JavaScript return statement. The value returned from this statement must be an element or list of elements.

>Example:
>driver.findElement({ js: \"return document.querySelectorAll('button')\" }))

Arguably a much better solution would be to make this function *actually* expect an expression.  There's really no reason to make devs provide:

    return document.querySelector('foo')`

when they could just provide:

    `document.querySelector('foo')`

... but I'm guessing it's easier to avoid breaking things by just fixing the documentation :)

OS: Any
Browser:Any
Browser version: Any
Browser Driver version: Current (documentation for version at https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/by_exports_By.html#By.js)
Language Bindings version: Javascript
Selenium Grid version (if applicable): n/a
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7251","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7251/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7251/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7251/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7251","id":450373438,"node_id":"MDU6SXNzdWU0NTAzNzM0Mzg=","number":7251,"title":"Documentation on By.js is Incorrect","user":{"login":"machineghost","id":448908,"node_id":"MDQ6VXNlcjQ0ODkwOA==","avatar_url":"https://avatars2.githubusercontent.com/u/448908?v=4","gravatar_id":"","url":"https://api.github.com/users/machineghost","html_url":"https://github.com/machineghost","followers_url":"https://api.github.com/users/machineghost/followers","following_url":"https://api.github.com/users/machineghost/following{/other_user}","gists_url":"https://api.github.com/users/machineghost/gists{/gist_id}","starred_url":"https://api.github.com/users/machineghost/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/machineghost/subscriptions","organizations_url":"https://api.github.com/users/machineghost/orgs","repos_url":"https://api.github.com/users/machineghost/repos","events_url":"https://api.github.com/users/machineghost/events{/privacy}","received_events_url":"https://api.github.com/users/machineghost/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-05-30T15:45:44Z","updated_at":"2019-08-14T09:09:36Z","closed_at":"2019-06-05T16:54:24Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nIn Javascript an expression (eg. `1 + 1`) and a statement (eg. `const a = 1 + 1;` or `return a;`) are different things, but the documentation for By.js refers to it requiring an \"expression\" when it actually requires a statement (specifically a `return` statement).\r\n\r\n## To Reproduce\r\n\r\n1. Go to https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/by_exports_By.html#By.js\r\n2. Try to follow the instructions: provide a \"a JavaScript expression\"\r\n3. Receive a (difficult to debug) `TypeError: Custom locator did not return a WebElement`\r\n4.  Change the \"Javascript expression\" to instead be the *statement* of: `return *whatever expression you had before*`\r\n5. Get your expected element\r\n\r\n## Expected behavior\r\n\r\nThe documentation should be changed from:\r\n\r\n>Locates an elements by evaluating a JavaScript expression. The result of this expression must be an element or list of elements.\r\n\r\nto:\r\n\r\n>Locates elements by evaluating a JavaScript return statement. The value returned from this statement must be an element or list of elements.\r\n\r\n>Example:\r\n>driver.findElement({ js: \"return document.querySelectorAll('button')\" }))\r\n\r\nArguably a much better solution would be to make this function *actually* expect an expression.  There's really no reason to make devs provide:\r\n\r\n    return document.querySelector('foo')`\r\n\r\nwhen they could just provide:\r\n\r\n    `document.querySelector('foo')`\r\n\r\n... but I'm guessing it's easier to avoid breaking things by just fixing the documentation :)\r\n\r\nOS: Any\r\nBrowser:Any\r\nBrowser version: Any\r\nBrowser Driver version: Current (documentation for version at https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/by_exports_By.html#By.js)\r\nLanguage Bindings version: Javascript\r\nSelenium Grid version (if applicable): n/a\r\n","closed_by":{"login":"jleyba","id":254985,"node_id":"MDQ6VXNlcjI1NDk4NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/254985?v=4","gravatar_id":"","url":"https://api.github.com/users/jleyba","html_url":"https://github.com/jleyba","followers_url":"https://api.github.com/users/jleyba/followers","following_url":"https://api.github.com/users/jleyba/following{/other_user}","gists_url":"https://api.github.com/users/jleyba/gists{/gist_id}","starred_url":"https://api.github.com/users/jleyba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jleyba/subscriptions","organizations_url":"https://api.github.com/users/jleyba/orgs","repos_url":"https://api.github.com/users/jleyba/repos","events_url":"https://api.github.com/users/jleyba/events{/privacy}","received_events_url":"https://api.github.com/users/jleyba/received_events","type":"User","site_admin":false}}", "commentIds":["500468144"], "labels":[]}