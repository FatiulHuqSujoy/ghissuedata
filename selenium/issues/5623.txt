{"id":"5623", "title":"Selenium Java program not sending search text to search box in Google", "body":"Browser Version:  
65

## Expected Behavior -
The search string should be passed by java program to google search text, chrome browser
## Actual Behavior -

Code used :
System.out.println(\"performing search in google\");
	if (textBox.isEnabled())
			System.out.println(\"search box enabled\");
		//send search-text and opening search result
		textBox.click();
		System.out.println(\"clicked\");
		textBox.clear();
		System.out.println(\"cleared\");
		textBox.sendKeys(\"Linux fast pc\");
		System.out.println(\"sent search string in google\");


Error Observed:
The search string is not entered in the search field.Infact it seems the click (first task of inputting in the program) is not working.

Exception in thread \"main\" org.openqa.selenium.WebDriverException: unknown error: call function result missing 'value'
  (Session info: chrome=65.0.3325.162)
  (Driver info: chromedriver=2.33.506120 (e3e53437346286c0bc2d2dc9aa4915ba81d9023f),platform=Windows NT 10.0.16299 x86) (WARNING: The server did not provide any stacktrace information)
Command duration or timeout: 0 milliseconds
Build info: version: '3.9.1', revision: '63f7b50', time: '2018-02-07T22:42:22.379Z'
System info: host: 'ADMIN-PC', ip: '192.168.1.5', os.name: 'Windows 10', os.arch: 'x86', os.version: '10.0', java.version: '1.8.0_161'
Driver info: org.openqa.selenium.chrome.ChromeDriver
Capabilities {acceptSslCerts: true, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.33.506120 (e3e53437346286..., userDataDir: C:\\Users\\admin\\AppData\\Loca...}, cssSelectorsEnabled: true, databaseEnabled: false, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 65.0.3325.162, webStorageEnabled: true}
Session ID: 2dd7e11d7fc40b7faabf475b1e145ee8
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)
	at java.lang.reflect.Constructor.newInstance(Unknown Source)
	at org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)
	at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)
	at org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)
	at org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)
	at org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)
	at org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:160)
	at org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)
	at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)
	at org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)
	at org.openqa.selenium.remote.RemoteWebElement.clear(RemoteWebElement.java:118)
	at browserAutomation.main(browserAutomation.java:94)

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5623","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5623/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5623/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5623/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5623","id":305610503,"node_id":"MDU6SXNzdWUzMDU2MTA1MDM=","number":5623,"title":"Selenium Java program not sending search text to search box in Google","user":{"login":"SudeeptMohan","id":32921253,"node_id":"MDQ6VXNlcjMyOTIxMjUz","avatar_url":"https://avatars1.githubusercontent.com/u/32921253?v=4","gravatar_id":"","url":"https://api.github.com/users/SudeeptMohan","html_url":"https://github.com/SudeeptMohan","followers_url":"https://api.github.com/users/SudeeptMohan/followers","following_url":"https://api.github.com/users/SudeeptMohan/following{/other_user}","gists_url":"https://api.github.com/users/SudeeptMohan/gists{/gist_id}","starred_url":"https://api.github.com/users/SudeeptMohan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SudeeptMohan/subscriptions","organizations_url":"https://api.github.com/users/SudeeptMohan/orgs","repos_url":"https://api.github.com/users/SudeeptMohan/repos","events_url":"https://api.github.com/users/SudeeptMohan/events{/privacy}","received_events_url":"https://api.github.com/users/SudeeptMohan/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-03-15T15:49:43Z","updated_at":"2019-08-16T10:09:35Z","closed_at":"2018-03-16T11:55:42Z","author_association":"NONE","body":"Browser Version:  \r\n65\r\n\r\n## Expected Behavior -\r\nThe search string should be passed by java program to google search text, chrome browser\r\n## Actual Behavior -\r\n\r\nCode used :\r\nSystem.out.println(\"performing search in google\");\r\n\tif (textBox.isEnabled())\r\n\t\t\tSystem.out.println(\"search box enabled\");\r\n\t\t//send search-text and opening search result\r\n\t\ttextBox.click();\r\n\t\tSystem.out.println(\"clicked\");\r\n\t\ttextBox.clear();\r\n\t\tSystem.out.println(\"cleared\");\r\n\t\ttextBox.sendKeys(\"Linux fast pc\");\r\n\t\tSystem.out.println(\"sent search string in google\");\r\n\r\n\r\nError Observed:\r\nThe search string is not entered in the search field.Infact it seems the click (first task of inputting in the program) is not working.\r\n\r\nException in thread \"main\" org.openqa.selenium.WebDriverException: unknown error: call function result missing 'value'\r\n  (Session info: chrome=65.0.3325.162)\r\n  (Driver info: chromedriver=2.33.506120 (e3e53437346286c0bc2d2dc9aa4915ba81d9023f),platform=Windows NT 10.0.16299 x86) (WARNING: The server did not provide any stacktrace information)\r\nCommand duration or timeout: 0 milliseconds\r\nBuild info: version: '3.9.1', revision: '63f7b50', time: '2018-02-07T22:42:22.379Z'\r\nSystem info: host: 'ADMIN-PC', ip: '192.168.1.5', os.name: 'Windows 10', os.arch: 'x86', os.version: '10.0', java.version: '1.8.0_161'\r\nDriver info: org.openqa.selenium.chrome.ChromeDriver\r\nCapabilities {acceptSslCerts: true, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.33.506120 (e3e53437346286..., userDataDir: C:\\Users\\admin\\AppData\\Loca...}, cssSelectorsEnabled: true, databaseEnabled: false, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 65.0.3325.162, webStorageEnabled: true}\r\nSession ID: 2dd7e11d7fc40b7faabf475b1e145ee8\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:160)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.clear(RemoteWebElement.java:118)\r\n\tat browserAutomation.main(browserAutomation.java:94)\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"SudeeptMohan","id":32921253,"node_id":"MDQ6VXNlcjMyOTIxMjUz","avatar_url":"https://avatars1.githubusercontent.com/u/32921253?v=4","gravatar_id":"","url":"https://api.github.com/users/SudeeptMohan","html_url":"https://github.com/SudeeptMohan","followers_url":"https://api.github.com/users/SudeeptMohan/followers","following_url":"https://api.github.com/users/SudeeptMohan/following{/other_user}","gists_url":"https://api.github.com/users/SudeeptMohan/gists{/gist_id}","starred_url":"https://api.github.com/users/SudeeptMohan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SudeeptMohan/subscriptions","organizations_url":"https://api.github.com/users/SudeeptMohan/orgs","repos_url":"https://api.github.com/users/SudeeptMohan/repos","events_url":"https://api.github.com/users/SudeeptMohan/events{/privacy}","received_events_url":"https://api.github.com/users/SudeeptMohan/received_events","type":"User","site_admin":false}}", "commentIds":["373472911","373691818"], "labels":[]}