{"id":"5951", "title":"Click is Passed But Action is not Performed on element using gecko driver !", "body":"## Meta -
OS:  Windows 10
Selenium Version:  3.12
Browser:  Firefox ?
Browser Version:  60.0.1 (64-bit)

## Expected Behavior -
Element Should Clicked.

## Actual Behavior -
Click Action is not Performed on element

## Steps to reproduce -
Open website https://www.shop.com/ 
Click on Drop down where All Department is by default chosen inside search text box
Click on Drop-down to open drop-down list
click  Cameras Option inside list

Click is Passed but Action is not performed.
 





", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5951","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5951/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5951/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5951/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5951","id":327332408,"node_id":"MDU6SXNzdWUzMjczMzI0MDg=","number":5951,"title":"Click is Passed But Action is not Performed on element using gecko driver !","user":{"login":"sachinbhatia39","id":39734092,"node_id":"MDQ6VXNlcjM5NzM0MDky","avatar_url":"https://avatars3.githubusercontent.com/u/39734092?v=4","gravatar_id":"","url":"https://api.github.com/users/sachinbhatia39","html_url":"https://github.com/sachinbhatia39","followers_url":"https://api.github.com/users/sachinbhatia39/followers","following_url":"https://api.github.com/users/sachinbhatia39/following{/other_user}","gists_url":"https://api.github.com/users/sachinbhatia39/gists{/gist_id}","starred_url":"https://api.github.com/users/sachinbhatia39/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sachinbhatia39/subscriptions","organizations_url":"https://api.github.com/users/sachinbhatia39/orgs","repos_url":"https://api.github.com/users/sachinbhatia39/repos","events_url":"https://api.github.com/users/sachinbhatia39/events{/privacy}","received_events_url":"https://api.github.com/users/sachinbhatia39/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-05-29T13:56:28Z","updated_at":"2019-08-15T21:09:44Z","closed_at":"2018-06-25T20:20:46Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\nSelenium Version:  3.12\r\nBrowser:  Firefox ?\r\nBrowser Version:  60.0.1 (64-bit)\r\n\r\n## Expected Behavior -\r\nElement Should Clicked.\r\n\r\n## Actual Behavior -\r\nClick Action is not Performed on element\r\n\r\n## Steps to reproduce -\r\nOpen website https://www.shop.com/ \r\nClick on Drop down where All Department is by default chosen inside search text box\r\nClick on Drop-down to open drop-down list\r\nclick  Cameras Option inside list\r\n\r\nClick is Passed but Action is not performed.\r\n \r\n\r\n\r\n\r\n\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["394159672","400082425"], "labels":["D-firefox","R-awaiting answer"]}