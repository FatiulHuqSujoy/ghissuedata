{"id":"7322", "title":"Issue with element locators in IE", "body":"**Environment:**
WebdriverIO version: 4.14.2
Mode: WDIO Testrunner (sync)
Browser name and version: IE 11
selenium standalone: selenium-server-standalone-3.141.59.jar
IE driver: 32 bit 3.14.0

**Config of WebdriverIO:**
specs: [${pathToTests}*.js],
deprecationWarnings: false,
maxInstances: 1,
capabilities: [
    {
      browserName: 'internet explorer',
      platform: 'WINDOWS',
      platformName: 'windows',
      logLevel: 'trace',
      'se:ieOptions': {
        'ie.ensureCleanSession': true,
        ignoreZoomSetting: true,
        ignoreProtectedModeSettings: true
      }
    }
  ],
sync: true,

**Issue Description:**
 I have following line of code that works perfectly fine in chrome browser but fails when running on IE:
`
        browser.waitForVisible('tbody tr #recruiter_application_status')
`
Getting error:
Error: Promise was rejected with the following reason: result.value.map is not a function
    at elementIdDisplayed

Did anyone face similar issue?", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7322","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7322/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7322/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7322/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7322","id":459951295,"node_id":"MDU6SXNzdWU0NTk5NTEyOTU=","number":7322,"title":"Issue with element locators in IE","user":{"login":"pk-saxena","id":35292540,"node_id":"MDQ6VXNlcjM1MjkyNTQw","avatar_url":"https://avatars0.githubusercontent.com/u/35292540?v=4","gravatar_id":"","url":"https://api.github.com/users/pk-saxena","html_url":"https://github.com/pk-saxena","followers_url":"https://api.github.com/users/pk-saxena/followers","following_url":"https://api.github.com/users/pk-saxena/following{/other_user}","gists_url":"https://api.github.com/users/pk-saxena/gists{/gist_id}","starred_url":"https://api.github.com/users/pk-saxena/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/pk-saxena/subscriptions","organizations_url":"https://api.github.com/users/pk-saxena/orgs","repos_url":"https://api.github.com/users/pk-saxena/repos","events_url":"https://api.github.com/users/pk-saxena/events{/privacy}","received_events_url":"https://api.github.com/users/pk-saxena/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-06-24T15:16:27Z","updated_at":"2019-08-14T07:09:44Z","closed_at":"2019-06-25T13:32:08Z","author_association":"NONE","body":"**Environment:**\r\nWebdriverIO version: 4.14.2\r\nMode: WDIO Testrunner (sync)\r\nBrowser name and version: IE 11\r\nselenium standalone: selenium-server-standalone-3.141.59.jar\r\nIE driver: 32 bit 3.14.0\r\n\r\n**Config of WebdriverIO:**\r\nspecs: [${pathToTests}*.js],\r\ndeprecationWarnings: false,\r\nmaxInstances: 1,\r\ncapabilities: [\r\n    {\r\n      browserName: 'internet explorer',\r\n      platform: 'WINDOWS',\r\n      platformName: 'windows',\r\n      logLevel: 'trace',\r\n      'se:ieOptions': {\r\n        'ie.ensureCleanSession': true,\r\n        ignoreZoomSetting: true,\r\n        ignoreProtectedModeSettings: true\r\n      }\r\n    }\r\n  ],\r\nsync: true,\r\n\r\n**Issue Description:**\r\n I have following line of code that works perfectly fine in chrome browser but fails when running on IE:\r\n`\r\n        browser.waitForVisible('tbody tr #recruiter_application_status')\r\n`\r\nGetting error:\r\nError: Promise was rejected with the following reason: result.value.map is not a function\r\n    at elementIdDisplayed\r\n\r\nDid anyone face similar issue?","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["505446298"], "labels":[]}