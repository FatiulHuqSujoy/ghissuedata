{"id":"7126", "title":"SessionNotCreatedError: Unable to create session error while launching Edge browser using Protractor", "body":"I am unable to launch edge browser session using Protractor. 

**Below are the configuration details:**

OS : Windows 10
Version : 1809
Os build : 17763.437
Selenium version : 3.141.59
Protractor version : 5.4.2

**### Error log**

Unable to create session from {
  \"desiredCapabilities\": {
    \"browserName\": \"MicrosoftEdge\"
  },
  \"capabilities\": {
    \"firstMatch\": [
      {
        \"browserName\": \"MicrosoftEdge\"
      }
    ]
  }
}
Build info: version: '3.141.59'
Driver info: driver.version: unknown
[15:32:48] E/launcher - SessionNotCreatedError: Unable to create session from {
  \"desiredCapabilities\": {
    \"browserName\": \"MicrosoftEdge\"
  },
  \"capabilities\": {
    \"firstMatch\": [
      {
        \"browserName\": \"MicrosoftEdge\"
      }
    ]
  }
}
Build info: version: '3.141.59'
Driver info: driver.version: unknown
    at Object.checkLegacyResponse (user\\node_modules\\selenium-webdriver\\lib\\error.js:546:15)
    at parseHttpResponse (user\\node_modules\\selenium-webdriver\\lib\\http.js:509:13)
    at doSend.then.response (user\\node_modules\\selenium-webdriver\\lib\\http.js:441:30)
    at <anonymous>
    at process._tickCallback (internal/process/next_tick.js:188:7)
From: Task: WebDriver.createSession()
    at Function.createSession (user\\node_modules\\selenium-webdriver\\lib\\webdriver.js:769:24)
    at createDriver (user\\node_modules\\selenium-webdriver\\index.js:170:33)
    at Builder.build (user\\node_modules\\selenium-webdriver\\index.js:635:14)
    at Hosted.getNewDriver (user\\node_modules\\protractor\\built\\driverProviders\\driverProvider.js:53:33)
    at Runner.createBrowser (user\\node_modules\\protractor\\built\\runner.js:195:43)
    at q.then.then (user\\node_modules\\protractor\\built\\runner.js:339:29)
    at _fulfilled (user\\node_modules\\q\\q.js:834:54)
    at self.promiseDispatch.done (user\\node_modules\\q\\q.js:863:30)
    at Promise.promise.promiseDispatch (user\\node_modules\\q\\q.js:796:13)
    at user\\node_modules\\q\\q.js:556:49
[15:32:48] E/launcher - Process exited with error code 199
 

I tried adding MicrosoftWebDriver.exe file to C:\\Windows\\System32 folder but still got the same error.

Does anyone have work around or solution for this issue?", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7126","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7126/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7126/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7126/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7126","id":435547054,"node_id":"MDU6SXNzdWU0MzU1NDcwNTQ=","number":7126,"title":"SessionNotCreatedError: Unable to create session error while launching Edge browser using Protractor","user":{"login":"GalmaJ","id":35112317,"node_id":"MDQ6VXNlcjM1MTEyMzE3","avatar_url":"https://avatars2.githubusercontent.com/u/35112317?v=4","gravatar_id":"","url":"https://api.github.com/users/GalmaJ","html_url":"https://github.com/GalmaJ","followers_url":"https://api.github.com/users/GalmaJ/followers","following_url":"https://api.github.com/users/GalmaJ/following{/other_user}","gists_url":"https://api.github.com/users/GalmaJ/gists{/gist_id}","starred_url":"https://api.github.com/users/GalmaJ/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/GalmaJ/subscriptions","organizations_url":"https://api.github.com/users/GalmaJ/orgs","repos_url":"https://api.github.com/users/GalmaJ/repos","events_url":"https://api.github.com/users/GalmaJ/events{/privacy}","received_events_url":"https://api.github.com/users/GalmaJ/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-04-21T20:55:06Z","updated_at":"2019-08-14T13:09:48Z","closed_at":"2019-04-21T21:46:06Z","author_association":"NONE","body":"I am unable to launch edge browser session using Protractor. \r\n\r\n**Below are the configuration details:**\r\n\r\nOS : Windows 10\r\nVersion : 1809\r\nOs build : 17763.437\r\nSelenium version : 3.141.59\r\nProtractor version : 5.4.2\r\n\r\n**### Error log**\r\n\r\nUnable to create session from {\r\n  \"desiredCapabilities\": {\r\n    \"browserName\": \"MicrosoftEdge\"\r\n  },\r\n  \"capabilities\": {\r\n    \"firstMatch\": [\r\n      {\r\n        \"browserName\": \"MicrosoftEdge\"\r\n      }\r\n    ]\r\n  }\r\n}\r\nBuild info: version: '3.141.59'\r\nDriver info: driver.version: unknown\r\n[15:32:48] E/launcher - SessionNotCreatedError: Unable to create session from {\r\n  \"desiredCapabilities\": {\r\n    \"browserName\": \"MicrosoftEdge\"\r\n  },\r\n  \"capabilities\": {\r\n    \"firstMatch\": [\r\n      {\r\n        \"browserName\": \"MicrosoftEdge\"\r\n      }\r\n    ]\r\n  }\r\n}\r\nBuild info: version: '3.141.59'\r\nDriver info: driver.version: unknown\r\n    at Object.checkLegacyResponse (user\\node_modules\\selenium-webdriver\\lib\\error.js:546:15)\r\n    at parseHttpResponse (user\\node_modules\\selenium-webdriver\\lib\\http.js:509:13)\r\n    at doSend.then.response (user\\node_modules\\selenium-webdriver\\lib\\http.js:441:30)\r\n    at <anonymous>\r\n    at process._tickCallback (internal/process/next_tick.js:188:7)\r\nFrom: Task: WebDriver.createSession()\r\n    at Function.createSession (user\\node_modules\\selenium-webdriver\\lib\\webdriver.js:769:24)\r\n    at createDriver (user\\node_modules\\selenium-webdriver\\index.js:170:33)\r\n    at Builder.build (user\\node_modules\\selenium-webdriver\\index.js:635:14)\r\n    at Hosted.getNewDriver (user\\node_modules\\protractor\\built\\driverProviders\\driverProvider.js:53:33)\r\n    at Runner.createBrowser (user\\node_modules\\protractor\\built\\runner.js:195:43)\r\n    at q.then.then (user\\node_modules\\protractor\\built\\runner.js:339:29)\r\n    at _fulfilled (user\\node_modules\\q\\q.js:834:54)\r\n    at self.promiseDispatch.done (user\\node_modules\\q\\q.js:863:30)\r\n    at Promise.promise.promiseDispatch (user\\node_modules\\q\\q.js:796:13)\r\n    at user\\node_modules\\q\\q.js:556:49\r\n[15:32:48] E/launcher - Process exited with error code 199\r\n \r\n\r\nI tried adding MicrosoftWebDriver.exe file to C:\\Windows\\System32 folder but still got the same error.\r\n\r\nDoes anyone have work around or solution for this issue?","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["485284584"], "labels":[]}