{"id":"6669", "title":"c# ChromeWebDriver throws exception when trying to get element property", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
 <package id=\"Selenium.Support\" version=\"3.141.0\" targetFramework=\"net461\" />
  <package id=\"Selenium.WebDriver\" version=\"3.141.0\" targetFramework=\"net461\" />
  <package id=\"Selenium.WebDriver.ChromeDriver\" version=\"2.43.0\" targetFramework=\"net461\" />

Browser:  
Chrome

Browser Version:  
Chrome Version 70.0.3538.102 

## Expected Behavior -
return \"https://www.google.co.nz\"
Tried with IE driver, it's slightly better, gives me null instead of exception.

## Actual Behavior -
System.NullReferenceException
  HResult=0x80004003
  Message=Object reference not set to an instance of an object.
  Source=WebDriver
  StackTrace:
   at OpenQA.Selenium.Remote.HttpCommandExecutor.HttpRequestInfo..ctor(Uri serverUri, Command commandToExecute, CommandInfo commandInfo)
   at OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)
   at OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)
   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   at OpenQA.Selenium.Remote.RemoteWebElement.Execute(String commandToExecute, Dictionary`2 parameters)
   at OpenQA.Selenium.Remote.RemoteWebElement.GetProperty(String propertyName)
...


## Steps to reproduce -
var driver = new ChromeDriver();
driver.Navigate().GoToUrl(\"https://www.google.co.nz\");	
driver.FindElement(By.Name(\"q\")).GetProperty(\"baseURI\");
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6669","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6669/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6669/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6669/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6669","id":382931803,"node_id":"MDU6SXNzdWUzODI5MzE4MDM=","number":6669,"title":"c# ChromeWebDriver throws exception when trying to get element property","user":{"login":"hejhj","id":6347633,"node_id":"MDQ6VXNlcjYzNDc2MzM=","avatar_url":"https://avatars2.githubusercontent.com/u/6347633?v=4","gravatar_id":"","url":"https://api.github.com/users/hejhj","html_url":"https://github.com/hejhj","followers_url":"https://api.github.com/users/hejhj/followers","following_url":"https://api.github.com/users/hejhj/following{/other_user}","gists_url":"https://api.github.com/users/hejhj/gists{/gist_id}","starred_url":"https://api.github.com/users/hejhj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hejhj/subscriptions","organizations_url":"https://api.github.com/users/hejhj/orgs","repos_url":"https://api.github.com/users/hejhj/repos","events_url":"https://api.github.com/users/hejhj/events{/privacy}","received_events_url":"https://api.github.com/users/hejhj/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2018-11-21T02:45:17Z","updated_at":"2019-08-14T16:09:34Z","closed_at":"2018-12-08T20:22:26Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n <package id=\"Selenium.Support\" version=\"3.141.0\" targetFramework=\"net461\" />\r\n  <package id=\"Selenium.WebDriver\" version=\"3.141.0\" targetFramework=\"net461\" />\r\n  <package id=\"Selenium.WebDriver.ChromeDriver\" version=\"2.43.0\" targetFramework=\"net461\" />\r\n\r\nBrowser:  \r\nChrome\r\n\r\nBrowser Version:  \r\nChrome Version 70.0.3538.102 \r\n\r\n## Expected Behavior -\r\nreturn \"https://www.google.co.nz\"\r\nTried with IE driver, it's slightly better, gives me null instead of exception.\r\n\r\n## Actual Behavior -\r\nSystem.NullReferenceException\r\n  HResult=0x80004003\r\n  Message=Object reference not set to an instance of an object.\r\n  Source=WebDriver\r\n  StackTrace:\r\n   at OpenQA.Selenium.Remote.HttpCommandExecutor.HttpRequestInfo..ctor(Uri serverUri, Command commandToExecute, CommandInfo commandInfo)\r\n   at OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)\r\n   at OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   at OpenQA.Selenium.Remote.RemoteWebElement.Execute(String commandToExecute, Dictionary`2 parameters)\r\n   at OpenQA.Selenium.Remote.RemoteWebElement.GetProperty(String propertyName)\r\n...\r\n\r\n\r\n## Steps to reproduce -\r\nvar driver = new ChromeDriver();\r\ndriver.Navigate().GoToUrl(\"https://www.google.co.nz\");\t\r\ndriver.FindElement(By.Name(\"q\")).GetProperty(\"baseURI\");\r\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["440519784","445487205","482459533","482584443","482586669","482590214","482616477"], "labels":[]}