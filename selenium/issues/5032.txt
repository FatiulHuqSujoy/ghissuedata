{"id":"5032", "title":"Actions.MoveToElement throws StaleElementException on Element that is found again after a DOM refresh", "body":"## Meta -
OS:  
Windows 10, v1703 build 15063.674
Selenium Version:  
v3.6.0
Browser:  
Chrome & Firefox 
Browser Version:  
v62.0.3202.75 (32-bit) & v56.0.2 (32-bit)

## Expected Behavior -
Actions.MoveToElement to not throw a StaleElementException when the element has been \"found\" again after a DOM refresh.
## Actual Behavior -
I move the mouse to an element, click on some other element, then navigate back.
I then \"find\" the element again and try to move the mouse to the element again where a StaleElementException is thrown.
This happens on both Chrome & FF.
```
public void CNN()
        {
            driver.Navigate().GoToUrl(\"http://www.cnn.com/\");
            //Maximize the window so that the list can be gathered successfully.
            driver.Manage().Window.Maximize();

            //find the list
            By xPath = By.XPath(\"//div[@class='nav-menu-links']/a\");
            IList<IWebElement> linkCollection = driver.FindElements(xPath);

            //Get the first tlink
            string locator = \"//div[@class='nav-menu-links']/a[1]\";
            IWebElement myLink = driver.FindElement(By.XPath(locator));

            //Move mouse to the 1st link, Click each link
            foreach (var item in linkCollection)
            {
                //Hover over the first link
                move.MoveToElement(myLink).Build().Perform();

                //Click on the link
                item.Click();

                //Go back
                driver.Navigate().Back();

                //Find the first link again
                myLink = driver.FindElement(By.XPath(locator));
            }
        }
```

Exception thrown: 'OpenQA.Selenium.StaleElementReferenceException' in WebDriver.dll
An exception of type 'OpenQA.Selenium.StaleElementReferenceException' occurred in WebDriver.dll but was not handled in user code
The element reference of <a class=\"nav-menu-links__link\"> stale: either the element is no longer attached to the DOM or the page has been refreshed", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5032","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5032/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5032/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5032/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5032","id":272228451,"node_id":"MDU6SXNzdWUyNzIyMjg0NTE=","number":5032,"title":"Actions.MoveToElement throws StaleElementException on Element that is found again after a DOM refresh","user":{"login":"realfaisalk","id":23498757,"node_id":"MDQ6VXNlcjIzNDk4NzU3","avatar_url":"https://avatars3.githubusercontent.com/u/23498757?v=4","gravatar_id":"","url":"https://api.github.com/users/realfaisalk","html_url":"https://github.com/realfaisalk","followers_url":"https://api.github.com/users/realfaisalk/followers","following_url":"https://api.github.com/users/realfaisalk/following{/other_user}","gists_url":"https://api.github.com/users/realfaisalk/gists{/gist_id}","starred_url":"https://api.github.com/users/realfaisalk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/realfaisalk/subscriptions","organizations_url":"https://api.github.com/users/realfaisalk/orgs","repos_url":"https://api.github.com/users/realfaisalk/repos","events_url":"https://api.github.com/users/realfaisalk/events{/privacy}","received_events_url":"https://api.github.com/users/realfaisalk/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-11-08T14:58:19Z","updated_at":"2019-08-17T03:09:46Z","closed_at":"2017-11-08T15:13:52Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10, v1703 build 15063.674\r\nSelenium Version:  \r\nv3.6.0\r\nBrowser:  \r\nChrome & Firefox \r\nBrowser Version:  \r\nv62.0.3202.75 (32-bit) & v56.0.2 (32-bit)\r\n\r\n## Expected Behavior -\r\nActions.MoveToElement to not throw a StaleElementException when the element has been \"found\" again after a DOM refresh.\r\n## Actual Behavior -\r\nI move the mouse to an element, click on some other element, then navigate back.\r\nI then \"find\" the element again and try to move the mouse to the element again where a StaleElementException is thrown.\r\nThis happens on both Chrome & FF.\r\n```\r\npublic void CNN()\r\n        {\r\n            driver.Navigate().GoToUrl(\"http://www.cnn.com/\");\r\n            //Maximize the window so that the list can be gathered successfully.\r\n            driver.Manage().Window.Maximize();\r\n\r\n            //find the list\r\n            By xPath = By.XPath(\"//div[@class='nav-menu-links']/a\");\r\n            IList<IWebElement> linkCollection = driver.FindElements(xPath);\r\n\r\n            //Get the first tlink\r\n            string locator = \"//div[@class='nav-menu-links']/a[1]\";\r\n            IWebElement myLink = driver.FindElement(By.XPath(locator));\r\n\r\n            //Move mouse to the 1st link, Click each link\r\n            foreach (var item in linkCollection)\r\n            {\r\n                //Hover over the first link\r\n                move.MoveToElement(myLink).Build().Perform();\r\n\r\n                //Click on the link\r\n                item.Click();\r\n\r\n                //Go back\r\n                driver.Navigate().Back();\r\n\r\n                //Find the first link again\r\n                myLink = driver.FindElement(By.XPath(locator));\r\n            }\r\n        }\r\n```\r\n\r\nException thrown: 'OpenQA.Selenium.StaleElementReferenceException' in WebDriver.dll\r\nAn exception of type 'OpenQA.Selenium.StaleElementReferenceException' occurred in WebDriver.dll but was not handled in user code\r\nThe element reference of <a class=\"nav-menu-links__link\"> stale: either the element is no longer attached to the DOM or the page has been refreshed","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["342847706","342851483"], "labels":[]}