{"id":"4518", "title":"send_keys(\"é\") to IE11 <input/> element sends e instead of é", "body":"## Meta -
OS:  
Windows 10 Pro (1703)
Selenium Version:  
3.4.0
Browser:  
IE11 (11.540.15063.0)
IEDriverServer.exe 3.5.1.0 (32-bit)

## Expected/Actual Behavior -

Input field text box should have value \"é\" but actually has \"e\".

## Steps to reproduce -

Using python selenium, run this script on a selenium server running remotely on a Win10 VM:

```
from selenium import webdriver
driver = webdriver.Remote(\"http://<server-ip>:4444/wd/hub\",
    webdriver.DesiredCapabilities.INTERNETEXPLORER)
driver.get(\"https://www.google.com\")
elem = driver.find_element_by_id(\"lst-ib\")  # the main search box
elem.send_keys(u\"é\")
```

The search box should show é but has e (no accent) instead. The same thing works with other non-ascii characters such as ç or 日本語, but whenever I use é the browser shows e instead (and submits e).

The same scenario works using IE11 (11.0.9600.17358) on Windows7 so it could well be a Windows10 thing I don't understand.

IE driver log at TRACE looks like this:

```
T 2017-08-21 11:10:32:339 server.cc(174) Entering Server::ProcessRequest
T 2017-08-21 11:10:32:339 server.cc(238) Entering Server::ReadRequestBody
T 2017-08-21 11:10:32:339 server.cc(183) Process request with: URI: /session/6526d364-2c09-4dc5-a651-25ef1cf377ce/element/14063de9-df6e-428a-9ec3-dbc419af81e9/value HTTP verb: POST
body: {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]}
T 2017-08-21 11:10:32:339 server.cc(272) Entering Server::DispatchCommand
T 2017-08-21 11:10:32:339 server.cc(583) Entering Server::LookupCommand
D 2017-08-21 11:10:32:339 server.cc(281) Command: POST /session/6526d364-2c09-4dc5-a651-25ef1cf377ce/element/14063de9-df6e-428a-9ec3-dbc419af81e9/value {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]}
T 2017-08-21 11:10:32:339 server.cc(378) Entering Server::LookupSession
T 2017-08-21 11:10:32:339 IESession.cpp(191) Entering IESession::ExecuteCommand
T 2017-08-21 11:10:32:339 IECommandExecutor.cpp(122) Entering IECommandExecutor::OnSetCommand
T 2017-08-21 11:10:32:339 command.cc(31) Entering Command::Deserialize
D 2017-08-21 11:10:32:339 command.cc(36) Raw JSON command: { \"name\" : \"sendKeys\", \"locator\" : { \"sessionid\" : \"6526d364-2c09-4dc5-a651-25ef1cf377ce\", \"id\" : \"14063de9-df6e-428a-9ec3-dbc419af81e9\" }, \"parameters\" : {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]} }
T 2017-08-21 11:10:32:340 IESession.cpp(212) Beginning wait for response length to be not zero
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(133) Entering IECommandExecutor::OnExecCommand
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(454) Entering IECommandExecutor::DispatchCommand
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(523) Entering IECommandExecutor::IsAlertActive
T 2017-08-21 11:10:32:340 Browser.cpp(713) Entering Browser::GetActiveDialogWindowHandle
T 2017-08-21 11:10:32:340 Browser.cpp(206) Entering Browser::GetContentWindowHandle
D 2017-08-21 11:10:32:340 IECommandExecutor.cpp(537) No alert handle is found
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser
T 2017-08-21 11:10:32:340 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser
T 2017-08-21 11:10:32:340 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:340 Browser.cpp(416) Entering Browser::GetTopLevelWindowHandle
T 2017-08-21 11:10:32:342 IECommandHandler.cpp(45) Entering IECommandHandler::GetElement
T 2017-08-21 11:10:32:342 IECommandExecutor.cpp(661) Entering IECommandExecutor::GetManagedElement
T 2017-08-21 11:10:32:342 ElementRepository.cpp(34) Entering ElementRepository::GetManagedElement
T 2017-08-21 11:10:32:346 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser
T 2017-08-21 11:10:32:346 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser
T 2017-08-21 11:10:32:346 Browser.cpp(126) Entering Browser::GetDocument
I 2017-08-21 11:10:32:346 Browser.cpp(130) No child frame focus. Focus is on top-level frame
T 2017-08-21 11:10:32:352 Browser.cpp(613) Entering Browser::GetDocumentFromWindow
T 2017-08-21 11:10:32:358 Element.cpp(310) Entering Element::GetLocationOnceScrolledIntoView
T 2017-08-21 11:10:32:358 Element.cpp(441) Entering Element::GetLocation
T 2017-08-21 11:10:32:358 Element.cpp(577) Entering Element::IsInline
D 2017-08-21 11:10:32:358 Element.cpp(487) Element is a block element, using IHTMLElement2::getBoundingClientRect
T 2017-08-21 11:10:32:363 Element.cpp(598) Entering Element::RectHasNonZeroDimensions
T 2017-08-21 11:10:32:364 Element.cpp(614) Entering Element::GetFrameDetails
T 2017-08-21 11:10:32:364 Element.cpp(904) Entering Element::GetContainingDocument
D 2017-08-21 11:10:32:374 Element.cpp(564) Element is not in a frame
T 2017-08-21 11:10:32:374 Element.cpp(853) Entering Element::CalculateClickPoint
T 2017-08-21 11:10:32:374 Element.cpp(792) Entering Element::GetClickableViewPortLocation
T 2017-08-21 11:10:32:374 Element.cpp(904) Entering Element::GetContainingDocument
T 2017-08-21 11:10:32:375 DocumentHost.cpp(271) Entering DocumentHost::GetDocumentMode
T 2017-08-21 11:10:32:381 Element.cpp(879) Entering Element::IsLocationInViewPort
T 2017-08-21 11:10:32:381 Element.cpp(792) Entering Element::GetClickableViewPortLocation
T 2017-08-21 11:10:32:381 Element.cpp(904) Entering Element::GetContainingDocument
T 2017-08-21 11:10:32:382 DocumentHost.cpp(271) Entering DocumentHost::GetDocumentMode
T 2017-08-21 11:10:32:390 Element.cpp(377) Entering Element::IsHiddenByOverflow
T 2017-08-21 11:10:32:391 Element.cpp(904) Entering Element::GetContainingDocument
T 2017-08-21 11:10:32:392 Script.cpp(49) Entering Script::Initialize
T 2017-08-21 11:10:32:392 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)
T 2017-08-21 11:10:32:392 Script.cpp(105) Entering Script::AddArgument(VARIANT)
T 2017-08-21 11:10:32:392 Script.cpp(169) Entering Script::Execute
T 2017-08-21 11:10:32:392 Script.cpp(477) Entering Script::CreateAnonymousFunction
T 2017-08-21 11:10:32:420 Script.cpp(433) Entering Script::ConvertResultToString
D 2017-08-21 11:10:32:420 Script.cpp(444) Result type is string
D 2017-08-21 11:10:32:420 Element.cpp(356) (x, y, w, h): 277, 324, 461, 34
T 2017-08-21 11:10:32:421 Element.cpp(89) Entering Element::IsDisplayed
T 2017-08-21 11:10:32:421 Element.cpp(904) Entering Element::GetContainingDocument
T 2017-08-21 11:10:32:422 Script.cpp(49) Entering Script::Initialize
T 2017-08-21 11:10:32:422 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)
T 2017-08-21 11:10:32:422 Script.cpp(105) Entering Script::AddArgument(VARIANT)
T 2017-08-21 11:10:32:422 Script.cpp(88) Entering Script::AddArgument(bool)
T 2017-08-21 11:10:32:422 Script.cpp(105) Entering Script::AddArgument(VARIANT)
T 2017-08-21 11:10:32:422 Script.cpp(169) Entering Script::Execute
T 2017-08-21 11:10:32:422 Script.cpp(477) Entering Script::CreateAnonymousFunction
T 2017-08-21 11:10:32:452 Element.cpp(133) Entering Element::IsEnabled
T 2017-08-21 11:10:32:452 Element.cpp(904) Entering Element::GetContainingDocument
T 2017-08-21 11:10:32:453 Script.cpp(49) Entering Script::Initialize
T 2017-08-21 11:10:32:453 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)
T 2017-08-21 11:10:32:453 Script.cpp(105) Entering Script::AddArgument(VARIANT)
T 2017-08-21 11:10:32:453 Script.cpp(169) Entering Script::Execute
T 2017-08-21 11:10:32:453 Script.cpp(477) Entering Script::CreateAnonymousFunction
I 2017-08-21 11:10:32:473 CommandHandlers\\SendKeysCommandHandler.cpp(800) Focus is on a UI element other than the HTML viewer pane.
T 2017-08-21 11:10:32:523 InputManager.cpp(80) Entering InputManager::PerformInputSequence
D 2017-08-21 11:10:32:523 InputManager.cpp(101) Mutex acquired for user interaction.
T 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput
T 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput
T 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput
T 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput
T 2017-08-21 11:10:32:523 HookProcessor.cpp(94) Entering HookProcessor::Initialize
T 2017-08-21 11:10:32:523 HookProcessor.cpp(104) Entering HookProcessor::Initialize
T 2017-08-21 11:10:32:523 HookProcessor.cpp(167) Entering HookProcessor::InstallWindowsHook
T 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle
T 2017-08-21 11:10:32:529 HookProcessor.cpp(244) Entering HookProcessor::Dispose
T 2017-08-21 11:10:32:529 HookProcessor.cpp(197) Entering HookProcessor::UninstallWindowsHook
T 2017-08-21 11:10:32:529 HookProcessor.cpp(244) Entering HookProcessor::Dispose
T 2017-08-21 11:10:32:583 response.cc(72) Entering Response::SetSuccessResponse
T 2017-08-21 11:10:32:583 response.cc(78) Entering Response::SetResponse
T 2017-08-21 11:10:32:583 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser
T 2017-08-21 11:10:32:583 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser
T 2017-08-21 11:10:32:583 response.cc(51) Entering Response::Serialize
T 2017-08-21 11:10:32:583 IESession.cpp(221) Found non-zero response length
T 2017-08-21 11:10:32:583 IECommandExecutor.cpp(156) Entering IECommandExecutor::OnGetResponse
T 2017-08-21 11:10:32:583 IECommandExecutor.cpp(285) Entering IECommandExecutor::OnIsSessionValid
D 2017-08-21 11:10:32:583 server.cc(338) Response: {\"value\":null}
```

So, it looks like IE driver is being asked to send the right text.

I can manually enter é on browser into the field so it seems like a bug in IEdriver or something I don't understand in terms of keyboard mapping?
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4518","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4518/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4518/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4518/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4518","id":251627647,"node_id":"MDU6SXNzdWUyNTE2Mjc2NDc=","number":4518,"title":"send_keys(\"é\") to IE11 <input/> element sends e instead of é","user":{"login":"rdharrison2","id":2032741,"node_id":"MDQ6VXNlcjIwMzI3NDE=","avatar_url":"https://avatars2.githubusercontent.com/u/2032741?v=4","gravatar_id":"","url":"https://api.github.com/users/rdharrison2","html_url":"https://github.com/rdharrison2","followers_url":"https://api.github.com/users/rdharrison2/followers","following_url":"https://api.github.com/users/rdharrison2/following{/other_user}","gists_url":"https://api.github.com/users/rdharrison2/gists{/gist_id}","starred_url":"https://api.github.com/users/rdharrison2/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rdharrison2/subscriptions","organizations_url":"https://api.github.com/users/rdharrison2/orgs","repos_url":"https://api.github.com/users/rdharrison2/repos","events_url":"https://api.github.com/users/rdharrison2/events{/privacy}","received_events_url":"https://api.github.com/users/rdharrison2/received_events","type":"User","site_admin":false},"labels":[{"id":182505367,"node_id":"MDU6TGFiZWwxODI1MDUzNjc=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-IE","name":"D-IE","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-08-21T11:16:49Z","updated_at":"2019-08-17T20:09:53Z","closed_at":"2017-08-22T14:51:44Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10 Pro (1703)\r\nSelenium Version:  \r\n3.4.0\r\nBrowser:  \r\nIE11 (11.540.15063.0)\r\nIEDriverServer.exe 3.5.1.0 (32-bit)\r\n\r\n## Expected/Actual Behavior -\r\n\r\nInput field text box should have value \"é\" but actually has \"e\".\r\n\r\n## Steps to reproduce -\r\n\r\nUsing python selenium, run this script on a selenium server running remotely on a Win10 VM:\r\n\r\n```\r\nfrom selenium import webdriver\r\ndriver = webdriver.Remote(\"http://<server-ip>:4444/wd/hub\",\r\n    webdriver.DesiredCapabilities.INTERNETEXPLORER)\r\ndriver.get(\"https://www.google.com\")\r\nelem = driver.find_element_by_id(\"lst-ib\")  # the main search box\r\nelem.send_keys(u\"é\")\r\n```\r\n\r\nThe search box should show é but has e (no accent) instead. The same thing works with other non-ascii characters such as ç or 日本語, but whenever I use é the browser shows e instead (and submits e).\r\n\r\nThe same scenario works using IE11 (11.0.9600.17358) on Windows7 so it could well be a Windows10 thing I don't understand.\r\n\r\nIE driver log at TRACE looks like this:\r\n\r\n```\r\nT 2017-08-21 11:10:32:339 server.cc(174) Entering Server::ProcessRequest\r\nT 2017-08-21 11:10:32:339 server.cc(238) Entering Server::ReadRequestBody\r\nT 2017-08-21 11:10:32:339 server.cc(183) Process request with: URI: /session/6526d364-2c09-4dc5-a651-25ef1cf377ce/element/14063de9-df6e-428a-9ec3-dbc419af81e9/value HTTP verb: POST\r\nbody: {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]}\r\nT 2017-08-21 11:10:32:339 server.cc(272) Entering Server::DispatchCommand\r\nT 2017-08-21 11:10:32:339 server.cc(583) Entering Server::LookupCommand\r\nD 2017-08-21 11:10:32:339 server.cc(281) Command: POST /session/6526d364-2c09-4dc5-a651-25ef1cf377ce/element/14063de9-df6e-428a-9ec3-dbc419af81e9/value {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]}\r\nT 2017-08-21 11:10:32:339 server.cc(378) Entering Server::LookupSession\r\nT 2017-08-21 11:10:32:339 IESession.cpp(191) Entering IESession::ExecuteCommand\r\nT 2017-08-21 11:10:32:339 IECommandExecutor.cpp(122) Entering IECommandExecutor::OnSetCommand\r\nT 2017-08-21 11:10:32:339 command.cc(31) Entering Command::Deserialize\r\nD 2017-08-21 11:10:32:339 command.cc(36) Raw JSON command: { \"name\" : \"sendKeys\", \"locator\" : { \"sessionid\" : \"6526d364-2c09-4dc5-a651-25ef1cf377ce\", \"id\" : \"14063de9-df6e-428a-9ec3-dbc419af81e9\" }, \"parameters\" : {\"id\":\"14063de9-df6e-428a-9ec3-dbc419af81e9\",\"text\":\"é\",\"value\":[\"é\"]} }\r\nT 2017-08-21 11:10:32:340 IESession.cpp(212) Beginning wait for response length to be not zero\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(133) Entering IECommandExecutor::OnExecCommand\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(454) Entering IECommandExecutor::DispatchCommand\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(523) Entering IECommandExecutor::IsAlertActive\r\nT 2017-08-21 11:10:32:340 Browser.cpp(713) Entering Browser::GetActiveDialogWindowHandle\r\nT 2017-08-21 11:10:32:340 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nD 2017-08-21 11:10:32:340 IECommandExecutor.cpp(537) No alert handle is found\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser\r\nT 2017-08-21 11:10:32:340 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser\r\nT 2017-08-21 11:10:32:340 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:340 Browser.cpp(416) Entering Browser::GetTopLevelWindowHandle\r\nT 2017-08-21 11:10:32:342 IECommandHandler.cpp(45) Entering IECommandHandler::GetElement\r\nT 2017-08-21 11:10:32:342 IECommandExecutor.cpp(661) Entering IECommandExecutor::GetManagedElement\r\nT 2017-08-21 11:10:32:342 ElementRepository.cpp(34) Entering ElementRepository::GetManagedElement\r\nT 2017-08-21 11:10:32:346 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser\r\nT 2017-08-21 11:10:32:346 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser\r\nT 2017-08-21 11:10:32:346 Browser.cpp(126) Entering Browser::GetDocument\r\nI 2017-08-21 11:10:32:346 Browser.cpp(130) No child frame focus. Focus is on top-level frame\r\nT 2017-08-21 11:10:32:352 Browser.cpp(613) Entering Browser::GetDocumentFromWindow\r\nT 2017-08-21 11:10:32:358 Element.cpp(310) Entering Element::GetLocationOnceScrolledIntoView\r\nT 2017-08-21 11:10:32:358 Element.cpp(441) Entering Element::GetLocation\r\nT 2017-08-21 11:10:32:358 Element.cpp(577) Entering Element::IsInline\r\nD 2017-08-21 11:10:32:358 Element.cpp(487) Element is a block element, using IHTMLElement2::getBoundingClientRect\r\nT 2017-08-21 11:10:32:363 Element.cpp(598) Entering Element::RectHasNonZeroDimensions\r\nT 2017-08-21 11:10:32:364 Element.cpp(614) Entering Element::GetFrameDetails\r\nT 2017-08-21 11:10:32:364 Element.cpp(904) Entering Element::GetContainingDocument\r\nD 2017-08-21 11:10:32:374 Element.cpp(564) Element is not in a frame\r\nT 2017-08-21 11:10:32:374 Element.cpp(853) Entering Element::CalculateClickPoint\r\nT 2017-08-21 11:10:32:374 Element.cpp(792) Entering Element::GetClickableViewPortLocation\r\nT 2017-08-21 11:10:32:374 Element.cpp(904) Entering Element::GetContainingDocument\r\nT 2017-08-21 11:10:32:375 DocumentHost.cpp(271) Entering DocumentHost::GetDocumentMode\r\nT 2017-08-21 11:10:32:381 Element.cpp(879) Entering Element::IsLocationInViewPort\r\nT 2017-08-21 11:10:32:381 Element.cpp(792) Entering Element::GetClickableViewPortLocation\r\nT 2017-08-21 11:10:32:381 Element.cpp(904) Entering Element::GetContainingDocument\r\nT 2017-08-21 11:10:32:382 DocumentHost.cpp(271) Entering DocumentHost::GetDocumentMode\r\nT 2017-08-21 11:10:32:390 Element.cpp(377) Entering Element::IsHiddenByOverflow\r\nT 2017-08-21 11:10:32:391 Element.cpp(904) Entering Element::GetContainingDocument\r\nT 2017-08-21 11:10:32:392 Script.cpp(49) Entering Script::Initialize\r\nT 2017-08-21 11:10:32:392 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)\r\nT 2017-08-21 11:10:32:392 Script.cpp(105) Entering Script::AddArgument(VARIANT)\r\nT 2017-08-21 11:10:32:392 Script.cpp(169) Entering Script::Execute\r\nT 2017-08-21 11:10:32:392 Script.cpp(477) Entering Script::CreateAnonymousFunction\r\nT 2017-08-21 11:10:32:420 Script.cpp(433) Entering Script::ConvertResultToString\r\nD 2017-08-21 11:10:32:420 Script.cpp(444) Result type is string\r\nD 2017-08-21 11:10:32:420 Element.cpp(356) (x, y, w, h): 277, 324, 461, 34\r\nT 2017-08-21 11:10:32:421 Element.cpp(89) Entering Element::IsDisplayed\r\nT 2017-08-21 11:10:32:421 Element.cpp(904) Entering Element::GetContainingDocument\r\nT 2017-08-21 11:10:32:422 Script.cpp(49) Entering Script::Initialize\r\nT 2017-08-21 11:10:32:422 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)\r\nT 2017-08-21 11:10:32:422 Script.cpp(105) Entering Script::AddArgument(VARIANT)\r\nT 2017-08-21 11:10:32:422 Script.cpp(88) Entering Script::AddArgument(bool)\r\nT 2017-08-21 11:10:32:422 Script.cpp(105) Entering Script::AddArgument(VARIANT)\r\nT 2017-08-21 11:10:32:422 Script.cpp(169) Entering Script::Execute\r\nT 2017-08-21 11:10:32:422 Script.cpp(477) Entering Script::CreateAnonymousFunction\r\nT 2017-08-21 11:10:32:452 Element.cpp(133) Entering Element::IsEnabled\r\nT 2017-08-21 11:10:32:452 Element.cpp(904) Entering Element::GetContainingDocument\r\nT 2017-08-21 11:10:32:453 Script.cpp(49) Entering Script::Initialize\r\nT 2017-08-21 11:10:32:453 Script.cpp(99) Entering Script::AddArgument(IHTMLElement*)\r\nT 2017-08-21 11:10:32:453 Script.cpp(105) Entering Script::AddArgument(VARIANT)\r\nT 2017-08-21 11:10:32:453 Script.cpp(169) Entering Script::Execute\r\nT 2017-08-21 11:10:32:453 Script.cpp(477) Entering Script::CreateAnonymousFunction\r\nI 2017-08-21 11:10:32:473 CommandHandlers\\SendKeysCommandHandler.cpp(800) Focus is on a UI element other than the HTML viewer pane.\r\nT 2017-08-21 11:10:32:523 InputManager.cpp(80) Entering InputManager::PerformInputSequence\r\nD 2017-08-21 11:10:32:523 InputManager.cpp(101) Mutex acquired for user interaction.\r\nT 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput\r\nT 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput\r\nT 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput\r\nT 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:523 InputManager.cpp(798) Entering InputManager::AddKeyboardInput\r\nT 2017-08-21 11:10:32:523 HookProcessor.cpp(94) Entering HookProcessor::Initialize\r\nT 2017-08-21 11:10:32:523 HookProcessor.cpp(104) Entering HookProcessor::Initialize\r\nT 2017-08-21 11:10:32:523 HookProcessor.cpp(167) Entering HookProcessor::InstallWindowsHook\r\nT 2017-08-21 11:10:32:523 Browser.cpp(206) Entering Browser::GetContentWindowHandle\r\nT 2017-08-21 11:10:32:529 HookProcessor.cpp(244) Entering HookProcessor::Dispose\r\nT 2017-08-21 11:10:32:529 HookProcessor.cpp(197) Entering HookProcessor::UninstallWindowsHook\r\nT 2017-08-21 11:10:32:529 HookProcessor.cpp(244) Entering HookProcessor::Dispose\r\nT 2017-08-21 11:10:32:583 response.cc(72) Entering Response::SetSuccessResponse\r\nT 2017-08-21 11:10:32:583 response.cc(78) Entering Response::SetResponse\r\nT 2017-08-21 11:10:32:583 IECommandExecutor.cpp(567) Entering IECommandExecutor::GetCurrentBrowser\r\nT 2017-08-21 11:10:32:583 IECommandExecutor.cpp(573) Entering IECommandExecutor::GetManagedBrowser\r\nT 2017-08-21 11:10:32:583 response.cc(51) Entering Response::Serialize\r\nT 2017-08-21 11:10:32:583 IESession.cpp(221) Found non-zero response length\r\nT 2017-08-21 11:10:32:583 IECommandExecutor.cpp(156) Entering IECommandExecutor::OnGetResponse\r\nT 2017-08-21 11:10:32:583 IECommandExecutor.cpp(285) Entering IECommandExecutor::OnIsSessionValid\r\nD 2017-08-21 11:10:32:583 server.cc(338) Response: {\"value\":null}\r\n```\r\n\r\nSo, it looks like IE driver is being asked to send the right text.\r\n\r\nI can manually enter é on browser into the field so it seems like a bug in IEdriver or something I don't understand in terms of keyboard mapping?\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["324051108"], "labels":["D-IE"]}