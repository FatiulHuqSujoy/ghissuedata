{"id":"6574", "title":"IEServerDriver PageLoadTimeout after closing one ModelDialog triggers a new ModelDialog.", "body":"## Meta -
OS: Windows 8.1
Selenium Version: selenium-java latest (3.14.0)
IEDriverServer Version(s) - Win32 3.14.0
Browser: Internet Explorer
Browser Version: 11.0.9600.19101

## Expected Behavior -

Expect the disappearance of the Modal Dialog to allow the test to continue, even if the application under test immediately opened a new Model Dialog.

## Actual Behavior -

Clicking a button that closes the dialog eventually fails with a `TimeoutException: Timed out waiting for page to load`. 

Looking in the [IEServerlog.log](https://github.com/SeleniumHQ/selenium/files/2513650/IEServerlog.log) it repeats a loop around `HtmlDialog::Wait`

```
iedriver\\iecommandexecutor.cpp(801) Creating wait thread with deferred response of `{ \"value\" : null }`
iedriver\\iecommandexecutor.cpp(558) Entering IECommandExecutor::WaitThreadProc
iedriver\\iecommandexecutor.cpp(181) Entering IECommandExecutor::OnWait
iedriver\\iecommandexecutor.cpp(890) Entering IECommandExecutor::GetCurrentBrowser
iedriver\\iecommandexecutor.cpp(896) Entering IECommandExecutor::GetManagedBrowser
iedriver\\htmldialog.cpp(126) Entering HtmlDialog::Wait
iedriver\\htmldialog.cpp(100) Entering HtmlDialog::IsValidWindow
iedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle
iedriver\\htmldialog.cpp(193) Entering HtmlDialog::GetActiveDialogWindowHandle
iedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle
iedriver\\iecommandexecutor.cpp(829) Entering IECommandExecutor::IsAlertActive
iedriver\\htmldialog.cpp(193) Entering HtmlDialog::GetActiveDialogWindowHandle
iedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle
iedriver\\iecommandexecutor.cpp(843) No alert handle is found
iedriver\\iecommandexecutor.cpp(801) Creating wait thread with deferred response of `{ \"value\" : null }`
[...]
```
## Steps to reproduce -

See [Attached code](https://gist.github.com/JonathanHallKJR/adefa242cbdda98b30ba48ad82b4b3d3#file-seleniumbugreport-java-L51)

While running it serves 3 pages:
* http://localhost:8080/ - main Page with test button and textarea for log messages.
* http://localhost:8080/popup/promptWindow - first dialog.
* http://localhost:8080/popup/areYouSure - second dialog.

Expected Flow would be:
1. User clicks Test button on the main page.
  * which opens a first dialog with `window.showModalDialog` passing a callback function in via `window.dialogArguments[0]`.
2. User clicks a button on the first dialog. 
  * which triggers calls to `window.close()` then `callback(answer)`
  * first callback logs the answer and opens the second modal dialog. 
  :warning: NOTE: When running in automation this is where it fails to detect the first dialog has gone.
3. User clicks a button on the second dialog.
  * which triggers calls to `window.close()` then `callback(answer)`
  * second callback logs the answer.

## Analysis
Originally discovered while testing a real application in which the automation would fail during a `driver.getWindowHandles()` (after the `element.click()`).  The IEDriverServer logs then showed a similar `HtmlDialog::Wait` loop above with a different deferred response.

:warning: If you catch the TimeoutException and call `driver.getWindowHandles()` there are **three** window handles returned despite only two being visible to the user.

It may be possible to fix this in the application under test since the either of the following prevent replication (however the code as is works for humans and is only causing problems for automation).
1. If the callback doesn't trigger a new `window.showModalDialog`.
2. If the callback is called before `window.close()` (but two dialogs are open at the same time)  ", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6574","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6574/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6574/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6574/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6574","id":373799728,"node_id":"MDU6SXNzdWUzNzM3OTk3Mjg=","number":6574,"title":"IEServerDriver PageLoadTimeout after closing one ModelDialog triggers a new ModelDialog.","user":{"login":"JonathanHallKJR","id":1574693,"node_id":"MDQ6VXNlcjE1NzQ2OTM=","avatar_url":"https://avatars2.githubusercontent.com/u/1574693?v=4","gravatar_id":"","url":"https://api.github.com/users/JonathanHallKJR","html_url":"https://github.com/JonathanHallKJR","followers_url":"https://api.github.com/users/JonathanHallKJR/followers","following_url":"https://api.github.com/users/JonathanHallKJR/following{/other_user}","gists_url":"https://api.github.com/users/JonathanHallKJR/gists{/gist_id}","starred_url":"https://api.github.com/users/JonathanHallKJR/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JonathanHallKJR/subscriptions","organizations_url":"https://api.github.com/users/JonathanHallKJR/orgs","repos_url":"https://api.github.com/users/JonathanHallKJR/repos","events_url":"https://api.github.com/users/JonathanHallKJR/events{/privacy}","received_events_url":"https://api.github.com/users/JonathanHallKJR/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-10-25T06:57:29Z","updated_at":"2019-08-15T07:09:43Z","closed_at":"2018-10-26T00:12:23Z","author_association":"NONE","body":"## Meta -\r\nOS: Windows 8.1\r\nSelenium Version: selenium-java latest (3.14.0)\r\nIEDriverServer Version(s) - Win32 3.14.0\r\nBrowser: Internet Explorer\r\nBrowser Version: 11.0.9600.19101\r\n\r\n## Expected Behavior -\r\n\r\nExpect the disappearance of the Modal Dialog to allow the test to continue, even if the application under test immediately opened a new Model Dialog.\r\n\r\n## Actual Behavior -\r\n\r\nClicking a button that closes the dialog eventually fails with a `TimeoutException: Timed out waiting for page to load`. \r\n\r\nLooking in the [IEServerlog.log](https://github.com/SeleniumHQ/selenium/files/2513650/IEServerlog.log) it repeats a loop around `HtmlDialog::Wait`\r\n\r\n```\r\niedriver\\iecommandexecutor.cpp(801) Creating wait thread with deferred response of `{ \"value\" : null }`\r\niedriver\\iecommandexecutor.cpp(558) Entering IECommandExecutor::WaitThreadProc\r\niedriver\\iecommandexecutor.cpp(181) Entering IECommandExecutor::OnWait\r\niedriver\\iecommandexecutor.cpp(890) Entering IECommandExecutor::GetCurrentBrowser\r\niedriver\\iecommandexecutor.cpp(896) Entering IECommandExecutor::GetManagedBrowser\r\niedriver\\htmldialog.cpp(126) Entering HtmlDialog::Wait\r\niedriver\\htmldialog.cpp(100) Entering HtmlDialog::IsValidWindow\r\niedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle\r\niedriver\\htmldialog.cpp(193) Entering HtmlDialog::GetActiveDialogWindowHandle\r\niedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle\r\niedriver\\iecommandexecutor.cpp(829) Entering IECommandExecutor::IsAlertActive\r\niedriver\\htmldialog.cpp(193) Entering HtmlDialog::GetActiveDialogWindowHandle\r\niedriver\\htmldialog.cpp(188) Entering HtmlDialog::GetTopLevelWindowHandle\r\niedriver\\iecommandexecutor.cpp(843) No alert handle is found\r\niedriver\\iecommandexecutor.cpp(801) Creating wait thread with deferred response of `{ \"value\" : null }`\r\n[...]\r\n```\r\n## Steps to reproduce -\r\n\r\nSee [Attached code](https://gist.github.com/JonathanHallKJR/adefa242cbdda98b30ba48ad82b4b3d3#file-seleniumbugreport-java-L51)\r\n\r\nWhile running it serves 3 pages:\r\n* http://localhost:8080/ - main Page with test button and textarea for log messages.\r\n* http://localhost:8080/popup/promptWindow - first dialog.\r\n* http://localhost:8080/popup/areYouSure - second dialog.\r\n\r\nExpected Flow would be:\r\n1. User clicks Test button on the main page.\r\n  * which opens a first dialog with `window.showModalDialog` passing a callback function in via `window.dialogArguments[0]`.\r\n2. User clicks a button on the first dialog. \r\n  * which triggers calls to `window.close()` then `callback(answer)`\r\n  * first callback logs the answer and opens the second modal dialog. \r\n  :warning: NOTE: When running in automation this is where it fails to detect the first dialog has gone.\r\n3. User clicks a button on the second dialog.\r\n  * which triggers calls to `window.close()` then `callback(answer)`\r\n  * second callback logs the answer.\r\n\r\n## Analysis\r\nOriginally discovered while testing a real application in which the automation would fail during a `driver.getWindowHandles()` (after the `element.click()`).  The IEDriverServer logs then showed a similar `HtmlDialog::Wait` loop above with a different deferred response.\r\n\r\n:warning: If you catch the TimeoutException and call `driver.getWindowHandles()` there are **three** window handles returned despite only two being visible to the user.\r\n\r\nIt may be possible to fix this in the application under test since the either of the following prevent replication (however the code as is works for humans and is only causing problems for automation).\r\n1. If the callback doesn't trigger a new `window.showModalDialog`.\r\n2. If the callback is called before `window.close()` (but two dialogs are open at the same time)  ","closed_by":{"login":"JonathanHallKJR","id":1574693,"node_id":"MDQ6VXNlcjE1NzQ2OTM=","avatar_url":"https://avatars2.githubusercontent.com/u/1574693?v=4","gravatar_id":"","url":"https://api.github.com/users/JonathanHallKJR","html_url":"https://github.com/JonathanHallKJR","followers_url":"https://api.github.com/users/JonathanHallKJR/followers","following_url":"https://api.github.com/users/JonathanHallKJR/following{/other_user}","gists_url":"https://api.github.com/users/JonathanHallKJR/gists{/gist_id}","starred_url":"https://api.github.com/users/JonathanHallKJR/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JonathanHallKJR/subscriptions","organizations_url":"https://api.github.com/users/JonathanHallKJR/orgs","repos_url":"https://api.github.com/users/JonathanHallKJR/repos","events_url":"https://api.github.com/users/JonathanHallKJR/events{/privacy}","received_events_url":"https://api.github.com/users/JonathanHallKJR/received_events","type":"User","site_admin":false}}", "commentIds":["433177637","433245838","433247201"], "labels":[]}