{"id":"1564", "title":"Selenium 2.50.0 in grid mode failing with an exception - 2.49.1 works fine", "body":"Hello everyone

I have just got 2.50.0 version and running it in grid mode (see configs below) each time I start my test I failed with below exception. Please note previous version 2.49.1 works perfectly fine.

**EXCEPTION**
`16:21:54.858 INFO [1] org.openqa.grid.selenium.GridLauncher - Launching a Selenium Grid node
16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - Writing debug logs to D:\\SELENIUM\\logs\\win7_node_config.log
16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - Java: Oracle Corporation 25.71-b15
16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - OS: Windows 7 6.1 amd64
16:21:55.237 INFO [1] org.openqa.selenium.server.SeleniumServer - v2.50.0, with Core v2.50.0. Built from revision 1070ace
16:21:55.266 INFO [1] org.openqa.selenium.remote.server.DefaultDriverProvider - Driver class not found: com.opera.core.systems.OperaDriver
16:21:55.267 INFO [1] org.openqa.selenium.remote.server.DefaultDriverFactory - Driver provider com.opera.core.systems.OperaDriver is not registered
16:21:55.270 INFO [1] org.openqa.selenium.remote.server.DefaultDriverSessions - Driver provider org.openqa.selenium.safari.SafariDriver registration is skipped:
registration capabilities Capabilities [{browserName=safari, version=, platform=MAC}] does not match the current platform VISTA
16:21:55.294 INFO [1] org.openqa.grid.selenium.GridLauncher - Selenium Grid node is up and ready to register to the hub
16:21:55.310 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - Starting auto registration thread. Will try to register every 5000 ms.
16:21:55.310 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - Registering the node to the hub: http://10.1.12.19:4444/grid/register
16:21:55.325 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - The node is registered to the hub and ready to use
16:22:02.578 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Executing: [new session: Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]])
16:22:02.584 INFO [24] org.openqa.selenium.remote.server.FirefoxDriverProvider - Creating a new session for Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]
16:22:08.197 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Done: [new session: Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]]
16:22:08.686 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Executing: [maximise window])
16:22:08.687 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Executing: [get: https://10.1.12.19/])
16:22:08.687 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Executing: [implicitly wait: 3000])
16:22:08.725 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Done: [maximise window]
16:22:10.327 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Executing: [delete session: b87a8bfe-9683-47a2-9368-392b3471e979])
16:22:10.949 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Done: [get: https://10.1.12.19/]
16:22:10.960 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Done: [implicitly wait: 3000]
16:22:10.964 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Executing: [find elements: By.xpath: //html[1]/body[1]/app[1]])
16:22:11.848 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Done: [delete session: b87a8bfe-9683-47a2-9368-392b3471e979]
16:22:12.122 WARN [16] org.openqa.selenium.remote.server.DriverServlet - Exception thrown
org.openqa.selenium.remote.SessionNotFoundException: The FirefoxDriver cannot be used after quit() was called.
Build info: version: '2.50.0', revision: '1070ace', time: '2016-01-27 18:43:24'
System info: host: 'PC-1063', ip: '10.1.12.19', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_71'
Driver info: driver.version: EventFiringWebDriver
    at org.openqa.selenium.firefox.FirefoxDriver$LazyCommandExecutor.execute(FirefoxDriver.java:377)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:644)
    at org.openqa.selenium.remote.RemoteWebDriver.findElements(RemoteWebDriver.java:388)
    at org.openqa.selenium.remote.RemoteWebDriver.findElementsByXPath(RemoteWebDriver.java:504)
    at org.openqa.selenium.By$ByXPath.findElements(By.java:356)
    at org.openqa.selenium.remote.RemoteWebDriver.findElements(RemoteWebDriver.java:351)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
    at java.lang.reflect.Method.invoke(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:103)
    at com.sun.proxy.$Proxy1.findElements(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver.findElements(EventFiringWebDriver.java:177)
    at org.openqa.selenium.remote.server.handler.FindElements.call(FindElements.java:50)
    at org.openqa.selenium.remote.server.handler.FindElements.call(FindElements.java:1)
    at java.util.concurrent.FutureTask.run(Unknown Source)
    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)
    at java.lang.Thread.run(Unknown Source)
16:22:12.124 WARN [16] org.openqa.selenium.remote.server.DriverServlet - Exception: The FirefoxDriver cannot be used after quit() was called.
Build info: version: '2.50.0', revision: '1070ace', time: '2016-01-27 18:43:24'
System info: host: 'PC-1063', ip: '10.1.12.19', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_71'
Driver info: driver.version: EventFiringWebDriver
16:22:18.912 INFO [18] org.openqa.selenium.server.SeleniumServer - Shutting down...
`
**HUB AND NODE**

> SET version=2.49.1
> start cmd /c \"java -jar %~dp0\\selenium_lib\\selenium-server-standalone-%version%.jar -role hub -hubConfig %~dp0\\configs\\hub_config.json
> 
> start cmd /c \"java -jar %~dp0\\selenium_lib\\selenium-server-standalone-%version%.jar -role webdriver -nodeConfig %~dp0\\configs\\win7_node_config.json -Dwebdriver.chrome.driver=%~dp0\\drivers\\chromedriver.exe -Dwebdriver.opera.driver=%~dp0\\drivers\\operadriver.exe -Dwebdriver.ie.driver=%~dp0\\drivers\\IEDriverServer.exe -log %~dp0\\logs\\win7_node_config.log\"

**HUB CONFIG**
`{
  \"host\": IP,
  \"port\": 4444,
  \"newSessionWaitTimeout\": -1,
  \"servlets\" : [],
  \"prioritizer\": null,
  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",
  \"throwOnCapabilityNotPresent\": true,
  \"nodePolling\": 5000,

  \"cleanUpCycle\": 5000,
  \"timeout\": 300000,
  \"maxSession\": 5
}`

**NODE CONFIG**
`{
  \"capabilities\":
      [
        {
          \"browserName\": \"firefox\",
          \"maxInstances\": 1,
          \"version\": 42,
          \"platform\": \"WINDOWS\",
          \"seleniumProtocol\": \"WebDriver\"
        },
        {
          \"browserName\": \"chrome\",
          \"maxInstances\": 1,
          \"version\": 47,
          \"platform\": \"WINDOWS\",
          \"seleniumProtocol\": \"WebDriver\"
        },
        {
          \"browserName\": \"opera\",
          \"maxInstances\": 1,
          \"version\": 12,
          \"platform\": \"WINDOWS\",
          \"seleniumProtocol\": \"WebDriver\"
        },
        {
          \"browserName\": \"internet explorer\",
          \"maxInstances\": 1,
          \"version\": 11,
          \"platform\": \"WINDOWS\",
          \"seleniumProtocol\": \"WebDriver\"
        }
      ],
  \"configuration\":
  {
    \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",
    \"maxSession\": 5,
    \"port\": 4455,
    \"host\": \"HOST IP\",
    \"register\": true,
    \"registerCycle\": 5000,
    \"hubPort\": 4444,
    \"hubHost\": \"HUB IP\"
  }
}`
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1564","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1564/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1564/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1564/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1564","id":129817533,"node_id":"MDU6SXNzdWUxMjk4MTc1MzM=","number":1564,"title":"Selenium 2.50.0 in grid mode failing with an exception - 2.49.1 works fine","user":{"login":"pi4r0n","id":3354914,"node_id":"MDQ6VXNlcjMzNTQ5MTQ=","avatar_url":"https://avatars2.githubusercontent.com/u/3354914?v=4","gravatar_id":"","url":"https://api.github.com/users/pi4r0n","html_url":"https://github.com/pi4r0n","followers_url":"https://api.github.com/users/pi4r0n/followers","following_url":"https://api.github.com/users/pi4r0n/following{/other_user}","gists_url":"https://api.github.com/users/pi4r0n/gists{/gist_id}","starred_url":"https://api.github.com/users/pi4r0n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/pi4r0n/subscriptions","organizations_url":"https://api.github.com/users/pi4r0n/orgs","repos_url":"https://api.github.com/users/pi4r0n/repos","events_url":"https://api.github.com/users/pi4r0n/events{/privacy}","received_events_url":"https://api.github.com/users/pi4r0n/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-01-29T16:33:10Z","updated_at":"2019-08-20T16:09:46Z","closed_at":"2016-01-30T18:27:40Z","author_association":"NONE","body":"Hello everyone\n\nI have just got 2.50.0 version and running it in grid mode (see configs below) each time I start my test I failed with below exception. Please note previous version 2.49.1 works perfectly fine.\n\n**EXCEPTION**\n`16:21:54.858 INFO [1] org.openqa.grid.selenium.GridLauncher - Launching a Selenium Grid node\n16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - Writing debug logs to D:\\SELENIUM\\logs\\win7_node_config.log\n16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - Java: Oracle Corporation 25.71-b15\n16:21:55.234 INFO [1] org.openqa.selenium.server.SeleniumServer - OS: Windows 7 6.1 amd64\n16:21:55.237 INFO [1] org.openqa.selenium.server.SeleniumServer - v2.50.0, with Core v2.50.0. Built from revision 1070ace\n16:21:55.266 INFO [1] org.openqa.selenium.remote.server.DefaultDriverProvider - Driver class not found: com.opera.core.systems.OperaDriver\n16:21:55.267 INFO [1] org.openqa.selenium.remote.server.DefaultDriverFactory - Driver provider com.opera.core.systems.OperaDriver is not registered\n16:21:55.270 INFO [1] org.openqa.selenium.remote.server.DefaultDriverSessions - Driver provider org.openqa.selenium.safari.SafariDriver registration is skipped:\nregistration capabilities Capabilities [{browserName=safari, version=, platform=MAC}] does not match the current platform VISTA\n16:21:55.294 INFO [1] org.openqa.grid.selenium.GridLauncher - Selenium Grid node is up and ready to register to the hub\n16:21:55.310 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - Starting auto registration thread. Will try to register every 5000 ms.\n16:21:55.310 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - Registering the node to the hub: http://10.1.12.19:4444/grid/register\n16:21:55.325 INFO [19] org.openqa.grid.internal.utils.SelfRegisteringRemote - The node is registered to the hub and ready to use\n16:22:02.578 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Executing: [new session: Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]])\n16:22:02.584 INFO [24] org.openqa.selenium.remote.server.FirefoxDriverProvider - Creating a new session for Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]\n16:22:08.197 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Done: [new session: Capabilities [{rotatable=true, locationContextEnabled=true, loggingPrefs=org.openqa.selenium.logging.LoggingPreferences@2c24f7b3, browserName=firefox, javascriptEnabled=true, handlesAlerts=true, version=, platform=ANY, requestOrigins={name=webdriverio, version=3.4.0, url=http://webdriver.io}}]]\n16:22:08.686 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Executing: [maximise window])\n16:22:08.687 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Executing: [get: https://10.1.12.19/])\n16:22:08.687 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Executing: [implicitly wait: 3000])\n16:22:08.725 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Done: [maximise window]\n16:22:10.327 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Executing: [delete session: b87a8bfe-9683-47a2-9368-392b3471e979])\n16:22:10.949 INFO [17] org.openqa.selenium.remote.server.DriverServlet - Done: [get: https://10.1.12.19/]\n16:22:10.960 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Done: [implicitly wait: 3000]\n16:22:10.964 INFO [16] org.openqa.selenium.remote.server.DriverServlet - Executing: [find elements: By.xpath: //html[1]/body[1]/app[1]])\n16:22:11.848 INFO [28] org.openqa.selenium.remote.server.DriverServlet - Done: [delete session: b87a8bfe-9683-47a2-9368-392b3471e979]\n16:22:12.122 WARN [16] org.openqa.selenium.remote.server.DriverServlet - Exception thrown\norg.openqa.selenium.remote.SessionNotFoundException: The FirefoxDriver cannot be used after quit() was called.\nBuild info: version: '2.50.0', revision: '1070ace', time: '2016-01-27 18:43:24'\nSystem info: host: 'PC-1063', ip: '10.1.12.19', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_71'\nDriver info: driver.version: EventFiringWebDriver\n    at org.openqa.selenium.firefox.FirefoxDriver$LazyCommandExecutor.execute(FirefoxDriver.java:377)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:644)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElements(RemoteWebDriver.java:388)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElementsByXPath(RemoteWebDriver.java:504)\n    at org.openqa.selenium.By$ByXPath.findElements(By.java:356)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElements(RemoteWebDriver.java:351)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\n    at java.lang.reflect.Method.invoke(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:103)\n    at com.sun.proxy.$Proxy1.findElements(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver.findElements(EventFiringWebDriver.java:177)\n    at org.openqa.selenium.remote.server.handler.FindElements.call(FindElements.java:50)\n    at org.openqa.selenium.remote.server.handler.FindElements.call(FindElements.java:1)\n    at java.util.concurrent.FutureTask.run(Unknown Source)\n    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)\n    at java.lang.Thread.run(Unknown Source)\n16:22:12.124 WARN [16] org.openqa.selenium.remote.server.DriverServlet - Exception: The FirefoxDriver cannot be used after quit() was called.\nBuild info: version: '2.50.0', revision: '1070ace', time: '2016-01-27 18:43:24'\nSystem info: host: 'PC-1063', ip: '10.1.12.19', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_71'\nDriver info: driver.version: EventFiringWebDriver\n16:22:18.912 INFO [18] org.openqa.selenium.server.SeleniumServer - Shutting down...\n`\n**HUB AND NODE**\n\n> SET version=2.49.1\n> start cmd /c \"java -jar %~dp0\\selenium_lib\\selenium-server-standalone-%version%.jar -role hub -hubConfig %~dp0\\configs\\hub_config.json\n> \n> start cmd /c \"java -jar %~dp0\\selenium_lib\\selenium-server-standalone-%version%.jar -role webdriver -nodeConfig %~dp0\\configs\\win7_node_config.json -Dwebdriver.chrome.driver=%~dp0\\drivers\\chromedriver.exe -Dwebdriver.opera.driver=%~dp0\\drivers\\operadriver.exe -Dwebdriver.ie.driver=%~dp0\\drivers\\IEDriverServer.exe -log %~dp0\\logs\\win7_node_config.log\"\n\n**HUB CONFIG**\n`{\n  \"host\": IP,\n  \"port\": 4444,\n  \"newSessionWaitTimeout\": -1,\n  \"servlets\" : [],\n  \"prioritizer\": null,\n  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",\n  \"throwOnCapabilityNotPresent\": true,\n  \"nodePolling\": 5000,\n\n  \"cleanUpCycle\": 5000,\n  \"timeout\": 300000,\n  \"maxSession\": 5\n}`\n\n**NODE CONFIG**\n`{\n  \"capabilities\":\n      [\n        {\n          \"browserName\": \"firefox\",\n          \"maxInstances\": 1,\n          \"version\": 42,\n          \"platform\": \"WINDOWS\",\n          \"seleniumProtocol\": \"WebDriver\"\n        },\n        {\n          \"browserName\": \"chrome\",\n          \"maxInstances\": 1,\n          \"version\": 47,\n          \"platform\": \"WINDOWS\",\n          \"seleniumProtocol\": \"WebDriver\"\n        },\n        {\n          \"browserName\": \"opera\",\n          \"maxInstances\": 1,\n          \"version\": 12,\n          \"platform\": \"WINDOWS\",\n          \"seleniumProtocol\": \"WebDriver\"\n        },\n        {\n          \"browserName\": \"internet explorer\",\n          \"maxInstances\": 1,\n          \"version\": 11,\n          \"platform\": \"WINDOWS\",\n          \"seleniumProtocol\": \"WebDriver\"\n        }\n      ],\n  \"configuration\":\n  {\n    \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",\n    \"maxSession\": 5,\n    \"port\": 4455,\n    \"host\": \"HOST IP\",\n    \"register\": true,\n    \"registerCycle\": 5000,\n    \"hubPort\": 4444,\n    \"hubHost\": \"HUB IP\"\n  }\n}`\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["177268050"], "labels":[]}