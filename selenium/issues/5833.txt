{"id":"5833", "title":"java-client's NewSessionPayload.class conflict with selenium's NewSessionPayload.class", "body":"OS: Windows 10
Selenium Version: 3.9.1
Browser: Firefox
Browser Version: 59.0.2 (64-bit) 

## Expected Behavior
- When start Firefox driver will call class NewSessionPageload.java of selenium instead call NewSessionPageload.java of java-client.
## Actual Behavior
- When start Firefox driver  called class NewSessionPageload.java of java-client  instead call NewSessionPageload.java of selenium.
- Error message.
**Exception in thread \"main\" org.openqa.selenium.InvalidArgumentException: firefox_profile is not the name of a known capability or extension capability**

## Steps to reproduce 

- Please run source code to reproduce :
```

   public class FirefoxTest {
	public static void main(String[] args) {
         // declaration and instantiation of objects/variables
        System.setProperty(\"webdriver.gecko.driver\", \"/path/to/geckodriver_win64.exe\");          
        FirefoxProfile ffprofile = new FirefoxProfile();
    	FirefoxOptions firefoxOptions = new FirefoxOptions();
    	firefoxOptions.setProfile(ffprofile);
	WebDriver driver = new FirefoxDriver(firefoxOptions);
        String baseUrl = \"http://demo.guru99.com/test/newtours/\";
        String expectedTitle = \"Welcome: Mercury Tours\";
        String actualTitle = \"\";
        driver.get(baseUrl);
        // get the actual value of the title
        actualTitle = driver.getTitle();
        /*
         * compare the actual title of the page with the expected one and print
         * the result as \"Passed\" or \"Failed\"
         */
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println(\"Test Passed!\");
        } else {
            System.out.println(\"Test Failed\");
        }     
        //close Fire fox
         driver.close();
           }
       }
```
------------------------------------------pom.xml file------------------------------------------------------

          <?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<project xmlns=\"http://maven.apache.org/POM/4.0.0\" 
        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
	xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">
	<modelVersion>4.0.0</modelVersion>
	<groupId>DemoFirefoxDriver</groupId>
	<artifactId>DemoFirefoxDriver</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<properties>
		<selenium.webdriver.version>3.9.1</selenium.webdriver.version>
	</properties>
	<build>
		<plugins>
			<plugin>
				<!-- Build an executable JAR -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>io.appium</groupId>
			<artifactId>java-client</artifactId>
			<version>6.0.0-BETA3</version>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>${selenium.webdriver.version}</version>
			<exclusions>
				<exclusion> <!-- declare the exclusion here -->
					<groupId>org.apache.httpcomponents</groupId>
					<artifactId>httpclient</artifactId>
				</exclusion>
				<exclusion>
					<groupId>xalan</groupId>
					<artifactId>xalan</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
                <dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>${selenium.webdriver.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.seleniumhq.selenium</groupId>
					<artifactId>selenium-firefox-driver</artifactId>
				</exclusion>
				<exclusion>
					<groupId>xalan</groupId>
					<artifactId>xalan</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-firefox-driver -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-firefox-driver</artifactId>
			<version>${selenium.webdriver.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-ie-driver -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-ie-driver</artifactId>
			<version>${selenium.webdriver.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-chrome-driver -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-chrome-driver</artifactId>
			<version>${selenium.webdriver.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-edge-driver -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-edge-driver</artifactId>
			<version>${selenium.webdriver.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-safari-driver -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-safari-driver</artifactId>
			<version>${selenium.webdriver.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>24.0-jre</version>
		</dependency>
	</dependencies>
</project>

-------------------------------------------------------------------------------------------------------------------
- This is a bug of selenium?
- Do Anyone have the workaround for this issue? Please advice me. Thank in advance!", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5833","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5833/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5833/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5833/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5833","id":317963646,"node_id":"MDU6SXNzdWUzMTc5NjM2NDY=","number":5833,"title":"java-client's NewSessionPayload.class conflict with selenium's NewSessionPayload.class","user":{"login":"Tenit2012","id":13044731,"node_id":"MDQ6VXNlcjEzMDQ0NzMx","avatar_url":"https://avatars0.githubusercontent.com/u/13044731?v=4","gravatar_id":"","url":"https://api.github.com/users/Tenit2012","html_url":"https://github.com/Tenit2012","followers_url":"https://api.github.com/users/Tenit2012/followers","following_url":"https://api.github.com/users/Tenit2012/following{/other_user}","gists_url":"https://api.github.com/users/Tenit2012/gists{/gist_id}","starred_url":"https://api.github.com/users/Tenit2012/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Tenit2012/subscriptions","organizations_url":"https://api.github.com/users/Tenit2012/orgs","repos_url":"https://api.github.com/users/Tenit2012/repos","events_url":"https://api.github.com/users/Tenit2012/events{/privacy}","received_events_url":"https://api.github.com/users/Tenit2012/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-04-26T10:13:11Z","updated_at":"2019-08-16T04:09:49Z","closed_at":"2018-04-26T21:17:53Z","author_association":"NONE","body":"OS: Windows 10\r\nSelenium Version: 3.9.1\r\nBrowser: Firefox\r\nBrowser Version: 59.0.2 (64-bit) \r\n\r\n## Expected Behavior\r\n- When start Firefox driver will call class NewSessionPageload.java of selenium instead call NewSessionPageload.java of java-client.\r\n## Actual Behavior\r\n- When start Firefox driver  called class NewSessionPageload.java of java-client  instead call NewSessionPageload.java of selenium.\r\n- Error message.\r\n**Exception in thread \"main\" org.openqa.selenium.InvalidArgumentException: firefox_profile is not the name of a known capability or extension capability**\r\n\r\n## Steps to reproduce \r\n\r\n- Please run source code to reproduce :\r\n```\r\n\r\n   public class FirefoxTest {\r\n\tpublic static void main(String[] args) {\r\n         // declaration and instantiation of objects/variables\r\n        System.setProperty(\"webdriver.gecko.driver\", \"/path/to/geckodriver_win64.exe\");          \r\n        FirefoxProfile ffprofile = new FirefoxProfile();\r\n    \tFirefoxOptions firefoxOptions = new FirefoxOptions();\r\n    \tfirefoxOptions.setProfile(ffprofile);\r\n\tWebDriver driver = new FirefoxDriver(firefoxOptions);\r\n        String baseUrl = \"http://demo.guru99.com/test/newtours/\";\r\n        String expectedTitle = \"Welcome: Mercury Tours\";\r\n        String actualTitle = \"\";\r\n        driver.get(baseUrl);\r\n        // get the actual value of the title\r\n        actualTitle = driver.getTitle();\r\n        /*\r\n         * compare the actual title of the page with the expected one and print\r\n         * the result as \"Passed\" or \"Failed\"\r\n         */\r\n        if (actualTitle.contentEquals(expectedTitle)){\r\n            System.out.println(\"Test Passed!\");\r\n        } else {\r\n            System.out.println(\"Test Failed\");\r\n        }     \r\n        //close Fire fox\r\n         driver.close();\r\n           }\r\n       }\r\n```\r\n------------------------------------------pom.xml file------------------------------------------------------\r\n\r\n          <?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\t<project xmlns=\"http://maven.apache.org/POM/4.0.0\" \r\n        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n\txsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">\r\n\t<modelVersion>4.0.0</modelVersion>\r\n\t<groupId>DemoFirefoxDriver</groupId>\r\n\t<artifactId>DemoFirefoxDriver</artifactId>\r\n\t<version>0.0.1-SNAPSHOT</version>\r\n\t<properties>\r\n\t\t<selenium.webdriver.version>3.9.1</selenium.webdriver.version>\r\n\t</properties>\r\n\t<build>\r\n\t\t<plugins>\r\n\t\t\t<plugin>\r\n\t\t\t\t<!-- Build an executable JAR -->\r\n\t\t\t\t<groupId>org.apache.maven.plugins</groupId>\r\n\t\t\t\t<artifactId>maven-jar-plugin</artifactId>\r\n\t\t\t</plugin>\r\n\t\t</plugins>\r\n\t</build>\r\n\t<dependencies>\r\n\t\t<dependency>\r\n\t\t\t<groupId>io.appium</groupId>\r\n\t\t\t<artifactId>java-client</artifactId>\r\n\t\t\t<version>6.0.0-BETA3</version>\r\n\t\t</dependency>\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-server</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t\t<exclusions>\r\n\t\t\t\t<exclusion> <!-- declare the exclusion here -->\r\n\t\t\t\t\t<groupId>org.apache.httpcomponents</groupId>\r\n\t\t\t\t\t<artifactId>httpclient</artifactId>\r\n\t\t\t\t</exclusion>\r\n\t\t\t\t<exclusion>\r\n\t\t\t\t\t<groupId>xalan</groupId>\r\n\t\t\t\t\t<artifactId>xalan</artifactId>\r\n\t\t\t\t</exclusion>\r\n\t\t\t</exclusions>\r\n\t\t</dependency>\r\n                <dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-java</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t\t<exclusions>\r\n\t\t\t\t<exclusion>\r\n\t\t\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t\t\t<artifactId>selenium-firefox-driver</artifactId>\r\n\t\t\t\t</exclusion>\r\n\t\t\t\t<exclusion>\r\n\t\t\t\t\t<groupId>xalan</groupId>\r\n\t\t\t\t\t<artifactId>xalan</artifactId>\r\n\t\t\t\t</exclusion>\r\n\t\t\t</exclusions>\r\n\t\t</dependency>\r\n\t\t<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-firefox-driver -->\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-firefox-driver</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t</dependency>\r\n\r\n\t\t<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-ie-driver -->\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-ie-driver</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t</dependency>\r\n\r\n\t\t<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-chrome-driver -->\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-chrome-driver</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t</dependency>\r\n\r\n\t\t<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-edge-driver -->\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-edge-driver</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t</dependency>\r\n\r\n\t\t<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-safari-driver -->\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.seleniumhq.selenium</groupId>\r\n\t\t\t<artifactId>selenium-safari-driver</artifactId>\r\n\t\t\t<version>${selenium.webdriver.version}</version>\r\n\t\t</dependency>\r\n\t\t<dependency>\r\n\t\t\t<groupId>com.google.guava</groupId>\r\n\t\t\t<artifactId>guava</artifactId>\r\n\t\t\t<version>24.0-jre</version>\r\n\t\t</dependency>\r\n\t</dependencies>\r\n</project>\r\n\r\n-------------------------------------------------------------------------------------------------------------------\r\n- This is a bug of selenium?\r\n- Do Anyone have the workaround for this issue? Please advice me. Thank in advance!","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["384791945"], "labels":[]}