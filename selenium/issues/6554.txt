{"id":"6554", "title":"Unable to browser.manage().window().maximize with Chrome under macOS High Sierra", "body":"OS:
macOS High Sierra 10.13.6

Selenium Version:
3.14.0

Browser:
Chrome 70.0.3538.67 (Official Build)

Expected Behavior - 
When test starts, maximize the browser window.

Actual Behavior - 
Chrome never maximizes nor changes size beyond the 'default' window size that the application shows when initially installed.  Even after launching Chrome manually and resizing the window to be larger (which is remembered during subsequent restarts), the Chrome window is still the smaller size at start.

When the test fails, here is the output:
    Failed: unknown error: failed to change window state to maximized, current state is normal
      (Session info: chrome=70.0.3538.67)
      (Driver info: chromedriver=2.43.600229 (3fae4d0cda5334b4f533bede5a4787f7b832d052),platform=Mac OS X 10.13.6 x86_64)

  Stack:
    WebDriverError: unknown error: failed to change window state to maximized, current state is normal
      (Session info: chrome=70.0.3538.67)
      (Driver info: chromedriver=2.43.600229 (3fae4d0cda5334b4f533bede5a4787f7b832d052),platform=Mac OS X 10.13.6 x86_64)
        at Object.checkLegacyResponse (/pathToMyCode/node_modules/selenium-webdriver/lib/error.js:546:15)
        at parseHttpResponse (/pathToMyCode/node_modules/selenium-webdriver/lib/http.js:509:13)
        at doSend.then.response (/pathToMyCode/node_modules/selenium-webdriver/lib/http.js:441:30)
        at <anonymous>
        at process._tickCallback (internal/process/next_tick.js:188:7)
    From: Task: WebDriver.manage().window().maximize()
        at thenableWebDriverProxy.schedule (/pathToMyCode/node_modules/selenium-webdriver/lib/webdriver.js:807:17)
        at Window.maximize (/pathToMyCode/node_modules/selenium-webdriver/lib/webdriver.js:1686:25)
        at UserContext.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:194:35)
        at /pathToMyCode/node_modules/jasminewd2/index.js:112:25
        at new ManagedPromise (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:1077:7)
        at ControlFlow.promise (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:2505:12)
        at schedulerExecute (/pathToMyCode/node_modules/jasminewd2/index.js:95:18)
        at TaskQueue.execute_ (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:3084:14)
        at TaskQueue.executeNext_ (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:3067:27)
        at asyncRun (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:2974:25)
    From: Task: Run beforeAll in control flow
        at UserContext.<anonymous> (/pathToMyCode/node_modules/jasminewd2/index.js:94:19)
    From asynchronous test: 
    Error
        at Suite.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:193:5)
        at Object.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:186:1)
        at Module._compile (module.js:573:30)
        at Object.Module._extensions..js (module.js:584:10)
        at Module.load (module.js:507:32)
        at tryModuleLoad (module.js:470:12)

Attempted Workarounds - 
I tried adding '--start-fullscreen' to the chromeOptions/args parameter but when Chrome launches it goes full screen for a second and then resizes itself back to the smaller window.  The only resize that is going on (which isn't occurring) is in the beforeAll function and it is just this:
browser.manage().window().maximize();

I have confirmed that the maximize does work fine on Chrome 70 under CentOS 7 and Windows 10 so the problem does appear to be OS X specific.

Obviously I wonder if this is the type of thing that may be related to 70 having just come out over the last couple of days but not being 100% sure of that fact, I figured I'd go ahead and report it anyway just in case its an indicator of a bigger problem.

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6554","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6554/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6554/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6554/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6554","id":371620050,"node_id":"MDU6SXNzdWUzNzE2MjAwNTA=","number":6554,"title":"Unable to browser.manage().window().maximize with Chrome under macOS High Sierra","user":{"login":"cwbuege","id":5445610,"node_id":"MDQ6VXNlcjU0NDU2MTA=","avatar_url":"https://avatars0.githubusercontent.com/u/5445610?v=4","gravatar_id":"","url":"https://api.github.com/users/cwbuege","html_url":"https://github.com/cwbuege","followers_url":"https://api.github.com/users/cwbuege/followers","following_url":"https://api.github.com/users/cwbuege/following{/other_user}","gists_url":"https://api.github.com/users/cwbuege/gists{/gist_id}","starred_url":"https://api.github.com/users/cwbuege/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cwbuege/subscriptions","organizations_url":"https://api.github.com/users/cwbuege/orgs","repos_url":"https://api.github.com/users/cwbuege/repos","events_url":"https://api.github.com/users/cwbuege/events{/privacy}","received_events_url":"https://api.github.com/users/cwbuege/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-10-18T16:50:04Z","updated_at":"2019-08-15T07:10:03Z","closed_at":"2018-10-18T16:51:51Z","author_association":"NONE","body":"OS:\r\nmacOS High Sierra 10.13.6\r\n\r\nSelenium Version:\r\n3.14.0\r\n\r\nBrowser:\r\nChrome 70.0.3538.67 (Official Build)\r\n\r\nExpected Behavior - \r\nWhen test starts, maximize the browser window.\r\n\r\nActual Behavior - \r\nChrome never maximizes nor changes size beyond the 'default' window size that the application shows when initially installed.  Even after launching Chrome manually and resizing the window to be larger (which is remembered during subsequent restarts), the Chrome window is still the smaller size at start.\r\n\r\nWhen the test fails, here is the output:\r\n    Failed: unknown error: failed to change window state to maximized, current state is normal\r\n      (Session info: chrome=70.0.3538.67)\r\n      (Driver info: chromedriver=2.43.600229 (3fae4d0cda5334b4f533bede5a4787f7b832d052),platform=Mac OS X 10.13.6 x86_64)\r\n\r\n  Stack:\r\n    WebDriverError: unknown error: failed to change window state to maximized, current state is normal\r\n      (Session info: chrome=70.0.3538.67)\r\n      (Driver info: chromedriver=2.43.600229 (3fae4d0cda5334b4f533bede5a4787f7b832d052),platform=Mac OS X 10.13.6 x86_64)\r\n        at Object.checkLegacyResponse (/pathToMyCode/node_modules/selenium-webdriver/lib/error.js:546:15)\r\n        at parseHttpResponse (/pathToMyCode/node_modules/selenium-webdriver/lib/http.js:509:13)\r\n        at doSend.then.response (/pathToMyCode/node_modules/selenium-webdriver/lib/http.js:441:30)\r\n        at <anonymous>\r\n        at process._tickCallback (internal/process/next_tick.js:188:7)\r\n    From: Task: WebDriver.manage().window().maximize()\r\n        at thenableWebDriverProxy.schedule (/pathToMyCode/node_modules/selenium-webdriver/lib/webdriver.js:807:17)\r\n        at Window.maximize (/pathToMyCode/node_modules/selenium-webdriver/lib/webdriver.js:1686:25)\r\n        at UserContext.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:194:35)\r\n        at /pathToMyCode/node_modules/jasminewd2/index.js:112:25\r\n        at new ManagedPromise (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:1077:7)\r\n        at ControlFlow.promise (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:2505:12)\r\n        at schedulerExecute (/pathToMyCode/node_modules/jasminewd2/index.js:95:18)\r\n        at TaskQueue.execute_ (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:3084:14)\r\n        at TaskQueue.executeNext_ (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:3067:27)\r\n        at asyncRun (/pathToMyCode/node_modules/selenium-webdriver/lib/promise.js:2974:25)\r\n    From: Task: Run beforeAll in control flow\r\n        at UserContext.<anonymous> (/pathToMyCode/node_modules/jasminewd2/index.js:94:19)\r\n    From asynchronous test: \r\n    Error\r\n        at Suite.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:193:5)\r\n        at Object.<anonymous> (/pathToMyCode/specs/smoke_test_spec.js:186:1)\r\n        at Module._compile (module.js:573:30)\r\n        at Object.Module._extensions..js (module.js:584:10)\r\n        at Module.load (module.js:507:32)\r\n        at tryModuleLoad (module.js:470:12)\r\n\r\nAttempted Workarounds - \r\nI tried adding '--start-fullscreen' to the chromeOptions/args parameter but when Chrome launches it goes full screen for a second and then resizes itself back to the smaller window.  The only resize that is going on (which isn't occurring) is in the beforeAll function and it is just this:\r\nbrowser.manage().window().maximize();\r\n\r\nI have confirmed that the maximize does work fine on Chrome 70 under CentOS 7 and Windows 10 so the problem does appear to be OS X specific.\r\n\r\nObviously I wonder if this is the type of thing that may be related to 70 having just come out over the last couple of days but not being 100% sure of that fact, I figured I'd go ahead and report it anyway just in case its an indicator of a bigger problem.\r\n\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["431083018"], "labels":[]}