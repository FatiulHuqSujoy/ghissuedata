{"id":"6003", "title":"Selenium WebDriver java - SPA Problem", "body":"## Meta -
OS:  
<!-- Windows 10? OSX? -->
Selenium Version:  3.12.0
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome, Firefox, Edge, IE
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  latest
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Description -
I'm doing my automated testing using java on Intellij IDEA.
I'm working on an SPA(single page application).
During one of my tests where I was trying to click on a found element, it seemed not to able to find the element and press on it which made it send back an exception from the next command line because it could get to the new pop up box where the next element exists.
It seems like it skips on this line of code...
I tried to make it wait until the driver would locate the element, also explicit wait and Thread.sleep but nothing seemed to work.
I was wondering maybe selenium webdriver isn't what I should use on a SPA.
I can't understand what's wrong about it! any help would be great.

## The Bug -
org.openqa.selenium.WebDriverException: unknown error: Element </div _ngcontent-c24=\"\" class=\"to-box\" style=\"cursor:pointer\">...</div> is not clickable at point (888, 400). Other element would receive the click: <input _ngcontent-c24=\"\" class=\"spy ng-untouched ng-pristine ng-valid\" name=\"subject\" placeholder=\"Subject\" type=\"text\" ng-reflect-name=\"subject\" ng-reflect-model=\"\"/>
  (Session info: chrome=66.0.3359.181)

The '/' at the start of the < and at the end of it is so it would be shown.
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6003","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6003/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6003/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6003/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6003","id":330414726,"node_id":"MDU6SXNzdWUzMzA0MTQ3MjY=","number":6003,"title":"Selenium WebDriver java - SPA Problem","user":{"login":"YakirSA","id":40040604,"node_id":"MDQ6VXNlcjQwMDQwNjA0","avatar_url":"https://avatars1.githubusercontent.com/u/40040604?v=4","gravatar_id":"","url":"https://api.github.com/users/YakirSA","html_url":"https://github.com/YakirSA","followers_url":"https://api.github.com/users/YakirSA/followers","following_url":"https://api.github.com/users/YakirSA/following{/other_user}","gists_url":"https://api.github.com/users/YakirSA/gists{/gist_id}","starred_url":"https://api.github.com/users/YakirSA/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/YakirSA/subscriptions","organizations_url":"https://api.github.com/users/YakirSA/orgs","repos_url":"https://api.github.com/users/YakirSA/repos","events_url":"https://api.github.com/users/YakirSA/events{/privacy}","received_events_url":"https://api.github.com/users/YakirSA/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-06-07T19:59:41Z","updated_at":"2019-08-15T23:09:50Z","closed_at":"2018-06-08T03:46:14Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.12.0\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome, Firefox, Edge, IE\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  latest\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Description -\r\nI'm doing my automated testing using java on Intellij IDEA.\r\nI'm working on an SPA(single page application).\r\nDuring one of my tests where I was trying to click on a found element, it seemed not to able to find the element and press on it which made it send back an exception from the next command line because it could get to the new pop up box where the next element exists.\r\nIt seems like it skips on this line of code...\r\nI tried to make it wait until the driver would locate the element, also explicit wait and Thread.sleep but nothing seemed to work.\r\nI was wondering maybe selenium webdriver isn't what I should use on a SPA.\r\nI can't understand what's wrong about it! any help would be great.\r\n\r\n## The Bug -\r\norg.openqa.selenium.WebDriverException: unknown error: Element </div _ngcontent-c24=\"\" class=\"to-box\" style=\"cursor:pointer\">...</div> is not clickable at point (888, 400). Other element would receive the click: <input _ngcontent-c24=\"\" class=\"spy ng-untouched ng-pristine ng-valid\" name=\"subject\" placeholder=\"Subject\" type=\"text\" ng-reflect-name=\"subject\" ng-reflect-model=\"\"/>\r\n  (Session info: chrome=66.0.3359.181)\r\n\r\nThe '/' at the start of the < and at the end of it is so it would be shown.\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["395637632"], "labels":[]}