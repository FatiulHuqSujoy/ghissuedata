{"id":"6478", "title":"Issue number of item during checkout", "body":"hello I would like to ask you a question please

on a site I extract all titles, prices, cities, categories. On the page there are 34 titles, prices etc.  Except that at times on some page the price element does not have 34 elements but 10 in my loop for x in range (len (title)): I have an indexing error but I would like to know how to get around this because I need to export to a csv file
I use python, thanks you

`for x in range(len(bot.titre())):

			if \"Aujourd'hui, 21:45\" in bot.date()[x].text:

				break
			else:

				

				global d
				

				ligne = feuil1.row(d)
				ligne2 = feuil1.row(d)
				ligne3 = feuil1.row(d)
				ligne4 = feuil1.row(d)
				ligne5 = feuil1.row(d)
				
				
				

				d = d + 1
				
				ligne.write(0,bot.titre()[x].text)
				
				

				**_ligne2.write(1,bot.prix()[x].text)_** 
					

				ligne3.write(2,bot.ville()[x].text)
				
				ligne4.write(3,bot.date()[x].text)
			
				ligne5.write(4,bot.categorie()[x].text)



				feuil1.col(0).width = 15000`

As you can see, I have a problem with the bot.price () function because it is this element that is sometimes missing from the list and gives an index error.", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6478","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6478/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6478/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6478/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6478","id":366422417,"node_id":"MDU6SXNzdWUzNjY0MjI0MTc=","number":6478,"title":"Issue number of item during checkout","user":{"login":"najbot","id":31626063,"node_id":"MDQ6VXNlcjMxNjI2MDYz","avatar_url":"https://avatars3.githubusercontent.com/u/31626063?v=4","gravatar_id":"","url":"https://api.github.com/users/najbot","html_url":"https://github.com/najbot","followers_url":"https://api.github.com/users/najbot/followers","following_url":"https://api.github.com/users/najbot/following{/other_user}","gists_url":"https://api.github.com/users/najbot/gists{/gist_id}","starred_url":"https://api.github.com/users/najbot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/najbot/subscriptions","organizations_url":"https://api.github.com/users/najbot/orgs","repos_url":"https://api.github.com/users/najbot/repos","events_url":"https://api.github.com/users/najbot/events{/privacy}","received_events_url":"https://api.github.com/users/najbot/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-10-03T16:25:45Z","updated_at":"2019-08-15T09:09:35Z","closed_at":"2018-10-03T16:27:56Z","author_association":"NONE","body":"hello I would like to ask you a question please\r\n\r\non a site I extract all titles, prices, cities, categories. On the page there are 34 titles, prices etc.  Except that at times on some page the price element does not have 34 elements but 10 in my loop for x in range (len (title)): I have an indexing error but I would like to know how to get around this because I need to export to a csv file\r\nI use python, thanks you\r\n\r\n`for x in range(len(bot.titre())):\r\n\r\n\t\t\tif \"Aujourd'hui, 21:45\" in bot.date()[x].text:\r\n\r\n\t\t\t\tbreak\r\n\t\t\telse:\r\n\r\n\t\t\t\t\r\n\r\n\t\t\t\tglobal d\r\n\t\t\t\t\r\n\r\n\t\t\t\tligne = feuil1.row(d)\r\n\t\t\t\tligne2 = feuil1.row(d)\r\n\t\t\t\tligne3 = feuil1.row(d)\r\n\t\t\t\tligne4 = feuil1.row(d)\r\n\t\t\t\tligne5 = feuil1.row(d)\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t\r\n\r\n\t\t\t\td = d + 1\r\n\t\t\t\t\r\n\t\t\t\tligne.write(0,bot.titre()[x].text)\r\n\t\t\t\t\r\n\t\t\t\t\r\n\r\n\t\t\t\t**_ligne2.write(1,bot.prix()[x].text)_** \r\n\t\t\t\t\t\r\n\r\n\t\t\t\tligne3.write(2,bot.ville()[x].text)\r\n\t\t\t\t\r\n\t\t\t\tligne4.write(3,bot.date()[x].text)\r\n\t\t\t\r\n\t\t\t\tligne5.write(4,bot.categorie()[x].text)\r\n\r\n\r\n\r\n\t\t\t\tfeuil1.col(0).width = 15000`\r\n\r\nAs you can see, I have a problem with the bot.price () function because it is this element that is sometimes missing from the list and gives an index error.","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["426703456","426707919"], "labels":[]}