{"id":"6391", "title":"No space symbols after send_keys (on Debian OS)", "body":"## Meta -
OS:  Xfce 4.12
<!-- Windows 10? OSX? -->
Selenium Version:  3.14
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  69.0.3497.81 (64-bit)
Chrome driver Version: 2.41
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
Entered text with space symbols
## Actual Behavior -
Entered text is without space symbols
## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

> from selenium import webdriver
> from selenium.webdriver.common.keys import Keys
> txt = \"hello! This is robot\"
> driver = webdriver.Chrome()
> driver.get(\"http://www.python.org\")
> elem = driver.find_element_by_id(\"id-search-field\")
> elem.send_keys(txt)
> driver.save_screenshot('screenie.png')
> driver.close()

NOTE: with Firefox GeckoDriver - space symbols are typed and displayed correctly. 
NOTE: with same Chrome driver, but on Ubuntu/Mint - no problem", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6391","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6391/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6391/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6391/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6391","id":358899373,"node_id":"MDU6SXNzdWUzNTg4OTkzNzM=","number":6391,"title":"No space symbols after send_keys (on Debian OS)","user":{"login":"chevi","id":1729590,"node_id":"MDQ6VXNlcjE3Mjk1OTA=","avatar_url":"https://avatars0.githubusercontent.com/u/1729590?v=4","gravatar_id":"","url":"https://api.github.com/users/chevi","html_url":"https://github.com/chevi","followers_url":"https://api.github.com/users/chevi/followers","following_url":"https://api.github.com/users/chevi/following{/other_user}","gists_url":"https://api.github.com/users/chevi/gists{/gist_id}","starred_url":"https://api.github.com/users/chevi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/chevi/subscriptions","organizations_url":"https://api.github.com/users/chevi/orgs","repos_url":"https://api.github.com/users/chevi/repos","events_url":"https://api.github.com/users/chevi/events{/privacy}","received_events_url":"https://api.github.com/users/chevi/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-09-11T06:08:52Z","updated_at":"2019-08-15T12:09:40Z","closed_at":"2018-09-11T07:35:08Z","author_association":"NONE","body":"## Meta -\r\nOS:  Xfce 4.12\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.14\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  69.0.3497.81 (64-bit)\r\nChrome driver Version: 2.41\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nEntered text with space symbols\r\n## Actual Behavior -\r\nEntered text is without space symbols\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\n> from selenium import webdriver\r\n> from selenium.webdriver.common.keys import Keys\r\n> txt = \"hello! This is robot\"\r\n> driver = webdriver.Chrome()\r\n> driver.get(\"http://www.python.org\")\r\n> elem = driver.find_element_by_id(\"id-search-field\")\r\n> elem.send_keys(txt)\r\n> driver.save_screenshot('screenie.png')\r\n> driver.close()\r\n\r\nNOTE: with Firefox GeckoDriver - space symbols are typed and displayed correctly. \r\nNOTE: with same Chrome driver, but on Ubuntu/Mint - no problem","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["420177271"], "labels":[]}