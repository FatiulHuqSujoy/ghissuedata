{"id":"5086", "title":"Send Keys do not send \"password123\" to any text field . While using Javascript i am able to fill", "body":"## Meta -
OS:  OSX
<!-- Windows 10? OSX? -->
Selenium Version:  3.5.2
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior - Below is code which I run and its not passing exact string to text field on any website

 I am passing value to textfield using sendkeys function it should send the exact value e.g. **password123** Its really strange this string can be tried with any type of text field issue remains

## Actual Behavior -
If you check on the website it will only send text **password12** its very strange as it work with all other string e.g. if i pass 
driver.findElement(By.id(\"user[login]\")).sendKeys(\"password456\"); it enter in field as expected : **password456**

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
**JAVA Code**

WebDriver driver = new ChromeDriver();
	driver.get(\"https://github.com/\");
	driver.findElement(By.id(\"user[login]\")).sendKeys(\"password123\");
	Thread.sleep(2000);

_**I have actually overcome the issue by using javascript which is as below:**_

	JavascriptExecutor  js= (JavascriptExecutor) driver;
	js.executeScript(String.format(\"document.getElementById('user[login]').value='password123';\"));
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5086","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5086/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5086/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5086/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5086","id":275437204,"node_id":"MDU6SXNzdWUyNzU0MzcyMDQ=","number":5086,"title":"Send Keys do not send \"password123\" to any text field . While using Javascript i am able to fill","user":{"login":"asjadtariq","id":15034234,"node_id":"MDQ6VXNlcjE1MDM0MjM0","avatar_url":"https://avatars1.githubusercontent.com/u/15034234?v=4","gravatar_id":"","url":"https://api.github.com/users/asjadtariq","html_url":"https://github.com/asjadtariq","followers_url":"https://api.github.com/users/asjadtariq/followers","following_url":"https://api.github.com/users/asjadtariq/following{/other_user}","gists_url":"https://api.github.com/users/asjadtariq/gists{/gist_id}","starred_url":"https://api.github.com/users/asjadtariq/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/asjadtariq/subscriptions","organizations_url":"https://api.github.com/users/asjadtariq/orgs","repos_url":"https://api.github.com/users/asjadtariq/repos","events_url":"https://api.github.com/users/asjadtariq/events{/privacy}","received_events_url":"https://api.github.com/users/asjadtariq/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-11-20T17:29:38Z","updated_at":"2019-08-17T01:09:53Z","closed_at":"2017-11-20T20:35:10Z","author_association":"NONE","body":"## Meta -\r\nOS:  OSX\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.5.2\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior - Below is code which I run and its not passing exact string to text field on any website\r\n\r\n I am passing value to textfield using sendkeys function it should send the exact value e.g. **password123** Its really strange this string can be tried with any type of text field issue remains\r\n\r\n## Actual Behavior -\r\nIf you check on the website it will only send text **password12** its very strange as it work with all other string e.g. if i pass \r\ndriver.findElement(By.id(\"user[login]\")).sendKeys(\"password456\"); it enter in field as expected : **password456**\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n**JAVA Code**\r\n\r\nWebDriver driver = new ChromeDriver();\r\n\tdriver.get(\"https://github.com/\");\r\n\tdriver.findElement(By.id(\"user[login]\")).sendKeys(\"password123\");\r\n\tThread.sleep(2000);\r\n\r\n_**I have actually overcome the issue by using javascript which is as below:**_\r\n\r\n\tJavascriptExecutor  js= (JavascriptExecutor) driver;\r\n\tjs.executeScript(String.format(\"document.getElementById('user[login]').value='password123';\"));\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["345769795","345823152"], "labels":[]}