{"id":"5155", "title":"<urlopen error [Errno 111] Connection refused>", "body":"## Meta -
OS:  Ubuntu 16.04
<!-- Windows 10? OSX? -->
Selenium Version:  3.8.0
<!-- 2.52.0, IDE, etc -->
Browser:  Firefox
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  57.0 (64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Steps to reproduce
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

I am crawling a site using django-rest-framework.
my sources are:
``` python
def create_driver_session(session_id, executor_url):
    from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver

    # Save the original function, so we can revert our patch
    org_command_execute = RemoteWebDriver.execute

    def new_command_execute(self, command, params=None):
        if command == \"newSession\":
            # Mock the response
            return {'success': 0, 'value': None, 'sessionId': session_id}
        else:
            return org_command_execute(self, command, params)

    # Patch the function before creating the driver object
    RemoteWebDriver.execute = new_command_execute

    new_driver = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
    new_driver.session_id = session_id

    # Replace the patched function with original function
    RemoteWebDriver.execute = org_command_execute

    return new_driver

@api_view(['POST'])
def phone_auth_num(request):
    display = Display(visible=0, size=(800, 600))
    display.start()

    driver = webdriver.Firefox()
    data = request.POST

    driver.get('https://noom.co.kr/faq/')

    time.sleep(3)

    t = driver.find_element_by_css_selector('#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4')
    t.click()

    executor_url = driver.command_executor._url
    session_id = driver.session_id

    return Response({\"message\": \"Success\", \"executor_url\": executor_url, \"session_id\": session_id})

@api_view(['POST'])
def manufacture_list(request):
    data = request.POST

    driver = create_driver_session(data['session_id'],data['executor_url'])

    for _ in range(0,30):
        d = driver.find_element_by_css_selector(\"#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4\")
        print(d.text)

    d.click()

    driver.quit()

    return Response({\"message\": \"Success\"})
```

`phone_auth_num` function will be called when `/phone-auth` url request, and  
`manufacture_list` function will be called when `/manufacture-list` url request.

When I continuously request `/phone-auth` and `/manufacture-list` at first time, it works well.
but When I request second time, it occurs error with below message:

``` shell
Internal Server Error: /manufacture-list/
Traceback (most recent call last):
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1318, in do_open
    encode_chunked=req.has_header('Transfer-encoding'))
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1239, in request
    self._send_request(method, url, body, headers, encode_chunked)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1285, in _send_request
    self.endheaders(body, encode_chunked=encode_chunked)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1234, in endheaders
    self._send_output(message_body, encode_chunked=encode_chunked)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1026, in _send_output
    self.send(msg)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 964, in send
    self.connect()
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 936, in connect
    (self.host,self.port), self.timeout, self.source_address)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/socket.py\", line 722, in create_connection
    raise err
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/socket.py\", line 713, in create_connection
    sock.connect(sa)
ConnectionRefusedError: [Errno 111] Connection refused

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/exception.py\", line 41, in inner
    response = get_response(request)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/base.py\", line 187, in _get_response
    response = self.process_exception_by_middleware(e, request)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/base.py\", line 185, in _get_response
    response = wrapped_callback(request, *callback_args, **callback_kwargs)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/views/decorators/csrf.py\", line 58, in wrapped_view
    return view_func(*args, **kwargs)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/views/generic/base.py\", line 68, in view
    return self.dispatch(request, *args, **kwargs)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 483, in dispatch
    response = self.handle_exception(exc)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 443, in handle_exception
    self.raise_uncaught_exception(exc)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 480, in dispatch
    response = handler(request, *args, **kwargs)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/decorators.py\", line 52, in handler
    return func(*args, **kwargs)
  File \"./inscheck/spider/views.py\", line 194, in manufacture_list
    d = driver.find_element_by_css_selector(\"#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4\")
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 512, in find_element_by_css_selector
    return self.find_element(by=By.CSS_SELECTOR, value=css_selector)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 858, in find_element
    'value': value})['value']
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 309, in execute
    response = self.command_executor.execute(driver_command, params)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 460, in execute
    return self._request(command_info[0], url, body=data)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 522, in _request
    resp = opener.open(request, timeout=self._timeout)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 526, in open
    response = self._open(req, data)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 544, in _open
    '_open', req)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 504, in _call_chain
    result = func(*args)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1346, in http_open
    return self.do_open(http.client.HTTPConnection, req)
  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1320, in do_open
    raise URLError(err)
urllib.error.URLError: <urlopen error [Errno 111] Connection refused>
```

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5155","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5155/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5155/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5155/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5155","id":278446613,"node_id":"MDU6SXNzdWUyNzg0NDY2MTM=","number":5155,"title":"<urlopen error [Errno 111] Connection refused>","user":{"login":"wkdtjsgur100","id":17163958,"node_id":"MDQ6VXNlcjE3MTYzOTU4","avatar_url":"https://avatars2.githubusercontent.com/u/17163958?v=4","gravatar_id":"","url":"https://api.github.com/users/wkdtjsgur100","html_url":"https://github.com/wkdtjsgur100","followers_url":"https://api.github.com/users/wkdtjsgur100/followers","following_url":"https://api.github.com/users/wkdtjsgur100/following{/other_user}","gists_url":"https://api.github.com/users/wkdtjsgur100/gists{/gist_id}","starred_url":"https://api.github.com/users/wkdtjsgur100/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wkdtjsgur100/subscriptions","organizations_url":"https://api.github.com/users/wkdtjsgur100/orgs","repos_url":"https://api.github.com/users/wkdtjsgur100/repos","events_url":"https://api.github.com/users/wkdtjsgur100/events{/privacy}","received_events_url":"https://api.github.com/users/wkdtjsgur100/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-12-01T12:06:17Z","updated_at":"2019-08-17T00:09:38Z","closed_at":"2017-12-01T15:13:49Z","author_association":"NONE","body":"## Meta -\r\nOS:  Ubuntu 16.04\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.8.0\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Firefox\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  57.0 (64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Steps to reproduce\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\nI am crawling a site using django-rest-framework.\r\nmy sources are:\r\n``` python\r\ndef create_driver_session(session_id, executor_url):\r\n    from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver\r\n\r\n    # Save the original function, so we can revert our patch\r\n    org_command_execute = RemoteWebDriver.execute\r\n\r\n    def new_command_execute(self, command, params=None):\r\n        if command == \"newSession\":\r\n            # Mock the response\r\n            return {'success': 0, 'value': None, 'sessionId': session_id}\r\n        else:\r\n            return org_command_execute(self, command, params)\r\n\r\n    # Patch the function before creating the driver object\r\n    RemoteWebDriver.execute = new_command_execute\r\n\r\n    new_driver = webdriver.Remote(command_executor=executor_url, desired_capabilities={})\r\n    new_driver.session_id = session_id\r\n\r\n    # Replace the patched function with original function\r\n    RemoteWebDriver.execute = org_command_execute\r\n\r\n    return new_driver\r\n\r\n@api_view(['POST'])\r\ndef phone_auth_num(request):\r\n    display = Display(visible=0, size=(800, 600))\r\n    display.start()\r\n\r\n    driver = webdriver.Firefox()\r\n    data = request.POST\r\n\r\n    driver.get('https://noom.co.kr/faq/')\r\n\r\n    time.sleep(3)\r\n\r\n    t = driver.find_element_by_css_selector('#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4')\r\n    t.click()\r\n\r\n    executor_url = driver.command_executor._url\r\n    session_id = driver.session_id\r\n\r\n    return Response({\"message\": \"Success\", \"executor_url\": executor_url, \"session_id\": session_id})\r\n\r\n@api_view(['POST'])\r\ndef manufacture_list(request):\r\n    data = request.POST\r\n\r\n    driver = create_driver_session(data['session_id'],data['executor_url'])\r\n\r\n    for _ in range(0,30):\r\n        d = driver.find_element_by_css_selector(\"#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4\")\r\n        print(d.text)\r\n\r\n    d.click()\r\n\r\n    driver.quit()\r\n\r\n    return Response({\"message\": \"Success\"})\r\n```\r\n\r\n`phone_auth_num` function will be called when `/phone-auth` url request, and  \r\n`manufacture_list` function will be called when `/manufacture-list` url request.\r\n\r\nWhen I continuously request `/phone-auth` and `/manufacture-list` at first time, it works well.\r\nbut When I request second time, it occurs error with below message:\r\n\r\n``` shell\r\nInternal Server Error: /manufacture-list/\r\nTraceback (most recent call last):\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1318, in do_open\r\n    encode_chunked=req.has_header('Transfer-encoding'))\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1239, in request\r\n    self._send_request(method, url, body, headers, encode_chunked)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1285, in _send_request\r\n    self.endheaders(body, encode_chunked=encode_chunked)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1234, in endheaders\r\n    self._send_output(message_body, encode_chunked=encode_chunked)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 1026, in _send_output\r\n    self.send(msg)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 964, in send\r\n    self.connect()\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/http/client.py\", line 936, in connect\r\n    (self.host,self.port), self.timeout, self.source_address)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/socket.py\", line 722, in create_connection\r\n    raise err\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/socket.py\", line 713, in create_connection\r\n    sock.connect(sa)\r\nConnectionRefusedError: [Errno 111] Connection refused\r\n\r\nDuring handling of the above exception, another exception occurred:\r\n\r\nTraceback (most recent call last):\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/exception.py\", line 41, in inner\r\n    response = get_response(request)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/base.py\", line 187, in _get_response\r\n    response = self.process_exception_by_middleware(e, request)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/core/handlers/base.py\", line 185, in _get_response\r\n    response = wrapped_callback(request, *callback_args, **callback_kwargs)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/views/decorators/csrf.py\", line 58, in wrapped_view\r\n    return view_func(*args, **kwargs)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/django/views/generic/base.py\", line 68, in view\r\n    return self.dispatch(request, *args, **kwargs)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 483, in dispatch\r\n    response = self.handle_exception(exc)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 443, in handle_exception\r\n    self.raise_uncaught_exception(exc)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/views.py\", line 480, in dispatch\r\n    response = handler(request, *args, **kwargs)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/rest_framework/decorators.py\", line 52, in handler\r\n    return func(*args, **kwargs)\r\n  File \"./inscheck/spider/views.py\", line 194, in manufacture_list\r\n    d = driver.find_element_by_css_selector(\"#accordion-5564-1 > div:nth-child(2) > div.panel-heading > h4\")\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 512, in find_element_by_css_selector\r\n    return self.find_element(by=By.CSS_SELECTOR, value=css_selector)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 858, in find_element\r\n    'value': value})['value']\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 309, in execute\r\n    response = self.command_executor.execute(driver_command, params)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 460, in execute\r\n    return self._request(command_info[0], url, body=data)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/envs/inscheck-3.6.0-env/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 522, in _request\r\n    resp = opener.open(request, timeout=self._timeout)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 526, in open\r\n    response = self._open(req, data)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 544, in _open\r\n    '_open', req)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 504, in _call_chain\r\n    result = func(*args)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1346, in http_open\r\n    return self.do_open(http.client.HTTPConnection, req)\r\n  File \"/home/ubuntu/.pyenv/versions/3.6.0/lib/python3.6/urllib/request.py\", line 1320, in do_open\r\n    raise URLError(err)\r\nurllib.error.URLError: <urlopen error [Errno 111] Connection refused>\r\n```\r\n\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["348520141"], "labels":[]}