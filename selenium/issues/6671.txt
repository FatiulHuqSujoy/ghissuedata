{"id":"6671", "title":"Task scheduler not launching powerbi report in chrome using Selenium script", "body":"Hi All,

I am trying to launch the powerbi report in chrome browser through Selenium script. its basically taking the username and password to launch the report in chrome browser.
I created the console application and make the exe.
Expected Behavior:
1. when we executed the exe manually then its should launch the powerbi report in chrome(username and password will be filled by script). 
--> This is working fine.

2. When we scheduled the exe from task scheduler then exe should run and launch the powerbi report in chrome browser(username and password will be filled by script). 
--> This should working but its not working.

Assumption for point 2: exe trying to sent the command to chrome browser in background but its not performing any action and throwing following error:
Error: The HTTP request to the remote WebDriver server for URL timed out after 60 seconds.

Sample Code: Used following code.

IWebDriver driver = new ChromeDriver();

                    driver.Navigate().GoToUrl(testreport);
                    driver.FindElement(By.Id(\"a12345\")).SendKeys(\"abc\");
                    driver.FindElement(By.Id(\"idTestButton1\")).Click();

                    Thread.Sleep(10000);
                    AutoItX.Send(\"abc\");
                    AutoItX.Send(\"{TAB}\");
                    AutoItX.Send(\"test123\", 1);
                    AutoItX.Send(\"{Enter}\");

                    DateTime startTime = DateTime.UtcNow;

WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(\"reprtname\")));

Thanks", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6671","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6671/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6671/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6671/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6671","id":382993281,"node_id":"MDU6SXNzdWUzODI5OTMyODE=","number":6671,"title":"Task scheduler not launching powerbi report in chrome using Selenium script","user":{"login":"ankittarsolia","id":38805661,"node_id":"MDQ6VXNlcjM4ODA1NjYx","avatar_url":"https://avatars2.githubusercontent.com/u/38805661?v=4","gravatar_id":"","url":"https://api.github.com/users/ankittarsolia","html_url":"https://github.com/ankittarsolia","followers_url":"https://api.github.com/users/ankittarsolia/followers","following_url":"https://api.github.com/users/ankittarsolia/following{/other_user}","gists_url":"https://api.github.com/users/ankittarsolia/gists{/gist_id}","starred_url":"https://api.github.com/users/ankittarsolia/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ankittarsolia/subscriptions","organizations_url":"https://api.github.com/users/ankittarsolia/orgs","repos_url":"https://api.github.com/users/ankittarsolia/repos","events_url":"https://api.github.com/users/ankittarsolia/events{/privacy}","received_events_url":"https://api.github.com/users/ankittarsolia/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-11-21T07:43:58Z","updated_at":"2019-08-15T04:10:02Z","closed_at":"2018-11-21T09:37:09Z","author_association":"NONE","body":"Hi All,\r\n\r\nI am trying to launch the powerbi report in chrome browser through Selenium script. its basically taking the username and password to launch the report in chrome browser.\r\nI created the console application and make the exe.\r\nExpected Behavior:\r\n1. when we executed the exe manually then its should launch the powerbi report in chrome(username and password will be filled by script). \r\n--> This is working fine.\r\n\r\n2. When we scheduled the exe from task scheduler then exe should run and launch the powerbi report in chrome browser(username and password will be filled by script). \r\n--> This should working but its not working.\r\n\r\nAssumption for point 2: exe trying to sent the command to chrome browser in background but its not performing any action and throwing following error:\r\nError: The HTTP request to the remote WebDriver server for URL timed out after 60 seconds.\r\n\r\nSample Code: Used following code.\r\n\r\nIWebDriver driver = new ChromeDriver();\r\n\r\n                    driver.Navigate().GoToUrl(testreport);\r\n                    driver.FindElement(By.Id(\"a12345\")).SendKeys(\"abc\");\r\n                    driver.FindElement(By.Id(\"idTestButton1\")).Click();\r\n\r\n                    Thread.Sleep(10000);\r\n                    AutoItX.Send(\"abc\");\r\n                    AutoItX.Send(\"{TAB}\");\r\n                    AutoItX.Send(\"test123\", 1);\r\n                    AutoItX.Send(\"{Enter}\");\r\n\r\n                    DateTime startTime = DateTime.UtcNow;\r\n\r\nWebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));\r\nwait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(\"reprtname\")));\r\n\r\nThanks","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["440597850"], "labels":[]}