{"id":"6874", "title":"Chrome version exception after update windows package", "body":"## To Reproduce

```
E:\\Archive\\Tool\\NetAuth>python e:\\Archive\\Tool\\NetAuth\\auth.py
[0123/085057.182:ERROR:gpu_process_transport_factory.cc(1007)] Lost UI shared context.

DevTools listening on ws://127.0.0.1:2775/devtools/browser/356fb785-1b82-4465-93da-1eeb6d20e745
Traceback (most recent call last):
  File \"e:\\Archive\\Tool\\NetAuth\\auth.py\", line 17, in <module>
    browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\chrome\\webdriver.py\", line 81, in __init__
    desired_capabilities=desired_capabilities)
  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__
    self.start_session(capabilities, browser_profile)
  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session
    response = self.execute(Command.NEW_SESSION, parameters)
  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.SessionNotCreatedException: Message: session not created: Chrome version must be between 70 and 73
  (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 10.0.17763 x86_64)

E:\\Archive\\Tool\\NetAuth>wmic datafile where name=\"C:\\\\Program Files (x86)\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\" get Version /value

Version=71.0.3578.98
```

## Environment

OS: <-- Windows 10 -->
Browser: <-- Chrome  -->
Browser version: <-- 71.0.3578.98 -->
Browser Driver version: <-- ChromeDriver 2.45.615291 -->
Language Bindings version: <-- Python 3.6.4 --> 


After uninstall these update then fix this problem but not sure which one caused
KB4480056 (probably)
KB4480116", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6874","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6874/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6874/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6874/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6874","id":402021030,"node_id":"MDU6SXNzdWU0MDIwMjEwMzA=","number":6874,"title":"Chrome version exception after update windows package","user":{"login":"rickywu","id":442398,"node_id":"MDQ6VXNlcjQ0MjM5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/442398?v=4","gravatar_id":"","url":"https://api.github.com/users/rickywu","html_url":"https://github.com/rickywu","followers_url":"https://api.github.com/users/rickywu/followers","following_url":"https://api.github.com/users/rickywu/following{/other_user}","gists_url":"https://api.github.com/users/rickywu/gists{/gist_id}","starred_url":"https://api.github.com/users/rickywu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rickywu/subscriptions","organizations_url":"https://api.github.com/users/rickywu/orgs","repos_url":"https://api.github.com/users/rickywu/repos","events_url":"https://api.github.com/users/rickywu/events{/privacy}","received_events_url":"https://api.github.com/users/rickywu/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-01-23T00:54:24Z","updated_at":"2019-08-14T23:09:52Z","closed_at":"2019-01-23T06:55:17Z","author_association":"NONE","body":"## To Reproduce\r\n\r\n```\r\nE:\\Archive\\Tool\\NetAuth>python e:\\Archive\\Tool\\NetAuth\\auth.py\r\n[0123/085057.182:ERROR:gpu_process_transport_factory.cc(1007)] Lost UI shared context.\r\n\r\nDevTools listening on ws://127.0.0.1:2775/devtools/browser/356fb785-1b82-4465-93da-1eeb6d20e745\r\nTraceback (most recent call last):\r\n  File \"e:\\Archive\\Tool\\NetAuth\\auth.py\", line 17, in <module>\r\n    browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)\r\n  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\chrome\\webdriver.py\", line 81, in __init__\r\n    desired_capabilities=desired_capabilities)\r\n  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__\r\n    self.start_session(capabilities, browser_profile)\r\n  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session\r\n    response = self.execute(Command.NEW_SESSION, parameters)\r\n  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"D:\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.SessionNotCreatedException: Message: session not created: Chrome version must be between 70 and 73\r\n  (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 10.0.17763 x86_64)\r\n\r\nE:\\Archive\\Tool\\NetAuth>wmic datafile where name=\"C:\\\\Program Files (x86)\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\" get Version /value\r\n\r\nVersion=71.0.3578.98\r\n```\r\n\r\n## Environment\r\n\r\nOS: <-- Windows 10 -->\r\nBrowser: <-- Chrome  -->\r\nBrowser version: <-- 71.0.3578.98 -->\r\nBrowser Driver version: <-- ChromeDriver 2.45.615291 -->\r\nLanguage Bindings version: <-- Python 3.6.4 --> \r\n\r\n\r\nAfter uninstall these update then fix this problem but not sure which one caused\r\nKB4480056 (probably)\r\nKB4480116","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["456691475"], "labels":[]}