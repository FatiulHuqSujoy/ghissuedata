{"id":"7023", "title":"Unable to install selenium server standalone 3.8.0", "body":"## 🐛 Bug Report

I started having problem when installing selenium-server-standalone-3.8.0.jar from npm this afternoon. Can someone help, please?

## To Reproduce
We are pulling selenium-server-standalone from npm and install to local and aws instance with a clean build, which mean no node_modules folder. Below is the error
> selenium-standalone installation starting
> 
> selenium install:
> from: https://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.0.jar
> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/selenium-server/3.8.0-server.jar
> 
> chrome install:
> from: https://chromedriver.storage.googleapis.com/2.37/chromedriver_mac64.zip
> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/chromedriver/2.37-x64-chromedriver
> 
> firefox install:
> from: https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-macos.tar.gz
> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/geckodriver/0.23.0-x64-geckodriver
> /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/bin/selenium-standalone:111
>         throw err;
>         ^
> 
> Error: Could not download https://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.0.jar
>     at Request.<anonymous> (/Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/lib/install.js:373:21)
>     at emitOne (events.js:116:13)
>     at Request.emit (events.js:211:7)
>     at Request.onRequestResponse (/Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/request/request.js:1066:10)
>     at emitOne (events.js:116:13)
>     at ClientRequest.emit (events.js:211:7)
>     at HTTPParser.parserOnIncomingClient [as onIncoming] (_http_client.js:551:21)
>     at HTTPParser.parserOnHeadersComplete (_http_common.js:117:23)
>     at TLSSocket.socketOnData (_http_client.js:440:20)
>     at emitOne (events.js:116:13)

## Environment

OS: <!-- Windows 10, OSX, Linux -->
Browser Driver version: <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7023","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7023/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7023/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7023/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7023","id":420311049,"node_id":"MDU6SXNzdWU0MjAzMTEwNDk=","number":7023,"title":"Unable to install selenium server standalone 3.8.0","user":{"login":"weiming-myob","id":30454953,"node_id":"MDQ6VXNlcjMwNDU0OTUz","avatar_url":"https://avatars3.githubusercontent.com/u/30454953?v=4","gravatar_id":"","url":"https://api.github.com/users/weiming-myob","html_url":"https://github.com/weiming-myob","followers_url":"https://api.github.com/users/weiming-myob/followers","following_url":"https://api.github.com/users/weiming-myob/following{/other_user}","gists_url":"https://api.github.com/users/weiming-myob/gists{/gist_id}","starred_url":"https://api.github.com/users/weiming-myob/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/weiming-myob/subscriptions","organizations_url":"https://api.github.com/users/weiming-myob/orgs","repos_url":"https://api.github.com/users/weiming-myob/repos","events_url":"https://api.github.com/users/weiming-myob/events{/privacy}","received_events_url":"https://api.github.com/users/weiming-myob/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":11,"created_at":"2019-03-13T04:23:50Z","updated_at":"2019-08-14T19:09:55Z","closed_at":"2019-03-13T06:50:51Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nI started having problem when installing selenium-server-standalone-3.8.0.jar from npm this afternoon. Can someone help, please?\r\n\r\n## To Reproduce\r\nWe are pulling selenium-server-standalone from npm and install to local and aws instance with a clean build, which mean no node_modules folder. Below is the error\r\n> selenium-standalone installation starting\r\n> \r\n> selenium install:\r\n> from: https://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.0.jar\r\n> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/selenium-server/3.8.0-server.jar\r\n> \r\n> chrome install:\r\n> from: https://chromedriver.storage.googleapis.com/2.37/chromedriver_mac64.zip\r\n> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/chromedriver/2.37-x64-chromedriver\r\n> \r\n> firefox install:\r\n> from: https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-macos.tar.gz\r\n> to: /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/.selenium/geckodriver/0.23.0-x64-geckodriver\r\n> /Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/bin/selenium-standalone:111\r\n>         throw err;\r\n>         ^\r\n> \r\n> Error: Could not download https://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.0.jar\r\n>     at Request.<anonymous> (/Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/selenium-standalone/lib/install.js:373:21)\r\n>     at emitOne (events.js:116:13)\r\n>     at Request.emit (events.js:211:7)\r\n>     at Request.onRequestResponse (/Users/weiming.chen/Projects/AD-PracticeOnline-Automation-Tests/node_modules/request/request.js:1066:10)\r\n>     at emitOne (events.js:116:13)\r\n>     at ClientRequest.emit (events.js:211:7)\r\n>     at HTTPParser.parserOnIncomingClient [as onIncoming] (_http_client.js:551:21)\r\n>     at HTTPParser.parserOnHeadersComplete (_http_common.js:117:23)\r\n>     at TLSSocket.socketOnData (_http_client.js:440:20)\r\n>     at emitOne (events.js:116:13)\r\n\r\n## Environment\r\n\r\nOS: <!-- Windows 10, OSX, Linux -->\r\nBrowser Driver version: <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["472276240","472277004","472277769","472278049","472278319","472279297","472285001","472301582","472624685","472641892","472669162"], "labels":[]}