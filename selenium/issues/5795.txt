{"id":"5795", "title":"Safari freezing after click, Which leads to naviagate to another page.", "body":"## Meta -
OS:  OSX 10
Selenium Version:  3.8.1
Browser:  Safari
Browser Version:  11.0.3

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->



## Expected Behavior -
After **click** in the current page, It needs to navigate to next page.
While navigating running WaitUntilElementClickable or presence or visible.
Once element clickable needs to perform another task.
## Actual Behavior -
After **click** in current page, It is slowly navigating to next page.
If we run while navigating running WaitUntilElementClickable. safari freezing. No more operations performed.

As my observation running WaitUntilElementClickable uses the while loop with multiple times trying findElement() until element clickable. This while loop will exit after wait time. On the first iteration in while loop it will go to findelement and then freezing.

until method logic.

while(true){
    findelement() -> freezing here as my expectation. And then it is not breaking the loop.
    if(wait time reached){
          break;
    }
}

## Steps to reproduce -

Go to any website.
Click on any link to navigate.
Run WaitUntilElementClickable or presence or visible. the issue will start here. safari freez.

We have a temporary solution like by giving sleep for some time(10 sec), We can perform further tasks.

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5795","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5795/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5795/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5795/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5795","id":315404208,"node_id":"MDU6SXNzdWUzMTU0MDQyMDg=","number":5795,"title":"Safari freezing after click, Which leads to naviagate to another page.","user":{"login":"NaveenKummetha","id":16399004,"node_id":"MDQ6VXNlcjE2Mzk5MDA0","avatar_url":"https://avatars3.githubusercontent.com/u/16399004?v=4","gravatar_id":"","url":"https://api.github.com/users/NaveenKummetha","html_url":"https://github.com/NaveenKummetha","followers_url":"https://api.github.com/users/NaveenKummetha/followers","following_url":"https://api.github.com/users/NaveenKummetha/following{/other_user}","gists_url":"https://api.github.com/users/NaveenKummetha/gists{/gist_id}","starred_url":"https://api.github.com/users/NaveenKummetha/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/NaveenKummetha/subscriptions","organizations_url":"https://api.github.com/users/NaveenKummetha/orgs","repos_url":"https://api.github.com/users/NaveenKummetha/repos","events_url":"https://api.github.com/users/NaveenKummetha/events{/privacy}","received_events_url":"https://api.github.com/users/NaveenKummetha/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-04-18T09:37:57Z","updated_at":"2019-08-16T03:09:55Z","closed_at":"2018-05-03T07:12:36Z","author_association":"NONE","body":"## Meta -\r\nOS:  OSX 10\r\nSelenium Version:  3.8.1\r\nBrowser:  Safari\r\nBrowser Version:  11.0.3\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\n\r\n\r\n## Expected Behavior -\r\nAfter **click** in the current page, It needs to navigate to next page.\r\nWhile navigating running WaitUntilElementClickable or presence or visible.\r\nOnce element clickable needs to perform another task.\r\n## Actual Behavior -\r\nAfter **click** in current page, It is slowly navigating to next page.\r\nIf we run while navigating running WaitUntilElementClickable. safari freezing. No more operations performed.\r\n\r\nAs my observation running WaitUntilElementClickable uses the while loop with multiple times trying findElement() until element clickable. This while loop will exit after wait time. On the first iteration in while loop it will go to findelement and then freezing.\r\n\r\nuntil method logic.\r\n\r\nwhile(true){\r\n    findelement() -> freezing here as my expectation. And then it is not breaking the loop.\r\n    if(wait time reached){\r\n          break;\r\n    }\r\n}\r\n\r\n## Steps to reproduce -\r\n\r\nGo to any website.\r\nClick on any link to navigate.\r\nRun WaitUntilElementClickable or presence or visible. the issue will start here. safari freez.\r\n\r\nWe have a temporary solution like by giving sleep for some time(10 sec), We can perform further tasks.\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["382371209","386208857"], "labels":["R-awaiting answer"]}