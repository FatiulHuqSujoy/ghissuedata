{"id":"4951", "title":"Selenium 3.6.0 with gecko 0.19 and FF version 56 is not working with default capabilities", "body":"OS:  
Windows 10
Selenium Version:  
3.60
Browser:  
Firefox version 56

We are trying to run selenium 3.60 using geckodriver version 0.19 on firefox 56 but the browser is not gettingopen and we are getting WebDriverException

Browser Version:  
56.0(32 bit)

## Expected Behavior -
Browser should open and driver.get() should redirect to the url entered

## Actual Behavior -
Browser is not getting opened and we are getting WebDriver Exception

## Steps to reproduce -
String path = \"C:\\\\Users\\\\Sarthak.Srivastava\\\\Desktop\\\\geckodriver.exe\";	
System.setProperty(\"webdriver.gecko.driver\", path);
WebDriver driver = new FirefoxDriver();
driver.get(\"https://navbharattimes.indiatimes.com\");
Thread.sleep(5000);   
driver.quit();
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4951","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4951/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4951/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4951/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4951","id":268350080,"node_id":"MDU6SXNzdWUyNjgzNTAwODA=","number":4951,"title":"Selenium 3.6.0 with gecko 0.19 and FF version 56 is not working with default capabilities","user":{"login":"sarthak2990","id":20354386,"node_id":"MDQ6VXNlcjIwMzU0Mzg2","avatar_url":"https://avatars1.githubusercontent.com/u/20354386?v=4","gravatar_id":"","url":"https://api.github.com/users/sarthak2990","html_url":"https://github.com/sarthak2990","followers_url":"https://api.github.com/users/sarthak2990/followers","following_url":"https://api.github.com/users/sarthak2990/following{/other_user}","gists_url":"https://api.github.com/users/sarthak2990/gists{/gist_id}","starred_url":"https://api.github.com/users/sarthak2990/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sarthak2990/subscriptions","organizations_url":"https://api.github.com/users/sarthak2990/orgs","repos_url":"https://api.github.com/users/sarthak2990/repos","events_url":"https://api.github.com/users/sarthak2990/events{/privacy}","received_events_url":"https://api.github.com/users/sarthak2990/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":9,"created_at":"2017-10-25T10:44:33Z","updated_at":"2019-08-15T20:09:54Z","closed_at":"2017-12-02T20:16:55Z","author_association":"NONE","body":"OS:  \r\nWindows 10\r\nSelenium Version:  \r\n3.60\r\nBrowser:  \r\nFirefox version 56\r\n\r\nWe are trying to run selenium 3.60 using geckodriver version 0.19 on firefox 56 but the browser is not gettingopen and we are getting WebDriverException\r\n\r\nBrowser Version:  \r\n56.0(32 bit)\r\n\r\n## Expected Behavior -\r\nBrowser should open and driver.get() should redirect to the url entered\r\n\r\n## Actual Behavior -\r\nBrowser is not getting opened and we are getting WebDriver Exception\r\n\r\n## Steps to reproduce -\r\nString path = \"C:\\\\Users\\\\Sarthak.Srivastava\\\\Desktop\\\\geckodriver.exe\";\t\r\nSystem.setProperty(\"webdriver.gecko.driver\", path);\r\nWebDriver driver = new FirefoxDriver();\r\ndriver.get(\"https://navbharattimes.indiatimes.com\");\r\nThread.sleep(5000);   \r\ndriver.quit();\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["339369135","339452440","340970334","341112214","342843969","348717140","359103036","401232519","401232740"], "labels":["R-awaiting answer"]}