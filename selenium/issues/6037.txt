{"id":"6037", "title":"Chromium based browser (Ghost browser) unexpectedly exited status code 0", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
<!-- 2.52.0, IDE, etc -->
Browser:  
Ghost Browser

## Expected Behavior -

Open and keep control of browser.

## Actual Behavior -
Opens the browser and exits, not closing the browser but losing control of it.

## Steps to reproduce -
Install Ghost Browser
Get path
insert it's path as the browser executable path

Code
   ```
 exe_path = r'C:\\Users\\Anonymous\\AppData\\Local\\GhostBrowser\\Application\\ghost.exe'
 driver = webdriver.Chrome(executable_path=exe_path)
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6037","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6037/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6037/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6037/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6037","id":333374299,"node_id":"MDU6SXNzdWUzMzMzNzQyOTk=","number":6037,"title":"Chromium based browser (Ghost browser) unexpectedly exited status code 0","user":{"login":"Jaquedeveloper","id":2420271,"node_id":"MDQ6VXNlcjI0MjAyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/2420271?v=4","gravatar_id":"","url":"https://api.github.com/users/Jaquedeveloper","html_url":"https://github.com/Jaquedeveloper","followers_url":"https://api.github.com/users/Jaquedeveloper/followers","following_url":"https://api.github.com/users/Jaquedeveloper/following{/other_user}","gists_url":"https://api.github.com/users/Jaquedeveloper/gists{/gist_id}","starred_url":"https://api.github.com/users/Jaquedeveloper/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Jaquedeveloper/subscriptions","organizations_url":"https://api.github.com/users/Jaquedeveloper/orgs","repos_url":"https://api.github.com/users/Jaquedeveloper/repos","events_url":"https://api.github.com/users/Jaquedeveloper/events{/privacy}","received_events_url":"https://api.github.com/users/Jaquedeveloper/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-06-18T17:56:49Z","updated_at":"2019-08-15T22:09:45Z","closed_at":"2018-06-18T18:13:57Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  \r\nGhost Browser\r\n\r\n## Expected Behavior -\r\n\r\nOpen and keep control of browser.\r\n\r\n## Actual Behavior -\r\nOpens the browser and exits, not closing the browser but losing control of it.\r\n\r\n## Steps to reproduce -\r\nInstall Ghost Browser\r\nGet path\r\ninsert it's path as the browser executable path\r\n\r\nCode\r\n   ```\r\n exe_path = r'C:\\Users\\Anonymous\\AppData\\Local\\GhostBrowser\\Application\\ghost.exe'\r\n driver = webdriver.Chrome(executable_path=exe_path)\r\n```","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["398146368","398189356","398288112"], "labels":[]}