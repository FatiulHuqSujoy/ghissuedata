{"id":"7342", "title":"CapabilityType.LOGGING_PREFS is not valid for latest versions of Chrome (75+)", "body":"## 🐛 Bug Report

Currently String LOGGING_PREFS is set to \"loggingPrefs\":
https://github.com/SeleniumHQ/selenium/blob/master/java/client/src/org/openqa/selenium/remote/CapabilityType.java#L52

I know, it was working in Chrome 71.
But it doesn't work in Chrome 75.
Chrome 75 only works when I manually use `'goog:loggingPrefs'` instead of `'loggingPrefs'`

## To Reproduce

Here is Groovy code, which is working (returns browser logs) with Chrome 71, but returns nothing with Chrome 75:
```Groovy
            ChromeOptions options = new ChromeOptions().addArguments(
                    '--headless',
                    '--no-sandbox',
                    '--disable-extensions')
            LoggingPreferences logPrefs = new LoggingPreferences()
            logPrefs.enable(LogType.BROWSER, Level.ALL)
            options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs)
            driver = new ChromeDriver(options)
            driver.get(Endpoints.TAGGED_PAGE_URL)
            LogEntries logs = driver.manage().logs().get(LogType.BROWSER)
            for (LogEntry entry : logs) {
                log.info entry.message
           }
```
## Expected behavior
if i replace one line in the example above, it works in Chrome 75:
```Groovy
            options.setCapability('goog:loggingPrefs', logPrefs)
```

## Environment

OS: Mac Os Mojave
Browser: Chrome 75
Browser version: 75.0.3770.100 
Browser Driver version: chromedriver 75.0.3770.8
Language Bindings version: 
```
    compile 'org.seleniumhq.selenium:selenium-java:3.14.0'
    compile 'org.seleniumhq.selenium:selenium-api:3.14.0'
    compile 'org.seleniumhq.selenium:selenium-chrome-driver:3.14.0'
```
Selenium Grid version (if applicable): n/a, using local headless Chrome
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7342","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7342/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7342/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7342/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7342","id":462164214,"node_id":"MDU6SXNzdWU0NjIxNjQyMTQ=","number":7342,"title":"CapabilityType.LOGGING_PREFS is not valid for latest versions of Chrome (75+)","user":{"login":"SlavikCA","id":6293679,"node_id":"MDQ6VXNlcjYyOTM2Nzk=","avatar_url":"https://avatars2.githubusercontent.com/u/6293679?v=4","gravatar_id":"","url":"https://api.github.com/users/SlavikCA","html_url":"https://github.com/SlavikCA","followers_url":"https://api.github.com/users/SlavikCA/followers","following_url":"https://api.github.com/users/SlavikCA/following{/other_user}","gists_url":"https://api.github.com/users/SlavikCA/gists{/gist_id}","starred_url":"https://api.github.com/users/SlavikCA/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SlavikCA/subscriptions","organizations_url":"https://api.github.com/users/SlavikCA/orgs","repos_url":"https://api.github.com/users/SlavikCA/repos","events_url":"https://api.github.com/users/SlavikCA/events{/privacy}","received_events_url":"https://api.github.com/users/SlavikCA/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false},{"id":316341013,"node_id":"MDU6TGFiZWwzMTYzNDEwMTM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-chrome","name":"D-chrome","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2019-06-28T18:48:55Z","updated_at":"2019-09-14T16:09:35Z","closed_at":"2019-07-20T09:38:38Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nCurrently String LOGGING_PREFS is set to \"loggingPrefs\":\r\nhttps://github.com/SeleniumHQ/selenium/blob/master/java/client/src/org/openqa/selenium/remote/CapabilityType.java#L52\r\n\r\nI know, it was working in Chrome 71.\r\nBut it doesn't work in Chrome 75.\r\nChrome 75 only works when I manually use `'goog:loggingPrefs'` instead of `'loggingPrefs'`\r\n\r\n## To Reproduce\r\n\r\nHere is Groovy code, which is working (returns browser logs) with Chrome 71, but returns nothing with Chrome 75:\r\n```Groovy\r\n            ChromeOptions options = new ChromeOptions().addArguments(\r\n                    '--headless',\r\n                    '--no-sandbox',\r\n                    '--disable-extensions')\r\n            LoggingPreferences logPrefs = new LoggingPreferences()\r\n            logPrefs.enable(LogType.BROWSER, Level.ALL)\r\n            options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs)\r\n            driver = new ChromeDriver(options)\r\n            driver.get(Endpoints.TAGGED_PAGE_URL)\r\n            LogEntries logs = driver.manage().logs().get(LogType.BROWSER)\r\n            for (LogEntry entry : logs) {\r\n                log.info entry.message\r\n           }\r\n```\r\n## Expected behavior\r\nif i replace one line in the example above, it works in Chrome 75:\r\n```Groovy\r\n            options.setCapability('goog:loggingPrefs', logPrefs)\r\n```\r\n\r\n## Environment\r\n\r\nOS: Mac Os Mojave\r\nBrowser: Chrome 75\r\nBrowser version: 75.0.3770.100 \r\nBrowser Driver version: chromedriver 75.0.3770.8\r\nLanguage Bindings version: \r\n```\r\n    compile 'org.seleniumhq.selenium:selenium-java:3.14.0'\r\n    compile 'org.seleniumhq.selenium:selenium-api:3.14.0'\r\n    compile 'org.seleniumhq.selenium:selenium-chrome-driver:3.14.0'\r\n```\r\nSelenium Grid version (if applicable): n/a, using local headless Chrome\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["506842835","506844477","513453057","521683842","521686522","521694576","531491822"], "labels":["C-java","D-chrome"]}