{"id":"4194", "title":"Implementations of TakesScreenshot not complying with the contract defined in the interface", "body":"`TakesScreenshot#getScreenshotAs` states that `@throws WebDriverException on failure`.

But:

- RemoteWebDriver and RemoteWebElement throw RuntimeException in case of Unexpected result from the CommandExecutor

```
    if (result instanceof String) {
      String base64EncodedPng = (String) result;
      return outputType.convertFromBase64Png(base64EncodedPng);
    } else if (result instanceof byte[]) {
      String base64EncodedPng = new String((byte[]) result);
      return outputType.convertFromBase64Png(base64EncodedPng);
    } else {
      throw new RuntimeException(String.format(\"Unexpected result for %s command: %s\",
          DriverCommand.SCREENSHOT,
          result == null ? \"null\" : result.getClass().getName() + \" instance\"));
    }
```

- EventFiringWebDriver throws UnsupportedOperationException if the underlying driver is not a TakesScreenshot

```
    if (driver instanceof TakesScreenshot) {
      return ((TakesScreenshot) driver).getScreenshotAs(target);
    }

    throw new UnsupportedOperationException(
        \"Underlying driver instance does not support taking screenshots\");
```

Shouldn't we throw WebDriverException even in those circumstances?

Thanks", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4194","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4194/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4194/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4194/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4194","id":236501225,"node_id":"MDU6SXNzdWUyMzY1MDEyMjU=","number":4194,"title":"Implementations of TakesScreenshot not complying with the contract defined in the interface","user":{"login":"alb-i986","id":2717169,"node_id":"MDQ6VXNlcjI3MTcxNjk=","avatar_url":"https://avatars1.githubusercontent.com/u/2717169?v=4","gravatar_id":"","url":"https://api.github.com/users/alb-i986","html_url":"https://github.com/alb-i986","followers_url":"https://api.github.com/users/alb-i986/followers","following_url":"https://api.github.com/users/alb-i986/following{/other_user}","gists_url":"https://api.github.com/users/alb-i986/gists{/gist_id}","starred_url":"https://api.github.com/users/alb-i986/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/alb-i986/subscriptions","organizations_url":"https://api.github.com/users/alb-i986/orgs","repos_url":"https://api.github.com/users/alb-i986/repos","events_url":"https://api.github.com/users/alb-i986/events{/privacy}","received_events_url":"https://api.github.com/users/alb-i986/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2017-06-16T14:35:11Z","updated_at":"2019-08-18T01:09:43Z","closed_at":"2017-07-31T08:04:29Z","author_association":"CONTRIBUTOR","body":"`TakesScreenshot#getScreenshotAs` states that `@throws WebDriverException on failure`.\r\n\r\nBut:\r\n\r\n- RemoteWebDriver and RemoteWebElement throw RuntimeException in case of Unexpected result from the CommandExecutor\r\n\r\n```\r\n    if (result instanceof String) {\r\n      String base64EncodedPng = (String) result;\r\n      return outputType.convertFromBase64Png(base64EncodedPng);\r\n    } else if (result instanceof byte[]) {\r\n      String base64EncodedPng = new String((byte[]) result);\r\n      return outputType.convertFromBase64Png(base64EncodedPng);\r\n    } else {\r\n      throw new RuntimeException(String.format(\"Unexpected result for %s command: %s\",\r\n          DriverCommand.SCREENSHOT,\r\n          result == null ? \"null\" : result.getClass().getName() + \" instance\"));\r\n    }\r\n```\r\n\r\n- EventFiringWebDriver throws UnsupportedOperationException if the underlying driver is not a TakesScreenshot\r\n\r\n```\r\n    if (driver instanceof TakesScreenshot) {\r\n      return ((TakesScreenshot) driver).getScreenshotAs(target);\r\n    }\r\n\r\n    throw new UnsupportedOperationException(\r\n        \"Underlying driver instance does not support taking screenshots\");\r\n```\r\n\r\nShouldn't we throw WebDriverException even in those circumstances?\r\n\r\nThanks","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["C-java"]}