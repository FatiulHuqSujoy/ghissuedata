{"id":"7334", "title":"Issue with generating logfile", "body":"**Environment:**
WebdriverIO version: 4.14.2
Mode: WDIO Testrunner (sync)
Browser name and version: IE 11
selenium standalone: selenium-server-standalone-3.141.59.jar
IE driver: 32 bit 3.14.0

**Config of WebdriverIO:**
specs: [${pathToTests}*.js],
deprecationWarnings: false,
maxInstances: 1,
capabilities: [
{
browserName: 'internet explorer',
platform: 'WINDOWS',
platformName: 'windows',
logLevel: 'trace',
logFile: 'C:\\\\Users\\\\Administrator\\\\Downloads\\\\ielog.txt'
'se:ieOptions': {
'ie.ensureCleanSession': true,
ignoreZoomSetting: true,
ignoreProtectedModeSettings: true
}
}
],
sync: true,

**Issue Description:**
I want to generate logFile as specified in capabilities object. But it is not being generated. Any idea on what I am missing here?
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7334","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7334/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7334/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7334/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7334","id":461597520,"node_id":"MDU6SXNzdWU0NjE1OTc1MjA=","number":7334,"title":"Issue with generating logfile","user":{"login":"pk-saxena","id":35292540,"node_id":"MDQ6VXNlcjM1MjkyNTQw","avatar_url":"https://avatars0.githubusercontent.com/u/35292540?v=4","gravatar_id":"","url":"https://api.github.com/users/pk-saxena","html_url":"https://github.com/pk-saxena","followers_url":"https://api.github.com/users/pk-saxena/followers","following_url":"https://api.github.com/users/pk-saxena/following{/other_user}","gists_url":"https://api.github.com/users/pk-saxena/gists{/gist_id}","starred_url":"https://api.github.com/users/pk-saxena/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/pk-saxena/subscriptions","organizations_url":"https://api.github.com/users/pk-saxena/orgs","repos_url":"https://api.github.com/users/pk-saxena/repos","events_url":"https://api.github.com/users/pk-saxena/events{/privacy}","received_events_url":"https://api.github.com/users/pk-saxena/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-06-27T15:28:43Z","updated_at":"2019-08-14T06:10:05Z","closed_at":"2019-06-29T12:26:22Z","author_association":"NONE","body":"**Environment:**\r\nWebdriverIO version: 4.14.2\r\nMode: WDIO Testrunner (sync)\r\nBrowser name and version: IE 11\r\nselenium standalone: selenium-server-standalone-3.141.59.jar\r\nIE driver: 32 bit 3.14.0\r\n\r\n**Config of WebdriverIO:**\r\nspecs: [${pathToTests}*.js],\r\ndeprecationWarnings: false,\r\nmaxInstances: 1,\r\ncapabilities: [\r\n{\r\nbrowserName: 'internet explorer',\r\nplatform: 'WINDOWS',\r\nplatformName: 'windows',\r\nlogLevel: 'trace',\r\nlogFile: 'C:\\\\Users\\\\Administrator\\\\Downloads\\\\ielog.txt'\r\n'se:ieOptions': {\r\n'ie.ensureCleanSession': true,\r\nignoreZoomSetting: true,\r\nignoreProtectedModeSettings: true\r\n}\r\n}\r\n],\r\nsync: true,\r\n\r\n**Issue Description:**\r\nI want to generate logFile as specified in capabilities object. But it is not being generated. Any idea on what I am missing here?\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["506953346"], "labels":[]}