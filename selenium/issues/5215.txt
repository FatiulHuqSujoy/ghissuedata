{"id":"5215", "title":"32 bit Windows IE driver is blocked by McAfee", "body":"## Meta -
OS:  Windows 10
<!-- Windows 10? OSX? -->
Selenium Version:  3.8.1
<!-- 2.52.0, IDE, etc -->
Browser:  Internet Explorer
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
IEDriverServer_Win32_3.8.0.zip should not be detected as malware.
 
## Actual Behavior -
It is not possible to download the IEDriverServer_Win32_3.8.0.zip as it is blocked by McAfee in our corp network.
Virus analysis can be seen on this page:
https://www.virustotal.com/en/file/17d0d4095c6fdfd8b8699eefb8fadd7fcfa322f789d9cba43aafc5e40c88789a/analysis/

Compare to 64bit version (which is not being blocked):
https://www.virustotal.com/en/file/d1e633dece685e506d06159bd70850711d38fd47c74b1db241b9020db796b92f/analysis/

## Steps to reproduce -

- Try to download driver for 32 bit Windows IE from http://www.seleniumhq.org/download/ while having McAfee Web Gateway enabled
OR
- Verify the file on https://www.virustotal.com
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5215","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5215/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5215/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5215/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5215","id":281778051,"node_id":"MDU6SXNzdWUyODE3NzgwNTE=","number":5215,"title":"32 bit Windows IE driver is blocked by McAfee","user":{"login":"allaco","id":19384695,"node_id":"MDQ6VXNlcjE5Mzg0Njk1","avatar_url":"https://avatars1.githubusercontent.com/u/19384695?v=4","gravatar_id":"","url":"https://api.github.com/users/allaco","html_url":"https://github.com/allaco","followers_url":"https://api.github.com/users/allaco/followers","following_url":"https://api.github.com/users/allaco/following{/other_user}","gists_url":"https://api.github.com/users/allaco/gists{/gist_id}","starred_url":"https://api.github.com/users/allaco/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/allaco/subscriptions","organizations_url":"https://api.github.com/users/allaco/orgs","repos_url":"https://api.github.com/users/allaco/repos","events_url":"https://api.github.com/users/allaco/events{/privacy}","received_events_url":"https://api.github.com/users/allaco/received_events","type":"User","site_admin":false},"labels":[{"id":182505367,"node_id":"MDU6TGFiZWwxODI1MDUzNjc=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-IE","name":"D-IE","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2017-12-13T14:56:18Z","updated_at":"2019-08-16T20:09:40Z","closed_at":"2017-12-20T12:34:03Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.8.1\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Internet Explorer\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nIEDriverServer_Win32_3.8.0.zip should not be detected as malware.\r\n \r\n## Actual Behavior -\r\nIt is not possible to download the IEDriverServer_Win32_3.8.0.zip as it is blocked by McAfee in our corp network.\r\nVirus analysis can be seen on this page:\r\nhttps://www.virustotal.com/en/file/17d0d4095c6fdfd8b8699eefb8fadd7fcfa322f789d9cba43aafc5e40c88789a/analysis/\r\n\r\nCompare to 64bit version (which is not being blocked):\r\nhttps://www.virustotal.com/en/file/d1e633dece685e506d06159bd70850711d38fd47c74b1db241b9020db796b92f/analysis/\r\n\r\n## Steps to reproduce -\r\n\r\n- Try to download driver for 32 bit Windows IE from http://www.seleniumhq.org/download/ while having McAfee Web Gateway enabled\r\nOR\r\n- Verify the file on https://www.virustotal.com\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"allaco","id":19384695,"node_id":"MDQ6VXNlcjE5Mzg0Njk1","avatar_url":"https://avatars1.githubusercontent.com/u/19384695?v=4","gravatar_id":"","url":"https://api.github.com/users/allaco","html_url":"https://github.com/allaco","followers_url":"https://api.github.com/users/allaco/followers","following_url":"https://api.github.com/users/allaco/following{/other_user}","gists_url":"https://api.github.com/users/allaco/gists{/gist_id}","starred_url":"https://api.github.com/users/allaco/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/allaco/subscriptions","organizations_url":"https://api.github.com/users/allaco/orgs","repos_url":"https://api.github.com/users/allaco/repos","events_url":"https://api.github.com/users/allaco/events{/privacy}","received_events_url":"https://api.github.com/users/allaco/received_events","type":"User","site_admin":false}}", "commentIds":["351443097","352677746","352831004","352868907","353051754"], "labels":["D-IE"]}