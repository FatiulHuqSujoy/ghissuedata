{"id":"897", "title":"Can't enter fullscreen in Firefox", "body":"I'm using Protractor, a framework on top of WebDriver designed to be used with Angular. In my application we use fullscreen and would like to test that you enter fullscreen when clicking a certain button. In Chrome it works, but when run in Firefox the click doesn't have any effect. If I pause before the click and manually click the button, fullscreen is entered. 

Here is a minimalistic example of when it works in Chrome but not in Firefox:

``` html
<html ng-app=\"test\">

<head>
    <script src=\"https://code.angularjs.org/1.4.3/angular.min.js\"></script>
    <script>
        angular.module(\"test\", []).controller(function() {});
    </script>
</head>

<body>
    <video style=\"background-color: red;\" height=\"100\" width=\"100\"></video>
    <button onclick=\"enterFullscreen()\">Enter fullscreen</button>
    <script>
        var enterFullscreen = function() {
            var elem = document.querySelector(\"video\");
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            }
        };
    </script>
</body>

</html>
```

``` javascript
describe('fullscreen tests', function () {
    it('should enter fullscreen when clicking button', function () {
        browser.get(...);
        element(by.buttonText('Enter fullscreen')).click();
        browser.sleep(3000); // Expecting fullscreen to be entered
        browser.driver.manage().window().setSize(800, 400); // Exit fullscreen
    });
});
```

In Chrome fullscreen is entered, but in Firefox the button is just active and pressed, but nothing else happens. And what causes this to only happen when WebDriver click the button but not when I do it.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/897","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/897/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/897/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/897/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/897","id":99671283,"node_id":"MDU6SXNzdWU5OTY3MTI4Mw==","number":897,"title":"Can't enter fullscreen in Firefox","user":{"login":"eolognt","id":10940539,"node_id":"MDQ6VXNlcjEwOTQwNTM5","avatar_url":"https://avatars1.githubusercontent.com/u/10940539?v=4","gravatar_id":"","url":"https://api.github.com/users/eolognt","html_url":"https://github.com/eolognt","followers_url":"https://api.github.com/users/eolognt/followers","following_url":"https://api.github.com/users/eolognt/following{/other_user}","gists_url":"https://api.github.com/users/eolognt/gists{/gist_id}","starred_url":"https://api.github.com/users/eolognt/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/eolognt/subscriptions","organizations_url":"https://api.github.com/users/eolognt/orgs","repos_url":"https://api.github.com/users/eolognt/repos","events_url":"https://api.github.com/users/eolognt/events{/privacy}","received_events_url":"https://api.github.com/users/eolognt/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-08-07T15:29:58Z","updated_at":"2019-08-21T02:09:59Z","closed_at":"2015-09-20T09:17:04Z","author_association":"NONE","body":"I'm using Protractor, a framework on top of WebDriver designed to be used with Angular. In my application we use fullscreen and would like to test that you enter fullscreen when clicking a certain button. In Chrome it works, but when run in Firefox the click doesn't have any effect. If I pause before the click and manually click the button, fullscreen is entered. \n\nHere is a minimalistic example of when it works in Chrome but not in Firefox:\n\n``` html\n<html ng-app=\"test\">\n\n<head>\n    <script src=\"https://code.angularjs.org/1.4.3/angular.min.js\"></script>\n    <script>\n        angular.module(\"test\", []).controller(function() {});\n    </script>\n</head>\n\n<body>\n    <video style=\"background-color: red;\" height=\"100\" width=\"100\"></video>\n    <button onclick=\"enterFullscreen()\">Enter fullscreen</button>\n    <script>\n        var enterFullscreen = function() {\n            var elem = document.querySelector(\"video\");\n            if (elem.requestFullscreen) {\n                elem.requestFullscreen();\n            } else if (elem.msRequestFullscreen) {\n                elem.msRequestFullscreen();\n            } else if (elem.mozRequestFullScreen) {\n                elem.mozRequestFullScreen();\n            } else if (elem.webkitRequestFullscreen) {\n                elem.webkitRequestFullscreen();\n            }\n        };\n    </script>\n</body>\n\n</html>\n```\n\n``` javascript\ndescribe('fullscreen tests', function () {\n    it('should enter fullscreen when clicking button', function () {\n        browser.get(...);\n        element(by.buttonText('Enter fullscreen')).click();\n        browser.sleep(3000); // Expecting fullscreen to be entered\n        browser.driver.manage().window().setSize(800, 400); // Exit fullscreen\n    });\n});\n```\n\nIn Chrome fullscreen is entered, but in Firefox the button is just active and pressed, but nothing else happens. And what causes this to only happen when WebDriver click the button but not when I do it.\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["140199802","141759916","141787385"], "labels":["D-firefox"]}