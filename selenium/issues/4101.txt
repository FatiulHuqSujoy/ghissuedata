{"id":"4101", "title":"It is not longer possible to check if WebElement is gone from DOM with ExpectedConditions", "body":"## Meta -
OS:  N/A
Selenium Version:  3.4.0
Browser:  N/A
Browser Version:  N/A

On Selenium 3.0.1 it was possible to wait until a WebElement was not longer present in the DOM by:
```
public void waitForElementGone(WebElement webElement, int timeOutInMillis) {
  waits(timeOutInMillis)
      .until(ExpectedConditions.invisibilityOfAllElements(Collections.singletonList(webElement)));

}
```

Since the Java 8 refactor on ExpectedConditions class this does not longer work as the new `isInvisible()` method is not catching the `NoSuchElementException`:
```
    private static boolean isInvisible(WebElement element) {
        try {
            return !element.isDisplayed();
        } catch (StaleElementReferenceException var2) {
            return true;
        }
    }
```

`isInvisible()` should be implemented in a similar way as this other method that it is working correctly:
```
    public static ExpectedCondition<Boolean> invisibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    return Boolean.valueOf(!ExpectedConditions.findElement(locator, driver).isDisplayed());
                } catch (NoSuchElementException var3) {
                    return Boolean.valueOf(true);
                } catch (StaleElementReferenceException var4) {
                    return Boolean.valueOf(true);
                }
            }

            public String toString() {
                return \"element to no longer be visible: \" + locator;
            }
        };
    }
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4101","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4101/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4101/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4101/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4101","id":232199527,"node_id":"MDU6SXNzdWUyMzIxOTk1Mjc=","number":4101,"title":"It is not longer possible to check if WebElement is gone from DOM with ExpectedConditions","user":{"login":"VictorPascualV","id":7248978,"node_id":"MDQ6VXNlcjcyNDg5Nzg=","avatar_url":"https://avatars2.githubusercontent.com/u/7248978?v=4","gravatar_id":"","url":"https://api.github.com/users/VictorPascualV","html_url":"https://github.com/VictorPascualV","followers_url":"https://api.github.com/users/VictorPascualV/followers","following_url":"https://api.github.com/users/VictorPascualV/following{/other_user}","gists_url":"https://api.github.com/users/VictorPascualV/gists{/gist_id}","starred_url":"https://api.github.com/users/VictorPascualV/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/VictorPascualV/subscriptions","organizations_url":"https://api.github.com/users/VictorPascualV/orgs","repos_url":"https://api.github.com/users/VictorPascualV/repos","events_url":"https://api.github.com/users/VictorPascualV/events{/privacy}","received_events_url":"https://api.github.com/users/VictorPascualV/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-05-30T09:51:51Z","updated_at":"2019-08-17T02:09:40Z","closed_at":"2017-08-26T13:08:35Z","author_association":"NONE","body":"## Meta -\r\nOS:  N/A\r\nSelenium Version:  3.4.0\r\nBrowser:  N/A\r\nBrowser Version:  N/A\r\n\r\nOn Selenium 3.0.1 it was possible to wait until a WebElement was not longer present in the DOM by:\r\n```\r\npublic void waitForElementGone(WebElement webElement, int timeOutInMillis) {\r\n  waits(timeOutInMillis)\r\n      .until(ExpectedConditions.invisibilityOfAllElements(Collections.singletonList(webElement)));\r\n\r\n}\r\n```\r\n\r\nSince the Java 8 refactor on ExpectedConditions class this does not longer work as the new `isInvisible()` method is not catching the `NoSuchElementException`:\r\n```\r\n    private static boolean isInvisible(WebElement element) {\r\n        try {\r\n            return !element.isDisplayed();\r\n        } catch (StaleElementReferenceException var2) {\r\n            return true;\r\n        }\r\n    }\r\n```\r\n\r\n`isInvisible()` should be implemented in a similar way as this other method that it is working correctly:\r\n```\r\n    public static ExpectedCondition<Boolean> invisibilityOfElementLocated(final By locator) {\r\n        return new ExpectedCondition<Boolean>() {\r\n            public Boolean apply(WebDriver driver) {\r\n                try {\r\n                    return Boolean.valueOf(!ExpectedConditions.findElement(locator, driver).isDisplayed());\r\n                } catch (NoSuchElementException var3) {\r\n                    return Boolean.valueOf(true);\r\n                } catch (StaleElementReferenceException var4) {\r\n                    return Boolean.valueOf(true);\r\n                }\r\n            }\r\n\r\n            public String toString() {\r\n                return \"element to no longer be visible: \" + locator;\r\n            }\r\n        };\r\n    }\r\n```","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["319000410","325125858","345347472"], "labels":["C-java"]}