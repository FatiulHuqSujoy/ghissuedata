{"id":"6207", "title":"SessionNotCreatedException, tab crashed", "body":"[![enter image description here][1]][1]

I'd like to test multiple mobile user agents with selenium and chrome. I'm using python 3.6 and trying to deploy to heroku. I'm using headless chrome=68.0.3440.75 chromedriver 2.40.565383 . Based on http://chromedriver.chromium.org/mobile-emulation :

	def create_chromedriver(ua=False):
	    options = webdriver.ChromeOptions()
	    CHROMEDRIVER_PATH = os.getenv('$HOME') or basedir+'/chromedriver.exe'
	    FLASK_CONFIG = os.getenv('FLASK_CONFIG')



	    if FLASK_CONFIG and FLASK_CONFIG == \"production\":
	        # CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'
	        CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'
	        GOOGLE_CHROME_SHIM = os.getenv('$GOOGLE_CHROME_SHIM') or 'no path found'

	        print(GOOGLE_CHROME_SHIM)
	        print(GOOGLE_CHROME_SHIM)
	        options.add_argument(\"--headless\")
	        options.add_argument(\"--disable-gpu\")

	    if ua:
	        print('ua block33')

	        mobile_emulation =  {\"deviceName\": \"iPad Mini\"}
	        options.add_experimental_option(\"mobileEmulation\", mobile_emulation)

	    return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, chrome_options=options)



	def some_long_calculation():
	    driver = create_chromedriver('test')
	    # driver = create_chromedriver()
	    print(driver.capabilities['version'])
	    print(driver.capabilities['version'])

	    driver.get(\"https://www.yahoo.com/\")
	  

	    print(1)

	    png = driver.get_screenshot_as_png()
	    driver.close()

	    # return 'a'
	    return png

On both windows locally and if I run on heroku with When I run this project on heroku  using 	

    def some_long_calculation():
	    # driver = create_chromedriver('test')
	    driver = create_chromedriver()

I get the expected yahoo screenshot

However using :

    def some_long_calculation():
	    driver = create_chromedriver('test')

results in:


	2018-07-26T15:08:21.448393+00:00 heroku[web.1]: State changed from starting to up
	2018-07-26T15:08:23.555153+00:00 app[web.1]: no path found
	2018-07-26T15:08:23.555197+00:00 app[web.1]: no path found
	2018-07-26T15:08:23.555199+00:00 app[web.1]: ua block33
	2018-07-26T15:08:25.772281+00:00 heroku[router]: at=info method=GET path=\"/\" host=mobiletest16.herokuapp.com request_id=4f72f6b0-3d8f-418f-a01b-a0281c4f644d fwd=\"54.86.59.209\" dyno=web.1 connect=0ms service=2219ms status=500 bytes=456 protocol=https
	2018-07-26T15:08:25.770879+00:00 app[web.1]: [2018-07-26 15:08:25,756] ERROR in app: Exception on / [GET]
	2018-07-26T15:08:25.770892+00:00 app[web.1]: Traceback (most recent call last):
	2018-07-26T15:08:25.770894+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 2292, in wsgi_app
	2018-07-26T15:08:25.770895+00:00 app[web.1]:     response = self.full_dispatch_request()
	2018-07-26T15:08:25.770897+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1815, in full_dispatch_request
	2018-07-26T15:08:25.770899+00:00 app[web.1]:     rv = self.handle_user_exception(e)
	2018-07-26T15:08:25.770900+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1718, in handle_user_exception
	2018-07-26T15:08:25.770902+00:00 app[web.1]:     reraise(exc_type, exc_value, tb)
	2018-07-26T15:08:25.770904+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/_compat.py\", line 35, in reraise
	2018-07-26T15:08:25.770907+00:00 app[web.1]:     raise value
	2018-07-26T15:08:25.770908+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1813, in full_dispatch_request
	2018-07-26T15:08:25.770910+00:00 app[web.1]:     rv = self.dispatch_request()
	2018-07-26T15:08:25.770911+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1799, in dispatch_request
	2018-07-26T15:08:25.770913+00:00 app[web.1]:     return self.view_functions[rule.endpoint](**req.view_args)
	2018-07-26T15:08:25.770914+00:00 app[web.1]:   File \"/app/err_test.py\", line 115, in sanity_check
	2018-07-26T15:08:25.770916+00:00 app[web.1]:     png = some_long_calculation()
	2018-07-26T15:08:25.770918+00:00 app[web.1]:   File \"/app/err_test.py\", line 89, in some_long_calculation
	2018-07-26T15:08:25.770919+00:00 app[web.1]:     driver = create_chromedriver('test')
	2018-07-26T15:08:25.770921+00:00 app[web.1]:   File \"/app/err_test.py\", line 78, in create_chromedriver
	2018-07-26T15:08:25.770923+00:00 app[web.1]:     return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)
	2018-07-26T15:08:25.770925+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__
	2018-07-26T15:08:25.770926+00:00 app[web.1]:     desired_capabilities=desired_capabilities)
	2018-07-26T15:08:25.770928+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 156, in __init__
	2018-07-26T15:08:25.770929+00:00 app[web.1]:     self.start_session(capabilities, browser_profile)
	2018-07-26T15:08:25.770931+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 251, in start_session
	2018-07-26T15:08:25.770933+00:00 app[web.1]:     response = self.execute(Command.NEW_SESSION, parameters)
	2018-07-26T15:08:25.770934+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 320, in execute
	2018-07-26T15:08:25.770936+00:00 app[web.1]:     self.error_handler.check_response(response)
	2018-07-26T15:08:25.770937+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response
	2018-07-26T15:08:25.770939+00:00 app[web.1]:     raise exception_class(message, screen, stacktrace)
	2018-07-26T15:08:25.770944+00:00 app[web.1]: selenium.common.exceptions.SessionNotCreatedException: Message: session not created exception
	2018-07-26T15:08:25.770946+00:00 app[web.1]: from tab crashed
	2018-07-26T15:08:25.770948+00:00 app[web.1]:   (Session info: headless chrome=68.0.3440.75)
	2018-07-26T15:08:25.770949+00:00 app[web.1]:   (Driver info: chromedriver=2.40.565383 (76257d1ab79276b2d53ee976b2c3e3b9f335cde7),platform=Linux 4.4.0-1019-aws x86_64)
	2018-07-26T15:08:25.771063+00:00 app[web.1]: 
	2018-07-26T15:08:25.773980+00:00 app[web.1]: 10.30.235.45 - - [26/Jul/2018:15:08:25 +0000] \"GET / HTTP/1.1\" 500 291 \"https://dashboard.heroku.com/\" \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\"


You can download my project for both windows and heroku use at:

    https://github.com/kc1/mobiletest

(keep in mind that if you deploy to heroku you have to set FLASK_CONFIG to production. Also you will need to add the 3 buildpacks shown in the screenshot.)




  [1]: https://i.stack.imgur.com/UI0ua.png
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6207","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6207/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6207/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6207/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6207","id":344920595,"node_id":"MDU6SXNzdWUzNDQ5MjA1OTU=","number":6207,"title":"SessionNotCreatedException, tab crashed","user":{"login":"kc1","id":1745710,"node_id":"MDQ6VXNlcjE3NDU3MTA=","avatar_url":"https://avatars1.githubusercontent.com/u/1745710?v=4","gravatar_id":"","url":"https://api.github.com/users/kc1","html_url":"https://github.com/kc1","followers_url":"https://api.github.com/users/kc1/followers","following_url":"https://api.github.com/users/kc1/following{/other_user}","gists_url":"https://api.github.com/users/kc1/gists{/gist_id}","starred_url":"https://api.github.com/users/kc1/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kc1/subscriptions","organizations_url":"https://api.github.com/users/kc1/orgs","repos_url":"https://api.github.com/users/kc1/repos","events_url":"https://api.github.com/users/kc1/events{/privacy}","received_events_url":"https://api.github.com/users/kc1/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2018-07-26T16:41:35Z","updated_at":"2019-08-15T17:09:54Z","closed_at":"2018-07-26T16:46:09Z","author_association":"NONE","body":"[![enter image description here][1]][1]\r\n\r\nI'd like to test multiple mobile user agents with selenium and chrome. I'm using python 3.6 and trying to deploy to heroku. I'm using headless chrome=68.0.3440.75 chromedriver 2.40.565383 . Based on http://chromedriver.chromium.org/mobile-emulation :\r\n\r\n\tdef create_chromedriver(ua=False):\r\n\t    options = webdriver.ChromeOptions()\r\n\t    CHROMEDRIVER_PATH = os.getenv('$HOME') or basedir+'/chromedriver.exe'\r\n\t    FLASK_CONFIG = os.getenv('FLASK_CONFIG')\r\n\r\n\r\n\r\n\t    if FLASK_CONFIG and FLASK_CONFIG == \"production\":\r\n\t        # CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'\r\n\t        CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'\r\n\t        GOOGLE_CHROME_SHIM = os.getenv('$GOOGLE_CHROME_SHIM') or 'no path found'\r\n\r\n\t        print(GOOGLE_CHROME_SHIM)\r\n\t        print(GOOGLE_CHROME_SHIM)\r\n\t        options.add_argument(\"--headless\")\r\n\t        options.add_argument(\"--disable-gpu\")\r\n\r\n\t    if ua:\r\n\t        print('ua block33')\r\n\r\n\t        mobile_emulation =  {\"deviceName\": \"iPad Mini\"}\r\n\t        options.add_experimental_option(\"mobileEmulation\", mobile_emulation)\r\n\r\n\t    return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, chrome_options=options)\r\n\r\n\r\n\r\n\tdef some_long_calculation():\r\n\t    driver = create_chromedriver('test')\r\n\t    # driver = create_chromedriver()\r\n\t    print(driver.capabilities['version'])\r\n\t    print(driver.capabilities['version'])\r\n\r\n\t    driver.get(\"https://www.yahoo.com/\")\r\n\t  \r\n\r\n\t    print(1)\r\n\r\n\t    png = driver.get_screenshot_as_png()\r\n\t    driver.close()\r\n\r\n\t    # return 'a'\r\n\t    return png\r\n\r\nOn both windows locally and if I run on heroku with When I run this project on heroku  using \t\r\n\r\n    def some_long_calculation():\r\n\t    # driver = create_chromedriver('test')\r\n\t    driver = create_chromedriver()\r\n\r\nI get the expected yahoo screenshot\r\n\r\nHowever using :\r\n\r\n    def some_long_calculation():\r\n\t    driver = create_chromedriver('test')\r\n\r\nresults in:\r\n\r\n\r\n\t2018-07-26T15:08:21.448393+00:00 heroku[web.1]: State changed from starting to up\r\n\t2018-07-26T15:08:23.555153+00:00 app[web.1]: no path found\r\n\t2018-07-26T15:08:23.555197+00:00 app[web.1]: no path found\r\n\t2018-07-26T15:08:23.555199+00:00 app[web.1]: ua block33\r\n\t2018-07-26T15:08:25.772281+00:00 heroku[router]: at=info method=GET path=\"/\" host=mobiletest16.herokuapp.com request_id=4f72f6b0-3d8f-418f-a01b-a0281c4f644d fwd=\"54.86.59.209\" dyno=web.1 connect=0ms service=2219ms status=500 bytes=456 protocol=https\r\n\t2018-07-26T15:08:25.770879+00:00 app[web.1]: [2018-07-26 15:08:25,756] ERROR in app: Exception on / [GET]\r\n\t2018-07-26T15:08:25.770892+00:00 app[web.1]: Traceback (most recent call last):\r\n\t2018-07-26T15:08:25.770894+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 2292, in wsgi_app\r\n\t2018-07-26T15:08:25.770895+00:00 app[web.1]:     response = self.full_dispatch_request()\r\n\t2018-07-26T15:08:25.770897+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1815, in full_dispatch_request\r\n\t2018-07-26T15:08:25.770899+00:00 app[web.1]:     rv = self.handle_user_exception(e)\r\n\t2018-07-26T15:08:25.770900+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1718, in handle_user_exception\r\n\t2018-07-26T15:08:25.770902+00:00 app[web.1]:     reraise(exc_type, exc_value, tb)\r\n\t2018-07-26T15:08:25.770904+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/_compat.py\", line 35, in reraise\r\n\t2018-07-26T15:08:25.770907+00:00 app[web.1]:     raise value\r\n\t2018-07-26T15:08:25.770908+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1813, in full_dispatch_request\r\n\t2018-07-26T15:08:25.770910+00:00 app[web.1]:     rv = self.dispatch_request()\r\n\t2018-07-26T15:08:25.770911+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/flask/app.py\", line 1799, in dispatch_request\r\n\t2018-07-26T15:08:25.770913+00:00 app[web.1]:     return self.view_functions[rule.endpoint](**req.view_args)\r\n\t2018-07-26T15:08:25.770914+00:00 app[web.1]:   File \"/app/err_test.py\", line 115, in sanity_check\r\n\t2018-07-26T15:08:25.770916+00:00 app[web.1]:     png = some_long_calculation()\r\n\t2018-07-26T15:08:25.770918+00:00 app[web.1]:   File \"/app/err_test.py\", line 89, in some_long_calculation\r\n\t2018-07-26T15:08:25.770919+00:00 app[web.1]:     driver = create_chromedriver('test')\r\n\t2018-07-26T15:08:25.770921+00:00 app[web.1]:   File \"/app/err_test.py\", line 78, in create_chromedriver\r\n\t2018-07-26T15:08:25.770923+00:00 app[web.1]:     return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)\r\n\t2018-07-26T15:08:25.770925+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__\r\n\t2018-07-26T15:08:25.770926+00:00 app[web.1]:     desired_capabilities=desired_capabilities)\r\n\t2018-07-26T15:08:25.770928+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 156, in __init__\r\n\t2018-07-26T15:08:25.770929+00:00 app[web.1]:     self.start_session(capabilities, browser_profile)\r\n\t2018-07-26T15:08:25.770931+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 251, in start_session\r\n\t2018-07-26T15:08:25.770933+00:00 app[web.1]:     response = self.execute(Command.NEW_SESSION, parameters)\r\n\t2018-07-26T15:08:25.770934+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 320, in execute\r\n\t2018-07-26T15:08:25.770936+00:00 app[web.1]:     self.error_handler.check_response(response)\r\n\t2018-07-26T15:08:25.770937+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response\r\n\t2018-07-26T15:08:25.770939+00:00 app[web.1]:     raise exception_class(message, screen, stacktrace)\r\n\t2018-07-26T15:08:25.770944+00:00 app[web.1]: selenium.common.exceptions.SessionNotCreatedException: Message: session not created exception\r\n\t2018-07-26T15:08:25.770946+00:00 app[web.1]: from tab crashed\r\n\t2018-07-26T15:08:25.770948+00:00 app[web.1]:   (Session info: headless chrome=68.0.3440.75)\r\n\t2018-07-26T15:08:25.770949+00:00 app[web.1]:   (Driver info: chromedriver=2.40.565383 (76257d1ab79276b2d53ee976b2c3e3b9f335cde7),platform=Linux 4.4.0-1019-aws x86_64)\r\n\t2018-07-26T15:08:25.771063+00:00 app[web.1]: \r\n\t2018-07-26T15:08:25.773980+00:00 app[web.1]: 10.30.235.45 - - [26/Jul/2018:15:08:25 +0000] \"GET / HTTP/1.1\" 500 291 \"https://dashboard.heroku.com/\" \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\"\r\n\r\n\r\nYou can download my project for both windows and heroku use at:\r\n\r\n    https://github.com/kc1/mobiletest\r\n\r\n(keep in mind that if you deploy to heroku you have to set FLASK_CONFIG to production. Also you will need to add the 3 buildpacks shown in the screenshot.)\r\n\r\n\r\n\r\n\r\n  [1]: https://i.stack.imgur.com/UI0ua.png\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["408161091","408163668","408168597","408175324"], "labels":[]}