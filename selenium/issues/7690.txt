{"id":"7690", "title":"Unable to spawn Edge browser with WebDriverException: Message: Unknown error", "body":"## 🐛 Bug Report

I'm unable to launch Edge in Selenium with Python. When trying to create the driver I get:
```
Traceback (most recent call last):
  File \"<stdin>\", line 1, in <module>
  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__
    desired_capabilities=capabilities)
  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__
    self.start_session(capabilities, browser_profile)
  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session
    response = self.execute(Command.NEW_SESSION, parameters)
  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response
    raise exception_class(value)
selenium.common.exceptions.WebDriverException: Message: Unknown error
```

Windows version: Windows 10 Pro, 1809, 17763.805, running in Azure
Selenium version: 3.141.0
Python version: 3.7.4 64-bit (the one from here: https://www.python.org/downloads/release/python-374/)
Java version: 1.8.0_221
EdgeDriver version: 10.0.17763.1 (from `MicrosoftWebDriver.exe --version`)

## To Reproduce

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

Detailed steps to reproduce the behavior:

Install Python, Java.
Add EdgeDriver to Windows via: PC Settings -> Search \"optional features\" -> Add a feature -> Install Microsoft WebDriver. Then reboot to make it appear in `C:\\Windows\\System32\\`.

Then inside command prompt:
```
cmd> pip install selenium
cmd> python
>>> from selenium import webdriver
>>> driver = webdriver.Edge()
Traceback (most recent call last):
  File \"<stdin>\", line 1, in <module>
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__
    desired_capabilities=capabilities)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__
    self.start_session(capabilities, browser_profile)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session
    response = self.execute(Command.NEW_SESSION, parameters)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response
    raise exception_class(value)
selenium.common.exceptions.WebDriverException: Message: Unknown error

>>> driver = webdriver.Edge(executable_path=\"C:\\\\Windows\\\\System32\\\\MicrosoftWebDriver.exe\")
Traceback (most recent call last):
  File \"<stdin>\", line 1, in <module>
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__
    desired_capabilities=capabilities)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__
    self.start_session(capabilities, browser_profile)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session
    response = self.execute(Command.NEW_SESSION, parameters)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response
    raise exception_class(value)
selenium.common.exceptions.WebDriverException: Message: Unknown error
```

I've also checked in Task Manager that no instances of Edge (as I know it doesn't have multiple profiles) or MicrosoftWebDriver are already running. If any were, I killed them off.

## Expected behavior

Edge launches as all the other browsers I've tried do and I can then drive it via my `driver` instance.

## Test script or set of commands reproducing this issue

See detailed steps to reproduce above.

## Environment

OS: Windows 10 Pro, 1809, 17763.805, running in Azure
Browser: Edge
Browser version: Microsoft Edge 44.17763.771.0
Browser Driver version: 10.0.17763.1 (from `MicrosoftWebDriver.exe --version`)
Language Bindings version: Python 3.7.4 64-bit 
Selenium version (if applicable): 3.141.0 
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7690","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7690/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7690/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7690/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7690","id":507875301,"node_id":"MDU6SXNzdWU1MDc4NzUzMDE=","number":7690,"title":"Unable to spawn Edge browser with WebDriverException: Message: Unknown error","user":{"login":"Jamie-","id":4096784,"node_id":"MDQ6VXNlcjQwOTY3ODQ=","avatar_url":"https://avatars2.githubusercontent.com/u/4096784?v=4","gravatar_id":"","url":"https://api.github.com/users/Jamie-","html_url":"https://github.com/Jamie-","followers_url":"https://api.github.com/users/Jamie-/followers","following_url":"https://api.github.com/users/Jamie-/following{/other_user}","gists_url":"https://api.github.com/users/Jamie-/gists{/gist_id}","starred_url":"https://api.github.com/users/Jamie-/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Jamie-/subscriptions","organizations_url":"https://api.github.com/users/Jamie-/orgs","repos_url":"https://api.github.com/users/Jamie-/repos","events_url":"https://api.github.com/users/Jamie-/events{/privacy}","received_events_url":"https://api.github.com/users/Jamie-/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-10-16T14:05:37Z","updated_at":"2019-10-28T17:20:40Z","closed_at":"2019-10-28T17:20:40Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nI'm unable to launch Edge in Selenium with Python. When trying to create the driver I get:\r\n```\r\nTraceback (most recent call last):\r\n  File \"<stdin>\", line 1, in <module>\r\n  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__\r\n    desired_capabilities=capabilities)\r\n  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__\r\n    self.start_session(capabilities, browser_profile)\r\n  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session\r\n    response = self.execute(Command.NEW_SESSION, parameters)\r\n  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"C:\\Python\\Python37-32\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response\r\n    raise exception_class(value)\r\nselenium.common.exceptions.WebDriverException: Message: Unknown error\r\n```\r\n\r\nWindows version: Windows 10 Pro, 1809, 17763.805, running in Azure\r\nSelenium version: 3.141.0\r\nPython version: 3.7.4 64-bit (the one from here: https://www.python.org/downloads/release/python-374/)\r\nJava version: 1.8.0_221\r\nEdgeDriver version: 10.0.17763.1 (from `MicrosoftWebDriver.exe --version`)\r\n\r\n## To Reproduce\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\nDetailed steps to reproduce the behavior:\r\n\r\nInstall Python, Java.\r\nAdd EdgeDriver to Windows via: PC Settings -> Search \"optional features\" -> Add a feature -> Install Microsoft WebDriver. Then reboot to make it appear in `C:\\Windows\\System32\\`.\r\n\r\nThen inside command prompt:\r\n```\r\ncmd> pip install selenium\r\ncmd> python\r\n>>> from selenium import webdriver\r\n>>> driver = webdriver.Edge()\r\nTraceback (most recent call last):\r\n  File \"<stdin>\", line 1, in <module>\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__\r\n    desired_capabilities=capabilities)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__\r\n    self.start_session(capabilities, browser_profile)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session\r\n    response = self.execute(Command.NEW_SESSION, parameters)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response\r\n    raise exception_class(value)\r\nselenium.common.exceptions.WebDriverException: Message: Unknown error\r\n\r\n>>> driver = webdriver.Edge(executable_path=\"C:\\\\Windows\\\\System32\\\\MicrosoftWebDriver.exe\")\r\nTraceback (most recent call last):\r\n  File \"<stdin>\", line 1, in <module>\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\edge\\webdriver.py\", line 66, in __init__\r\n    desired_capabilities=capabilities)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 157, in __init__\r\n    self.start_session(capabilities, browser_profile)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 252, in start_session\r\n    response = self.execute(Command.NEW_SESSION, parameters)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 208, in check_response\r\n    raise exception_class(value)\r\nselenium.common.exceptions.WebDriverException: Message: Unknown error\r\n```\r\n\r\nI've also checked in Task Manager that no instances of Edge (as I know it doesn't have multiple profiles) or MicrosoftWebDriver are already running. If any were, I killed them off.\r\n\r\n## Expected behavior\r\n\r\nEdge launches as all the other browsers I've tried do and I can then drive it via my `driver` instance.\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\nSee detailed steps to reproduce above.\r\n\r\n## Environment\r\n\r\nOS: Windows 10 Pro, 1809, 17763.805, running in Azure\r\nBrowser: Edge\r\nBrowser version: Microsoft Edge 44.17763.771.0\r\nBrowser Driver version: 10.0.17763.1 (from `MicrosoftWebDriver.exe --version`)\r\nLanguage Bindings version: Python 3.7.4 64-bit \r\nSelenium version (if applicable): 3.141.0 \r\n","closed_by":{"login":"Jamie-","id":4096784,"node_id":"MDQ6VXNlcjQwOTY3ODQ=","avatar_url":"https://avatars2.githubusercontent.com/u/4096784?v=4","gravatar_id":"","url":"https://api.github.com/users/Jamie-","html_url":"https://github.com/Jamie-","followers_url":"https://api.github.com/users/Jamie-/followers","following_url":"https://api.github.com/users/Jamie-/following{/other_user}","gists_url":"https://api.github.com/users/Jamie-/gists{/gist_id}","starred_url":"https://api.github.com/users/Jamie-/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Jamie-/subscriptions","organizations_url":"https://api.github.com/users/Jamie-/orgs","repos_url":"https://api.github.com/users/Jamie-/repos","events_url":"https://api.github.com/users/Jamie-/events{/privacy}","received_events_url":"https://api.github.com/users/Jamie-/received_events","type":"User","site_admin":false}}", "commentIds":["547054712"], "labels":[]}