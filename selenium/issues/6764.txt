{"id":"6764", "title":"DevToolsActivePort file doesn't exist (Driver info: chromedriver=2.45.615291,platform=Windows NT 6.1.7601 SP1 x86_64)", "body":"**Issue Description
============**
Chromedriver fails to spin up Chrome browser when tests are run (sequentially). Out of 100 tests typically such failures would impact 10-20 tests.
I've tried various chromedriver and chrome versions but none of them seems to make the issue any better.  Since this is an intermittent issue I'm unable to provide a 100% reproducible case, but hope the exception stack trace and other details below helps.


**Bindings and Versions
===============**
Chrome Version -  71.0.3578.80
\"Selenium.Support\" version=\"3.10.0\" targetFramework=\"net461\" 
 \"Selenium.WebDriver\" version=\"3.10.0\" targetFramework=\"net461\"
 \"Selenium.WebDriver.ChromeDriver\" version=\"2.45.0\" targetFramework=\"net461\" 


**Exception Details 
============**
**Exception Message** : unknown error: DevToolsActivePort file doesn't exist (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 6.1.7601 SP1 x86_64)

**Exception StackTrace** : at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse) at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters) at OpenQA.Selenium.Remote.RemoteWebDriver.StartSession(ICapabilities desiredCapabilities) at OpenQA.Selenium.Remote.RemoteWebDriver..ctor(ICommandExecutor commandExecutor, ICapabilities desiredCapabilities) at OpenQA.Selenium.Chrome.ChromeDriver..ctor(ChromeDriverService service, ChromeOptions options, TimeSpan commandTimeout) at OpenQA.Selenium.Chrome.ChromeDriver..ctor(String chromeDriverDirectory, ChromeOptions options, TimeSpan commandTimeout) at Common.BaseHooks.GetSeleniumDriver(TimeSpan commandTimeout) 

Exception Source : WebDriver


**Code which instantiates Chromedriver Instance :** 
==================================

       ChromeOptions chromeOptions = new ChromeOptions();
	chromeOptions.Proxy = proxy;
	chromeOptions.BinaryLocation = PathHelper.GetBrowserAbsolutePath(browser);
	chromeOptions.AddArgument(\"--start-maximized\"); 
	chromeOptions.AddArgument(\"--disable-infobars\"); 
	chromeOptions.AddArgument(\"--disable-extensions\"); 
	chromeOptions.AddArgument(\"--disable-gpu\"); 					
	chromeOptions.AddArgument(\"--disable-dev-shm-usage\"); 
	chromeOptions.AddArgument(\"--no-sandbox\");
	chromeOptions.AddUserProfilePreference(\"download.default_directory\", downloadDirectory);
	chromeOptions.AddUserProfilePreference(\"download.prompt_for_download\", false);
	chromeOptions.AddUserProfilePreference(\"disable-popup-blocking\", \"true\");				
					
	driver = new ChromeDriver(AssemblyDirectory, chromeOptions, commandTimeout)


**The next line from where the exception is being thrown is :	driver.Manage().Window.Maximize();**

					
Attached is the test log from the execution, PFA.

![image](https://user-images.githubusercontent.com/33328317/50104552-53259580-01f8-11e9-9cce-2e8dfc821688.png)


", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6764","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6764/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6764/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6764/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6764","id":391820071,"node_id":"MDU6SXNzdWUzOTE4MjAwNzE=","number":6764,"title":"DevToolsActivePort file doesn't exist (Driver info: chromedriver=2.45.615291,platform=Windows NT 6.1.7601 SP1 x86_64)","user":{"login":"599139","id":33328317,"node_id":"MDQ6VXNlcjMzMzI4MzE3","avatar_url":"https://avatars1.githubusercontent.com/u/33328317?v=4","gravatar_id":"","url":"https://api.github.com/users/599139","html_url":"https://github.com/599139","followers_url":"https://api.github.com/users/599139/followers","following_url":"https://api.github.com/users/599139/following{/other_user}","gists_url":"https://api.github.com/users/599139/gists{/gist_id}","starred_url":"https://api.github.com/users/599139/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/599139/subscriptions","organizations_url":"https://api.github.com/users/599139/orgs","repos_url":"https://api.github.com/users/599139/repos","events_url":"https://api.github.com/users/599139/events{/privacy}","received_events_url":"https://api.github.com/users/599139/received_events","type":"User","site_admin":false},"labels":[{"id":182503933,"node_id":"MDU6TGFiZWwxODI1MDM5MzM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-dotnet","name":"C-dotnet","color":"fbca04","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":9,"created_at":"2018-12-17T17:41:15Z","updated_at":"2019-08-15T01:10:01Z","closed_at":"2018-12-20T06:42:29Z","author_association":"NONE","body":"**Issue Description\r\n============**\r\nChromedriver fails to spin up Chrome browser when tests are run (sequentially). Out of 100 tests typically such failures would impact 10-20 tests.\r\nI've tried various chromedriver and chrome versions but none of them seems to make the issue any better.  Since this is an intermittent issue I'm unable to provide a 100% reproducible case, but hope the exception stack trace and other details below helps.\r\n\r\n\r\n**Bindings and Versions\r\n===============**\r\nChrome Version -  71.0.3578.80\r\n\"Selenium.Support\" version=\"3.10.0\" targetFramework=\"net461\" \r\n \"Selenium.WebDriver\" version=\"3.10.0\" targetFramework=\"net461\"\r\n \"Selenium.WebDriver.ChromeDriver\" version=\"2.45.0\" targetFramework=\"net461\" \r\n\r\n\r\n**Exception Details \r\n============**\r\n**Exception Message** : unknown error: DevToolsActivePort file doesn't exist (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 6.1.7601 SP1 x86_64)\r\n\r\n**Exception StackTrace** : at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse) at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters) at OpenQA.Selenium.Remote.RemoteWebDriver.StartSession(ICapabilities desiredCapabilities) at OpenQA.Selenium.Remote.RemoteWebDriver..ctor(ICommandExecutor commandExecutor, ICapabilities desiredCapabilities) at OpenQA.Selenium.Chrome.ChromeDriver..ctor(ChromeDriverService service, ChromeOptions options, TimeSpan commandTimeout) at OpenQA.Selenium.Chrome.ChromeDriver..ctor(String chromeDriverDirectory, ChromeOptions options, TimeSpan commandTimeout) at Common.BaseHooks.GetSeleniumDriver(TimeSpan commandTimeout) \r\n\r\nException Source : WebDriver\r\n\r\n\r\n**Code which instantiates Chromedriver Instance :** \r\n==================================\r\n\r\n       ChromeOptions chromeOptions = new ChromeOptions();\r\n\tchromeOptions.Proxy = proxy;\r\n\tchromeOptions.BinaryLocation = PathHelper.GetBrowserAbsolutePath(browser);\r\n\tchromeOptions.AddArgument(\"--start-maximized\"); \r\n\tchromeOptions.AddArgument(\"--disable-infobars\"); \r\n\tchromeOptions.AddArgument(\"--disable-extensions\"); \r\n\tchromeOptions.AddArgument(\"--disable-gpu\"); \t\t\t\t\t\r\n\tchromeOptions.AddArgument(\"--disable-dev-shm-usage\"); \r\n\tchromeOptions.AddArgument(\"--no-sandbox\");\r\n\tchromeOptions.AddUserProfilePreference(\"download.default_directory\", downloadDirectory);\r\n\tchromeOptions.AddUserProfilePreference(\"download.prompt_for_download\", false);\r\n\tchromeOptions.AddUserProfilePreference(\"disable-popup-blocking\", \"true\");\t\t\t\t\r\n\t\t\t\t\t\r\n\tdriver = new ChromeDriver(AssemblyDirectory, chromeOptions, commandTimeout)\r\n\r\n\r\n**The next line from where the exception is being thrown is :\tdriver.Manage().Window.Maximize();**\r\n\r\n\t\t\t\t\t\r\nAttached is the test log from the execution, PFA.\r\n\r\n![image](https://user-images.githubusercontent.com/33328317/50104552-53259580-01f8-11e9-9cce-2e8dfc821688.png)\r\n\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["448073816","448354833","448458865","448752113","448775766","448882880","448891626","448891924","449011722"], "labels":["C-dotnet","R-awaiting answer"]}