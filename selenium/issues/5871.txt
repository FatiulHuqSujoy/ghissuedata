{"id":"5871", "title":"Issues with set_page_load_timeout", "body":"## Meta -
OS:  
Ubuntu 16.04.4 LTS
Selenium Version:  
3.11.0
Browser:  
GeckoDriver

Browser Version:  
0.20.1 (If I go to help -> about Firefox in the window that opens with GeckoDriver is launched, it says \"Firefox 59.0.2\")

## Expected Behavior -
I am trying to get the contents on a page when it first loads. If the Javascript on the page hasn't finished running after 10 seconds, I just want to get whatever was loaded. However, set_page_load_timeout does not seem to work reliably. 

## Actual Behavior -
If I call set_page_load_timeout and pass the value 10 the code hangs on the driver.get line after 10 seconds have passed. However if I pass the value 1, it works fine. 

## Steps to reproduce -
I created the following code snippet that demonstrates the problem:
```
driver = webdriver.Firefox(executable_path=<Path to geckodriver executable>)
driver.set_page_load_timeout(10)

try:
    driver.get('https://yahoo.com')
except:
    pass

print(\"HERE\")
print(driver.page_source)
print(\"END\")
```

Note that yahoo.com doesn't finish loading for me in Firefox 59.0.2 on Linux. The page partly loads but the little blue dot on the left of the tab keeps moving back on forth. If Yahoo loads properly for you (as it does for me on Firefox on macOS, you probably won't be able to reproduce this issue using Yahoo.

Is this an actual bug with set_page_load_timeout? If not, is there something that I'm doing wrong? I apologize if this is an issue I should be bringing up with Mozilla. It seemed like a Selenium issue because set_page_load_timeout worked fine with 1 but not with 10. ", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5871","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5871/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5871/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5871/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5871","id":321357093,"node_id":"MDU6SXNzdWUzMjEzNTcwOTM=","number":5871,"title":"Issues with set_page_load_timeout","user":{"login":"rogerthat94","id":2359046,"node_id":"MDQ6VXNlcjIzNTkwNDY=","avatar_url":"https://avatars3.githubusercontent.com/u/2359046?v=4","gravatar_id":"","url":"https://api.github.com/users/rogerthat94","html_url":"https://github.com/rogerthat94","followers_url":"https://api.github.com/users/rogerthat94/followers","following_url":"https://api.github.com/users/rogerthat94/following{/other_user}","gists_url":"https://api.github.com/users/rogerthat94/gists{/gist_id}","starred_url":"https://api.github.com/users/rogerthat94/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rogerthat94/subscriptions","organizations_url":"https://api.github.com/users/rogerthat94/orgs","repos_url":"https://api.github.com/users/rogerthat94/repos","events_url":"https://api.github.com/users/rogerthat94/events{/privacy}","received_events_url":"https://api.github.com/users/rogerthat94/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-05-08T21:42:09Z","updated_at":"2019-08-16T02:10:02Z","closed_at":"2018-05-09T00:43:04Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nUbuntu 16.04.4 LTS\r\nSelenium Version:  \r\n3.11.0\r\nBrowser:  \r\nGeckoDriver\r\n\r\nBrowser Version:  \r\n0.20.1 (If I go to help -> about Firefox in the window that opens with GeckoDriver is launched, it says \"Firefox 59.0.2\")\r\n\r\n## Expected Behavior -\r\nI am trying to get the contents on a page when it first loads. If the Javascript on the page hasn't finished running after 10 seconds, I just want to get whatever was loaded. However, set_page_load_timeout does not seem to work reliably. \r\n\r\n## Actual Behavior -\r\nIf I call set_page_load_timeout and pass the value 10 the code hangs on the driver.get line after 10 seconds have passed. However if I pass the value 1, it works fine. \r\n\r\n## Steps to reproduce -\r\nI created the following code snippet that demonstrates the problem:\r\n```\r\ndriver = webdriver.Firefox(executable_path=<Path to geckodriver executable>)\r\ndriver.set_page_load_timeout(10)\r\n\r\ntry:\r\n    driver.get('https://yahoo.com')\r\nexcept:\r\n    pass\r\n\r\nprint(\"HERE\")\r\nprint(driver.page_source)\r\nprint(\"END\")\r\n```\r\n\r\nNote that yahoo.com doesn't finish loading for me in Firefox 59.0.2 on Linux. The page partly loads but the little blue dot on the left of the tab keeps moving back on forth. If Yahoo loads properly for you (as it does for me on Firefox on macOS, you probably won't be able to reproduce this issue using Yahoo.\r\n\r\nIs this an actual bug with set_page_load_timeout? If not, is there something that I'm doing wrong? I apologize if this is an issue I should be bringing up with Mozilla. It seemed like a Selenium issue because set_page_load_timeout worked fine with 1 but not with 10. ","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["387585341","387602042"], "labels":[]}