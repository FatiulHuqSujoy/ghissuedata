{"id":"463", "title":"After operation on the page at 1st url, the 2nd url is not loaded by webdriver leading to fails in locating elements", "body":"The following java code

``` java
package ua.net.itlabs;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TwoUrlsTest {

    @Test
    public void test2Urls(){
        WebDriver d = new FirefoxDriver();
        d.get(\"http://google.com/ncr\");
        d.findElement(By.cssSelector(\"[name='q']\")).sendKeys(\"something\");
        d.findElement(By.cssSelector(\"[name='q']\")).sendKeys(Keys.RETURN);
        d.get(\"http://todomvc.com/examples/troopjs_require/#/\");
        d.findElement(By.cssSelector(\"#new-todo\")).sendKeys(\"task1\");
    }
}
```

leads to `org.openqa.selenium.NoSuchElementException: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#new-todo\"}`

Full error message at: https://www.refheap.com/99721

The reason is: webdriver did not loaded 2nd url, at `d.get(\"http://todomvc.com/examples/troopjs_require/#/\");` and did tell nothing about this. At least the latter \"wordless ignoring\" seems buggy.

Versions:
Selenium 2.45.0
Firefox 34

P.S.
The same in python (see example in https://www.refheap.com/99717)
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/463","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/463/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/463/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/463/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/463","id":69138869,"node_id":"MDU6SXNzdWU2OTEzODg2OQ==","number":463,"title":"After operation on the page at 1st url, the 2nd url is not loaded by webdriver leading to fails in locating elements","user":{"login":"yashaka","id":1961118,"node_id":"MDQ6VXNlcjE5NjExMTg=","avatar_url":"https://avatars1.githubusercontent.com/u/1961118?v=4","gravatar_id":"","url":"https://api.github.com/users/yashaka","html_url":"https://github.com/yashaka","followers_url":"https://api.github.com/users/yashaka/followers","following_url":"https://api.github.com/users/yashaka/following{/other_user}","gists_url":"https://api.github.com/users/yashaka/gists{/gist_id}","starred_url":"https://api.github.com/users/yashaka/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yashaka/subscriptions","organizations_url":"https://api.github.com/users/yashaka/orgs","repos_url":"https://api.github.com/users/yashaka/repos","events_url":"https://api.github.com/users/yashaka/events{/privacy}","received_events_url":"https://api.github.com/users/yashaka/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-04-17T13:26:52Z","updated_at":"2019-08-17T19:09:52Z","closed_at":"2017-08-27T17:28:50Z","author_association":"NONE","body":"The following java code\n\n``` java\npackage ua.net.itlabs;\n\nimport org.junit.Test;\nimport org.openqa.selenium.By;\nimport org.openqa.selenium.Keys;\nimport org.openqa.selenium.WebDriver;\nimport org.openqa.selenium.firefox.FirefoxDriver;\n\npublic class TwoUrlsTest {\n\n    @Test\n    public void test2Urls(){\n        WebDriver d = new FirefoxDriver();\n        d.get(\"http://google.com/ncr\");\n        d.findElement(By.cssSelector(\"[name='q']\")).sendKeys(\"something\");\n        d.findElement(By.cssSelector(\"[name='q']\")).sendKeys(Keys.RETURN);\n        d.get(\"http://todomvc.com/examples/troopjs_require/#/\");\n        d.findElement(By.cssSelector(\"#new-todo\")).sendKeys(\"task1\");\n    }\n}\n```\n\nleads to `org.openqa.selenium.NoSuchElementException: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#new-todo\"}`\n\nFull error message at: https://www.refheap.com/99721\n\nThe reason is: webdriver did not loaded 2nd url, at `d.get(\"http://todomvc.com/examples/troopjs_require/#/\");` and did tell nothing about this. At least the latter \"wordless ignoring\" seems buggy.\n\nVersions:\nSelenium 2.45.0\nFirefox 34\n\nP.S.\nThe same in python (see example in https://www.refheap.com/99717)\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["93990094","104812348","325212416"], "labels":["D-firefox"]}