{"id":"5673", "title":"Using a specific driver using setBinary() not working", "body":"## Meta -
**OS**:  Windows 10
<!-- Windows 10? OSX? -->
**Selenium Version**:  selenium-webdriver 3.6.0
<!-- 2.52.0, IDE, etc -->
**Browser**:  Firefox (geckodriver v0.20.0)

**Browser Version**:  59.0.1 (64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior 
`new firefox.Options().setBinary(\"<path>\")` should use a specific driver
## Actual Behavior
Setting a binary path does not work. Throws this error instead.
```
D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\promise.js:2626
        throw error;
        ^

WebDriverError: Process unexpectedly closed with status 1
    at Object.throwDecodedError (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\error.js:514:15)
    at parseHttpResponse (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\http.js:519:13)
    at doSend.then.response (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\http.js:441:30)
    at <anonymous>
    at process._tickCallback (internal/process/next_tick.js:188:7)
From: Task: WebDriver.createSession()
    at Function.createSession (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\webdriver.js:769:24)
    at Function.createSession (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\firefox\\index.js:521:41)
    at createDriver (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\index.js:170:33)
    at Builder.build (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\index.js:645:16)
    at Object.<anonymous> (D:\\Web Automation Bot\\index.js:7:6)
    at Module._compile (module.js:652:30)
    at Object.Module._extensions..js (module.js:663:10)
    at Module.load (module.js:565:32)
    at tryModuleLoad (module.js:505:12)
    at Function.Module._load (module.js:497:3)
```

## Steps to reproduce
Here is my code.  If i remove the `setFirefoxOptions()` it works perfectly as I have added it to my path. 
```
var webdriver = require('selenium-webdriver');
var firefox = require('selenium-webdriver/firefox');

var driver = new webdriver.Builder()
    .forBrowser('firefox')
    .setFirefoxOptions(
        new firefox.Options()
            .setBinary('D:\\\\Web Automation Bot\\\\drivers\\\\geckodriver.exe')
    )
    .build();

driver.get('https://google.com');
driver.sleep(1000);
driver.quit();
```

Looking for some quick help here.  Thanks 😁 


", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5673","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5673/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5673/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5673/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5673","id":308262774,"node_id":"MDU6SXNzdWUzMDgyNjI3NzQ=","number":5673,"title":"Using a specific driver using setBinary() not working","user":{"login":"DollarAkshay","id":6565924,"node_id":"MDQ6VXNlcjY1NjU5MjQ=","avatar_url":"https://avatars2.githubusercontent.com/u/6565924?v=4","gravatar_id":"","url":"https://api.github.com/users/DollarAkshay","html_url":"https://github.com/DollarAkshay","followers_url":"https://api.github.com/users/DollarAkshay/followers","following_url":"https://api.github.com/users/DollarAkshay/following{/other_user}","gists_url":"https://api.github.com/users/DollarAkshay/gists{/gist_id}","starred_url":"https://api.github.com/users/DollarAkshay/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/DollarAkshay/subscriptions","organizations_url":"https://api.github.com/users/DollarAkshay/orgs","repos_url":"https://api.github.com/users/DollarAkshay/repos","events_url":"https://api.github.com/users/DollarAkshay/events{/privacy}","received_events_url":"https://api.github.com/users/DollarAkshay/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-03-24T13:33:42Z","updated_at":"2019-08-16T09:09:34Z","closed_at":"2018-03-25T12:13:38Z","author_association":"NONE","body":"## Meta -\r\n**OS**:  Windows 10\r\n<!-- Windows 10? OSX? -->\r\n**Selenium Version**:  selenium-webdriver 3.6.0\r\n<!-- 2.52.0, IDE, etc -->\r\n**Browser**:  Firefox (geckodriver v0.20.0)\r\n\r\n**Browser Version**:  59.0.1 (64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior \r\n`new firefox.Options().setBinary(\"<path>\")` should use a specific driver\r\n## Actual Behavior\r\nSetting a binary path does not work. Throws this error instead.\r\n```\r\nD:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\promise.js:2626\r\n        throw error;\r\n        ^\r\n\r\nWebDriverError: Process unexpectedly closed with status 1\r\n    at Object.throwDecodedError (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\error.js:514:15)\r\n    at parseHttpResponse (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\http.js:519:13)\r\n    at doSend.then.response (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\http.js:441:30)\r\n    at <anonymous>\r\n    at process._tickCallback (internal/process/next_tick.js:188:7)\r\nFrom: Task: WebDriver.createSession()\r\n    at Function.createSession (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\lib\\webdriver.js:769:24)\r\n    at Function.createSession (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\firefox\\index.js:521:41)\r\n    at createDriver (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\index.js:170:33)\r\n    at Builder.build (D:\\Web Automation Bot\\node_modules\\selenium-webdriver\\index.js:645:16)\r\n    at Object.<anonymous> (D:\\Web Automation Bot\\index.js:7:6)\r\n    at Module._compile (module.js:652:30)\r\n    at Object.Module._extensions..js (module.js:663:10)\r\n    at Module.load (module.js:565:32)\r\n    at tryModuleLoad (module.js:505:12)\r\n    at Function.Module._load (module.js:497:3)\r\n```\r\n\r\n## Steps to reproduce\r\nHere is my code.  If i remove the `setFirefoxOptions()` it works perfectly as I have added it to my path. \r\n```\r\nvar webdriver = require('selenium-webdriver');\r\nvar firefox = require('selenium-webdriver/firefox');\r\n\r\nvar driver = new webdriver.Builder()\r\n    .forBrowser('firefox')\r\n    .setFirefoxOptions(\r\n        new firefox.Options()\r\n            .setBinary('D:\\\\Web Automation Bot\\\\drivers\\\\geckodriver.exe')\r\n    )\r\n    .build();\r\n\r\ndriver.get('https://google.com');\r\ndriver.sleep(1000);\r\ndriver.quit();\r\n```\r\n\r\nLooking for some quick help here.  Thanks 😁 \r\n\r\n\r\n","closed_by":{"login":"DollarAkshay","id":6565924,"node_id":"MDQ6VXNlcjY1NjU5MjQ=","avatar_url":"https://avatars2.githubusercontent.com/u/6565924?v=4","gravatar_id":"","url":"https://api.github.com/users/DollarAkshay","html_url":"https://github.com/DollarAkshay","followers_url":"https://api.github.com/users/DollarAkshay/followers","following_url":"https://api.github.com/users/DollarAkshay/following{/other_user}","gists_url":"https://api.github.com/users/DollarAkshay/gists{/gist_id}","starred_url":"https://api.github.com/users/DollarAkshay/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/DollarAkshay/subscriptions","organizations_url":"https://api.github.com/users/DollarAkshay/orgs","repos_url":"https://api.github.com/users/DollarAkshay/repos","events_url":"https://api.github.com/users/DollarAkshay/events{/privacy}","received_events_url":"https://api.github.com/users/DollarAkshay/received_events","type":"User","site_admin":false}}", "commentIds":["375891338","375891906","375912846","375917082","375921462","375965816"], "labels":[]}