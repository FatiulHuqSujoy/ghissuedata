{"id":"7601", "title":"Obtain the content of \"https://free-ss.site\" with selenium + python + firefox + geckodriver", "body":"Hi,

I want to obtain the content of \"https://free-ss.site\" with selenium + python + firefox + geckodriver.   I write the following code with python:


```
import os

def get_path(path):
    return os.path.abspath(os.path.expanduser(path))

firefox_binary=get_path(\"~/software/mozilla.org/firefox_relative/firefox-68.0esr/firefox/firefox\")
executable_path=get_path(\"~/software/mozilla.org/geckodriver-v0.25.0-linux64/geckodriver\")

from selenium import webdriver
browser = webdriver.Firefox(firefox_binary=firefox_binary, executable_path=executable_path)
browser.get(\"https://free-ss.site\")
```


But failed the get all of the contents on that website.    
Any hints?

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7601","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7601/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7601/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7601/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7601","id":498320416,"node_id":"MDU6SXNzdWU0OTgzMjA0MTY=","number":7601,"title":"Obtain the content of \"https://free-ss.site\" with selenium + python + firefox + geckodriver","user":{"login":"hongyi-zhao","id":11155854,"node_id":"MDQ6VXNlcjExMTU1ODU0","avatar_url":"https://avatars2.githubusercontent.com/u/11155854?v=4","gravatar_id":"","url":"https://api.github.com/users/hongyi-zhao","html_url":"https://github.com/hongyi-zhao","followers_url":"https://api.github.com/users/hongyi-zhao/followers","following_url":"https://api.github.com/users/hongyi-zhao/following{/other_user}","gists_url":"https://api.github.com/users/hongyi-zhao/gists{/gist_id}","starred_url":"https://api.github.com/users/hongyi-zhao/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hongyi-zhao/subscriptions","organizations_url":"https://api.github.com/users/hongyi-zhao/orgs","repos_url":"https://api.github.com/users/hongyi-zhao/repos","events_url":"https://api.github.com/users/hongyi-zhao/events{/privacy}","received_events_url":"https://api.github.com/users/hongyi-zhao/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-09-25T14:22:33Z","updated_at":"2019-10-25T16:30:42Z","closed_at":"2019-09-25T15:57:56Z","author_association":"NONE","body":"Hi,\r\n\r\nI want to obtain the content of \"https://free-ss.site\" with selenium + python + firefox + geckodriver.   I write the following code with python:\r\n\r\n\r\n```\r\nimport os\r\n\r\ndef get_path(path):\r\n    return os.path.abspath(os.path.expanduser(path))\r\n\r\nfirefox_binary=get_path(\"~/software/mozilla.org/firefox_relative/firefox-68.0esr/firefox/firefox\")\r\nexecutable_path=get_path(\"~/software/mozilla.org/geckodriver-v0.25.0-linux64/geckodriver\")\r\n\r\nfrom selenium import webdriver\r\nbrowser = webdriver.Firefox(firefox_binary=firefox_binary, executable_path=executable_path)\r\nbrowser.get(\"https://free-ss.site\")\r\n```\r\n\r\n\r\nBut failed the get all of the contents on that website.    \r\nAny hints?\r\n\r\n","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["535090765","546422726"], "labels":[]}