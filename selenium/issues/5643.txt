{"id":"5643", "title":"WebDriver ruby API tries to call new for nil class in Selenium::WebDriver.for", "body":"## Meta -
OS:  Ubuntu 17.10 (64 bit)
<!-- Windows 10? OSX? -->
Selenium Version:  selenium-webdriver 3.11.0
<!-- 2.52.0, IDE, etc -->
Browser:  Firefox
<!-- Internet Explorer?  Firefox?
Browser Version:  59.0.1 (64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
A functional driver instance.

## Actual Behavior -
Uncaught exception: undefined method `new' for main:Object
	/home/jeffery/RubymineProjects/selenium/navigate.rb:3:in `<top (required)>'

Using RubyMine, it looks like the for method gets to its last step in driver.rb:

def initialize(bridge, listener: nil)
        @bridge = bridge
        @bridge = Support::EventFiringBridge.new(bridge, listener) if listener
end

The error occurs upon return from this  superclass constructor, which should return back through the stack to the original for method.  Everything to this point appears to be set up properly: geckodriver is started, the session request is successful, and  browser window appears.  It seems like the ruby stack is corrupted in some fashion.

## Steps to reproduce -
Firefox 59.0.1 (64-bit)
selenium-webdriver 3.11.0
ruby 2.5.0
geckodriver 0.20.0
Ubuntu 17.10 (64 bit)

Code (navigate.rb):

require 'selenium-webdriver'

driver = Selenium::WebDriver.for :firefox  # error here line 3
driver.navigate.to 'http://google.com'
driver.quit

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5643","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5643/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5643/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5643/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5643","id":306729190,"node_id":"MDU6SXNzdWUzMDY3MjkxOTA=","number":5643,"title":"WebDriver ruby API tries to call new for nil class in Selenium::WebDriver.for","user":{"login":"jeffery-cavallaro-cavcom","id":37490775,"node_id":"MDQ6VXNlcjM3NDkwNzc1","avatar_url":"https://avatars0.githubusercontent.com/u/37490775?v=4","gravatar_id":"","url":"https://api.github.com/users/jeffery-cavallaro-cavcom","html_url":"https://github.com/jeffery-cavallaro-cavcom","followers_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/followers","following_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/following{/other_user}","gists_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/gists{/gist_id}","starred_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/subscriptions","organizations_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/orgs","repos_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/repos","events_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/events{/privacy}","received_events_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-03-20T04:53:05Z","updated_at":"2019-08-16T09:09:55Z","closed_at":"2018-03-20T04:56:58Z","author_association":"NONE","body":"## Meta -\r\nOS:  Ubuntu 17.10 (64 bit)\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  selenium-webdriver 3.11.0\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Firefox\r\n<!-- Internet Explorer?  Firefox?\r\nBrowser Version:  59.0.1 (64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nA functional driver instance.\r\n\r\n## Actual Behavior -\r\nUncaught exception: undefined method `new' for main:Object\r\n\t/home/jeffery/RubymineProjects/selenium/navigate.rb:3:in `<top (required)>'\r\n\r\nUsing RubyMine, it looks like the for method gets to its last step in driver.rb:\r\n\r\ndef initialize(bridge, listener: nil)\r\n        @bridge = bridge\r\n        @bridge = Support::EventFiringBridge.new(bridge, listener) if listener\r\nend\r\n\r\nThe error occurs upon return from this  superclass constructor, which should return back through the stack to the original for method.  Everything to this point appears to be set up properly: geckodriver is started, the session request is successful, and  browser window appears.  It seems like the ruby stack is corrupted in some fashion.\r\n\r\n## Steps to reproduce -\r\nFirefox 59.0.1 (64-bit)\r\nselenium-webdriver 3.11.0\r\nruby 2.5.0\r\ngeckodriver 0.20.0\r\nUbuntu 17.10 (64 bit)\r\n\r\nCode (navigate.rb):\r\n\r\nrequire 'selenium-webdriver'\r\n\r\ndriver = Selenium::WebDriver.for :firefox  # error here line 3\r\ndriver.navigate.to 'http://google.com'\r\ndriver.quit\r\n\r\n","closed_by":{"login":"jeffery-cavallaro-cavcom","id":37490775,"node_id":"MDQ6VXNlcjM3NDkwNzc1","avatar_url":"https://avatars0.githubusercontent.com/u/37490775?v=4","gravatar_id":"","url":"https://api.github.com/users/jeffery-cavallaro-cavcom","html_url":"https://github.com/jeffery-cavallaro-cavcom","followers_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/followers","following_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/following{/other_user}","gists_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/gists{/gist_id}","starred_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/subscriptions","organizations_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/orgs","repos_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/repos","events_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/events{/privacy}","received_events_url":"https://api.github.com/users/jeffery-cavallaro-cavcom/received_events","type":"User","site_admin":false}}", "commentIds":["374476234"], "labels":[]}