{"id":"6498", "title":"/GET/session/{session id}/element/{element id}/equals/{other id} is not a W3C conformant command", "body":"## Meta -
OS:  Windows 10
Selenium Version:  3.14
Browser:  Firefox

Browser Version:  64.0a1

## Expected Behavior -
Most Selenium bindings call /equals/{other id} in certain cases. Speaking of .NET bindings I previously submitted [issue 6129](https://github.com/SeleniumHQ/selenium/issues/6129) a few months ago, thinking that the equals command was part of the W3C standard and should work or at least provide a shim.

## Actual Behavior -
The reason for this is that [the W3C standard doesn't have any /element{id}/equals/ command](https://www.w3.org/TR/webdriver1/#list-of-endpoints). 
However all the implementations rely on it one way or another:
- [relevant Java bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/java/client/src/org/openqa/selenium/remote/http/AbstractHttpCommandCodec.java#L169)
- [relevant Python bindings](https://github.com/SeleniumHQ/selenium/blob/c88ac018489ebfe7366c7da8fc2fded963b2fe2d/py/selenium/webdriver/remote/remote_connection.py#L212)
- [relevant C++ bindings](https://github.com/SeleniumHQ/selenium/blob/ef87df83b084406800c456b673c692c475f43d28/cpp/webdriver-server/server.cc#L771)
- [relevant .NET bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/dotnet/src/webdriver/Remote/W3CWireProtocolCommandInfoRepository.cs#L118)
- [relevant Javascript bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/javascript/webdriver/http/http.js#L272)

This results in 
```
1531119112888   webdriver::server       DEBUG   -> GET /session/9676b5a3-4619-456d-95dc-f85d7677a56b/element/17729eda-aec3-4c7a-835c-27223f038b0b/equals/9d0b339f-811b-4079-b30d-5d0d886fbd81
1531119112888   webdriver::server       DEBUG   <- 404 Not Found {\"value\":{\"error\":\"unknown command\",\"message\":\"GET /session/9676b5a3-4619-456d-95dc-f85d7677a56b/element/17729eda-aec3-4c7a-835c-27223f038b0b/equals/9d0b339f-811b-4079-b30d-5d0d886fbd81 did not match a known command\",\"stacktrace\":\"\"}}
```

## Steps to reproduce -
Using .NET bindings (but should behave the same way in any of the above mentioned):
```
[Test]
    public void TestCase() {
      Browser.Driver.Navigate().GoToUrl(\"https://curtisy1.github.io/demo/indexof\");
      var listOfParagraphs = Browser.Driver.FindElements(By.CssSelector(\"label\"));
      var paragraphWithClassIndex = 0;

      foreach (var paragraph in listOfParagraphs) {
        if (paragraph.FindElement(By.XPath(\"../p\")).GetAttribute(\"class\").Contains(\"faildemo\")) {
          paragraphWithClassIndex = listOfParagraphs.IndexOf(paragraph);
          break;
        }
      }

      Assert.That(paragraphWithClassIndex == 2);
    }
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6498","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6498/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6498/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6498/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6498","id":368198088,"node_id":"MDU6SXNzdWUzNjgxOTgwODg=","number":6498,"title":"/GET/session/{session id}/element/{element id}/equals/{other id} is not a W3C conformant command","user":{"login":"curtisy1","id":23632152,"node_id":"MDQ6VXNlcjIzNjMyMTUy","avatar_url":"https://avatars2.githubusercontent.com/u/23632152?v=4","gravatar_id":"","url":"https://api.github.com/users/curtisy1","html_url":"https://github.com/curtisy1","followers_url":"https://api.github.com/users/curtisy1/followers","following_url":"https://api.github.com/users/curtisy1/following{/other_user}","gists_url":"https://api.github.com/users/curtisy1/gists{/gist_id}","starred_url":"https://api.github.com/users/curtisy1/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/curtisy1/subscriptions","organizations_url":"https://api.github.com/users/curtisy1/orgs","repos_url":"https://api.github.com/users/curtisy1/repos","events_url":"https://api.github.com/users/curtisy1/events{/privacy}","received_events_url":"https://api.github.com/users/curtisy1/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":11,"created_at":"2018-10-09T12:59:35Z","updated_at":"2019-08-15T07:10:08Z","closed_at":"2018-10-16T21:23:51Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\nSelenium Version:  3.14\r\nBrowser:  Firefox\r\n\r\nBrowser Version:  64.0a1\r\n\r\n## Expected Behavior -\r\nMost Selenium bindings call /equals/{other id} in certain cases. Speaking of .NET bindings I previously submitted [issue 6129](https://github.com/SeleniumHQ/selenium/issues/6129) a few months ago, thinking that the equals command was part of the W3C standard and should work or at least provide a shim.\r\n\r\n## Actual Behavior -\r\nThe reason for this is that [the W3C standard doesn't have any /element{id}/equals/ command](https://www.w3.org/TR/webdriver1/#list-of-endpoints). \r\nHowever all the implementations rely on it one way or another:\r\n- [relevant Java bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/java/client/src/org/openqa/selenium/remote/http/AbstractHttpCommandCodec.java#L169)\r\n- [relevant Python bindings](https://github.com/SeleniumHQ/selenium/blob/c88ac018489ebfe7366c7da8fc2fded963b2fe2d/py/selenium/webdriver/remote/remote_connection.py#L212)\r\n- [relevant C++ bindings](https://github.com/SeleniumHQ/selenium/blob/ef87df83b084406800c456b673c692c475f43d28/cpp/webdriver-server/server.cc#L771)\r\n- [relevant .NET bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/dotnet/src/webdriver/Remote/W3CWireProtocolCommandInfoRepository.cs#L118)\r\n- [relevant Javascript bindings](https://github.com/SeleniumHQ/selenium/blob/07a18746ff756e90fd79ef253a328bd7dfa9e6dc/javascript/webdriver/http/http.js#L272)\r\n\r\nThis results in \r\n```\r\n1531119112888   webdriver::server       DEBUG   -> GET /session/9676b5a3-4619-456d-95dc-f85d7677a56b/element/17729eda-aec3-4c7a-835c-27223f038b0b/equals/9d0b339f-811b-4079-b30d-5d0d886fbd81\r\n1531119112888   webdriver::server       DEBUG   <- 404 Not Found {\"value\":{\"error\":\"unknown command\",\"message\":\"GET /session/9676b5a3-4619-456d-95dc-f85d7677a56b/element/17729eda-aec3-4c7a-835c-27223f038b0b/equals/9d0b339f-811b-4079-b30d-5d0d886fbd81 did not match a known command\",\"stacktrace\":\"\"}}\r\n```\r\n\r\n## Steps to reproduce -\r\nUsing .NET bindings (but should behave the same way in any of the above mentioned):\r\n```\r\n[Test]\r\n    public void TestCase() {\r\n      Browser.Driver.Navigate().GoToUrl(\"https://curtisy1.github.io/demo/indexof\");\r\n      var listOfParagraphs = Browser.Driver.FindElements(By.CssSelector(\"label\"));\r\n      var paragraphWithClassIndex = 0;\r\n\r\n      foreach (var paragraph in listOfParagraphs) {\r\n        if (paragraph.FindElement(By.XPath(\"../p\")).GetAttribute(\"class\").Contains(\"faildemo\")) {\r\n          paragraphWithClassIndex = listOfParagraphs.IndexOf(paragraph);\r\n          break;\r\n        }\r\n      }\r\n\r\n      Assert.That(paragraphWithClassIndex == 2);\r\n    }\r\n```\r\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["428213553","428225319","428227342","428237155","428261710","428330874","428452845","428766663","430210667","430210800","430406066"], "labels":[]}