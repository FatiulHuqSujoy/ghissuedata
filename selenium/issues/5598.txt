{"id":"5598", "title":"Selenium::WebDriver::Error::UnknownError", "body":"## Meta -
OS:  
Ubuntu Server 16.04 (64-bit)
Selenium Version:  

Browser:  
Firefox 

Browser Version:  
Mozilla Firefox 58.0.2

Ruby 2.5.0

## Expected Behavior -
Just run the selenium-webdriver

## Actual Behavior -
stack backtrace:: address not available (Selenium::WebDriver::Error::UnknownError)

## Steps to reproduce -
`require 'selenium-webdriver'
require 'rspec'
require 'headless'

puts \"headless...\"
headless = Headless.new
headless.start

puts \"selenium...\"
driver = Selenium::WebDriver.for :firefox`
When i run this code, the error is occur, address not available (Selenium::WebDriver::Error::UnknownError).
What's problem?
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5598","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5598/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5598/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5598/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5598","id":304323159,"node_id":"MDU6SXNzdWUzMDQzMjMxNTk=","number":5598,"title":"Selenium::WebDriver::Error::UnknownError","user":{"login":"NanKisu","id":14308689,"node_id":"MDQ6VXNlcjE0MzA4Njg5","avatar_url":"https://avatars1.githubusercontent.com/u/14308689?v=4","gravatar_id":"","url":"https://api.github.com/users/NanKisu","html_url":"https://github.com/NanKisu","followers_url":"https://api.github.com/users/NanKisu/followers","following_url":"https://api.github.com/users/NanKisu/following{/other_user}","gists_url":"https://api.github.com/users/NanKisu/gists{/gist_id}","starred_url":"https://api.github.com/users/NanKisu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/NanKisu/subscriptions","organizations_url":"https://api.github.com/users/NanKisu/orgs","repos_url":"https://api.github.com/users/NanKisu/repos","events_url":"https://api.github.com/users/NanKisu/events{/privacy}","received_events_url":"https://api.github.com/users/NanKisu/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-03-12T10:53:17Z","updated_at":"2019-08-16T10:09:56Z","closed_at":"2018-03-13T02:22:43Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nUbuntu Server 16.04 (64-bit)\r\nSelenium Version:  \r\n\r\nBrowser:  \r\nFirefox \r\n\r\nBrowser Version:  \r\nMozilla Firefox 58.0.2\r\n\r\nRuby 2.5.0\r\n\r\n## Expected Behavior -\r\nJust run the selenium-webdriver\r\n\r\n## Actual Behavior -\r\nstack backtrace:: address not available (Selenium::WebDriver::Error::UnknownError)\r\n\r\n## Steps to reproduce -\r\n`require 'selenium-webdriver'\r\nrequire 'rspec'\r\nrequire 'headless'\r\n\r\nputs \"headless...\"\r\nheadless = Headless.new\r\nheadless.start\r\n\r\nputs \"selenium...\"\r\ndriver = Selenium::WebDriver.for :firefox`\r\nWhen i run this code, the error is occur, address not available (Selenium::WebDriver::Error::UnknownError).\r\nWhat's problem?\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["372524801"], "labels":[]}