{"id":"6142", "title":"java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker", "body":"java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker

code snippet:

public class FirstSeleniumTest {

WebDriver driver = null;
String baseURL = \"https://mail.google.com/\";

@BeforeMethod
public void setup() throws InterruptedException {
	String currentDir = System.getProperty(\"user.dir\");
	System.setProperty(\"webdriver.chrome.driver\", currentDir + \"\\\\executabledrivers\\\\chromedriver.exe\");
	driver = new ChromeDriver();
	driver.get(baseURL);
	Thread.sleep(4000);
}

@Test
public void seleniumTest1() throws InterruptedException {
	System.out.println(\"seleniumTest1\");
	driver.findElement(By.id(\"identifierId\")).sendKeys(\"testautomation\");

	Thread.sleep(2000);

}

@AfterMethod
public void tearDown() {
	driver.close();
	driver.quit();
}
}

OS: Windows

Browser: Chrome

Browser Version: 66.0.3359.181 (Official Build) (64-bit)

Expected Behavior - Chrome browser has to be launched
Actual Behavior - Getting exception: java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker
Steps to reproduce -
Execute above script", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6142","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6142/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6142/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6142/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6142","id":340155985,"node_id":"MDU6SXNzdWUzNDAxNTU5ODU=","number":6142,"title":"java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker","user":{"login":"srinivasgoud-n","id":29139328,"node_id":"MDQ6VXNlcjI5MTM5MzI4","avatar_url":"https://avatars1.githubusercontent.com/u/29139328?v=4","gravatar_id":"","url":"https://api.github.com/users/srinivasgoud-n","html_url":"https://github.com/srinivasgoud-n","followers_url":"https://api.github.com/users/srinivasgoud-n/followers","following_url":"https://api.github.com/users/srinivasgoud-n/following{/other_user}","gists_url":"https://api.github.com/users/srinivasgoud-n/gists{/gist_id}","starred_url":"https://api.github.com/users/srinivasgoud-n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/srinivasgoud-n/subscriptions","organizations_url":"https://api.github.com/users/srinivasgoud-n/orgs","repos_url":"https://api.github.com/users/srinivasgoud-n/repos","events_url":"https://api.github.com/users/srinivasgoud-n/events{/privacy}","received_events_url":"https://api.github.com/users/srinivasgoud-n/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-07-11T09:11:07Z","updated_at":"2019-08-15T19:09:45Z","closed_at":"2018-07-11T11:18:24Z","author_association":"NONE","body":"java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker\r\n\r\ncode snippet:\r\n\r\npublic class FirstSeleniumTest {\r\n\r\nWebDriver driver = null;\r\nString baseURL = \"https://mail.google.com/\";\r\n\r\n@BeforeMethod\r\npublic void setup() throws InterruptedException {\r\n\tString currentDir = System.getProperty(\"user.dir\");\r\n\tSystem.setProperty(\"webdriver.chrome.driver\", currentDir + \"\\\\executabledrivers\\\\chromedriver.exe\");\r\n\tdriver = new ChromeDriver();\r\n\tdriver.get(baseURL);\r\n\tThread.sleep(4000);\r\n}\r\n\r\n@Test\r\npublic void seleniumTest1() throws InterruptedException {\r\n\tSystem.out.println(\"seleniumTest1\");\r\n\tdriver.findElement(By.id(\"identifierId\")).sendKeys(\"testautomation\");\r\n\r\n\tThread.sleep(2000);\r\n\r\n}\r\n\r\n@AfterMethod\r\npublic void tearDown() {\r\n\tdriver.close();\r\n\tdriver.quit();\r\n}\r\n}\r\n\r\nOS: Windows\r\n\r\nBrowser: Chrome\r\n\r\nBrowser Version: 66.0.3359.181 (Official Build) (64-bit)\r\n\r\nExpected Behavior - Chrome browser has to be launched\r\nActual Behavior - Getting exception: java.lang.IllegalAccessError: tried to access method com.google.common.util.concurrent.SimpleTimeLimiter.(Ljava/util/concurrent/ExecutorService;)V from class org.openqa.selenium.net.UrlChecker\r\nSteps to reproduce -\r\nExecute above script","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["404100704","404134261"], "labels":[]}