{"id":"6071", "title":"[rb] Options overwrite custom desired capabilities", "body":"## Meta
Selenium-rb gem version:  
selenium-webdriver (3.13.0)

Browser: 
Chrome 67.0.3396.87 (Official Build) (64-bit)

## Driver setup

```ruby

Capybara.register_driver(:selenium_chrome_headless) do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    'goog:chromeOptions': { args: %w[no-sandbox headless disable-gpu window-size=1024,768] }
  )

  Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
end
```

I know I can setup args using `args` (deprecated) or options

## Expected Behavior

Create driver with given capabilities

## Actual Behavior

Options overwrite custom capabilities because of https://github.com/SeleniumHQ/selenium/blob/master/rb/lib/selenium/webdriver/chrome/driver.rb#L106

```ruby
caps.merge!(options) unless options.empty?
```

`options` here is not empty. It equals `{\"goog:chromeOptions\" => {} }`

## Steps to reproduce

Added spec and possible fix in:
https://github.com/artplan1/selenium/commit/9b14903b44b8136f418ce520aa564150603244d5

Possible fix:

```ruby
caps.merge!(options) unless options[Options::KEY].empty?
```

or `as_json` in Options.rb can return `{}` instead of `{\"goog:chromeOptions\" => {} }`
not sure if it's required with recent changes

```ruby
def as_json(*)
...
  opts ? {KEY => opts} : {}
end
```

Spec:

```ruby
it 'does not merge empty options' do
  custom_caps = Remote::Capabilities.new('goog:chromeOptions' => {args: %w[foo bar]})

  expect(http).to receive(:call) do |_, _, payload|
    expect(payload[:desiredCapabilities]['goog:chromeOptions'][:args]).to eq(%w[foo bar])
    resp
  end

  Driver.new(http_client: http, desired_capabilities: custom_caps)
end
```

Thanks
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6071","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6071/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6071/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6071/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6071","id":335797880,"node_id":"MDU6SXNzdWUzMzU3OTc4ODA=","number":6071,"title":"[rb] Options overwrite custom desired capabilities","user":{"login":"artplan1","id":9060346,"node_id":"MDQ6VXNlcjkwNjAzNDY=","avatar_url":"https://avatars3.githubusercontent.com/u/9060346?v=4","gravatar_id":"","url":"https://api.github.com/users/artplan1","html_url":"https://github.com/artplan1","followers_url":"https://api.github.com/users/artplan1/followers","following_url":"https://api.github.com/users/artplan1/following{/other_user}","gists_url":"https://api.github.com/users/artplan1/gists{/gist_id}","starred_url":"https://api.github.com/users/artplan1/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/artplan1/subscriptions","organizations_url":"https://api.github.com/users/artplan1/orgs","repos_url":"https://api.github.com/users/artplan1/repos","events_url":"https://api.github.com/users/artplan1/events{/privacy}","received_events_url":"https://api.github.com/users/artplan1/received_events","type":"User","site_admin":false},"labels":[{"id":182503883,"node_id":"MDU6TGFiZWwxODI1MDM4ODM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-rb","name":"C-rb","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-06-26T12:33:04Z","updated_at":"2019-08-15T20:09:49Z","closed_at":"2018-07-01T06:21:48Z","author_association":"CONTRIBUTOR","body":"## Meta\r\nSelenium-rb gem version:  \r\nselenium-webdriver (3.13.0)\r\n\r\nBrowser: \r\nChrome 67.0.3396.87 (Official Build) (64-bit)\r\n\r\n## Driver setup\r\n\r\n```ruby\r\n\r\nCapybara.register_driver(:selenium_chrome_headless) do |app|\r\n  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(\r\n    'goog:chromeOptions': { args: %w[no-sandbox headless disable-gpu window-size=1024,768] }\r\n  )\r\n\r\n  Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)\r\nend\r\n```\r\n\r\nI know I can setup args using `args` (deprecated) or options\r\n\r\n## Expected Behavior\r\n\r\nCreate driver with given capabilities\r\n\r\n## Actual Behavior\r\n\r\nOptions overwrite custom capabilities because of https://github.com/SeleniumHQ/selenium/blob/master/rb/lib/selenium/webdriver/chrome/driver.rb#L106\r\n\r\n```ruby\r\ncaps.merge!(options) unless options.empty?\r\n```\r\n\r\n`options` here is not empty. It equals `{\"goog:chromeOptions\" => {} }`\r\n\r\n## Steps to reproduce\r\n\r\nAdded spec and possible fix in:\r\nhttps://github.com/artplan1/selenium/commit/9b14903b44b8136f418ce520aa564150603244d5\r\n\r\nPossible fix:\r\n\r\n```ruby\r\ncaps.merge!(options) unless options[Options::KEY].empty?\r\n```\r\n\r\nor `as_json` in Options.rb can return `{}` instead of `{\"goog:chromeOptions\" => {} }`\r\nnot sure if it's required with recent changes\r\n\r\n```ruby\r\ndef as_json(*)\r\n...\r\n  opts ? {KEY => opts} : {}\r\nend\r\n```\r\n\r\nSpec:\r\n\r\n```ruby\r\nit 'does not merge empty options' do\r\n  custom_caps = Remote::Capabilities.new('goog:chromeOptions' => {args: %w[foo bar]})\r\n\r\n  expect(http).to receive(:call) do |_, _, payload|\r\n    expect(payload[:desiredCapabilities]['goog:chromeOptions'][:args]).to eq(%w[foo bar])\r\n    resp\r\n  end\r\n\r\n  Driver.new(http_client: http, desired_capabilities: custom_caps)\r\nend\r\n```\r\n\r\nThanks\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["400478197","400485506","400486272","400487507","400487731","400596133"], "labels":["C-rb"]}