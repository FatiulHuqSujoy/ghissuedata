{"id":"5316", "title":"Get [WinError 6] The handle is invalid in Windows 10", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
3.8.0
Browser:  
PhantomJS
Browser Version:  
2.1

## Expected Behavior -
I wrote a pyqt4=4.11.4 application and use pyinstaller=3.3 to make a windows executable. The application use selenium and PhantomJS to browse website and do stuff. It worked perfectly in windows 7.
## Actual Behavior -
`Traceback (most recent call last):
  File \"test.py\", line 37, in function
  File \"site-packages\\selenium\\webdriver\\phantomjs\\webdriver.py\", line 52, in __init__
  File \"site-packages\\selenium\\webdriver\\common\\service.py\", line 74, in start
  File \"subprocess.py\", line 640, in __init__
  File \"subprocess.py\", line 850, in _get_handles
OSError: [WinError 6] The handle is invalid`

## Steps to reproduce -
```
import sys
from PyQt4 import QtGui
import os
from selenium import webdriver

class Example(QtGui.QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        
    def initUI(self):
        btn = QtGui.QPushButton('Get html', self)
        btn.clicked.connect(self.function)
        self.edit = QtGui.QPlainTextEdit()
        
        layout = QtGui.QVBoxLayout()
        layout.addWidget(btn)
        layout.addWidget(self.edit)
        self.setLayout(layout)
        
        self.setGeometry(300, 300, 300, 500) 
        self.show()
        
    def function(self):
        url = 'https://www.google.com'
        executable_path = os.getcwd() + '\\\\phantomjs.exe'
        browser = webdriver.PhantomJS(executable_path=executable_path)
        browser.get(url)
        html = browser.page_source
        browser.quit()
        self.edit.setPlainText(html)
        return
def Start():
    global window
    window = Example()
    window.show()

if __name__ == '__main__':
    sys.stderr=open('err.log','w')
    app=QtGui.QApplication.instance()
    if not app:
       app = QtGui.QApplication(sys.argv)
       
    Start()
    sys.exit(app.exec_())
```


I actually found a workaround based on https://stackoverflow.com/questions/40108816/python-running-as-windows-service-oserror-winerror-6-the-handle-is-invalid. In Line 72 of service.py, at the end add stdin,
`self.process = subprocess.Popen(cmd, env=self.env,
                                            close_fds=platform.system() != 'Windows',
                                            stdout=self.log_file, stderr=self.log_file, stdin=self.log_file)`
since I thought the log file won't mess the package. I guess there should be more appropriate way to add stdin. After this, it works find in windows 10.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5316","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5316/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5316/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5316/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5316","id":287333823,"node_id":"MDU6SXNzdWUyODczMzM4MjM=","number":5316,"title":"Get [WinError 6] The handle is invalid in Windows 10","user":{"login":"hazardary","id":20841062,"node_id":"MDQ6VXNlcjIwODQxMDYy","avatar_url":"https://avatars0.githubusercontent.com/u/20841062?v=4","gravatar_id":"","url":"https://api.github.com/users/hazardary","html_url":"https://github.com/hazardary","followers_url":"https://api.github.com/users/hazardary/followers","following_url":"https://api.github.com/users/hazardary/following{/other_user}","gists_url":"https://api.github.com/users/hazardary/gists{/gist_id}","starred_url":"https://api.github.com/users/hazardary/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hazardary/subscriptions","organizations_url":"https://api.github.com/users/hazardary/orgs","repos_url":"https://api.github.com/users/hazardary/repos","events_url":"https://api.github.com/users/hazardary/events{/privacy}","received_events_url":"https://api.github.com/users/hazardary/received_events","type":"User","site_admin":false},"labels":[{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-01-10T07:03:32Z","updated_at":"2019-08-16T18:09:39Z","closed_at":"2018-01-10T17:20:24Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n3.8.0\r\nBrowser:  \r\nPhantomJS\r\nBrowser Version:  \r\n2.1\r\n\r\n## Expected Behavior -\r\nI wrote a pyqt4=4.11.4 application and use pyinstaller=3.3 to make a windows executable. The application use selenium and PhantomJS to browse website and do stuff. It worked perfectly in windows 7.\r\n## Actual Behavior -\r\n`Traceback (most recent call last):\r\n  File \"test.py\", line 37, in function\r\n  File \"site-packages\\selenium\\webdriver\\phantomjs\\webdriver.py\", line 52, in __init__\r\n  File \"site-packages\\selenium\\webdriver\\common\\service.py\", line 74, in start\r\n  File \"subprocess.py\", line 640, in __init__\r\n  File \"subprocess.py\", line 850, in _get_handles\r\nOSError: [WinError 6] The handle is invalid`\r\n\r\n## Steps to reproduce -\r\n```\r\nimport sys\r\nfrom PyQt4 import QtGui\r\nimport os\r\nfrom selenium import webdriver\r\n\r\nclass Example(QtGui.QWidget):\r\n    def __init__(self):\r\n        super(Example, self).__init__()\r\n        self.initUI()\r\n        \r\n    def initUI(self):\r\n        btn = QtGui.QPushButton('Get html', self)\r\n        btn.clicked.connect(self.function)\r\n        self.edit = QtGui.QPlainTextEdit()\r\n        \r\n        layout = QtGui.QVBoxLayout()\r\n        layout.addWidget(btn)\r\n        layout.addWidget(self.edit)\r\n        self.setLayout(layout)\r\n        \r\n        self.setGeometry(300, 300, 300, 500) \r\n        self.show()\r\n        \r\n    def function(self):\r\n        url = 'https://www.google.com'\r\n        executable_path = os.getcwd() + '\\\\phantomjs.exe'\r\n        browser = webdriver.PhantomJS(executable_path=executable_path)\r\n        browser.get(url)\r\n        html = browser.page_source\r\n        browser.quit()\r\n        self.edit.setPlainText(html)\r\n        return\r\ndef Start():\r\n    global window\r\n    window = Example()\r\n    window.show()\r\n\r\nif __name__ == '__main__':\r\n    sys.stderr=open('err.log','w')\r\n    app=QtGui.QApplication.instance()\r\n    if not app:\r\n       app = QtGui.QApplication(sys.argv)\r\n       \r\n    Start()\r\n    sys.exit(app.exec_())\r\n```\r\n\r\n\r\nI actually found a workaround based on https://stackoverflow.com/questions/40108816/python-running-as-windows-service-oserror-winerror-6-the-handle-is-invalid. In Line 72 of service.py, at the end add stdin,\r\n`self.process = subprocess.Popen(cmd, env=self.env,\r\n                                            close_fds=platform.system() != 'Windows',\r\n                                            stdout=self.log_file, stderr=self.log_file, stdin=self.log_file)`\r\nsince I thought the log file won't mess the package. I guess there should be more appropriate way to add stdin. After this, it works find in windows 10.\r\n","closed_by":{"login":"hazardary","id":20841062,"node_id":"MDQ6VXNlcjIwODQxMDYy","avatar_url":"https://avatars0.githubusercontent.com/u/20841062?v=4","gravatar_id":"","url":"https://api.github.com/users/hazardary","html_url":"https://github.com/hazardary","followers_url":"https://api.github.com/users/hazardary/followers","following_url":"https://api.github.com/users/hazardary/following{/other_user}","gists_url":"https://api.github.com/users/hazardary/gists{/gist_id}","starred_url":"https://api.github.com/users/hazardary/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hazardary/subscriptions","organizations_url":"https://api.github.com/users/hazardary/orgs","repos_url":"https://api.github.com/users/hazardary/repos","events_url":"https://api.github.com/users/hazardary/events{/privacy}","received_events_url":"https://api.github.com/users/hazardary/received_events","type":"User","site_admin":false}}", "commentIds":["356672844","356699941"], "labels":["C-py"]}