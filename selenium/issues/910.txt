{"id":"910", "title":"IE11 miss window_handles after nevigate and throw \"Unable to get current browser\" when InternetExplorerDriver first start", "body":"IE11 miss window_handles after nevigate and throw \"Unable to get current browser\".  
 I have try some times, and find that it happen in the InternetExplorerDriver first start. 
 I manual start the InternetExplorerDriver  and modify the client code to use the started InternetExplorerDriver, the window handles can get correct.
1. My test env:
     IE 11 64-bit  ( Version 11.0.9600.17914)
     InternetExplorerDriver server (64-bit)  2.47.0.0
2. test step:
    DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
    capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);
                capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, 30);
   
   ```
   System.setProperty(\"webdriver.ie.driver\", \"D://ie//win64//IEDriverServer.exe\");
   WebDriver driver=new InternetExplorerDriver(capabilities); 
   
   System.out.println( driver.getWindowHandles() );
   driver.get(\"https://www.google.com\");   
   Thread.sleep(10*1000);
   
   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
   ```
   
      System.out.println( driver.getWindowHandles() );
3. Useful InternetExplorerDriver trace log

============navigate====================
T 2015-08-11 10:52:54:897 server.cc(373) Entering Server::SendResponseToClient
T 2015-08-11 10:52:54:897 response.cc(36) Entering Response::Deserialize
T 2015-08-11 10:52:54:897 server.cc(427) Entering Server::SendHttpOk
T 2015-08-11 10:52:54:900 server.cc(139) Entering Server::ProcessRequest
T 2015-08-11 10:52:54:900 server.cc(203) Entering Server::ReadRequestBody
T 2015-08-11 10:52:54:900 server.cc(148) Process request with: URI: /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/url HTTP verb: POST
body: {\"url\":\"https://www.google.com\"}
T 2015-08-11 10:52:54:900 server.cc(237) Entering Server::DispatchCommand
T 2015-08-11 10:52:54:900 server.cc(544) Entering Server::LookupCommand
D 2015-08-11 10:52:54:900 server.cc(246) Command: POST /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/url {\"url\":\"https://www.google.com\"}
T 2015-08-11 10:52:54:900 server.cc(360) Entering Server::LookupSession
T 2015-08-11 10:52:54:900 IESession.cpp(215) Entering IESession::ExecuteCommand
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(163) Entering IECommandExecutor::OnSetCommand
T 2015-08-11 10:52:54:900 command.cc(31) Entering Command::Deserialize
D 2015-08-11 10:52:54:900 command.cc(36) Raw JSON command: { \"name\" : \"get\", \"locator\" : { \"sessionid\" : \"a47495e4-f326-4a37-a7e3-d8ff4180d891\" }, \"parameters\" : {\"url\":\"https://www.google.com\"} }
T 2015-08-11 10:52:54:900 IESession.cpp(236) Beginning wait for response length to be not zero
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(174) Entering IECommandExecutor::OnExecCommand
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(475) Entering IECommandExecutor::DispatchCommand
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
T 2015-08-11 10:52:54:900 IECommandExecutor.cpp(548) Entering IECommandExecutor::IsAlertActive
T 2015-08-11 10:52:54:900 Browser.cpp(702) Entering Browser::GetActiveDialogWindowHandle
T 2015-08-11 10:52:54:900 Browser.cpp(200) Entering Browser::GetContentWindowHandle
D 2015-08-11 10:52:54:901 IECommandExecutor.cpp(562) No alert handle is found
T 2015-08-11 10:52:54:901 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:52:54:901 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
T 2015-08-11 10:52:54:901 Browser.cpp(312) Entring Browser::NavigateToUrl
T 2015-08-11 10:52:54:902 DocumentHost.cpp(128) Entering DocumentHost::SetFocusedFrameByElement
T 2015-08-11 10:52:54:902 response.cc(63) Entering Response::SetSuccessResponse
T 2015-08-11 10:52:54:902 response.cc(69) Entering Response::SetResponse
T 2015-08-11 10:52:54:902 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:52:54:902 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
T 2015-08-11 10:52:54:902 response.cc(51) Entering Response::Serialize
T 2015-08-11 10:52:54:902 IECommandExecutor.cpp(213) Entering IECommandExecutor::OnWait
T 2015-08-11 10:52:54:902 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:52:54:902 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
T 2015-08-11 10:52:54:902 Browser.cpp(432) Entering Browser::Wait
D 2015-08-11 10:52:54:902 Browser.cpp(442) Navigate Events Completed.
T 2015-08-11 10:52:54:902 Browser.cpp(702) Entering Browser::GetActiveDialogWindowHandle
T 2015-08-11 10:52:54:902 Browser.cpp(200) Entering Browser::GetContentWindowHandle
T 2015-08-11 10:52:54:903 Browser.cpp(44) Entering Browser::BeforeNavigate2
D 2015-08-11 10:52:54:903 Browser.cpp(473) Browser ReadyState is not '4', indicating 'Complete'; it was 1
T 2015-08-11 10:52:54:904 IECommandExecutor.cpp(398) Entering IECommandExecutor::WaitThreadProc
T 2015-08-11 10:52:54:955 Browser.cpp(48) Entering Browser::OnQuit
T 2015-08-11 10:52:54:955 DocumentHost.cpp(262) Entering DocumentHost::PostQuitMessage
T 2015-08-11 10:52:54:955 IECommandExecutor.cpp(291) Entering IECommandExecutor::OnBrowserQuit
T 2015-08-11 10:52:54:955 Browser.cpp(294) Entering Browser::DetachEvents
T 2015-08-11 10:52:55:104 IECommandExecutor.cpp(213) Entering IECommandExecutor::OnWait
T 2015-08-11 10:52:55:104 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:52:55:104 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
W 2015-08-11 10:52:55:104 IECommandExecutor.cpp(606) Browser ID requested was an empty string
T 2015-08-11 10:52:55:110 IESession.cpp(245) Found non-zero response length
T 2015-08-11 10:52:55:110 IECommandExecutor.cpp(197) Entering IECommandExecutor::OnGetResponse
T 2015-08-11 10:52:55:110 IECommandExecutor.cpp(312) Entering IECommandExecutor::OnIsSessionValid
D 2015-08-11 10:52:55:110 server.cc(320) Response: {\"sessionId\":\"a47495e4-f326-4a37-a7e3-d8ff4180d891\",\"status\":0,\"value\":null}

==========after navigate,  get window handles of the session==============
T 2015-08-11 10:52:55:110 server.cc(373) Entering Server::SendResponseToClient
T 2015-08-11 10:52:55:110 response.cc(36) Entering Response::Deserialize
T 2015-08-11 10:52:55:110 server.cc(427) Entering Server::SendHttpOk
T 2015-08-11 10:53:07:452 server.cc(139) Entering Server::ProcessRequest
T 2015-08-11 10:53:07:452 server.cc(148) Process request with: URI: /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/window_handles HTTP verb: GET
body: {}
T 2015-08-11 10:53:07:452 server.cc(237) Entering Server::DispatchCommand
T 2015-08-11 10:53:07:452 server.cc(544) Entering Server::LookupCommand
D 2015-08-11 10:53:07:452 server.cc(246) Command: GET /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/window_handles {}
T 2015-08-11 10:53:07:452 server.cc(360) Entering Server::LookupSession
T 2015-08-11 10:53:07:452 IESession.cpp(215) Entering IESession::ExecuteCommand
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(163) Entering IECommandExecutor::OnSetCommand
T 2015-08-11 10:53:07:452 command.cc(31) Entering Command::Deserialize
D 2015-08-11 10:53:07:452 command.cc(36) Raw JSON command: { \"name\" : \"getWindowHandles\", \"locator\" : { \"sessionid\" : \"a47495e4-f326-4a37-a7e3-d8ff4180d891\" }, \"parameters\" : {} }
T 2015-08-11 10:53:07:452 IESession.cpp(236) Beginning wait for response length to be not zero
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(174) Entering IECommandExecutor::OnExecCommand
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(475) Entering IECommandExecutor::DispatchCommand
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser

===========Error=====================
W 2015-08-11 10:53:07:452 IECommandExecutor.cpp(606) Browser ID requested was an empty string
W 2015-08-11 10:53:07:452 IECommandExecutor.cpp(522) Unable to find current browser
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(622) Entering IECommandExecutor::GetManagedBrowserHandles
T 2015-08-11 10:53:07:452 response.cc(63) Entering Response::SetSuccessResponse
T 2015-08-11 10:53:07:452 response.cc(69) Entering Response::SetResponse
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser
T 2015-08-11 10:53:07:452 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser
W 2015-08-11 10:53:07:452 IECommandExecutor.cpp(606) Browser ID requested was an empty string
W 2015-08-11 10:53:07:453 IECommandExecutor.cpp(539) Unable to get current browser
T 2015-08-11 10:53:07:453 response.cc(51) Entering Response::Serialize
T 2015-08-11 10:53:07:462 IESession.cpp(245) Found non-zero response length
T 2015-08-11 10:53:07:462 IECommandExecutor.cpp(197) Entering IECommandExecutor::OnGetResponse
T 2015-08-11 10:53:07:462 IECommandExecutor.cpp(312) Entering IECommandExecutor::OnIsSessionValid
D 2015-08-11 10:53:07:462 server.cc(320) Response: {\"sessionId\":\"a47495e4-f326-4a37-a7e3-d8ff4180d891\",\"status\":0,\"value\":[]}

T 2015-08-11 10:53:07:462 server.cc(373) Entering Server::SendResponseToClient
T 2015-08-11 10:53:07:462 response.cc(36) Entering Response::Deserialize
T 2015-08-11 10:53:07:462 server.cc(427) Entering Server::SendHttpOk
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/910","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/910/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/910/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/910/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/910","id":100217445,"node_id":"MDU6SXNzdWUxMDAyMTc0NDU=","number":910,"title":"IE11 miss window_handles after nevigate and throw \"Unable to get current browser\" when InternetExplorerDriver first start","user":{"login":"xiaoyuefeihz","id":5370715,"node_id":"MDQ6VXNlcjUzNzA3MTU=","avatar_url":"https://avatars0.githubusercontent.com/u/5370715?v=4","gravatar_id":"","url":"https://api.github.com/users/xiaoyuefeihz","html_url":"https://github.com/xiaoyuefeihz","followers_url":"https://api.github.com/users/xiaoyuefeihz/followers","following_url":"https://api.github.com/users/xiaoyuefeihz/following{/other_user}","gists_url":"https://api.github.com/users/xiaoyuefeihz/gists{/gist_id}","starred_url":"https://api.github.com/users/xiaoyuefeihz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/xiaoyuefeihz/subscriptions","organizations_url":"https://api.github.com/users/xiaoyuefeihz/orgs","repos_url":"https://api.github.com/users/xiaoyuefeihz/repos","events_url":"https://api.github.com/users/xiaoyuefeihz/events{/privacy}","received_events_url":"https://api.github.com/users/xiaoyuefeihz/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2015-08-11T03:25:34Z","updated_at":"2019-08-15T21:09:34Z","closed_at":"2015-08-11T07:17:50Z","author_association":"NONE","body":"IE11 miss window_handles after nevigate and throw \"Unable to get current browser\".  \n I have try some times, and find that it happen in the InternetExplorerDriver first start. \n I manual start the InternetExplorerDriver  and modify the client code to use the started InternetExplorerDriver, the window handles can get correct.\n1. My test env:\n     IE 11 64-bit  ( Version 11.0.9600.17914)\n     InternetExplorerDriver server (64-bit)  2.47.0.0\n2. test step:\n    DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();\n    capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);\n                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);\n                capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, 30);\n   \n   ```\n   System.setProperty(\"webdriver.ie.driver\", \"D://ie//win64//IEDriverServer.exe\");\n   WebDriver driver=new InternetExplorerDriver(capabilities); \n   \n   System.out.println( driver.getWindowHandles() );\n   driver.get(\"https://www.google.com\");   \n   Thread.sleep(10*1000);\n   \n   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); \n   ```\n   \n      System.out.println( driver.getWindowHandles() );\n3. Useful InternetExplorerDriver trace log\n\n============navigate====================\nT 2015-08-11 10:52:54:897 server.cc(373) Entering Server::SendResponseToClient\nT 2015-08-11 10:52:54:897 response.cc(36) Entering Response::Deserialize\nT 2015-08-11 10:52:54:897 server.cc(427) Entering Server::SendHttpOk\nT 2015-08-11 10:52:54:900 server.cc(139) Entering Server::ProcessRequest\nT 2015-08-11 10:52:54:900 server.cc(203) Entering Server::ReadRequestBody\nT 2015-08-11 10:52:54:900 server.cc(148) Process request with: URI: /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/url HTTP verb: POST\nbody: {\"url\":\"https://www.google.com\"}\nT 2015-08-11 10:52:54:900 server.cc(237) Entering Server::DispatchCommand\nT 2015-08-11 10:52:54:900 server.cc(544) Entering Server::LookupCommand\nD 2015-08-11 10:52:54:900 server.cc(246) Command: POST /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/url {\"url\":\"https://www.google.com\"}\nT 2015-08-11 10:52:54:900 server.cc(360) Entering Server::LookupSession\nT 2015-08-11 10:52:54:900 IESession.cpp(215) Entering IESession::ExecuteCommand\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(163) Entering IECommandExecutor::OnSetCommand\nT 2015-08-11 10:52:54:900 command.cc(31) Entering Command::Deserialize\nD 2015-08-11 10:52:54:900 command.cc(36) Raw JSON command: { \"name\" : \"get\", \"locator\" : { \"sessionid\" : \"a47495e4-f326-4a37-a7e3-d8ff4180d891\" }, \"parameters\" : {\"url\":\"https://www.google.com\"} }\nT 2015-08-11 10:52:54:900 IESession.cpp(236) Beginning wait for response length to be not zero\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(174) Entering IECommandExecutor::OnExecCommand\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(475) Entering IECommandExecutor::DispatchCommand\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nT 2015-08-11 10:52:54:900 IECommandExecutor.cpp(548) Entering IECommandExecutor::IsAlertActive\nT 2015-08-11 10:52:54:900 Browser.cpp(702) Entering Browser::GetActiveDialogWindowHandle\nT 2015-08-11 10:52:54:900 Browser.cpp(200) Entering Browser::GetContentWindowHandle\nD 2015-08-11 10:52:54:901 IECommandExecutor.cpp(562) No alert handle is found\nT 2015-08-11 10:52:54:901 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:52:54:901 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nT 2015-08-11 10:52:54:901 Browser.cpp(312) Entring Browser::NavigateToUrl\nT 2015-08-11 10:52:54:902 DocumentHost.cpp(128) Entering DocumentHost::SetFocusedFrameByElement\nT 2015-08-11 10:52:54:902 response.cc(63) Entering Response::SetSuccessResponse\nT 2015-08-11 10:52:54:902 response.cc(69) Entering Response::SetResponse\nT 2015-08-11 10:52:54:902 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:52:54:902 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nT 2015-08-11 10:52:54:902 response.cc(51) Entering Response::Serialize\nT 2015-08-11 10:52:54:902 IECommandExecutor.cpp(213) Entering IECommandExecutor::OnWait\nT 2015-08-11 10:52:54:902 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:52:54:902 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nT 2015-08-11 10:52:54:902 Browser.cpp(432) Entering Browser::Wait\nD 2015-08-11 10:52:54:902 Browser.cpp(442) Navigate Events Completed.\nT 2015-08-11 10:52:54:902 Browser.cpp(702) Entering Browser::GetActiveDialogWindowHandle\nT 2015-08-11 10:52:54:902 Browser.cpp(200) Entering Browser::GetContentWindowHandle\nT 2015-08-11 10:52:54:903 Browser.cpp(44) Entering Browser::BeforeNavigate2\nD 2015-08-11 10:52:54:903 Browser.cpp(473) Browser ReadyState is not '4', indicating 'Complete'; it was 1\nT 2015-08-11 10:52:54:904 IECommandExecutor.cpp(398) Entering IECommandExecutor::WaitThreadProc\nT 2015-08-11 10:52:54:955 Browser.cpp(48) Entering Browser::OnQuit\nT 2015-08-11 10:52:54:955 DocumentHost.cpp(262) Entering DocumentHost::PostQuitMessage\nT 2015-08-11 10:52:54:955 IECommandExecutor.cpp(291) Entering IECommandExecutor::OnBrowserQuit\nT 2015-08-11 10:52:54:955 Browser.cpp(294) Entering Browser::DetachEvents\nT 2015-08-11 10:52:55:104 IECommandExecutor.cpp(213) Entering IECommandExecutor::OnWait\nT 2015-08-11 10:52:55:104 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:52:55:104 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nW 2015-08-11 10:52:55:104 IECommandExecutor.cpp(606) Browser ID requested was an empty string\nT 2015-08-11 10:52:55:110 IESession.cpp(245) Found non-zero response length\nT 2015-08-11 10:52:55:110 IECommandExecutor.cpp(197) Entering IECommandExecutor::OnGetResponse\nT 2015-08-11 10:52:55:110 IECommandExecutor.cpp(312) Entering IECommandExecutor::OnIsSessionValid\nD 2015-08-11 10:52:55:110 server.cc(320) Response: {\"sessionId\":\"a47495e4-f326-4a37-a7e3-d8ff4180d891\",\"status\":0,\"value\":null}\n\n==========after navigate,  get window handles of the session==============\nT 2015-08-11 10:52:55:110 server.cc(373) Entering Server::SendResponseToClient\nT 2015-08-11 10:52:55:110 response.cc(36) Entering Response::Deserialize\nT 2015-08-11 10:52:55:110 server.cc(427) Entering Server::SendHttpOk\nT 2015-08-11 10:53:07:452 server.cc(139) Entering Server::ProcessRequest\nT 2015-08-11 10:53:07:452 server.cc(148) Process request with: URI: /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/window_handles HTTP verb: GET\nbody: {}\nT 2015-08-11 10:53:07:452 server.cc(237) Entering Server::DispatchCommand\nT 2015-08-11 10:53:07:452 server.cc(544) Entering Server::LookupCommand\nD 2015-08-11 10:53:07:452 server.cc(246) Command: GET /session/a47495e4-f326-4a37-a7e3-d8ff4180d891/window_handles {}\nT 2015-08-11 10:53:07:452 server.cc(360) Entering Server::LookupSession\nT 2015-08-11 10:53:07:452 IESession.cpp(215) Entering IESession::ExecuteCommand\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(163) Entering IECommandExecutor::OnSetCommand\nT 2015-08-11 10:53:07:452 command.cc(31) Entering Command::Deserialize\nD 2015-08-11 10:53:07:452 command.cc(36) Raw JSON command: { \"name\" : \"getWindowHandles\", \"locator\" : { \"sessionid\" : \"a47495e4-f326-4a37-a7e3-d8ff4180d891\" }, \"parameters\" : {} }\nT 2015-08-11 10:53:07:452 IESession.cpp(236) Beginning wait for response length to be not zero\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(174) Entering IECommandExecutor::OnExecCommand\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(475) Entering IECommandExecutor::DispatchCommand\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\n\n===========Error=====================\nW 2015-08-11 10:53:07:452 IECommandExecutor.cpp(606) Browser ID requested was an empty string\nW 2015-08-11 10:53:07:452 IECommandExecutor.cpp(522) Unable to find current browser\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(622) Entering IECommandExecutor::GetManagedBrowserHandles\nT 2015-08-11 10:53:07:452 response.cc(63) Entering Response::SetSuccessResponse\nT 2015-08-11 10:53:07:452 response.cc(69) Entering Response::SetResponse\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(592) Entering IECommandExecutor::GetCurrentBrowser\nT 2015-08-11 10:53:07:452 IECommandExecutor.cpp(598) Entering IECommandExecutor::GetManagedBrowser\nW 2015-08-11 10:53:07:452 IECommandExecutor.cpp(606) Browser ID requested was an empty string\nW 2015-08-11 10:53:07:453 IECommandExecutor.cpp(539) Unable to get current browser\nT 2015-08-11 10:53:07:453 response.cc(51) Entering Response::Serialize\nT 2015-08-11 10:53:07:462 IESession.cpp(245) Found non-zero response length\nT 2015-08-11 10:53:07:462 IECommandExecutor.cpp(197) Entering IECommandExecutor::OnGetResponse\nT 2015-08-11 10:53:07:462 IECommandExecutor.cpp(312) Entering IECommandExecutor::OnIsSessionValid\nD 2015-08-11 10:53:07:462 server.cc(320) Response: {\"sessionId\":\"a47495e4-f326-4a37-a7e3-d8ff4180d891\",\"status\":0,\"value\":[]}\n\nT 2015-08-11 10:53:07:462 server.cc(373) Entering Server::SendResponseToClient\nT 2015-08-11 10:53:07:462 response.cc(36) Entering Response::Deserialize\nT 2015-08-11 10:53:07:462 server.cc(427) Entering Server::SendHttpOk\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["129725236","129736993","129789607","400734710"], "labels":[]}