{"id":"1347", "title":"Python. Execute JS with element", "body":"I found two problems with python selenium.

First.
There is not direct way to get driver from element uses it.
It would be nice to have `element._driver` attribute.

Second.
There is no way to execute script with specific element.

``` python
element = driver.find_element_by_id(\"scrollable\")
driver.execute_script(\"e = arguments[0]; return e.scrollHeight\", element)
```

This case is complicated by first.
I'd like to see code like below.

``` python
element.execute_script(\"return e.scrollHeight\")
```

Thank you for any advice.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1347","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1347/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1347/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1347/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1347","id":120434448,"node_id":"MDU6SXNzdWUxMjA0MzQ0NDg=","number":1347,"title":"Python. Execute JS with element","user":{"login":"hexum","id":6324742,"node_id":"MDQ6VXNlcjYzMjQ3NDI=","avatar_url":"https://avatars3.githubusercontent.com/u/6324742?v=4","gravatar_id":"","url":"https://api.github.com/users/hexum","html_url":"https://github.com/hexum","followers_url":"https://api.github.com/users/hexum/followers","following_url":"https://api.github.com/users/hexum/following{/other_user}","gists_url":"https://api.github.com/users/hexum/gists{/gist_id}","starred_url":"https://api.github.com/users/hexum/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hexum/subscriptions","organizations_url":"https://api.github.com/users/hexum/orgs","repos_url":"https://api.github.com/users/hexum/repos","events_url":"https://api.github.com/users/hexum/events{/privacy}","received_events_url":"https://api.github.com/users/hexum/received_events","type":"User","site_admin":false},"labels":[{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-12-04T16:18:08Z","updated_at":"2019-08-20T21:09:43Z","closed_at":"2015-12-04T21:09:03Z","author_association":"NONE","body":"I found two problems with python selenium.\n\nFirst.\nThere is not direct way to get driver from element uses it.\nIt would be nice to have `element._driver` attribute.\n\nSecond.\nThere is no way to execute script with specific element.\n\n``` python\nelement = driver.find_element_by_id(\"scrollable\")\ndriver.execute_script(\"e = arguments[0]; return e.scrollHeight\", element)\n```\n\nThis case is complicated by first.\nI'd like to see code like below.\n\n``` python\nelement.execute_script(\"return e.scrollHeight\")\n```\n\nThank you for any advice.\n","closed_by":{"login":"AutomatedTester","id":128518,"node_id":"MDQ6VXNlcjEyODUxOA==","avatar_url":"https://avatars1.githubusercontent.com/u/128518?v=4","gravatar_id":"","url":"https://api.github.com/users/AutomatedTester","html_url":"https://github.com/AutomatedTester","followers_url":"https://api.github.com/users/AutomatedTester/followers","following_url":"https://api.github.com/users/AutomatedTester/following{/other_user}","gists_url":"https://api.github.com/users/AutomatedTester/gists{/gist_id}","starred_url":"https://api.github.com/users/AutomatedTester/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/AutomatedTester/subscriptions","organizations_url":"https://api.github.com/users/AutomatedTester/orgs","repos_url":"https://api.github.com/users/AutomatedTester/repos","events_url":"https://api.github.com/users/AutomatedTester/events{/privacy}","received_events_url":"https://api.github.com/users/AutomatedTester/received_events","type":"User","site_admin":false}}", "commentIds":["162013762","162021831","162082429"], "labels":["C-py"]}