{"id":"340", "title":"Focused input not allowing multiple delete / backspace keypresses", "body":"**What steps will reproduce the problem?**

Create a fixture html file called \"focustest.html\" with the following content:

``` html
<!DOCTYPE html>
<html>
<head>
    <title>Focus Test</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"/>
</head>
<body>
    <input id=\"field\" name=\"data\" value=\"Initial Value\" type=\"text\" />

    <button id=\"focusbtn\" onclick=\"doFocus()\">Focus</button>

    <script>
      function doFocus() {
        document.getElementById('field').focus();
      }
    </script>
</body>
</html>
```

Create a PHPUnit test called \"test.php\" with the following content:

``` php

class WebTest extends PHPUnit_Extensions_Selenium2TestCase {

  function setUp() {
    $this->setBrowser('firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  function testFillFocusedField() {
    $value = 'Chekote';

    $this->url('/focustest.html');
    $this->byId('focusbtn')->click();

    // get the field value current length, and add backspace/delete keystrokes to remove it
    $field = $this->byId('field');
    $deletes = str_repeat(
      PHPUnit_Extensions_Selenium2TestCase_Keys::BACKSPACE . PHPUnit_Extensions_Selenium2TestCase_Keys::DELETE,
      strlen($field->value())
    );

    $this->keys($deletes . $value);

    $this->assertEquals($value, $field->value());
  }
}
```

Run the test:

phpunit test.php

**What is the expected output?**
I expect the test to pass, and the field's \"Initial Value\" text to be replaced with \"Chekote\"

**What do you see instead?**
The first character of the Initial Value (The \"I\") is deleted, but no other characters are deleted. The result is the field have the value \"Chekotenitial Value\".

Selenium version: 2.45
OS: Mac OS X 10.10.2
Browser: Firefox
Browser version: 31.4.0esr, 33.1.1, 34.0.5 & 36.0.1

This problem was first discovered and discussed in the MinkSelenium2Driver issues board here:

https://github.com/minkphp/MinkSelenium2Driver/issues/198

It was also originally reported here. I closed it thinking a typo was causing the tests to fail, but the typo was not the only reason it was failing. Sorry for the confusion!

https://code.google.com/p/selenium/issues/detail?id=8589
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/340","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/340/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/340/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/340/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/340","id":62218964,"node_id":"MDU6SXNzdWU2MjIxODk2NA==","number":340,"title":"Focused input not allowing multiple delete / backspace keypresses","user":{"login":"Chekote","id":550486,"node_id":"MDQ6VXNlcjU1MDQ4Ng==","avatar_url":"https://avatars2.githubusercontent.com/u/550486?v=4","gravatar_id":"","url":"https://api.github.com/users/Chekote","html_url":"https://github.com/Chekote","followers_url":"https://api.github.com/users/Chekote/followers","following_url":"https://api.github.com/users/Chekote/following{/other_user}","gists_url":"https://api.github.com/users/Chekote/gists{/gist_id}","starred_url":"https://api.github.com/users/Chekote/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Chekote/subscriptions","organizations_url":"https://api.github.com/users/Chekote/orgs","repos_url":"https://api.github.com/users/Chekote/repos","events_url":"https://api.github.com/users/Chekote/events{/privacy}","received_events_url":"https://api.github.com/users/Chekote/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false},{"id":186797723,"node_id":"MDU6TGFiZWwxODY3OTc3MjM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/E-less%20easy","name":"E-less easy","color":"02e10c","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":20,"created_at":"2015-03-16T21:50:15Z","updated_at":"2019-08-20T14:10:01Z","closed_at":"2016-02-10T14:48:05Z","author_association":"NONE","body":"**What steps will reproduce the problem?**\n\nCreate a fixture html file called \"focustest.html\" with the following content:\n\n``` html\n<!DOCTYPE html>\n<html>\n<head>\n    <title>Focus Test</title>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"/>\n</head>\n<body>\n    <input id=\"field\" name=\"data\" value=\"Initial Value\" type=\"text\" />\n\n    <button id=\"focusbtn\" onclick=\"doFocus()\">Focus</button>\n\n    <script>\n      function doFocus() {\n        document.getElementById('field').focus();\n      }\n    </script>\n</body>\n</html>\n```\n\nCreate a PHPUnit test called \"test.php\" with the following content:\n\n``` php\n\nclass WebTest extends PHPUnit_Extensions_Selenium2TestCase {\n\n  function setUp() {\n    $this->setBrowser('firefox');\n    $this->setBrowserUrl('http://localhost/');\n  }\n\n  function testFillFocusedField() {\n    $value = 'Chekote';\n\n    $this->url('/focustest.html');\n    $this->byId('focusbtn')->click();\n\n    // get the field value current length, and add backspace/delete keystrokes to remove it\n    $field = $this->byId('field');\n    $deletes = str_repeat(\n      PHPUnit_Extensions_Selenium2TestCase_Keys::BACKSPACE . PHPUnit_Extensions_Selenium2TestCase_Keys::DELETE,\n      strlen($field->value())\n    );\n\n    $this->keys($deletes . $value);\n\n    $this->assertEquals($value, $field->value());\n  }\n}\n```\n\nRun the test:\n\nphpunit test.php\n\n**What is the expected output?**\nI expect the test to pass, and the field's \"Initial Value\" text to be replaced with \"Chekote\"\n\n**What do you see instead?**\nThe first character of the Initial Value (The \"I\") is deleted, but no other characters are deleted. The result is the field have the value \"Chekotenitial Value\".\n\nSelenium version: 2.45\nOS: Mac OS X 10.10.2\nBrowser: Firefox\nBrowser version: 31.4.0esr, 33.1.1, 34.0.5 & 36.0.1\n\nThis problem was first discovered and discussed in the MinkSelenium2Driver issues board here:\n\nhttps://github.com/minkphp/MinkSelenium2Driver/issues/198\n\nIt was also originally reported here. I closed it thinking a typo was causing the tests to fail, but the typo was not the only reason it was failing. Sorry for the confusion!\n\nhttps://code.google.com/p/selenium/issues/detail?id=8589\n","closed_by":{"login":"Chekote","id":550486,"node_id":"MDQ6VXNlcjU1MDQ4Ng==","avatar_url":"https://avatars2.githubusercontent.com/u/550486?v=4","gravatar_id":"","url":"https://api.github.com/users/Chekote","html_url":"https://github.com/Chekote","followers_url":"https://api.github.com/users/Chekote/followers","following_url":"https://api.github.com/users/Chekote/following{/other_user}","gists_url":"https://api.github.com/users/Chekote/gists{/gist_id}","starred_url":"https://api.github.com/users/Chekote/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Chekote/subscriptions","organizations_url":"https://api.github.com/users/Chekote/orgs","repos_url":"https://api.github.com/users/Chekote/repos","events_url":"https://api.github.com/users/Chekote/events{/privacy}","received_events_url":"https://api.github.com/users/Chekote/received_events","type":"User","site_admin":false}}", "commentIds":["81958803","81960251","81961206","81983513","82006700","82009546","87830574","90970729","90973060","90975335","91003487","91004907","91019437","142863435","142864338","142924430","142928941","142931762","143191560","182403958"], "labels":["D-firefox","E-less easy"]}