{"id":"4544", "title":"function click, select throw \"Timeout loading page after 1ms\" exception", "body":"## Meta -
OS:  win 7, win 10
<!-- Windows 10? OSX? -->
Selenium Version:  3.5.0, 3.5.1, 3.5.2
<!-- 2.52.0, IDE, etc -->
Browser:  firefox
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  54 , 55
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
perform sucessfull and return without exception
## Actual Behavior -
return -1 and throw exception \"org.openqa.selenium.TimeoutException: Timeout loading page after 1ms\"
## Steps to reproduce -
_WebElement list = driver.findElement(By.id(\"list\"));
List<WebElement> eles = list.findElements(By.tagName(\"option\"));
eles.get(0).click();_

or 
_WebElement list = driver.findElement(By.id(\"list\"));
Select select = new Select(list);
select.selectByIndex(0);_
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4544","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4544/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4544/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4544/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4544","id":252154800,"node_id":"MDU6SXNzdWUyNTIxNTQ4MDA=","number":4544,"title":"function click, select throw \"Timeout loading page after 1ms\" exception","user":{"login":"phamluk","id":31266795,"node_id":"MDQ6VXNlcjMxMjY2Nzk1","avatar_url":"https://avatars2.githubusercontent.com/u/31266795?v=4","gravatar_id":"","url":"https://api.github.com/users/phamluk","html_url":"https://github.com/phamluk","followers_url":"https://api.github.com/users/phamluk/followers","following_url":"https://api.github.com/users/phamluk/following{/other_user}","gists_url":"https://api.github.com/users/phamluk/gists{/gist_id}","starred_url":"https://api.github.com/users/phamluk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/phamluk/subscriptions","organizations_url":"https://api.github.com/users/phamluk/orgs","repos_url":"https://api.github.com/users/phamluk/repos","events_url":"https://api.github.com/users/phamluk/events{/privacy}","received_events_url":"https://api.github.com/users/phamluk/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-08-23T04:13:13Z","updated_at":"2019-08-17T14:09:34Z","closed_at":"2017-09-06T06:54:55Z","author_association":"NONE","body":"## Meta -\r\nOS:  win 7, win 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.5.0, 3.5.1, 3.5.2\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  firefox\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  54 , 55\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nperform sucessfull and return without exception\r\n## Actual Behavior -\r\nreturn -1 and throw exception \"org.openqa.selenium.TimeoutException: Timeout loading page after 1ms\"\r\n## Steps to reproduce -\r\n_WebElement list = driver.findElement(By.id(\"list\"));\r\nList<WebElement> eles = list.findElements(By.tagName(\"option\"));\r\neles.get(0).click();_\r\n\r\nor \r\n_WebElement list = driver.findElement(By.id(\"list\"));\r\nSelect select = new Select(list);\r\nselect.selectByIndex(0);_\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["324232728","327392576"], "labels":[]}