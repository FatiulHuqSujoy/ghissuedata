{"id":"5925", "title":"Can't save a screenshot of the SVG file", "body":"## Meta -
OS: Arch Linux
Selenium Version:  3.12.0-1
Browser:  Firefox
Browser Version: 60.0.1 (64-bit)
geckodriver Version: 0.20.0

## Expected Behavior -
A screenshot of the rendered SVG.

## Actual Behavior -
`selenium` creates a zero size file.

## Steps to reproduce -
```python
#!/usr/bin/env python3.6

from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.wait import WebDriverWait

if __name__ == \"__main__\":
    options = Options()
    options.add_argument('-headless')
    driver = Firefox(executable_path='geckodriver', firefox_options=options)
    wait = WebDriverWait(driver, timeout=10)
    driver.get('https://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg')
    driver.save_screenshot('screenshot.png')
    driver.quit()
```

Links to local SVG files produce the same error. This code works fine with HTML.

geckodriver.log:
```
1526910879710	geckodriver	INFO	geckodriver 0.20.0
1526910879715	geckodriver	INFO	Listening on 127.0.0.1:35109
1526910880750	mozrunner::runner	INFO	Running command: \"/usr/bin/firefox\" \"-marionette\" \"-headless\" \"-profile\" \"/tmp/rust_mozprofile.E7MT0rgLQ95V\"
*** You are running in headless mode.
1526910881934	Marionette	INFO	Listening on port 37957
1526910881958	Marionette	WARN	TLS certificate errors will be ignored for this session
1526910882363	addons.xpi	WARN	Exception running bootstrap method shutdown on activity-stream@mozilla.org: [Exception... \"Component returned failure code: 0x80004005 (NS_ERROR_FAILURE) [nsIObserverService.removeObserver]\"  nsresult: \"0x80004005 (NS_ERROR_FAILURE)\"  location: \"JS frame :: resource://activity-stream/lib/SnippetsFeed.jsm :: uninit :: line 185\"  data: no] Stack trace: uninit()@resource://activity-stream/lib/SnippetsFeed.jsm:185
onAction()@resource://activity-stream/lib/SnippetsFeed.jsm:201
_middleware/</<()@resource://activity-stream/lib/Store.jsm:49
Store/this[method]()@resource://activity-stream/lib/Store.jsm:28
uninit()@resource://activity-stream/lib/Store.jsm:151
uninit()@resource://activity-stream/lib/ActivityStream.jsm:300
uninit()@resource://gre/modules/addons/XPIProvider.jsm -> jar:file:///usr/lib/firefox/browser/features/activity-stream@mozilla.org.xpi!/bootstrap.js:73
shutdown()@resource://gre/modules/addons/XPIProvider.jsm -> jar:file:///usr/lib/firefox/browser/features/activity-stream@mozilla.org.xpi!/bootstrap.js:169
callBootstrapMethod()@resource://gre/modules/addons/XPIProvider.jsm:4436
observe()@resource://gre/modules/addons/XPIProvider.jsm:2287
GeckoDriver.prototype.quit()@driver.js:3300
despatch()@server.js:293
execute()@server.js:267
onPacket/<()@server.js:242
onPacket()@server.js:241
_onJSONObjectReady/<()@transport.js:500
*** UTM:SVC TimerManager:registerTimer called after profile-before-change notification. Ignoring timer registration for id: telemetry_modules_ping
1526910882690	geckodriver::marionette	ERROR	Failed to stop browser process
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5925","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5925/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5925/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5925/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5925","id":324926449,"node_id":"MDU6SXNzdWUzMjQ5MjY0NDk=","number":5925,"title":"Can't save a screenshot of the SVG file","user":{"login":"RazrFalcon","id":725494,"node_id":"MDQ6VXNlcjcyNTQ5NA==","avatar_url":"https://avatars3.githubusercontent.com/u/725494?v=4","gravatar_id":"","url":"https://api.github.com/users/RazrFalcon","html_url":"https://github.com/RazrFalcon","followers_url":"https://api.github.com/users/RazrFalcon/followers","following_url":"https://api.github.com/users/RazrFalcon/following{/other_user}","gists_url":"https://api.github.com/users/RazrFalcon/gists{/gist_id}","starred_url":"https://api.github.com/users/RazrFalcon/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/RazrFalcon/subscriptions","organizations_url":"https://api.github.com/users/RazrFalcon/orgs","repos_url":"https://api.github.com/users/RazrFalcon/repos","events_url":"https://api.github.com/users/RazrFalcon/events{/privacy}","received_events_url":"https://api.github.com/users/RazrFalcon/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-05-21T13:59:18Z","updated_at":"2019-08-16T01:09:46Z","closed_at":"2018-05-21T14:31:46Z","author_association":"NONE","body":"## Meta -\r\nOS: Arch Linux\r\nSelenium Version:  3.12.0-1\r\nBrowser:  Firefox\r\nBrowser Version: 60.0.1 (64-bit)\r\ngeckodriver Version: 0.20.0\r\n\r\n## Expected Behavior -\r\nA screenshot of the rendered SVG.\r\n\r\n## Actual Behavior -\r\n`selenium` creates a zero size file.\r\n\r\n## Steps to reproduce -\r\n```python\r\n#!/usr/bin/env python3.6\r\n\r\nfrom selenium.webdriver import Firefox\r\nfrom selenium.webdriver.common.by import By\r\nfrom selenium.webdriver.common.keys import Keys\r\nfrom selenium.webdriver.firefox.options import Options\r\nfrom selenium.webdriver.support import expected_conditions as expected\r\nfrom selenium.webdriver.support.wait import WebDriverWait\r\n\r\nif __name__ == \"__main__\":\r\n    options = Options()\r\n    options.add_argument('-headless')\r\n    driver = Firefox(executable_path='geckodriver', firefox_options=options)\r\n    wait = WebDriverWait(driver, timeout=10)\r\n    driver.get('https://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg')\r\n    driver.save_screenshot('screenshot.png')\r\n    driver.quit()\r\n```\r\n\r\nLinks to local SVG files produce the same error. This code works fine with HTML.\r\n\r\ngeckodriver.log:\r\n```\r\n1526910879710\tgeckodriver\tINFO\tgeckodriver 0.20.0\r\n1526910879715\tgeckodriver\tINFO\tListening on 127.0.0.1:35109\r\n1526910880750\tmozrunner::runner\tINFO\tRunning command: \"/usr/bin/firefox\" \"-marionette\" \"-headless\" \"-profile\" \"/tmp/rust_mozprofile.E7MT0rgLQ95V\"\r\n*** You are running in headless mode.\r\n1526910881934\tMarionette\tINFO\tListening on port 37957\r\n1526910881958\tMarionette\tWARN\tTLS certificate errors will be ignored for this session\r\n1526910882363\taddons.xpi\tWARN\tException running bootstrap method shutdown on activity-stream@mozilla.org: [Exception... \"Component returned failure code: 0x80004005 (NS_ERROR_FAILURE) [nsIObserverService.removeObserver]\"  nsresult: \"0x80004005 (NS_ERROR_FAILURE)\"  location: \"JS frame :: resource://activity-stream/lib/SnippetsFeed.jsm :: uninit :: line 185\"  data: no] Stack trace: uninit()@resource://activity-stream/lib/SnippetsFeed.jsm:185\r\nonAction()@resource://activity-stream/lib/SnippetsFeed.jsm:201\r\n_middleware/</<()@resource://activity-stream/lib/Store.jsm:49\r\nStore/this[method]()@resource://activity-stream/lib/Store.jsm:28\r\nuninit()@resource://activity-stream/lib/Store.jsm:151\r\nuninit()@resource://activity-stream/lib/ActivityStream.jsm:300\r\nuninit()@resource://gre/modules/addons/XPIProvider.jsm -> jar:file:///usr/lib/firefox/browser/features/activity-stream@mozilla.org.xpi!/bootstrap.js:73\r\nshutdown()@resource://gre/modules/addons/XPIProvider.jsm -> jar:file:///usr/lib/firefox/browser/features/activity-stream@mozilla.org.xpi!/bootstrap.js:169\r\ncallBootstrapMethod()@resource://gre/modules/addons/XPIProvider.jsm:4436\r\nobserve()@resource://gre/modules/addons/XPIProvider.jsm:2287\r\nGeckoDriver.prototype.quit()@driver.js:3300\r\ndespatch()@server.js:293\r\nexecute()@server.js:267\r\nonPacket/<()@server.js:242\r\nonPacket()@server.js:241\r\n_onJSONObjectReady/<()@transport.js:500\r\n*** UTM:SVC TimerManager:registerTimer called after profile-before-change notification. Ignoring timer registration for id: telemetry_modules_ping\r\n1526910882690\tgeckodriver::marionette\tERROR\tFailed to stop browser process\r\n```","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["390671644"], "labels":[]}