{"id":"4488", "title":"Selenium js not work with winium", "body":"Meta -

OS: Mac OS 10.12.6
Selenium Version: 3.5.0
Browser: non browser, project [winium](https://github.com/2gis/Winium.Desktop)

I'm trying to get element window

```
const webdriver = require('selenium-webdriver')

const By = webdriver.By

const driver = new webdriver.Builder()
  .usingServer('http://terminal:9999')
  .withCapabilities({
    'app': 'C:/windows/system32/calc.exe'
  })
  .forBrowser('windows')
  .build()


  const w = driver.findElement(By.id('CalcFrame'))
```


`UnsupportedOperationError: 'css selector' is not valid or implemented searching strategy.`", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4488","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4488/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4488/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4488/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4488","id":250938676,"node_id":"MDU6SXNzdWUyNTA5Mzg2NzY=","number":4488,"title":"Selenium js not work with winium","user":{"login":"randomailer","id":1937791,"node_id":"MDQ6VXNlcjE5Mzc3OTE=","avatar_url":"https://avatars3.githubusercontent.com/u/1937791?v=4","gravatar_id":"","url":"https://api.github.com/users/randomailer","html_url":"https://github.com/randomailer","followers_url":"https://api.github.com/users/randomailer/followers","following_url":"https://api.github.com/users/randomailer/following{/other_user}","gists_url":"https://api.github.com/users/randomailer/gists{/gist_id}","starred_url":"https://api.github.com/users/randomailer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/randomailer/subscriptions","organizations_url":"https://api.github.com/users/randomailer/orgs","repos_url":"https://api.github.com/users/randomailer/repos","events_url":"https://api.github.com/users/randomailer/events{/privacy}","received_events_url":"https://api.github.com/users/randomailer/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-08-17T13:11:11Z","updated_at":"2019-08-17T21:09:57Z","closed_at":"2017-08-17T16:50:07Z","author_association":"NONE","body":"Meta -\r\n\r\nOS: Mac OS 10.12.6\r\nSelenium Version: 3.5.0\r\nBrowser: non browser, project [winium](https://github.com/2gis/Winium.Desktop)\r\n\r\nI'm trying to get element window\r\n\r\n```\r\nconst webdriver = require('selenium-webdriver')\r\n\r\nconst By = webdriver.By\r\n\r\nconst driver = new webdriver.Builder()\r\n  .usingServer('http://terminal:9999')\r\n  .withCapabilities({\r\n    'app': 'C:/windows/system32/calc.exe'\r\n  })\r\n  .forBrowser('windows')\r\n  .build()\r\n\r\n\r\n  const w = driver.findElement(By.id('CalcFrame'))\r\n```\r\n\r\n\r\n`UnsupportedOperationError: 'css selector' is not valid or implemented searching strategy.`","closed_by":{"login":"jleyba","id":254985,"node_id":"MDQ6VXNlcjI1NDk4NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/254985?v=4","gravatar_id":"","url":"https://api.github.com/users/jleyba","html_url":"https://github.com/jleyba","followers_url":"https://api.github.com/users/jleyba/followers","following_url":"https://api.github.com/users/jleyba/following{/other_user}","gists_url":"https://api.github.com/users/jleyba/gists{/gist_id}","starred_url":"https://api.github.com/users/jleyba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jleyba/subscriptions","organizations_url":"https://api.github.com/users/jleyba/orgs","repos_url":"https://api.github.com/users/jleyba/repos","events_url":"https://api.github.com/users/jleyba/events{/privacy}","received_events_url":"https://api.github.com/users/jleyba/received_events","type":"User","site_admin":false}}", "commentIds":["323130642"], "labels":["C-nodejs"]}