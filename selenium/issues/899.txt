{"id":"899", "title":"Impossible to registering the node to the hub", "body":"Hello I got an error when on my log file. It seems if I add a new node with selenium grid extra pointing on my hub. I got this error below in the log. (I changed the ip address by XXX:XX:XX:XX).

> 16:52:37.429 INFO [17] org.openqa.grid.internal.utils.SelfRegisteringRemote - Registering the node to the hub: http://XXX:XX:XX:XX:4444/grid/register
> 16:52:37.444 INFO [17] org.openqa.grid.internal.utils.SelfRegisteringRemote - Couldn't register this node: Error sending the registration request: The hub responded with 500:Could not initialize class com.groupon.seleniumgridextras.grid.proxies.SetupTeardownProxy

My hub config file is 

``` javascript
{
  \"port\": 4444,
  \"newSessionWaitTimeout\": 25000,
  \"servlets\": [
    \"com.groupon.seleniumgridextras.grid.servlets.SeleniumGridExtrasServlet\",
    \"com.groupon.seleniumgridextras.grid.servlets.ProxyStatusJsonServlet\"
  ],
  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",
  \"throwOnCapabilityNotPresent\": true,
  \"nodePolling\": 5000,
  \"cleanUpCycle\": 5000,
  \"browserTimeout\": 120000,
  \"timeout\": 120000,
  \"maxSession\": 5
}
```

And my node config file is : 

``` javascript
{
  \"capabilities\": [
    {
      \"seleniumProtocol\": \"WebDriver\",
      \"browserName\": \"firefox\",
      \"maxInstances\": 3,
      \"version\": \"37\",
      \"platform\": \"VISTA\"
    },
    {
      \"seleniumProtocol\": \"WebDriver\",
      \"browserName\": \"internet explorer\",
      \"maxInstances\": 1,
      \"version\": \"8\",
      \"platform\": \"VISTA\"
    },
    {
      \"seleniumProtocol\": \"WebDriver\",
      \"browserName\": \"chrome\",
      \"maxInstances\": 3,
      \"version\": \"43\",
      \"platform\": \"VISTA\"
    }
  ],
  \"configuration\": {
    \"proxy\": \"com.groupon.seleniumgridextras.grid.proxies.SetupTeardownProxy\",
    \"maxSession\": 3,
    \"port\": 5555,
    \"register\": true,
    \"unregisterIfStillDownAfter\": 10000,
    \"hubPort\": 4444,
    \"hubHost\": \"XXX:XX:XX:XX\",
    \"nodeStatusCheckTimeout\": 10000,
    \"downPollingLimit\": 0
  },
  \"loadedFromFile\": \"node_5555.json\"
}
```

Do you have any clue ? 
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/899","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/899/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/899/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/899/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/899","id":99684880,"node_id":"MDU6SXNzdWU5OTY4NDg4MA==","number":899,"title":"Impossible to registering the node to the hub","user":{"login":"mikyone","id":987162,"node_id":"MDQ6VXNlcjk4NzE2Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/987162?v=4","gravatar_id":"","url":"https://api.github.com/users/mikyone","html_url":"https://github.com/mikyone","followers_url":"https://api.github.com/users/mikyone/followers","following_url":"https://api.github.com/users/mikyone/following{/other_user}","gists_url":"https://api.github.com/users/mikyone/gists{/gist_id}","starred_url":"https://api.github.com/users/mikyone/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mikyone/subscriptions","organizations_url":"https://api.github.com/users/mikyone/orgs","repos_url":"https://api.github.com/users/mikyone/repos","events_url":"https://api.github.com/users/mikyone/events{/privacy}","received_events_url":"https://api.github.com/users/mikyone/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2015-08-07T16:41:29Z","updated_at":"2019-08-21T06:09:49Z","closed_at":"2015-08-12T13:56:35Z","author_association":"NONE","body":"Hello I got an error when on my log file. It seems if I add a new node with selenium grid extra pointing on my hub. I got this error below in the log. (I changed the ip address by XXX:XX:XX:XX).\n\n> 16:52:37.429 INFO [17] org.openqa.grid.internal.utils.SelfRegisteringRemote - Registering the node to the hub: http://XXX:XX:XX:XX:4444/grid/register\n> 16:52:37.444 INFO [17] org.openqa.grid.internal.utils.SelfRegisteringRemote - Couldn't register this node: Error sending the registration request: The hub responded with 500:Could not initialize class com.groupon.seleniumgridextras.grid.proxies.SetupTeardownProxy\n\nMy hub config file is \n\n``` javascript\n{\n  \"port\": 4444,\n  \"newSessionWaitTimeout\": 25000,\n  \"servlets\": [\n    \"com.groupon.seleniumgridextras.grid.servlets.SeleniumGridExtrasServlet\",\n    \"com.groupon.seleniumgridextras.grid.servlets.ProxyStatusJsonServlet\"\n  ],\n  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",\n  \"throwOnCapabilityNotPresent\": true,\n  \"nodePolling\": 5000,\n  \"cleanUpCycle\": 5000,\n  \"browserTimeout\": 120000,\n  \"timeout\": 120000,\n  \"maxSession\": 5\n}\n```\n\nAnd my node config file is : \n\n``` javascript\n{\n  \"capabilities\": [\n    {\n      \"seleniumProtocol\": \"WebDriver\",\n      \"browserName\": \"firefox\",\n      \"maxInstances\": 3,\n      \"version\": \"37\",\n      \"platform\": \"VISTA\"\n    },\n    {\n      \"seleniumProtocol\": \"WebDriver\",\n      \"browserName\": \"internet explorer\",\n      \"maxInstances\": 1,\n      \"version\": \"8\",\n      \"platform\": \"VISTA\"\n    },\n    {\n      \"seleniumProtocol\": \"WebDriver\",\n      \"browserName\": \"chrome\",\n      \"maxInstances\": 3,\n      \"version\": \"43\",\n      \"platform\": \"VISTA\"\n    }\n  ],\n  \"configuration\": {\n    \"proxy\": \"com.groupon.seleniumgridextras.grid.proxies.SetupTeardownProxy\",\n    \"maxSession\": 3,\n    \"port\": 5555,\n    \"register\": true,\n    \"unregisterIfStillDownAfter\": 10000,\n    \"hubPort\": 4444,\n    \"hubHost\": \"XXX:XX:XX:XX\",\n    \"nodeStatusCheckTimeout\": 10000,\n    \"downPollingLimit\": 0\n  },\n  \"loadedFromFile\": \"node_5555.json\"\n}\n```\n\nDo you have any clue ? \n","closed_by":{"login":"ddavison","id":2972876,"node_id":"MDQ6VXNlcjI5NzI4NzY=","avatar_url":"https://avatars3.githubusercontent.com/u/2972876?v=4","gravatar_id":"","url":"https://api.github.com/users/ddavison","html_url":"https://github.com/ddavison","followers_url":"https://api.github.com/users/ddavison/followers","following_url":"https://api.github.com/users/ddavison/following{/other_user}","gists_url":"https://api.github.com/users/ddavison/gists{/gist_id}","starred_url":"https://api.github.com/users/ddavison/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ddavison/subscriptions","organizations_url":"https://api.github.com/users/ddavison/orgs","repos_url":"https://api.github.com/users/ddavison/repos","events_url":"https://api.github.com/users/ddavison/events{/privacy}","received_events_url":"https://api.github.com/users/ddavison/received_events","type":"User","site_admin":false}}", "commentIds":["128767041","130314206","130382783","130385858","130422943"], "labels":[]}