{"id":"1756", "title":"Node configuration with JSON", "body":"Hi,

I didn't really know where to post it, so I think an issue might be ok.
When configuring a node with a JSON file, we've got:

```
{\"browserName\": \"firefox\",
  \"platform\": \"WINDOWS\"}
```

For Chrome and IE, we need to provide the custom path to driver. But, the driver property key is in the form `webdriver.BROWSER.driver`

Shouldn't it be better to be something like `driver` or `webdriver` to properly match a POJO class for example ?
Because we can face:

```
{\"browserName\": \"chrome\",
\"webdriver.chrome.driver\": \"chrome\",
  \"platform\": \"WINDOWS\"}

{\"browserName\": \"internet explorer\",
\"webdriver.ie.driver\": \"chrome\",
  \"platform\": \"WINDOWS\"}
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1756","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1756/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1756/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1756/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1756","id":138905399,"node_id":"MDU6SXNzdWUxMzg5MDUzOTk=","number":1756,"title":"Node configuration with JSON","user":{"login":"Buzz2Buzz","id":6714374,"node_id":"MDQ6VXNlcjY3MTQzNzQ=","avatar_url":"https://avatars1.githubusercontent.com/u/6714374?v=4","gravatar_id":"","url":"https://api.github.com/users/Buzz2Buzz","html_url":"https://github.com/Buzz2Buzz","followers_url":"https://api.github.com/users/Buzz2Buzz/followers","following_url":"https://api.github.com/users/Buzz2Buzz/following{/other_user}","gists_url":"https://api.github.com/users/Buzz2Buzz/gists{/gist_id}","starred_url":"https://api.github.com/users/Buzz2Buzz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Buzz2Buzz/subscriptions","organizations_url":"https://api.github.com/users/Buzz2Buzz/orgs","repos_url":"https://api.github.com/users/Buzz2Buzz/repos","events_url":"https://api.github.com/users/Buzz2Buzz/events{/privacy}","received_events_url":"https://api.github.com/users/Buzz2Buzz/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-03-07T07:12:15Z","updated_at":"2019-08-20T12:09:37Z","closed_at":"2016-03-07T14:27:22Z","author_association":"NONE","body":"Hi,\n\nI didn't really know where to post it, so I think an issue might be ok.\nWhen configuring a node with a JSON file, we've got:\n\n```\n{\"browserName\": \"firefox\",\n  \"platform\": \"WINDOWS\"}\n```\n\nFor Chrome and IE, we need to provide the custom path to driver. But, the driver property key is in the form `webdriver.BROWSER.driver`\n\nShouldn't it be better to be something like `driver` or `webdriver` to properly match a POJO class for example ?\nBecause we can face:\n\n```\n{\"browserName\": \"chrome\",\n\"webdriver.chrome.driver\": \"chrome\",\n  \"platform\": \"WINDOWS\"}\n\n{\"browserName\": \"internet explorer\",\n\"webdriver.ie.driver\": \"chrome\",\n  \"platform\": \"WINDOWS\"}\n```\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["193270871"], "labels":[]}