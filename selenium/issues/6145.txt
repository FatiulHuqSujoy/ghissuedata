{"id":"6145", "title":"Socket not found exception, Multiple test runs fails", "body":"## Meta -
OS:  
Windows 10 1803

Selenium Version:  
3.13.1.0
Browser: 
Chrome Version 67.0.3396.99 (Offizieller Build) (64-Bit)
chromedriver.exe 2.40.0 from Nuget

Browser Version:  
67.0.3396.99 (Offizieller Build) (64-Bit)

Test Framework:
xUnit

Programming IDE:
Visual Studio 2017 v15.7.5

## Expected Behavior -
Select multiple test from test explorer and run in debug mode
## Actual Behavior -
Select multiple test from test explorer and run in debug mode.  Only the first test finished succesfuly.
When the second test wants to initialize the driver it crashes with the folowing exception:

`Testname:	SellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator
Test FullName:	SellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator
Testquelle:	D:\\workspace\\...\\...\\ChromeTests.cs Zeile 39
Testergebnis:	Fehlgeschlagene
Testdauer:	0:00:41,699

Ergebnis StackTrace:	
----- Inner Stack Trace #1 (OpenQA.Selenium.WebDriverException) -----
   bei OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)
   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   bei OpenQA.Selenium.Remote.RemoteTimeouts.ExecuteSetTimeout(String timeoutType, TimeSpan timeToWait)
   bei OpenQA.Selenium.Remote.RemoteTimeouts.set_PageLoad(TimeSpan value)
   bei SellyconSeleniumLibrary.WrapperFactory.BrowserFactory.InitBrowser[T,TDriver](Action`1 optionCallback, TDriver tdriver)
   bei SellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator() in D:\\workspace\\SellyconSeleniumIntegration\\SellyconSelniumXUnit\\ChromeTests.cs:Zeile 41.
----- Inner Stack Trace #2 (OpenQA.Selenium.WebDriverException) -----
   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)
   bei OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)
   bei OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)
   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   bei OpenQA.Selenium.Remote.RemoteWebDriver.Close()
   bei SellyconSeleniumLibrary.WrapperFactory.BrowserFactory.CloseDriver()
   bei SellyconSelniumXUnit.ChromeTests.Dispose() in D:\\workspace\\SellyconSeleniumIntegration\\SellyconSelniumXUnit\\ChromeTests.cs:Zeile 267.
   bei Xunit.Sdk.ExecutionTimer.Aggregate(Action action)
   bei ReflectionAbstractionExtensions.DisposeTestClass(ITest test, Object testClass, IMessageBus messageBus, ExecutionTimer timer, CancellationTokenSource cancellationTokenSource)
   bei Xunit.Sdk.TestInvoker`1.<>c__DisplayClass47_0.<RunAsync>b__1()
   bei Xunit.Sdk.ExceptionAggregator.Run(Action code)
----- Inner Stack Trace -----
   bei System.Net.HttpWebRequest.GetResponse()
   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)
----- Inner Stack Trace -----
   bei System.Net.Sockets.Socket.DoConnect(EndPoint endPointSnapshot, SocketAddress socketAddress)
   bei System.Net.ServicePoint.ConnectSocketInternal(Boolean connectFailure, Socket s4, Socket s6, Socket& socket, IPAddress& address, ConnectSocketState state, IAsyncResult asyncResult, Exception& exception)
Ergebnis Meldung:	
System.AggregateException : Mindestens ein Fehler ist aufgetreten.
---- OpenQA.Selenium.WebDriverException : Unexpected error. System.Net.WebException: Die Verbindung mit dem Remoteserver kann nicht hergestellt werden. ---> System.Net.Sockets.SocketException: Es konnte keine Verbindung hergestellt werden, da der Zielcomputer die Verbindung verweigerte 127.0.0.1:63311
   bei System.Net.Sockets.Socket.DoConnect(EndPoint endPointSnapshot, SocketAddress socketAddress)
   bei System.Net.ServicePoint.ConnectSocketInternal(Boolean connectFailure, Socket s4, Socket s6, Socket& socket, IPAddress& address, ConnectSocketState state, IAsyncResult asyncResult, Exception& exception)
   --- Ende der internen Ausnahmestapelüberwachung ---
   bei System.Net.HttpWebRequest.GetRequestStream(TransportContext& context)
   bei System.Net.HttpWebRequest.GetRequestStream()
   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)
   bei OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)
   bei OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)
   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
---- OpenQA.Selenium.WebDriverException : A exception with a null response was thrown sending an HTTP request to the remote WebDriver server for URL http://localhost:63311/session//window. The status of the exception was ConnectFailure, and the message was: Die Verbindung mit dem Remoteserver kann nicht hergestellt werden.
-------- System.Net.WebException : Die Verbindung mit dem Remoteserver kann nicht hergestellt werden.
------------ System.Net.Sockets.SocketException : Es konnte keine Verbindung hergestellt werden, da der Zielcomputer die Verbindung verweigerte 127.0.0.1:63311

`

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6145","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6145/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6145/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6145/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6145","id":340281267,"node_id":"MDU6SXNzdWUzNDAyODEyNjc=","number":6145,"title":"Socket not found exception, Multiple test runs fails","user":{"login":"FabianO0815","id":41120132,"node_id":"MDQ6VXNlcjQxMTIwMTMy","avatar_url":"https://avatars1.githubusercontent.com/u/41120132?v=4","gravatar_id":"","url":"https://api.github.com/users/FabianO0815","html_url":"https://github.com/FabianO0815","followers_url":"https://api.github.com/users/FabianO0815/followers","following_url":"https://api.github.com/users/FabianO0815/following{/other_user}","gists_url":"https://api.github.com/users/FabianO0815/gists{/gist_id}","starred_url":"https://api.github.com/users/FabianO0815/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/FabianO0815/subscriptions","organizations_url":"https://api.github.com/users/FabianO0815/orgs","repos_url":"https://api.github.com/users/FabianO0815/repos","events_url":"https://api.github.com/users/FabianO0815/events{/privacy}","received_events_url":"https://api.github.com/users/FabianO0815/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-07-11T14:58:56Z","updated_at":"2019-08-15T19:09:43Z","closed_at":"2018-07-11T15:11:01Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10 1803\r\n\r\nSelenium Version:  \r\n3.13.1.0\r\nBrowser: \r\nChrome Version 67.0.3396.99 (Offizieller Build) (64-Bit)\r\nchromedriver.exe 2.40.0 from Nuget\r\n\r\nBrowser Version:  \r\n67.0.3396.99 (Offizieller Build) (64-Bit)\r\n\r\nTest Framework:\r\nxUnit\r\n\r\nProgramming IDE:\r\nVisual Studio 2017 v15.7.5\r\n\r\n## Expected Behavior -\r\nSelect multiple test from test explorer and run in debug mode\r\n## Actual Behavior -\r\nSelect multiple test from test explorer and run in debug mode.  Only the first test finished succesfuly.\r\nWhen the second test wants to initialize the driver it crashes with the folowing exception:\r\n\r\n`Testname:\tSellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator\r\nTest FullName:\tSellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator\r\nTestquelle:\tD:\\workspace\\...\\...\\ChromeTests.cs Zeile 39\r\nTestergebnis:\tFehlgeschlagene\r\nTestdauer:\t0:00:41,699\r\n\r\nErgebnis StackTrace:\t\r\n----- Inner Stack Trace #1 (OpenQA.Selenium.WebDriverException) -----\r\n   bei OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)\r\n   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   bei OpenQA.Selenium.Remote.RemoteTimeouts.ExecuteSetTimeout(String timeoutType, TimeSpan timeToWait)\r\n   bei OpenQA.Selenium.Remote.RemoteTimeouts.set_PageLoad(TimeSpan value)\r\n   bei SellyconSeleniumLibrary.WrapperFactory.BrowserFactory.InitBrowser[T,TDriver](Action`1 optionCallback, TDriver tdriver)\r\n   bei SellyconSelniumXUnit.ChromeTests.ChromeUploadDxfConfigurator() in D:\\workspace\\SellyconSeleniumIntegration\\SellyconSelniumXUnit\\ChromeTests.cs:Zeile 41.\r\n----- Inner Stack Trace #2 (OpenQA.Selenium.WebDriverException) -----\r\n   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)\r\n   bei OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)\r\n   bei OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)\r\n   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   bei OpenQA.Selenium.Remote.RemoteWebDriver.Close()\r\n   bei SellyconSeleniumLibrary.WrapperFactory.BrowserFactory.CloseDriver()\r\n   bei SellyconSelniumXUnit.ChromeTests.Dispose() in D:\\workspace\\SellyconSeleniumIntegration\\SellyconSelniumXUnit\\ChromeTests.cs:Zeile 267.\r\n   bei Xunit.Sdk.ExecutionTimer.Aggregate(Action action)\r\n   bei ReflectionAbstractionExtensions.DisposeTestClass(ITest test, Object testClass, IMessageBus messageBus, ExecutionTimer timer, CancellationTokenSource cancellationTokenSource)\r\n   bei Xunit.Sdk.TestInvoker`1.<>c__DisplayClass47_0.<RunAsync>b__1()\r\n   bei Xunit.Sdk.ExceptionAggregator.Run(Action code)\r\n----- Inner Stack Trace -----\r\n   bei System.Net.HttpWebRequest.GetResponse()\r\n   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)\r\n----- Inner Stack Trace -----\r\n   bei System.Net.Sockets.Socket.DoConnect(EndPoint endPointSnapshot, SocketAddress socketAddress)\r\n   bei System.Net.ServicePoint.ConnectSocketInternal(Boolean connectFailure, Socket s4, Socket s6, Socket& socket, IPAddress& address, ConnectSocketState state, IAsyncResult asyncResult, Exception& exception)\r\nErgebnis Meldung:\t\r\nSystem.AggregateException : Mindestens ein Fehler ist aufgetreten.\r\n---- OpenQA.Selenium.WebDriverException : Unexpected error. System.Net.WebException: Die Verbindung mit dem Remoteserver kann nicht hergestellt werden. ---> System.Net.Sockets.SocketException: Es konnte keine Verbindung hergestellt werden, da der Zielcomputer die Verbindung verweigerte 127.0.0.1:63311\r\n   bei System.Net.Sockets.Socket.DoConnect(EndPoint endPointSnapshot, SocketAddress socketAddress)\r\n   bei System.Net.ServicePoint.ConnectSocketInternal(Boolean connectFailure, Socket s4, Socket s6, Socket& socket, IPAddress& address, ConnectSocketState state, IAsyncResult asyncResult, Exception& exception)\r\n   --- Ende der internen Ausnahmestapelüberwachung ---\r\n   bei System.Net.HttpWebRequest.GetRequestStream(TransportContext& context)\r\n   bei System.Net.HttpWebRequest.GetRequestStream()\r\n   bei OpenQA.Selenium.Remote.HttpCommandExecutor.MakeHttpRequest(HttpRequestInfo requestInfo)\r\n   bei OpenQA.Selenium.Remote.HttpCommandExecutor.Execute(Command commandToExecute)\r\n   bei OpenQA.Selenium.Remote.DriverServiceCommandExecutor.Execute(Command commandToExecute)\r\n   bei OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n---- OpenQA.Selenium.WebDriverException : A exception with a null response was thrown sending an HTTP request to the remote WebDriver server for URL http://localhost:63311/session//window. The status of the exception was ConnectFailure, and the message was: Die Verbindung mit dem Remoteserver kann nicht hergestellt werden.\r\n-------- System.Net.WebException : Die Verbindung mit dem Remoteserver kann nicht hergestellt werden.\r\n------------ System.Net.Sockets.SocketException : Es konnte keine Verbindung hergestellt werden, da der Zielcomputer die Verbindung verweigerte 127.0.0.1:63311\r\n\r\n`\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["404205297"], "labels":[]}