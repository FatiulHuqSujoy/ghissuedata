{"id":"4334", "title":"Selenium (or Firefox/geckodriver) fails to type space characters", "body":"## Meta -
OS:  OS X, Linux
Selenium Version:  3.4.3
Browser:  Firefox/geckodriver
Browser Version:  54.0.1/0.18.0

## Expected Behavior -
Given I have a handle on a text field element
When I send `Keys.SPACE` with `send_keys()`
Then I see a space character in the text field

## Actual Behavior -
No space character appears in the text field.

## Steps to reproduce -
```python
from unittest import TestCase, main

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys


class TestSendKeys(TestCase):
    def setUp(self):
        capabilities = DesiredCapabilities.FIREFOX.copy()
        capabilities[\"marionette\"] = True
        self.browser = webdriver.Firefox(capabilities=capabilities)
        self.browser.get(\"https://mdn.github.io/learning-area/html/forms/editable-input-example/editable_input.html\")
        self.element = self.browser.find_element_by_css_selector(\"input[type='text']\")
        
    def tearDown(self):
        self.browser.quit()

    def test_special_characters(self):
        self.element.send_keys(\"ocean\", Keys.SPACE, \"sie\", Keys.LEFT, \"d\")
        self.assertEqual(self.element.get_attribute(\"value\"), \"ocean side\")

if __name__ == \"__main__\":
    main()
```

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4334","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4334/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4334/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4334/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4334","id":243794283,"node_id":"MDU6SXNzdWUyNDM3OTQyODM=","number":4334,"title":"Selenium (or Firefox/geckodriver) fails to type space characters","user":{"login":"elliterate","id":6326,"node_id":"MDQ6VXNlcjYzMjY=","avatar_url":"https://avatars0.githubusercontent.com/u/6326?v=4","gravatar_id":"","url":"https://api.github.com/users/elliterate","html_url":"https://github.com/elliterate","followers_url":"https://api.github.com/users/elliterate/followers","following_url":"https://api.github.com/users/elliterate/following{/other_user}","gists_url":"https://api.github.com/users/elliterate/gists{/gist_id}","starred_url":"https://api.github.com/users/elliterate/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elliterate/subscriptions","organizations_url":"https://api.github.com/users/elliterate/orgs","repos_url":"https://api.github.com/users/elliterate/repos","events_url":"https://api.github.com/users/elliterate/events{/privacy}","received_events_url":"https://api.github.com/users/elliterate/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2017-07-18T17:37:05Z","updated_at":"2019-08-18T02:09:34Z","closed_at":"2017-07-26T09:28:17Z","author_association":"CONTRIBUTOR","body":"## Meta -\r\nOS:  OS X, Linux\r\nSelenium Version:  3.4.3\r\nBrowser:  Firefox/geckodriver\r\nBrowser Version:  54.0.1/0.18.0\r\n\r\n## Expected Behavior -\r\nGiven I have a handle on a text field element\r\nWhen I send `Keys.SPACE` with `send_keys()`\r\nThen I see a space character in the text field\r\n\r\n## Actual Behavior -\r\nNo space character appears in the text field.\r\n\r\n## Steps to reproduce -\r\n```python\r\nfrom unittest import TestCase, main\r\n\r\nfrom selenium import webdriver\r\nfrom selenium.webdriver.common.desired_capabilities import DesiredCapabilities\r\nfrom selenium.webdriver.common.keys import Keys\r\n\r\n\r\nclass TestSendKeys(TestCase):\r\n    def setUp(self):\r\n        capabilities = DesiredCapabilities.FIREFOX.copy()\r\n        capabilities[\"marionette\"] = True\r\n        self.browser = webdriver.Firefox(capabilities=capabilities)\r\n        self.browser.get(\"https://mdn.github.io/learning-area/html/forms/editable-input-example/editable_input.html\")\r\n        self.element = self.browser.find_element_by_css_selector(\"input[type='text']\")\r\n        \r\n    def tearDown(self):\r\n        self.browser.quit()\r\n\r\n    def test_special_characters(self):\r\n        self.element.send_keys(\"ocean\", Keys.SPACE, \"sie\", Keys.LEFT, \"d\")\r\n        self.assertEqual(self.element.get_attribute(\"value\"), \"ocean side\")\r\n\r\nif __name__ == \"__main__\":\r\n    main()\r\n```\r\n\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["318000638","318040955","318058046","318077547"], "labels":[]}