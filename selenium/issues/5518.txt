{"id":"5518", "title":"Object throwing null pointer exception while running with android Driver using Selenium 3.8.1", "body":"The Object map is not getting initialised using the android driver. The object is throwing null pointer exception whenever trying to use any sendkeys or click.

Please find below the code snippet : -

Code used in OR :

```
@FindAll({ @FindBy(xpath = \"//*[@id='tsbb']\") })
    public WebElement Google_homepage__ssearchBTN;

WebDriver driver;
public tesitng_android_om(WebDriver driver){
    this.driver=driver;
}
```
Inside Testcase :

        Android driver = new AndroidDriver(sURL, oCap); 

        tesitng_android_om tesitng_android_om = new tesitng_android_om((WebDriver)driver);

        PageFactory.initElements((WebDriver)driver,tesitng_android_om);

        tesitng_android_om.Google_homepage__ssearchBTN.click();
The Same code works fine with **Selenium 3.4.0** but is not working in **3.8.1**

Following is the stack trace of the issue :

> Error: java.lang.NullPointerException at
> org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)
> at
> org.openqa.selenium.remote.RemoteWebElement.isDisplayed(RemoteWebElement.java:320)
> at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) at
> sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) at
> sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source) at
> java.lang.reflect.Method.invoke(Unknown Source) at
> org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:51)
> at com.sun.proxy.$Proxy11.isDisplayed(Unknown Source) at
> org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:315)
> at
> org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:44)
> at
> org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:301)
> at
> org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:298)
> at
> org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:686)
> at
> org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:682)
> at
> org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)
> at myPackage.library.Android$Helper.checkReady(Android.java:2397) at
> myPackage.library.Android$Web.awType(Android.java:1108) at
> myPackage.testScripts.test_android_tcweb.test(test_android_tcweb.java:64)
> at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) at
> sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) at
> sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source) at
> java.lang.reflect.Method.invoke(Unknown Source) at
> org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:84)
> at org.testng.internal.Invoker.invokeMethod(Invoker.java:714) at
> org.testng.internal.Invoker.invokeTestMethod(Invoker.java:901) at
> org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1231) at
> org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:127)
> at org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:111)
> at org.testng.TestRunner.privateRun(TestRunner.java:767) at
> org.testng.TestRunner.run(TestRunner.java:617) at
> org.testng.SuiteRunner.runTest(SuiteRunner.java:334) at
> org.testng.SuiteRunner.access$000(SuiteRunner.java:37) at
> org.testng.SuiteRunner$SuiteWorker.run(SuiteRunner.java:368) at
> org.testng.internal.thread.ThreadUtil$2.call(ThreadUtil.java:64) at
> java.util.concurrent.FutureTask.run(Unknown Source) at
> java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source) at
> java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source) at
> java.lang.Thread.run(Unknown Source)
> -- false -- [Expected - True, Actual - False]", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5518","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5518/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5518/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5518/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5518","id":298943941,"node_id":"MDU6SXNzdWUyOTg5NDM5NDE=","number":5518,"title":"Object throwing null pointer exception while running with android Driver using Selenium 3.8.1","user":{"login":"samnit","id":28050899,"node_id":"MDQ6VXNlcjI4MDUwODk5","avatar_url":"https://avatars3.githubusercontent.com/u/28050899?v=4","gravatar_id":"","url":"https://api.github.com/users/samnit","html_url":"https://github.com/samnit","followers_url":"https://api.github.com/users/samnit/followers","following_url":"https://api.github.com/users/samnit/following{/other_user}","gists_url":"https://api.github.com/users/samnit/gists{/gist_id}","starred_url":"https://api.github.com/users/samnit/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/samnit/subscriptions","organizations_url":"https://api.github.com/users/samnit/orgs","repos_url":"https://api.github.com/users/samnit/repos","events_url":"https://api.github.com/users/samnit/events{/privacy}","received_events_url":"https://api.github.com/users/samnit/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-02-21T12:03:19Z","updated_at":"2019-08-16T12:10:04Z","closed_at":"2018-02-23T16:33:18Z","author_association":"NONE","body":"The Object map is not getting initialised using the android driver. The object is throwing null pointer exception whenever trying to use any sendkeys or click.\r\n\r\nPlease find below the code snippet : -\r\n\r\nCode used in OR :\r\n\r\n```\r\n@FindAll({ @FindBy(xpath = \"//*[@id='tsbb']\") })\r\n    public WebElement Google_homepage__ssearchBTN;\r\n\r\nWebDriver driver;\r\npublic tesitng_android_om(WebDriver driver){\r\n    this.driver=driver;\r\n}\r\n```\r\nInside Testcase :\r\n\r\n        Android driver = new AndroidDriver(sURL, oCap); \r\n\r\n        tesitng_android_om tesitng_android_om = new tesitng_android_om((WebDriver)driver);\r\n\r\n        PageFactory.initElements((WebDriver)driver,tesitng_android_om);\r\n\r\n        tesitng_android_om.Google_homepage__ssearchBTN.click();\r\nThe Same code works fine with **Selenium 3.4.0** but is not working in **3.8.1**\r\n\r\nFollowing is the stack trace of the issue :\r\n\r\n> Error: java.lang.NullPointerException at\r\n> org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n> at\r\n> org.openqa.selenium.remote.RemoteWebElement.isDisplayed(RemoteWebElement.java:320)\r\n> at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) at\r\n> sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) at\r\n> sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source) at\r\n> java.lang.reflect.Method.invoke(Unknown Source) at\r\n> org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:51)\r\n> at com.sun.proxy.$Proxy11.isDisplayed(Unknown Source) at\r\n> org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:315)\r\n> at\r\n> org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:44)\r\n> at\r\n> org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:301)\r\n> at\r\n> org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:298)\r\n> at\r\n> org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:686)\r\n> at\r\n> org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:682)\r\n> at\r\n> org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n> at myPackage.library.Android$Helper.checkReady(Android.java:2397) at\r\n> myPackage.library.Android$Web.awType(Android.java:1108) at\r\n> myPackage.testScripts.test_android_tcweb.test(test_android_tcweb.java:64)\r\n> at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) at\r\n> sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) at\r\n> sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source) at\r\n> java.lang.reflect.Method.invoke(Unknown Source) at\r\n> org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:84)\r\n> at org.testng.internal.Invoker.invokeMethod(Invoker.java:714) at\r\n> org.testng.internal.Invoker.invokeTestMethod(Invoker.java:901) at\r\n> org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1231) at\r\n> org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:127)\r\n> at org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:111)\r\n> at org.testng.TestRunner.privateRun(TestRunner.java:767) at\r\n> org.testng.TestRunner.run(TestRunner.java:617) at\r\n> org.testng.SuiteRunner.runTest(SuiteRunner.java:334) at\r\n> org.testng.SuiteRunner.access$000(SuiteRunner.java:37) at\r\n> org.testng.SuiteRunner$SuiteWorker.run(SuiteRunner.java:368) at\r\n> org.testng.internal.thread.ThreadUtil$2.call(ThreadUtil.java:64) at\r\n> java.util.concurrent.FutureTask.run(Unknown Source) at\r\n> java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source) at\r\n> java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source) at\r\n> java.lang.Thread.run(Unknown Source)\r\n> -- false -- [Expected - True, Actual - False]","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["367982751","368061676"], "labels":[]}