{"id":"4073", "title":"Not able to open Mozilla FF Browser", "body":"## Meta -
OS:  
<!-- Windows 10? OSX? -->
Selenium Version:  
<Selenium-java 3.4->
< geckodriver-v0.16.1-win64
Browser:  
<!--   Firefox 53.0?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -

## Actual Behavior -

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4073","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4073/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4073/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4073/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4073","id":231101451,"node_id":"MDU6SXNzdWUyMzExMDE0NTE=","number":4073,"title":"Not able to open Mozilla FF Browser","user":{"login":"kadaga","id":26859083,"node_id":"MDQ6VXNlcjI2ODU5MDgz","avatar_url":"https://avatars1.githubusercontent.com/u/26859083?v=4","gravatar_id":"","url":"https://api.github.com/users/kadaga","html_url":"https://github.com/kadaga","followers_url":"https://api.github.com/users/kadaga/followers","following_url":"https://api.github.com/users/kadaga/following{/other_user}","gists_url":"https://api.github.com/users/kadaga/gists{/gist_id}","starred_url":"https://api.github.com/users/kadaga/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kadaga/subscriptions","organizations_url":"https://api.github.com/users/kadaga/orgs","repos_url":"https://api.github.com/users/kadaga/repos","events_url":"https://api.github.com/users/kadaga/events{/privacy}","received_events_url":"https://api.github.com/users/kadaga/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2017-05-24T16:37:51Z","updated_at":"2019-08-18T09:10:02Z","closed_at":"2017-05-24T17:45:51Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  \r\n<Selenium-java 3.4->\r\n< geckodriver-v0.16.1-win64\r\nBrowser:  \r\n<!--   Firefox 53.0?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\n\r\n## Actual Behavior -\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"AutomatedTester","id":128518,"node_id":"MDQ6VXNlcjEyODUxOA==","avatar_url":"https://avatars1.githubusercontent.com/u/128518?v=4","gravatar_id":"","url":"https://api.github.com/users/AutomatedTester","html_url":"https://github.com/AutomatedTester","followers_url":"https://api.github.com/users/AutomatedTester/followers","following_url":"https://api.github.com/users/AutomatedTester/following{/other_user}","gists_url":"https://api.github.com/users/AutomatedTester/gists{/gist_id}","starred_url":"https://api.github.com/users/AutomatedTester/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/AutomatedTester/subscriptions","organizations_url":"https://api.github.com/users/AutomatedTester/orgs","repos_url":"https://api.github.com/users/AutomatedTester/repos","events_url":"https://api.github.com/users/AutomatedTester/events{/privacy}","received_events_url":"https://api.github.com/users/AutomatedTester/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":[]}