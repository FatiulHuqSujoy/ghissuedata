{"id":"1161", "title":"Firefox: logs().get( \"browser\" ) doesn't show console errors?", "body":"I'm trying to save the console logs after each test but having some problems with Firefox.
With a test like:

```
describe(\"protractor/webdriver\", function() {
    it(\"should be able to get browser logs\", function() {
        browser.get(\"https://angularjs.org\");

        browser.executeScript(\"console.log('Nothing to see here - move along.')\");
        browser.executeScript(\"console.warn('This is your first warning!')\");
        browser.executeScript(\"console.error('This is serious!!!')\");

        browser.driver.manage().logs().get( \"browser\" ).then(function( logsEntries ) {
            logsEntries.forEach( (item) => {
                console.log( \"LOG LEVEL:\", item.level.name, item.message );
            });
        });
    });
});
```

For Chrome I get the result nicely:

```
[chrome #1] LOG LEVEL: WARNING console-api 240:41 This is your first warning!
[chrome #1] LOG LEVEL: SEVERE console-api 240:41 This is serious!!!
```

But Firefox only returns \"Internal\" warnings (CSS parse):

```
[firefox #2] LOG LEVEL: WARNING Unknown property 'zoom'.  Declaration dropped.
[firefox #2] LOG LEVEL: WARNING Unknown property 'user-select'.  Declaration dropped.
[firefox #2] LOG LEVEL: WARNING Unknown property 'zoom'.  Declaration dropped.
[firefox #2] LOG LEVEL: WARNING Error in parsing value for 'background-image'.  Declaration dropped.
[firefox #2] LOG LEVEL: WARNING Error in parsing value for 'background-image'.  Declaration dropped.
[firefox #2] LOG LEVEL: WARNING Expected media feature name but found '-webkit-min-device-pixel-ratio'.
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1161","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1161/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1161/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1161/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1161","id":111554753,"node_id":"MDU6SXNzdWUxMTE1NTQ3NTM=","number":1161,"title":"Firefox: logs().get( \"browser\" ) doesn't show console errors?","user":{"login":"axelssonHakan","id":5768580,"node_id":"MDQ6VXNlcjU3Njg1ODA=","avatar_url":"https://avatars1.githubusercontent.com/u/5768580?v=4","gravatar_id":"","url":"https://api.github.com/users/axelssonHakan","html_url":"https://github.com/axelssonHakan","followers_url":"https://api.github.com/users/axelssonHakan/followers","following_url":"https://api.github.com/users/axelssonHakan/following{/other_user}","gists_url":"https://api.github.com/users/axelssonHakan/gists{/gist_id}","starred_url":"https://api.github.com/users/axelssonHakan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/axelssonHakan/subscriptions","organizations_url":"https://api.github.com/users/axelssonHakan/orgs","repos_url":"https://api.github.com/users/axelssonHakan/repos","events_url":"https://api.github.com/users/axelssonHakan/events{/privacy}","received_events_url":"https://api.github.com/users/axelssonHakan/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false},{"id":316439733,"node_id":"MDU6TGFiZWwzMTY0Mzk3MzM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/I-enhancement","name":"I-enhancement","color":"eb6420","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":14,"created_at":"2015-10-15T06:38:36Z","updated_at":"2019-08-14T14:09:56Z","closed_at":"2019-04-17T18:43:40Z","author_association":"NONE","body":"I'm trying to save the console logs after each test but having some problems with Firefox.\nWith a test like:\n\n```\ndescribe(\"protractor/webdriver\", function() {\n    it(\"should be able to get browser logs\", function() {\n        browser.get(\"https://angularjs.org\");\n\n        browser.executeScript(\"console.log('Nothing to see here - move along.')\");\n        browser.executeScript(\"console.warn('This is your first warning!')\");\n        browser.executeScript(\"console.error('This is serious!!!')\");\n\n        browser.driver.manage().logs().get( \"browser\" ).then(function( logsEntries ) {\n            logsEntries.forEach( (item) => {\n                console.log( \"LOG LEVEL:\", item.level.name, item.message );\n            });\n        });\n    });\n});\n```\n\nFor Chrome I get the result nicely:\n\n```\n[chrome #1] LOG LEVEL: WARNING console-api 240:41 This is your first warning!\n[chrome #1] LOG LEVEL: SEVERE console-api 240:41 This is serious!!!\n```\n\nBut Firefox only returns \"Internal\" warnings (CSS parse):\n\n```\n[firefox #2] LOG LEVEL: WARNING Unknown property 'zoom'.  Declaration dropped.\n[firefox #2] LOG LEVEL: WARNING Unknown property 'user-select'.  Declaration dropped.\n[firefox #2] LOG LEVEL: WARNING Unknown property 'zoom'.  Declaration dropped.\n[firefox #2] LOG LEVEL: WARNING Error in parsing value for 'background-image'.  Declaration dropped.\n[firefox #2] LOG LEVEL: WARNING Error in parsing value for 'background-image'.  Declaration dropped.\n[firefox #2] LOG LEVEL: WARNING Expected media feature name but found '-webkit-min-device-pixel-ratio'.\n```\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["148419274","160122778","160123150","160130630","161976565","163002342","221279459","226167694","240014698","256974032","290696441","314582548","326385484","484213626"], "labels":["D-firefox","I-enhancement"]}