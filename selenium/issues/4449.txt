{"id":"4449", "title":"Selenium IE Webdriver ClassCastException If register EventListener", "body":"

## Meta -
OS:  Windows 7
<!-- Windows 10? OSX? -->
Selenium Version:  3.4.0
<!-- 2.52.0, IDE, etc -->
Browser:  Internet Explorer 
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  IE 11.0.9600.18762
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
When registering an EventListener with an EventFiringWebDriver(Webdriver)
Should be able to access webpage with driver.get(\"myurl\")

## Actual Behavior -
Get ClassCastException
java.lang.ClassCastException: com.google.common.collect.Maps$TransformedEntriesMap cannot be cast to java.util.List
	at org.openqa.selenium.remote.RemoteLogs.getRemoteEntries(RemoteLogs.java:83)
	at org.openqa.selenium.remote.RemoteLogs.get(RemoteLogs.java:77)
	at com.notimportant.seleniumbugwithie.RunningEventListener.getBrowserLogs(RunningEventListener.java:91)
	at com.notimportant.seleniumbugwithie.RunningEventListener.beforeNavigateTo(RunningEventListener.java:96)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.openqa.selenium.support.events.EventFiringWebDriver$1.invoke(EventFiringWebDriver.java:81)
	at com.sun.proxy.$Proxy0.beforeNavigateTo(Unknown Source)
	at org.openqa.selenium.support.events.EventFiringWebDriver.get(EventFiringWebDriver.java:162)
	at com.notimportant.seleniumbugwithie.ShowBug.runit(ShowBug.java:72)
	at com.notimportant.seleniumbugwithie.ShowBug.main(ShowBug.java:82)

## Steps to reproduce -
Clone SSCCE from https://github.com/bernaps/SeleniumBugWithIE.git 
Start IE Web driver version 3.5.0.0 64 Bit (Win7) Download from here
 * http://selenium-release.storage.googleapis.com/index.html
 *
 IEDriverServer.exe /port=6666
run maven project 

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4449","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4449/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4449/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4449/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4449","id":249880345,"node_id":"MDU6SXNzdWUyNDk4ODAzNDU=","number":4449,"title":"Selenium IE Webdriver ClassCastException If register EventListener","user":{"login":"bernaps","id":3292435,"node_id":"MDQ6VXNlcjMyOTI0MzU=","avatar_url":"https://avatars0.githubusercontent.com/u/3292435?v=4","gravatar_id":"","url":"https://api.github.com/users/bernaps","html_url":"https://github.com/bernaps","followers_url":"https://api.github.com/users/bernaps/followers","following_url":"https://api.github.com/users/bernaps/following{/other_user}","gists_url":"https://api.github.com/users/bernaps/gists{/gist_id}","starred_url":"https://api.github.com/users/bernaps/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bernaps/subscriptions","organizations_url":"https://api.github.com/users/bernaps/orgs","repos_url":"https://api.github.com/users/bernaps/repos","events_url":"https://api.github.com/users/bernaps/events{/privacy}","received_events_url":"https://api.github.com/users/bernaps/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-08-13T15:27:21Z","updated_at":"2019-08-17T21:09:54Z","closed_at":"2017-08-17T20:49:40Z","author_association":"NONE","body":"\r\n\r\n## Meta -\r\nOS:  Windows 7\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.4.0\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Internet Explorer \r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  IE 11.0.9600.18762\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nWhen registering an EventListener with an EventFiringWebDriver(Webdriver)\r\nShould be able to access webpage with driver.get(\"myurl\")\r\n\r\n## Actual Behavior -\r\nGet ClassCastException\r\njava.lang.ClassCastException: com.google.common.collect.Maps$TransformedEntriesMap cannot be cast to java.util.List\r\n\tat org.openqa.selenium.remote.RemoteLogs.getRemoteEntries(RemoteLogs.java:83)\r\n\tat org.openqa.selenium.remote.RemoteLogs.get(RemoteLogs.java:77)\r\n\tat com.notimportant.seleniumbugwithie.RunningEventListener.getBrowserLogs(RunningEventListener.java:91)\r\n\tat com.notimportant.seleniumbugwithie.RunningEventListener.beforeNavigateTo(RunningEventListener.java:96)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.openqa.selenium.support.events.EventFiringWebDriver$1.invoke(EventFiringWebDriver.java:81)\r\n\tat com.sun.proxy.$Proxy0.beforeNavigateTo(Unknown Source)\r\n\tat org.openqa.selenium.support.events.EventFiringWebDriver.get(EventFiringWebDriver.java:162)\r\n\tat com.notimportant.seleniumbugwithie.ShowBug.runit(ShowBug.java:72)\r\n\tat com.notimportant.seleniumbugwithie.ShowBug.main(ShowBug.java:82)\r\n\r\n## Steps to reproduce -\r\nClone SSCCE from https://github.com/bernaps/SeleniumBugWithIE.git \r\nStart IE Web driver version 3.5.0.0 64 Bit (Win7) Download from here\r\n * http://selenium-release.storage.googleapis.com/index.html\r\n *\r\n IEDriverServer.exe /port=6666\r\nrun maven project \r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["322048657","322168478","323190356"], "labels":["C-java"]}