{"id":"4405", "title":"C# bindings interpet error 33 as Insecure Certificate after closing and attempting a new session in Edge", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
3.4
Browser:  
Edge
Browser Version:  
16.16259

## Expected Behavior -
C# bindings return \"Session Not Created Exception\" when WebDriver throws an error 33.
## Actual Behavior -
C# bindings return \"Insecure Certificate\" when WebDriver throws an error  33.
## Steps to reproduce -
`try
{
    var service = EdgeDriverService.CreateDefaultService();
    service.UseVerboseLogging = true;
    var driver = new EdgeDriver(service);
    driver.Url = \"https://www.bing.com/\";
    Console.WriteLine(\"navigation complete\");
    System.Threading.Thread.Sleep(5000);
    driver.Close();
    Console.WriteLine(\"Start new session\");
    driver = new EdgeDriver(service);
    driver.Url = \"https://www.bing.com/\";
    driver.Close();
    if (driver != null) driver.Quit();
}
catch (Exception e)
{
    Console.WriteLine(\"Test failed with {0} error. callstack: {1}\", e.Message, e.StackTrace);
}`", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4405","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4405/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4405/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4405/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4405","id":247861673,"node_id":"MDU6SXNzdWUyNDc4NjE2NzM=","number":4405,"title":"C# bindings interpet error 33 as Insecure Certificate after closing and attempting a new session in Edge","user":{"login":"InstyleVII","id":1965742,"node_id":"MDQ6VXNlcjE5NjU3NDI=","avatar_url":"https://avatars3.githubusercontent.com/u/1965742?v=4","gravatar_id":"","url":"https://api.github.com/users/InstyleVII","html_url":"https://github.com/InstyleVII","followers_url":"https://api.github.com/users/InstyleVII/followers","following_url":"https://api.github.com/users/InstyleVII/following{/other_user}","gists_url":"https://api.github.com/users/InstyleVII/gists{/gist_id}","starred_url":"https://api.github.com/users/InstyleVII/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/InstyleVII/subscriptions","organizations_url":"https://api.github.com/users/InstyleVII/orgs","repos_url":"https://api.github.com/users/InstyleVII/repos","events_url":"https://api.github.com/users/InstyleVII/events{/privacy}","received_events_url":"https://api.github.com/users/InstyleVII/received_events","type":"User","site_admin":false},"labels":[{"id":182503933,"node_id":"MDU6TGFiZWwxODI1MDM5MzM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-dotnet","name":"C-dotnet","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-08-03T23:17:01Z","updated_at":"2019-08-16T08:09:39Z","closed_at":"2018-03-29T17:37:22Z","author_association":"CONTRIBUTOR","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n3.4\r\nBrowser:  \r\nEdge\r\nBrowser Version:  \r\n16.16259\r\n\r\n## Expected Behavior -\r\nC# bindings return \"Session Not Created Exception\" when WebDriver throws an error 33.\r\n## Actual Behavior -\r\nC# bindings return \"Insecure Certificate\" when WebDriver throws an error  33.\r\n## Steps to reproduce -\r\n`try\r\n{\r\n    var service = EdgeDriverService.CreateDefaultService();\r\n    service.UseVerboseLogging = true;\r\n    var driver = new EdgeDriver(service);\r\n    driver.Url = \"https://www.bing.com/\";\r\n    Console.WriteLine(\"navigation complete\");\r\n    System.Threading.Thread.Sleep(5000);\r\n    driver.Close();\r\n    Console.WriteLine(\"Start new session\");\r\n    driver = new EdgeDriver(service);\r\n    driver.Url = \"https://www.bing.com/\";\r\n    driver.Close();\r\n    if (driver != null) driver.Quit();\r\n}\r\ncatch (Exception e)\r\n{\r\n    Console.WriteLine(\"Test failed with {0} error. callstack: {1}\", e.Message, e.StackTrace);\r\n}`","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["377313974"], "labels":["C-dotnet"]}