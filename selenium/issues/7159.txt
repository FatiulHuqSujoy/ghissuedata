{"id":"7159", "title":"Selenium does not scroll element high enough if there is a navigation bar with position: fixed", "body":"## 🐛 Bug Report

When page contains a navigation bar, which type `position: fixed` and page contains lots of elements, so that Selenium scrolls the element in view, before the element is clicked. Then Selenium does not scroll the element high enough in the browser window so that it could successfully click the element. 

Instead Selenium will scroll the element to the edge of the browser window, but because there is navigation bar, which type position: fixed, the element is behind the navigation bar. Therefore Selenium will raise an error:
```
Traceback (most recent call last):
  File \"selenium_test.py \", line 7, in <module>
    element.click()
  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webelement.py\", line 80, in click
    self._execute(Command.CLICK_ELEMENT)
  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webelement.py\", line 633, in _execute
    return self._parent.execute(command, params)
  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.ElementClickInterceptedException: Message: element click intercepted: Element <p id=\"p29\">...</p> is not clickable at point (628, 713). Other element would receive the click: <ul>...</ul>
  (Session info: chrome=74.0.3729.108)
  (Driver info: chromedriver=74.0.3729.6 (255758eccf3d244491b8a1317aa76e1ce10d57e9-refs/branch-heads/3729@{#29}),platform=Linux 4.15.0-48-generic x86_64)
```

## To Reproduce
https://gist.github.com/aaltat/bff3d0511e306abb2abb9c8373343eed#file-page-html
https://gist.github.com/aaltat/bff3d0511e306abb2abb9c8373343eed#file-selenium_test-py

## Expected behavior
I did expect element to be scrolled in view and the click to be successful.

## Test script or set of commands reproducing this issue
See the gist in above. 

## Environment

OS: Ubuntu 18.04 LTS
Browser: Chrome and Firefoc
Browser version: Chrome:  74.0.3729.108, Firefox: 66.0.3
Browser Driver version: ChromeDriver 74.0.3729.6, geckodriver 0.24.0 ( 2019-01-28)
Language Bindings version: Python 3.7.1
Selenium Grid version (if applicable): Not used.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7159","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7159/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7159/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7159/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7159","id":438989873,"node_id":"MDU6SXNzdWU0Mzg5ODk4NzM=","number":7159,"title":"Selenium does not scroll element high enough if there is a navigation bar with position: fixed","user":{"login":"aaltat","id":2665023,"node_id":"MDQ6VXNlcjI2NjUwMjM=","avatar_url":"https://avatars0.githubusercontent.com/u/2665023?v=4","gravatar_id":"","url":"https://api.github.com/users/aaltat","html_url":"https://github.com/aaltat","followers_url":"https://api.github.com/users/aaltat/followers","following_url":"https://api.github.com/users/aaltat/following{/other_user}","gists_url":"https://api.github.com/users/aaltat/gists{/gist_id}","starred_url":"https://api.github.com/users/aaltat/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/aaltat/subscriptions","organizations_url":"https://api.github.com/users/aaltat/orgs","repos_url":"https://api.github.com/users/aaltat/repos","events_url":"https://api.github.com/users/aaltat/events{/privacy}","received_events_url":"https://api.github.com/users/aaltat/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-04-30T21:42:13Z","updated_at":"2019-08-14T12:09:44Z","closed_at":"2019-05-01T02:27:20Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nWhen page contains a navigation bar, which type `position: fixed` and page contains lots of elements, so that Selenium scrolls the element in view, before the element is clicked. Then Selenium does not scroll the element high enough in the browser window so that it could successfully click the element. \r\n\r\nInstead Selenium will scroll the element to the edge of the browser window, but because there is navigation bar, which type position: fixed, the element is behind the navigation bar. Therefore Selenium will raise an error:\r\n```\r\nTraceback (most recent call last):\r\n  File \"selenium_test.py \", line 7, in <module>\r\n    element.click()\r\n  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webelement.py\", line 80, in click\r\n    self._execute(Command.CLICK_ELEMENT)\r\n  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webelement.py\", line 633, in _execute\r\n    return self._parent.execute(command, params)\r\n  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.ElementClickInterceptedException: Message: element click intercepted: Element <p id=\"p29\">...</p> is not clickable at point (628, 713). Other element would receive the click: <ul>...</ul>\r\n  (Session info: chrome=74.0.3729.108)\r\n  (Driver info: chromedriver=74.0.3729.6 (255758eccf3d244491b8a1317aa76e1ce10d57e9-refs/branch-heads/3729@{#29}),platform=Linux 4.15.0-48-generic x86_64)\r\n```\r\n\r\n## To Reproduce\r\nhttps://gist.github.com/aaltat/bff3d0511e306abb2abb9c8373343eed#file-page-html\r\nhttps://gist.github.com/aaltat/bff3d0511e306abb2abb9c8373343eed#file-selenium_test-py\r\n\r\n## Expected behavior\r\nI did expect element to be scrolled in view and the click to be successful.\r\n\r\n## Test script or set of commands reproducing this issue\r\nSee the gist in above. \r\n\r\n## Environment\r\n\r\nOS: Ubuntu 18.04 LTS\r\nBrowser: Chrome and Firefoc\r\nBrowser version: Chrome:  74.0.3729.108, Firefox: 66.0.3\r\nBrowser Driver version: ChromeDriver 74.0.3729.6, geckodriver 0.24.0 ( 2019-01-28)\r\nLanguage Bindings version: Python 3.7.1\r\nSelenium Grid version (if applicable): Not used.\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["488177832","488723934"], "labels":[]}