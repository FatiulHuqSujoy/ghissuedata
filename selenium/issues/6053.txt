{"id":"6053", "title":"Weird behaviour with Selenium's Click() method", "body":"OS:  
Windows 7 Ultimate SP1
Selenium Version:  
3.12.1
Browser:  
Chromedriver/GoogleChrome

Browser Version:  
2.40.565498 of Chromedriver / Latest one, downloaded it yesterday (20th June 2018)

## Expected Behavior -
Should click on the specified element.

## Actual Behavior -
Sometimes it opens a whole new web page that isn't relevant anywhere on the current page.
Clicks on a different element on the same page. (Both elements are static and there shouldn't be a conflict)

## Steps to reproduce -
No need. Fixed.


## Also 
I am aware that there was a similar issue on this GitHub, but the issue didn't help my case.
Link to that issue: https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/5404", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6053","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6053/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6053/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6053/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6053","id":334429421,"node_id":"MDU6SXNzdWUzMzQ0Mjk0MjE=","number":6053,"title":"Weird behaviour with Selenium's Click() method","user":{"login":"rokoburilo","id":40317810,"node_id":"MDQ6VXNlcjQwMzE3ODEw","avatar_url":"https://avatars0.githubusercontent.com/u/40317810?v=4","gravatar_id":"","url":"https://api.github.com/users/rokoburilo","html_url":"https://github.com/rokoburilo","followers_url":"https://api.github.com/users/rokoburilo/followers","following_url":"https://api.github.com/users/rokoburilo/following{/other_user}","gists_url":"https://api.github.com/users/rokoburilo/gists{/gist_id}","starred_url":"https://api.github.com/users/rokoburilo/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rokoburilo/subscriptions","organizations_url":"https://api.github.com/users/rokoburilo/orgs","repos_url":"https://api.github.com/users/rokoburilo/repos","events_url":"https://api.github.com/users/rokoburilo/events{/privacy}","received_events_url":"https://api.github.com/users/rokoburilo/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-06-21T10:19:29Z","updated_at":"2019-08-15T21:10:03Z","closed_at":"2018-06-21T13:25:39Z","author_association":"NONE","body":"OS:  \r\nWindows 7 Ultimate SP1\r\nSelenium Version:  \r\n3.12.1\r\nBrowser:  \r\nChromedriver/GoogleChrome\r\n\r\nBrowser Version:  \r\n2.40.565498 of Chromedriver / Latest one, downloaded it yesterday (20th June 2018)\r\n\r\n## Expected Behavior -\r\nShould click on the specified element.\r\n\r\n## Actual Behavior -\r\nSometimes it opens a whole new web page that isn't relevant anywhere on the current page.\r\nClicks on a different element on the same page. (Both elements are static and there shouldn't be a conflict)\r\n\r\n## Steps to reproduce -\r\nNo need. Fixed.\r\n\r\n\r\n## Also \r\nI am aware that there was a similar issue on this GitHub, but the issue didn't help my case.\r\nLink to that issue: https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/5404","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["399055606","399102578"], "labels":[]}