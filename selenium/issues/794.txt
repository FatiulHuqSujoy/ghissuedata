{"id":"794", "title":"Running webdriver from an npm script fails", "body":"`$ webdriver-manager start --standalone` works for me, but `$ npm run wd` doesn't with this setup in my package.json:

```
\"scripts\": {
    \"test-unit\": \"karma start karma.conf.js\",

    \"update-wd\": \"webdriver-manager update\",
    \"wd\": \"webdriver-manager start --standalone\",
    \"test-e2e\": \"protractor protractor.conf.js\"
  }
```

I get this error message:

```
$ npm run wd

> <<project name>>@0.1.0 wd <<project path>>
> webdriver-manager start --standalone

Selenium Standalone is not present. Install with webdriver-manager update --standalone

npm ERR! Darwin 14.4.0
npm ERR! argv \"node\" \"/usr/local/bin/npm\" \"run\" \"wd\"
npm ERR! node v0.12.7
npm ERR! npm  v2.11.3
npm ERR! code ELIFECYCLE
npm ERR! <<project name>>@0.1.0 wd: `webdriver-manager start --standalone`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the <<project name>>@0.1.0 wd script 'webdriver-manager start --standalone'.
npm ERR! This is most likely a problem with the <<project name>> package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     webdriver-manager start --standalone
npm ERR! You can get their info via:
npm ERR!     npm owner ls <<project name>>
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     <<project path>>
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/794","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/794/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/794/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/794/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/794","id":95251250,"node_id":"MDU6SXNzdWU5NTI1MTI1MA==","number":794,"title":"Running webdriver from an npm script fails","user":{"login":"ilanbiala","id":3695164,"node_id":"MDQ6VXNlcjM2OTUxNjQ=","avatar_url":"https://avatars3.githubusercontent.com/u/3695164?v=4","gravatar_id":"","url":"https://api.github.com/users/ilanbiala","html_url":"https://github.com/ilanbiala","followers_url":"https://api.github.com/users/ilanbiala/followers","following_url":"https://api.github.com/users/ilanbiala/following{/other_user}","gists_url":"https://api.github.com/users/ilanbiala/gists{/gist_id}","starred_url":"https://api.github.com/users/ilanbiala/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ilanbiala/subscriptions","organizations_url":"https://api.github.com/users/ilanbiala/orgs","repos_url":"https://api.github.com/users/ilanbiala/repos","events_url":"https://api.github.com/users/ilanbiala/events{/privacy}","received_events_url":"https://api.github.com/users/ilanbiala/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2015-07-15T17:48:13Z","updated_at":"2019-08-20T20:10:00Z","closed_at":"2015-07-15T18:08:56Z","author_association":"NONE","body":"`$ webdriver-manager start --standalone` works for me, but `$ npm run wd` doesn't with this setup in my package.json:\n\n```\n\"scripts\": {\n    \"test-unit\": \"karma start karma.conf.js\",\n\n    \"update-wd\": \"webdriver-manager update\",\n    \"wd\": \"webdriver-manager start --standalone\",\n    \"test-e2e\": \"protractor protractor.conf.js\"\n  }\n```\n\nI get this error message:\n\n```\n$ npm run wd\n\n> <<project name>>@0.1.0 wd <<project path>>\n> webdriver-manager start --standalone\n\nSelenium Standalone is not present. Install with webdriver-manager update --standalone\n\nnpm ERR! Darwin 14.4.0\nnpm ERR! argv \"node\" \"/usr/local/bin/npm\" \"run\" \"wd\"\nnpm ERR! node v0.12.7\nnpm ERR! npm  v2.11.3\nnpm ERR! code ELIFECYCLE\nnpm ERR! <<project name>>@0.1.0 wd: `webdriver-manager start --standalone`\nnpm ERR! Exit status 1\nnpm ERR!\nnpm ERR! Failed at the <<project name>>@0.1.0 wd script 'webdriver-manager start --standalone'.\nnpm ERR! This is most likely a problem with the <<project name>> package,\nnpm ERR! not with npm itself.\nnpm ERR! Tell the author that this fails on your system:\nnpm ERR!     webdriver-manager start --standalone\nnpm ERR! You can get their info via:\nnpm ERR!     npm owner ls <<project name>>\nnpm ERR! There is likely additional logging output above.\n\nnpm ERR! Please include the following file with any support request:\nnpm ERR!     <<project path>>\n```\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["121697512","121698173","121703702","121704956","128539055","128598542","164077691"], "labels":[]}