{"id":"4388", "title":"sockjs client unreleasable dependency problem", "body":"## Meta -
OS:  Windows 10
Selenium Version:  latest - cannot npm install webdriver
Expected Behavior -
Selenium-webdriver npm library is installed

## Actual Behavior -
installation fails and I get message 
PS D:\\projects\\NGANGA\\nganga_projects\\accessbility> npm install selenium-webdriver
npm ERR! code ETARGET
npm ERR! notarget No matching version found for sockjs-client@0.0.0-unreleasable
npm ERR! notarget In most cases you or one of your dependencies are requesting
npm ERR! notarget a package version that doesn't exist.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\\Users\\Bruger\\AppData\\Roaming\\npm-cache\\_logs\\2017-07-30T18_58_59_647Z-debug.log

## Steps to reproduce -
run npm install selenium-webdriver on a system that hasn't had it installed before.


- the complete error log is 

0 info it worked if it ends with ok
1 verbose cli [ 'C:\\\\Program Files\\\\nodejs\\\\node.exe',
1 verbose cli   'C:\\\\Program Files\\\\nodejs\\\\node_modules\\\\npm\\\\bin\\\\npm-cli.js',
1 verbose cli   'install',
1 verbose cli   'selenium-webdriver' ]
2 info using npm@5.0.4
3 info using node@v6.9.1
4 verbose npm-session 83764cee881fc252
5 silly install loadCurrentTree
6 silly install readLocalPackageData
7 http fetch GET 200 https://registry.npmjs.org/selenium-webdriver 23ms (from cache)
8 silly pacote tag manifest for selenium-webdriver@latest fetched in 92ms
9 silly install loadIdealTree
10 silly install cloneCurrentTreeToIdealTree
11 silly install loadShrinkwrap
12 http fetch GET 200 https://registry.npmjs.org/sockjs-client 15ms (from cache)
13 silly registry:manifest no matching version for sockjs-client@0.0.0-unreleasable in the cache. Forcing revalidation
14 http fetch GET 304 https://registry.npmjs.org/sockjs-client 526ms (from cache)
15 silly fetchPackageMetaData error for sockjs-client@0.0.0-unreleasable No matching version found for sockjs-client@0.0.0-unreleasable
16 verbose type version
17 verbose stack sockjs-client: No matching version found for sockjs-client@0.0.0-unreleasable
17 verbose stack     at pickManifest (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\pacote\\node_modules\\npm-pick-manifest\\index.js:61:11)
17 verbose stack     at fetchPackument.then.packument (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\pacote\\lib\\fetchers\\registry\\manifest.js:51:18)
17 verbose stack     at tryCatcher (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\util.js:16:23)
17 verbose stack     at Promise._settlePromiseFromHandler (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:512:31)
17 verbose stack     at Promise._settlePromise (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:569:18)
17 verbose stack     at Promise._settlePromise0 (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:614:10)
17 verbose stack     at Promise._settlePromises (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:693:18)
17 verbose stack     at Async._drainQueue (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:133:16)
17 verbose stack     at Async._drainQueues (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:143:10)
17 verbose stack     at Immediate.Async.drainQueues (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:17:14)
17 verbose stack     at runCallback (timers.js:637:20)
17 verbose stack     at tryOnImmediate (timers.js:610:5)
17 verbose stack     at processImmediate [as _immediateCallback] (timers.js:582:5)
18 verbose cwd D:\\projects\\NGANGA\\nganga_projects\\accessbility
19 verbose Windows_NT 10.0.14393
20 verbose argv \"C:\\\\Program Files\\\\nodejs\\\\node.exe\" \"C:\\\\Program Files\\\\nodejs\\\\node_modules\\\\npm\\\\bin\\\\npm-cli.js\" \"install\" \"selenium-webdriver\"
21 verbose node v6.9.1
22 verbose npm  v5.0.4
23 error code ETARGET
24 error notarget No matching version found for sockjs-client@0.0.0-unreleasable
25 error notarget In most cases you or one of your dependencies are requesting
25 error notarget a package version that doesn't exist.
26 verbose exit [ 1, true ]
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4388","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4388/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4388/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4388/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4388","id":246602426,"node_id":"MDU6SXNzdWUyNDY2MDI0MjY=","number":4388,"title":"sockjs client unreleasable dependency problem","user":{"login":"bryanrasmussen","id":312384,"node_id":"MDQ6VXNlcjMxMjM4NA==","avatar_url":"https://avatars2.githubusercontent.com/u/312384?v=4","gravatar_id":"","url":"https://api.github.com/users/bryanrasmussen","html_url":"https://github.com/bryanrasmussen","followers_url":"https://api.github.com/users/bryanrasmussen/followers","following_url":"https://api.github.com/users/bryanrasmussen/following{/other_user}","gists_url":"https://api.github.com/users/bryanrasmussen/gists{/gist_id}","starred_url":"https://api.github.com/users/bryanrasmussen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bryanrasmussen/subscriptions","organizations_url":"https://api.github.com/users/bryanrasmussen/orgs","repos_url":"https://api.github.com/users/bryanrasmussen/repos","events_url":"https://api.github.com/users/bryanrasmussen/events{/privacy}","received_events_url":"https://api.github.com/users/bryanrasmussen/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-07-30T19:04:37Z","updated_at":"2019-08-16T10:09:47Z","closed_at":"2017-07-31T04:04:46Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\nSelenium Version:  latest - cannot npm install webdriver\r\nExpected Behavior -\r\nSelenium-webdriver npm library is installed\r\n\r\n## Actual Behavior -\r\ninstallation fails and I get message \r\nPS D:\\projects\\NGANGA\\nganga_projects\\accessbility> npm install selenium-webdriver\r\nnpm ERR! code ETARGET\r\nnpm ERR! notarget No matching version found for sockjs-client@0.0.0-unreleasable\r\nnpm ERR! notarget In most cases you or one of your dependencies are requesting\r\nnpm ERR! notarget a package version that doesn't exist.\r\n\r\nnpm ERR! A complete log of this run can be found in:\r\nnpm ERR!     C:\\Users\\Bruger\\AppData\\Roaming\\npm-cache\\_logs\\2017-07-30T18_58_59_647Z-debug.log\r\n\r\n## Steps to reproduce -\r\nrun npm install selenium-webdriver on a system that hasn't had it installed before.\r\n\r\n\r\n- the complete error log is \r\n\r\n0 info it worked if it ends with ok\r\n1 verbose cli [ 'C:\\\\Program Files\\\\nodejs\\\\node.exe',\r\n1 verbose cli   'C:\\\\Program Files\\\\nodejs\\\\node_modules\\\\npm\\\\bin\\\\npm-cli.js',\r\n1 verbose cli   'install',\r\n1 verbose cli   'selenium-webdriver' ]\r\n2 info using npm@5.0.4\r\n3 info using node@v6.9.1\r\n4 verbose npm-session 83764cee881fc252\r\n5 silly install loadCurrentTree\r\n6 silly install readLocalPackageData\r\n7 http fetch GET 200 https://registry.npmjs.org/selenium-webdriver 23ms (from cache)\r\n8 silly pacote tag manifest for selenium-webdriver@latest fetched in 92ms\r\n9 silly install loadIdealTree\r\n10 silly install cloneCurrentTreeToIdealTree\r\n11 silly install loadShrinkwrap\r\n12 http fetch GET 200 https://registry.npmjs.org/sockjs-client 15ms (from cache)\r\n13 silly registry:manifest no matching version for sockjs-client@0.0.0-unreleasable in the cache. Forcing revalidation\r\n14 http fetch GET 304 https://registry.npmjs.org/sockjs-client 526ms (from cache)\r\n15 silly fetchPackageMetaData error for sockjs-client@0.0.0-unreleasable No matching version found for sockjs-client@0.0.0-unreleasable\r\n16 verbose type version\r\n17 verbose stack sockjs-client: No matching version found for sockjs-client@0.0.0-unreleasable\r\n17 verbose stack     at pickManifest (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\pacote\\node_modules\\npm-pick-manifest\\index.js:61:11)\r\n17 verbose stack     at fetchPackument.then.packument (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\pacote\\lib\\fetchers\\registry\\manifest.js:51:18)\r\n17 verbose stack     at tryCatcher (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\util.js:16:23)\r\n17 verbose stack     at Promise._settlePromiseFromHandler (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:512:31)\r\n17 verbose stack     at Promise._settlePromise (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:569:18)\r\n17 verbose stack     at Promise._settlePromise0 (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:614:10)\r\n17 verbose stack     at Promise._settlePromises (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\promise.js:693:18)\r\n17 verbose stack     at Async._drainQueue (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:133:16)\r\n17 verbose stack     at Async._drainQueues (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:143:10)\r\n17 verbose stack     at Immediate.Async.drainQueues (C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\bluebird\\js\\release\\async.js:17:14)\r\n17 verbose stack     at runCallback (timers.js:637:20)\r\n17 verbose stack     at tryOnImmediate (timers.js:610:5)\r\n17 verbose stack     at processImmediate [as _immediateCallback] (timers.js:582:5)\r\n18 verbose cwd D:\\projects\\NGANGA\\nganga_projects\\accessbility\r\n19 verbose Windows_NT 10.0.14393\r\n20 verbose argv \"C:\\\\Program Files\\\\nodejs\\\\node.exe\" \"C:\\\\Program Files\\\\nodejs\\\\node_modules\\\\npm\\\\bin\\\\npm-cli.js\" \"install\" \"selenium-webdriver\"\r\n21 verbose node v6.9.1\r\n22 verbose npm  v5.0.4\r\n23 error code ETARGET\r\n24 error notarget No matching version found for sockjs-client@0.0.0-unreleasable\r\n25 error notarget In most cases you or one of your dependencies are requesting\r\n25 error notarget a package version that doesn't exist.\r\n26 verbose exit [ 1, true ]\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["318962205","372956342"], "labels":["C-nodejs"]}