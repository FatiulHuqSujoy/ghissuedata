{"id":"4262", "title":"Chrome does not close", "body":"OS:

Mac OSX 10.12.3
Windows 7
node:

lts 6.10.2
npm:

4.2.0
chrome window does not close after test run.

\"dependencies\": {
    \"chromedriver\": \"^2.29.0\",
    \"jasmine\": \"^2.5.3\",
    \"jasmine-promises\": \"^0.4.1\",
    \"jasmine-reporters\": \"^2.2.0\",
    \"jasmine-spec-reporter\": \"^3.2.0\",
    \"selenium-webdriver\": \"^3.3.0\"
  }
import seleniumWebDriver from \"selenium-webdriver\";

describe(\"e2e tests\", () => {
    const { By, until } = seleniumWebDriver;
    let driver;

    beforeEach(() => {
        driver = new seleniumWebDriver.Builder()
            .forBrowser(\"chrome\")
            .build();

        return driver.navigate().to(\"https://website.mine/\")
            .then(() => driver.wait(until.elementLocated(By.id(\"login-username\"))))
            .then(() => driver.findElement(By.id(\"login-username\")).sendKeys(\"admin\"))
            .then(() => driver.findElement(By.id(\"login-password\")).sendKeys(\"admin\"))
            .then(() => driver.findElement(By.tagName(\"button\")).click())
            .then(() => driver.wait(until.elementLocated(By.css(\".app-container-inner\"))));
    });

    it(\"should have favorites card\", () => driver.findElement(By.css(\".start-view-favourites-list\")).then(element => expect(element).toBeDefined()));

    afterEach(() => {
        driver.close();
        driver.quit();
    });
});", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4262","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4262/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4262/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4262/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4262","id":240569236,"node_id":"MDU6SXNzdWUyNDA1NjkyMzY=","number":4262,"title":"Chrome does not close","user":{"login":"jonashartwig","id":8947747,"node_id":"MDQ6VXNlcjg5NDc3NDc=","avatar_url":"https://avatars1.githubusercontent.com/u/8947747?v=4","gravatar_id":"","url":"https://api.github.com/users/jonashartwig","html_url":"https://github.com/jonashartwig","followers_url":"https://api.github.com/users/jonashartwig/followers","following_url":"https://api.github.com/users/jonashartwig/following{/other_user}","gists_url":"https://api.github.com/users/jonashartwig/gists{/gist_id}","starred_url":"https://api.github.com/users/jonashartwig/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jonashartwig/subscriptions","organizations_url":"https://api.github.com/users/jonashartwig/orgs","repos_url":"https://api.github.com/users/jonashartwig/repos","events_url":"https://api.github.com/users/jonashartwig/events{/privacy}","received_events_url":"https://api.github.com/users/jonashartwig/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-07-05T08:02:15Z","updated_at":"2019-08-18T04:09:57Z","closed_at":"2017-07-05T18:16:02Z","author_association":"NONE","body":"OS:\r\n\r\nMac OSX 10.12.3\r\nWindows 7\r\nnode:\r\n\r\nlts 6.10.2\r\nnpm:\r\n\r\n4.2.0\r\nchrome window does not close after test run.\r\n\r\n\"dependencies\": {\r\n    \"chromedriver\": \"^2.29.0\",\r\n    \"jasmine\": \"^2.5.3\",\r\n    \"jasmine-promises\": \"^0.4.1\",\r\n    \"jasmine-reporters\": \"^2.2.0\",\r\n    \"jasmine-spec-reporter\": \"^3.2.0\",\r\n    \"selenium-webdriver\": \"^3.3.0\"\r\n  }\r\nimport seleniumWebDriver from \"selenium-webdriver\";\r\n\r\ndescribe(\"e2e tests\", () => {\r\n    const { By, until } = seleniumWebDriver;\r\n    let driver;\r\n\r\n    beforeEach(() => {\r\n        driver = new seleniumWebDriver.Builder()\r\n            .forBrowser(\"chrome\")\r\n            .build();\r\n\r\n        return driver.navigate().to(\"https://website.mine/\")\r\n            .then(() => driver.wait(until.elementLocated(By.id(\"login-username\"))))\r\n            .then(() => driver.findElement(By.id(\"login-username\")).sendKeys(\"admin\"))\r\n            .then(() => driver.findElement(By.id(\"login-password\")).sendKeys(\"admin\"))\r\n            .then(() => driver.findElement(By.tagName(\"button\")).click())\r\n            .then(() => driver.wait(until.elementLocated(By.css(\".app-container-inner\"))));\r\n    });\r\n\r\n    it(\"should have favorites card\", () => driver.findElement(By.css(\".start-view-favourites-list\")).then(element => expect(element).toBeDefined()));\r\n\r\n    afterEach(() => {\r\n        driver.close();\r\n        driver.quit();\r\n    });\r\n});","closed_by":{"login":"jleyba","id":254985,"node_id":"MDQ6VXNlcjI1NDk4NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/254985?v=4","gravatar_id":"","url":"https://api.github.com/users/jleyba","html_url":"https://github.com/jleyba","followers_url":"https://api.github.com/users/jleyba/followers","following_url":"https://api.github.com/users/jleyba/following{/other_user}","gists_url":"https://api.github.com/users/jleyba/gists{/gist_id}","starred_url":"https://api.github.com/users/jleyba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jleyba/subscriptions","organizations_url":"https://api.github.com/users/jleyba/orgs","repos_url":"https://api.github.com/users/jleyba/repos","events_url":"https://api.github.com/users/jleyba/events{/privacy}","received_events_url":"https://api.github.com/users/jleyba/received_events","type":"User","site_admin":false}}", "commentIds":["313183954","313337948"], "labels":["C-nodejs"]}