{"id":"6165", "title":"Issue with running webdriverio tests in parallel with selenium grid", "body":"## Meta -
OS: docker for mac
Selenium Version: 3.11.0
Browser:  Chrome

## Expected Behavior -
Should be able to run parallel tests with selenium grid

## Actual Behavior -
Tests start to fail as soon as number of chrome nodes become more than 2

## Steps to reproduce -
I am running webriverio ui tests with mocha framework in selenium grid. The selenium grid set up is done using docker-compose. I am only using chrome nodes in selenium grid. 

My tests start to fail when the number of chrome nodes becomes more than 2. I am specifying same value for ‘maxInstances’ property in test runner and ‘NODE_MAX_INSTANCES’ in docker-compose.yml. Using vnc viewer, I see that it launches same number of parallel browser as mentioned in ‘maxInstances’ property (3, in this case). But tests start to fail when parallel browser count becomes more than 2. I have a total of 12 test spec files grouped in as many test suites.

I don’t understand why this is happening only with increase in number of nodes. The final error shown in logs is session terminated due to BROWSER_TIMEOUT.  Please find attached my test container logs. I have set logging level to ‘verbose’. I have also attached logs for successful test run when only 1 chrome node is part of grid.


[parallelTestsSuccess.pdf](https://github.com/SeleniumHQ/selenium/files/2197666/parallelTestsSuccess.pdf)
[parallelTestFailures.pdf](https://github.com/SeleniumHQ/selenium/files/2197667/parallelTestFailures.pdf)

**docker-compose.yml:**
version: '3'
services:
  app:
    image: jobz_web_employer
    build:
      context: .
      dockerfile: Dockerfile.app
    environment:
    - NODE_ENV=production
    - CONFIG_ENV=staging
    - CONFIG_CUSTOM_API=https://blah.de
    restart: always

  seleniumhub:
    image: \"selenium/hub:3.11.0\"
    container_name: shub
    restart: always
    ports:
    - 4444:4444
    environment:
    - GRID_MAX_SESSION=\"20\"
    - GRID_BROWSER_TIMEOUT=100
    - GRID_TIMEOUT=90

  chromenode:
    image: \"selenium/node-chrome-debug:3.11.0\"
    container_name: chrome
    restart: always
    ports:
    - 5900
    depends_on:
    - seleniumhub
    environment:
    - HUB_PORT_4444_TCP_ADDR=hub
    - HUB_PORT_4444_TCP_PORT=4444
    - NODE_MAX_INSTANCES=3
    - NODE_MAX_SESSION=3
    links:
    - jobz_web_employer_proxy:blah.blah.de
    - seleniumhub:hub
    
  jobz_web_employer_proxy:
    image: jobz_web_employer_proxy
    build:
      context: .
      dockerfile: Dockerfile.proxy
    ports:
    - 80:80
    restart: always
    depends_on:
    - app
    links:
    - app

  tests:
    image: jobz_web_employer
    command: yarn wdio
    environment:
    - NODE_ENV=production
    - CONFIG_ENV=staging
    - PORT=80
    depends_on:
    - seleniumhub
    links:
    - seleniumhub:hub

My test runner (**wdio.ci.conf.js**) is below:
exports.config = merge(wdioConf.config, {
  host: 'shub',
  port: 4444,
  capabilities: [
    {
      browserName: 'chrome',
      maxInstances: 3,
      chromeOptions: {
        args: [
          '--disable-blink-features=BlockCredentialedSubresources',
          '--window-size=1024,768',
          '--no-sandbox'
        ]
      }
    }
  ],
  baseUrl: 'http://blah:80'
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6165","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6165/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6165/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6165/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6165","id":341509780,"node_id":"MDU6SXNzdWUzNDE1MDk3ODA=","number":6165,"title":"Issue with running webdriverio tests in parallel with selenium grid","user":{"login":"pk-saxena","id":35292540,"node_id":"MDQ6VXNlcjM1MjkyNTQw","avatar_url":"https://avatars0.githubusercontent.com/u/35292540?v=4","gravatar_id":"","url":"https://api.github.com/users/pk-saxena","html_url":"https://github.com/pk-saxena","followers_url":"https://api.github.com/users/pk-saxena/followers","following_url":"https://api.github.com/users/pk-saxena/following{/other_user}","gists_url":"https://api.github.com/users/pk-saxena/gists{/gist_id}","starred_url":"https://api.github.com/users/pk-saxena/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/pk-saxena/subscriptions","organizations_url":"https://api.github.com/users/pk-saxena/orgs","repos_url":"https://api.github.com/users/pk-saxena/repos","events_url":"https://api.github.com/users/pk-saxena/events{/privacy}","received_events_url":"https://api.github.com/users/pk-saxena/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-07-16T13:02:52Z","updated_at":"2019-08-15T18:10:01Z","closed_at":"2018-07-16T14:57:41Z","author_association":"NONE","body":"## Meta -\r\nOS: docker for mac\r\nSelenium Version: 3.11.0\r\nBrowser:  Chrome\r\n\r\n## Expected Behavior -\r\nShould be able to run parallel tests with selenium grid\r\n\r\n## Actual Behavior -\r\nTests start to fail as soon as number of chrome nodes become more than 2\r\n\r\n## Steps to reproduce -\r\nI am running webriverio ui tests with mocha framework in selenium grid. The selenium grid set up is done using docker-compose. I am only using chrome nodes in selenium grid. \r\n\r\nMy tests start to fail when the number of chrome nodes becomes more than 2. I am specifying same value for ‘maxInstances’ property in test runner and ‘NODE_MAX_INSTANCES’ in docker-compose.yml. Using vnc viewer, I see that it launches same number of parallel browser as mentioned in ‘maxInstances’ property (3, in this case). But tests start to fail when parallel browser count becomes more than 2. I have a total of 12 test spec files grouped in as many test suites.\r\n\r\nI don’t understand why this is happening only with increase in number of nodes. The final error shown in logs is session terminated due to BROWSER_TIMEOUT.  Please find attached my test container logs. I have set logging level to ‘verbose’. I have also attached logs for successful test run when only 1 chrome node is part of grid.\r\n\r\n\r\n[parallelTestsSuccess.pdf](https://github.com/SeleniumHQ/selenium/files/2197666/parallelTestsSuccess.pdf)\r\n[parallelTestFailures.pdf](https://github.com/SeleniumHQ/selenium/files/2197667/parallelTestFailures.pdf)\r\n\r\n**docker-compose.yml:**\r\nversion: '3'\r\nservices:\r\n  app:\r\n    image: jobz_web_employer\r\n    build:\r\n      context: .\r\n      dockerfile: Dockerfile.app\r\n    environment:\r\n    - NODE_ENV=production\r\n    - CONFIG_ENV=staging\r\n    - CONFIG_CUSTOM_API=https://blah.de\r\n    restart: always\r\n\r\n  seleniumhub:\r\n    image: \"selenium/hub:3.11.0\"\r\n    container_name: shub\r\n    restart: always\r\n    ports:\r\n    - 4444:4444\r\n    environment:\r\n    - GRID_MAX_SESSION=\"20\"\r\n    - GRID_BROWSER_TIMEOUT=100\r\n    - GRID_TIMEOUT=90\r\n\r\n  chromenode:\r\n    image: \"selenium/node-chrome-debug:3.11.0\"\r\n    container_name: chrome\r\n    restart: always\r\n    ports:\r\n    - 5900\r\n    depends_on:\r\n    - seleniumhub\r\n    environment:\r\n    - HUB_PORT_4444_TCP_ADDR=hub\r\n    - HUB_PORT_4444_TCP_PORT=4444\r\n    - NODE_MAX_INSTANCES=3\r\n    - NODE_MAX_SESSION=3\r\n    links:\r\n    - jobz_web_employer_proxy:blah.blah.de\r\n    - seleniumhub:hub\r\n    \r\n  jobz_web_employer_proxy:\r\n    image: jobz_web_employer_proxy\r\n    build:\r\n      context: .\r\n      dockerfile: Dockerfile.proxy\r\n    ports:\r\n    - 80:80\r\n    restart: always\r\n    depends_on:\r\n    - app\r\n    links:\r\n    - app\r\n\r\n  tests:\r\n    image: jobz_web_employer\r\n    command: yarn wdio\r\n    environment:\r\n    - NODE_ENV=production\r\n    - CONFIG_ENV=staging\r\n    - PORT=80\r\n    depends_on:\r\n    - seleniumhub\r\n    links:\r\n    - seleniumhub:hub\r\n\r\nMy test runner (**wdio.ci.conf.js**) is below:\r\nexports.config = merge(wdioConf.config, {\r\n  host: 'shub',\r\n  port: 4444,\r\n  capabilities: [\r\n    {\r\n      browserName: 'chrome',\r\n      maxInstances: 3,\r\n      chromeOptions: {\r\n        args: [\r\n          '--disable-blink-features=BlockCredentialedSubresources',\r\n          '--window-size=1024,768',\r\n          '--no-sandbox'\r\n        ]\r\n      }\r\n    }\r\n  ],\r\n  baseUrl: 'http://blah:80'\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["405276144"], "labels":[]}