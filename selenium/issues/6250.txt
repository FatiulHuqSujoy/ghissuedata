{"id":"6250", "title":"Python RemoteDriver regression in version 3.14.0", "body":"OS: macOS
Selenium Version: 3.14.0 (Python bindings)
Browser:  Apple Safari
Browser Version: 11.1.1 (13605.2.8)

## Expected Behavior -

The RemoteDriver should be capable of sending multiple commands to the same session.

## Actual Behavior -

The RemoteDriver fails to interact with a session following creation.

## Steps to reproduce -

Given a file `repro.py`:

```py
from selenium import webdriver
driver = webdriver.Remote('http://127.0.0.1:4444')
driver.title
```

And a running instance of `safaridriver` via

    $ safaridriver --port 4444

Version 3.13.0 executes the script with no errors. Version 3.14.0 reports the following error:

    Traceback (most recent call last):
      File \"repro.py\", line 3, in <module>
        driver.title
      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 341, in title
        resp = self.execute(Command.GET_TITLE)
      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 320, in execute
        self.error_handler.check_response(response)
      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/errorhandler.py\", line 208, in check_response
        raise exception_class(value)
    selenium.common.exceptions.WebDriverException: Message: 

(The reported error message is truncated.)", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6250","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6250/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6250/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6250/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6250","id":347578329,"node_id":"MDU6SXNzdWUzNDc1NzgzMjk=","number":6250,"title":"Python RemoteDriver regression in version 3.14.0","user":{"login":"jugglinmike","id":677252,"node_id":"MDQ6VXNlcjY3NzI1Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/677252?v=4","gravatar_id":"","url":"https://api.github.com/users/jugglinmike","html_url":"https://github.com/jugglinmike","followers_url":"https://api.github.com/users/jugglinmike/followers","following_url":"https://api.github.com/users/jugglinmike/following{/other_user}","gists_url":"https://api.github.com/users/jugglinmike/gists{/gist_id}","starred_url":"https://api.github.com/users/jugglinmike/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jugglinmike/subscriptions","organizations_url":"https://api.github.com/users/jugglinmike/orgs","repos_url":"https://api.github.com/users/jugglinmike/repos","events_url":"https://api.github.com/users/jugglinmike/events{/privacy}","received_events_url":"https://api.github.com/users/jugglinmike/received_events","type":"User","site_admin":false},"labels":[{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-08-04T02:27:45Z","updated_at":"2019-08-15T15:09:44Z","closed_at":"2018-08-16T16:08:33Z","author_association":"CONTRIBUTOR","body":"OS: macOS\r\nSelenium Version: 3.14.0 (Python bindings)\r\nBrowser:  Apple Safari\r\nBrowser Version: 11.1.1 (13605.2.8)\r\n\r\n## Expected Behavior -\r\n\r\nThe RemoteDriver should be capable of sending multiple commands to the same session.\r\n\r\n## Actual Behavior -\r\n\r\nThe RemoteDriver fails to interact with a session following creation.\r\n\r\n## Steps to reproduce -\r\n\r\nGiven a file `repro.py`:\r\n\r\n```py\r\nfrom selenium import webdriver\r\ndriver = webdriver.Remote('http://127.0.0.1:4444')\r\ndriver.title\r\n```\r\n\r\nAnd a running instance of `safaridriver` via\r\n\r\n    $ safaridriver --port 4444\r\n\r\nVersion 3.13.0 executes the script with no errors. Version 3.14.0 reports the following error:\r\n\r\n    Traceback (most recent call last):\r\n      File \"repro.py\", line 3, in <module>\r\n        driver.title\r\n      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 341, in title\r\n        resp = self.execute(Command.GET_TITLE)\r\n      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 320, in execute\r\n        self.error_handler.check_response(response)\r\n      File \"/Library/Python/2.7/site-packages/selenium/webdriver/remote/errorhandler.py\", line 208, in check_response\r\n        raise exception_class(value)\r\n    selenium.common.exceptions.WebDriverException: Message: \r\n\r\n(The reported error message is truncated.)","closed_by":{"login":"AutomatedTester","id":128518,"node_id":"MDQ6VXNlcjEyODUxOA==","avatar_url":"https://avatars1.githubusercontent.com/u/128518?v=4","gravatar_id":"","url":"https://api.github.com/users/AutomatedTester","html_url":"https://github.com/AutomatedTester","followers_url":"https://api.github.com/users/AutomatedTester/followers","following_url":"https://api.github.com/users/AutomatedTester/following{/other_user}","gists_url":"https://api.github.com/users/AutomatedTester/gists{/gist_id}","starred_url":"https://api.github.com/users/AutomatedTester/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/AutomatedTester/subscriptions","organizations_url":"https://api.github.com/users/AutomatedTester/orgs","repos_url":"https://api.github.com/users/AutomatedTester/repos","events_url":"https://api.github.com/users/AutomatedTester/events{/privacy}","received_events_url":"https://api.github.com/users/AutomatedTester/received_events","type":"User","site_admin":false}}", "commentIds":["410421284","410423935","410424711","410485038","413744117"], "labels":["C-py"]}