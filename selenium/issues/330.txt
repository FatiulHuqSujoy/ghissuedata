{"id":"330", "title":"Unable to get pop up after uploading a file using AutoIt", "body":"Scenario: After uploading an invalid file (apart from image file) using AutoIt, pop up is not displaying. Uploading a proper file (any image file) works fine

Browser: Firefox 34.0.5
Selenium Library Version: selenium-server-standalone-2.42.2
Autoit: autoit-v3

Steps: 
1. The test is performed on \"www.pspsrint.com\" portal using Guest account
2. When I try to click on any product e.g \"Business Card\" and try to upload invalid file I don't get an error popup

<div id=\"ui-id-5\" class=\"dialog-window message-dialog ui-dialog-content ui-widget-content\" style=\"width: auto; min-height: 118px; max-height: none; height: auto;\" data-bind=\"dialog: messageDialogOptions, dialogVisible: isMessageDialogVisible\">

My Code is as below

public static  WebDriver openfirefox;
@Test(priority = 1)
public void openLink() {  
                openfirefox.get(https://www.psprint.com/);//Opens the URL
        openfirefox.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);// Wait
}
@Test(priority = 2)
public void upload() throws InterruptedException {
        if(isElementPresent(By.id(\"fileupload_front1\"))) {
        upload(\"front\");
  }
}

private void upload(String side) {
        WebElement error = openfirefox.findElement(By.xpath(Constant.CloseError));
        if(side == \"front\"){
        openfirefox.findElement(By.id(\"fileupload_front1\")).click();// Clicks on Upload now button
        Runtime.getRuntime().exec(Constant.UploadPath);
        openfirefox.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
   }
        if(error.isDisplayed()) {
        openfirefox.findElement(By.xpath(CloseError)).click();//Close the error window
   }
}
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/330","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/330/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/330/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/330/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/330","id":61006379,"node_id":"MDU6SXNzdWU2MTAwNjM3OQ==","number":330,"title":"Unable to get pop up after uploading a file using AutoIt","user":{"login":"SunilGhargankar","id":9819702,"node_id":"MDQ6VXNlcjk4MTk3MDI=","avatar_url":"https://avatars0.githubusercontent.com/u/9819702?v=4","gravatar_id":"","url":"https://api.github.com/users/SunilGhargankar","html_url":"https://github.com/SunilGhargankar","followers_url":"https://api.github.com/users/SunilGhargankar/followers","following_url":"https://api.github.com/users/SunilGhargankar/following{/other_user}","gists_url":"https://api.github.com/users/SunilGhargankar/gists{/gist_id}","starred_url":"https://api.github.com/users/SunilGhargankar/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SunilGhargankar/subscriptions","organizations_url":"https://api.github.com/users/SunilGhargankar/orgs","repos_url":"https://api.github.com/users/SunilGhargankar/repos","events_url":"https://api.github.com/users/SunilGhargankar/events{/privacy}","received_events_url":"https://api.github.com/users/SunilGhargankar/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2015-03-13T07:45:35Z","updated_at":"2019-08-21T16:09:51Z","closed_at":"2015-03-13T10:34:38Z","author_association":"NONE","body":"Scenario: After uploading an invalid file (apart from image file) using AutoIt, pop up is not displaying. Uploading a proper file (any image file) works fine\n\nBrowser: Firefox 34.0.5\nSelenium Library Version: selenium-server-standalone-2.42.2\nAutoit: autoit-v3\n\nSteps: \n1. The test is performed on \"www.pspsrint.com\" portal using Guest account\n2. When I try to click on any product e.g \"Business Card\" and try to upload invalid file I don't get an error popup\n\n<div id=\"ui-id-5\" class=\"dialog-window message-dialog ui-dialog-content ui-widget-content\" style=\"width: auto; min-height: 118px; max-height: none; height: auto;\" data-bind=\"dialog: messageDialogOptions, dialogVisible: isMessageDialogVisible\">\n\nMy Code is as below\n\npublic static  WebDriver openfirefox;\n@Test(priority = 1)\npublic void openLink() {  \n                openfirefox.get(https://www.psprint.com/);//Opens the URL\n        openfirefox.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);// Wait\n}\n@Test(priority = 2)\npublic void upload() throws InterruptedException {\n        if(isElementPresent(By.id(\"fileupload_front1\"))) {\n        upload(\"front\");\n  }\n}\n\nprivate void upload(String side) {\n        WebElement error = openfirefox.findElement(By.xpath(Constant.CloseError));\n        if(side == \"front\"){\n        openfirefox.findElement(By.id(\"fileupload_front1\")).click();// Clicks on Upload now button\n        Runtime.getRuntime().exec(Constant.UploadPath);\n        openfirefox.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);\n   }\n        if(error.isDisplayed()) {\n        openfirefox.findElement(By.xpath(CloseError)).click();//Close the error window\n   }\n}\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["78857744","78889329","78909032","78940026"], "labels":[]}