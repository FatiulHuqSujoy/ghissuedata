{"id":"6901", "title":"selenium with chrome --headless mode receive a timeout error", "body":"## 💬 Questions and Help
## my chrome version ： 71.0.3578.98
## I update my chrome driver version ： 71.0.3578.137 (86ee722808adfe9e3c92e6e8ea746ade08423c7e
## the error report:
```
Traceback (most recent call last):
  File \"E:/timeoutTest.py\", line 46, in <module>
    test()
  File \"E:/timeoutTest.py\", line 31, in test
    driver.get(\"https://baidu.com\")
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 333, in get
    self.execute(Command.GET, {'url': url})
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.TimeoutException: Message: timeout
  (Session info: headless chrome=71.0.3578.98)
  (Driver info: chromedriver=2.41.578737 (49da6702b16031c40d63e5618de03a32ff6c197e),platform=Windows NT 6.1.7601 SP1 x86_64)
```

## my code : 
```

import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

def test():
    _chrome_options = Options()
    _chrome_options.add_argument(\"--headless\")

    driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\", chrome_options=_chrome_options)
    # driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\")
    # cls.driver = webdriver.Chrome(executable_path=cls._chrome_driver_path)

    driver.set_window_size(1200, 812)
    driver.set_page_load_timeout(60)
    driver.set_script_timeout(20)
    # 隐式等待
    driver.implicitly_wait(5)

    driver.get(\"https://baidu.com\")
    time.sleep(5)
    driver.find_element_by_id(\"kw\").send_keys(\"htmlunit\")
    time.sleep(5)
    driver.find_element_by_id(\"su\").click()
    time.sleep(5)
    now_time = time.strftime(\"%Y%m%d.%H.%M.%S\")
    driver.get_screenshot_as_file(now_time + '.png')
    print('screenshot:' + now_time + '.png')

    print(driver.current_url)
    print(driver.page_source)


if __name__ == '__main__':
    test()
```
the code can‘t run in the --headless mode.

-----------
# update：
After adding sleep time and update the chrome driver to 71.0.3578.137, and I change the request url to another，I received anther error like
this : 
```
Traceback (most recent call last):
  File \"E:/timeoutTest/Customized/testTemp.py\", line 35, in <module>
    test()
  File \"E:/timeoutTest/Customized/testTemp.py\", line 23, in test
    driver.find_element(By.XPATH, \"//*[@data-view-name=\\\"index\\\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[\"
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 978, in find_element
    'value': value})['value']
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
    self.error_handler.check_response(response)
  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//*[@data-view-name=\"index\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[1]/div[1]/p\"}
  (Session info: headless chrome=71.0.3578.98)
  (Driver info: chromedriver=71.0.3578.137 (86ee722808adfe9e3c92e6e8ea746ade08423c7e),platform=Windows NT 6.1.7601 SP1 x86_64)
```
## and my code like this ：
```
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

def test():
    _chrome_options = Options()
    _chrome_options.add_argument(\"--headless\")

    driver = webdriver.Chrome(r\"chromedriver\", chrome_options=_chrome_options)
    # driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\")
    # cls.driver = webdriver.Chrome(executable_path=cls._chrome_driver_path)

    driver.set_window_size(1200, 600)
    driver.set_page_load_timeout(100)
    driver.set_script_timeout(40)
    # 隐式等待
    driver.implicitly_wait(10)

    driver.get(\"https://m.ctrip.com/webapp/dingzhi/index\")
    time.sleep(10)
    driver.find_element(By.XPATH, \"//*[@data-view-name=\\\"index\\\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[\"
                                        \"1]/div[1]/p\").click()
    time.sleep(10)
    now_time = time.strftime(\"%Y%m%d.%H.%M.%S\")
    driver.get_screenshot_as_file(now_time + '.png')
    print('screenshot:' + now_time + '.png')
    time.sleep(5)
    print(driver.current_url)
    print(driver.page_source)
    driver.quit()

if __name__ == '__main__':
    test()
```

the main reqestion is that, when I doesn't use the --headless mode, I can receive a page  screenshot like this: 
![image](https://user-images.githubusercontent.com/15920812/52026703-c9315200-2543-11e9-8d20-0c3dc1c7b9fb.png)

but when I use --headless argument, the page seems can't load and throw NoSuchElementException, the screenshot like this:
![image](https://user-images.githubusercontent.com/15920812/52026797-17465580-2544-11e9-84d7-a2c5c28a6b77.png)
this is a blank image.

and the question is no error in another seem config computer.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6901","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6901/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6901/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6901/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6901","id":404740763,"node_id":"MDU6SXNzdWU0MDQ3NDA3NjM=","number":6901,"title":"selenium with chrome --headless mode receive a timeout error","user":{"login":"GrayGraySmall","id":15920812,"node_id":"MDQ6VXNlcjE1OTIwODEy","avatar_url":"https://avatars3.githubusercontent.com/u/15920812?v=4","gravatar_id":"","url":"https://api.github.com/users/GrayGraySmall","html_url":"https://github.com/GrayGraySmall","followers_url":"https://api.github.com/users/GrayGraySmall/followers","following_url":"https://api.github.com/users/GrayGraySmall/following{/other_user}","gists_url":"https://api.github.com/users/GrayGraySmall/gists{/gist_id}","starred_url":"https://api.github.com/users/GrayGraySmall/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/GrayGraySmall/subscriptions","organizations_url":"https://api.github.com/users/GrayGraySmall/orgs","repos_url":"https://api.github.com/users/GrayGraySmall/repos","events_url":"https://api.github.com/users/GrayGraySmall/events{/privacy}","received_events_url":"https://api.github.com/users/GrayGraySmall/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-01-30T12:07:52Z","updated_at":"2019-08-14T22:09:35Z","closed_at":"2019-02-13T21:14:13Z","author_association":"NONE","body":"## 💬 Questions and Help\r\n## my chrome version ： 71.0.3578.98\r\n## I update my chrome driver version ： 71.0.3578.137 (86ee722808adfe9e3c92e6e8ea746ade08423c7e\r\n## the error report:\r\n```\r\nTraceback (most recent call last):\r\n  File \"E:/timeoutTest.py\", line 46, in <module>\r\n    test()\r\n  File \"E:/timeoutTest.py\", line 31, in test\r\n    driver.get(\"https://baidu.com\")\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 333, in get\r\n    self.execute(Command.GET, {'url': url})\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.TimeoutException: Message: timeout\r\n  (Session info: headless chrome=71.0.3578.98)\r\n  (Driver info: chromedriver=2.41.578737 (49da6702b16031c40d63e5618de03a32ff6c197e),platform=Windows NT 6.1.7601 SP1 x86_64)\r\n```\r\n\r\n## my code : \r\n```\r\n\r\nimport time\r\n\r\nfrom selenium import webdriver\r\nfrom selenium.webdriver.chrome.options import Options\r\nfrom selenium.webdriver.common.by import By\r\n\r\ndef test():\r\n    _chrome_options = Options()\r\n    _chrome_options.add_argument(\"--headless\")\r\n\r\n    driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\", chrome_options=_chrome_options)\r\n    # driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\")\r\n    # cls.driver = webdriver.Chrome(executable_path=cls._chrome_driver_path)\r\n\r\n    driver.set_window_size(1200, 812)\r\n    driver.set_page_load_timeout(60)\r\n    driver.set_script_timeout(20)\r\n    # 隐式等待\r\n    driver.implicitly_wait(5)\r\n\r\n    driver.get(\"https://baidu.com\")\r\n    time.sleep(5)\r\n    driver.find_element_by_id(\"kw\").send_keys(\"htmlunit\")\r\n    time.sleep(5)\r\n    driver.find_element_by_id(\"su\").click()\r\n    time.sleep(5)\r\n    now_time = time.strftime(\"%Y%m%d.%H.%M.%S\")\r\n    driver.get_screenshot_as_file(now_time + '.png')\r\n    print('screenshot:' + now_time + '.png')\r\n\r\n    print(driver.current_url)\r\n    print(driver.page_source)\r\n\r\n\r\nif __name__ == '__main__':\r\n    test()\r\n```\r\nthe code can‘t run in the --headless mode.\r\n\r\n-----------\r\n# update：\r\nAfter adding sleep time and update the chrome driver to 71.0.3578.137, and I change the request url to another，I received anther error like\r\nthis : \r\n```\r\nTraceback (most recent call last):\r\n  File \"E:/timeoutTest/Customized/testTemp.py\", line 35, in <module>\r\n    test()\r\n  File \"E:/timeoutTest/Customized/testTemp.py\", line 23, in test\r\n    driver.find_element(By.XPATH, \"//*[@data-view-name=\\\"index\\\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[\"\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 978, in find_element\r\n    'value': value})['value']\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"d:\\Users\\dzlx\\AppData\\Local\\Programs\\Python\\Python36\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//*[@data-view-name=\"index\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[1]/div[1]/p\"}\r\n  (Session info: headless chrome=71.0.3578.98)\r\n  (Driver info: chromedriver=71.0.3578.137 (86ee722808adfe9e3c92e6e8ea746ade08423c7e),platform=Windows NT 6.1.7601 SP1 x86_64)\r\n```\r\n## and my code like this ：\r\n```\r\nimport time\r\n\r\nfrom selenium import webdriver\r\nfrom selenium.webdriver.chrome.options import Options\r\nfrom selenium.webdriver.common.by import By\r\n\r\ndef test():\r\n    _chrome_options = Options()\r\n    _chrome_options.add_argument(\"--headless\")\r\n\r\n    driver = webdriver.Chrome(r\"chromedriver\", chrome_options=_chrome_options)\r\n    # driver = webdriver.Chrome(r\"E:\\\\timeoutTest\\\\Customized\\\\config\\\\chromedriver\")\r\n    # cls.driver = webdriver.Chrome(executable_path=cls._chrome_driver_path)\r\n\r\n    driver.set_window_size(1200, 600)\r\n    driver.set_page_load_timeout(100)\r\n    driver.set_script_timeout(40)\r\n    # 隐式等待\r\n    driver.implicitly_wait(10)\r\n\r\n    driver.get(\"https://m.ctrip.com/webapp/dingzhi/index\")\r\n    time.sleep(10)\r\n    driver.find_element(By.XPATH, \"//*[@data-view-name=\\\"index\\\"]/div/div[2]/div/div[3]/div[1]/div[1]/ul/li[\"\r\n                                        \"1]/div[1]/p\").click()\r\n    time.sleep(10)\r\n    now_time = time.strftime(\"%Y%m%d.%H.%M.%S\")\r\n    driver.get_screenshot_as_file(now_time + '.png')\r\n    print('screenshot:' + now_time + '.png')\r\n    time.sleep(5)\r\n    print(driver.current_url)\r\n    print(driver.page_source)\r\n    driver.quit()\r\n\r\nif __name__ == '__main__':\r\n    test()\r\n```\r\n\r\nthe main reqestion is that, when I doesn't use the --headless mode, I can receive a page  screenshot like this: \r\n![image](https://user-images.githubusercontent.com/15920812/52026703-c9315200-2543-11e9-8d20-0c3dc1c7b9fb.png)\r\n\r\nbut when I use --headless argument, the page seems can't load and throw NoSuchElementException, the screenshot like this:\r\n![image](https://user-images.githubusercontent.com/15920812/52026797-17465580-2544-11e9-84d7-a2c5c28a6b77.png)\r\nthis is a blank image.\r\n\r\nand the question is no error in another seem config computer.\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["458991052","459193455","463373919"], "labels":[]}