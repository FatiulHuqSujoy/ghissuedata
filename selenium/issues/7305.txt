{"id":"7305", "title":"Project Keeps needing to save as new file", "body":"## 🐛 Bug Report

Selenium IDE keeps wanting to save to new file after every change.


## To Reproduce

1. Start new project
2. Save project
3. Make any sort of edit (add echo command)
4. Attempt to save

## Expected behavior

File updates state and saves Normally

## Test script or set of commands reproducing this issue
any command is fine
echo hello world

## Environment

OS: Ubuntu Linux 18.04.2 LTS x86_64
Browser: Firefox
Browser version: 67.0.3
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7305","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7305/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7305/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7305/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7305","id":458125243,"node_id":"MDU6SXNzdWU0NTgxMjUyNDM=","number":7305,"title":"Project Keeps needing to save as new file","user":{"login":"panaman67","id":16823053,"node_id":"MDQ6VXNlcjE2ODIzMDUz","avatar_url":"https://avatars2.githubusercontent.com/u/16823053?v=4","gravatar_id":"","url":"https://api.github.com/users/panaman67","html_url":"https://github.com/panaman67","followers_url":"https://api.github.com/users/panaman67/followers","following_url":"https://api.github.com/users/panaman67/following{/other_user}","gists_url":"https://api.github.com/users/panaman67/gists{/gist_id}","starred_url":"https://api.github.com/users/panaman67/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/panaman67/subscriptions","organizations_url":"https://api.github.com/users/panaman67/orgs","repos_url":"https://api.github.com/users/panaman67/repos","events_url":"https://api.github.com/users/panaman67/events{/privacy}","received_events_url":"https://api.github.com/users/panaman67/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-06-19T17:04:32Z","updated_at":"2019-08-14T08:09:42Z","closed_at":"2019-06-19T17:21:53Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nSelenium IDE keeps wanting to save to new file after every change.\r\n\r\n\r\n## To Reproduce\r\n\r\n1. Start new project\r\n2. Save project\r\n3. Make any sort of edit (add echo command)\r\n4. Attempt to save\r\n\r\n## Expected behavior\r\n\r\nFile updates state and saves Normally\r\n\r\n## Test script or set of commands reproducing this issue\r\nany command is fine\r\necho hello world\r\n\r\n## Environment\r\n\r\nOS: Ubuntu Linux 18.04.2 LTS x86_64\r\nBrowser: Firefox\r\nBrowser version: 67.0.3\r\n","closed_by":{"login":"panaman67","id":16823053,"node_id":"MDQ6VXNlcjE2ODIzMDUz","avatar_url":"https://avatars2.githubusercontent.com/u/16823053?v=4","gravatar_id":"","url":"https://api.github.com/users/panaman67","html_url":"https://github.com/panaman67","followers_url":"https://api.github.com/users/panaman67/followers","following_url":"https://api.github.com/users/panaman67/following{/other_user}","gists_url":"https://api.github.com/users/panaman67/gists{/gist_id}","starred_url":"https://api.github.com/users/panaman67/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/panaman67/subscriptions","organizations_url":"https://api.github.com/users/panaman67/orgs","repos_url":"https://api.github.com/users/panaman67/repos","events_url":"https://api.github.com/users/panaman67/events{/privacy}","received_events_url":"https://api.github.com/users/panaman67/received_events","type":"User","site_admin":false}}", "commentIds":["503653000"], "labels":[]}