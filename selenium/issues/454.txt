{"id":"454", "title":"IEDriver stalls at startup sporadically", "body":"I am starting IEDriver using java client

``` java
        // Some setup for registry etc. I can provide details on what we do for each one of these, but we have had to build these up over the years for various reasons.
        setupProtectedMode();
        formatRegistryForIE11();
        turnAutocompleteOff();
        turnOffPopupBlocking();
        turnOffIEMaxScriptStatementsWarning();
        resetCompatViewForIntranetSetting();

// Actual driver startup section
        caps = DesiredCapabilities.internetExplorer();
    caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
    final WebDriver d = new InternetExplorerDriver(serv,
                    caps);
```

Here are my specs:

```
Windows 7 64 bit 
IE11 11.0.9600.17691CO
IEDriver: 2.45.0.0 (32 bit version)
Most Recent Windows update ran on this host was: Security Update for Windows 7 for x64-based Systems (KB3035131). I can get more updates info if relevant
```

Java thread dump shows 'newSession' is being attempted:

``` ruby
  \"Forwarding newSession on session null to remote\" prio=6 tid=0x00000000022ef000 nid=0x1030 runnable [0x00000000025bd000]
   java.lang.Thread.State: RUNNABLE
    at java.net.SocketInputStream.socketRead0(Native Method)
    at java.net.SocketInputStream.read(SocketInputStream.java:152)
    at java.net.SocketInputStream.read(SocketInputStream.java:122)
    at org.apache.http.impl.io.SessionInputBufferImpl.streamRead(SessionInputBufferImpl.java:136)
    at org.apache.http.impl.io.SessionInputBufferImpl.fillBuffer(SessionInputBufferImpl.java:152)
    at org.apache.http.impl.io.SessionInputBufferImpl.readLine(SessionInputBufferImpl.java:270)
    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:140)
    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:57)
    at org.apache.http.impl.io.AbstractMessageParser.parse(AbstractMessageParser.java:260)
    at org.apache.http.impl.DefaultBHttpClientConnection.receiveResponseHeader(DefaultBHttpClientConnection.java:161)
    at org.apache.http.impl.conn.CPoolProxy.receiveResponseHeader(CPoolProxy.java:153)
    at org.apache.http.protocol.HttpRequestExecutor.doReceiveResponse(HttpRequestExecutor.java:271)
    at org.apache.http.protocol.HttpRequestExecutor.execute(HttpRequestExecutor.java:123)
    at org.apache.http.impl.execchain.MainClientExec.execute(MainClientExec.java:254)
    at org.apache.http.impl.execchain.ProtocolExec.execute(ProtocolExec.java:195)
    at org.apache.http.impl.execchain.RetryExec.execute(RetryExec.java:86)
    at org.apache.http.impl.execchain.RedirectExec.execute(RedirectExec.java:108)
    at org.apache.http.impl.client.InternalHttpClient.doExecute(InternalHttpClient.java:184)
    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:72)
    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:57)
    at org.openqa.selenium.remote.internal.ApacheHttpClient.fallBackExecute(ApacheHttpClient.java:126)
    at org.openqa.selenium.remote.internal.ApacheHttpClient.execute(ApacheHttpClient.java:72)
    at org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:133)
    at org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:66)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:572)
    at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:240)
    at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:225)
    at org.openqa.selenium.ie.InternetExplorerDriver.run(InternetExplorerDriver.java:182)
    at org.openqa.selenium.ie.InternetExplorerDriver.<init>(InternetExplorerDriver.java:174)
    at org.openqa.selenium.ie.InternetExplorerDriver.<init>(InternetExplorerDriver.java:162)
```

At this time, even closing the IE browser does not unlock this, and the stall continues. Killing IEDriver is the only resort.

How do I go about further debugging this? Should I setup IEDriver VS for debugging or can I get some more diagnostics first?
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/454","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/454/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/454/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/454/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/454","id":68417156,"node_id":"MDU6SXNzdWU2ODQxNzE1Ng==","number":454,"title":"IEDriver stalls at startup sporadically","user":{"login":"binodpanta","id":408855,"node_id":"MDQ6VXNlcjQwODg1NQ==","avatar_url":"https://avatars0.githubusercontent.com/u/408855?v=4","gravatar_id":"","url":"https://api.github.com/users/binodpanta","html_url":"https://github.com/binodpanta","followers_url":"https://api.github.com/users/binodpanta/followers","following_url":"https://api.github.com/users/binodpanta/following{/other_user}","gists_url":"https://api.github.com/users/binodpanta/gists{/gist_id}","starred_url":"https://api.github.com/users/binodpanta/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/binodpanta/subscriptions","organizations_url":"https://api.github.com/users/binodpanta/orgs","repos_url":"https://api.github.com/users/binodpanta/repos","events_url":"https://api.github.com/users/binodpanta/events{/privacy}","received_events_url":"https://api.github.com/users/binodpanta/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2015-04-14T15:44:07Z","updated_at":"2019-08-21T14:09:54Z","closed_at":"2015-04-14T16:02:23Z","author_association":"NONE","body":"I am starting IEDriver using java client\n\n``` java\n        // Some setup for registry etc. I can provide details on what we do for each one of these, but we have had to build these up over the years for various reasons.\n        setupProtectedMode();\n        formatRegistryForIE11();\n        turnAutocompleteOff();\n        turnOffPopupBlocking();\n        turnOffIEMaxScriptStatementsWarning();\n        resetCompatViewForIntranetSetting();\n\n// Actual driver startup section\n        caps = DesiredCapabilities.internetExplorer();\n    caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);\n    final WebDriver d = new InternetExplorerDriver(serv,\n                    caps);\n```\n\nHere are my specs:\n\n```\nWindows 7 64 bit \nIE11 11.0.9600.17691CO\nIEDriver: 2.45.0.0 (32 bit version)\nMost Recent Windows update ran on this host was: Security Update for Windows 7 for x64-based Systems (KB3035131). I can get more updates info if relevant\n```\n\nJava thread dump shows 'newSession' is being attempted:\n\n``` ruby\n  \"Forwarding newSession on session null to remote\" prio=6 tid=0x00000000022ef000 nid=0x1030 runnable [0x00000000025bd000]\n   java.lang.Thread.State: RUNNABLE\n    at java.net.SocketInputStream.socketRead0(Native Method)\n    at java.net.SocketInputStream.read(SocketInputStream.java:152)\n    at java.net.SocketInputStream.read(SocketInputStream.java:122)\n    at org.apache.http.impl.io.SessionInputBufferImpl.streamRead(SessionInputBufferImpl.java:136)\n    at org.apache.http.impl.io.SessionInputBufferImpl.fillBuffer(SessionInputBufferImpl.java:152)\n    at org.apache.http.impl.io.SessionInputBufferImpl.readLine(SessionInputBufferImpl.java:270)\n    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:140)\n    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:57)\n    at org.apache.http.impl.io.AbstractMessageParser.parse(AbstractMessageParser.java:260)\n    at org.apache.http.impl.DefaultBHttpClientConnection.receiveResponseHeader(DefaultBHttpClientConnection.java:161)\n    at org.apache.http.impl.conn.CPoolProxy.receiveResponseHeader(CPoolProxy.java:153)\n    at org.apache.http.protocol.HttpRequestExecutor.doReceiveResponse(HttpRequestExecutor.java:271)\n    at org.apache.http.protocol.HttpRequestExecutor.execute(HttpRequestExecutor.java:123)\n    at org.apache.http.impl.execchain.MainClientExec.execute(MainClientExec.java:254)\n    at org.apache.http.impl.execchain.ProtocolExec.execute(ProtocolExec.java:195)\n    at org.apache.http.impl.execchain.RetryExec.execute(RetryExec.java:86)\n    at org.apache.http.impl.execchain.RedirectExec.execute(RedirectExec.java:108)\n    at org.apache.http.impl.client.InternalHttpClient.doExecute(InternalHttpClient.java:184)\n    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:72)\n    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:57)\n    at org.openqa.selenium.remote.internal.ApacheHttpClient.fallBackExecute(ApacheHttpClient.java:126)\n    at org.openqa.selenium.remote.internal.ApacheHttpClient.execute(ApacheHttpClient.java:72)\n    at org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:133)\n    at org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:66)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:572)\n    at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:240)\n    at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:225)\n    at org.openqa.selenium.ie.InternetExplorerDriver.run(InternetExplorerDriver.java:182)\n    at org.openqa.selenium.ie.InternetExplorerDriver.<init>(InternetExplorerDriver.java:174)\n    at org.openqa.selenium.ie.InternetExplorerDriver.<init>(InternetExplorerDriver.java:162)\n```\n\nAt this time, even closing the IE browser does not unlock this, and the stall continues. Killing IEDriver is the only resort.\n\nHow do I go about further debugging this? Should I setup IEDriver VS for debugging or can I get some more diagnostics first?\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["92934365","94173728"], "labels":[]}