{"id":"7300", "title":"Performance log for Chrome doesn't work in 4.0.0-alpha.3", "body":"## 🐛 Bug Report

With Chrome 75/Chromedriver 75 and NodeJS Selenium 4.0.0-alpha.3 getting the Chrome performance log fails. I think it was introduced in https://github.com/SeleniumHQ/selenium/commit/8edc13bdf0e0ca081f5ee8cb1e822a71298cad26 together with the Chromedriver switch to have w3c compliant as default.

This works with Selenium 3.6.0

## To Reproduce

Run the following code + use Chromedriver 75 in the same dir and have Chrome 75 installed.
```
const chrome = require('selenium-webdriver/chrome');
const { Builder, logging } = require('selenium-webdriver');

const options = new chrome.Options();
const logPrefs = new logging.Preferences();
logPrefs.setLevel(logging.Type.PERFORMANCE, logging.Level.ALL);

const perfLogConf = { enableNetwork: true, enablePage: true };

options.setPerfLoggingPrefs(perfLogConf);
options.setLoggingPrefs(logPrefs);

const service = new chrome.ServiceBuilder()
  .loggingTo('./chromedriver.log')
  .enableVerboseLogging()
  .build();
chrome.setDefaultService(service);

const driver = new Builder()
  .forBrowser('chrome')
  .setChromeOptions(options)
  .build();

driver.get(
  'https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/chrome_exports_Driver.html'
);
```
## Expected behavior
The code should not return any error.

It generates an error:  **InvalidArgumentError: invalid argument: perfLoggingPrefs specified, but performance logging was not enabled**. However the log is enabled.

Checking the Chromedriver log we can see that the loggingPrefs is filtered out in the \"always match part\" (since it's only w3c compliant fields).

```
[1560795572.228][INFO]: Starting ChromeDriver 75.0.3770.90 (a6dcaf7e3ec6f70a194cc25e8149475c6590e025-refs/branch-heads/3770@{#1003})
[1560795572.228][INFO]: Please protect ports used by ChromeDriver and related test frameworks to prevent access by malicious code.
[1560795572.251][INFO]: [8580073239e9346564a93f6dfcbd2cae] COMMAND InitSession {
   \"capabilities\": {
      \"alwaysMatch\": {
         \"browserName\": \"chrome\",
         \"goog:chromeOptions\": {
            \"perfLoggingPrefs\": {
               \"enableNetwork\": true,
               \"enablePage\": true
            }
         }
      }
   },
   \"desiredCapabilities\": {
      \"browserName\": \"chrome\",
      \"goog:chromeOptions\": {
         \"perfLoggingPrefs\": {
            \"enableNetwork\": true,
            \"enablePage\": true
         }
      },
      \"loggingPrefs\": {
         \"performance\": \"ALL\"
      }
   }
}
[1560795572.251][INFO]: [8580073239e9346564a93f6dfcbd2cae] RESPONSE InitSession ERROR invalid argument: perfLoggingPrefs specified, but performance logging was not enabled
[1560795572.251][DEBUG]: Log type 'driver' lost 1 entries on destruction
```

Is this a Chromedriver error or Selenium or both? It works in 3.6.0 :)

## Environment

OS: OSX
Browser: Chrome 
Browser version: 75
Browser Driver version: Chromedriver 75.0.3770.90
Language Bindings version: NodeJs 4.0.0-alpha.3
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7300","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7300/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7300/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7300/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7300","id":457090980,"node_id":"MDU6SXNzdWU0NTcwOTA5ODA=","number":7300,"title":"Performance log for Chrome doesn't work in 4.0.0-alpha.3","user":{"login":"soulgalore","id":540757,"node_id":"MDQ6VXNlcjU0MDc1Nw==","avatar_url":"https://avatars2.githubusercontent.com/u/540757?v=4","gravatar_id":"","url":"https://api.github.com/users/soulgalore","html_url":"https://github.com/soulgalore","followers_url":"https://api.github.com/users/soulgalore/followers","following_url":"https://api.github.com/users/soulgalore/following{/other_user}","gists_url":"https://api.github.com/users/soulgalore/gists{/gist_id}","starred_url":"https://api.github.com/users/soulgalore/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/soulgalore/subscriptions","organizations_url":"https://api.github.com/users/soulgalore/orgs","repos_url":"https://api.github.com/users/soulgalore/repos","events_url":"https://api.github.com/users/soulgalore/events{/privacy}","received_events_url":"https://api.github.com/users/soulgalore/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false},{"id":316341013,"node_id":"MDU6TGFiZWwzMTYzNDEwMTM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-chrome","name":"D-chrome","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2019-06-17T18:40:13Z","updated_at":"2019-08-19T10:09:34Z","closed_at":"2019-07-20T09:41:41Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nWith Chrome 75/Chromedriver 75 and NodeJS Selenium 4.0.0-alpha.3 getting the Chrome performance log fails. I think it was introduced in https://github.com/SeleniumHQ/selenium/commit/8edc13bdf0e0ca081f5ee8cb1e822a71298cad26 together with the Chromedriver switch to have w3c compliant as default.\r\n\r\nThis works with Selenium 3.6.0\r\n\r\n## To Reproduce\r\n\r\nRun the following code + use Chromedriver 75 in the same dir and have Chrome 75 installed.\r\n```\r\nconst chrome = require('selenium-webdriver/chrome');\r\nconst { Builder, logging } = require('selenium-webdriver');\r\n\r\nconst options = new chrome.Options();\r\nconst logPrefs = new logging.Preferences();\r\nlogPrefs.setLevel(logging.Type.PERFORMANCE, logging.Level.ALL);\r\n\r\nconst perfLogConf = { enableNetwork: true, enablePage: true };\r\n\r\noptions.setPerfLoggingPrefs(perfLogConf);\r\noptions.setLoggingPrefs(logPrefs);\r\n\r\nconst service = new chrome.ServiceBuilder()\r\n  .loggingTo('./chromedriver.log')\r\n  .enableVerboseLogging()\r\n  .build();\r\nchrome.setDefaultService(service);\r\n\r\nconst driver = new Builder()\r\n  .forBrowser('chrome')\r\n  .setChromeOptions(options)\r\n  .build();\r\n\r\ndriver.get(\r\n  'https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/chrome_exports_Driver.html'\r\n);\r\n```\r\n## Expected behavior\r\nThe code should not return any error.\r\n\r\nIt generates an error:  **InvalidArgumentError: invalid argument: perfLoggingPrefs specified, but performance logging was not enabled**. However the log is enabled.\r\n\r\nChecking the Chromedriver log we can see that the loggingPrefs is filtered out in the \"always match part\" (since it's only w3c compliant fields).\r\n\r\n```\r\n[1560795572.228][INFO]: Starting ChromeDriver 75.0.3770.90 (a6dcaf7e3ec6f70a194cc25e8149475c6590e025-refs/branch-heads/3770@{#1003})\r\n[1560795572.228][INFO]: Please protect ports used by ChromeDriver and related test frameworks to prevent access by malicious code.\r\n[1560795572.251][INFO]: [8580073239e9346564a93f6dfcbd2cae] COMMAND InitSession {\r\n   \"capabilities\": {\r\n      \"alwaysMatch\": {\r\n         \"browserName\": \"chrome\",\r\n         \"goog:chromeOptions\": {\r\n            \"perfLoggingPrefs\": {\r\n               \"enableNetwork\": true,\r\n               \"enablePage\": true\r\n            }\r\n         }\r\n      }\r\n   },\r\n   \"desiredCapabilities\": {\r\n      \"browserName\": \"chrome\",\r\n      \"goog:chromeOptions\": {\r\n         \"perfLoggingPrefs\": {\r\n            \"enableNetwork\": true,\r\n            \"enablePage\": true\r\n         }\r\n      },\r\n      \"loggingPrefs\": {\r\n         \"performance\": \"ALL\"\r\n      }\r\n   }\r\n}\r\n[1560795572.251][INFO]: [8580073239e9346564a93f6dfcbd2cae] RESPONSE InitSession ERROR invalid argument: perfLoggingPrefs specified, but performance logging was not enabled\r\n[1560795572.251][DEBUG]: Log type 'driver' lost 1 entries on destruction\r\n```\r\n\r\nIs this a Chromedriver error or Selenium or both? It works in 3.6.0 :)\r\n\r\n## Environment\r\n\r\nOS: OSX\r\nBrowser: Chrome \r\nBrowser version: 75\r\nBrowser Driver version: Chromedriver 75.0.3770.90\r\nLanguage Bindings version: NodeJs 4.0.0-alpha.3\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["504813820","504838751","504839436","513453226"], "labels":["C-nodejs","D-chrome"]}