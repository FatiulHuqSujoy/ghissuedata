{"id":"6609", "title":"Backward incompatible minor release selenium-3.141.0", "body":"## Meta -
OS:  Ubuntu 16.04
<!-- Windows 10? OSX? -->
Selenium Version:  selenium 3.141.0
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  64.0.3282.167 (64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -

`org.openqa.selenium.support.ui.Duration` was deprecated in Selenium 3.10.0 but still I expect it to be present in selenium-3.141.0 because it is a minor version change and also it is not in part of the internal package.

## Actual Behavior -

`org.openqa.selenium.support.ui.Duration` was removed in selenium-3.141.0.

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

```
dependencies {
    compile('com.paulhammant:ngwebdriver:1.0') 
}
```
`com.paulhammant:ngwebdriver:1.0` has as an open-ended dependency `org.seleniumhq.selenium:selenium-java:[3.0.1,)`

```java
java.lang.NoClassDefFoundError: org/openqa/selenium/support/ui/Duration

	at java.lang.Class.getDeclaredMethods0(Native Method)
	at java.lang.Class.privateGetDeclaredMethods(Class.java:2701)
	at java.lang.Class.getDeclaredMethods(Class.java:1975)
	at com.google.inject.spi.InjectionPoint.getInjectionPoints(InjectionPoint.java:688)
	at com.google.inject.spi.InjectionPoint.forInstanceMethodsAndFields(InjectionPoint.java:380)
	at com.google.inject.internal.ConstructorBindingImpl.getInternalDependencies(ConstructorBindingImpl.java:165)
	at com.google.inject.internal.InjectorImpl.getInternalDependencies(InjectorImpl.java:616)
	at com.google.inject.internal.InjectorImpl.cleanup(InjectorImpl.java:572)
	at com.google.inject.internal.InjectorImpl.initializeJitBinding(InjectorImpl.java:558)
	at com.google.inject.internal.InjectorImpl.createJustInTimeBinding(InjectorImpl.java:887)
	at com.google.inject.internal.InjectorImpl.createJustInTimeBindingRecursive(InjectorImpl.java:808)
	at com.google.inject.internal.InjectorImpl.getJustInTimeBinding(InjectorImpl.java:285)
	at com.google.inject.internal.InjectorImpl.getBindingOrThrow(InjectorImpl.java:217)
	at com.google.inject.internal.InjectorImpl.getProviderOrThrow(InjectorImpl.java:1009)
	at com.google.inject.internal.InjectorImpl.getProvider(InjectorImpl.java:1041)
	at com.google.inject.internal.InjectorImpl.getProvider(InjectorImpl.java:1004)
	at com.google.inject.internal.InjectorImpl.getInstance(InjectorImpl.java:1054)
	at net.serenitybdd.core.Serenity.setupWebDriverFactory(Serenity.java:117)
	at net.serenitybdd.core.Serenity.initializeWithNoStepListener(Serenity.java:93)
	at cucumber.runtime.SerenityObjectFactory.newInstance(SerenityObjectFactory.java:68)
	at cucumber.runtime.SerenityObjectFactory.cacheNewInstance(SerenityObjectFactory.java:51)
	at cucumber.runtime.SerenityObjectFactory.getInstance(SerenityObjectFactory.java:41)
	at cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)
	at cucumber.runtime.HookDefinitionMatch.runStep(HookDefinitionMatch.java:17)
	at cucumber.runner.UnskipableStep.executeStep(UnskipableStep.java:22)
	at cucumber.api.TestStep.run(TestStep.java:83)
	at cucumber.api.TestCase.run(TestCase.java:58)
	at cucumber.runner.Runner.runPickle(Runner.java:80)
	at cucumber.runtime.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:140)
	at cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:68)
	at cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:23)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:73)
	at cucumber.api.junit.Cucumber.runChild(Cucumber.java:117)
	at cucumber.api.junit.Cucumber.runChild(Cucumber.java:55)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at cucumber.api.junit.Cucumber$1.evaluate(Cucumber.java:126)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at org.junit.runner.JUnitCore.run(JUnitCore.java:137)
	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:68)
	at com.intellij.rt.execution.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:47)
	at com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:242)
	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
Caused by: java.lang.ClassNotFoundException: org.openqa.selenium.support.ui.Duration
	at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:349)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
	... 52 more
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6609","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6609/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6609/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6609/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6609","id":376791847,"node_id":"MDU6SXNzdWUzNzY3OTE4NDc=","number":6609,"title":"Backward incompatible minor release selenium-3.141.0","user":{"login":"maestrokame","id":7707752,"node_id":"MDQ6VXNlcjc3MDc3NTI=","avatar_url":"https://avatars1.githubusercontent.com/u/7707752?v=4","gravatar_id":"","url":"https://api.github.com/users/maestrokame","html_url":"https://github.com/maestrokame","followers_url":"https://api.github.com/users/maestrokame/followers","following_url":"https://api.github.com/users/maestrokame/following{/other_user}","gists_url":"https://api.github.com/users/maestrokame/gists{/gist_id}","starred_url":"https://api.github.com/users/maestrokame/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/maestrokame/subscriptions","organizations_url":"https://api.github.com/users/maestrokame/orgs","repos_url":"https://api.github.com/users/maestrokame/repos","events_url":"https://api.github.com/users/maestrokame/events{/privacy}","received_events_url":"https://api.github.com/users/maestrokame/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-11-02T12:33:04Z","updated_at":"2019-08-15T06:09:57Z","closed_at":"2018-11-02T12:55:24Z","author_association":"NONE","body":"## Meta -\r\nOS:  Ubuntu 16.04\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  selenium 3.141.0\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  64.0.3282.167 (64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\n\r\n`org.openqa.selenium.support.ui.Duration` was deprecated in Selenium 3.10.0 but still I expect it to be present in selenium-3.141.0 because it is a minor version change and also it is not in part of the internal package.\r\n\r\n## Actual Behavior -\r\n\r\n`org.openqa.selenium.support.ui.Duration` was removed in selenium-3.141.0.\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\n```\r\ndependencies {\r\n    compile('com.paulhammant:ngwebdriver:1.0') \r\n}\r\n```\r\n`com.paulhammant:ngwebdriver:1.0` has as an open-ended dependency `org.seleniumhq.selenium:selenium-java:[3.0.1,)`\r\n\r\n```java\r\njava.lang.NoClassDefFoundError: org/openqa/selenium/support/ui/Duration\r\n\r\n\tat java.lang.Class.getDeclaredMethods0(Native Method)\r\n\tat java.lang.Class.privateGetDeclaredMethods(Class.java:2701)\r\n\tat java.lang.Class.getDeclaredMethods(Class.java:1975)\r\n\tat com.google.inject.spi.InjectionPoint.getInjectionPoints(InjectionPoint.java:688)\r\n\tat com.google.inject.spi.InjectionPoint.forInstanceMethodsAndFields(InjectionPoint.java:380)\r\n\tat com.google.inject.internal.ConstructorBindingImpl.getInternalDependencies(ConstructorBindingImpl.java:165)\r\n\tat com.google.inject.internal.InjectorImpl.getInternalDependencies(InjectorImpl.java:616)\r\n\tat com.google.inject.internal.InjectorImpl.cleanup(InjectorImpl.java:572)\r\n\tat com.google.inject.internal.InjectorImpl.initializeJitBinding(InjectorImpl.java:558)\r\n\tat com.google.inject.internal.InjectorImpl.createJustInTimeBinding(InjectorImpl.java:887)\r\n\tat com.google.inject.internal.InjectorImpl.createJustInTimeBindingRecursive(InjectorImpl.java:808)\r\n\tat com.google.inject.internal.InjectorImpl.getJustInTimeBinding(InjectorImpl.java:285)\r\n\tat com.google.inject.internal.InjectorImpl.getBindingOrThrow(InjectorImpl.java:217)\r\n\tat com.google.inject.internal.InjectorImpl.getProviderOrThrow(InjectorImpl.java:1009)\r\n\tat com.google.inject.internal.InjectorImpl.getProvider(InjectorImpl.java:1041)\r\n\tat com.google.inject.internal.InjectorImpl.getProvider(InjectorImpl.java:1004)\r\n\tat com.google.inject.internal.InjectorImpl.getInstance(InjectorImpl.java:1054)\r\n\tat net.serenitybdd.core.Serenity.setupWebDriverFactory(Serenity.java:117)\r\n\tat net.serenitybdd.core.Serenity.initializeWithNoStepListener(Serenity.java:93)\r\n\tat cucumber.runtime.SerenityObjectFactory.newInstance(SerenityObjectFactory.java:68)\r\n\tat cucumber.runtime.SerenityObjectFactory.cacheNewInstance(SerenityObjectFactory.java:51)\r\n\tat cucumber.runtime.SerenityObjectFactory.getInstance(SerenityObjectFactory.java:41)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.HookDefinitionMatch.runStep(HookDefinitionMatch.java:17)\r\n\tat cucumber.runner.UnskipableStep.executeStep(UnskipableStep.java:22)\r\n\tat cucumber.api.TestStep.run(TestStep.java:83)\r\n\tat cucumber.api.TestCase.run(TestCase.java:58)\r\n\tat cucumber.runner.Runner.runPickle(Runner.java:80)\r\n\tat cucumber.runtime.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:140)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:68)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:23)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:73)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:117)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:55)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat cucumber.api.junit.Cucumber$1.evaluate(Cucumber.java:126)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:68)\r\n\tat com.intellij.rt.execution.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:47)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:242)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)\r\nCaused by: java.lang.ClassNotFoundException: org.openqa.selenium.support.ui.Duration\r\n\tat java.net.URLClassLoader.findClass(URLClassLoader.java:381)\r\n\tat java.lang.ClassLoader.loadClass(ClassLoader.java:424)\r\n\tat sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:349)\r\n\tat java.lang.ClassLoader.loadClass(ClassLoader.java:357)\r\n\t... 52 more\r\n```","closed_by":{"login":"shs96c","id":28229,"node_id":"MDQ6VXNlcjI4MjI5","avatar_url":"https://avatars2.githubusercontent.com/u/28229?v=4","gravatar_id":"","url":"https://api.github.com/users/shs96c","html_url":"https://github.com/shs96c","followers_url":"https://api.github.com/users/shs96c/followers","following_url":"https://api.github.com/users/shs96c/following{/other_user}","gists_url":"https://api.github.com/users/shs96c/gists{/gist_id}","starred_url":"https://api.github.com/users/shs96c/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/shs96c/subscriptions","organizations_url":"https://api.github.com/users/shs96c/orgs","repos_url":"https://api.github.com/users/shs96c/repos","events_url":"https://api.github.com/users/shs96c/events{/privacy}","received_events_url":"https://api.github.com/users/shs96c/received_events","type":"User","site_admin":false}}", "commentIds":["435369981"], "labels":[]}