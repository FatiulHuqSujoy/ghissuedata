{"id":"4427", "title":"Selenium IDE 2.9.1", "body":"Getting error on loading for Selenium IDE 2.9.1. My IDE doesn't run, I cannot add test cases or open a test suite or create or record one! Error as followings: 
There was an unexpected error. Msg: TypeError: obj is undefined
Url: chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789, line: 185, column: 9
wrap@chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789:185:9
ImplicitWait/<@chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789:27:9 

There was an unexpected error. Msg: TypeError: obj is undefined
Url: chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808, line: 217, column: 9
wrap@chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808:217:9
ImplicitWait/<@chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808:27:9
Function.prototype.bind/<@chrome://selenium-ide/content/selenium-core/lib/prototype.js:48:12

## Meta -
OS:  windows 10
<!-- Windows 10? OSX? -->
Selenium Version:  2.9.1 IDE
<!-- 2.52.0, IDE, etc -->
Browser:  FireFox 55
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  55 (64 bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior - 

## Actual Behavior -

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4427","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4427/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4427/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4427/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4427","id":248848017,"node_id":"MDU6SXNzdWUyNDg4NDgwMTc=","number":4427,"title":"Selenium IDE 2.9.1","user":{"login":"folu6643","id":25209678,"node_id":"MDQ6VXNlcjI1MjA5Njc4","avatar_url":"https://avatars0.githubusercontent.com/u/25209678?v=4","gravatar_id":"","url":"https://api.github.com/users/folu6643","html_url":"https://github.com/folu6643","followers_url":"https://api.github.com/users/folu6643/followers","following_url":"https://api.github.com/users/folu6643/following{/other_user}","gists_url":"https://api.github.com/users/folu6643/gists{/gist_id}","starred_url":"https://api.github.com/users/folu6643/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/folu6643/subscriptions","organizations_url":"https://api.github.com/users/folu6643/orgs","repos_url":"https://api.github.com/users/folu6643/repos","events_url":"https://api.github.com/users/folu6643/events{/privacy}","received_events_url":"https://api.github.com/users/folu6643/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-08-08T21:12:25Z","updated_at":"2019-08-17T14:09:35Z","closed_at":"2017-08-09T05:36:33Z","author_association":"NONE","body":"Getting error on loading for Selenium IDE 2.9.1. My IDE doesn't run, I cannot add test cases or open a test suite or create or record one! Error as followings: \r\nThere was an unexpected error. Msg: TypeError: obj is undefined\r\nUrl: chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789, line: 185, column: 9\r\nwrap@chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789:185:9\r\nImplicitWait/<@chrome://implicit-wait/content/implicit-wait-ide.js?1502226081789:27:9 \r\n\r\nThere was an unexpected error. Msg: TypeError: obj is undefined\r\nUrl: chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808, line: 217, column: 9\r\nwrap@chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808:217:9\r\nImplicitWait/<@chrome://idex/content/extensions/implicit-wait-ide.js?1502226081808:27:9\r\nFunction.prototype.bind/<@chrome://selenium-ide/content/selenium-core/lib/prototype.js:48:12\r\n\r\n## Meta -\r\nOS:  windows 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  2.9.1 IDE\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  FireFox 55\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  55 (64 bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior - \r\n\r\n## Actual Behavior -\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["321157495","323930649","327329777"], "labels":[]}