{"id":"5471", "title":"Alert Authentication Method not working properly", "body":"## Meta -
OS:  Windows 7
<!-- Windows 10? OSX? -->
Selenium Version:  3.4.3
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  Version 64.0.3282.140 (Offizieller Build) (64-Bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
Alert Authenticate method authenticates and execution continues

## Actual Behavior -
Nothing happens. Authenticate method freezes. I think around this point (webdriver.py:295)

```
        if response:
            self.error_handler.check_response(response)
            response['value'] = self._unwrap_value(
                response.get('value', None))
            return response
```

## Steps to reproduce -

This is the function i'm using to handle the alert:

```
    def login_browser_alert(driver):
        WebDriverWait(driver, 3).until(EC.alert_is_present())
        current_window = driver.window_handles[0]
        driver.switch_to.alert.authenticate('admin', 'admin')
        driver.switch_to.window(current_window)
```

This is how I navigate to the page:

```
    def goto_page(self):
            self.driver.get(self.URL)
            login_browser_alert(self.driver)
```

I'm using http://the-internet.herokuapp.com/basic_auth as a dummy Authentication example. Credentials are user: \"admin\" pass: \"admin\"

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5471","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5471/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5471/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5471/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5471","id":295831446,"node_id":"MDU6SXNzdWUyOTU4MzE0NDY=","number":5471,"title":"Alert Authentication Method not working properly","user":{"login":"fertaku","id":3844782,"node_id":"MDQ6VXNlcjM4NDQ3ODI=","avatar_url":"https://avatars3.githubusercontent.com/u/3844782?v=4","gravatar_id":"","url":"https://api.github.com/users/fertaku","html_url":"https://github.com/fertaku","followers_url":"https://api.github.com/users/fertaku/followers","following_url":"https://api.github.com/users/fertaku/following{/other_user}","gists_url":"https://api.github.com/users/fertaku/gists{/gist_id}","starred_url":"https://api.github.com/users/fertaku/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/fertaku/subscriptions","organizations_url":"https://api.github.com/users/fertaku/orgs","repos_url":"https://api.github.com/users/fertaku/repos","events_url":"https://api.github.com/users/fertaku/events{/privacy}","received_events_url":"https://api.github.com/users/fertaku/received_events","type":"User","site_admin":false},"labels":[{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-02-09T10:57:26Z","updated_at":"2019-08-16T04:09:45Z","closed_at":"2018-04-28T03:48:39Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 7\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.4.3\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  Version 64.0.3282.140 (Offizieller Build) (64-Bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nAlert Authenticate method authenticates and execution continues\r\n\r\n## Actual Behavior -\r\nNothing happens. Authenticate method freezes. I think around this point (webdriver.py:295)\r\n\r\n```\r\n        if response:\r\n            self.error_handler.check_response(response)\r\n            response['value'] = self._unwrap_value(\r\n                response.get('value', None))\r\n            return response\r\n```\r\n\r\n## Steps to reproduce -\r\n\r\nThis is the function i'm using to handle the alert:\r\n\r\n```\r\n    def login_browser_alert(driver):\r\n        WebDriverWait(driver, 3).until(EC.alert_is_present())\r\n        current_window = driver.window_handles[0]\r\n        driver.switch_to.alert.authenticate('admin', 'admin')\r\n        driver.switch_to.window(current_window)\r\n```\r\n\r\nThis is how I navigate to the page:\r\n\r\n```\r\n    def goto_page(self):\r\n            self.driver.get(self.URL)\r\n            login_browser_alert(self.driver)\r\n```\r\n\r\nI'm using http://the-internet.herokuapp.com/basic_auth as a dummy Authentication example. Credentials are user: \"admin\" pass: \"admin\"\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["364457555","364846060","385137537"], "labels":["C-py"]}