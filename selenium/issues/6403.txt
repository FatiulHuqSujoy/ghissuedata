{"id":"6403", "title":"WebExtension can't installed in Firefox 62.0 by Java WebDriver", "body":"## Meta -
OS:   16.04.4 LTS
Selenium Version:   3.14.0
Browser:   Firefox: 62.0
Java: JDK 1.8

Testcase

I download a XPI file from https://addons.mozilla.org/en-US/firefox/addon/search-encrypt/?src=hp-dl-promo. 
And try to install it with Selenium Java.  It works fine in Firefox 61, but after upgrade to Firefox 62 , those can't not be installed by \"not correctly signed\" error. 
For the same case, I can install the extension with Selenium nodejs API.
I also can install the extension manually in firefox 62 without selenium and geckodriver.

Java code
```

                FirefoxProfile profile = null;
	        String fileName = \"C:\\\\candle\\\\xpi\\\\search_encrypt-2.3.4-an+fx.xpi\";
		File xpiFile1 = new File(fileName);
		
		profile = new FirefoxProfile();
		
		FirefoxOptions options = new FirefoxOptions();
		options.setLogLevel(FirefoxDriverLogLevel.TRACE);
		
//          profile.setPreference(\"devtools.toolbox.selectedTool\", \"netmonitor\");
		profile.setPreference(\"devtools.toolbox.footer.height\", 0);
		profile.setPreference(\"devtools.devedition.promo.enabled\", false);
		profile.setPreference(\"devtools.devedition.promo.shown\", false);
//		profile.addExtension(xpiFile);
		profile.addExtension(xpiFile1);
		options.addArguments(\"-devtools\");
		options.setProfile(profile);
		String hub = \"xx.xx.xx.xx\";
		URL seleniumHub = new URL(\"http://\" + hub + \":4444/wd/hub\");
		RemoteWebDriver driver = new RemoteWebDriver(seleniumHub, options);
		driver.manage().window().maximize();
```

Error log

```
1536730521001   addons.xpi-utils        DEBUG   Error: Synchronously loading the add-ons database (resource://gre/modules/addons/XPIDatabase.jsm:1329:15) JS Stack trace: syncLoadDB@XPIDatabase.jsm:1329:15
checkForChanges@XPIProvider.jsm:2533:9
startup@XPIProvider.jsm:2116:25
callProvider@AddonManager.jsm:206:12
_startProvider@AddonManager.jsm:654:5
startup@AddonManager.jsm:813:9
startup@AddonManager.jsm:2811:5
observe@addonManager.js:66:9
1536730521001   addons.xpi-utils        DEBUG   Starting async load of XPI database /tmp/rust_mozprofile.dyVcySvW0uEt/extensions.json
1536730521032   addons.xpi-utils        DEBUG   New add-on @searchencrypt installed in app-profile
*** Blocklist::_preloadBlocklistFile: blocklist is disabled
1536730521158   addons.xpi-utils        WARN    Add-on @searchencrypt is not correctly signed.
1536730521158   addons.xpi-utils        WARN    Add-on @searchencrypt is not correctly signed.
1536730521158   addons.xpi-utils        WARN    addMetadata: Add-on @searchencrypt is invalid: Error: Extension @searchencrypt is not correctly signed (resource://gre/modules/addons/XPIDatabase.jsm:2395:17) JS Stack trace: addMetadata@XPIDatabase.jsm:2395:17
processFileChanges@XPIDatabase.jsm:2727:21
checkForChanges@XPIProvider.jsm:2535:34
startup@XPIProvider.jsm:2116:25
callProvider@AddonManager.jsm:206:12
_startProvider@AddonManager.jsm:654:5
startup@AddonManager.jsm:813:9
startup@AddonManager.jsm:2811:5
```

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6403","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6403/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6403/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6403/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6403","id":359744234,"node_id":"MDU6SXNzdWUzNTk3NDQyMzQ=","number":6403,"title":"WebExtension can't installed in Firefox 62.0 by Java WebDriver","user":{"login":"xiaospider","id":4019176,"node_id":"MDQ6VXNlcjQwMTkxNzY=","avatar_url":"https://avatars1.githubusercontent.com/u/4019176?v=4","gravatar_id":"","url":"https://api.github.com/users/xiaospider","html_url":"https://github.com/xiaospider","followers_url":"https://api.github.com/users/xiaospider/followers","following_url":"https://api.github.com/users/xiaospider/following{/other_user}","gists_url":"https://api.github.com/users/xiaospider/gists{/gist_id}","starred_url":"https://api.github.com/users/xiaospider/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/xiaospider/subscriptions","organizations_url":"https://api.github.com/users/xiaospider/orgs","repos_url":"https://api.github.com/users/xiaospider/repos","events_url":"https://api.github.com/users/xiaospider/events{/privacy}","received_events_url":"https://api.github.com/users/xiaospider/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false},{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":10,"created_at":"2018-09-13T05:06:40Z","updated_at":"2019-08-15T09:09:50Z","closed_at":"2018-09-28T13:22:22Z","author_association":"NONE","body":"## Meta -\r\nOS:   16.04.4 LTS\r\nSelenium Version:   3.14.0\r\nBrowser:   Firefox: 62.0\r\nJava: JDK 1.8\r\n\r\nTestcase\r\n\r\nI download a XPI file from https://addons.mozilla.org/en-US/firefox/addon/search-encrypt/?src=hp-dl-promo. \r\nAnd try to install it with Selenium Java.  It works fine in Firefox 61, but after upgrade to Firefox 62 , those can't not be installed by \"not correctly signed\" error. \r\nFor the same case, I can install the extension with Selenium nodejs API.\r\nI also can install the extension manually in firefox 62 without selenium and geckodriver.\r\n\r\nJava code\r\n```\r\n\r\n                FirefoxProfile profile = null;\r\n\t        String fileName = \"C:\\\\candle\\\\xpi\\\\search_encrypt-2.3.4-an+fx.xpi\";\r\n\t\tFile xpiFile1 = new File(fileName);\r\n\t\t\r\n\t\tprofile = new FirefoxProfile();\r\n\t\t\r\n\t\tFirefoxOptions options = new FirefoxOptions();\r\n\t\toptions.setLogLevel(FirefoxDriverLogLevel.TRACE);\r\n\t\t\r\n//          profile.setPreference(\"devtools.toolbox.selectedTool\", \"netmonitor\");\r\n\t\tprofile.setPreference(\"devtools.toolbox.footer.height\", 0);\r\n\t\tprofile.setPreference(\"devtools.devedition.promo.enabled\", false);\r\n\t\tprofile.setPreference(\"devtools.devedition.promo.shown\", false);\r\n//\t\tprofile.addExtension(xpiFile);\r\n\t\tprofile.addExtension(xpiFile1);\r\n\t\toptions.addArguments(\"-devtools\");\r\n\t\toptions.setProfile(profile);\r\n\t\tString hub = \"xx.xx.xx.xx\";\r\n\t\tURL seleniumHub = new URL(\"http://\" + hub + \":4444/wd/hub\");\r\n\t\tRemoteWebDriver driver = new RemoteWebDriver(seleniumHub, options);\r\n\t\tdriver.manage().window().maximize();\r\n```\r\n\r\nError log\r\n\r\n```\r\n1536730521001   addons.xpi-utils        DEBUG   Error: Synchronously loading the add-ons database (resource://gre/modules/addons/XPIDatabase.jsm:1329:15) JS Stack trace: syncLoadDB@XPIDatabase.jsm:1329:15\r\ncheckForChanges@XPIProvider.jsm:2533:9\r\nstartup@XPIProvider.jsm:2116:25\r\ncallProvider@AddonManager.jsm:206:12\r\n_startProvider@AddonManager.jsm:654:5\r\nstartup@AddonManager.jsm:813:9\r\nstartup@AddonManager.jsm:2811:5\r\nobserve@addonManager.js:66:9\r\n1536730521001   addons.xpi-utils        DEBUG   Starting async load of XPI database /tmp/rust_mozprofile.dyVcySvW0uEt/extensions.json\r\n1536730521032   addons.xpi-utils        DEBUG   New add-on @searchencrypt installed in app-profile\r\n*** Blocklist::_preloadBlocklistFile: blocklist is disabled\r\n1536730521158   addons.xpi-utils        WARN    Add-on @searchencrypt is not correctly signed.\r\n1536730521158   addons.xpi-utils        WARN    Add-on @searchencrypt is not correctly signed.\r\n1536730521158   addons.xpi-utils        WARN    addMetadata: Add-on @searchencrypt is invalid: Error: Extension @searchencrypt is not correctly signed (resource://gre/modules/addons/XPIDatabase.jsm:2395:17) JS Stack trace: addMetadata@XPIDatabase.jsm:2395:17\r\nprocessFileChanges@XPIDatabase.jsm:2727:21\r\ncheckForChanges@XPIProvider.jsm:2535:34\r\nstartup@XPIProvider.jsm:2116:25\r\ncallProvider@AddonManager.jsm:206:12\r\n_startProvider@AddonManager.jsm:654:5\r\nstartup@AddonManager.jsm:813:9\r\nstartup@AddonManager.jsm:2811:5\r\n```\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["421015540","421197821","421252889","421702362","423416830","424578451","425227452","425259355","425399877","425433799"], "labels":["C-java","D-firefox"]}