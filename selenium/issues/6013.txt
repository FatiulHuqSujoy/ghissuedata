{"id":"6013", "title":"java.net.ConnectException: Failed to connect to localhost/127.0.0.1:xxxx when Initialising Firefox Driver in Ubuntu 16.04 (Linux)", "body":"## Meta -
OS:  
Ubuntu 16.04

Selenium Version:  
3.12.0

Browser:  
Firefox

Browser Version:  
60.0.1  (64-bit)

## Expected Behavior -
Driver Initialize without error.

## Actual Behavior -

The following Error Stacktrace, occurs upon driver initialization:

java.net.ConnectException: Failed to connect to localhost/127.0.0.1:5054 Build info: version: '3.12.0', revision: '7c6e0b3', time: '2018-05-08T14:04:26.12Z' System info: host: 'michelad-Lenovo-YOGA-3-Pro-1370', ip: '127.0.1.1', os.name: 'Linux', os.arch: 'amd64', os.version: '4.4.0-127-generic', java.version: '1.8.0_171' Driver info: driver.version: FirefoxDriver
org.openqa.selenium.WebDriverException
Build info: version: '3.12.0', revision: '7c6e0b3', time: '2018-05-08T14:04:26.12Z'
System info: host: 'michelad-Lenovo-YOGA-3-Pro-1370', ip: '127.0.1.1', os.name: 'Linux', os.arch: 'amd64', os.version: '4.4.0-127-generic', java.version: '1.8.0_171'
Driver info: driver.version: FirefoxDriver
	at org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:92)
	at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:543)
	at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:207)
	at org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:130)
	at org.openqa.selenium.firefox.FirefoxDriver.<init>(FirefoxDriver.java:125)


## Steps to reproduce -
Run the following java code:

   public static void main(String[] args) {
           System.setProperty(\"webdriver.gecko.driver\", \"/path/to/geckodriver\");
           FirefoxOptions options = new FirefoxOptions();
           options.addArguments(\"--headless\");
           WebDriver driver =  new FirefoxDriver(options);
           System.out.println(\"Mozilla Firefox Headless Browser Initialized\");
           driver.get(\"http://www.google.com\");
           System.out.println(\"Page Title is : \"+driver.getTitle());
           driver.quit();
      }

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6013","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6013/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6013/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6013/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6013","id":331670278,"node_id":"MDU6SXNzdWUzMzE2NzAyNzg=","number":6013,"title":"java.net.ConnectException: Failed to connect to localhost/127.0.0.1:xxxx when Initialising Firefox Driver in Ubuntu 16.04 (Linux)","user":{"login":"micheladennis","id":32935300,"node_id":"MDQ6VXNlcjMyOTM1MzAw","avatar_url":"https://avatars3.githubusercontent.com/u/32935300?v=4","gravatar_id":"","url":"https://api.github.com/users/micheladennis","html_url":"https://github.com/micheladennis","followers_url":"https://api.github.com/users/micheladennis/followers","following_url":"https://api.github.com/users/micheladennis/following{/other_user}","gists_url":"https://api.github.com/users/micheladennis/gists{/gist_id}","starred_url":"https://api.github.com/users/micheladennis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/micheladennis/subscriptions","organizations_url":"https://api.github.com/users/micheladennis/orgs","repos_url":"https://api.github.com/users/micheladennis/repos","events_url":"https://api.github.com/users/micheladennis/events{/privacy}","received_events_url":"https://api.github.com/users/micheladennis/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-06-12T16:50:26Z","updated_at":"2019-08-15T22:10:01Z","closed_at":"2018-06-12T21:14:43Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nUbuntu 16.04\r\n\r\nSelenium Version:  \r\n3.12.0\r\n\r\nBrowser:  \r\nFirefox\r\n\r\nBrowser Version:  \r\n60.0.1  (64-bit)\r\n\r\n## Expected Behavior -\r\nDriver Initialize without error.\r\n\r\n## Actual Behavior -\r\n\r\nThe following Error Stacktrace, occurs upon driver initialization:\r\n\r\njava.net.ConnectException: Failed to connect to localhost/127.0.0.1:5054 Build info: version: '3.12.0', revision: '7c6e0b3', time: '2018-05-08T14:04:26.12Z' System info: host: 'michelad-Lenovo-YOGA-3-Pro-1370', ip: '127.0.1.1', os.name: 'Linux', os.arch: 'amd64', os.version: '4.4.0-127-generic', java.version: '1.8.0_171' Driver info: driver.version: FirefoxDriver\r\norg.openqa.selenium.WebDriverException\r\nBuild info: version: '3.12.0', revision: '7c6e0b3', time: '2018-05-08T14:04:26.12Z'\r\nSystem info: host: 'michelad-Lenovo-YOGA-3-Pro-1370', ip: '127.0.1.1', os.name: 'Linux', os.arch: 'amd64', os.version: '4.4.0-127-generic', java.version: '1.8.0_171'\r\nDriver info: driver.version: FirefoxDriver\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:92)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:543)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:207)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:130)\r\n\tat org.openqa.selenium.firefox.FirefoxDriver.<init>(FirefoxDriver.java:125)\r\n\r\n\r\n## Steps to reproduce -\r\nRun the following java code:\r\n\r\n   public static void main(String[] args) {\r\n           System.setProperty(\"webdriver.gecko.driver\", \"/path/to/geckodriver\");\r\n           FirefoxOptions options = new FirefoxOptions();\r\n           options.addArguments(\"--headless\");\r\n           WebDriver driver =  new FirefoxDriver(options);\r\n           System.out.println(\"Mozilla Firefox Headless Browser Initialized\");\r\n           driver.get(\"http://www.google.com\");\r\n           System.out.println(\"Page Title is : \"+driver.getTitle());\r\n           driver.quit();\r\n      }\r\n\r\n","closed_by":{"login":"micheladennis","id":32935300,"node_id":"MDQ6VXNlcjMyOTM1MzAw","avatar_url":"https://avatars3.githubusercontent.com/u/32935300?v=4","gravatar_id":"","url":"https://api.github.com/users/micheladennis","html_url":"https://github.com/micheladennis","followers_url":"https://api.github.com/users/micheladennis/followers","following_url":"https://api.github.com/users/micheladennis/following{/other_user}","gists_url":"https://api.github.com/users/micheladennis/gists{/gist_id}","starred_url":"https://api.github.com/users/micheladennis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/micheladennis/subscriptions","organizations_url":"https://api.github.com/users/micheladennis/orgs","repos_url":"https://api.github.com/users/micheladennis/repos","events_url":"https://api.github.com/users/micheladennis/events{/privacy}","received_events_url":"https://api.github.com/users/micheladennis/received_events","type":"User","site_admin":false}}", "commentIds":["396665924","396681117","396691414","396715999","396717233","396736008"], "labels":["D-firefox","R-awaiting answer"]}