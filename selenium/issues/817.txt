{"id":"817", "title":"Unexpected exception during findElement", "body":"I'm getting the following error when trying to use findElement via facebook's php webdriver.
Have replicated it on Mac OSX 10.10.4, Windows 7 and CentOS 6.6

```
10:25:25.229 INFO - Launching a standalone Selenium Server
10:25:25.272 INFO - Java: Oracle Corporation 25.20-b23
10:25:25.273 INFO - OS: Mac OS X 10.10.4 x86_64
10:25:25.284 INFO - v2.46.0, with Core v2.46.0. Built from revision 87c69e2
10:25:25.366 INFO - Driver provider org.openqa.selenium.ie.InternetExplorerDriver registration is skipped:
registration capabilities Capabilities [{ensureCleanSession=true, browserName=internet explorer, version=, platform=WINDOWS}] does not match the current platform MAC
10:25:25.366 INFO - Driver class not found: com.opera.core.systems.OperaDriver
10:25:25.367 INFO - Driver provider com.opera.core.systems.OperaDriver is not registered
10:25:25.446 INFO - RemoteWebDriver instances should connect to: http://127.0.0.1:4444/wd/hub
10:25:25.446 INFO - Selenium Server is up and running
10:25:44.219 INFO - Executing: [new session: Capabilities [{browserName=firefox}]])
10:25:44.229 INFO - Creating a new session for Capabilities [{browserName=firefox}]
10:25:46.212 INFO - Done: [new session: Capabilities [{browserName=firefox}]]
10:25:46.217 INFO - Executing: [get: http://www.drugs.com/imprints.php])
10:25:51.508 INFO - Done: [get: http://www.drugs.com/imprints.php]
10:25:51.511 INFO - Executing: [find element: By.id: livesearch-imprint])
10:25:52.320 ERROR - Unexpected exception during findElement
java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)
    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)
    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)
    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)
    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)
    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)
    at com.google.common.collect.Iterators.addAll(Iterators.java:361)
    at com.google.common.collect.Lists.newArrayList(Lists.java:160)
    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)
    at com.google.common.collect.Iterables.toArray(Iterables.java:315)
    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)
    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)
    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)
    at org.openqa.selenium.By$ById.findElement(By.java:215)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:483)
    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)
    at com.sun.proxy.$Proxy1.findElement(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
    at java.lang.Thread.run(Thread.java:745)
10:25:52.321 WARN - Exception thrown
java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)
    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)
    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)
    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)
    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)
    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)
    at com.google.common.collect.Iterators.addAll(Iterators.java:361)
    at com.google.common.collect.Lists.newArrayList(Lists.java:160)
    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)
    at com.google.common.collect.Iterables.toArray(Iterables.java:315)
    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)
    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)
    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)
    at org.openqa.selenium.By$ById.findElement(By.java:215)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:483)
    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)
    at com.sun.proxy.$Proxy1.findElement(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
    at java.lang.Thread.run(Thread.java:745)
10:25:52.322 WARN - Exception: java.lang.String cannot be cast to java.lang.Number
10:25:52.390 INFO - Executing: [close window])
10:25:52.397 INFO - Done: [close window]
10:31:54.178 INFO - Executing: [new session: Capabilities [{browserName=firefox}]])
10:31:54.179 INFO - Creating a new session for Capabilities [{browserName=firefox}]
10:31:55.723 INFO - Done: [new session: Capabilities [{browserName=firefox}]]
10:31:55.726 INFO - Executing: [get: http://www.drugs.com/imprints.php])
10:32:03.338 INFO - Done: [get: http://www.drugs.com/imprints.php]
10:32:03.340 INFO - Executing: [find element: By.id: livesearch-imprint])
10:32:03.755 ERROR - Unexpected exception during findElement
java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)
    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)
    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)
    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)
    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)
    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)
    at com.google.common.collect.Iterators.addAll(Iterators.java:361)
    at com.google.common.collect.Lists.newArrayList(Lists.java:160)
    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)
    at com.google.common.collect.Iterables.toArray(Iterables.java:315)
    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)
    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)
    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)
    at org.openqa.selenium.By$ById.findElement(By.java:215)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:483)
    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)
    at com.sun.proxy.$Proxy1.findElement(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
    at java.lang.Thread.run(Thread.java:745)
10:32:03.756 WARN - Exception thrown
java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)
    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)
    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)
    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)
    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)
    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)
    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)
    at com.google.common.collect.Iterators.addAll(Iterators.java:361)
    at com.google.common.collect.Lists.newArrayList(Lists.java:160)
    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)
    at com.google.common.collect.Iterables.toArray(Iterables.java:315)
    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)
    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)
    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)
    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)
    at org.openqa.selenium.By$ById.findElement(By.java:215)
    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:483)
    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)
    at com.sun.proxy.$Proxy1.findElement(Unknown Source)
    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)
    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
    at java.lang.Thread.run(Thread.java:745)
10:32:03.756 WARN - Exception: java.lang.String cannot be cast to java.lang.Number
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/817","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/817/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/817/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/817/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/817","id":96433880,"node_id":"MDU6SXNzdWU5NjQzMzg4MA==","number":817,"title":"Unexpected exception during findElement","user":{"login":"NoodlesNZ","id":1063979,"node_id":"MDQ6VXNlcjEwNjM5Nzk=","avatar_url":"https://avatars0.githubusercontent.com/u/1063979?v=4","gravatar_id":"","url":"https://api.github.com/users/NoodlesNZ","html_url":"https://github.com/NoodlesNZ","followers_url":"https://api.github.com/users/NoodlesNZ/followers","following_url":"https://api.github.com/users/NoodlesNZ/following{/other_user}","gists_url":"https://api.github.com/users/NoodlesNZ/gists{/gist_id}","starred_url":"https://api.github.com/users/NoodlesNZ/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/NoodlesNZ/subscriptions","organizations_url":"https://api.github.com/users/NoodlesNZ/orgs","repos_url":"https://api.github.com/users/NoodlesNZ/repos","events_url":"https://api.github.com/users/NoodlesNZ/events{/privacy}","received_events_url":"https://api.github.com/users/NoodlesNZ/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-07-21T22:47:30Z","updated_at":"2019-08-21T08:09:42Z","closed_at":"2015-07-21T23:00:22Z","author_association":"NONE","body":"I'm getting the following error when trying to use findElement via facebook's php webdriver.\nHave replicated it on Mac OSX 10.10.4, Windows 7 and CentOS 6.6\n\n```\n10:25:25.229 INFO - Launching a standalone Selenium Server\n10:25:25.272 INFO - Java: Oracle Corporation 25.20-b23\n10:25:25.273 INFO - OS: Mac OS X 10.10.4 x86_64\n10:25:25.284 INFO - v2.46.0, with Core v2.46.0. Built from revision 87c69e2\n10:25:25.366 INFO - Driver provider org.openqa.selenium.ie.InternetExplorerDriver registration is skipped:\nregistration capabilities Capabilities [{ensureCleanSession=true, browserName=internet explorer, version=, platform=WINDOWS}] does not match the current platform MAC\n10:25:25.366 INFO - Driver class not found: com.opera.core.systems.OperaDriver\n10:25:25.367 INFO - Driver provider com.opera.core.systems.OperaDriver is not registered\n10:25:25.446 INFO - RemoteWebDriver instances should connect to: http://127.0.0.1:4444/wd/hub\n10:25:25.446 INFO - Selenium Server is up and running\n10:25:44.219 INFO - Executing: [new session: Capabilities [{browserName=firefox}]])\n10:25:44.229 INFO - Creating a new session for Capabilities [{browserName=firefox}]\n10:25:46.212 INFO - Done: [new session: Capabilities [{browserName=firefox}]]\n10:25:46.217 INFO - Executing: [get: http://www.drugs.com/imprints.php])\n10:25:51.508 INFO - Done: [get: http://www.drugs.com/imprints.php]\n10:25:51.511 INFO - Executing: [find element: By.id: livesearch-imprint])\n10:25:52.320 ERROR - Unexpected exception during findElement\njava.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)\n    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)\n    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)\n    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)\n    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)\n    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)\n    at com.google.common.collect.Iterators.addAll(Iterators.java:361)\n    at com.google.common.collect.Lists.newArrayList(Lists.java:160)\n    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)\n    at com.google.common.collect.Iterables.toArray(Iterables.java:315)\n    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)\n    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)\n    at org.openqa.selenium.By$ById.findElement(By.java:215)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:483)\n    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)\n    at com.sun.proxy.$Proxy1.findElement(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)\n    at java.util.concurrent.FutureTask.run(FutureTask.java:266)\n    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\n    at java.lang.Thread.run(Thread.java:745)\n10:25:52.321 WARN - Exception thrown\njava.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)\n    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)\n    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)\n    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)\n    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)\n    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)\n    at com.google.common.collect.Iterators.addAll(Iterators.java:361)\n    at com.google.common.collect.Lists.newArrayList(Lists.java:160)\n    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)\n    at com.google.common.collect.Iterables.toArray(Iterables.java:315)\n    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)\n    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)\n    at org.openqa.selenium.By$ById.findElement(By.java:215)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:483)\n    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)\n    at com.sun.proxy.$Proxy1.findElement(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)\n    at java.util.concurrent.FutureTask.run(FutureTask.java:266)\n    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\n    at java.lang.Thread.run(Thread.java:745)\n10:25:52.322 WARN - Exception: java.lang.String cannot be cast to java.lang.Number\n10:25:52.390 INFO - Executing: [close window])\n10:25:52.397 INFO - Done: [close window]\n10:31:54.178 INFO - Executing: [new session: Capabilities [{browserName=firefox}]])\n10:31:54.179 INFO - Creating a new session for Capabilities [{browserName=firefox}]\n10:31:55.723 INFO - Done: [new session: Capabilities [{browserName=firefox}]]\n10:31:55.726 INFO - Executing: [get: http://www.drugs.com/imprints.php])\n10:32:03.338 INFO - Done: [get: http://www.drugs.com/imprints.php]\n10:32:03.340 INFO - Executing: [find element: By.id: livesearch-imprint])\n10:32:03.755 ERROR - Unexpected exception during findElement\njava.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)\n    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)\n    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)\n    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)\n    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)\n    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)\n    at com.google.common.collect.Iterators.addAll(Iterators.java:361)\n    at com.google.common.collect.Lists.newArrayList(Lists.java:160)\n    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)\n    at com.google.common.collect.Iterables.toArray(Iterables.java:315)\n    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)\n    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)\n    at org.openqa.selenium.By$ById.findElement(By.java:215)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:483)\n    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)\n    at com.sun.proxy.$Proxy1.findElement(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)\n    at java.util.concurrent.FutureTask.run(FutureTask.java:266)\n    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\n    at java.lang.Thread.run(Thread.java:745)\n10:32:03.756 WARN - Exception thrown\njava.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Number\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:297)\n    at org.openqa.selenium.remote.ErrorHandler$FrameInfoToStackFrame.apply(ErrorHandler.java:1)\n    at com.google.common.collect.Iterators$8.transform(Iterators.java:799)\n    at com.google.common.collect.TransformedIterator.next(TransformedIterator.java:48)\n    at com.google.common.collect.Iterators$7.computeNext(Iterators.java:651)\n    at com.google.common.collect.AbstractIterator.tryToComputeNext(AbstractIterator.java:143)\n    at com.google.common.collect.AbstractIterator.hasNext(AbstractIterator.java:138)\n    at com.google.common.collect.Iterators.addAll(Iterators.java:361)\n    at com.google.common.collect.Lists.newArrayList(Lists.java:160)\n    at com.google.common.collect.Iterables.toCollection(Iterables.java:337)\n    at com.google.common.collect.Iterables.toArray(Iterables.java:315)\n    at org.openqa.selenium.remote.ErrorHandler.rebuildServerError(ErrorHandler.java:270)\n    at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:118)\n    at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:605)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:358)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:399)\n    at org.openqa.selenium.By$ById.findElement(By.java:215)\n    at org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:350)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:483)\n    at org.openqa.selenium.support.events.EventFiringWebDriver$2.invoke(EventFiringWebDriver.java:102)\n    at com.sun.proxy.$Proxy1.findElement(Unknown Source)\n    at org.openqa.selenium.support.events.EventFiringWebDriver.findElement(EventFiringWebDriver.java:185)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:48)\n    at org.openqa.selenium.remote.server.handler.FindElement.call(FindElement.java:1)\n    at java.util.concurrent.FutureTask.run(FutureTask.java:266)\n    at org.openqa.selenium.remote.server.DefaultSession$1.run(DefaultSession.java:176)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\n    at java.lang.Thread.run(Thread.java:745)\n10:32:03.756 WARN - Exception: java.lang.String cannot be cast to java.lang.Number\n```\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["123503453"], "labels":[]}