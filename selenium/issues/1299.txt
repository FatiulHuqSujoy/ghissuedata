{"id":"1299", "title":"Incorrect handling of \"port 0\" in Selenium Grid setup", "body":"###### Background:
- Under Linux and Windows, bind()-ing to network port 0 assigns the application a free port from the dynamic port range.
- I opened a similar ticket (#1292) but it was rejected because I only referred to deprecated RC tests, but the described behaviour also affects WebDriver tests.
###### Test setup:
- Selenium server standalone version 2.48.2
- CentOS 7
- Java 1.7.0_91
###### Issue description:
- In a Selenium Grid setup, nodes launched using option `-port 0` will correctly listen to a random free port assigned by the OS but will wrongly register on the hub using **port 0** instead of the effective port they are listening on.
###### Example:

While having a hub listening on port 4444, launch a node with a `nodeconfig.json` similar to:

```
{
  \"capabilities\":
      [
        {
          \"browserName\": \"firefox\" ,
          \"maxInstances\": 2,
          \"seleniumProtocol\": \"WebDriver\"
        }
      ],
  \"configuration\":
  {
    \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",
    \"maxSession\": 2,
    \"port\": 0,
    \"host\": 127.0.0.1,
    \"register\": true,
    \"registerCycle\": 5000,
    \"hubPort\": 4444,
    \"hubHost\": 127.0.0.1
  }
}
```

`lsof` will show that the node process listens on a random port offered by the OS, but the Grid console will show the node as being registered with **port 0** instead of the effective listening port:

```
nodeConfig:nodeconfig3.json
port:0
hubConfig:/opt/selenium/hubconfig.json
servlets:[]
host:127.0.0.1
cleanUpCycle:5000
browserTimeout:0
hubHost:127.0.0.1
registerCycle:5000
capabilityMatcher:org.openqa.grid.internal.utils.DefaultCapabilityMatcher
newSessionWaitTimeout:-1
url:http://127.0.0.1:0
remoteHost:http://127.0.0.1:0
prioritizer:null
register:true
throwOnCapabilityNotPresent:true
nodePolling:5000
proxy:org.openqa.grid.selenium.proxy.DefaultRemoteProxy
maxSession:2
role:node
jettyMaxThreads:-1
hubPort:4444
timeout:300000 
```
- No submitted Webdriver, RC, etc. tests will be able to run on this wrongly registered node.

Also this incorrect behaviour seems to affect all the possible use cases of Selenium (see also the closed ticket #1292 )

Kind regards,

Volker
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1299","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1299/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1299/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1299/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1299","id":118196407,"node_id":"MDU6SXNzdWUxMTgxOTY0MDc=","number":1299,"title":"Incorrect handling of \"port 0\" in Selenium Grid setup","user":{"login":"vflegel-sib","id":15673079,"node_id":"MDQ6VXNlcjE1NjczMDc5","avatar_url":"https://avatars1.githubusercontent.com/u/15673079?v=4","gravatar_id":"","url":"https://api.github.com/users/vflegel-sib","html_url":"https://github.com/vflegel-sib","followers_url":"https://api.github.com/users/vflegel-sib/followers","following_url":"https://api.github.com/users/vflegel-sib/following{/other_user}","gists_url":"https://api.github.com/users/vflegel-sib/gists{/gist_id}","starred_url":"https://api.github.com/users/vflegel-sib/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/vflegel-sib/subscriptions","organizations_url":"https://api.github.com/users/vflegel-sib/orgs","repos_url":"https://api.github.com/users/vflegel-sib/repos","events_url":"https://api.github.com/users/vflegel-sib/events{/privacy}","received_events_url":"https://api.github.com/users/vflegel-sib/received_events","type":"User","site_admin":false},"labels":[{"id":182484652,"node_id":"MDU6TGFiZWwxODI0ODQ2NTI=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-grid","name":"C-grid","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2015-11-21T13:21:42Z","updated_at":"2019-08-20T11:09:37Z","closed_at":"2016-03-15T16:12:13Z","author_association":"NONE","body":"###### Background:\n- Under Linux and Windows, bind()-ing to network port 0 assigns the application a free port from the dynamic port range.\n- I opened a similar ticket (#1292) but it was rejected because I only referred to deprecated RC tests, but the described behaviour also affects WebDriver tests.\n###### Test setup:\n- Selenium server standalone version 2.48.2\n- CentOS 7\n- Java 1.7.0_91\n###### Issue description:\n- In a Selenium Grid setup, nodes launched using option `-port 0` will correctly listen to a random free port assigned by the OS but will wrongly register on the hub using **port 0** instead of the effective port they are listening on.\n###### Example:\n\nWhile having a hub listening on port 4444, launch a node with a `nodeconfig.json` similar to:\n\n```\n{\n  \"capabilities\":\n      [\n        {\n          \"browserName\": \"firefox\" ,\n          \"maxInstances\": 2,\n          \"seleniumProtocol\": \"WebDriver\"\n        }\n      ],\n  \"configuration\":\n  {\n    \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",\n    \"maxSession\": 2,\n    \"port\": 0,\n    \"host\": 127.0.0.1,\n    \"register\": true,\n    \"registerCycle\": 5000,\n    \"hubPort\": 4444,\n    \"hubHost\": 127.0.0.1\n  }\n}\n```\n\n`lsof` will show that the node process listens on a random port offered by the OS, but the Grid console will show the node as being registered with **port 0** instead of the effective listening port:\n\n```\nnodeConfig:nodeconfig3.json\nport:0\nhubConfig:/opt/selenium/hubconfig.json\nservlets:[]\nhost:127.0.0.1\ncleanUpCycle:5000\nbrowserTimeout:0\nhubHost:127.0.0.1\nregisterCycle:5000\ncapabilityMatcher:org.openqa.grid.internal.utils.DefaultCapabilityMatcher\nnewSessionWaitTimeout:-1\nurl:http://127.0.0.1:0\nremoteHost:http://127.0.0.1:0\nprioritizer:null\nregister:true\nthrowOnCapabilityNotPresent:true\nnodePolling:5000\nproxy:org.openqa.grid.selenium.proxy.DefaultRemoteProxy\nmaxSession:2\nrole:node\njettyMaxThreads:-1\nhubPort:4444\ntimeout:300000 \n```\n- No submitted Webdriver, RC, etc. tests will be able to run on this wrongly registered node.\n\nAlso this incorrect behaviour seems to affect all the possible use cases of Selenium (see also the closed ticket #1292 )\n\nKind regards,\n\nVolker\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["C-grid"]}