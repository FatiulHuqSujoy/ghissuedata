{"id":"7674", "title":"IOError: [Errno 2] No such file or directory: '/usr/lib64/python2.7/site-packages/selenium-4.0.0a3-py2.7.egg/selenium/webdriver/remote/getAttribute.js'", "body":"## 🐛 Bug Report

A clear and concise description of what the bug is.

<!-- NOTE
FIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://chromedriver.chromium.org/help

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

-->

## To Reproduce

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

Detailed steps to reproduce the behavior:

## Expected behavior

A clear and concise description of what you expected to happen.

## Test script or set of commands reproducing this issue

Please provide a test script to reproduce the issue you are reporting, if the 
setup is more complex, GitHub repo links with are also OK.

Issues without a reproduction script are likely to stall and eventually be closed.

## Environment

OS: <!-- Windows 10? OSX? -->
Browser: <!-- Chrome? Safari?  -->
Browser version: <!-- e.g.: 70.0.3538.110 -->
Browser Driver version: <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->
Language Bindings version: <!-- e.g.: Java 3.141.0 --> 
Selenium Grid version (if applicable): <!-- e.g.: 3.141.59 --> 
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7674","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7674/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7674/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7674/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7674","id":505624011,"node_id":"MDU6SXNzdWU1MDU2MjQwMTE=","number":7674,"title":"IOError: [Errno 2] No such file or directory: '/usr/lib64/python2.7/site-packages/selenium-4.0.0a3-py2.7.egg/selenium/webdriver/remote/getAttribute.js'","user":{"login":"Nachtel","id":56330106,"node_id":"MDQ6VXNlcjU2MzMwMTA2","avatar_url":"https://avatars1.githubusercontent.com/u/56330106?v=4","gravatar_id":"","url":"https://api.github.com/users/Nachtel","html_url":"https://github.com/Nachtel","followers_url":"https://api.github.com/users/Nachtel/followers","following_url":"https://api.github.com/users/Nachtel/following{/other_user}","gists_url":"https://api.github.com/users/Nachtel/gists{/gist_id}","starred_url":"https://api.github.com/users/Nachtel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Nachtel/subscriptions","organizations_url":"https://api.github.com/users/Nachtel/orgs","repos_url":"https://api.github.com/users/Nachtel/repos","events_url":"https://api.github.com/users/Nachtel/events{/privacy}","received_events_url":"https://api.github.com/users/Nachtel/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-10-11T04:01:54Z","updated_at":"2019-11-10T08:07:55Z","closed_at":"2019-10-11T07:15:07Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nA clear and concise description of what the bug is.\r\n\r\n<!-- NOTE\r\nFIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://chromedriver.chromium.org/help\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\n-->\r\n\r\n## To Reproduce\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\nDetailed steps to reproduce the behavior:\r\n\r\n## Expected behavior\r\n\r\nA clear and concise description of what you expected to happen.\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\nPlease provide a test script to reproduce the issue you are reporting, if the \r\nsetup is more complex, GitHub repo links with are also OK.\r\n\r\nIssues without a reproduction script are likely to stall and eventually be closed.\r\n\r\n## Environment\r\n\r\nOS: <!-- Windows 10? OSX? -->\r\nBrowser: <!-- Chrome? Safari?  -->\r\nBrowser version: <!-- e.g.: 70.0.3538.110 -->\r\nBrowser Driver version: <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->\r\nLanguage Bindings version: <!-- e.g.: Java 3.141.0 --> \r\nSelenium Grid version (if applicable): <!-- e.g.: 3.141.59 --> \r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["552172734"], "labels":[]}