{"id":"6439", "title":"Selenium 2.9 record button is not working", "body":"## Meta -
OS:  Windows 10
<!-- Windows 10? OSX? -->
Selenium Version:   2.9.1
<!-- 2.52.0, IDE, etc -->
Browser:  Firefox
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) --> 56.0b10

## Expected Behavior - Record button should record test case.

## Actual Behavior - Record button is not record the test case.

## Steps to reproduce -
![record issue](https://user-images.githubusercontent.com/43517807/45928816-5f86b300-bf67-11e8-8d1a-4efb300c367b.jpg)

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6439","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6439/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6439/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6439/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6439","id":362945055,"node_id":"MDU6SXNzdWUzNjI5NDUwNTU=","number":6439,"title":"Selenium 2.9 record button is not working","user":{"login":"Ranj1912","id":43517807,"node_id":"MDQ6VXNlcjQzNTE3ODA3","avatar_url":"https://avatars3.githubusercontent.com/u/43517807?v=4","gravatar_id":"","url":"https://api.github.com/users/Ranj1912","html_url":"https://github.com/Ranj1912","followers_url":"https://api.github.com/users/Ranj1912/followers","following_url":"https://api.github.com/users/Ranj1912/following{/other_user}","gists_url":"https://api.github.com/users/Ranj1912/gists{/gist_id}","starred_url":"https://api.github.com/users/Ranj1912/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Ranj1912/subscriptions","organizations_url":"https://api.github.com/users/Ranj1912/orgs","repos_url":"https://api.github.com/users/Ranj1912/repos","events_url":"https://api.github.com/users/Ranj1912/events{/privacy}","received_events_url":"https://api.github.com/users/Ranj1912/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-09-23T14:02:18Z","updated_at":"2019-08-15T10:09:53Z","closed_at":"2018-09-23T15:04:24Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:   2.9.1\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Firefox\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) --> 56.0b10\r\n\r\n## Expected Behavior - Record button should record test case.\r\n\r\n## Actual Behavior - Record button is not record the test case.\r\n\r\n## Steps to reproduce -\r\n![record issue](https://user-images.githubusercontent.com/43517807/45928816-5f86b300-bf67-11e8-8d1a-4efb300c367b.jpg)\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"cgoldberg","id":1113081,"node_id":"MDQ6VXNlcjExMTMwODE=","avatar_url":"https://avatars0.githubusercontent.com/u/1113081?v=4","gravatar_id":"","url":"https://api.github.com/users/cgoldberg","html_url":"https://github.com/cgoldberg","followers_url":"https://api.github.com/users/cgoldberg/followers","following_url":"https://api.github.com/users/cgoldberg/following{/other_user}","gists_url":"https://api.github.com/users/cgoldberg/gists{/gist_id}","starred_url":"https://api.github.com/users/cgoldberg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cgoldberg/subscriptions","organizations_url":"https://api.github.com/users/cgoldberg/orgs","repos_url":"https://api.github.com/users/cgoldberg/repos","events_url":"https://api.github.com/users/cgoldberg/events{/privacy}","received_events_url":"https://api.github.com/users/cgoldberg/received_events","type":"User","site_admin":false}}", "commentIds":["423822993"], "labels":[]}