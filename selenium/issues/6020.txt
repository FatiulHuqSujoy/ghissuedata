{"id":"6020", "title":"Selenium IDE - Cannot clone/duplicate tests. Instructions in #6276 don't work.", "body":"## Meta -
OS: Windows 10
Selenium Version:  IDE 3.0.3

Browser:  
Chrome 67.0.3396.87 (Official Build) (64-bit)
Firefox 60.0.2 (64bit)

## Expected Behavior -
Objective: Duplicate a test case and alter a few params. Issue 6276 from archive (https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/6276#issuecomment-192136639) states that we should either:

1. Select all the commands in the test case and copying them (ctrl-C or Command-C or Edit->Copy)
and pasting them in a new test case!
**Expected:** IDE would allow you to *Select all the commands in the test case*

or 

2. You can also go to the \"source\" view and copying all the text there and paste it in
a new one. 
**Expected:** *Source View* to exist or, when loading a .side file, IDE would warn you the IDs are duplicated and *offer the option* to fix that (assigning new IDs to all elements)

## Actual Behavior -
1. You cannot select more than one command (test step).
and
2. When you copy and paste JSON elements, the IDs are duplicated. IDE stops working correctly. Displays all tests selected. Changes in one test is replicated to all other tests, etc

## Steps to reproduce -
### For case 1
1. Open Selenium IDE
2. Create 3 test steps (content doesnt matter)
3. Copy all commands - **ISSUE* You cannot select more than one command.

### For case 2
1. Open Selenium IDE
2. Create 3 test steps (content doesnt matter)
3. Save project to a .side file
4. Open the file in a text (JSON) editor
5. Duplicate a Test
6. Save file
7. Load on IDE - **ISSUE** IDE stops working correctly. Displays all tests selected. Changes in one test is replicated to all other tests, etc
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6020","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6020/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6020/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6020/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6020","id":332377112,"node_id":"MDU6SXNzdWUzMzIzNzcxMTI=","number":6020,"title":"Selenium IDE - Cannot clone/duplicate tests. Instructions in #6276 don't work.","user":{"login":"lucianobargmann","id":495852,"node_id":"MDQ6VXNlcjQ5NTg1Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/495852?v=4","gravatar_id":"","url":"https://api.github.com/users/lucianobargmann","html_url":"https://github.com/lucianobargmann","followers_url":"https://api.github.com/users/lucianobargmann/followers","following_url":"https://api.github.com/users/lucianobargmann/following{/other_user}","gists_url":"https://api.github.com/users/lucianobargmann/gists{/gist_id}","starred_url":"https://api.github.com/users/lucianobargmann/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lucianobargmann/subscriptions","organizations_url":"https://api.github.com/users/lucianobargmann/orgs","repos_url":"https://api.github.com/users/lucianobargmann/repos","events_url":"https://api.github.com/users/lucianobargmann/events{/privacy}","received_events_url":"https://api.github.com/users/lucianobargmann/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-06-14T12:18:51Z","updated_at":"2019-08-15T22:09:54Z","closed_at":"2018-06-14T14:05:31Z","author_association":"NONE","body":"## Meta -\r\nOS: Windows 10\r\nSelenium Version:  IDE 3.0.3\r\n\r\nBrowser:  \r\nChrome 67.0.3396.87 (Official Build) (64-bit)\r\nFirefox 60.0.2 (64bit)\r\n\r\n## Expected Behavior -\r\nObjective: Duplicate a test case and alter a few params. Issue 6276 from archive (https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/6276#issuecomment-192136639) states that we should either:\r\n\r\n1. Select all the commands in the test case and copying them (ctrl-C or Command-C or Edit->Copy)\r\nand pasting them in a new test case!\r\n**Expected:** IDE would allow you to *Select all the commands in the test case*\r\n\r\nor \r\n\r\n2. You can also go to the \"source\" view and copying all the text there and paste it in\r\na new one. \r\n**Expected:** *Source View* to exist or, when loading a .side file, IDE would warn you the IDs are duplicated and *offer the option* to fix that (assigning new IDs to all elements)\r\n\r\n## Actual Behavior -\r\n1. You cannot select more than one command (test step).\r\nand\r\n2. When you copy and paste JSON elements, the IDs are duplicated. IDE stops working correctly. Displays all tests selected. Changes in one test is replicated to all other tests, etc\r\n\r\n## Steps to reproduce -\r\n### For case 1\r\n1. Open Selenium IDE\r\n2. Create 3 test steps (content doesnt matter)\r\n3. Copy all commands - **ISSUE* You cannot select more than one command.\r\n\r\n### For case 2\r\n1. Open Selenium IDE\r\n2. Create 3 test steps (content doesnt matter)\r\n3. Save project to a .side file\r\n4. Open the file in a text (JSON) editor\r\n5. Duplicate a Test\r\n6. Save file\r\n7. Load on IDE - **ISSUE** IDE stops working correctly. Displays all tests selected. Changes in one test is replicated to all other tests, etc\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["397307994"], "labels":[]}