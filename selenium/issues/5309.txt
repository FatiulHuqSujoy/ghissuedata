{"id":"5309", "title":"Parameter 'registryClass' in DefaultHub.json does not mapped to field 'registry' ", "body":"Expected:
  User should be able to use DefaultHub.json as reference implementation. 
Actual:
  parameter 'registryClass' is not mapped to 'registry' field in `GridHubConfiguration`

This issue was introduced from the beginning in 6a19886. So no need to preserve both parameter names. Just need to fix parameter name in json file.
**GridHubConfiguration.java:**
```java
@Expose
@Parameter(
      names = \"-registry\",
      description = \"<String> class name : a class implementing the GridRegistry interface. Specifies the registry the hub will use.\"
  )
  public String registry = DEFAULT_HUB_REGISTRY_CLASS;
```
**DefaultHub.json:**
```json
{
  \"registryClass\": \"org.openqa.grid.internal.DefaultGridRegistry\",
}
```


", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5309","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5309/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5309/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5309/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5309","id":287171353,"node_id":"MDU6SXNzdWUyODcxNzEzNTM=","number":5309,"title":"Parameter 'registryClass' in DefaultHub.json does not mapped to field 'registry' ","user":{"login":"kool79","id":4625317,"node_id":"MDQ6VXNlcjQ2MjUzMTc=","avatar_url":"https://avatars0.githubusercontent.com/u/4625317?v=4","gravatar_id":"","url":"https://api.github.com/users/kool79","html_url":"https://github.com/kool79","followers_url":"https://api.github.com/users/kool79/followers","following_url":"https://api.github.com/users/kool79/following{/other_user}","gists_url":"https://api.github.com/users/kool79/gists{/gist_id}","starred_url":"https://api.github.com/users/kool79/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kool79/subscriptions","organizations_url":"https://api.github.com/users/kool79/orgs","repos_url":"https://api.github.com/users/kool79/repos","events_url":"https://api.github.com/users/kool79/events{/privacy}","received_events_url":"https://api.github.com/users/kool79/received_events","type":"User","site_admin":false},"labels":[{"id":182484652,"node_id":"MDU6TGFiZWwxODI0ODQ2NTI=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-grid","name":"C-grid","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2018-01-09T17:36:50Z","updated_at":"2019-08-16T18:09:35Z","closed_at":"2018-01-11T06:24:47Z","author_association":"NONE","body":"Expected:\r\n  User should be able to use DefaultHub.json as reference implementation. \r\nActual:\r\n  parameter 'registryClass' is not mapped to 'registry' field in `GridHubConfiguration`\r\n\r\nThis issue was introduced from the beginning in 6a19886. So no need to preserve both parameter names. Just need to fix parameter name in json file.\r\n**GridHubConfiguration.java:**\r\n```java\r\n@Expose\r\n@Parameter(\r\n      names = \"-registry\",\r\n      description = \"<String> class name : a class implementing the GridRegistry interface. Specifies the registry the hub will use.\"\r\n  )\r\n  public String registry = DEFAULT_HUB_REGISTRY_CLASS;\r\n```\r\n**DefaultHub.json:**\r\n```json\r\n{\r\n  \"registryClass\": \"org.openqa.grid.internal.DefaultGridRegistry\",\r\n}\r\n```\r\n\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["C-grid"]}