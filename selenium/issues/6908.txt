{"id":"6908", "title":"Browser test fails because of cygwin internal path", "body":"## 🐛 Bug Report: Browser test fails because of cygwin internal path

When trying to run a browser test on firefox written in ruby (capybara, selenium webdriver), I get the following error:
```
Failures:

  1) logging in is possible
     Failure/Error: super(url)

     Selenium::WebDriver::Error::SessionNotCreatedError:
       Failed to start browser /firefox/firefox.exe: no such file or directory
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/response.rb:69:in `assert_ok'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/response.rb:32:in `initialize'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:81:in `new'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:81:in `create_response'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/default.rb:104:in `request'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:59:in `call'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/bridge.rb:164:in `execute'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/bridge.rb:97:in `create_session'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/marionette/driver.rb:50:in `initialize'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/driver.rb:31:in `new'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/driver.rb:31:in `new'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/common/driver.rb:52:in `for'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver.rb:85:in `for'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/selenium/driver.rb:24:in `browser'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/selenium/driver.rb:50:in `visit'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/session.rb:274:in `visit'
     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/dsl.rb:50:in `block (2 levels) in <module:DSL>'
```

The problem seems to be, that when selenium is trying to locate firefox.exe, it is using the cygwin relative path (/firefox/firefox.exe) instead of the absolute windows path (C:/cygwin64/firefox/firefox.exe). I'm assuming this would cause a problem with other browsers as well.  
Since the code will not run firefox inside of the cygwin platform itself, but on windows, it needs to get the windows absolute path to kick off the browser.

#### Proposed solution: 
https://github.com/SeleniumHQ/selenium/blob/master/rb/lib/selenium/webdriver/common/platform.rb line 124 in function `cygwin_path` `
```
`cygpath #{flags.join ' '} \"#{path}\"`.strip
```
be changed to 
```
`cygpath -d #{flags.join ' '} \"#{path}\"\\`.strip
```

## To Reproduce

To reproduce, execute any browser test that is written in ruby and powered by selenium from the cygwin console. Test should fail with  
```
Selenium::WebDriver::Error::SessionNotCreatedError:
       Failed to start browser <browser cygwin path>: no such file or directory
```

Detailed steps to reproduce the behavior:

## Expected behavior

Expecting the browser to open when running a browser test.

## Test script or set of commands reproducing this issue

Sadly, I'm working with company code, so I can't paste examples

## Environment

OS: Windows 10
Browser: Mozilla Firefox
Browser version: 65.0 (64bit)
Browser Driver version:  GeckoDriver 0.24
Language Bindings version: ruby 2.5.3p105 (2018-10-18 revision 65156) [x86_64-cygwin]
Selenium Grid version (if applicable): -
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6908","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6908/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6908/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6908/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6908","id":405752296,"node_id":"MDU6SXNzdWU0MDU3NTIyOTY=","number":6908,"title":"Browser test fails because of cygwin internal path","user":{"login":"Grandmagic13","id":21217495,"node_id":"MDQ6VXNlcjIxMjE3NDk1","avatar_url":"https://avatars3.githubusercontent.com/u/21217495?v=4","gravatar_id":"","url":"https://api.github.com/users/Grandmagic13","html_url":"https://github.com/Grandmagic13","followers_url":"https://api.github.com/users/Grandmagic13/followers","following_url":"https://api.github.com/users/Grandmagic13/following{/other_user}","gists_url":"https://api.github.com/users/Grandmagic13/gists{/gist_id}","starred_url":"https://api.github.com/users/Grandmagic13/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Grandmagic13/subscriptions","organizations_url":"https://api.github.com/users/Grandmagic13/orgs","repos_url":"https://api.github.com/users/Grandmagic13/repos","events_url":"https://api.github.com/users/Grandmagic13/events{/privacy}","received_events_url":"https://api.github.com/users/Grandmagic13/received_events","type":"User","site_admin":false},"labels":[{"id":182503883,"node_id":"MDU6TGFiZWwxODI1MDM4ODM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-rb","name":"C-rb","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2019-02-01T15:35:59Z","updated_at":"2019-08-14T20:09:45Z","closed_at":"2019-03-08T02:45:30Z","author_association":"NONE","body":"## 🐛 Bug Report: Browser test fails because of cygwin internal path\r\n\r\nWhen trying to run a browser test on firefox written in ruby (capybara, selenium webdriver), I get the following error:\r\n```\r\nFailures:\r\n\r\n  1) logging in is possible\r\n     Failure/Error: super(url)\r\n\r\n     Selenium::WebDriver::Error::SessionNotCreatedError:\r\n       Failed to start browser /firefox/firefox.exe: no such file or directory\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/response.rb:69:in `assert_ok'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/response.rb:32:in `initialize'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:81:in `new'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:81:in `create_response'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/default.rb:104:in `request'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/http/common.rb:59:in `call'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/bridge.rb:164:in `execute'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/remote/bridge.rb:97:in `create_session'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/marionette/driver.rb:50:in `initialize'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/driver.rb:31:in `new'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/firefox/driver.rb:31:in `new'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver/common/driver.rb:52:in `for'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/selenium-webdriver-3.11.0/lib/selenium/webdriver.rb:85:in `for'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/selenium/driver.rb:24:in `browser'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/selenium/driver.rb:50:in `visit'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/session.rb:274:in `visit'\r\n     # /home/bjanoska/.rvm/gems/ruby-2.5.3/gems/capybara-2.18.0/lib/capybara/dsl.rb:50:in `block (2 levels) in <module:DSL>'\r\n```\r\n\r\nThe problem seems to be, that when selenium is trying to locate firefox.exe, it is using the cygwin relative path (/firefox/firefox.exe) instead of the absolute windows path (C:/cygwin64/firefox/firefox.exe). I'm assuming this would cause a problem with other browsers as well.  \r\nSince the code will not run firefox inside of the cygwin platform itself, but on windows, it needs to get the windows absolute path to kick off the browser.\r\n\r\n#### Proposed solution: \r\nhttps://github.com/SeleniumHQ/selenium/blob/master/rb/lib/selenium/webdriver/common/platform.rb line 124 in function `cygwin_path` `\r\n```\r\n`cygpath #{flags.join ' '} \"#{path}\"`.strip\r\n```\r\nbe changed to \r\n```\r\n`cygpath -d #{flags.join ' '} \"#{path}\"\\`.strip\r\n```\r\n\r\n## To Reproduce\r\n\r\nTo reproduce, execute any browser test that is written in ruby and powered by selenium from the cygwin console. Test should fail with  \r\n```\r\nSelenium::WebDriver::Error::SessionNotCreatedError:\r\n       Failed to start browser <browser cygwin path>: no such file or directory\r\n```\r\n\r\nDetailed steps to reproduce the behavior:\r\n\r\n## Expected behavior\r\n\r\nExpecting the browser to open when running a browser test.\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\nSadly, I'm working with company code, so I can't paste examples\r\n\r\n## Environment\r\n\r\nOS: Windows 10\r\nBrowser: Mozilla Firefox\r\nBrowser version: 65.0 (64bit)\r\nBrowser Driver version:  GeckoDriver 0.24\r\nLanguage Bindings version: ruby 2.5.3p105 (2018-10-18 revision 65156) [x86_64-cygwin]\r\nSelenium Grid version (if applicable): -\r\n","closed_by":{"login":"twalpole","id":16556,"node_id":"MDQ6VXNlcjE2NTU2","avatar_url":"https://avatars2.githubusercontent.com/u/16556?v=4","gravatar_id":"","url":"https://api.github.com/users/twalpole","html_url":"https://github.com/twalpole","followers_url":"https://api.github.com/users/twalpole/followers","following_url":"https://api.github.com/users/twalpole/following{/other_user}","gists_url":"https://api.github.com/users/twalpole/gists{/gist_id}","starred_url":"https://api.github.com/users/twalpole/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/twalpole/subscriptions","organizations_url":"https://api.github.com/users/twalpole/orgs","repos_url":"https://api.github.com/users/twalpole/repos","events_url":"https://api.github.com/users/twalpole/events{/privacy}","received_events_url":"https://api.github.com/users/twalpole/received_events","type":"User","site_admin":false}}", "commentIds":["466216473","466221420","466225670","466226331","466399428","470783799"], "labels":["C-rb"]}