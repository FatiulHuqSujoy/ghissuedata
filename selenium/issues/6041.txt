{"id":"6041", "title":"Webdriver Exception - Selenium Webdriver Java < Urgent >", "body":"## Meta -
OS:  Windows 10
<!-- Windows 10? OSX? -->
Selenium Version:  3.12.0 (latest)
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  67.0.3396.87(64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
It supposed to keep running as usual.

## Actual Behavior -
When I run my automation it gets stuck and stops working on random parts of the code, and sends back webdriver exception.
Checked if the element has changed and it didn't. I'm trying to find it using xpath but also tried id, class name, link text, and tag name....
Now I'm losing my mind because of it, I just can't understand why it happens it doesn't make any sense.
Just now I tried it twice and it stopped in two different locations.
I can't send a link to the page but, if anyone could help me it would be great!

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6041","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6041/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6041/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6041/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6041","id":333570000,"node_id":"MDU6SXNzdWUzMzM1NzAwMDA=","number":6041,"title":"Webdriver Exception - Selenium Webdriver Java < Urgent >","user":{"login":"YakirSA","id":40040604,"node_id":"MDQ6VXNlcjQwMDQwNjA0","avatar_url":"https://avatars1.githubusercontent.com/u/40040604?v=4","gravatar_id":"","url":"https://api.github.com/users/YakirSA","html_url":"https://github.com/YakirSA","followers_url":"https://api.github.com/users/YakirSA/followers","following_url":"https://api.github.com/users/YakirSA/following{/other_user}","gists_url":"https://api.github.com/users/YakirSA/gists{/gist_id}","starred_url":"https://api.github.com/users/YakirSA/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/YakirSA/subscriptions","organizations_url":"https://api.github.com/users/YakirSA/orgs","repos_url":"https://api.github.com/users/YakirSA/repos","events_url":"https://api.github.com/users/YakirSA/events{/privacy}","received_events_url":"https://api.github.com/users/YakirSA/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-06-19T08:32:19Z","updated_at":"2019-08-15T22:09:42Z","closed_at":"2018-06-19T09:41:03Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.12.0 (latest)\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  67.0.3396.87(64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nIt supposed to keep running as usual.\r\n\r\n## Actual Behavior -\r\nWhen I run my automation it gets stuck and stops working on random parts of the code, and sends back webdriver exception.\r\nChecked if the element has changed and it didn't. I'm trying to find it using xpath but also tried id, class name, link text, and tag name....\r\nNow I'm losing my mind because of it, I just can't understand why it happens it doesn't make any sense.\r\nJust now I tried it twice and it stopped in two different locations.\r\nI can't send a link to the page but, if anyone could help me it would be great!\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["398339491","398357570","398366636"], "labels":[]}