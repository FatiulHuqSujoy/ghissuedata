{"id":"5793", "title":"\"Maximise browser Window\" keyword not working in chrome.I am using selenium2Library.", "body":"## Meta -
OS:  macOS High Sierra 10.13.4
Selenium Version:  3.11.0, Selenium2library=3.1.1 
Browser:  Chrome
Browser Version:  
Chrome --> 65.0.3325.181

## Expected Behavior -
Chrome browser should maximize to full screen mode.
## Actual Behavior -
Chrome browser does not maximize fully on using \"Maximize Browser Window\". Its not full screen.
## Steps to reproduce -
try Maximize Browser Window command with chrome browser on pycharm. or 
Easiest way to find where the problem is to you to run the following script:
save below steps as test.py script. Run from command line. --> python test.py
driver = webdriver.Chrome()
driver.get('https://github.com/robotframework/SeleniumLibrary')
driver.maximize_window()
time.sleep(5)
driver.quit()

from discussion : https://github.com/robotframework/SeleniumLibrary/issues/123#issuecomment-382142668

-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5793","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5793/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5793/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5793/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5793","id":315295709,"node_id":"MDU6SXNzdWUzMTUyOTU3MDk=","number":5793,"title":"\"Maximise browser Window\" keyword not working in chrome.I am using selenium2Library.","user":{"login":"mithunsureshqa","id":38289752,"node_id":"MDQ6VXNlcjM4Mjg5NzUy","avatar_url":"https://avatars2.githubusercontent.com/u/38289752?v=4","gravatar_id":"","url":"https://api.github.com/users/mithunsureshqa","html_url":"https://github.com/mithunsureshqa","followers_url":"https://api.github.com/users/mithunsureshqa/followers","following_url":"https://api.github.com/users/mithunsureshqa/following{/other_user}","gists_url":"https://api.github.com/users/mithunsureshqa/gists{/gist_id}","starred_url":"https://api.github.com/users/mithunsureshqa/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mithunsureshqa/subscriptions","organizations_url":"https://api.github.com/users/mithunsureshqa/orgs","repos_url":"https://api.github.com/users/mithunsureshqa/repos","events_url":"https://api.github.com/users/mithunsureshqa/events{/privacy}","received_events_url":"https://api.github.com/users/mithunsureshqa/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-04-18T01:22:41Z","updated_at":"2019-08-16T05:09:52Z","closed_at":"2018-04-18T01:59:36Z","author_association":"NONE","body":"## Meta -\r\nOS:  macOS High Sierra 10.13.4\r\nSelenium Version:  3.11.0, Selenium2library=3.1.1 \r\nBrowser:  Chrome\r\nBrowser Version:  \r\nChrome --> 65.0.3325.181\r\n\r\n## Expected Behavior -\r\nChrome browser should maximize to full screen mode.\r\n## Actual Behavior -\r\nChrome browser does not maximize fully on using \"Maximize Browser Window\". Its not full screen.\r\n## Steps to reproduce -\r\ntry Maximize Browser Window command with chrome browser on pycharm. or \r\nEasiest way to find where the problem is to you to run the following script:\r\nsave below steps as test.py script. Run from command line. --> python test.py\r\ndriver = webdriver.Chrome()\r\ndriver.get('https://github.com/robotframework/SeleniumLibrary')\r\ndriver.maximize_window()\r\ntime.sleep(5)\r\ndriver.quit()\r\n\r\nfrom discussion : https://github.com/robotframework/SeleniumLibrary/issues/123#issuecomment-382142668\r\n\r\n-->\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["382221050"], "labels":[]}