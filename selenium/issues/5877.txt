{"id":"5877", "title":"How to set acceptInsecureCerts in Selenium Node json file or in command line.", "body":"## Meta -
OS:                            
**Linux, Ubuntu 16.04**
Selenium Version:    
**3.11.0**
Browser:                    
**Firefox 59.x**

## Expected Behavior -
When i declare `acceptInsecureCerts=True` in node.json (configuration file for Selenium Grid Node) it should be respected.
My node config:
``` 
{
  \"capabilities\":
  [
    {
      \"browserName\": \"chrome\",
      \"maxInstances\": 15,
      \"seleniumProtocol\": \"WebDriver\",
      \"platform\":\"MAC\",
      \"chromeOptions\": {
            \"args\": [
                  \"--headless\",
                  \"--disable-gpu\",
                  \"--window-size=1024x570\",
                  \"--no-sandbox\"]
      }},
      {
      \"browserName\": \"firefox\",
      \"marionette\": true,
      \"version\": \"59\",
      \"platform\": \"LINUX\",
      \"maxInstances\": 1,
      \"seleniumProtocol\": \"WebDriver\",
      \"acceptSslCerts\": true,
      \"javascriptEnabled\": true,
      \"takesScreenshot\": true,
      \"cleanSession\": true,
      \"acceptInsecureCerts\": true,
      \"webdriver.firefox.profile\": \"default\"
    }
  ],
  \"host\": \"<IP>\",
  \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",
  \"maxSession\": 15,
  \"port\": 5511,
  \"register\": true,
  \"registerCycle\": 5000,
  \"hub\": \"<HUB_ADDRESS>\",
  \"nodeStatusCheckTimeout\": 5000,
  \"nodePolling\": 5000,
  \"role\": \"node\",
  \"unregisterIfStillDownAfter\": 60000,
  \"downPollingLimit\": 2,
  \"debug\": false,
  \"servlets\" : [],
  \"withoutServlets\": [],
  \"custom\": {}
}
```
## Actual Behavior -
Default value for `acceptSslCerts` is always `False` until i declare in my test file:
`desired_capabilities={'moz:firefoxOptions': {'args': ['-headless']}, 'platform': 'LINUX', 'browserName': 'firefox', 'version': '59', 'acceptInsecureCerts': True}`.

Maybe is there different solution for my problem (i don't wan't to edit multiple test files to add to `desired_capabilities` `acceptInsecureCerts`)? 

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5877","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5877/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5877/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5877/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5877","id":321748928,"node_id":"MDU6SXNzdWUzMjE3NDg5Mjg=","number":5877,"title":"How to set acceptInsecureCerts in Selenium Node json file or in command line.","user":{"login":"gregall","id":3317402,"node_id":"MDQ6VXNlcjMzMTc0MDI=","avatar_url":"https://avatars0.githubusercontent.com/u/3317402?v=4","gravatar_id":"","url":"https://api.github.com/users/gregall","html_url":"https://github.com/gregall","followers_url":"https://api.github.com/users/gregall/followers","following_url":"https://api.github.com/users/gregall/following{/other_user}","gists_url":"https://api.github.com/users/gregall/gists{/gist_id}","starred_url":"https://api.github.com/users/gregall/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gregall/subscriptions","organizations_url":"https://api.github.com/users/gregall/orgs","repos_url":"https://api.github.com/users/gregall/repos","events_url":"https://api.github.com/users/gregall/events{/privacy}","received_events_url":"https://api.github.com/users/gregall/received_events","type":"User","site_admin":false},"labels":[{"id":182484652,"node_id":"MDU6TGFiZWwxODI0ODQ2NTI=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-grid","name":"C-grid","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-05-09T22:33:12Z","updated_at":"2019-08-15T01:09:52Z","closed_at":"2018-12-22T02:24:38Z","author_association":"NONE","body":"## Meta -\r\nOS:                            \r\n**Linux, Ubuntu 16.04**\r\nSelenium Version:    \r\n**3.11.0**\r\nBrowser:                    \r\n**Firefox 59.x**\r\n\r\n## Expected Behavior -\r\nWhen i declare `acceptInsecureCerts=True` in node.json (configuration file for Selenium Grid Node) it should be respected.\r\nMy node config:\r\n``` \r\n{\r\n  \"capabilities\":\r\n  [\r\n    {\r\n      \"browserName\": \"chrome\",\r\n      \"maxInstances\": 15,\r\n      \"seleniumProtocol\": \"WebDriver\",\r\n      \"platform\":\"MAC\",\r\n      \"chromeOptions\": {\r\n            \"args\": [\r\n                  \"--headless\",\r\n                  \"--disable-gpu\",\r\n                  \"--window-size=1024x570\",\r\n                  \"--no-sandbox\"]\r\n      }},\r\n      {\r\n      \"browserName\": \"firefox\",\r\n      \"marionette\": true,\r\n      \"version\": \"59\",\r\n      \"platform\": \"LINUX\",\r\n      \"maxInstances\": 1,\r\n      \"seleniumProtocol\": \"WebDriver\",\r\n      \"acceptSslCerts\": true,\r\n      \"javascriptEnabled\": true,\r\n      \"takesScreenshot\": true,\r\n      \"cleanSession\": true,\r\n      \"acceptInsecureCerts\": true,\r\n      \"webdriver.firefox.profile\": \"default\"\r\n    }\r\n  ],\r\n  \"host\": \"<IP>\",\r\n  \"proxy\": \"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",\r\n  \"maxSession\": 15,\r\n  \"port\": 5511,\r\n  \"register\": true,\r\n  \"registerCycle\": 5000,\r\n  \"hub\": \"<HUB_ADDRESS>\",\r\n  \"nodeStatusCheckTimeout\": 5000,\r\n  \"nodePolling\": 5000,\r\n  \"role\": \"node\",\r\n  \"unregisterIfStillDownAfter\": 60000,\r\n  \"downPollingLimit\": 2,\r\n  \"debug\": false,\r\n  \"servlets\" : [],\r\n  \"withoutServlets\": [],\r\n  \"custom\": {}\r\n}\r\n```\r\n## Actual Behavior -\r\nDefault value for `acceptSslCerts` is always `False` until i declare in my test file:\r\n`desired_capabilities={'moz:firefoxOptions': {'args': ['-headless']}, 'platform': 'LINUX', 'browserName': 'firefox', 'version': '59', 'acceptInsecureCerts': True}`.\r\n\r\nMaybe is there different solution for my problem (i don't wan't to edit multiple test files to add to `desired_capabilities` `acceptInsecureCerts`)? \r\n\r\n","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["388311517","449280170","449538527"], "labels":["C-grid"]}