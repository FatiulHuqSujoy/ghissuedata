{"id":"5875", "title":"TestNG compilation errors", "body":"## Meta -
OS:  
 Windows 10
Selenium Version:  
3.11
Browser:  
 Firefox

Following code produces compilation errors, can you help eliminate those and get the code to run? (I am new to TestNG)

package TestNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class DemoTestNG {
	public WebDriver driver = new FirefoxDriver();
	String appUrl = &quot;https://accounts.google.com&quot;;
		
	
  @Test
  public void gmailLogin() {
	  
      // launch the firefox browser and open the application url
       driver.get(&quot;https://gmail.com&quot;); 
  }

	// declare and initialize the variable to store the expected title of the webpage.
    String expectedTitle = &quot; Sign in - Google Accounts &quot;;
   
//fetch the title of the web page and save it into a string variable
    String actualTitle = driver.getTitle();
    Assert.assertEquals(expectedTitle,actualTitle);
   
//enter a valid username in the email textbox
    WebElement username = driver.findElement(By.id(&quot;Email&quot;));
    username.clear();
    username.sendKeys(&quot;TestSelenium&quot;);

//enter a valid password in the password textbox
    WebElement password = driver.findElement(By.id(&quot;Passwd&quot;));
    password.clear();
    password.sendKeys(&quot;password123&quot;);
   
//click on the Sign in button
    WebElement SignInButton = driver.findElement(By.id(&quot;signIn&quot;));
    SignInButton.click();
   
//close the web browser
    driver.close();
}
} ", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5875","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5875/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5875/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5875/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5875","id":321617423,"node_id":"MDU6SXNzdWUzMjE2MTc0MjM=","number":5875,"title":"TestNG compilation errors","user":{"login":"SudeeptMohan","id":32921253,"node_id":"MDQ6VXNlcjMyOTIxMjUz","avatar_url":"https://avatars1.githubusercontent.com/u/32921253?v=4","gravatar_id":"","url":"https://api.github.com/users/SudeeptMohan","html_url":"https://github.com/SudeeptMohan","followers_url":"https://api.github.com/users/SudeeptMohan/followers","following_url":"https://api.github.com/users/SudeeptMohan/following{/other_user}","gists_url":"https://api.github.com/users/SudeeptMohan/gists{/gist_id}","starred_url":"https://api.github.com/users/SudeeptMohan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SudeeptMohan/subscriptions","organizations_url":"https://api.github.com/users/SudeeptMohan/orgs","repos_url":"https://api.github.com/users/SudeeptMohan/repos","events_url":"https://api.github.com/users/SudeeptMohan/events{/privacy}","received_events_url":"https://api.github.com/users/SudeeptMohan/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-05-09T15:27:01Z","updated_at":"2019-08-16T02:09:52Z","closed_at":"2018-05-09T15:43:48Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n Windows 10\r\nSelenium Version:  \r\n3.11\r\nBrowser:  \r\n Firefox\r\n\r\nFollowing code produces compilation errors, can you help eliminate those and get the code to run? (I am new to TestNG)\r\n\r\npackage TestNG;\r\n\r\nimport org.openqa.selenium.WebDriver;\r\nimport org.openqa.selenium.firefox.FirefoxDriver;\r\nimport org.testng.annotations.Test;\r\n\r\npublic class DemoTestNG {\r\n\tpublic WebDriver driver = new FirefoxDriver();\r\n\tString appUrl = &quot;https://accounts.google.com&quot;;\r\n\t\t\r\n\t\r\n  @Test\r\n  public void gmailLogin() {\r\n\t  \r\n      // launch the firefox browser and open the application url\r\n       driver.get(&quot;https://gmail.com&quot;); \r\n  }\r\n\r\n\t// declare and initialize the variable to store the expected title of the webpage.\r\n    String expectedTitle = &quot; Sign in - Google Accounts &quot;;\r\n   \r\n//fetch the title of the web page and save it into a string variable\r\n    String actualTitle = driver.getTitle();\r\n    Assert.assertEquals(expectedTitle,actualTitle);\r\n   \r\n//enter a valid username in the email textbox\r\n    WebElement username = driver.findElement(By.id(&quot;Email&quot;));\r\n    username.clear();\r\n    username.sendKeys(&quot;TestSelenium&quot;);\r\n\r\n//enter a valid password in the password textbox\r\n    WebElement password = driver.findElement(By.id(&quot;Passwd&quot;));\r\n    password.clear();\r\n    password.sendKeys(&quot;password123&quot;);\r\n   \r\n//click on the Sign in button\r\n    WebElement SignInButton = driver.findElement(By.id(&quot;signIn&quot;));\r\n    SignInButton.click();\r\n   \r\n//close the web browser\r\n    driver.close();\r\n}\r\n} ","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["387783601"], "labels":[]}