{"id":"586", "title":"InternetExplorerDriver losing WindowHandles after Navigate().GoToUrl()", "body":"### setup
- Windows 8.1 64bit
- IE11(v.11.0.19 KB3049563) 64bit
- Chrome (v.43.0.2357.81m)
- NuGet package Selenium.WebDriver (v.2.45.0)
- IEDriverServer.exe (v.2.45.0 - 64bit downloaded not built locally)
- IEProtectedMode is off
- HKLM/SOFTWARE/Wow6432Node/Microsoft/InternetExplorer/Main/FeatureControl/FEATURE_BFCACHE exists with a iexplore.exe dword = 0
### code

``` C#
static void Main(string[] args)
{
    //var driver = new OpenQA.Selenium.Chrome.ChromeDriver();
    var driver = new OpenQA.Selenium.IE.InternetExplorerDriver();
    try
    {
        driver.Navigate().GoToUrl(\"https://www.google.com/\");
        driver.FindElementByName(\"q\").SendKeys(\"FooBar\");
    }
    catch(Exception ex)
    {
        Console.WriteLine($\"Exception: {ex.Message}\");
    }
    finally
    {
        Console.ReadLine();
        driver.Quit();
    }
}
```
### problem

When the code reaches line 8 (driver.FindElementByName) I get a \"NoSuchElementException\" thrown, with the message \"Unable to find element with the name == q\". 
### investigation

Looking at driver.WindowHandles before line 8 the count is one, after line 8 the count is 0. It appears that during the navigation the handles are lost.
### what I've tried
- Adding a Delay (System.Threading.Tasks.Task.Delay(1000).Wait()) between the navigation and findelement in case the page hadn't loaded properly in time. However the resulted in a different exception \"NoSuchWindowException\" with the message \"Unable to find element on closed window\".
- Before the Navigate grabbing the current window handle (var handle = driver.CurrentWindowHandle;) and then using a SwitchTo back to that handle (driver.SwitchTo().Window(handle);) - however this has the exact same NoSuchWindowException as above.
- Using the ChromeDriver with the exact same code works fine

Our particular environment requires that we test against IE10 + IE11, as such using the ChromeDriver is not a viable workaround
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/586","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/586/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/586/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/586/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/586","id":82347067,"node_id":"MDU6SXNzdWU4MjM0NzA2Nw==","number":586,"title":"InternetExplorerDriver losing WindowHandles after Navigate().GoToUrl()","user":{"login":"mattridgway","id":2500671,"node_id":"MDQ6VXNlcjI1MDA2NzE=","avatar_url":"https://avatars0.githubusercontent.com/u/2500671?v=4","gravatar_id":"","url":"https://api.github.com/users/mattridgway","html_url":"https://github.com/mattridgway","followers_url":"https://api.github.com/users/mattridgway/followers","following_url":"https://api.github.com/users/mattridgway/following{/other_user}","gists_url":"https://api.github.com/users/mattridgway/gists{/gist_id}","starred_url":"https://api.github.com/users/mattridgway/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mattridgway/subscriptions","organizations_url":"https://api.github.com/users/mattridgway/orgs","repos_url":"https://api.github.com/users/mattridgway/repos","events_url":"https://api.github.com/users/mattridgway/events{/privacy}","received_events_url":"https://api.github.com/users/mattridgway/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2015-05-29T09:20:20Z","updated_at":"2019-08-21T12:09:50Z","closed_at":"2015-05-29T19:04:07Z","author_association":"NONE","body":"### setup\n- Windows 8.1 64bit\n- IE11(v.11.0.19 KB3049563) 64bit\n- Chrome (v.43.0.2357.81m)\n- NuGet package Selenium.WebDriver (v.2.45.0)\n- IEDriverServer.exe (v.2.45.0 - 64bit downloaded not built locally)\n- IEProtectedMode is off\n- HKLM/SOFTWARE/Wow6432Node/Microsoft/InternetExplorer/Main/FeatureControl/FEATURE_BFCACHE exists with a iexplore.exe dword = 0\n### code\n\n``` C#\nstatic void Main(string[] args)\n{\n    //var driver = new OpenQA.Selenium.Chrome.ChromeDriver();\n    var driver = new OpenQA.Selenium.IE.InternetExplorerDriver();\n    try\n    {\n        driver.Navigate().GoToUrl(\"https://www.google.com/\");\n        driver.FindElementByName(\"q\").SendKeys(\"FooBar\");\n    }\n    catch(Exception ex)\n    {\n        Console.WriteLine($\"Exception: {ex.Message}\");\n    }\n    finally\n    {\n        Console.ReadLine();\n        driver.Quit();\n    }\n}\n```\n### problem\n\nWhen the code reaches line 8 (driver.FindElementByName) I get a \"NoSuchElementException\" thrown, with the message \"Unable to find element with the name == q\". \n### investigation\n\nLooking at driver.WindowHandles before line 8 the count is one, after line 8 the count is 0. It appears that during the navigation the handles are lost.\n### what I've tried\n- Adding a Delay (System.Threading.Tasks.Task.Delay(1000).Wait()) between the navigation and findelement in case the page hadn't loaded properly in time. However the resulted in a different exception \"NoSuchWindowException\" with the message \"Unable to find element on closed window\".\n- Before the Navigate grabbing the current window handle (var handle = driver.CurrentWindowHandle;) and then using a SwitchTo back to that handle (driver.SwitchTo().Window(handle);) - however this has the exact same NoSuchWindowException as above.\n- Using the ChromeDriver with the exact same code works fine\n\nOur particular environment requires that we test against IE10 + IE11, as such using the ChromeDriver is not a viable workaround\n","closed_by":{"login":"mattridgway","id":2500671,"node_id":"MDQ6VXNlcjI1MDA2NzE=","avatar_url":"https://avatars0.githubusercontent.com/u/2500671?v=4","gravatar_id":"","url":"https://api.github.com/users/mattridgway","html_url":"https://github.com/mattridgway","followers_url":"https://api.github.com/users/mattridgway/followers","following_url":"https://api.github.com/users/mattridgway/following{/other_user}","gists_url":"https://api.github.com/users/mattridgway/gists{/gist_id}","starred_url":"https://api.github.com/users/mattridgway/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mattridgway/subscriptions","organizations_url":"https://api.github.com/users/mattridgway/orgs","repos_url":"https://api.github.com/users/mattridgway/repos","events_url":"https://api.github.com/users/mattridgway/events{/privacy}","received_events_url":"https://api.github.com/users/mattridgway/received_events","type":"User","site_admin":false}}", "commentIds":["106836209","106904669"], "labels":[]}