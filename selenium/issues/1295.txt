{"id":"1295", "title":"How do I tell selenium-webdriver where phantomjs is?", "body":"I have the path to phantom via:

```
var phantomjs = require('phantomjs');
var pathToPhantom = phantomjs.path;
```

Is there a way to just tell selenium-webdriver to use that path through the builder api instead of requiring me to modify my systems path?

e.g.

```
var webdriver = require('selenium-webdriver');
var phantomjs = require('phantomjs');

var driver = new webdriver.Builder()
  .fromBrowser('phantomjs', phantomjs.path)
  .build();
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1295","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1295/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1295/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1295/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1295","id":118153036,"node_id":"MDU6SXNzdWUxMTgxNTMwMzY=","number":1295,"title":"How do I tell selenium-webdriver where phantomjs is?","user":{"login":"justinmchase","id":10974,"node_id":"MDQ6VXNlcjEwOTc0","avatar_url":"https://avatars2.githubusercontent.com/u/10974?v=4","gravatar_id":"","url":"https://api.github.com/users/justinmchase","html_url":"https://github.com/justinmchase","followers_url":"https://api.github.com/users/justinmchase/followers","following_url":"https://api.github.com/users/justinmchase/following{/other_user}","gists_url":"https://api.github.com/users/justinmchase/gists{/gist_id}","starred_url":"https://api.github.com/users/justinmchase/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/justinmchase/subscriptions","organizations_url":"https://api.github.com/users/justinmchase/orgs","repos_url":"https://api.github.com/users/justinmchase/repos","events_url":"https://api.github.com/users/justinmchase/events{/privacy}","received_events_url":"https://api.github.com/users/justinmchase/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2015-11-21T00:03:23Z","updated_at":"2019-08-20T22:09:36Z","closed_at":"2015-11-21T01:19:52Z","author_association":"NONE","body":"I have the path to phantom via:\n\n```\nvar phantomjs = require('phantomjs');\nvar pathToPhantom = phantomjs.path;\n```\n\nIs there a way to just tell selenium-webdriver to use that path through the builder api instead of requiring me to modify my systems path?\n\ne.g.\n\n```\nvar webdriver = require('selenium-webdriver');\nvar phantomjs = require('phantomjs');\n\nvar driver = new webdriver.Builder()\n  .fromBrowser('phantomjs', phantomjs.path)\n  .build();\n```\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["158571630","158572213"], "labels":[]}