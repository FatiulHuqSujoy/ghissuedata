{"id":"6036", "title":"Selenium does not run via cron -> unknown error", "body":"## Meta -
OS:  
Ubuntu 18.04 LTS
Selenium Version:  
3.11.0
Browser:  
Chrome

I am not sure if this is an issue with selenium or cromedriver. A somewhat similar reported issue was closed by the thread starter without further comments. The issue only arises when running the script as a cronjob, and not when running the script from the IDE or terminal.

-->

Browser Version:  
67.0.3396.87 (Official Build) (64-bit)

## Expected Behavior -
Expected the script to start a new Chrome session, open a web-page and start parsing it.

## Actual Behavior -
The script produces the following error:

`Traceback (most recent call last):
  File \"/home/dpa/PycharmProjects/ParseRealEscort/Parser.py\", line 117, in <module>
    parser = Parse_Realescort(r\"http://www.realescort.eu/ads/category/girls/3/oslo\")
  File \"/home/dpa/PycharmProjects/ParseRealEscort/Parser.py\", line 15, in __init__
    self.driver = webdriver.Chrome(self.path)
  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__
    desired_capabilities=desired_capabilities)
  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 154, in __init__
    self.start_session(desired_capabilities, browser_profile)
  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 243, in start_session
    response = self.execute(Command.NEW_SESSION, parameters)
  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 312, in execute
    self.error_handler.check_response(response)
  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.WebDriverException: Message: unknown error: DevToolsActivePort file doesn't exist
  (Driver info: chromedriver=2.39.562737 (dba483cee6a5f15e2e2d73df16968ab10b38a2bf),platform=Linux 4.15.0-22-generic x86_64)`

## Steps to reproduce -
I added the following to the sudo crontab:
`PATH=/usr/bin:/bin:/home/dpa/anaconda3/bin/

39 18 * * * python /home/dpa/PycharmProjects/Parser/Parser.py >> /tmp/mycommand.log 2>&1
`
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6036","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6036/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6036/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6036/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6036","id":333354010,"node_id":"MDU6SXNzdWUzMzMzNTQwMTA=","number":6036,"title":"Selenium does not run via cron -> unknown error","user":{"login":"Centurion80","id":7833033,"node_id":"MDQ6VXNlcjc4MzMwMzM=","avatar_url":"https://avatars0.githubusercontent.com/u/7833033?v=4","gravatar_id":"","url":"https://api.github.com/users/Centurion80","html_url":"https://github.com/Centurion80","followers_url":"https://api.github.com/users/Centurion80/followers","following_url":"https://api.github.com/users/Centurion80/following{/other_user}","gists_url":"https://api.github.com/users/Centurion80/gists{/gist_id}","starred_url":"https://api.github.com/users/Centurion80/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Centurion80/subscriptions","organizations_url":"https://api.github.com/users/Centurion80/orgs","repos_url":"https://api.github.com/users/Centurion80/repos","events_url":"https://api.github.com/users/Centurion80/events{/privacy}","received_events_url":"https://api.github.com/users/Centurion80/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-06-18T16:53:00Z","updated_at":"2019-08-15T22:09:46Z","closed_at":"2018-06-18T17:40:23Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nUbuntu 18.04 LTS\r\nSelenium Version:  \r\n3.11.0\r\nBrowser:  \r\nChrome\r\n\r\nI am not sure if this is an issue with selenium or cromedriver. A somewhat similar reported issue was closed by the thread starter without further comments. The issue only arises when running the script as a cronjob, and not when running the script from the IDE or terminal.\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n67.0.3396.87 (Official Build) (64-bit)\r\n\r\n## Expected Behavior -\r\nExpected the script to start a new Chrome session, open a web-page and start parsing it.\r\n\r\n## Actual Behavior -\r\nThe script produces the following error:\r\n\r\n`Traceback (most recent call last):\r\n  File \"/home/dpa/PycharmProjects/ParseRealEscort/Parser.py\", line 117, in <module>\r\n    parser = Parse_Realescort(r\"http://www.realescort.eu/ads/category/girls/3/oslo\")\r\n  File \"/home/dpa/PycharmProjects/ParseRealEscort/Parser.py\", line 15, in __init__\r\n    self.driver = webdriver.Chrome(self.path)\r\n  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__\r\n    desired_capabilities=desired_capabilities)\r\n  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 154, in __init__\r\n    self.start_session(desired_capabilities, browser_profile)\r\n  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 243, in start_session\r\n    response = self.execute(Command.NEW_SESSION, parameters)\r\n  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 312, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"/home/dpa/anaconda3/lib/python3.6/site-packages/selenium/webdriver/remote/errorhandler.py\", line 242, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.WebDriverException: Message: unknown error: DevToolsActivePort file doesn't exist\r\n  (Driver info: chromedriver=2.39.562737 (dba483cee6a5f15e2e2d73df16968ab10b38a2bf),platform=Linux 4.15.0-22-generic x86_64)`\r\n\r\n## Steps to reproduce -\r\nI added the following to the sudo crontab:\r\n`PATH=/usr/bin:/bin:/home/dpa/anaconda3/bin/\r\n\r\n39 18 * * * python /home/dpa/PycharmProjects/Parser/Parser.py >> /tmp/mycommand.log 2>&1\r\n`\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["398136400"], "labels":[]}