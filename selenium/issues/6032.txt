{"id":"6032", "title":"IE11 Remote WebDriver Issue -- Error forwarding the new session cannot find : Capabilities", "body":"
OS:  
Windows 7
Selenium Version:  
3.10.0
Browser:  
IE 11

Trying to execute my code via Remote WebDriver + IE 11.
Grid Configuration:

Hub Running in Linux machine: java -jar selenium-server-standalone-2.48.2.jar -role hub 

Node Running in Windows Machine: java -Dwebdriver.chrome.driver=IEDriverServer.exe -jar selenium-server-standalone-2.48.2.jar -role node -hub __http://localhost:4444/grid/register -port 4446 -browser \"browserName=internet explorer,version=11,platform=WINDOWS\"

Code:

System.setProperty(\"webdriver.ie.driver\", \"c:/hub/IEDriverServer.exe\");
			InternetExplorerOptions options = new InternetExplorerOptions();			options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			log.info(\"Creating object of \" + browser);
			driver = new InternetExplorerDriver(options);
			log.info(\"IE Browser Instance is Launched\");
			//log.info(\"Creating object of \" + browser);
			log.info(\"IE Browser Instance is Launched\");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
			dc.setBrowserName(\"internet explorer\");
			dc.setPlatform(Platform.WINDOWS);
			dc.setVersion(\"11\");
			dc.setCapability(\"webdriver.ie.driver\", \"c:/hub/IEDriverServer.exe\");
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			dc.setCapability(\"acceptInsecureCerts\", true);
			dc.setCapability(\"acceptSslCerts\", true);
			dc.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			
			driver = new RemoteWebDriver(new URL(\"http://111.111.1.12:4444/wd/hub\"), dc);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 

Error Console:

FAILED CONFIGURATION: @BeforeClass setUp
org.openqa.selenium.WebDriverException: Error forwarding the new session cannot find : Capabilities [{webdriver.ie.driver=c:/hub/IEDriverServer.exe, ensureCleanSession=true, acceptSslCerts=true, enablePersistentHover=true, acceptInsecureCerts=true, browserName=internet explorer, ignoreProtectedModeSettings=true, version=11, platform=WINDOWS}]
Command duration or timeout: 15 milliseconds
Build info: version: '3.10.0', revision: '176b4a9', time: '2018-03-02T19:03:16.397Z'
System info: host: 'L-PBG466P', ip: '10.53.251.14', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_161'
Driver info: driver.version: RemoteWebDriver
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
	at org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)
	at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)
	at org.openqa.selenium.remote.JsonWireProtocolResponse.lambda$new$0(JsonWireProtocolResponse.java:53)
	at org.openqa.selenium.remote.JsonWireProtocolResponse.lambda$getResponseFunction$2(JsonWireProtocolResponse.java:91)
	at org.openqa.selenium.remote.ProtocolHandshake.lambda$createSession$0(ProtocolHandshake.java:123)
	at java.util.stream.ReferencePipeline$3$1.accept(ReferencePipeline.java:193)
	at java.util.Spliterators$ArraySpliterator.tryAdvance(Spliterators.java:958)
	at java.util.stream.ReferencePipeline.forEachWithCancel(ReferencePipeline.java:126)
	at java.util.stream.AbstractPipeline.copyIntoWithCancel(AbstractPipeline.java:498)
	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:471)
	at java.util.stream.FindOps$FindOp.evaluateSequential(FindOps.java:152)
	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
	at java.util.stream.ReferencePipeline.findFirst(ReferencePipeline.java:464)
	at org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:126)
	at org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:73)
	at org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:138)
	at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)
	at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:219)
	at org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:142)
	at org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:155)
	at com.wvo.automation.core.TestBase.selectBrowser(TestBase.java:129)
	at com.wvo.automation.core.TestBase.init(TestBase.java:66)
	at com.wvo.salepoint.scripts.Monitoring.setUp(Monitoring.java:32)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:84)
	at org.testng.internal.Invoker.invokeConfigurationMethod(Invoker.java:564)
	at org.testng.internal.Invoker.invokeConfigurations(Invoker.java:213)
	at org.testng.internal.Invoker.invokeConfigurations(Invoker.java:138)
	at org.testng.internal.TestMethodWorker.invokeBeforeClassMethods(TestMethodWorker.java:175)
	at org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:107)
	at org.testng.TestRunner.privateRun(TestRunner.java:767)
	at org.testng.TestRunner.run(TestRunner.java:617)
	at org.testng.SuiteRunner.runTest(SuiteRunner.java:334)
	at org.testng.SuiteRunner.runSequentially(SuiteRunner.java:329)
	at org.testng.SuiteRunner.privateRun(SuiteRunner.java:291)
	at org.testng.SuiteRunner.run(SuiteRunner.java:240)
	at org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)
	at org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)
	at org.testng.TestNG.runSuitesSequentially(TestNG.java:1224)
	at org.testng.TestNG.runSuitesLocally(TestNG.java:1149)
	at org.testng.TestNG.run(TestNG.java:1057)
	at org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)
	at org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)
	at org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)
Caused by: org.openqa.grid.common.exception.GridException: Error forwarding the new session cannot find : Capabilities [{webdriver.ie.driver=c:/hub/IEDriverServer.exe, ensureCleanSession=true, acceptSslCerts=true, enablePersistentHover=true, acceptInsecureCerts=true, browserName=internet explorer, ignoreProtectedModeSettings=true, version=11, platform=WINDOWS}]
	at org.openqa.grid.web.servlet.handler.RequestHandler.process(RequestHandler.java:115)
	at org.openqa.grid.web.servlet.DriverServlet.process(DriverServlet.java:83)
	at org.openqa.grid.web.servlet.DriverServlet.doPost(DriverServlet.java:67)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:790)
	at org.seleniumhq.jetty9.servlet.ServletHolder.handle(ServletHolder.java:808)
	at org.seleniumhq.jetty9.servlet.ServletHandler.doHandle(ServletHandler.java:587)
	at org.seleniumhq.jetty9.server.session.SessionHandler.doHandle(SessionHandler.java:221)
	at org.seleniumhq.jetty9.server.handler.ContextHandler.doHandle(ContextHandler.java:1127)
	at org.seleniumhq.jetty9.servlet.ServletHandler.doScope(ServletHandler.java:515)
	at org.seleniumhq.jetty9.server.session.SessionHandler.doScope(SessionHandler.java:185)
	at org.seleniumhq.jetty9.server.handler.ContextHandler.doScope(ContextHandler.java:1061)
	at org.seleniumhq.jetty9.server.handler.ScopedHandler.handle(ScopedHandler.java:141)
	at org.seleniumhq.jetty9.server.handler.HandlerWrapper.handle(HandlerWrapper.java:97)
	at org.seleniumhq.jetty9.server.Server.handle(Server.java:499)
	at org.seleniumhq.jetty9.server.HttpChannel.handle(HttpChannel.java:310)
	at org.seleniumhq.jetty9.server.HttpConnection.onFillable(HttpConnection.java:257)
	at org.seleniumhq.jetty9.io.AbstractConnection$2.run(AbstractConnection.java:540)
	at org.seleniumhq.jetty9.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:635)
	at org.seleniumhq.jetty9.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:555)
	at java.lang.Thread.run(Thread.java:748)
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6032","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6032/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6032/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6032/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6032","id":333113848,"node_id":"MDU6SXNzdWUzMzMxMTM4NDg=","number":6032,"title":"IE11 Remote WebDriver Issue -- Error forwarding the new session cannot find : Capabilities","user":{"login":"sushmithaguntupalli","id":40346541,"node_id":"MDQ6VXNlcjQwMzQ2NTQx","avatar_url":"https://avatars2.githubusercontent.com/u/40346541?v=4","gravatar_id":"","url":"https://api.github.com/users/sushmithaguntupalli","html_url":"https://github.com/sushmithaguntupalli","followers_url":"https://api.github.com/users/sushmithaguntupalli/followers","following_url":"https://api.github.com/users/sushmithaguntupalli/following{/other_user}","gists_url":"https://api.github.com/users/sushmithaguntupalli/gists{/gist_id}","starred_url":"https://api.github.com/users/sushmithaguntupalli/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sushmithaguntupalli/subscriptions","organizations_url":"https://api.github.com/users/sushmithaguntupalli/orgs","repos_url":"https://api.github.com/users/sushmithaguntupalli/repos","events_url":"https://api.github.com/users/sushmithaguntupalli/events{/privacy}","received_events_url":"https://api.github.com/users/sushmithaguntupalli/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-06-18T02:06:20Z","updated_at":"2019-08-15T04:09:41Z","closed_at":"2018-06-19T18:56:27Z","author_association":"NONE","body":"\r\nOS:  \r\nWindows 7\r\nSelenium Version:  \r\n3.10.0\r\nBrowser:  \r\nIE 11\r\n\r\nTrying to execute my code via Remote WebDriver + IE 11.\r\nGrid Configuration:\r\n\r\nHub Running in Linux machine: java -jar selenium-server-standalone-2.48.2.jar -role hub \r\n\r\nNode Running in Windows Machine: java -Dwebdriver.chrome.driver=IEDriverServer.exe -jar selenium-server-standalone-2.48.2.jar -role node -hub __http://localhost:4444/grid/register -port 4446 -browser \"browserName=internet explorer,version=11,platform=WINDOWS\"\r\n\r\nCode:\r\n\r\nSystem.setProperty(\"webdriver.ie.driver\", \"c:/hub/IEDriverServer.exe\");\r\n\t\t\tInternetExplorerOptions options = new InternetExplorerOptions();\t\t\toptions.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);\r\n\t\t\tlog.info(\"Creating object of \" + browser);\r\n\t\t\tdriver = new InternetExplorerDriver(options);\r\n\t\t\tlog.info(\"IE Browser Instance is Launched\");\r\n\t\t\t//log.info(\"Creating object of \" + browser);\r\n\t\t\tlog.info(\"IE Browser Instance is Launched\");\r\n\t\t\tdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);\r\n\t\t\t\r\n\t\t\tDesiredCapabilities dc = DesiredCapabilities.internetExplorer();\r\n\t\t\tdc.setBrowserName(\"internet explorer\");\r\n\t\t\tdc.setPlatform(Platform.WINDOWS);\r\n\t\t\tdc.setVersion(\"11\");\r\n\t\t\tdc.setCapability(\"webdriver.ie.driver\", \"c:/hub/IEDriverServer.exe\");\r\n\t\t\tdc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);\r\n\t\t\tdc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);\r\n\t\t\tdc.setCapability(\"acceptInsecureCerts\", true);\r\n\t\t\tdc.setCapability(\"acceptSslCerts\", true);\r\n\t\t\tdc.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);\r\n\t\t\tdc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);\r\n\t\t\tdc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);\r\n\t\t\t\r\n\t\t\tdriver = new RemoteWebDriver(new URL(\"http://111.111.1.12:4444/wd/hub\"), dc);\r\n\t\t\tdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); \r\n\r\nError Console:\r\n\r\nFAILED CONFIGURATION: @BeforeClass setUp\r\norg.openqa.selenium.WebDriverException: Error forwarding the new session cannot find : Capabilities [{webdriver.ie.driver=c:/hub/IEDriverServer.exe, ensureCleanSession=true, acceptSslCerts=true, enablePersistentHover=true, acceptInsecureCerts=true, browserName=internet explorer, ignoreProtectedModeSettings=true, version=11, platform=WINDOWS}]\r\nCommand duration or timeout: 15 milliseconds\r\nBuild info: version: '3.10.0', revision: '176b4a9', time: '2018-03-02T19:03:16.397Z'\r\nSystem info: host: 'L-PBG466P', ip: '10.53.251.14', os.name: 'Windows 7', os.arch: 'amd64', os.version: '6.1', java.version: '1.8.0_161'\r\nDriver info: driver.version: RemoteWebDriver\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.JsonWireProtocolResponse.lambda$new$0(JsonWireProtocolResponse.java:53)\r\n\tat org.openqa.selenium.remote.JsonWireProtocolResponse.lambda$getResponseFunction$2(JsonWireProtocolResponse.java:91)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.lambda$createSession$0(ProtocolHandshake.java:123)\r\n\tat java.util.stream.ReferencePipeline$3$1.accept(ReferencePipeline.java:193)\r\n\tat java.util.Spliterators$ArraySpliterator.tryAdvance(Spliterators.java:958)\r\n\tat java.util.stream.ReferencePipeline.forEachWithCancel(ReferencePipeline.java:126)\r\n\tat java.util.stream.AbstractPipeline.copyIntoWithCancel(AbstractPipeline.java:498)\r\n\tat java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)\r\n\tat java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:471)\r\n\tat java.util.stream.FindOps$FindOp.evaluateSequential(FindOps.java:152)\r\n\tat java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)\r\n\tat java.util.stream.ReferencePipeline.findFirst(ReferencePipeline.java:464)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:126)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:73)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:138)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:219)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:142)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:155)\r\n\tat com.wvo.automation.core.TestBase.selectBrowser(TestBase.java:129)\r\n\tat com.wvo.automation.core.TestBase.init(TestBase.java:66)\r\n\tat com.wvo.salepoint.scripts.Monitoring.setUp(Monitoring.java:32)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:84)\r\n\tat org.testng.internal.Invoker.invokeConfigurationMethod(Invoker.java:564)\r\n\tat org.testng.internal.Invoker.invokeConfigurations(Invoker.java:213)\r\n\tat org.testng.internal.Invoker.invokeConfigurations(Invoker.java:138)\r\n\tat org.testng.internal.TestMethodWorker.invokeBeforeClassMethods(TestMethodWorker.java:175)\r\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:107)\r\n\tat org.testng.TestRunner.privateRun(TestRunner.java:767)\r\n\tat org.testng.TestRunner.run(TestRunner.java:617)\r\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:334)\r\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:329)\r\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:291)\r\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:240)\r\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\r\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\r\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1224)\r\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1149)\r\n\tat org.testng.TestNG.run(TestNG.java:1057)\r\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\r\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\r\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\r\nCaused by: org.openqa.grid.common.exception.GridException: Error forwarding the new session cannot find : Capabilities [{webdriver.ie.driver=c:/hub/IEDriverServer.exe, ensureCleanSession=true, acceptSslCerts=true, enablePersistentHover=true, acceptInsecureCerts=true, browserName=internet explorer, ignoreProtectedModeSettings=true, version=11, platform=WINDOWS}]\r\n\tat org.openqa.grid.web.servlet.handler.RequestHandler.process(RequestHandler.java:115)\r\n\tat org.openqa.grid.web.servlet.DriverServlet.process(DriverServlet.java:83)\r\n\tat org.openqa.grid.web.servlet.DriverServlet.doPost(DriverServlet.java:67)\r\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:707)\r\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:790)\r\n\tat org.seleniumhq.jetty9.servlet.ServletHolder.handle(ServletHolder.java:808)\r\n\tat org.seleniumhq.jetty9.servlet.ServletHandler.doHandle(ServletHandler.java:587)\r\n\tat org.seleniumhq.jetty9.server.session.SessionHandler.doHandle(SessionHandler.java:221)\r\n\tat org.seleniumhq.jetty9.server.handler.ContextHandler.doHandle(ContextHandler.java:1127)\r\n\tat org.seleniumhq.jetty9.servlet.ServletHandler.doScope(ServletHandler.java:515)\r\n\tat org.seleniumhq.jetty9.server.session.SessionHandler.doScope(SessionHandler.java:185)\r\n\tat org.seleniumhq.jetty9.server.handler.ContextHandler.doScope(ContextHandler.java:1061)\r\n\tat org.seleniumhq.jetty9.server.handler.ScopedHandler.handle(ScopedHandler.java:141)\r\n\tat org.seleniumhq.jetty9.server.handler.HandlerWrapper.handle(HandlerWrapper.java:97)\r\n\tat org.seleniumhq.jetty9.server.Server.handle(Server.java:499)\r\n\tat org.seleniumhq.jetty9.server.HttpChannel.handle(HttpChannel.java:310)\r\n\tat org.seleniumhq.jetty9.server.HttpConnection.onFillable(HttpConnection.java:257)\r\n\tat org.seleniumhq.jetty9.io.AbstractConnection$2.run(AbstractConnection.java:540)\r\n\tat org.seleniumhq.jetty9.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:635)\r\n\tat org.seleniumhq.jetty9.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:555)\r\n\tat java.lang.Thread.run(Thread.java:748)\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["398024892","398077155","398083750","398507832","442835420"], "labels":[]}