{"id":"6410", "title":"Unable to bring the node after upgrading the selenium server to 3.12", "body":"## Meta -
OS:  Windows 10
<!-- Windows 10? OSX? -->
Selenium Version:  3.12
<!-- 3.12.0, IDE, etc -->

After upgrading the selenium to version 3.12, I am not able to bring up the node server. throwing NPE on the options. Please find the error below that I get when I run RunLocal-Node.bat, to check if the node is up and running.

17:11:58.321 INFO [GridLauncherV3.launch] - Selenium build info: version: '3.12.
0', revision: '7c6e0b3'
17:11:58.337 INFO [GridLauncherV3$3.launch] - Launching a Selenium Grid node on
port 5556
Usage: <main class> [options]
  Options:
    --debug, -debug
      <Boolean> : enables LogLevel.FINE.
      Default: false
    --version, -version
      Displays the version and exits.
      Default: false
    -browserTimeout
      <Integer> in seconds : number of seconds a browser session is allowed to
      hang while a WebDriver command is running (example: driver.get(url)). If
      the timeout is reached while a WebDriver command is still processing,
      the session will quit. Minimum value is 60. An unspecified, zero, or
      negative value means wait indefinitely.
    -capabilities, -browser
      <String> : comma separated Capability values. Example: -capabilities
      browserName=firefox,platform=linux -capabilities
      browserName=chrome,platform=linux
    -cleanUpCycle
      <Integer> in ms : specifies how often the hub will poll running proxies
      for timed-out (i.e. hung) threads. Must also specify \"timeout\" option
    -custom
      <String> : comma separated key=value pairs for custom grid extensions.
      NOT RECOMMENDED -- may be deprecated in a future revision. Example:
      -custom myParamA=Value1,myParamB=Value2
    -downPollingLimit
      <Integer> : node is marked as \"down\" if the node hasn't responded after
      the number of checks specified in [downPollingLimit].
      Default: 2
    -enablePlatformVerification
      <Boolean>: Whether or not to drop capabilities that does not belong to
      the current platform family. Defaults to true.
    -host
      <String> IP or hostname : usually determined automatically. Most
      commonly useful in exotic network configurations (e.g. network with VPN)
      Default: 0.0.0.0
    -hub
      <String> : the url that will be used to post the registration request.
      This option takes precedence over -hubHost and -hubPort options.
      Default: https://c9uat01.baplc.com:4445
    -hubHost
      <String> IP or hostname : the host address of the hub we're attempting
      to register with. If -hub is specified the -hubHost is determined from
      it.
    -hubPort
      <Integer> : the port of the hub we're attempting to register with. If
      -hub is specified the -hubPort is determined from it.
    -id
      <String> : optional unique identifier for the node. Defaults to the url
      of the remoteHost, when not specified.
    -jettyThreads, -jettyMaxThreads
      <Integer> : max number of threads for Jetty. An unspecified, zero, or
      negative value means the Jetty default value (200) will be used.
    -log
      <String> filename : the filename to use for logging. If omitted, will
      log to STDOUT
    -maxSession
      <Integer> max number of tests that can run at the same time on the node,
      irrespective of the browser used
      Default: 5
    -nodeConfig
      <String> filename : JSON configuration file for the node. Overrides
      default values
      Default: D:\\Selenium\\Node-B\\nodeBConf.json
    -nodePolling
      <Integer> in ms : specifies how often the hub will poll to see if the
      node is still responding.
      Default: 5000
    -nodeStatusCheckTimeout
      <Integer> in ms : connection/socket timeout, used for node \"nodePolling\"
      check.
      Default: 5000
    -port
      <Integer> : the port number the server will use.
      Default: 5556
    -proxy
      <String> : the class used to represent the node proxy. Default is
      [org.openqa.grid.selenium.proxy.DefaultRemoteProxy].
    -register
      if specified, node will attempt to re-register itself automatically with
      its known grid hub if the hub becomes unavailable.
      Default: true
    -registerCycle
      <Integer> in ms : specifies how often the node will try to register
      itself again. Allows administrator to restart the hub without restarting
      (or risk orphaning) registered nodes. Must be specified with the
      \"-register\" option.
      Default: 5000
    -remoteHost
      <String> URL: Address to report to the hub. Used to override default
      (http://<host>:<port>).
    -role
      <String> options are [hub], [node], or [standalone].
      Default: node
    -servlet, -servlets
      <String> : list of extra servlets the grid (hub or node) will make
      available. Specify multiple on the command line: -servlet
      tld.company.ServletA -servlet tld.company.ServletB. The servlet must
      exist in the path: /grid/admin/ServletA /grid/admin/ServletB
    -timeout, -sessionTimeout
      <Integer> in seconds : Specifies the timeout before the server
      automatically kills a session that hasn't had any activity in the last X
      seconds. The test slot will then be released for another test to use.
      This is typically used to take care of client crashes. For grid hub/node
      roles, cleanUpCycle must also be set.
    -unregisterIfStillDownAfter
      <Integer> in ms : if the node remains down for more than
      [unregisterIfStillDownAfter] ms, it will stop attempting to re-register
      from the hub.
      Default: 60000
    -withoutServlet, -withoutServlets
      <String> : list of default (hub or node) servlets to disable. Advanced
      use cases only. Not all default servlets can be disabled. Specify
      multiple on the command line: -withoutServlet tld.company.ServletA
      -withoutServlet tld.company.ServletB
java.lang.NullPointerException
        at org.openqa.grid.internal.utils.configuration.GridNodeConfiguration.la
mbda$fixUpCapabilities$0(GridNodeConfiguration.java:421)
        at java.util.stream.ReferencePipeline$11$1.accept(ReferencePipeline.java
:372)
        at java.util.ArrayList$ArrayListSpliterator.forEachRemaining(ArrayList.j
ava:1374)
        at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:481)

        at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.ja
va:471)
        at java.util.stream.ReduceOps$ReduceOp.evaluateSequential(ReduceOps.java
:708)
        at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)

        at java.util.stream.ReferencePipeline.collect(ReferencePipeline.java:499
)
        at org.openqa.grid.internal.utils.configuration.GridNodeConfiguration.fi
xUpCapabilities(GridNodeConfiguration.java:431)
        at org.openqa.grid.common.RegistrationRequest.<init>(RegistrationRequest
.java:103)
        at org.openqa.grid.common.RegistrationRequest.build(RegistrationRequest.
java:207)
        at org.openqa.grid.internal.utils.SelfRegisteringRemote.<init>(SelfRegis
teringRemote.java:70)
        at org.openqa.grid.selenium.GridLauncherV3$3.launch(GridLauncherV3.java:
312)
        at org.openqa.grid.selenium.GridLauncherV3.launch(GridLauncherV3.java:12
2)
        at org.openqa.grid.selenium.GridLauncherV3.main(GridLauncherV3.java:82)
Press any key to continue . . .





















































## Expected Behavior - Node should be up and running

## Actual Behavior - Getting NullPointerException error


[nodecong.txt](https://github.com/SeleniumHQ/selenium/files/2382907/nodecong.txt)

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6410","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6410/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6410/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6410/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6410","id":360241434,"node_id":"MDU6SXNzdWUzNjAyNDE0MzQ=","number":6410,"title":"Unable to bring the node after upgrading the selenium server to 3.12","user":{"login":"Jayita09","id":43247176,"node_id":"MDQ6VXNlcjQzMjQ3MTc2","avatar_url":"https://avatars2.githubusercontent.com/u/43247176?v=4","gravatar_id":"","url":"https://api.github.com/users/Jayita09","html_url":"https://github.com/Jayita09","followers_url":"https://api.github.com/users/Jayita09/followers","following_url":"https://api.github.com/users/Jayita09/following{/other_user}","gists_url":"https://api.github.com/users/Jayita09/gists{/gist_id}","starred_url":"https://api.github.com/users/Jayita09/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Jayita09/subscriptions","organizations_url":"https://api.github.com/users/Jayita09/orgs","repos_url":"https://api.github.com/users/Jayita09/repos","events_url":"https://api.github.com/users/Jayita09/events{/privacy}","received_events_url":"https://api.github.com/users/Jayita09/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-09-14T10:07:55Z","updated_at":"2019-08-15T11:09:55Z","closed_at":"2018-09-14T10:30:25Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.12\r\n<!-- 3.12.0, IDE, etc -->\r\n\r\nAfter upgrading the selenium to version 3.12, I am not able to bring up the node server. throwing NPE on the options. Please find the error below that I get when I run RunLocal-Node.bat, to check if the node is up and running.\r\n\r\n17:11:58.321 INFO [GridLauncherV3.launch] - Selenium build info: version: '3.12.\r\n0', revision: '7c6e0b3'\r\n17:11:58.337 INFO [GridLauncherV3$3.launch] - Launching a Selenium Grid node on\r\nport 5556\r\nUsage: <main class> [options]\r\n  Options:\r\n    --debug, -debug\r\n      <Boolean> : enables LogLevel.FINE.\r\n      Default: false\r\n    --version, -version\r\n      Displays the version and exits.\r\n      Default: false\r\n    -browserTimeout\r\n      <Integer> in seconds : number of seconds a browser session is allowed to\r\n      hang while a WebDriver command is running (example: driver.get(url)). If\r\n      the timeout is reached while a WebDriver command is still processing,\r\n      the session will quit. Minimum value is 60. An unspecified, zero, or\r\n      negative value means wait indefinitely.\r\n    -capabilities, -browser\r\n      <String> : comma separated Capability values. Example: -capabilities\r\n      browserName=firefox,platform=linux -capabilities\r\n      browserName=chrome,platform=linux\r\n    -cleanUpCycle\r\n      <Integer> in ms : specifies how often the hub will poll running proxies\r\n      for timed-out (i.e. hung) threads. Must also specify \"timeout\" option\r\n    -custom\r\n      <String> : comma separated key=value pairs for custom grid extensions.\r\n      NOT RECOMMENDED -- may be deprecated in a future revision. Example:\r\n      -custom myParamA=Value1,myParamB=Value2\r\n    -downPollingLimit\r\n      <Integer> : node is marked as \"down\" if the node hasn't responded after\r\n      the number of checks specified in [downPollingLimit].\r\n      Default: 2\r\n    -enablePlatformVerification\r\n      <Boolean>: Whether or not to drop capabilities that does not belong to\r\n      the current platform family. Defaults to true.\r\n    -host\r\n      <String> IP or hostname : usually determined automatically. Most\r\n      commonly useful in exotic network configurations (e.g. network with VPN)\r\n      Default: 0.0.0.0\r\n    -hub\r\n      <String> : the url that will be used to post the registration request.\r\n      This option takes precedence over -hubHost and -hubPort options.\r\n      Default: https://c9uat01.baplc.com:4445\r\n    -hubHost\r\n      <String> IP or hostname : the host address of the hub we're attempting\r\n      to register with. If -hub is specified the -hubHost is determined from\r\n      it.\r\n    -hubPort\r\n      <Integer> : the port of the hub we're attempting to register with. If\r\n      -hub is specified the -hubPort is determined from it.\r\n    -id\r\n      <String> : optional unique identifier for the node. Defaults to the url\r\n      of the remoteHost, when not specified.\r\n    -jettyThreads, -jettyMaxThreads\r\n      <Integer> : max number of threads for Jetty. An unspecified, zero, or\r\n      negative value means the Jetty default value (200) will be used.\r\n    -log\r\n      <String> filename : the filename to use for logging. If omitted, will\r\n      log to STDOUT\r\n    -maxSession\r\n      <Integer> max number of tests that can run at the same time on the node,\r\n      irrespective of the browser used\r\n      Default: 5\r\n    -nodeConfig\r\n      <String> filename : JSON configuration file for the node. Overrides\r\n      default values\r\n      Default: D:\\Selenium\\Node-B\\nodeBConf.json\r\n    -nodePolling\r\n      <Integer> in ms : specifies how often the hub will poll to see if the\r\n      node is still responding.\r\n      Default: 5000\r\n    -nodeStatusCheckTimeout\r\n      <Integer> in ms : connection/socket timeout, used for node \"nodePolling\"\r\n      check.\r\n      Default: 5000\r\n    -port\r\n      <Integer> : the port number the server will use.\r\n      Default: 5556\r\n    -proxy\r\n      <String> : the class used to represent the node proxy. Default is\r\n      [org.openqa.grid.selenium.proxy.DefaultRemoteProxy].\r\n    -register\r\n      if specified, node will attempt to re-register itself automatically with\r\n      its known grid hub if the hub becomes unavailable.\r\n      Default: true\r\n    -registerCycle\r\n      <Integer> in ms : specifies how often the node will try to register\r\n      itself again. Allows administrator to restart the hub without restarting\r\n      (or risk orphaning) registered nodes. Must be specified with the\r\n      \"-register\" option.\r\n      Default: 5000\r\n    -remoteHost\r\n      <String> URL: Address to report to the hub. Used to override default\r\n      (http://<host>:<port>).\r\n    -role\r\n      <String> options are [hub], [node], or [standalone].\r\n      Default: node\r\n    -servlet, -servlets\r\n      <String> : list of extra servlets the grid (hub or node) will make\r\n      available. Specify multiple on the command line: -servlet\r\n      tld.company.ServletA -servlet tld.company.ServletB. The servlet must\r\n      exist in the path: /grid/admin/ServletA /grid/admin/ServletB\r\n    -timeout, -sessionTimeout\r\n      <Integer> in seconds : Specifies the timeout before the server\r\n      automatically kills a session that hasn't had any activity in the last X\r\n      seconds. The test slot will then be released for another test to use.\r\n      This is typically used to take care of client crashes. For grid hub/node\r\n      roles, cleanUpCycle must also be set.\r\n    -unregisterIfStillDownAfter\r\n      <Integer> in ms : if the node remains down for more than\r\n      [unregisterIfStillDownAfter] ms, it will stop attempting to re-register\r\n      from the hub.\r\n      Default: 60000\r\n    -withoutServlet, -withoutServlets\r\n      <String> : list of default (hub or node) servlets to disable. Advanced\r\n      use cases only. Not all default servlets can be disabled. Specify\r\n      multiple on the command line: -withoutServlet tld.company.ServletA\r\n      -withoutServlet tld.company.ServletB\r\njava.lang.NullPointerException\r\n        at org.openqa.grid.internal.utils.configuration.GridNodeConfiguration.la\r\nmbda$fixUpCapabilities$0(GridNodeConfiguration.java:421)\r\n        at java.util.stream.ReferencePipeline$11$1.accept(ReferencePipeline.java\r\n:372)\r\n        at java.util.ArrayList$ArrayListSpliterator.forEachRemaining(ArrayList.j\r\nava:1374)\r\n        at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:481)\r\n\r\n        at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.ja\r\nva:471)\r\n        at java.util.stream.ReduceOps$ReduceOp.evaluateSequential(ReduceOps.java\r\n:708)\r\n        at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)\r\n\r\n        at java.util.stream.ReferencePipeline.collect(ReferencePipeline.java:499\r\n)\r\n        at org.openqa.grid.internal.utils.configuration.GridNodeConfiguration.fi\r\nxUpCapabilities(GridNodeConfiguration.java:431)\r\n        at org.openqa.grid.common.RegistrationRequest.<init>(RegistrationRequest\r\n.java:103)\r\n        at org.openqa.grid.common.RegistrationRequest.build(RegistrationRequest.\r\njava:207)\r\n        at org.openqa.grid.internal.utils.SelfRegisteringRemote.<init>(SelfRegis\r\nteringRemote.java:70)\r\n        at org.openqa.grid.selenium.GridLauncherV3$3.launch(GridLauncherV3.java:\r\n312)\r\n        at org.openqa.grid.selenium.GridLauncherV3.launch(GridLauncherV3.java:12\r\n2)\r\n        at org.openqa.grid.selenium.GridLauncherV3.main(GridLauncherV3.java:82)\r\nPress any key to continue . . .\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n## Expected Behavior - Node should be up and running\r\n\r\n## Actual Behavior - Getting NullPointerException error\r\n\r\n\r\n[nodecong.txt](https://github.com/SeleniumHQ/selenium/files/2382907/nodecong.txt)\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"diemol","id":5992658,"node_id":"MDQ6VXNlcjU5OTI2NTg=","avatar_url":"https://avatars1.githubusercontent.com/u/5992658?v=4","gravatar_id":"","url":"https://api.github.com/users/diemol","html_url":"https://github.com/diemol","followers_url":"https://api.github.com/users/diemol/followers","following_url":"https://api.github.com/users/diemol/following{/other_user}","gists_url":"https://api.github.com/users/diemol/gists{/gist_id}","starred_url":"https://api.github.com/users/diemol/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/diemol/subscriptions","organizations_url":"https://api.github.com/users/diemol/orgs","repos_url":"https://api.github.com/users/diemol/repos","events_url":"https://api.github.com/users/diemol/events{/privacy}","received_events_url":"https://api.github.com/users/diemol/received_events","type":"User","site_admin":false}}", "commentIds":["421311471"], "labels":[]}