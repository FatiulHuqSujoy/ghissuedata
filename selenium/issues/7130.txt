{"id":"7130", "title":"python UnexpectedAlertPresentException doesn't contain alert_text string", "body":"## 🐛 Bug Report

When `UnexpectedAlertPresentException` is raised the exception attribute `alert_text` is None.  The `alert_text` message _can_ be found within the `msg` attribute but it's a pain to parse out the original alert text string from the rest of the exception message.  It would be nice if the `alert_text` attribute contained the original alert text.

Is this possible on the selenium side of things or is this something chromedriver must remedy?

## To Reproduce

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

Raise and catch `UnexpectedAlertPresentException` in python


## Expected behavior

The `UnexpectedAlertPresentException` attribute `alert_text` should contain the alert.text string.

## Test script or set of commands reproducing this issue



## Environment

OS: OSX
Browser: Chrome
Browser version: 73.0.3683.103
Browser Driver version: chromedriver=2.38.552518
Language Bindings version: python 3.7
Selenium Grid version (if applicable): 3.141.0
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7130","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7130/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7130/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7130/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7130","id":436402568,"node_id":"MDU6SXNzdWU0MzY0MDI1Njg=","number":7130,"title":"python UnexpectedAlertPresentException doesn't contain alert_text string","user":{"login":"bandophahita","id":4211287,"node_id":"MDQ6VXNlcjQyMTEyODc=","avatar_url":"https://avatars1.githubusercontent.com/u/4211287?v=4","gravatar_id":"","url":"https://api.github.com/users/bandophahita","html_url":"https://github.com/bandophahita","followers_url":"https://api.github.com/users/bandophahita/followers","following_url":"https://api.github.com/users/bandophahita/following{/other_user}","gists_url":"https://api.github.com/users/bandophahita/gists{/gist_id}","starred_url":"https://api.github.com/users/bandophahita/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bandophahita/subscriptions","organizations_url":"https://api.github.com/users/bandophahita/orgs","repos_url":"https://api.github.com/users/bandophahita/repos","events_url":"https://api.github.com/users/bandophahita/events{/privacy}","received_events_url":"https://api.github.com/users/bandophahita/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-04-23T21:40:19Z","updated_at":"2019-08-14T13:09:43Z","closed_at":"2019-04-23T22:25:13Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nWhen `UnexpectedAlertPresentException` is raised the exception attribute `alert_text` is None.  The `alert_text` message _can_ be found within the `msg` attribute but it's a pain to parse out the original alert text string from the rest of the exception message.  It would be nice if the `alert_text` attribute contained the original alert text.\r\n\r\nIs this possible on the selenium side of things or is this something chromedriver must remedy?\r\n\r\n## To Reproduce\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\nRaise and catch `UnexpectedAlertPresentException` in python\r\n\r\n\r\n## Expected behavior\r\n\r\nThe `UnexpectedAlertPresentException` attribute `alert_text` should contain the alert.text string.\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\n\r\n\r\n## Environment\r\n\r\nOS: OSX\r\nBrowser: Chrome\r\nBrowser version: 73.0.3683.103\r\nBrowser Driver version: chromedriver=2.38.552518\r\nLanguage Bindings version: python 3.7\r\nSelenium Grid version (if applicable): 3.141.0\r\n","closed_by":{"login":"cgoldberg","id":1113081,"node_id":"MDQ6VXNlcjExMTMwODE=","avatar_url":"https://avatars0.githubusercontent.com/u/1113081?v=4","gravatar_id":"","url":"https://api.github.com/users/cgoldberg","html_url":"https://github.com/cgoldberg","followers_url":"https://api.github.com/users/cgoldberg/followers","following_url":"https://api.github.com/users/cgoldberg/following{/other_user}","gists_url":"https://api.github.com/users/cgoldberg/gists{/gist_id}","starred_url":"https://api.github.com/users/cgoldberg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cgoldberg/subscriptions","organizations_url":"https://api.github.com/users/cgoldberg/orgs","repos_url":"https://api.github.com/users/cgoldberg/repos","events_url":"https://api.github.com/users/cgoldberg/events{/privacy}","received_events_url":"https://api.github.com/users/cgoldberg/received_events","type":"User","site_admin":false}}", "commentIds":["485997121","486049929"], "labels":[]}