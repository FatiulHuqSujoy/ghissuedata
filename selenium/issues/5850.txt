{"id":"5850", "title":"Unable to verify the element in Headless browser", "body":"## Meta -
OS:  macsOS high Sierra
Selenium Version: 3.8.1
Browser:  
Google Chrome

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

-->

Browser Version:  65(64-bit)
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
It should able to select element as its working fine when open for actual UI.

## Actual Behavior -
Its not able to select any element on the page.

## Steps to reproduce -
@BeforeMethod
    public void setUp() throws Exception {
        ChromeOptions chromeOptions = new ChromeOptions();
        //todo Change your chrome driver path
        System.setProperty(\"webdriver.chrome.driver\",
             \"/Users/snehal/Downloads/chromedriver\");
        chromeOptions.addArguments(\"--headless\");
        driver = new ChromeDriver(chromeOptions);
    }

    @Test
    public void testIssue() throws Exception {
        // todo change it to test html page
        driver.get(\"https://stag.furlenco.com\");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(
                ExpectedConditions.presenceOfElementLocated(By.xpath(\"//*[text='Bengaluru']\")));
        System.out.println(
                \"Label text : \" + driver.findElement(By.xpath((\"//*[text='Bengaluru']\"))).getText());
    }

}
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5850","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5850/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5850/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5850/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5850","id":319332632,"node_id":"MDU6SXNzdWUzMTkzMzI2MzI=","number":5850,"title":"Unable to verify the element in Headless browser","user":{"login":"snehalgajbhiye","id":27271205,"node_id":"MDQ6VXNlcjI3MjcxMjA1","avatar_url":"https://avatars3.githubusercontent.com/u/27271205?v=4","gravatar_id":"","url":"https://api.github.com/users/snehalgajbhiye","html_url":"https://github.com/snehalgajbhiye","followers_url":"https://api.github.com/users/snehalgajbhiye/followers","following_url":"https://api.github.com/users/snehalgajbhiye/following{/other_user}","gists_url":"https://api.github.com/users/snehalgajbhiye/gists{/gist_id}","starred_url":"https://api.github.com/users/snehalgajbhiye/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snehalgajbhiye/subscriptions","organizations_url":"https://api.github.com/users/snehalgajbhiye/orgs","repos_url":"https://api.github.com/users/snehalgajbhiye/repos","events_url":"https://api.github.com/users/snehalgajbhiye/events{/privacy}","received_events_url":"https://api.github.com/users/snehalgajbhiye/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-05-01T21:22:36Z","updated_at":"2019-08-16T04:09:35Z","closed_at":"2018-05-02T19:26:02Z","author_association":"NONE","body":"## Meta -\r\nOS:  macsOS high Sierra\r\nSelenium Version: 3.8.1\r\nBrowser:  \r\nGoogle Chrome\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\n-->\r\n\r\nBrowser Version:  65(64-bit)\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nIt should able to select element as its working fine when open for actual UI.\r\n\r\n## Actual Behavior -\r\nIts not able to select any element on the page.\r\n\r\n## Steps to reproduce -\r\n@BeforeMethod\r\n    public void setUp() throws Exception {\r\n        ChromeOptions chromeOptions = new ChromeOptions();\r\n        //todo Change your chrome driver path\r\n        System.setProperty(\"webdriver.chrome.driver\",\r\n             \"/Users/snehal/Downloads/chromedriver\");\r\n        chromeOptions.addArguments(\"--headless\");\r\n        driver = new ChromeDriver(chromeOptions);\r\n    }\r\n\r\n    @Test\r\n    public void testIssue() throws Exception {\r\n        // todo change it to test html page\r\n        driver.get(\"https://stag.furlenco.com\");\r\n        WebDriverWait wait = new WebDriverWait(driver, 10);\r\n        wait.until(\r\n                ExpectedConditions.presenceOfElementLocated(By.xpath(\"//*[text='Bengaluru']\")));\r\n        System.out.println(\r\n                \"Label text : \" + driver.findElement(By.xpath((\"//*[text='Bengaluru']\"))).getText());\r\n    }\r\n\r\n}\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["385871284","386092347"], "labels":[]}