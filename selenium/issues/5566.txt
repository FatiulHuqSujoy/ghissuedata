{"id":"5566", "title":"Element attributes class and id are concatenated by ruby element.attribute(:class)", "body":"## Meta -
OS:  
Darwin Kernel Version 16.7.0 x86_64

Selenium Version:  
chromedriver 2.36, selenium-webdriver 3.10.0

Browser:  
Chrome

Browser Version:  
64.0.3282.186 (64-bit)

## Expected Behavior -
element.attribute(:class) should return the class string of the target element.

## Actual Behavior -
When inspecting google.com, the returned class string is a concatenation of the class and id strings.  I am new to this, so this might be the expected behavior, but I can't find anything to confirm this.

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->

require 'selenium-webdriver'

driver = Selenium::WebDriver.for :chrome
driver.get 'http://www.google.com'

ARGV.each do |t|
  elements = driver.find_elements(tag_name: t)
  puts \"#{t}: #{elements.size}\"
  elements.size.times do |i|
    element = elements[i]
    n = element.attribute(:name)
    c = element.attribute(:class)
    t = element.attribute(:type)
    v = element.attribute(:value)
    puts \"[#{i}]: name=#{n} class=#{c} type=#{t} value=#{v}\"
  end
end

driver.quit

jcavallaro-mbp:ch01 jcavallaro$ ruby findtags.rb input
input: 9
[0]: name=source class= type=hidden value=hp
[1]: name=ei class= type=hidden value=V4SdWu3CO4K9Ud_yqZgJ
[2]: name=q class=gsfi lst-d-f type=text value=      *Note that the class and id together here*
[3]: name= class=gsfi type=text value=
[4]: name= class=gsfi type=text value=
[5]: name=btnK class= type=submit value=Google Search
[6]: name=btnI class= type=submit value=I'm Feeling Lucky
[7]: name=oq class= type=hidden value=
[8]: name=gs_l class= type=hidden value=", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5566","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5566/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5566/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5566/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5566","id":302405886,"node_id":"MDU6SXNzdWUzMDI0MDU4ODY=","number":5566,"title":"Element attributes class and id are concatenated by ruby element.attribute(:class)","user":{"login":"jeffery-cavallaro","id":35273119,"node_id":"MDQ6VXNlcjM1MjczMTE5","avatar_url":"https://avatars1.githubusercontent.com/u/35273119?v=4","gravatar_id":"","url":"https://api.github.com/users/jeffery-cavallaro","html_url":"https://github.com/jeffery-cavallaro","followers_url":"https://api.github.com/users/jeffery-cavallaro/followers","following_url":"https://api.github.com/users/jeffery-cavallaro/following{/other_user}","gists_url":"https://api.github.com/users/jeffery-cavallaro/gists{/gist_id}","starred_url":"https://api.github.com/users/jeffery-cavallaro/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jeffery-cavallaro/subscriptions","organizations_url":"https://api.github.com/users/jeffery-cavallaro/orgs","repos_url":"https://api.github.com/users/jeffery-cavallaro/repos","events_url":"https://api.github.com/users/jeffery-cavallaro/events{/privacy}","received_events_url":"https://api.github.com/users/jeffery-cavallaro/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-03-05T18:13:47Z","updated_at":"2019-08-16T11:09:52Z","closed_at":"2018-03-05T19:02:58Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nDarwin Kernel Version 16.7.0 x86_64\r\n\r\nSelenium Version:  \r\nchromedriver 2.36, selenium-webdriver 3.10.0\r\n\r\nBrowser:  \r\nChrome\r\n\r\nBrowser Version:  \r\n64.0.3282.186 (64-bit)\r\n\r\n## Expected Behavior -\r\nelement.attribute(:class) should return the class string of the target element.\r\n\r\n## Actual Behavior -\r\nWhen inspecting google.com, the returned class string is a concatenation of the class and id strings.  I am new to this, so this might be the expected behavior, but I can't find anything to confirm this.\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\nrequire 'selenium-webdriver'\r\n\r\ndriver = Selenium::WebDriver.for :chrome\r\ndriver.get 'http://www.google.com'\r\n\r\nARGV.each do |t|\r\n  elements = driver.find_elements(tag_name: t)\r\n  puts \"#{t}: #{elements.size}\"\r\n  elements.size.times do |i|\r\n    element = elements[i]\r\n    n = element.attribute(:name)\r\n    c = element.attribute(:class)\r\n    t = element.attribute(:type)\r\n    v = element.attribute(:value)\r\n    puts \"[#{i}]: name=#{n} class=#{c} type=#{t} value=#{v}\"\r\n  end\r\nend\r\n\r\ndriver.quit\r\n\r\njcavallaro-mbp:ch01 jcavallaro$ ruby findtags.rb input\r\ninput: 9\r\n[0]: name=source class= type=hidden value=hp\r\n[1]: name=ei class= type=hidden value=V4SdWu3CO4K9Ud_yqZgJ\r\n[2]: name=q class=gsfi lst-d-f type=text value=      *Note that the class and id together here*\r\n[3]: name= class=gsfi type=text value=\r\n[4]: name= class=gsfi type=text value=\r\n[5]: name=btnK class= type=submit value=Google Search\r\n[6]: name=btnI class= type=submit value=I'm Feeling Lucky\r\n[7]: name=oq class= type=hidden value=\r\n[8]: name=gs_l class= type=hidden value=","closed_by":{"login":"jeffery-cavallaro","id":35273119,"node_id":"MDQ6VXNlcjM1MjczMTE5","avatar_url":"https://avatars1.githubusercontent.com/u/35273119?v=4","gravatar_id":"","url":"https://api.github.com/users/jeffery-cavallaro","html_url":"https://github.com/jeffery-cavallaro","followers_url":"https://api.github.com/users/jeffery-cavallaro/followers","following_url":"https://api.github.com/users/jeffery-cavallaro/following{/other_user}","gists_url":"https://api.github.com/users/jeffery-cavallaro/gists{/gist_id}","starred_url":"https://api.github.com/users/jeffery-cavallaro/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jeffery-cavallaro/subscriptions","organizations_url":"https://api.github.com/users/jeffery-cavallaro/orgs","repos_url":"https://api.github.com/users/jeffery-cavallaro/repos","events_url":"https://api.github.com/users/jeffery-cavallaro/events{/privacy}","received_events_url":"https://api.github.com/users/jeffery-cavallaro/received_events","type":"User","site_admin":false}}", "commentIds":["370526652"], "labels":[]}