{"id":"6561", "title":"Nav windows remain in dock after Mojave update", "body":"## Meta -
OS:  MacOS Mojave 10.14
<!-- Windows 10? OSX? -->
Selenium Version:  3.5.3
<!-- 2.52.0, IDE, etc -->
Browser:  Google Chrome 
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  69.0.3497.100 
<!-- e.g.: 49.0.2623.87 (64-bit) -->

So, since i updated to Mojave in my Mac, i noticed that every time i run a test, the window nav that selenium opens remains in the dock after it's finished, like this:

[https://imgur.com/a/vHz2tv3](url)

I'm not sure if it's Selenium, WebDriver or what, but what i'm sure is that this behavior started after the OS update, so it might have something to do with Mojave?

## Expected Behavior -
Once the test is finished, the window nav closes

## Actual Behavior -

Once the test is finished, the window nav closes but it remains in the dock, like in the photo.

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6561","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6561/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6561/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6561/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6561","id":371969728,"node_id":"MDU6SXNzdWUzNzE5Njk3Mjg=","number":6561,"title":"Nav windows remain in dock after Mojave update","user":{"login":"ssorriba","id":36347993,"node_id":"MDQ6VXNlcjM2MzQ3OTkz","avatar_url":"https://avatars2.githubusercontent.com/u/36347993?v=4","gravatar_id":"","url":"https://api.github.com/users/ssorriba","html_url":"https://github.com/ssorriba","followers_url":"https://api.github.com/users/ssorriba/followers","following_url":"https://api.github.com/users/ssorriba/following{/other_user}","gists_url":"https://api.github.com/users/ssorriba/gists{/gist_id}","starred_url":"https://api.github.com/users/ssorriba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ssorriba/subscriptions","organizations_url":"https://api.github.com/users/ssorriba/orgs","repos_url":"https://api.github.com/users/ssorriba/repos","events_url":"https://api.github.com/users/ssorriba/events{/privacy}","received_events_url":"https://api.github.com/users/ssorriba/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2018-10-19T13:58:31Z","updated_at":"2019-08-14T10:10:02Z","closed_at":"2018-10-19T16:57:48Z","author_association":"NONE","body":"## Meta -\r\nOS:  MacOS Mojave 10.14\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.5.3\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Google Chrome \r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  69.0.3497.100 \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\nSo, since i updated to Mojave in my Mac, i noticed that every time i run a test, the window nav that selenium opens remains in the dock after it's finished, like this:\r\n\r\n[https://imgur.com/a/vHz2tv3](url)\r\n\r\nI'm not sure if it's Selenium, WebDriver or what, but what i'm sure is that this behavior started after the OS update, so it might have something to do with Mojave?\r\n\r\n## Expected Behavior -\r\nOnce the test is finished, the window nav closes\r\n\r\n## Actual Behavior -\r\n\r\nOnce the test is finished, the window nav closes but it remains in the dock, like in the photo.\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"ssorriba","id":36347993,"node_id":"MDQ6VXNlcjM2MzQ3OTkz","avatar_url":"https://avatars2.githubusercontent.com/u/36347993?v=4","gravatar_id":"","url":"https://api.github.com/users/ssorriba","html_url":"https://github.com/ssorriba","followers_url":"https://api.github.com/users/ssorriba/followers","following_url":"https://api.github.com/users/ssorriba/following{/other_user}","gists_url":"https://api.github.com/users/ssorriba/gists{/gist_id}","starred_url":"https://api.github.com/users/ssorriba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ssorriba/subscriptions","organizations_url":"https://api.github.com/users/ssorriba/orgs","repos_url":"https://api.github.com/users/ssorriba/repos","events_url":"https://api.github.com/users/ssorriba/events{/privacy}","received_events_url":"https://api.github.com/users/ssorriba/received_events","type":"User","site_admin":false}}", "commentIds":["435526286","435595117","435637962","492372801"], "labels":[]}