{"id":"1292", "title":"Add support for \"port 0\" dynamic port allocation", "body":"###### Background:
- Under Linux and Windows, bind()-ing to network port 0 assigns the application a free port from the dynamic port range.
###### Test setup:
- Selenium server standalone version 2.48.2
- CentOS 7
- Java 1.7.0_91
- Firefox 38.3.0
###### Issue description:
- When launching multiple Selenium instances on \"HtmlSuites\" it can be very useful for a user to be able to set the option `-port 0`. This would allow the Selenium application to receive a free port from the OS, removing the burden of finding one from the user's shoulders.
- The tested Selenium server accepts `-port 0` option and will correctly listen on the random free port offered by the OS, as can be seen via `lsof`.
-  The automatically generated configuration files for the browser (here: Firefox) do **not** reflect this assigned port but reference the port set via command-line: **0**.
###### Example:

Selenium command used, i.e. with your own favourite HtmlSuite:

```
java -jar selenium-server-standalone.jar -port 0 -htmlSuite \"*firefox\" \"http://some.url.ch/\" \"test_suite.html\" \"results.html\"
```

Launching `lsof -p <pID>` on the process ID will show the listening port:

```
[...]
java    9500 jenkins   11u  IPv6             190573       0t0      TCP *:51276 (LISTEN)
[...]
```

When looking at the `prefs.js` file generated for the Firefox instance, we can see that the URL wrongly references **localhost:0** instead of **localhost:51276** (URL %XX notation removed for readability):

```
[...]
user_pref(\"browser.startup.homepage\", \"chrome://src/content/TestRunner.html?auto=true&multiWindow=true&defaultLogLevel=info&baseUrl=http://some.url.ch/&resultsUrl=../postResults&test=http://localhost:0/selenium-server/tests/test_suite.html\");
[...]
```
###### Possible resolution:
- I'm not a programmer but the Java socket documentation has a [getLocalPort](https://docs.oracle.com/javase/8/docs/api/java/net/Socket.html#getLocalPort--) function which could be used to identify the effective port number on which Selenium is listening. This value should then override the command-line value.

This would really help in our current setup of Selenium test cases :-)

Kind regards,
Volker
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1292","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1292/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1292/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1292/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1292","id":118073606,"node_id":"MDU6SXNzdWUxMTgwNzM2MDY=","number":1292,"title":"Add support for \"port 0\" dynamic port allocation","user":{"login":"vflegel-sib","id":15673079,"node_id":"MDQ6VXNlcjE1NjczMDc5","avatar_url":"https://avatars1.githubusercontent.com/u/15673079?v=4","gravatar_id":"","url":"https://api.github.com/users/vflegel-sib","html_url":"https://github.com/vflegel-sib","followers_url":"https://api.github.com/users/vflegel-sib/followers","following_url":"https://api.github.com/users/vflegel-sib/following{/other_user}","gists_url":"https://api.github.com/users/vflegel-sib/gists{/gist_id}","starred_url":"https://api.github.com/users/vflegel-sib/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/vflegel-sib/subscriptions","organizations_url":"https://api.github.com/users/vflegel-sib/orgs","repos_url":"https://api.github.com/users/vflegel-sib/repos","events_url":"https://api.github.com/users/vflegel-sib/events{/privacy}","received_events_url":"https://api.github.com/users/vflegel-sib/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-11-20T16:15:24Z","updated_at":"2019-08-20T22:09:38Z","closed_at":"2015-11-20T17:18:15Z","author_association":"NONE","body":"###### Background:\n- Under Linux and Windows, bind()-ing to network port 0 assigns the application a free port from the dynamic port range.\n###### Test setup:\n- Selenium server standalone version 2.48.2\n- CentOS 7\n- Java 1.7.0_91\n- Firefox 38.3.0\n###### Issue description:\n- When launching multiple Selenium instances on \"HtmlSuites\" it can be very useful for a user to be able to set the option `-port 0`. This would allow the Selenium application to receive a free port from the OS, removing the burden of finding one from the user's shoulders.\n- The tested Selenium server accepts `-port 0` option and will correctly listen on the random free port offered by the OS, as can be seen via `lsof`.\n-  The automatically generated configuration files for the browser (here: Firefox) do **not** reflect this assigned port but reference the port set via command-line: **0**.\n###### Example:\n\nSelenium command used, i.e. with your own favourite HtmlSuite:\n\n```\njava -jar selenium-server-standalone.jar -port 0 -htmlSuite \"*firefox\" \"http://some.url.ch/\" \"test_suite.html\" \"results.html\"\n```\n\nLaunching `lsof -p <pID>` on the process ID will show the listening port:\n\n```\n[...]\njava    9500 jenkins   11u  IPv6             190573       0t0      TCP *:51276 (LISTEN)\n[...]\n```\n\nWhen looking at the `prefs.js` file generated for the Firefox instance, we can see that the URL wrongly references **localhost:0** instead of **localhost:51276** (URL %XX notation removed for readability):\n\n```\n[...]\nuser_pref(\"browser.startup.homepage\", \"chrome://src/content/TestRunner.html?auto=true&multiWindow=true&defaultLogLevel=info&baseUrl=http://some.url.ch/&resultsUrl=../postResults&test=http://localhost:0/selenium-server/tests/test_suite.html\");\n[...]\n```\n###### Possible resolution:\n- I'm not a programmer but the Java socket documentation has a [getLocalPort](https://docs.oracle.com/javase/8/docs/api/java/net/Socket.html#getLocalPort--) function which could be used to identify the effective port number on which Selenium is listening. This value should then override the command-line value.\n\nThis would really help in our current setup of Selenium test cases :-)\n\nKind regards,\nVolker\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["158464211"], "labels":[]}