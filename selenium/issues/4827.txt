{"id":"4827", "title":"Chrome driver reloaded on Clicking specific element on webpage", "body":"## Meta -
OS:  
Windows 7
Selenium Version:  
3.6
Browser:  
Chrome 

Currently problem I m facing is when I click on specific element in chrome browser the Webpage gets reloaded and makes my script to get failed

package pmo_automation;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.host.Location;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

public class loginPage  {
	
	
	public static void main(String[] args) throws IOException, InterruptedException  {
			
		
		System.setProperty(\"webdriver.chrome.driver\", \"C:/Users/aamir.fatimi/Downloads/chromedriver_win32/chromedriver.exe\");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(\"http://sohaib.ahmed:nov@1234@dbu-export-09/en/Pages/default.aspx/\");

WebElement eservices1 = driver.findElement(By.linkText(\"E-SERVICES\"));
		eservices1.click();
		
	 }
}
		", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4827","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4827/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4827/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4827/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4827","id":263898418,"node_id":"MDU6SXNzdWUyNjM4OTg0MTg=","number":4827,"title":"Chrome driver reloaded on Clicking specific element on webpage","user":{"login":"Fatimi","id":3796742,"node_id":"MDQ6VXNlcjM3OTY3NDI=","avatar_url":"https://avatars3.githubusercontent.com/u/3796742?v=4","gravatar_id":"","url":"https://api.github.com/users/Fatimi","html_url":"https://github.com/Fatimi","followers_url":"https://api.github.com/users/Fatimi/followers","following_url":"https://api.github.com/users/Fatimi/following{/other_user}","gists_url":"https://api.github.com/users/Fatimi/gists{/gist_id}","starred_url":"https://api.github.com/users/Fatimi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Fatimi/subscriptions","organizations_url":"https://api.github.com/users/Fatimi/orgs","repos_url":"https://api.github.com/users/Fatimi/repos","events_url":"https://api.github.com/users/Fatimi/events{/privacy}","received_events_url":"https://api.github.com/users/Fatimi/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-10-09T13:38:25Z","updated_at":"2019-08-17T05:09:43Z","closed_at":"2017-10-28T19:59:22Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 7\r\nSelenium Version:  \r\n3.6\r\nBrowser:  \r\nChrome \r\n\r\nCurrently problem I m facing is when I click on specific element in chrome browser the Webpage gets reloaded and makes my script to get failed\r\n\r\npackage pmo_automation;\r\n\r\nimport java.io.IOException;\r\nimport java.util.concurrent.TimeUnit;\r\n\r\nimport org.openqa.selenium.Alert;\r\nimport org.openqa.selenium.By;\r\nimport org.openqa.selenium.JavascriptExecutor;\r\nimport org.openqa.selenium.TimeoutException;\r\nimport org.openqa.selenium.WebDriver;\r\nimport org.openqa.selenium.WebElement;\r\nimport org.openqa.selenium.chrome.ChromeDriver;\r\nimport org.openqa.selenium.firefox.FirefoxDriver;\r\nimport org.openqa.selenium.security.Credentials;\r\nimport org.openqa.selenium.security.UserAndPassword;\r\nimport org.openqa.selenium.support.ui.ExpectedCondition;\r\nimport org.openqa.selenium.support.ui.ExpectedConditions;\r\nimport org.openqa.selenium.support.ui.WebDriverWait;\r\n\r\nimport com.gargoylesoftware.htmlunit.javascript.host.Location;\r\nimport com.gargoylesoftware.htmlunit.javascript.host.Window;\r\n\r\npublic class loginPage  {\r\n\t\r\n\t\r\n\tpublic static void main(String[] args) throws IOException, InterruptedException  {\r\n\t\t\t\r\n\t\t\r\n\t\tSystem.setProperty(\"webdriver.chrome.driver\", \"C:/Users/aamir.fatimi/Downloads/chromedriver_win32/chromedriver.exe\");\r\n\t\tWebDriver driver = new ChromeDriver();\r\n\t\tdriver.manage().window().maximize();\r\n\t\tdriver.navigate().to(\"http://sohaib.ahmed:nov@1234@dbu-export-09/en/Pages/default.aspx/\");\r\n\r\nWebElement eservices1 = driver.findElement(By.linkText(\"E-SERVICES\"));\r\n\t\teservices1.click();\r\n\t\t\r\n\t }\r\n}\r\n\t\t","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["335398988","340216056"], "labels":["R-awaiting answer"]}