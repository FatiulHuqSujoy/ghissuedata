{"id":"7017", "title":"Not able to login to the website using Alert.send_keys() in google chrome", "body":"I am not able to login to the site using Alert.send_keys()
My System configuration:
OS: Windows10 64bit
Browser Version: 72.0.3
Selenium: 3.141
Python: 3.7
Chrome Driver: 72.0.3

Code tried:1
+++++++++++
browser = webdriver.Chrome()
browser.get('http://10.179.230.232/desktop/')

alert = Alert(browser)
alert.send_keys(\"apc\"+str(Keys.TAB)+\"apc\")
alert.accept()
++++++++
Code Tried: 2
++++++++
browser = webdriver.Chrome()
browser.get('http://10.179.230.232/desktop/')

Alert.send_keys(\"apc\")
browser.switch_to.alert()
Alert.send_keys(\"apc\")
+++++++++
Error:
Traceback (most recent call last):
File \"C:/DataVerificationSQA/UITest/dceUITest.py\", line 47, in 
alert.send_keys(\"apc\"+str(Keys.TAB)+\"apc\")
File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\common\\alert.py\", line 105, in send_keys
self.driver.execute(Command.SET_ALERT_VALUE, {'text': keysToSend})
File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute
self.error_handler.check_response(response)
File \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response
raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.NoAlertPresentException: Message: no such alert
(Session info: chrome=72.0.3626.121)
(Driver info: chromedriver=72.0.3626.69 (3c16f8a135abc0d4da2dff33804db79b849a7c38),platform=Windows NT 10.0.14393 x86_64)

__
git

Firefox is working fine.
++++++++++
browser = webdriver.Firefox()
browser.get(r'http://10.179.230.232/desktop/')
alert = Alert(browser)
alert.send_keys(f'apc{Keys.TAB}apc')
alert.accept()
++++++++++++

Please help me to solve this issue.
Thanks", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7017","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7017/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7017/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7017/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7017","id":419174894,"node_id":"MDU6SXNzdWU0MTkxNzQ4OTQ=","number":7017,"title":"Not able to login to the website using Alert.send_keys() in google chrome","user":{"login":"arunkrh","id":26589115,"node_id":"MDQ6VXNlcjI2NTg5MTE1","avatar_url":"https://avatars2.githubusercontent.com/u/26589115?v=4","gravatar_id":"","url":"https://api.github.com/users/arunkrh","html_url":"https://github.com/arunkrh","followers_url":"https://api.github.com/users/arunkrh/followers","following_url":"https://api.github.com/users/arunkrh/following{/other_user}","gists_url":"https://api.github.com/users/arunkrh/gists{/gist_id}","starred_url":"https://api.github.com/users/arunkrh/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/arunkrh/subscriptions","organizations_url":"https://api.github.com/users/arunkrh/orgs","repos_url":"https://api.github.com/users/arunkrh/repos","events_url":"https://api.github.com/users/arunkrh/events{/privacy}","received_events_url":"https://api.github.com/users/arunkrh/received_events","type":"User","site_admin":false},"labels":[{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-03-10T11:27:06Z","updated_at":"2019-08-14T17:09:46Z","closed_at":"2019-03-29T15:20:58Z","author_association":"NONE","body":"I am not able to login to the site using Alert.send_keys()\r\nMy System configuration:\r\nOS: Windows10 64bit\r\nBrowser Version: 72.0.3\r\nSelenium: 3.141\r\nPython: 3.7\r\nChrome Driver: 72.0.3\r\n\r\nCode tried:1\r\n+++++++++++\r\nbrowser = webdriver.Chrome()\r\nbrowser.get('http://10.179.230.232/desktop/')\r\n\r\nalert = Alert(browser)\r\nalert.send_keys(\"apc\"+str(Keys.TAB)+\"apc\")\r\nalert.accept()\r\n++++++++\r\nCode Tried: 2\r\n++++++++\r\nbrowser = webdriver.Chrome()\r\nbrowser.get('http://10.179.230.232/desktop/')\r\n\r\nAlert.send_keys(\"apc\")\r\nbrowser.switch_to.alert()\r\nAlert.send_keys(\"apc\")\r\n+++++++++\r\nError:\r\nTraceback (most recent call last):\r\nFile \"C:/DataVerificationSQA/UITest/dceUITest.py\", line 47, in \r\nalert.send_keys(\"apc\"+str(Keys.TAB)+\"apc\")\r\nFile \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\common\\alert.py\", line 105, in send_keys\r\nself.driver.execute(Command.SET_ALERT_VALUE, {'text': keysToSend})\r\nFile \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\webdriver.py\", line 321, in execute\r\nself.error_handler.check_response(response)\r\nFile \"C:\\Program Files\\Python37\\lib\\site-packages\\selenium\\webdriver\\remote\\errorhandler.py\", line 242, in check_response\r\nraise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.NoAlertPresentException: Message: no such alert\r\n(Session info: chrome=72.0.3626.121)\r\n(Driver info: chromedriver=72.0.3626.69 (3c16f8a135abc0d4da2dff33804db79b849a7c38),platform=Windows NT 10.0.14393 x86_64)\r\n\r\n__\r\ngit\r\n\r\nFirefox is working fine.\r\n++++++++++\r\nbrowser = webdriver.Firefox()\r\nbrowser.get(r'http://10.179.230.232/desktop/')\r\nalert = Alert(browser)\r\nalert.send_keys(f'apc{Keys.TAB}apc')\r\nalert.accept()\r\n++++++++++++\r\n\r\nPlease help me to solve this issue.\r\nThanks","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["475951090","475987027","478037516"], "labels":["R-awaiting answer"]}