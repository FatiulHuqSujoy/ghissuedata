{"id":"5172", "title":"Selenium IDE: Error loading test case: no command found.", "body":"## Meta -
OS:  
<!-- Windows 10? OSX? -->
Selenium Version:  
Selenium IDE 2.9.1.1
Browser:  
Firefox 48.0.2

I am a new user of Selenium, and I am trying out my first automation using Selenium IDE. I have recorded a test case & it ran OK. Then I saved the file. When I re-open it & try to run it, I get the error message: Error loading test case: no command found.
I have checked other issues mentioning this problem, but none of them contain an actual answer.

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -

## Actual Behavior -

## Steps to reproduce -
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5172","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5172/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5172/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5172/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5172","id":279451192,"node_id":"MDU6SXNzdWUyNzk0NTExOTI=","number":5172,"title":"Selenium IDE: Error loading test case: no command found.","user":{"login":"staplesj","id":26226414,"node_id":"MDQ6VXNlcjI2MjI2NDE0","avatar_url":"https://avatars2.githubusercontent.com/u/26226414?v=4","gravatar_id":"","url":"https://api.github.com/users/staplesj","html_url":"https://github.com/staplesj","followers_url":"https://api.github.com/users/staplesj/followers","following_url":"https://api.github.com/users/staplesj/following{/other_user}","gists_url":"https://api.github.com/users/staplesj/gists{/gist_id}","starred_url":"https://api.github.com/users/staplesj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/staplesj/subscriptions","organizations_url":"https://api.github.com/users/staplesj/orgs","repos_url":"https://api.github.com/users/staplesj/repos","events_url":"https://api.github.com/users/staplesj/events{/privacy}","received_events_url":"https://api.github.com/users/staplesj/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-12-05T16:56:53Z","updated_at":"2019-08-16T22:09:47Z","closed_at":"2017-12-06T04:55:12Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  \r\nSelenium IDE 2.9.1.1\r\nBrowser:  \r\nFirefox 48.0.2\r\n\r\nI am a new user of Selenium, and I am trying out my first automation using Selenium IDE. I have recorded a test case & it ran OK. Then I saved the file. When I re-open it & try to run it, I get the error message: Error loading test case: no command found.\r\nI have checked other issues mentioning this problem, but none of them contain an actual answer.\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\n\r\n## Actual Behavior -\r\n\r\n## Steps to reproduce -\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["349533370"], "labels":[]}