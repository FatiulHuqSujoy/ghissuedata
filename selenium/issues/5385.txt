{"id":"5385", "title":"Option \"-enablePassThrough false\" does not apply", "body":"## Meta -
OS:  
Ubuntu Linux 16.04
Selenium Version:  
3.8.1
Browser:  
Firefox (docker image selenium/node-firefox-debug)

## Expected Behavior -
Option \"enablePassThrough\" is set appropriately
## Actual Behavior -
Option \"enablePassThrough\" is set to default (true)
## Steps to reproduce -
launch java -Xms500m -Xmx500m -jar /opt/selenium/selenium-server-standalone.jar -role node -hub http://localhost:4444/grid/register -nodeConfig /opt/selenium/config.json **-enablePassThrough false**

LOG from selenium HUB:
`11:13:30.392 DEBUG - getting the following registration request  : {\"class\":\"org.openqa.grid.common.RegistrationRequest\",\"name\":null,\"description\":null,\"configuration\":{\"remoteHost\":\"http://10.64.209.10:5555\",\"hubHost\":\"localhost\",\"hubPort\":4444,\"id\":\"http://10.64.209.10:5555\",\"capabilities\":[{\"applicationName\":\"\",\"browserName\":\"firefox\",\"maxInstances\":1,\"moz:firefoxOptions\":{\"log\":{\"level\":\"info\"}},\"platform\":\"LINUX\",\"se:CONFIG_UUID\":\"0359fb34-6c18-47c6-bf17-c61fe05cd835\",\"seleniumProtocol\":\"WebDriver\",\"version\":\"57.0.2\"}],\"downPollingLimit\":2,\"hub\":\"http://selenium-hub:4444/grid/register\",\"nodePolling\":5000,\"nodeStatusCheckTimeout\":5000,\"proxy\":\"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",\"register\":true,\"registerCycle\":5000,\"unregisterIfStillDownAfter\":60000,\"cleanUpCycle\":null,\"custom\":{},\"host\":\"10.64.209.10\",\"maxSession\":1,\"servlets\":[],\"withoutServlets\":[],\"browserTimeout\":240,\"debug\":false,\"jettyMaxThreads\":null,\"log\":null,\"port\":5555,\"role\":\"node\",\"timeout\":15,\"enablePassThrough\":true}}`

From selenium HUB console:
```
browserTimeout: 240
debug: false
help: false
jettyMaxThreads: -1
port: 5555
role: node
timeout: 15
enablePassThrough: true
cleanUpCycle: 5000
host: 10.64.209.10
maxSession: 1
capabilities: Capabilities {applicationName: , browserName: firefox, maxInstances: 1, moz:firefoxOptions: {log: {level: info}}, platform: LINUX, se:CONFIG_UUID: 0359fb34-6c18-47c6-bf17-c61..., seleniumProtocol: WebDriver, version: 57.0.2}
downPollingLimit: 2
hub: http://localhost:4444/grid/register
id: http://10.64.209.10:5555
hubHost: selenium-hub
hubPort: 4444
nodePolling: 5000
nodeStatusCheckTimeout: 5000
proxy: org.openqa.grid.selenium.proxy.DefaultRemoteProxy
register: true
registerCycle: 5000
remoteHost: http://10.64.209.10:5555
unregisterIfStillDownAfter: 60000
```

Works if add the option to JSON config. But even in this case changes are not shown in hub console.

Launching HUB with $SE_OPTS=\"-enablePassThrough false\":
from LOG:
```
starting selenium hub with configuration:
{
  \"host\": null,
  \"port\": 4444,
  \"role\": \"hub\",
  \"maxSession\": 5,
  \"newSessionWaitTimeout\": -1,
  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",
  \"throwOnCapabilityNotPresent\": true,
  \"jettyMaxThreads\": -1,
  \"cleanUpCycle\": 5000,
  \"browserTimeout\": 240,
  \"timeout\": 15,
  \"debug\": true
}
appending selenium options: -enablePassThrough false
11:27:38.517 INFO - Selenium build info: version: '3.8.1', revision: '6e95a6684b'
11:27:38.517 INFO - Launching Selenium Grid hub
```

From HUB console:
```
Config for the hub :
browserTimeout : 240
debug : true
help : false
jettyMaxThreads : -1
port : 4444
role : hub
timeout : 15
enablePassThrough : false
cleanUpCycle : 5000
host : 10.64.214.30
maxSession : 5
hubConfig : /opt/selenium/config.json
capabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher
newSessionWaitTimeout : -1
throwOnCapabilityNotPresent : true
registry : org.openqa.grid.internal.DefaultGridRegistry
Config details :
hub launched with : -browserTimeout 240 -debug true -help false -jettyMaxThreads -1 -port 4444 -role hub -timeout 15 -enablePassThrough false -cleanUpCycle 5000 -host 10.64.214.30 -maxSession 5 -hubConfig /opt/selenium/config.json -capabilityMatcher org.openqa.grid.internal.utils.DefaultCapabilityMatcher -newSessionWaitTimeout -1 -throwOnCapabilityNotPresent true -registry org.openqa.grid.internal.DefaultGridRegistry
the final configuration comes from :
the default :
browserTimeout : 0
debug : false
help : false
port : 4444
role : hub
timeout : 1800
enablePassThrough : true
cleanUpCycle : 5000
capabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher
newSessionWaitTimeout : -1
throwOnCapabilityNotPresent : true
registry : org.openqa.grid.internal.DefaultGridRegistry

updated with params :
browserTimeout : 240
debug : false
help : false
jettyMaxThreads : -1
port : 4444
role : hub
timeout : 15
enablePassThrough : true
cleanUpCycle : 5000
maxSession : 5
capabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher
newSessionWaitTimeout : -1
throwOnCapabilityNotPresent : true
registry : org.openqa.grid.internal.DefaultGridRegistry
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5385","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5385/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5385/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5385/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5385","id":290802888,"node_id":"MDU6SXNzdWUyOTA4MDI4ODg=","number":5385,"title":"Option \"-enablePassThrough false\" does not apply","user":{"login":"dev-e","id":22076937,"node_id":"MDQ6VXNlcjIyMDc2OTM3","avatar_url":"https://avatars1.githubusercontent.com/u/22076937?v=4","gravatar_id":"","url":"https://api.github.com/users/dev-e","html_url":"https://github.com/dev-e","followers_url":"https://api.github.com/users/dev-e/followers","following_url":"https://api.github.com/users/dev-e/following{/other_user}","gists_url":"https://api.github.com/users/dev-e/gists{/gist_id}","starred_url":"https://api.github.com/users/dev-e/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dev-e/subscriptions","organizations_url":"https://api.github.com/users/dev-e/orgs","repos_url":"https://api.github.com/users/dev-e/repos","events_url":"https://api.github.com/users/dev-e/events{/privacy}","received_events_url":"https://api.github.com/users/dev-e/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-01-23T11:31:22Z","updated_at":"2019-08-16T16:09:40Z","closed_at":"2018-01-26T09:13:10Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nUbuntu Linux 16.04\r\nSelenium Version:  \r\n3.8.1\r\nBrowser:  \r\nFirefox (docker image selenium/node-firefox-debug)\r\n\r\n## Expected Behavior -\r\nOption \"enablePassThrough\" is set appropriately\r\n## Actual Behavior -\r\nOption \"enablePassThrough\" is set to default (true)\r\n## Steps to reproduce -\r\nlaunch java -Xms500m -Xmx500m -jar /opt/selenium/selenium-server-standalone.jar -role node -hub http://localhost:4444/grid/register -nodeConfig /opt/selenium/config.json **-enablePassThrough false**\r\n\r\nLOG from selenium HUB:\r\n`11:13:30.392 DEBUG - getting the following registration request  : {\"class\":\"org.openqa.grid.common.RegistrationRequest\",\"name\":null,\"description\":null,\"configuration\":{\"remoteHost\":\"http://10.64.209.10:5555\",\"hubHost\":\"localhost\",\"hubPort\":4444,\"id\":\"http://10.64.209.10:5555\",\"capabilities\":[{\"applicationName\":\"\",\"browserName\":\"firefox\",\"maxInstances\":1,\"moz:firefoxOptions\":{\"log\":{\"level\":\"info\"}},\"platform\":\"LINUX\",\"se:CONFIG_UUID\":\"0359fb34-6c18-47c6-bf17-c61fe05cd835\",\"seleniumProtocol\":\"WebDriver\",\"version\":\"57.0.2\"}],\"downPollingLimit\":2,\"hub\":\"http://selenium-hub:4444/grid/register\",\"nodePolling\":5000,\"nodeStatusCheckTimeout\":5000,\"proxy\":\"org.openqa.grid.selenium.proxy.DefaultRemoteProxy\",\"register\":true,\"registerCycle\":5000,\"unregisterIfStillDownAfter\":60000,\"cleanUpCycle\":null,\"custom\":{},\"host\":\"10.64.209.10\",\"maxSession\":1,\"servlets\":[],\"withoutServlets\":[],\"browserTimeout\":240,\"debug\":false,\"jettyMaxThreads\":null,\"log\":null,\"port\":5555,\"role\":\"node\",\"timeout\":15,\"enablePassThrough\":true}}`\r\n\r\nFrom selenium HUB console:\r\n```\r\nbrowserTimeout: 240\r\ndebug: false\r\nhelp: false\r\njettyMaxThreads: -1\r\nport: 5555\r\nrole: node\r\ntimeout: 15\r\nenablePassThrough: true\r\ncleanUpCycle: 5000\r\nhost: 10.64.209.10\r\nmaxSession: 1\r\ncapabilities: Capabilities {applicationName: , browserName: firefox, maxInstances: 1, moz:firefoxOptions: {log: {level: info}}, platform: LINUX, se:CONFIG_UUID: 0359fb34-6c18-47c6-bf17-c61..., seleniumProtocol: WebDriver, version: 57.0.2}\r\ndownPollingLimit: 2\r\nhub: http://localhost:4444/grid/register\r\nid: http://10.64.209.10:5555\r\nhubHost: selenium-hub\r\nhubPort: 4444\r\nnodePolling: 5000\r\nnodeStatusCheckTimeout: 5000\r\nproxy: org.openqa.grid.selenium.proxy.DefaultRemoteProxy\r\nregister: true\r\nregisterCycle: 5000\r\nremoteHost: http://10.64.209.10:5555\r\nunregisterIfStillDownAfter: 60000\r\n```\r\n\r\nWorks if add the option to JSON config. But even in this case changes are not shown in hub console.\r\n\r\nLaunching HUB with $SE_OPTS=\"-enablePassThrough false\":\r\nfrom LOG:\r\n```\r\nstarting selenium hub with configuration:\r\n{\r\n  \"host\": null,\r\n  \"port\": 4444,\r\n  \"role\": \"hub\",\r\n  \"maxSession\": 5,\r\n  \"newSessionWaitTimeout\": -1,\r\n  \"capabilityMatcher\": \"org.openqa.grid.internal.utils.DefaultCapabilityMatcher\",\r\n  \"throwOnCapabilityNotPresent\": true,\r\n  \"jettyMaxThreads\": -1,\r\n  \"cleanUpCycle\": 5000,\r\n  \"browserTimeout\": 240,\r\n  \"timeout\": 15,\r\n  \"debug\": true\r\n}\r\nappending selenium options: -enablePassThrough false\r\n11:27:38.517 INFO - Selenium build info: version: '3.8.1', revision: '6e95a6684b'\r\n11:27:38.517 INFO - Launching Selenium Grid hub\r\n```\r\n\r\nFrom HUB console:\r\n```\r\nConfig for the hub :\r\nbrowserTimeout : 240\r\ndebug : true\r\nhelp : false\r\njettyMaxThreads : -1\r\nport : 4444\r\nrole : hub\r\ntimeout : 15\r\nenablePassThrough : false\r\ncleanUpCycle : 5000\r\nhost : 10.64.214.30\r\nmaxSession : 5\r\nhubConfig : /opt/selenium/config.json\r\ncapabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher\r\nnewSessionWaitTimeout : -1\r\nthrowOnCapabilityNotPresent : true\r\nregistry : org.openqa.grid.internal.DefaultGridRegistry\r\nConfig details :\r\nhub launched with : -browserTimeout 240 -debug true -help false -jettyMaxThreads -1 -port 4444 -role hub -timeout 15 -enablePassThrough false -cleanUpCycle 5000 -host 10.64.214.30 -maxSession 5 -hubConfig /opt/selenium/config.json -capabilityMatcher org.openqa.grid.internal.utils.DefaultCapabilityMatcher -newSessionWaitTimeout -1 -throwOnCapabilityNotPresent true -registry org.openqa.grid.internal.DefaultGridRegistry\r\nthe final configuration comes from :\r\nthe default :\r\nbrowserTimeout : 0\r\ndebug : false\r\nhelp : false\r\nport : 4444\r\nrole : hub\r\ntimeout : 1800\r\nenablePassThrough : true\r\ncleanUpCycle : 5000\r\ncapabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher\r\nnewSessionWaitTimeout : -1\r\nthrowOnCapabilityNotPresent : true\r\nregistry : org.openqa.grid.internal.DefaultGridRegistry\r\n\r\nupdated with params :\r\nbrowserTimeout : 240\r\ndebug : false\r\nhelp : false\r\njettyMaxThreads : -1\r\nport : 4444\r\nrole : hub\r\ntimeout : 15\r\nenablePassThrough : true\r\ncleanUpCycle : 5000\r\nmaxSession : 5\r\ncapabilityMatcher : org.openqa.grid.internal.utils.DefaultCapabilityMatcher\r\nnewSessionWaitTimeout : -1\r\nthrowOnCapabilityNotPresent : true\r\nregistry : org.openqa.grid.internal.DefaultGridRegistry\r\n```","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["360725415"], "labels":[]}