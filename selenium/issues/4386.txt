{"id":"4386", "title":"[ruby] How to add debuggerAddress experimental option?", "body":"## Meta -
OS:  
archlinux
Selenium Version:  
gem selenium-webdriver (3.4.4)
Browser:  
google-chrome-stable 60.0.3112.78

## Discription
I want to attach selenium to existing session of chrome, and then tweak pages using my chrome profile, but from terminal
I have found that this is [feasible using debuggerAddress](https://bugs.chromium.org/p/chromedriver/issues/detail?id=710#c3)

## Excerpt - 
```
Launch Chrome from command prompt:
chrome.exe --remote-debugging-port=8181

Sample Code:
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption(\"debuggerAddress\", \"127.0.0.1:8181\");
		
		WebDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		driver.get(\"http://www.google.com\");
		driver.findElement(By.name(\"q\")).sendKeys(\"test\");
		driver.quit();

```

## Steps to reproduce -
```
// close all chrome windows
$ google-chrome-stable --remote-debugging-port=4444
// chrome opened with my profile
$ pry
>  require 'selenium-webdriver'
>  Selenium::WebDriver::Chrome.path = '/usr/bin/google-chrome-stable'
>  Selenium::WebDriver::Chrome.driver_path = '/usr/bin/chromedriver'
>  options = Selenium::WebDriver::Chrome::Options.new
>  # options.add_experimental_option(\"debuggerAddress\", \"127.0.0.1:4444\") # how????
>  driver = Selenium::WebDriver.for :chrome, options: options

```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4386","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4386/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4386/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4386/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4386","id":246580287,"node_id":"MDU6SXNzdWUyNDY1ODAyODc=","number":4386,"title":"[ruby] How to add debuggerAddress experimental option?","user":{"login":"srghma","id":7573215,"node_id":"MDQ6VXNlcjc1NzMyMTU=","avatar_url":"https://avatars2.githubusercontent.com/u/7573215?v=4","gravatar_id":"","url":"https://api.github.com/users/srghma","html_url":"https://github.com/srghma","followers_url":"https://api.github.com/users/srghma/followers","following_url":"https://api.github.com/users/srghma/following{/other_user}","gists_url":"https://api.github.com/users/srghma/gists{/gist_id}","starred_url":"https://api.github.com/users/srghma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/srghma/subscriptions","organizations_url":"https://api.github.com/users/srghma/orgs","repos_url":"https://api.github.com/users/srghma/repos","events_url":"https://api.github.com/users/srghma/events{/privacy}","received_events_url":"https://api.github.com/users/srghma/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-07-30T12:15:03Z","updated_at":"2019-08-18T01:09:54Z","closed_at":"2017-07-30T13:45:04Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\narchlinux\r\nSelenium Version:  \r\ngem selenium-webdriver (3.4.4)\r\nBrowser:  \r\ngoogle-chrome-stable 60.0.3112.78\r\n\r\n## Discription\r\nI want to attach selenium to existing session of chrome, and then tweak pages using my chrome profile, but from terminal\r\nI have found that this is [feasible using debuggerAddress](https://bugs.chromium.org/p/chromedriver/issues/detail?id=710#c3)\r\n\r\n## Excerpt - \r\n```\r\nLaunch Chrome from command prompt:\r\nchrome.exe --remote-debugging-port=8181\r\n\r\nSample Code:\r\n\t\tChromeOptions options = new ChromeOptions();\r\n\t\toptions.setExperimentalOption(\"debuggerAddress\", \"127.0.0.1:8181\");\r\n\t\t\r\n\t\tWebDriver driver = new ChromeDriver(options);\r\n\t\tdriver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);\r\n\t\tdriver.get(\"http://www.google.com\");\r\n\t\tdriver.findElement(By.name(\"q\")).sendKeys(\"test\");\r\n\t\tdriver.quit();\r\n\r\n```\r\n\r\n## Steps to reproduce -\r\n```\r\n// close all chrome windows\r\n$ google-chrome-stable --remote-debugging-port=4444\r\n// chrome opened with my profile\r\n$ pry\r\n>  require 'selenium-webdriver'\r\n>  Selenium::WebDriver::Chrome.path = '/usr/bin/google-chrome-stable'\r\n>  Selenium::WebDriver::Chrome.driver_path = '/usr/bin/chromedriver'\r\n>  options = Selenium::WebDriver::Chrome::Options.new\r\n>  # options.add_experimental_option(\"debuggerAddress\", \"127.0.0.1:4444\") # how????\r\n>  driver = Selenium::WebDriver.for :chrome, options: options\r\n\r\n```","closed_by":{"login":"srghma","id":7573215,"node_id":"MDQ6VXNlcjc1NzMyMTU=","avatar_url":"https://avatars2.githubusercontent.com/u/7573215?v=4","gravatar_id":"","url":"https://api.github.com/users/srghma","html_url":"https://github.com/srghma","followers_url":"https://api.github.com/users/srghma/followers","following_url":"https://api.github.com/users/srghma/following{/other_user}","gists_url":"https://api.github.com/users/srghma/gists{/gist_id}","starred_url":"https://api.github.com/users/srghma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/srghma/subscriptions","organizations_url":"https://api.github.com/users/srghma/orgs","repos_url":"https://api.github.com/users/srghma/repos","events_url":"https://api.github.com/users/srghma/events{/privacy}","received_events_url":"https://api.github.com/users/srghma/received_events","type":"User","site_admin":false}}", "commentIds":["318902769"], "labels":[]}