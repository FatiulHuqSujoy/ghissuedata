{"id":"854", "title":"How to use Chrome profile other than default?", "body":"### Documentation:

https://sites.google.com/a/chromium.org/chromedriver/capabilities

>  you can use the user-data-dir Chrome command-line switch to tell Chrome which profile to use:

```
ChromeOptions options = new ChromeOptions();
options.addArguments(\"user-data-dir=/path/to/your/custom/profile\");
```

---

I'm using - https://code.google.com/p/selenium/wiki/WebDriverJs 2.46.1 - and the following code:

```
var webdriver = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');
var o = new chrome.Options();
o.addArguments(\"user-data-dir=c:/Users/a-miste/AppData/Local/Google/Chrome/User Data/\");
var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).setChromeOptions(o).build(); 

driver.get('http://www.facebook.com');

driver.wait(function() {
 return driver.getTitle().then(function(title) {
   console.log(\"title: \" + title);
   return true;
 });
}, 5000);
```

It was partially created thanks to this StackOverflow question: http://stackoverflow.com/questions/25779027/load-default-chrome-profile-with-webdriverjs-selenium

As I navigate to `chrome://version/` the profile path is correct:

> C:\\Users\\a-miste\\AppData\\Local\\Google\\Chrome\\User Data\\Default

However the whole thing crashes and I do not navigate to Facebook (I stay on the homepage)

```
C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\goog\\async\\nexttick.js:39
  goog.global.setTimeout(function() { throw exception; }, 0);
                                            ^
UnknownError: unknown error: Chrome failed to start: crashed
  (Driver info: chromedriver=2.16.333243 (0bfa1d3575fc1044244f21ddb82bf870944ef961),platform=Windows NT 6.3 x86
_64)
    at new bot.Error (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\atoms\\error.js:108:18)
    at Object.bot.response.checkResponse (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\atoms\\res
ponse.js:109:9)
    at C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webdriver.js:160:24
    at promise.ControlFlow.runInFrame_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../web
driver/promise.js:1857:20)
    at goog.defineClass.notify (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdriver/p
romise.js:2448:25)
    at promise.Promise.notify_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdriver/p
romise.js:564:12)
    at Array.forEach (native)
    at promise.Promise.notifyAll_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdrive
r/promise.js:553:15)
    at goog.async.run.processWorkQueue (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\goog\\async\\
run.js:125:21)
    at runMicrotasksCallback (node.js:337:7)
From: Task: WebDriver.createSession()
    at Function.webdriver.WebDriver.acquireSession_ (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\li
b\\webdriver\\webdriver.js:157:22)
    at Function.webdriver.WebDriver.createSession (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\
webdriver\\webdriver.js:131:30)
    at new Driver (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\chrome.js:783:36)
    at Builder.build (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\builder.js:454:14)
    at Object.<anonymous> (C:\\Michal\\Code\\Selenium\\3-stack.js:5:108)
    at Module._compile (module.js:460:26)
    at Object.Module._extensions..js (module.js:478:10)
    at Module.load (module.js:355:32)
    at Function.Module._load (module.js:310:12)
    at Function.Module.runMain (module.js:501:10)
From: Task: WebDriver.navigate().to(http://www.facebook.com)
    at webdriver.WebDriver.schedule (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webd
river.js:362:15)
    at webdriver.WebDriver.Navigation.to (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver
\\webdriver.js:1099:23)
    at webdriver.WebDriver.get (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webdriver
.js:793:26)
    at Object.<anonymous> (C:\\Michal\\Code\\Selenium\\3-stack.js:8:8)
    at Module._compile (module.js:460:26)
    at Object.Module._extensions..js (module.js:478:10)
    at Module.load (module.js:355:32)
    at Function.Module._load (module.js:310:12)
    at Function.Module.runMain (module.js:501:10)
    at startup (node.js:129:16)
```

---
### Using different (non-default) profile

As I change the profile setting line to:
`\"user-data-dir=c:/Users/a-miste/AppData/Local/Google/Chrome/User Data/Profile 1\"`

The `chrome://version` shows the profile path as `c:\\Users\\a-miste\\AppData\\Local\\Google\\Chrome\\User Data\\Profile 1\\Default` (note the **Default** appended by default)

---
### Copying profile to a new location

If I try using different (non-default) profile **Default** is appended. To circumvent that I copied **Profile 1** to the following directory structure:

![directory structure](https://cloud.githubusercontent.com/assets/249703/8988373/b693c252-36dd-11e5-9075-27b33ed79a0c.PNG)

And now it works just fine...

---

So the questions are:
- how to use Default profile without crashing?
- how to use other profiles without necessity of copying them?
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/854","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/854/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/854/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/854/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/854","id":98207918,"node_id":"MDU6SXNzdWU5ODIwNzkxOA==","number":854,"title":"How to use Chrome profile other than default?","user":{"login":"stefek99","id":249703,"node_id":"MDQ6VXNlcjI0OTcwMw==","avatar_url":"https://avatars3.githubusercontent.com/u/249703?v=4","gravatar_id":"","url":"https://api.github.com/users/stefek99","html_url":"https://github.com/stefek99","followers_url":"https://api.github.com/users/stefek99/followers","following_url":"https://api.github.com/users/stefek99/following{/other_user}","gists_url":"https://api.github.com/users/stefek99/gists{/gist_id}","starred_url":"https://api.github.com/users/stefek99/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/stefek99/subscriptions","organizations_url":"https://api.github.com/users/stefek99/orgs","repos_url":"https://api.github.com/users/stefek99/repos","events_url":"https://api.github.com/users/stefek99/events{/privacy}","received_events_url":"https://api.github.com/users/stefek99/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2015-07-30T16:15:32Z","updated_at":"2019-08-14T23:09:56Z","closed_at":"2015-07-30T17:59:24Z","author_association":"NONE","body":"### Documentation:\n\nhttps://sites.google.com/a/chromium.org/chromedriver/capabilities\n\n>  you can use the user-data-dir Chrome command-line switch to tell Chrome which profile to use:\n\n```\nChromeOptions options = new ChromeOptions();\noptions.addArguments(\"user-data-dir=/path/to/your/custom/profile\");\n```\n\n---\n\nI'm using - https://code.google.com/p/selenium/wiki/WebDriverJs 2.46.1 - and the following code:\n\n```\nvar webdriver = require('selenium-webdriver');\nvar chrome = require('selenium-webdriver/chrome');\nvar o = new chrome.Options();\no.addArguments(\"user-data-dir=c:/Users/a-miste/AppData/Local/Google/Chrome/User Data/\");\nvar driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).setChromeOptions(o).build(); \n\ndriver.get('http://www.facebook.com');\n\ndriver.wait(function() {\n return driver.getTitle().then(function(title) {\n   console.log(\"title: \" + title);\n   return true;\n });\n}, 5000);\n```\n\nIt was partially created thanks to this StackOverflow question: http://stackoverflow.com/questions/25779027/load-default-chrome-profile-with-webdriverjs-selenium\n\nAs I navigate to `chrome://version/` the profile path is correct:\n\n> C:\\Users\\a-miste\\AppData\\Local\\Google\\Chrome\\User Data\\Default\n\nHowever the whole thing crashes and I do not navigate to Facebook (I stay on the homepage)\n\n```\nC:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\goog\\async\\nexttick.js:39\n  goog.global.setTimeout(function() { throw exception; }, 0);\n                                            ^\nUnknownError: unknown error: Chrome failed to start: crashed\n  (Driver info: chromedriver=2.16.333243 (0bfa1d3575fc1044244f21ddb82bf870944ef961),platform=Windows NT 6.3 x86\n_64)\n    at new bot.Error (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\atoms\\error.js:108:18)\n    at Object.bot.response.checkResponse (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\atoms\\res\nponse.js:109:9)\n    at C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webdriver.js:160:24\n    at promise.ControlFlow.runInFrame_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../web\ndriver/promise.js:1857:20)\n    at goog.defineClass.notify (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdriver/p\nromise.js:2448:25)\n    at promise.Promise.notify_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdriver/p\nromise.js:564:12)\n    at Array.forEach (native)\n    at promise.Promise.notifyAll_ (C:/Michal/Code/Selenium/node_modules/selenium-webdriver/lib/goog/../webdrive\nr/promise.js:553:15)\n    at goog.async.run.processWorkQueue (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\goog\\async\\\nrun.js:125:21)\n    at runMicrotasksCallback (node.js:337:7)\nFrom: Task: WebDriver.createSession()\n    at Function.webdriver.WebDriver.acquireSession_ (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\li\nb\\webdriver\\webdriver.js:157:22)\n    at Function.webdriver.WebDriver.createSession (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\\nwebdriver\\webdriver.js:131:30)\n    at new Driver (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\chrome.js:783:36)\n    at Builder.build (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\builder.js:454:14)\n    at Object.<anonymous> (C:\\Michal\\Code\\Selenium\\3-stack.js:5:108)\n    at Module._compile (module.js:460:26)\n    at Object.Module._extensions..js (module.js:478:10)\n    at Module.load (module.js:355:32)\n    at Function.Module._load (module.js:310:12)\n    at Function.Module.runMain (module.js:501:10)\nFrom: Task: WebDriver.navigate().to(http://www.facebook.com)\n    at webdriver.WebDriver.schedule (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webd\nriver.js:362:15)\n    at webdriver.WebDriver.Navigation.to (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\n\\webdriver.js:1099:23)\n    at webdriver.WebDriver.get (C:\\Michal\\Code\\Selenium\\node_modules\\selenium-webdriver\\lib\\webdriver\\webdriver\n.js:793:26)\n    at Object.<anonymous> (C:\\Michal\\Code\\Selenium\\3-stack.js:8:8)\n    at Module._compile (module.js:460:26)\n    at Object.Module._extensions..js (module.js:478:10)\n    at Module.load (module.js:355:32)\n    at Function.Module._load (module.js:310:12)\n    at Function.Module.runMain (module.js:501:10)\n    at startup (node.js:129:16)\n```\n\n---\n### Using different (non-default) profile\n\nAs I change the profile setting line to:\n`\"user-data-dir=c:/Users/a-miste/AppData/Local/Google/Chrome/User Data/Profile 1\"`\n\nThe `chrome://version` shows the profile path as `c:\\Users\\a-miste\\AppData\\Local\\Google\\Chrome\\User Data\\Profile 1\\Default` (note the **Default** appended by default)\n\n---\n### Copying profile to a new location\n\nIf I try using different (non-default) profile **Default** is appended. To circumvent that I copied **Profile 1** to the following directory structure:\n\n![directory structure](https://cloud.githubusercontent.com/assets/249703/8988373/b693c252-36dd-11e5-9075-27b33ed79a0c.PNG)\n\nAnd now it works just fine...\n\n---\n\nSo the questions are:\n- how to use Default profile without crashing?\n- how to use other profiles without necessity of copying them?\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["126418620","126511804","126524906","133352053","237458142","398239635","455804126"], "labels":[]}