{"id":"7323", "title":"NullReferenceException exception thrown when getting browser logs", "body":"## 🐛 Bug Report

When trying to get AvailableLogTypes from logs. the NullReferenceException is thrown.

Same happens on Chrome and Firefox.
Basically same issue as reported while ago https://github.com/SeleniumHQ/selenium/issues/5842.

## To Reproduce
Try to get log types `driver.Manage().Logs.AvailableLogTypes`
or browser logs directly `driver.Manage().GetLog(LogType.Browser)`

## Expected behavior
Do not throw an exception.

## Test script or set of commands reproducing this issue

```
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
_driver = new ChromeDriver(chromeOptions);
_driver.Url = \"https://github.com\";
var logs = _driver.Manage().Logs;
var logTypes = logs.AvailableLogTypes; //NullReferenceException thrown here
```

## Environment

OS: Windows 10
Browser: Chrome, Firefox
Browser version: Chrome 75.0.3770.100, Firefox 67.0.2
Browser Driver version: ChromeDriver 75.0.3770.90, GeckoDriver 0.24
Language Bindings version: C# 3.141", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7323","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7323/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7323/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7323/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7323","id":459996100,"node_id":"MDU6SXNzdWU0NTk5OTYxMDA=","number":7323,"title":"NullReferenceException exception thrown when getting browser logs","user":{"login":"richja","id":3606623,"node_id":"MDQ6VXNlcjM2MDY2MjM=","avatar_url":"https://avatars3.githubusercontent.com/u/3606623?v=4","gravatar_id":"","url":"https://api.github.com/users/richja","html_url":"https://github.com/richja","followers_url":"https://api.github.com/users/richja/followers","following_url":"https://api.github.com/users/richja/following{/other_user}","gists_url":"https://api.github.com/users/richja/gists{/gist_id}","starred_url":"https://api.github.com/users/richja/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/richja/subscriptions","organizations_url":"https://api.github.com/users/richja/orgs","repos_url":"https://api.github.com/users/richja/repos","events_url":"https://api.github.com/users/richja/events{/privacy}","received_events_url":"https://api.github.com/users/richja/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2019-06-24T16:54:47Z","updated_at":"2019-08-14T05:09:50Z","closed_at":"2019-06-24T17:35:31Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nWhen trying to get AvailableLogTypes from logs. the NullReferenceException is thrown.\r\n\r\nSame happens on Chrome and Firefox.\r\nBasically same issue as reported while ago https://github.com/SeleniumHQ/selenium/issues/5842.\r\n\r\n## To Reproduce\r\nTry to get log types `driver.Manage().Logs.AvailableLogTypes`\r\nor browser logs directly `driver.Manage().GetLog(LogType.Browser)`\r\n\r\n## Expected behavior\r\nDo not throw an exception.\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\n```\r\nChromeOptions chromeOptions = new ChromeOptions();\r\nchromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);\r\n_driver = new ChromeDriver(chromeOptions);\r\n_driver.Url = \"https://github.com\";\r\nvar logs = _driver.Manage().Logs;\r\nvar logTypes = logs.AvailableLogTypes; //NullReferenceException thrown here\r\n```\r\n\r\n## Environment\r\n\r\nOS: Windows 10\r\nBrowser: Chrome, Firefox\r\nBrowser version: Chrome 75.0.3770.100, Firefox 67.0.2\r\nBrowser Driver version: ChromeDriver 75.0.3770.90, GeckoDriver 0.24\r\nLanguage Bindings version: C# 3.141","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["505107297","511196752","511204014","511208039"], "labels":[]}