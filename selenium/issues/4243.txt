{"id":"4243", "title":"Regarding: HTML5 Drag and Drop with Selenium Webdriver #3604", "body":"**NOTE:** This is not an issue, but rather a temporary alternative for developers (like myself) whom are looking for their tests to function while this issue remains open. I would have liked to comment on the existing issue, but it has been closed for comments (:-/).

## Meta -
OS:  Ubuntu 16.04
Selenium Version:  PHPUnit_Selenium 5.0.8 - 2015-10-23
Browser:  Chrome

Browser Version:  Version 58.0.3029.110 Built on Ubuntu , running on Ubuntu 16.04 (64-bit)

## Expected Behavior -
As already known, drag and drop events using draggable and droppable attributes/functionality does not function as expected when ran in Laravel's Dusk tests (among others). Running a $browser->drag('#from','#to') from testers should automate dragging the \"from\" element to the \"to\" element firing the \"dragstart\", \"dragover\", and \"drop\" events. 

## Actual Behavior -
```
/**
     * Drag and drop from $source to $target.
     *
     * @param WebDriverElement $source
     * @param WebDriverElement $target
     * @return WebDriverActions
     */
    public function dragAndDrop(WebDriverElement $source, WebDriverElement $target)
    {
        $this->action->addAction(
            new WebDriverClickAndHoldAction($this->mouse, $source)
        );
        $this->action->addAction(
            new WebDriverMouseMoveAction($this->mouse, $target)
        );
        $this->action->addAction(
            new WebDriverButtonReleaseAction($this->mouse, $target)
        );

        return $this;
    }
```

This behavior, though in theory simulates a drag and drop, actually fires \"mousedown\" and \"mouseup\" events rather than the \"dragstart\", \"dragover\", and \"drop\" events.


## Steps to reproduce -
```
$this->browse(function (Browser $browser) {
    $browser->setUp()->visit($this->url)->drag('#element-a', '#element-b');
}
```

## **Temporary solution**
Create events for \"mousedown\" and \"mouseup\", on draggable and droppable elements (respectively), that trigger the dragstart and drop events (respectively).

 -- Example:
Included is a jsfiddle link that provides the code necessary for user driven drag-and-drop as well as the Dusk test drag and drop simulation (though of course the dusk test cannot be ran from it):

[Dusk \"testable\" Drag and Drop Fiddle](https://jsfiddle.net/grantdscs/nehjrbzo/)

A highlight of the functionality:
```
    $(\"a[draggable='true']\").on(\"mousedown\", function(event) {
        $(this).trigger(\"dragstart\");
    });

    $(\"div[droppable='true']\").on(\"mouseup\", function(event) {
        $(this).trigger(\"drop\");
    });
```", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4243","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4243/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4243/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4243/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4243","id":239842393,"node_id":"MDU6SXNzdWUyMzk4NDIzOTM=","number":4243,"title":"Regarding: HTML5 Drag and Drop with Selenium Webdriver #3604","user":{"login":"isaiahgrant","id":7319493,"node_id":"MDQ6VXNlcjczMTk0OTM=","avatar_url":"https://avatars0.githubusercontent.com/u/7319493?v=4","gravatar_id":"","url":"https://api.github.com/users/isaiahgrant","html_url":"https://github.com/isaiahgrant","followers_url":"https://api.github.com/users/isaiahgrant/followers","following_url":"https://api.github.com/users/isaiahgrant/following{/other_user}","gists_url":"https://api.github.com/users/isaiahgrant/gists{/gist_id}","starred_url":"https://api.github.com/users/isaiahgrant/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/isaiahgrant/subscriptions","organizations_url":"https://api.github.com/users/isaiahgrant/orgs","repos_url":"https://api.github.com/users/isaiahgrant/repos","events_url":"https://api.github.com/users/isaiahgrant/events{/privacy}","received_events_url":"https://api.github.com/users/isaiahgrant/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-06-30T17:30:18Z","updated_at":"2019-08-18T05:09:36Z","closed_at":"2017-07-03T06:14:11Z","author_association":"NONE","body":"**NOTE:** This is not an issue, but rather a temporary alternative for developers (like myself) whom are looking for their tests to function while this issue remains open. I would have liked to comment on the existing issue, but it has been closed for comments (:-/).\r\n\r\n## Meta -\r\nOS:  Ubuntu 16.04\r\nSelenium Version:  PHPUnit_Selenium 5.0.8 - 2015-10-23\r\nBrowser:  Chrome\r\n\r\nBrowser Version:  Version 58.0.3029.110 Built on Ubuntu , running on Ubuntu 16.04 (64-bit)\r\n\r\n## Expected Behavior -\r\nAs already known, drag and drop events using draggable and droppable attributes/functionality does not function as expected when ran in Laravel's Dusk tests (among others). Running a $browser->drag('#from','#to') from testers should automate dragging the \"from\" element to the \"to\" element firing the \"dragstart\", \"dragover\", and \"drop\" events. \r\n\r\n## Actual Behavior -\r\n```\r\n/**\r\n     * Drag and drop from $source to $target.\r\n     *\r\n     * @param WebDriverElement $source\r\n     * @param WebDriverElement $target\r\n     * @return WebDriverActions\r\n     */\r\n    public function dragAndDrop(WebDriverElement $source, WebDriverElement $target)\r\n    {\r\n        $this->action->addAction(\r\n            new WebDriverClickAndHoldAction($this->mouse, $source)\r\n        );\r\n        $this->action->addAction(\r\n            new WebDriverMouseMoveAction($this->mouse, $target)\r\n        );\r\n        $this->action->addAction(\r\n            new WebDriverButtonReleaseAction($this->mouse, $target)\r\n        );\r\n\r\n        return $this;\r\n    }\r\n```\r\n\r\nThis behavior, though in theory simulates a drag and drop, actually fires \"mousedown\" and \"mouseup\" events rather than the \"dragstart\", \"dragover\", and \"drop\" events.\r\n\r\n\r\n## Steps to reproduce -\r\n```\r\n$this->browse(function (Browser $browser) {\r\n    $browser->setUp()->visit($this->url)->drag('#element-a', '#element-b');\r\n}\r\n```\r\n\r\n## **Temporary solution**\r\nCreate events for \"mousedown\" and \"mouseup\", on draggable and droppable elements (respectively), that trigger the dragstart and drop events (respectively).\r\n\r\n -- Example:\r\nIncluded is a jsfiddle link that provides the code necessary for user driven drag-and-drop as well as the Dusk test drag and drop simulation (though of course the dusk test cannot be ran from it):\r\n\r\n[Dusk \"testable\" Drag and Drop Fiddle](https://jsfiddle.net/grantdscs/nehjrbzo/)\r\n\r\nA highlight of the functionality:\r\n```\r\n    $(\"a[draggable='true']\").on(\"mousedown\", function(event) {\r\n        $(this).trigger(\"dragstart\");\r\n    });\r\n\r\n    $(\"div[droppable='true']\").on(\"mouseup\", function(event) {\r\n        $(this).trigger(\"drop\");\r\n    });\r\n```","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["312557888","312678111"], "labels":[]}