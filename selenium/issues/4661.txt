{"id":"4661", "title":"Getting UnhandledAlertException even after handling", "body":"## Meta -
OS:  
Windows 10
Selenium Version:  
3.4.0
Browser:  
Firefox 45.5.1

## Expected Behavior -

## Actual Behavior -

## Steps to reproduce -
Project i am working on needed window authentication, handled and it was working fine until last week and suddenly it started throwing the following error, any help would be really appreciated 
Cheers,
Following is the error and code

**# Error**
Test Name:	verifyLocationStatus(\"1 Amarina Avenue, Greenacre\",\"OFF-NET\")
Test FullName:	PricingTool.AutomationTests.Tests.PTStatusTests.verifyLocationStatus(\"1 Amarina Avenue, Greenacre\",\"OFF-NET\")
Test Source:	C:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs : line 16
Test Outcome:	Failed
Test Duration:	0:00:07.112

Result StackTrace:	
at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)
   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(String mechanism, String value)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElementById(String id)
   at OpenQA.Selenium.By.<>c__DisplayClass2.<Id>b__0(ISearchContext context)
   at OpenQA.Selenium.By.FindElement(ISearchContext context)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(By by)
   at OpenQA.Selenium.Support.PageObjects.DefaultElementLocator.LocateElement(IEnumerable`1 bys)
   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.get_Element()
   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.Invoke(IMessage msg)
   at System.Runtime.Remoting.Proxies.RealProxy.PrivateInvoke(MessageData& msgData, Int32 type)
   at OpenQA.Selenium.IWebElement.Click()
   at PricingTool.AutomationTests.Steps.PTSteps.click_On_BuildNewPriceList() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 51
   at PricingTool.AutomationTests.Tests.PTStatusTests.verifyLocationStatus(String pointA_Address, String status) in C:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs:line 18
--TearDown
   at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)
   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(String mechanism, String value)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElementByXPath(String xpath)
   at OpenQA.Selenium.By.<>c__DisplayClasse.<XPath>b__c(ISearchContext context)
   at OpenQA.Selenium.By.FindElement(ISearchContext context)
   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(By by)
   at OpenQA.Selenium.Support.PageObjects.DefaultElementLocator.LocateElement(IEnumerable`1 bys)
   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.get_Element()
   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.Invoke(IMessage msg)
   at System.Runtime.Remoting.Proxies.RealProxy.PrivateInvoke(MessageData& msgData, Int32 type)
   at OpenQA.Selenium.IWebElement.Click()
   at PricingTool.AutomationTests.Steps.PTSteps.navigate_To_Dashboard() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 580
   at PricingTool.AutomationTests.Steps.PTSteps.delete_Created_Pricelist_And_Close_Window() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 585
   at PricingTool.AutomationTests.Tests.PTStatusTests.testTearDown() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs:line 31
Result Message:	
OpenQA.Selenium.UnhandledAlertException : 
TearDown : OpenQA.Selenium.UnhandledAlertException :


# **Code**
using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using PricingTool.AutomationTests.PageObjects;
using OpenQA.Selenium.Remote;

namespace PricingTool.AutomationTests.Steps
{
    public class PTSteps
    {
        IWebDriver driver;

            
        HomePage homePage = new HomePage();
        ListBuilderPage listBuilderPage = new ListBuilderPage();
        List<string> serviceSpeedList = new List<string>();


        internal void login_To_NPTDirect()
        {
            driver = new FirefoxDriver();
            driver.Manage().Window.Maximize();
            driver.Url = \"https://ptstaging.interactive.com.au\";
             driver.SwitchTo().Alert();
               driver.SwitchTo().Alert().SendKeys(\"softwaretest\" + Keys.Tab + \"SoftTest17\");
            driver.SwitchTo().Alert().SendKeys(Keys.Enter);


                 }

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4661","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4661/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4661/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4661/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4661","id":256141279,"node_id":"MDU6SXNzdWUyNTYxNDEyNzk=","number":4661,"title":"Getting UnhandledAlertException even after handling","user":{"login":"DeepthiBalla","id":31024173,"node_id":"MDQ6VXNlcjMxMDI0MTcz","avatar_url":"https://avatars0.githubusercontent.com/u/31024173?v=4","gravatar_id":"","url":"https://api.github.com/users/DeepthiBalla","html_url":"https://github.com/DeepthiBalla","followers_url":"https://api.github.com/users/DeepthiBalla/followers","following_url":"https://api.github.com/users/DeepthiBalla/following{/other_user}","gists_url":"https://api.github.com/users/DeepthiBalla/gists{/gist_id}","starred_url":"https://api.github.com/users/DeepthiBalla/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/DeepthiBalla/subscriptions","organizations_url":"https://api.github.com/users/DeepthiBalla/orgs","repos_url":"https://api.github.com/users/DeepthiBalla/repos","events_url":"https://api.github.com/users/DeepthiBalla/events{/privacy}","received_events_url":"https://api.github.com/users/DeepthiBalla/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-09-08T05:00:48Z","updated_at":"2019-08-17T13:09:46Z","closed_at":"2017-09-08T05:34:39Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows 10\r\nSelenium Version:  \r\n3.4.0\r\nBrowser:  \r\nFirefox 45.5.1\r\n\r\n## Expected Behavior -\r\n\r\n## Actual Behavior -\r\n\r\n## Steps to reproduce -\r\nProject i am working on needed window authentication, handled and it was working fine until last week and suddenly it started throwing the following error, any help would be really appreciated \r\nCheers,\r\nFollowing is the error and code\r\n\r\n**# Error**\r\nTest Name:\tverifyLocationStatus(\"1 Amarina Avenue, Greenacre\",\"OFF-NET\")\r\nTest FullName:\tPricingTool.AutomationTests.Tests.PTStatusTests.verifyLocationStatus(\"1 Amarina Avenue, Greenacre\",\"OFF-NET\")\r\nTest Source:\tC:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs : line 16\r\nTest Outcome:\tFailed\r\nTest Duration:\t0:00:07.112\r\n\r\nResult StackTrace:\t\r\nat OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(String mechanism, String value)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElementById(String id)\r\n   at OpenQA.Selenium.By.<>c__DisplayClass2.<Id>b__0(ISearchContext context)\r\n   at OpenQA.Selenium.By.FindElement(ISearchContext context)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(By by)\r\n   at OpenQA.Selenium.Support.PageObjects.DefaultElementLocator.LocateElement(IEnumerable`1 bys)\r\n   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.get_Element()\r\n   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.Invoke(IMessage msg)\r\n   at System.Runtime.Remoting.Proxies.RealProxy.PrivateInvoke(MessageData& msgData, Int32 type)\r\n   at OpenQA.Selenium.IWebElement.Click()\r\n   at PricingTool.AutomationTests.Steps.PTSteps.click_On_BuildNewPriceList() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 51\r\n   at PricingTool.AutomationTests.Tests.PTStatusTests.verifyLocationStatus(String pointA_Address, String status) in C:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs:line 18\r\n--TearDown\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.UnpackAndThrowOnError(Response errorResponse)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.Execute(String driverCommandToExecute, Dictionary`2 parameters)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(String mechanism, String value)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElementByXPath(String xpath)\r\n   at OpenQA.Selenium.By.<>c__DisplayClasse.<XPath>b__c(ISearchContext context)\r\n   at OpenQA.Selenium.By.FindElement(ISearchContext context)\r\n   at OpenQA.Selenium.Remote.RemoteWebDriver.FindElement(By by)\r\n   at OpenQA.Selenium.Support.PageObjects.DefaultElementLocator.LocateElement(IEnumerable`1 bys)\r\n   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.get_Element()\r\n   at OpenQA.Selenium.Support.PageObjects.WebElementProxy.Invoke(IMessage msg)\r\n   at System.Runtime.Remoting.Proxies.RealProxy.PrivateInvoke(MessageData& msgData, Int32 type)\r\n   at OpenQA.Selenium.IWebElement.Click()\r\n   at PricingTool.AutomationTests.Steps.PTSteps.navigate_To_Dashboard() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 580\r\n   at PricingTool.AutomationTests.Steps.PTSteps.delete_Created_Pricelist_And_Close_Window() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Steps\\PTSteps.cs:line 585\r\n   at PricingTool.AutomationTests.Tests.PTStatusTests.testTearDown() in C:\\Projects\\QA\\PricingTool.AutomationTests\\Tests\\PTStatusTests.cs:line 31\r\nResult Message:\t\r\nOpenQA.Selenium.UnhandledAlertException : \r\nTearDown : OpenQA.Selenium.UnhandledAlertException :\r\n\r\n\r\n# **Code**\r\nusing System;\r\nusing System.Collections.Generic;\r\nusing System.Threading;\r\nusing NUnit.Framework;\r\nusing OpenQA.Selenium;\r\nusing OpenQA.Selenium.Firefox;\r\nusing OpenQA.Selenium.Support.PageObjects;\r\nusing OpenQA.Selenium.Support.UI;\r\nusing PricingTool.AutomationTests.PageObjects;\r\nusing OpenQA.Selenium.Remote;\r\n\r\nnamespace PricingTool.AutomationTests.Steps\r\n{\r\n    public class PTSteps\r\n    {\r\n        IWebDriver driver;\r\n\r\n            \r\n        HomePage homePage = new HomePage();\r\n        ListBuilderPage listBuilderPage = new ListBuilderPage();\r\n        List<string> serviceSpeedList = new List<string>();\r\n\r\n\r\n        internal void login_To_NPTDirect()\r\n        {\r\n            driver = new FirefoxDriver();\r\n            driver.Manage().Window.Maximize();\r\n            driver.Url = \"https://ptstaging.interactive.com.au\";\r\n             driver.SwitchTo().Alert();\r\n               driver.SwitchTo().Alert().SendKeys(\"softwaretest\" + Keys.Tab + \"SoftTest17\");\r\n            driver.SwitchTo().Alert().SendKeys(Keys.Enter);\r\n\r\n\r\n                 }\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["328001741","328002912","328003481"], "labels":[]}