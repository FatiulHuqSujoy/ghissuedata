{"id":"4549", "title":"touch.flick implemented not properly in ruby bindings", "body":"## Meta -
OS:  any
Selenium Version:  3.4.4
Browser:  any

## Steps to reproduce -

```
require 'selenium-webdriver'

username = \"please put your upwork login here\"
password = \"please put your upwork password here!\"

mobile_emulation = { \"deviceName\" => \"Nexus 5\" }
caps = Selenium::WebDriver::Remote::Capabilities.chrome(
    \"chromeOptions\" => { \"mobileEmulation\" => mobile_emulation })
driver = Selenium::WebDriver.for :chrome, desired_capabilities: caps
driver.extend Selenium::WebDriver::DriverExtensions::HasTouchScreen

driver.get \"https://www.upwork.com/\"

# go to login
# open a menu
e = driver.find_element css: '.right-off-canvas-toggle.menu-icon.pull-right'
driver.touch.single_tap(e).perform
# navigate to login
sleep 1
e = driver.find_element xpath: '/html/body/div[1]/div/header[1]/div[2]/aside/nav/ul/li[7]/a'
driver.touch.single_tap(e).perform
# fill user_name and password
e = driver.find_element id: 'login_username'
e.send_keys username

e = driver.find_element id: 'login_password'
e.send_keys password

e = driver.find_element xpath: '//*[@id=\"layout\"]/div[2]/div/form/div[8]/button'
driver.touch.single_tap(e).perform


sleep 10
e = driver.find_element xpath: '//body/ion-nav-view'
# swipe left to unlock a menu
driver.touch.flick(e, Integer(e.rect.width * (-0.75)), 0, 'normal').perform
```
this code will fails with next exception:
```
in `<top (required)>': unknown error: 'speed' must be a positive integer (Selenium::WebDriver::Error::UnknownError)
  (Session info: chrome=60.0.3112.101)
  (Driver info: chromedriver=2.30.477690 (c53f4ad87510ee97b5c3425a14c0e79780cdf262),platform=Mac OS X 10.12.6 x86_64)
```
if you modify a  last line like 
`driver.touch.flick(e, Integer(e.rect.width * (-0.75)), 0, 'fast').perform`
it will work but extremely slow since in this case speed will be 1 pixel per second
since
```
module Selenium
  module WebDriver
    class TouchScreen
      FLICK_SPEED = {normal: 0, fast: 1}.freeze
```

## Solution -
replace flick speed values
```
module Selenium
  module WebDriver
    class TouchScreen
      FLICK_SPEED = {normal: 50, fast: 100}.freeze
```
and another thing is to allow user to pass integer values 
because current implementation does not allow to do so
see original code
```
when 4
          element, xoffset, yoffset, speed = args

          assert_element element
          flick_speed = FLICK_SPEED[speed.to_sym]

          unless flick_speed
            raise ArgumentError, \"expected one of #{FLICK_SPEED.keys.inspect}, got #{speed.inspect}\"
          end

          @bridge.touch_element_flick element.ref, Integer(xoffset), Integer(yoffset), flick_speed
```

so it could be modified like
```
when 4
          element, xoffset, yoffset, speed = args

          assert_element element
          flick_speed =  speed.is_a?(Numeric) && speed > 0 ?
                                      Integer(speed) :
                                      FLICK_SPEED[speed.to_sym]

          unless flick_speed
            raise ArgumentError, \"expected one of #{FLICK_SPEED.keys.inspect}, got #{speed.inspect}\"
          end

          @bridge.touch_element_flick element.ref, Integer(xoffset), Integer(yoffset), flick_speed
```

P.S. sorry for a miss format but I think I'm already found the root of the issue and I'm able to overcome this difficulty with a monkey patch but its not best solution...", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4549","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4549/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4549/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4549/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4549","id":252238085,"node_id":"MDU6SXNzdWUyNTIyMzgwODU=","number":4549,"title":"touch.flick implemented not properly in ruby bindings","user":{"login":"oleksiybondar","id":18579300,"node_id":"MDQ6VXNlcjE4NTc5MzAw","avatar_url":"https://avatars1.githubusercontent.com/u/18579300?v=4","gravatar_id":"","url":"https://api.github.com/users/oleksiybondar","html_url":"https://github.com/oleksiybondar","followers_url":"https://api.github.com/users/oleksiybondar/followers","following_url":"https://api.github.com/users/oleksiybondar/following{/other_user}","gists_url":"https://api.github.com/users/oleksiybondar/gists{/gist_id}","starred_url":"https://api.github.com/users/oleksiybondar/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/oleksiybondar/subscriptions","organizations_url":"https://api.github.com/users/oleksiybondar/orgs","repos_url":"https://api.github.com/users/oleksiybondar/repos","events_url":"https://api.github.com/users/oleksiybondar/events{/privacy}","received_events_url":"https://api.github.com/users/oleksiybondar/received_events","type":"User","site_admin":false},"labels":[{"id":182503883,"node_id":"MDU6TGFiZWwxODI1MDM4ODM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-rb","name":"C-rb","color":"fbca04","default":false},{"id":316341013,"node_id":"MDU6TGFiZWwzMTYzNDEwMTM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-chrome","name":"D-chrome","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-08-23T10:43:31Z","updated_at":"2019-08-17T20:09:43Z","closed_at":"2017-08-24T07:29:41Z","author_association":"NONE","body":"## Meta -\r\nOS:  any\r\nSelenium Version:  3.4.4\r\nBrowser:  any\r\n\r\n## Steps to reproduce -\r\n\r\n```\r\nrequire 'selenium-webdriver'\r\n\r\nusername = \"please put your upwork login here\"\r\npassword = \"please put your upwork password here!\"\r\n\r\nmobile_emulation = { \"deviceName\" => \"Nexus 5\" }\r\ncaps = Selenium::WebDriver::Remote::Capabilities.chrome(\r\n    \"chromeOptions\" => { \"mobileEmulation\" => mobile_emulation })\r\ndriver = Selenium::WebDriver.for :chrome, desired_capabilities: caps\r\ndriver.extend Selenium::WebDriver::DriverExtensions::HasTouchScreen\r\n\r\ndriver.get \"https://www.upwork.com/\"\r\n\r\n# go to login\r\n# open a menu\r\ne = driver.find_element css: '.right-off-canvas-toggle.menu-icon.pull-right'\r\ndriver.touch.single_tap(e).perform\r\n# navigate to login\r\nsleep 1\r\ne = driver.find_element xpath: '/html/body/div[1]/div/header[1]/div[2]/aside/nav/ul/li[7]/a'\r\ndriver.touch.single_tap(e).perform\r\n# fill user_name and password\r\ne = driver.find_element id: 'login_username'\r\ne.send_keys username\r\n\r\ne = driver.find_element id: 'login_password'\r\ne.send_keys password\r\n\r\ne = driver.find_element xpath: '//*[@id=\"layout\"]/div[2]/div/form/div[8]/button'\r\ndriver.touch.single_tap(e).perform\r\n\r\n\r\nsleep 10\r\ne = driver.find_element xpath: '//body/ion-nav-view'\r\n# swipe left to unlock a menu\r\ndriver.touch.flick(e, Integer(e.rect.width * (-0.75)), 0, 'normal').perform\r\n```\r\nthis code will fails with next exception:\r\n```\r\nin `<top (required)>': unknown error: 'speed' must be a positive integer (Selenium::WebDriver::Error::UnknownError)\r\n  (Session info: chrome=60.0.3112.101)\r\n  (Driver info: chromedriver=2.30.477690 (c53f4ad87510ee97b5c3425a14c0e79780cdf262),platform=Mac OS X 10.12.6 x86_64)\r\n```\r\nif you modify a  last line like \r\n`driver.touch.flick(e, Integer(e.rect.width * (-0.75)), 0, 'fast').perform`\r\nit will work but extremely slow since in this case speed will be 1 pixel per second\r\nsince\r\n```\r\nmodule Selenium\r\n  module WebDriver\r\n    class TouchScreen\r\n      FLICK_SPEED = {normal: 0, fast: 1}.freeze\r\n```\r\n\r\n## Solution -\r\nreplace flick speed values\r\n```\r\nmodule Selenium\r\n  module WebDriver\r\n    class TouchScreen\r\n      FLICK_SPEED = {normal: 50, fast: 100}.freeze\r\n```\r\nand another thing is to allow user to pass integer values \r\nbecause current implementation does not allow to do so\r\nsee original code\r\n```\r\nwhen 4\r\n          element, xoffset, yoffset, speed = args\r\n\r\n          assert_element element\r\n          flick_speed = FLICK_SPEED[speed.to_sym]\r\n\r\n          unless flick_speed\r\n            raise ArgumentError, \"expected one of #{FLICK_SPEED.keys.inspect}, got #{speed.inspect}\"\r\n          end\r\n\r\n          @bridge.touch_element_flick element.ref, Integer(xoffset), Integer(yoffset), flick_speed\r\n```\r\n\r\nso it could be modified like\r\n```\r\nwhen 4\r\n          element, xoffset, yoffset, speed = args\r\n\r\n          assert_element element\r\n          flick_speed =  speed.is_a?(Numeric) && speed > 0 ?\r\n                                      Integer(speed) :\r\n                                      FLICK_SPEED[speed.to_sym]\r\n\r\n          unless flick_speed\r\n            raise ArgumentError, \"expected one of #{FLICK_SPEED.keys.inspect}, got #{speed.inspect}\"\r\n          end\r\n\r\n          @bridge.touch_element_flick element.ref, Integer(xoffset), Integer(yoffset), flick_speed\r\n```\r\n\r\nP.S. sorry for a miss format but I think I'm already found the root of the issue and I'm able to overcome this difficulty with a monkey patch but its not best solution...","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["324298010","324556555"], "labels":["C-rb","D-chrome"]}