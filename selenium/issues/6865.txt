{"id":"6865", "title":"I can't change browser.link.open_newwindow pref if I need use profile", "body":"## 💥 Regression Report

Hi.

**Meta -**

OS: MacOS X, Linux, Windows

Selenium Version: 3.141.59

Browser: Firefox (geckodriver)

Browser Version: 64,63,62,61,60 (64 bits)

**Expected Behavior**

**_browser.link.open_newwindow_** preference always set to 3

**Actual Behavior**
Exception in thread \"main\" java.lang.IllegalArgumentException: Preference browser.link.open_newwindow may not be overridden: frozen value=2, requested value=3


**Scenario:**
`FirefoxProfile profile = new FirefoxProfile();
FirefoxOptions options = new FirefoxOptions().setProfile(profile);
options.addPreference(\"browser.link.open_newwindow\", 3)
 //or options.addPreference(\"browser.link.open_newwindow\", 1)
desiredCapabilities.merge(options);
WebDriver driver1 = new RemoteWebDriver(new URL(\"http://localhost:4444/wd/hub\"), desiredCapabilities);`


but if I send custom profile with \"browser.link.open_newwindow\" = 3 in pref.js, so I don't get exception. I get browser with \"browser.link.open_newwindow\" = 2
Why???
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6865","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6865/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6865/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6865/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6865","id":400789789,"node_id":"MDU6SXNzdWU0MDA3ODk3ODk=","number":6865,"title":"I can't change browser.link.open_newwindow pref if I need use profile","user":{"login":"azapevalov","id":44862915,"node_id":"MDQ6VXNlcjQ0ODYyOTE1","avatar_url":"https://avatars0.githubusercontent.com/u/44862915?v=4","gravatar_id":"","url":"https://api.github.com/users/azapevalov","html_url":"https://github.com/azapevalov","followers_url":"https://api.github.com/users/azapevalov/followers","following_url":"https://api.github.com/users/azapevalov/following{/other_user}","gists_url":"https://api.github.com/users/azapevalov/gists{/gist_id}","starred_url":"https://api.github.com/users/azapevalov/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/azapevalov/subscriptions","organizations_url":"https://api.github.com/users/azapevalov/orgs","repos_url":"https://api.github.com/users/azapevalov/repos","events_url":"https://api.github.com/users/azapevalov/events{/privacy}","received_events_url":"https://api.github.com/users/azapevalov/received_events","type":"User","site_admin":false},"labels":[{"id":182509154,"node_id":"MDU6TGFiZWwxODI1MDkxNTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-java","name":"C-java","color":"fbca04","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-01-18T16:10:13Z","updated_at":"2019-08-14T16:09:35Z","closed_at":"2019-04-12T11:43:52Z","author_association":"NONE","body":"## 💥 Regression Report\r\n\r\nHi.\r\n\r\n**Meta -**\r\n\r\nOS: MacOS X, Linux, Windows\r\n\r\nSelenium Version: 3.141.59\r\n\r\nBrowser: Firefox (geckodriver)\r\n\r\nBrowser Version: 64,63,62,61,60 (64 bits)\r\n\r\n**Expected Behavior**\r\n\r\n**_browser.link.open_newwindow_** preference always set to 3\r\n\r\n**Actual Behavior**\r\nException in thread \"main\" java.lang.IllegalArgumentException: Preference browser.link.open_newwindow may not be overridden: frozen value=2, requested value=3\r\n\r\n\r\n**Scenario:**\r\n`FirefoxProfile profile = new FirefoxProfile();\r\nFirefoxOptions options = new FirefoxOptions().setProfile(profile);\r\noptions.addPreference(\"browser.link.open_newwindow\", 3)\r\n //or options.addPreference(\"browser.link.open_newwindow\", 1)\r\ndesiredCapabilities.merge(options);\r\nWebDriver driver1 = new RemoteWebDriver(new URL(\"http://localhost:4444/wd/hub\"), desiredCapabilities);`\r\n\r\n\r\nbut if I send custom profile with \"browser.link.open_newwindow\" = 3 in pref.js, so I don't get exception. I get browser with \"browser.link.open_newwindow\" = 2\r\nWhy???\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["456698824","456764784","482543728"], "labels":["C-java","R-awaiting answer"]}