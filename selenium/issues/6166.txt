{"id":"6166", "title":" ERR_NAME_NOT_RESOLVED on heroku with mobile testing", "body":"## Meta -
OS:  Windows 10 working vs cedar 14 stack heroku not working
Selenium Version:  selenium==3.13.0
Browser:  Chrome https://github.com/heroku/heroku-buildpack-chromedriver
chromedriver v2.40

I'd like to test multiple mobile user agents with selenium and chrome. I'm using python 3.6 and deploying to heroku. Based on http://chromedriver.chromium.org/mobile-emulation , I have:

    def some_long_calculation():
        driver = create_chromedriver('kkk')
        # driver = create_chromedriver()
    
        driver.get(\"https://www.yahoo.com/\")
        .....

and :

	def create_chromedriver(ua=False):
	    options = webdriver.ChromeOptions()
	    CHROMEDRIVER_PATH = os.getenv('$HOME') or basedir+'/chromedriver.exe'
	    FLASK_CONFIG = os.getenv('FLASK_CONFIG')

	    if ua:

	        mobile_emulation = {\"deviceName\": \"Nexus 5\"}
	        options.add_experimental_option(\"mobileEmulation\", mobile_emulation)


	    if FLASK_CONFIG and FLASK_CONFIG == \"production\":
	        CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'
	        GOOGLE_CHROME_SHIM = os.getenv('$GOOGLE_CHROME_SHIM') or 'no path found'
	        options.binary_location = '/app/.apt/usr/bin/google-chrome-stable'

	        options.add_argument('--disable-gpu')
	        options.add_argument('--no-sandbox')

	    return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)  

If I run it locally with the mobile browser enabled It works as expected:


[![enter image description here][2]][2]


If I run it on heroku with the mobile browser enabled :

[![enter image description here][3]][3]

Then I tried it on heroku with the mobile user disabled I get:

[![enter image description here][1]][1]

So at least I know the setup is working as far as chrome and chromedriver. 

heroku Logs:

	2018-07-15T17:37:53.967643+00:00 app[web.1]:     driver = create_chromedriver('kkk')
	2018-07-15T17:37:53.967637+00:00 app[web.1]:     png = some_long_calculation()
	2018-07-15T17:37:53.967645+00:00 app[web.1]:   File \"/app/app/main/cl.py\", line 120, in create_chromedriver
	2018-07-15T17:37:53.967640+00:00 app[web.1]:   File \"/app/app/main/cl.py\", line 123, in some_long_calculation
	2018-07-15T17:37:53.967648+00:00 app[web.1]:     return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)
	2018-07-15T17:37:53.967651+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__
	2018-07-15T17:37:53.967654+00:00 app[web.1]:     desired_capabilities=desired_capabilities)
	2018-07-15T17:37:53.967656+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 156, in __init__
	2018-07-15T17:37:53.967659+00:00 app[web.1]:     self.start_session(capabilities, browser_profile)
	2018-07-15T17:37:53.967661+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 251, in start_session
	2018-07-15T17:37:53.967669+00:00 app[web.1]:     response = self.command_executor.execute(driver_command, params)
	2018-07-15T17:37:53.967664+00:00 app[web.1]:     response = self.execute(Command.NEW_SESSION, parameters)
	2018-07-15T17:37:53.967667+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 318, in execute
	2018-07-15T17:37:53.967672+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 472, in execute
	2018-07-15T17:37:53.967674+00:00 app[web.1]:     return self._request(command_info[0], url, body=data)
	2018-07-15T17:37:53.967677+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 496, in _request
	2018-07-15T17:37:53.967679+00:00 app[web.1]:     resp = self._conn.getresponse()
	2018-07-15T17:37:53.967682+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 1331, in getresponse
	2018-07-15T17:37:53.967685+00:00 app[web.1]:     response.begin()
	2018-07-15T17:37:53.967687+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 297, in begin
	2018-07-15T17:37:53.967695+00:00 app[web.1]:     line = str(self.fp.readline(_MAXLINE + 1), \"iso-8859-1\")
	2018-07-15T17:37:53.967690+00:00 app[web.1]:     version, status, reason = self._read_status()
	2018-07-15T17:37:53.967698+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/socket.py\", line 586, in readinto
	2018-07-15T17:37:53.967692+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 258, in _read_status
	2018-07-15T17:37:53.967700+00:00 app[web.1]:     return self._sock.recv_into(b)
	2018-07-15T17:37:53.967712+00:00 app[web.1]: ConnectionResetError: [Errno 104] Connection reset by peer


How can I fix this?


  [1]: https://i.stack.imgur.com/32ObW.png
  [2]: https://i.stack.imgur.com/fSGZY.png
  [3]: https://i.stack.imgur.com/eVP8Z.png", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6166","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6166/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6166/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6166/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6166","id":341542328,"node_id":"MDU6SXNzdWUzNDE1NDIzMjg=","number":6166,"title":" ERR_NAME_NOT_RESOLVED on heroku with mobile testing","user":{"login":"kc1","id":1745710,"node_id":"MDQ6VXNlcjE3NDU3MTA=","avatar_url":"https://avatars1.githubusercontent.com/u/1745710?v=4","gravatar_id":"","url":"https://api.github.com/users/kc1","html_url":"https://github.com/kc1","followers_url":"https://api.github.com/users/kc1/followers","following_url":"https://api.github.com/users/kc1/following{/other_user}","gists_url":"https://api.github.com/users/kc1/gists{/gist_id}","starred_url":"https://api.github.com/users/kc1/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kc1/subscriptions","organizations_url":"https://api.github.com/users/kc1/orgs","repos_url":"https://api.github.com/users/kc1/repos","events_url":"https://api.github.com/users/kc1/events{/privacy}","received_events_url":"https://api.github.com/users/kc1/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-07-16T14:28:09Z","updated_at":"2019-08-15T17:09:59Z","closed_at":"2018-07-16T14:59:11Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 10 working vs cedar 14 stack heroku not working\r\nSelenium Version:  selenium==3.13.0\r\nBrowser:  Chrome https://github.com/heroku/heroku-buildpack-chromedriver\r\nchromedriver v2.40\r\n\r\nI'd like to test multiple mobile user agents with selenium and chrome. I'm using python 3.6 and deploying to heroku. Based on http://chromedriver.chromium.org/mobile-emulation , I have:\r\n\r\n    def some_long_calculation():\r\n        driver = create_chromedriver('kkk')\r\n        # driver = create_chromedriver()\r\n    \r\n        driver.get(\"https://www.yahoo.com/\")\r\n        .....\r\n\r\nand :\r\n\r\n\tdef create_chromedriver(ua=False):\r\n\t    options = webdriver.ChromeOptions()\r\n\t    CHROMEDRIVER_PATH = os.getenv('$HOME') or basedir+'/chromedriver.exe'\r\n\t    FLASK_CONFIG = os.getenv('FLASK_CONFIG')\r\n\r\n\t    if ua:\r\n\r\n\t        mobile_emulation = {\"deviceName\": \"Nexus 5\"}\r\n\t        options.add_experimental_option(\"mobileEmulation\", mobile_emulation)\r\n\r\n\r\n\t    if FLASK_CONFIG and FLASK_CONFIG == \"production\":\r\n\t        CHROMEDRIVER_PATH = '/app/.chromedriver/bin/chromedriver'\r\n\t        GOOGLE_CHROME_SHIM = os.getenv('$GOOGLE_CHROME_SHIM') or 'no path found'\r\n\t        options.binary_location = '/app/.apt/usr/bin/google-chrome-stable'\r\n\r\n\t        options.add_argument('--disable-gpu')\r\n\t        options.add_argument('--no-sandbox')\r\n\r\n\t    return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)  \r\n\r\nIf I run it locally with the mobile browser enabled It works as expected:\r\n\r\n\r\n[![enter image description here][2]][2]\r\n\r\n\r\nIf I run it on heroku with the mobile browser enabled :\r\n\r\n[![enter image description here][3]][3]\r\n\r\nThen I tried it on heroku with the mobile user disabled I get:\r\n\r\n[![enter image description here][1]][1]\r\n\r\nSo at least I know the setup is working as far as chrome and chromedriver. \r\n\r\nheroku Logs:\r\n\r\n\t2018-07-15T17:37:53.967643+00:00 app[web.1]:     driver = create_chromedriver('kkk')\r\n\t2018-07-15T17:37:53.967637+00:00 app[web.1]:     png = some_long_calculation()\r\n\t2018-07-15T17:37:53.967645+00:00 app[web.1]:   File \"/app/app/main/cl.py\", line 120, in create_chromedriver\r\n\t2018-07-15T17:37:53.967640+00:00 app[web.1]:   File \"/app/app/main/cl.py\", line 123, in some_long_calculation\r\n\t2018-07-15T17:37:53.967648+00:00 app[web.1]:     return webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)\r\n\t2018-07-15T17:37:53.967651+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/chrome/webdriver.py\", line 75, in __init__\r\n\t2018-07-15T17:37:53.967654+00:00 app[web.1]:     desired_capabilities=desired_capabilities)\r\n\t2018-07-15T17:37:53.967656+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 156, in __init__\r\n\t2018-07-15T17:37:53.967659+00:00 app[web.1]:     self.start_session(capabilities, browser_profile)\r\n\t2018-07-15T17:37:53.967661+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 251, in start_session\r\n\t2018-07-15T17:37:53.967669+00:00 app[web.1]:     response = self.command_executor.execute(driver_command, params)\r\n\t2018-07-15T17:37:53.967664+00:00 app[web.1]:     response = self.execute(Command.NEW_SESSION, parameters)\r\n\t2018-07-15T17:37:53.967667+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/webdriver.py\", line 318, in execute\r\n\t2018-07-15T17:37:53.967672+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 472, in execute\r\n\t2018-07-15T17:37:53.967674+00:00 app[web.1]:     return self._request(command_info[0], url, body=data)\r\n\t2018-07-15T17:37:53.967677+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/site-packages/selenium/webdriver/remote/remote_connection.py\", line 496, in _request\r\n\t2018-07-15T17:37:53.967679+00:00 app[web.1]:     resp = self._conn.getresponse()\r\n\t2018-07-15T17:37:53.967682+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 1331, in getresponse\r\n\t2018-07-15T17:37:53.967685+00:00 app[web.1]:     response.begin()\r\n\t2018-07-15T17:37:53.967687+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 297, in begin\r\n\t2018-07-15T17:37:53.967695+00:00 app[web.1]:     line = str(self.fp.readline(_MAXLINE + 1), \"iso-8859-1\")\r\n\t2018-07-15T17:37:53.967690+00:00 app[web.1]:     version, status, reason = self._read_status()\r\n\t2018-07-15T17:37:53.967698+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/socket.py\", line 586, in readinto\r\n\t2018-07-15T17:37:53.967692+00:00 app[web.1]:   File \"/app/.heroku/python/lib/python3.6/http/client.py\", line 258, in _read_status\r\n\t2018-07-15T17:37:53.967700+00:00 app[web.1]:     return self._sock.recv_into(b)\r\n\t2018-07-15T17:37:53.967712+00:00 app[web.1]: ConnectionResetError: [Errno 104] Connection reset by peer\r\n\r\n\r\nHow can I fix this?\r\n\r\n\r\n  [1]: https://i.stack.imgur.com/32ObW.png\r\n  [2]: https://i.stack.imgur.com/fSGZY.png\r\n  [3]: https://i.stack.imgur.com/eVP8Z.png","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["405276665","405278864","407110742","407308035","407431460"], "labels":[]}