{"id":"1412", "title":"get to same url, with authentication, with hash anchor, hangs on Firefox", "body":"I'm running tests on Firefox 43 with Selenium 2.48.2 in node.js environment (testing https://github.com/elastic/kibana).
I'm passing user:password@ url in the get command.  My test environment is actually not authenticating yet but will in the future so I went ahead and started sending the test credentials we're going to use.
If the url in the get command is the same as what the browser already has loaded it can hang.  I'll attach a file I created to reproduce the issue.  In the test I get the same url over and over.  The first 11 times without credentials in the url (it all works).  The next 11 attempts with credentials hangs on the 3rd get.

With ChromeDriver it doesn't hang.
Commented-out in the attached test file is a wrapper for get that inserts a unique query parameter (timestamp).  It slows down the get but doesn't hang.  (sorry wikipedia for using you as a test page).

OK, I can't attach a .js file.  I'll paste here;

```
define(function (require) {
  var _ = require('intern/dojo/node!lodash');
  var parse = require('intern/dojo/node!url').parse;
  var format = require('intern/dojo/node!url').format;
  var bdd = require('intern!bdd');

  bdd.describe('discover app', function describeIndexTests() {
    var remote;

    bdd.describe('query', function () {

      bdd.it('should show correct time range string', function () {

        var remote = this.remote;

        // if (this.remote.get === remote.constructor.prototype.get) {
        //   this.remote.get = _.wrap(this.remote.get, function (func, url) {
        //     console.log('url = ' + url);
        //     var parsed = parse(url, true);
        //     if (parsed.query === null) {
        //       parsed.query = {};
        //     }
        //     parsed.query._t = Date.now();
        //     var formatted = format(_.pick(parsed, 'protocol', 'hostname', 'port', 'pathname', 'query', 'hash', 'auth'));
        //     console.log('formatted url = ' + formatted);
        //     return func.call(this, formatted);
        //   });
        // }

        console.log('get 1');
        return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem')
        .then(function () {
          console.log('get 2');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 3');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 4');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 5');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 6');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 7');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 8');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 9');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 10');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 11');
          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('----------------------done without auth');
        })
        .then(function () {
          console.log('get 1');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 2');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 3');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 4');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 5');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 6');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 7');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 8');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 9');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 10');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('get 11');
          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');
        })
        .then(function () {
          console.log('----------------------done with auth');
        });
      });


    });
  });
});

```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1412","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1412/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1412/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1412/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1412","id":123542084,"node_id":"MDU6SXNzdWUxMjM1NDIwODQ=","number":1412,"title":"get to same url, with authentication, with hash anchor, hangs on Firefox","user":{"login":"LeeDr","id":13542669,"node_id":"MDQ6VXNlcjEzNTQyNjY5","avatar_url":"https://avatars1.githubusercontent.com/u/13542669?v=4","gravatar_id":"","url":"https://api.github.com/users/LeeDr","html_url":"https://github.com/LeeDr","followers_url":"https://api.github.com/users/LeeDr/followers","following_url":"https://api.github.com/users/LeeDr/following{/other_user}","gists_url":"https://api.github.com/users/LeeDr/gists{/gist_id}","starred_url":"https://api.github.com/users/LeeDr/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/LeeDr/subscriptions","organizations_url":"https://api.github.com/users/LeeDr/orgs","repos_url":"https://api.github.com/users/LeeDr/repos","events_url":"https://api.github.com/users/LeeDr/events{/privacy}","received_events_url":"https://api.github.com/users/LeeDr/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2015-12-22T19:30:09Z","updated_at":"2019-08-17T19:09:54Z","closed_at":"2017-08-26T13:09:55Z","author_association":"NONE","body":"I'm running tests on Firefox 43 with Selenium 2.48.2 in node.js environment (testing https://github.com/elastic/kibana).\nI'm passing user:password@ url in the get command.  My test environment is actually not authenticating yet but will in the future so I went ahead and started sending the test credentials we're going to use.\nIf the url in the get command is the same as what the browser already has loaded it can hang.  I'll attach a file I created to reproduce the issue.  In the test I get the same url over and over.  The first 11 times without credentials in the url (it all works).  The next 11 attempts with credentials hangs on the 3rd get.\n\nWith ChromeDriver it doesn't hang.\nCommented-out in the attached test file is a wrapper for get that inserts a unique query parameter (timestamp).  It slows down the get but doesn't hang.  (sorry wikipedia for using you as a test page).\n\nOK, I can't attach a .js file.  I'll paste here;\n\n```\ndefine(function (require) {\n  var _ = require('intern/dojo/node!lodash');\n  var parse = require('intern/dojo/node!url').parse;\n  var format = require('intern/dojo/node!url').format;\n  var bdd = require('intern!bdd');\n\n  bdd.describe('discover app', function describeIndexTests() {\n    var remote;\n\n    bdd.describe('query', function () {\n\n      bdd.it('should show correct time range string', function () {\n\n        var remote = this.remote;\n\n        // if (this.remote.get === remote.constructor.prototype.get) {\n        //   this.remote.get = _.wrap(this.remote.get, function (func, url) {\n        //     console.log('url = ' + url);\n        //     var parsed = parse(url, true);\n        //     if (parsed.query === null) {\n        //       parsed.query = {};\n        //     }\n        //     parsed.query._t = Date.now();\n        //     var formatted = format(_.pick(parsed, 'protocol', 'hostname', 'port', 'pathname', 'query', 'hash', 'auth'));\n        //     console.log('formatted url = ' + formatted);\n        //     return func.call(this, formatted);\n        //   });\n        // }\n\n        console.log('get 1');\n        return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem')\n        .then(function () {\n          console.log('get 2');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 3');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 4');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 5');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 6');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 7');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 8');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 9');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 10');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 11');\n          return remote.get('https://en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('----------------------done without auth');\n        })\n        .then(function () {\n          console.log('get 1');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 2');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 3');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 4');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 5');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 6');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 7');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 8');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 9');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 10');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('get 11');\n          return remote.get('https://user:notsecure@en.wikipedia.org/wiki/FIFA#Anthem');\n        })\n        .then(function () {\n          console.log('----------------------done with auth');\n        });\n      });\n\n\n    });\n  });\n});\n\n```\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["315602766","317998060","318002023","325125975"], "labels":["D-firefox"]}