{"id":"4911", "title":"Unable to emulate captureEntirePageScreenshot on html-runner-3.6.0", "body":"## Meta -
OS:  Windows 8.1
<!-- Windows 10? OSX? -->
Selenium Version:  selenium-html-runner-3.6.0.jar
<!-- 2.52.0, IDE, etc -->
Browser:  chrome 61.0 and firefox 53.0.3
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

## Expected Behavior - 
Hi, I want to perform a screenshot from selenium-html-runner-3.6.0.jar

## Actual Behavior -
Actually it's work fine with selenium IDE 2.9.1  
captureEntirePageScreenshot | C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg |  |

but with html runner, I got this error:

oct. 19, 2017 3:20:50 PM org.openqa.selenium.server.htmlrunner.CoreTestCase run
INFOS: |captureEntirePageScreenshot | C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg |  |
2017-10-19 15:20:50.045:INFO:osjs.AbstractConnector:main: Stopped ServerConnector@1506f20f{HTTP/1.1,[http/1.1]}{0.0.0.0:6804}
2017-10-19 15:20:50.049:INFO:osjsh.ContextHandler:main: Stopped o.s.j.s.h.ContextHandler@7f6f61c8{/tests,null,UNAVAILABLE}
1508419250063   Marionette      INFO    New connections will no longer be accepted
[GFX1-]: Receive IPC close with reason=AbnormalShutdown
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by org.openqa.selenium.os.ProcessUtils (file:/C:/Users/jeirgino/Desktop/Edito/selenium-html-runner-3.6.0.jar) to field java.lang.ProcessImpl.handle
WARNING: Please consider reporting this to the maintainers of org.openqa.selenium.os.ProcessUtils
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
oct. 19, 2017 3:20:51 PM org.openqa.selenium.server.htmlrunner.HTMLLauncher mainInt
AVERTISSEMENT: Test of browser failed: *firefox
org.openqa.selenium.server.htmlrunner.CoreRunnerError: Unable to emulate captureEntirePageScreenshot [C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg, ]
        at org.openqa.selenium.server.htmlrunner.ReflectivelyDiscoveredSteps.invokeMethod(ReflectivelyDiscoveredSteps.java:323)
        at org.openqa.selenium.server.htmlrunner.ReflectivelyDiscoveredSteps.lambda$null$1(ReflectivelyDiscoveredSteps.java:74)
        at org.openqa.selenium.server.htmlrunner.CoreTestCase$LoggableStep.execute(CoreTestCase.java:170)
        at org.openqa.selenium.server.htmlrunner.NextStepDecorator.evaluate(NextStepDecorator.java:46)
        at org.openqa.selenium.server.htmlrunner.CoreTestCase.run(CoreTestCase.java:71)
        at org.openqa.selenium.server.htmlrunner.CoreTestSuite.run(CoreTestSuite.java:73)
        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.runHTMLSuite(HTMLLauncher.java:118)
        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.mainInt(HTMLLauncher.java:245)
        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.main(HTMLLauncher.java:273)

## Steps to reproduce -
java -jar selenium-html-runner-3.6.0.jar -htmlSuite *firefox http://myurl.com testSuite.html log.html -timeout 15


Anyone can help me please ?
<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4911","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4911/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4911/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4911/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4911","id":266855122,"node_id":"MDU6SXNzdWUyNjY4NTUxMjI=","number":4911,"title":"Unable to emulate captureEntirePageScreenshot on html-runner-3.6.0","user":{"login":"jeirgino","id":7736427,"node_id":"MDQ6VXNlcjc3MzY0Mjc=","avatar_url":"https://avatars3.githubusercontent.com/u/7736427?v=4","gravatar_id":"","url":"https://api.github.com/users/jeirgino","html_url":"https://github.com/jeirgino","followers_url":"https://api.github.com/users/jeirgino/followers","following_url":"https://api.github.com/users/jeirgino/following{/other_user}","gists_url":"https://api.github.com/users/jeirgino/gists{/gist_id}","starred_url":"https://api.github.com/users/jeirgino/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jeirgino/subscriptions","organizations_url":"https://api.github.com/users/jeirgino/orgs","repos_url":"https://api.github.com/users/jeirgino/repos","events_url":"https://api.github.com/users/jeirgino/events{/privacy}","received_events_url":"https://api.github.com/users/jeirgino/received_events","type":"User","site_admin":false},"labels":[{"id":466477920,"node_id":"MDU6TGFiZWw0NjY0Nzc5MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-htmlrunner","name":"C-htmlrunner","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-10-19T14:05:44Z","updated_at":"2019-08-15T10:09:35Z","closed_at":"2018-09-25T20:56:19Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 8.1\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  selenium-html-runner-3.6.0.jar\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  chrome 61.0 and firefox 53.0.3\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\n## Expected Behavior - \r\nHi, I want to perform a screenshot from selenium-html-runner-3.6.0.jar\r\n\r\n## Actual Behavior -\r\nActually it's work fine with selenium IDE 2.9.1  \r\ncaptureEntirePageScreenshot | C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg |  |\r\n\r\nbut with html runner, I got this error:\r\n\r\noct. 19, 2017 3:20:50 PM org.openqa.selenium.server.htmlrunner.CoreTestCase run\r\nINFOS: |captureEntirePageScreenshot | C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg |  |\r\n2017-10-19 15:20:50.045:INFO:osjs.AbstractConnector:main: Stopped ServerConnector@1506f20f{HTTP/1.1,[http/1.1]}{0.0.0.0:6804}\r\n2017-10-19 15:20:50.049:INFO:osjsh.ContextHandler:main: Stopped o.s.j.s.h.ContextHandler@7f6f61c8{/tests,null,UNAVAILABLE}\r\n1508419250063   Marionette      INFO    New connections will no longer be accepted\r\n[GFX1-]: Receive IPC close with reason=AbnormalShutdown\r\nWARNING: An illegal reflective access operation has occurred\r\nWARNING: Illegal reflective access by org.openqa.selenium.os.ProcessUtils (file:/C:/Users/jeirgino/Desktop/Edito/selenium-html-runner-3.6.0.jar) to field java.lang.ProcessImpl.handle\r\nWARNING: Please consider reporting this to the maintainers of org.openqa.selenium.os.ProcessUtils\r\nWARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations\r\nWARNING: All illegal access operations will be denied in a future release\r\noct. 19, 2017 3:20:51 PM org.openqa.selenium.server.htmlrunner.HTMLLauncher mainInt\r\nAVERTISSEMENT: Test of browser failed: *firefox\r\norg.openqa.selenium.server.htmlrunner.CoreRunnerError: Unable to emulate captureEntirePageScreenshot [C:\\Users\\jeirgino\\Documents\\myscreenshot.jpg, ]\r\n        at org.openqa.selenium.server.htmlrunner.ReflectivelyDiscoveredSteps.invokeMethod(ReflectivelyDiscoveredSteps.java:323)\r\n        at org.openqa.selenium.server.htmlrunner.ReflectivelyDiscoveredSteps.lambda$null$1(ReflectivelyDiscoveredSteps.java:74)\r\n        at org.openqa.selenium.server.htmlrunner.CoreTestCase$LoggableStep.execute(CoreTestCase.java:170)\r\n        at org.openqa.selenium.server.htmlrunner.NextStepDecorator.evaluate(NextStepDecorator.java:46)\r\n        at org.openqa.selenium.server.htmlrunner.CoreTestCase.run(CoreTestCase.java:71)\r\n        at org.openqa.selenium.server.htmlrunner.CoreTestSuite.run(CoreTestSuite.java:73)\r\n        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.runHTMLSuite(HTMLLauncher.java:118)\r\n        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.mainInt(HTMLLauncher.java:245)\r\n        at org.openqa.selenium.server.htmlrunner.HTMLLauncher.main(HTMLLauncher.java:273)\r\n\r\n## Steps to reproduce -\r\njava -jar selenium-html-runner-3.6.0.jar -htmlSuite *firefox http://myurl.com testSuite.html log.html -timeout 15\r\n\r\n\r\nAnyone can help me please ?\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["424498627"], "labels":["C-htmlrunner"]}