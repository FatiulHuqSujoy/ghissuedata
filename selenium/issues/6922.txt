{"id":"6922", "title":"Clicking only works the first time, and fill_in doesn't work.", "body":"## 🐛 Bug Report

`.click(x: 0, y: 0)` (Firefox) and some `click_at` method I found (chrome-driver) only worked the first few clicks (from 1 to ~5) then didn't work anymore (returns `nil` but the clicks don't happen)

```
Capybara::Node::Element.class_eval do
  def click_at(x, y)
    right = x - (native.size.width / 2)
    top = y - (native.size.height / 2)
    driver.browser.action.move_to(native).move_by(right.to_i, top.to_i).click.perform
  end
end
```

`fill_in` didn't work (I was getting some weird \"value\" \"recursive\" error from chrome driver), so I had to use:

```
def hacked_fill_in(selector, value)
  C.accept_confirm {
    C.execute_script(\"alert($('#{selector}').val('#{value}'))\")
  }
end
```

OS: OSX
Browser: Firefox / Chrome
Browser version: 65.0 / 72.0
Browser Driver version: geckodriver 0.23.0 / ChromeDriver 2.34.522932
Language Bindings version: Ruby 2.5.0 with Capybara

Sorry for filling multiple bugs. I have various more or less hacky workarounds.", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6922","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6922/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6922/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6922/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6922","id":408256625,"node_id":"MDU6SXNzdWU0MDgyNTY2MjU=","number":6922,"title":"Clicking only works the first time, and fill_in doesn't work.","user":{"login":"localhostdotdev","id":47308085,"node_id":"MDQ6VXNlcjQ3MzA4MDg1","avatar_url":"https://avatars3.githubusercontent.com/u/47308085?v=4","gravatar_id":"","url":"https://api.github.com/users/localhostdotdev","html_url":"https://github.com/localhostdotdev","followers_url":"https://api.github.com/users/localhostdotdev/followers","following_url":"https://api.github.com/users/localhostdotdev/following{/other_user}","gists_url":"https://api.github.com/users/localhostdotdev/gists{/gist_id}","starred_url":"https://api.github.com/users/localhostdotdev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/localhostdotdev/subscriptions","organizations_url":"https://api.github.com/users/localhostdotdev/orgs","repos_url":"https://api.github.com/users/localhostdotdev/repos","events_url":"https://api.github.com/users/localhostdotdev/events{/privacy}","received_events_url":"https://api.github.com/users/localhostdotdev/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2019-02-08T17:45:28Z","updated_at":"2019-08-14T22:09:44Z","closed_at":"2019-02-08T19:59:16Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\n`.click(x: 0, y: 0)` (Firefox) and some `click_at` method I found (chrome-driver) only worked the first few clicks (from 1 to ~5) then didn't work anymore (returns `nil` but the clicks don't happen)\r\n\r\n```\r\nCapybara::Node::Element.class_eval do\r\n  def click_at(x, y)\r\n    right = x - (native.size.width / 2)\r\n    top = y - (native.size.height / 2)\r\n    driver.browser.action.move_to(native).move_by(right.to_i, top.to_i).click.perform\r\n  end\r\nend\r\n```\r\n\r\n`fill_in` didn't work (I was getting some weird \"value\" \"recursive\" error from chrome driver), so I had to use:\r\n\r\n```\r\ndef hacked_fill_in(selector, value)\r\n  C.accept_confirm {\r\n    C.execute_script(\"alert($('#{selector}').val('#{value}'))\")\r\n  }\r\nend\r\n```\r\n\r\nOS: OSX\r\nBrowser: Firefox / Chrome\r\nBrowser version: 65.0 / 72.0\r\nBrowser Driver version: geckodriver 0.23.0 / ChromeDriver 2.34.522932\r\nLanguage Bindings version: Ruby 2.5.0 with Capybara\r\n\r\nSorry for filling multiple bugs. I have various more or less hacky workarounds.","closed_by":{"login":"twalpole","id":16556,"node_id":"MDQ6VXNlcjE2NTU2","avatar_url":"https://avatars2.githubusercontent.com/u/16556?v=4","gravatar_id":"","url":"https://api.github.com/users/twalpole","html_url":"https://github.com/twalpole","followers_url":"https://api.github.com/users/twalpole/followers","following_url":"https://api.github.com/users/twalpole/following{/other_user}","gists_url":"https://api.github.com/users/twalpole/gists{/gist_id}","starred_url":"https://api.github.com/users/twalpole/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/twalpole/subscriptions","organizations_url":"https://api.github.com/users/twalpole/orgs","repos_url":"https://api.github.com/users/twalpole/repos","events_url":"https://api.github.com/users/twalpole/events{/privacy}","received_events_url":"https://api.github.com/users/twalpole/received_events","type":"User","site_admin":false}}", "commentIds":["461927516","461928035","461940431","461963257"], "labels":[]}