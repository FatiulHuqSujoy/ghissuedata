{"id":"6947", "title":"Could not convert 'text' to string on Firefox", "body":"## 🐛 Bug Report

I started getting the error `Could not convert 'text' to string` on Firefox. I know there is an old issue about this but I have updated version of the selenium web driver and I am still getting this error (just from today).


## To Reproduce

Use a script `ElementFinder.sendKeys(\"somevalue\")` and execute it on browser stack in firefox

## Expected behavior

Script should work

## Test script or set of commands reproducing this issue

`ElementFinder.sendKeys()`

## Environment

Platform	 Windows 10
Browser	 Firefox 65.0
Browserstack

## Observations

When I changed the following piece of code on `lib/webdriver.js`

```javascript
    if (!this.driver_.fileDetector_) {
      return this.schedule_(
          new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).
              setParameter('text', keys.then(keys => keys.join(''))).
              setParameter('value', keys),
          'WebElement.sendKeys()');
    }
```

to

```javascript
    if (!this.driver_.fileDetector_) {
      return this.schedule_(
          new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).
              setParameter('text', keys.then(keys => keys.join(''))).
              setParameter('value', keys.then(keys => keys.join(''))),
          'WebElement.sendKeys()');
    }
```
made it work", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6947","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6947/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6947/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6947/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6947","id":411741933,"node_id":"MDU6SXNzdWU0MTE3NDE5MzM=","number":6947,"title":"Could not convert 'text' to string on Firefox","user":{"login":"dparne","id":1863682,"node_id":"MDQ6VXNlcjE4NjM2ODI=","avatar_url":"https://avatars1.githubusercontent.com/u/1863682?v=4","gravatar_id":"","url":"https://api.github.com/users/dparne","html_url":"https://github.com/dparne","followers_url":"https://api.github.com/users/dparne/followers","following_url":"https://api.github.com/users/dparne/following{/other_user}","gists_url":"https://api.github.com/users/dparne/gists{/gist_id}","starred_url":"https://api.github.com/users/dparne/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dparne/subscriptions","organizations_url":"https://api.github.com/users/dparne/orgs","repos_url":"https://api.github.com/users/dparne/repos","events_url":"https://api.github.com/users/dparne/events{/privacy}","received_events_url":"https://api.github.com/users/dparne/received_events","type":"User","site_admin":false},"labels":[{"id":182515289,"node_id":"MDU6TGFiZWwxODI1MTUyODk=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-nodejs","name":"C-nodejs","color":"fbca04","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-02-19T04:37:50Z","updated_at":"2019-08-14T21:09:52Z","closed_at":"2019-02-20T15:15:27Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nI started getting the error `Could not convert 'text' to string` on Firefox. I know there is an old issue about this but I have updated version of the selenium web driver and I am still getting this error (just from today).\r\n\r\n\r\n## To Reproduce\r\n\r\nUse a script `ElementFinder.sendKeys(\"somevalue\")` and execute it on browser stack in firefox\r\n\r\n## Expected behavior\r\n\r\nScript should work\r\n\r\n## Test script or set of commands reproducing this issue\r\n\r\n`ElementFinder.sendKeys()`\r\n\r\n## Environment\r\n\r\nPlatform\t Windows 10\r\nBrowser\t Firefox 65.0\r\nBrowserstack\r\n\r\n## Observations\r\n\r\nWhen I changed the following piece of code on `lib/webdriver.js`\r\n\r\n```javascript\r\n    if (!this.driver_.fileDetector_) {\r\n      return this.schedule_(\r\n          new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).\r\n              setParameter('text', keys.then(keys => keys.join(''))).\r\n              setParameter('value', keys),\r\n          'WebElement.sendKeys()');\r\n    }\r\n```\r\n\r\nto\r\n\r\n```javascript\r\n    if (!this.driver_.fileDetector_) {\r\n      return this.schedule_(\r\n          new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).\r\n              setParameter('text', keys.then(keys => keys.join(''))).\r\n              setParameter('value', keys.then(keys => keys.join(''))),\r\n          'WebElement.sendKeys()');\r\n    }\r\n```\r\nmade it work","closed_by":{"login":"dparne","id":1863682,"node_id":"MDQ6VXNlcjE4NjM2ODI=","avatar_url":"https://avatars1.githubusercontent.com/u/1863682?v=4","gravatar_id":"","url":"https://api.github.com/users/dparne","html_url":"https://github.com/dparne","followers_url":"https://api.github.com/users/dparne/followers","following_url":"https://api.github.com/users/dparne/following{/other_user}","gists_url":"https://api.github.com/users/dparne/gists{/gist_id}","starred_url":"https://api.github.com/users/dparne/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dparne/subscriptions","organizations_url":"https://api.github.com/users/dparne/orgs","repos_url":"https://api.github.com/users/dparne/repos","events_url":"https://api.github.com/users/dparne/events{/privacy}","received_events_url":"https://api.github.com/users/dparne/received_events","type":"User","site_admin":false}}", "commentIds":["465439858","465442214","465619902"], "labels":["C-nodejs","R-awaiting answer"]}