{"id":"814", "title":"Webdriver get text includes space from within HTML tags", "body":"Using this perl webdriver code (simplified for the example):

``` perl
my $page_header = $webdriver->find_element( 'pageHeading', 'id' )->get_text;
like(
                    $page_header,
                    qr/PageTitle]/s,
                    \"Page \\\"PageTitle\\\" loaded after it was set to default and has correct heading\"
                );
```

I was getting this error (note the leading space)

```
    #   at page_headers.test line 342.
    #                   'PageTitle'
    #     doesn't match '(?^s: PageTitle)'
```

The problem turned out to be a trailing space in the HTML element:

`<span id=\"pageHeading\" >PageTitle</span>`

get_text should not be getting spaces in the html tag before the closing angle bracket.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/814","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/814/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/814/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/814/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/814","id":96143660,"node_id":"MDU6SXNzdWU5NjE0MzY2MA==","number":814,"title":"Webdriver get text includes space from within HTML tags","user":{"login":"TraceyC77","id":10402612,"node_id":"MDQ6VXNlcjEwNDAyNjEy","avatar_url":"https://avatars3.githubusercontent.com/u/10402612?v=4","gravatar_id":"","url":"https://api.github.com/users/TraceyC77","html_url":"https://github.com/TraceyC77","followers_url":"https://api.github.com/users/TraceyC77/followers","following_url":"https://api.github.com/users/TraceyC77/following{/other_user}","gists_url":"https://api.github.com/users/TraceyC77/gists{/gist_id}","starred_url":"https://api.github.com/users/TraceyC77/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TraceyC77/subscriptions","organizations_url":"https://api.github.com/users/TraceyC77/orgs","repos_url":"https://api.github.com/users/TraceyC77/repos","events_url":"https://api.github.com/users/TraceyC77/events{/privacy}","received_events_url":"https://api.github.com/users/TraceyC77/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2015-07-20T19:47:52Z","updated_at":"2019-08-21T08:09:45Z","closed_at":"2015-07-21T14:48:23Z","author_association":"NONE","body":"Using this perl webdriver code (simplified for the example):\n\n``` perl\nmy $page_header = $webdriver->find_element( 'pageHeading', 'id' )->get_text;\nlike(\n                    $page_header,\n                    qr/PageTitle]/s,\n                    \"Page \\\"PageTitle\\\" loaded after it was set to default and has correct heading\"\n                );\n```\n\nI was getting this error (note the leading space)\n\n```\n    #   at page_headers.test line 342.\n    #                   'PageTitle'\n    #     doesn't match '(?^s: PageTitle)'\n```\n\nThe problem turned out to be a trailing space in the HTML element:\n\n`<span id=\"pageHeading\" >PageTitle</span>`\n\nget_text should not be getting spaces in the html tag before the closing angle bracket.\n","closed_by":{"login":"TraceyC77","id":10402612,"node_id":"MDQ6VXNlcjEwNDAyNjEy","avatar_url":"https://avatars3.githubusercontent.com/u/10402612?v=4","gravatar_id":"","url":"https://api.github.com/users/TraceyC77","html_url":"https://github.com/TraceyC77","followers_url":"https://api.github.com/users/TraceyC77/followers","following_url":"https://api.github.com/users/TraceyC77/following{/other_user}","gists_url":"https://api.github.com/users/TraceyC77/gists{/gist_id}","starred_url":"https://api.github.com/users/TraceyC77/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TraceyC77/subscriptions","organizations_url":"https://api.github.com/users/TraceyC77/orgs","repos_url":"https://api.github.com/users/TraceyC77/repos","events_url":"https://api.github.com/users/TraceyC77/events{/privacy}","received_events_url":"https://api.github.com/users/TraceyC77/received_events","type":"User","site_admin":false}}", "commentIds":["123353368","123353464"], "labels":[]}