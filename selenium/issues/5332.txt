{"id":"5332", "title":"Selenium Grid - Firefox - UnknownCommandError: POST /session/{sessionId}/moveto did not match a known command", "body":"## Meta -
OS:  OSX
<!-- Windows 10? OSX? -->
Selenium Grid Version:  3.8.1
<!-- 2.52.0, IDE, etc -->
Browser:  Firefox
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  57.0.4
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior - Actions class function should work

## Actual Behavior - Actions class function are not working

# _**This issue is specific to selenium grid only**_

## Steps to reproduce -
Run the following protractor script in FIREFOX browser: 


`describe('Test', function () {
 'use strict';
  it('Firefox Actions class', function () {
        browser.get('http://www.protractortest.org/#/');
        browser.actions().mouseMove(element(by.css('.github-button'))).perform();
        browser.sleep(10000);
    });
})`

_**Note that the same works on Selenium grid 3.2.0**_

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5332","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5332/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5332/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5332/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5332","id":288028679,"node_id":"MDU6SXNzdWUyODgwMjg2Nzk=","number":5332,"title":"Selenium Grid - Firefox - UnknownCommandError: POST /session/{sessionId}/moveto did not match a known command","user":{"login":"sahil1610","id":7914556,"node_id":"MDQ6VXNlcjc5MTQ1NTY=","avatar_url":"https://avatars3.githubusercontent.com/u/7914556?v=4","gravatar_id":"","url":"https://api.github.com/users/sahil1610","html_url":"https://github.com/sahil1610","followers_url":"https://api.github.com/users/sahil1610/followers","following_url":"https://api.github.com/users/sahil1610/following{/other_user}","gists_url":"https://api.github.com/users/sahil1610/gists{/gist_id}","starred_url":"https://api.github.com/users/sahil1610/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sahil1610/subscriptions","organizations_url":"https://api.github.com/users/sahil1610/orgs","repos_url":"https://api.github.com/users/sahil1610/repos","events_url":"https://api.github.com/users/sahil1610/events{/privacy}","received_events_url":"https://api.github.com/users/sahil1610/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2018-01-12T07:24:33Z","updated_at":"2019-08-14T19:09:57Z","closed_at":"2018-01-12T15:08:30Z","author_association":"NONE","body":"## Meta -\r\nOS:  OSX\r\n<!-- Windows 10? OSX? -->\r\nSelenium Grid Version:  3.8.1\r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Firefox\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  57.0.4\r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior - Actions class function should work\r\n\r\n## Actual Behavior - Actions class function are not working\r\n\r\n# _**This issue is specific to selenium grid only**_\r\n\r\n## Steps to reproduce -\r\nRun the following protractor script in FIREFOX browser: \r\n\r\n\r\n`describe('Test', function () {\r\n 'use strict';\r\n  it('Firefox Actions class', function () {\r\n        browser.get('http://www.protractortest.org/#/');\r\n        browser.actions().mouseMove(element(by.css('.github-button'))).perform();\r\n        browser.sleep(10000);\r\n    });\r\n})`\r\n\r\n_**Note that the same works on Selenium grid 3.2.0**_\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["357262767","357370459","357449841","357602924","361829263","414813969","414817393","472488983"], "labels":[]}