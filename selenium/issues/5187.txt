{"id":"5187", "title":"WebDriverException in python binding of 'set_page_load_timeout'", "body":"## Meta -
OS:  Ubuntu 16.04.3
Selenium Version:  3.8.0
Browser:   Firefox 57.0 / geckodriver 0.16.0




## Expected Behavior -

The method `set_page_load_timeout`  works as described in the [documentation](http://selenium-python.readthedocs.io/api.html)

## Actual Behavior -

```
Traceback (most recent call last):
  File \"example.py\", line 3, in <module>
    driver.set_page_load_timeout(3)
  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 833, in set_page_load_timeout
    'type': 'page load'})
  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 311, in execute
    self.error_handler.check_response(response)
  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/errorhandler.py\", line 237, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.WebDriverException: Message: timeouts
```


## Steps to reproduce -

```
from selenium import webdriver
driver = webdriver.Firefox()
driver.set_page_load_timeout(3)
driver.get(\"http://selenium-python.readthedocs.io\")
```

with python 2.7.12
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5187","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5187/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5187/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5187/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5187","id":280129372,"node_id":"MDU6SXNzdWUyODAxMjkzNzI=","number":5187,"title":"WebDriverException in python binding of 'set_page_load_timeout'","user":{"login":"alex4200","id":12729291,"node_id":"MDQ6VXNlcjEyNzI5Mjkx","avatar_url":"https://avatars3.githubusercontent.com/u/12729291?v=4","gravatar_id":"","url":"https://api.github.com/users/alex4200","html_url":"https://github.com/alex4200","followers_url":"https://api.github.com/users/alex4200/followers","following_url":"https://api.github.com/users/alex4200/following{/other_user}","gists_url":"https://api.github.com/users/alex4200/gists{/gist_id}","starred_url":"https://api.github.com/users/alex4200/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/alex4200/subscriptions","organizations_url":"https://api.github.com/users/alex4200/orgs","repos_url":"https://api.github.com/users/alex4200/repos","events_url":"https://api.github.com/users/alex4200/events{/privacy}","received_events_url":"https://api.github.com/users/alex4200/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2017-12-07T13:27:12Z","updated_at":"2019-08-16T21:09:58Z","closed_at":"2017-12-07T14:27:33Z","author_association":"NONE","body":"## Meta -\r\nOS:  Ubuntu 16.04.3\r\nSelenium Version:  3.8.0\r\nBrowser:   Firefox 57.0 / geckodriver 0.16.0\r\n\r\n\r\n\r\n\r\n## Expected Behavior -\r\n\r\nThe method `set_page_load_timeout`  works as described in the [documentation](http://selenium-python.readthedocs.io/api.html)\r\n\r\n## Actual Behavior -\r\n\r\n```\r\nTraceback (most recent call last):\r\n  File \"example.py\", line 3, in <module>\r\n    driver.set_page_load_timeout(3)\r\n  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 833, in set_page_load_timeout\r\n    'type': 'page load'})\r\n  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/webdriver.py\", line 311, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"/usr/local/lib/python2.7/dist-packages/selenium/webdriver/remote/errorhandler.py\", line 237, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.WebDriverException: Message: timeouts\r\n```\r\n\r\n\r\n## Steps to reproduce -\r\n\r\n```\r\nfrom selenium import webdriver\r\ndriver = webdriver.Firefox()\r\ndriver.set_page_load_timeout(3)\r\ndriver.get(\"http://selenium-python.readthedocs.io\")\r\n```\r\n\r\nwith python 2.7.12\r\n","closed_by":{"login":"lmtierney","id":4405962,"node_id":"MDQ6VXNlcjQ0MDU5NjI=","avatar_url":"https://avatars1.githubusercontent.com/u/4405962?v=4","gravatar_id":"","url":"https://api.github.com/users/lmtierney","html_url":"https://github.com/lmtierney","followers_url":"https://api.github.com/users/lmtierney/followers","following_url":"https://api.github.com/users/lmtierney/following{/other_user}","gists_url":"https://api.github.com/users/lmtierney/gists{/gist_id}","starred_url":"https://api.github.com/users/lmtierney/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lmtierney/subscriptions","organizations_url":"https://api.github.com/users/lmtierney/orgs","repos_url":"https://api.github.com/users/lmtierney/repos","events_url":"https://api.github.com/users/lmtierney/events{/privacy}","received_events_url":"https://api.github.com/users/lmtierney/received_events","type":"User","site_admin":false}}", "commentIds":["349982442","350283166","350284437","350284441","350290415","350319093","350339188"], "labels":[]}