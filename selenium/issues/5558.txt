{"id":"5558", "title":"select2 menus do not work in firefox-driver", "body":"## Meta -
OS:  
 any
Selenium Version:  
 any after 3.3.1, tested up to 3.8.1
Browser:  
 Firefox
Browser Version:  
 any after 52, tested up to 58

## Expected Behavior -
 select2 (see https://select2.org/dropdown) menus should work in Firefox, same as other browsers

## Actual Behavior -
 select2 do not work correctly

## Steps to reproduce -
I have used the following (Java) code running through BrowserStack to test on various browsers.

```
driver.get(\"https://select2.org/dropdown\");

List<WebElement> selectElements = driver.findElements(By.tagName(\"select\"));
Assert.assertEquals(1, selectElements.size());

Select select = new Select(selectElements.get(0));
select.selectByValue(\"NV\");
```

If the RemoteDriver is latest Firefox using latest Selenium version, I get:
```
org.openqa.selenium.WebDriverException: Element <option> could not be scrolled into view
Build info: version: '3.8.1', revision: '6e95a6684b', time: '2017-12-01T19:05:32.194Z'
System info: host: 'mac-207-254-53-65.browserstack.com', ip: 'fe80:0:0:0:75:bdb3:579e:f092%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.13.3', java.version: '1.8.0_51'
Driver info: driver.version: unknown
Command duration or timeout: 0 milliseconds
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
	at org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)
	at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)
	at org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)
	at org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)
	at org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)
	at org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:160)
	at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)
	at org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)
	at org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:83)
	at org.openqa.selenium.support.ui.Select.setSelected(Select.java:324)
	at org.openqa.selenium.support.ui.Select.selectByValue(Select.java:201)
	at ...
Caused by: org.openqa.selenium.WebDriverException: Element <option> could not be scrolled into view
Build info: version: '3.8.1', revision: '6e95a6684b', time: '2017-12-01T19:05:32.194Z'
System info: host: 'mac-207-254-53-65.browserstack.com', ip: 'fe80:0:0:0:75:bdb3:579e:f092%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.13.3', java.version: '1.8.0_51'
Driver info: driver.version: unknown
Build info: version: '3.9.1', revision: '63f7b50', time: '2018-02-07T22:25:02.294Z'
System info: host: 'SCG00845', ip: 'fe80:0:0:0:14:b5bd:eeda:a89a%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.6', java.version: '1.8.0_144'
Driver info: driver.version: unknown
```

This code works fine with other drivers - tested with Chrome, Edge, and IE. But more importantly: **this code works with Selenium version 3.3.1 + (last supported) Firefox 52**.

A workaround can be based on [this SO answer](https://stackoverflow.com/a/17757761/3124333).", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5558","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5558/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5558/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5558/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5558","id":301560214,"node_id":"MDU6SXNzdWUzMDE1NjAyMTQ=","number":5558,"title":"select2 menus do not work in firefox-driver","user":{"login":"SiKing","id":5496738,"node_id":"MDQ6VXNlcjU0OTY3Mzg=","avatar_url":"https://avatars2.githubusercontent.com/u/5496738?v=4","gravatar_id":"","url":"https://api.github.com/users/SiKing","html_url":"https://github.com/SiKing","followers_url":"https://api.github.com/users/SiKing/followers","following_url":"https://api.github.com/users/SiKing/following{/other_user}","gists_url":"https://api.github.com/users/SiKing/gists{/gist_id}","starred_url":"https://api.github.com/users/SiKing/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/SiKing/subscriptions","organizations_url":"https://api.github.com/users/SiKing/orgs","repos_url":"https://api.github.com/users/SiKing/repos","events_url":"https://api.github.com/users/SiKing/events{/privacy}","received_events_url":"https://api.github.com/users/SiKing/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":12,"created_at":"2018-03-01T21:09:22Z","updated_at":"2019-08-15T23:09:51Z","closed_at":"2018-03-02T06:35:04Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\n any\r\nSelenium Version:  \r\n any after 3.3.1, tested up to 3.8.1\r\nBrowser:  \r\n Firefox\r\nBrowser Version:  \r\n any after 52, tested up to 58\r\n\r\n## Expected Behavior -\r\n select2 (see https://select2.org/dropdown) menus should work in Firefox, same as other browsers\r\n\r\n## Actual Behavior -\r\n select2 do not work correctly\r\n\r\n## Steps to reproduce -\r\nI have used the following (Java) code running through BrowserStack to test on various browsers.\r\n\r\n```\r\ndriver.get(\"https://select2.org/dropdown\");\r\n\r\nList<WebElement> selectElements = driver.findElements(By.tagName(\"select\"));\r\nAssert.assertEquals(1, selectElements.size());\r\n\r\nSelect select = new Select(selectElements.get(0));\r\nselect.selectByValue(\"NV\");\r\n```\r\n\r\nIf the RemoteDriver is latest Firefox using latest Selenium version, I get:\r\n```\r\norg.openqa.selenium.WebDriverException: Element <option> could not be scrolled into view\r\nBuild info: version: '3.8.1', revision: '6e95a6684b', time: '2017-12-01T19:05:32.194Z'\r\nSystem info: host: 'mac-207-254-53-65.browserstack.com', ip: 'fe80:0:0:0:75:bdb3:579e:f092%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.13.3', java.version: '1.8.0_51'\r\nDriver info: driver.version: unknown\r\nCommand duration or timeout: 0 milliseconds\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:160)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:83)\r\n\tat org.openqa.selenium.support.ui.Select.setSelected(Select.java:324)\r\n\tat org.openqa.selenium.support.ui.Select.selectByValue(Select.java:201)\r\n\tat ...\r\nCaused by: org.openqa.selenium.WebDriverException: Element <option> could not be scrolled into view\r\nBuild info: version: '3.8.1', revision: '6e95a6684b', time: '2017-12-01T19:05:32.194Z'\r\nSystem info: host: 'mac-207-254-53-65.browserstack.com', ip: 'fe80:0:0:0:75:bdb3:579e:f092%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.13.3', java.version: '1.8.0_51'\r\nDriver info: driver.version: unknown\r\nBuild info: version: '3.9.1', revision: '63f7b50', time: '2018-02-07T22:25:02.294Z'\r\nSystem info: host: 'SCG00845', ip: 'fe80:0:0:0:14:b5bd:eeda:a89a%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.6', java.version: '1.8.0_144'\r\nDriver info: driver.version: unknown\r\n```\r\n\r\nThis code works fine with other drivers - tested with Chrome, Edge, and IE. But more importantly: **this code works with Selenium version 3.3.1 + (last supported) Firefox 52**.\r\n\r\nA workaround can be based on [this SO answer](https://stackoverflow.com/a/17757761/3124333).","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["369835054","369972795","369979236","370126455","394721486","394752227","394929624","394930055","395110729","395312595","395464775","395631455"], "labels":[]}