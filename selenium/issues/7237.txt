{"id":"7237", "title":"WebDriverWait doesn't work for EventFiringWebDriver", "body":"## 🐛 Bug Report

A clear and concise description of what the bug is.

<!-- NOTE
FIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

-->
I wrapped the ChromeDriver with EventFiringWebDriver to capture screenshot when exception occurs , but found that the WebDriverWait() functionality is broken.

## To Reproduce

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->


Detailed steps to reproduce the behavior:

**Set up**

_driver = new ChromeDriver(options);
 _driver = new EventFiringWebDriver(_driver);

Then,
new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(d => d.FindElement(locator));
It returned immediately without waiting.

If I comment out the line ` _driver = new EventFiringWebDriver(_driver);`, it works.

## Expected behavior

The WebDriverWait should also work for EventFiringWebDriver

## Environment

OS: Windows 10
Browser: Chrome 
Browser version:  74.0.3729.169 (Official Build) (64-bit)
Browser Driver version: 3.141.0.0 <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->
Language Bindings version:  .Net framework 4.5 <!-- e.g.: Java 3.141.0 --> 
Selenium Grid version (if applicable): <!-- e.g.: 3.141.59 --> 
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7237","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7237/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7237/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7237/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7237","id":448635011,"node_id":"MDU6SXNzdWU0NDg2MzUwMTE=","number":7237,"title":"WebDriverWait doesn't work for EventFiringWebDriver","user":{"login":"qszhuan","id":1080538,"node_id":"MDQ6VXNlcjEwODA1Mzg=","avatar_url":"https://avatars3.githubusercontent.com/u/1080538?v=4","gravatar_id":"","url":"https://api.github.com/users/qszhuan","html_url":"https://github.com/qszhuan","followers_url":"https://api.github.com/users/qszhuan/followers","following_url":"https://api.github.com/users/qszhuan/following{/other_user}","gists_url":"https://api.github.com/users/qszhuan/gists{/gist_id}","starred_url":"https://api.github.com/users/qszhuan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/qszhuan/subscriptions","organizations_url":"https://api.github.com/users/qszhuan/orgs","repos_url":"https://api.github.com/users/qszhuan/repos","events_url":"https://api.github.com/users/qszhuan/events{/privacy}","received_events_url":"https://api.github.com/users/qszhuan/received_events","type":"User","site_admin":false},"labels":[{"id":182503933,"node_id":"MDU6TGFiZWwxODI1MDM5MzM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-dotnet","name":"C-dotnet","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-05-27T02:00:40Z","updated_at":"2019-08-14T09:09:56Z","closed_at":"2019-05-30T01:41:20Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nA clear and concise description of what the bug is.\r\n\r\n<!-- NOTE\r\nFIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\n-->\r\nI wrapped the ChromeDriver with EventFiringWebDriver to capture screenshot when exception occurs , but found that the WebDriverWait() functionality is broken.\r\n\r\n## To Reproduce\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n\r\n\r\nDetailed steps to reproduce the behavior:\r\n\r\n**Set up**\r\n\r\n_driver = new ChromeDriver(options);\r\n _driver = new EventFiringWebDriver(_driver);\r\n\r\nThen,\r\nnew WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(d => d.FindElement(locator));\r\nIt returned immediately without waiting.\r\n\r\nIf I comment out the line ` _driver = new EventFiringWebDriver(_driver);`, it works.\r\n\r\n## Expected behavior\r\n\r\nThe WebDriverWait should also work for EventFiringWebDriver\r\n\r\n## Environment\r\n\r\nOS: Windows 10\r\nBrowser: Chrome \r\nBrowser version:  74.0.3729.169 (Official Build) (64-bit)\r\nBrowser Driver version: 3.141.0.0 <!-- e.g.: ChromeDriver 2.43, GeckoDriver 0.23 -->\r\nLanguage Bindings version:  .Net framework 4.5 <!-- e.g.: Java 3.141.0 --> \r\nSelenium Grid version (if applicable): <!-- e.g.: 3.141.59 --> \r\n","closed_by":{"login":"qszhuan","id":1080538,"node_id":"MDQ6VXNlcjEwODA1Mzg=","avatar_url":"https://avatars3.githubusercontent.com/u/1080538?v=4","gravatar_id":"","url":"https://api.github.com/users/qszhuan","html_url":"https://github.com/qszhuan","followers_url":"https://api.github.com/users/qszhuan/followers","following_url":"https://api.github.com/users/qszhuan/following{/other_user}","gists_url":"https://api.github.com/users/qszhuan/gists{/gist_id}","starred_url":"https://api.github.com/users/qszhuan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/qszhuan/subscriptions","organizations_url":"https://api.github.com/users/qszhuan/orgs","repos_url":"https://api.github.com/users/qszhuan/repos","events_url":"https://api.github.com/users/qszhuan/events{/privacy}","received_events_url":"https://api.github.com/users/qszhuan/received_events","type":"User","site_admin":false}}", "commentIds":["497168776"], "labels":["C-dotnet"]}