{"id":"5510", "title":"Timeout issue with Selenium 3.9.0 & okhttp", "body":"## Meta -
OS:  Windows 7
<!-- Windows 10? OSX? -->
Selenium Version:  3.9.0 
<!-- 2.52.0, IDE, etc -->
Browser:  Chrome
<!-- Internet Explorer?  Firefox?

Since Firefox version 48, Mozilla requires all add-ons to be signed. Until
recently, Firefox support in Selenium was exclusively provided by an add-on.
As this add-on is not currently signed, this solution does not work with the
latest Firefox releases. As an alternative, Mozilla are working on a WebDriver
specification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with
the specification at this time. This means that features previously available
through Selenium will not be available using GeckoDriver.

Any issue logged here for Firefox 48 or later will be closed as a duplicate of
#2559. Our recommendation is to switch to GeckoDriver, or to continue testing
on Firefox 45 until GeckoDriver is a viable option for you. If you are
interested in helping us to sign the add-on to restore support for later
Firefox versions, please see the following comment for what's needed:
https://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

If the issue is with PhantomJS consider logging an issue with Ghostdriver:
https://github.com/detro/ghostdriver

-->

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior - 
All tests should pass with Okhttp in version 3.9.0

## Actual Behavior -
Getting org.openqa.selenium.WebDriverException: java.net.SocketException: Read Timeout error. And below error when run in AWS

org.openqa.selenium.remote.UnreachableBrowserException: org.openqa.selenium.remote.UnreachableBrowserException: Could not start a new session. Possible causes are invalid address of the remote server or browser start-up failure.
Build info: version: '3.9.0', revision: '698b3178f0', time: '2018-02-05T14:26:55.441Z'
System info: , os.name: 'Windows Server 2012 R2', os.arch: 'amd64', os.version: '6.3', java.version: '1.8.0_131'
Driver info: driver.version: RemoteWebDriver

Test passes on setting the below property:

System.setProperty(\"webdriver.http.factory\", \"apache\"); 

## Steps to reproduce -
On executing the tests cases using selenium version 3.9.0 , Getting org.openqa.selenium.WebDriverException: java.net.SocketException: Read Timeout error.

Same behavior is observed from CI  like Jenkins/Bamboo

Test pass when run individually.

<!--
Please be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/
If you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/
-->
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5510","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5510/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5510/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/5510/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/5510","id":298214669,"node_id":"MDU6SXNzdWUyOTgyMTQ2Njk=","number":5510,"title":"Timeout issue with Selenium 3.9.0 & okhttp","user":{"login":"KuzhaliDhivyan","id":36621205,"node_id":"MDQ6VXNlcjM2NjIxMjA1","avatar_url":"https://avatars1.githubusercontent.com/u/36621205?v=4","gravatar_id":"","url":"https://api.github.com/users/KuzhaliDhivyan","html_url":"https://github.com/KuzhaliDhivyan","followers_url":"https://api.github.com/users/KuzhaliDhivyan/followers","following_url":"https://api.github.com/users/KuzhaliDhivyan/following{/other_user}","gists_url":"https://api.github.com/users/KuzhaliDhivyan/gists{/gist_id}","starred_url":"https://api.github.com/users/KuzhaliDhivyan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/KuzhaliDhivyan/subscriptions","organizations_url":"https://api.github.com/users/KuzhaliDhivyan/orgs","repos_url":"https://api.github.com/users/KuzhaliDhivyan/repos","events_url":"https://api.github.com/users/KuzhaliDhivyan/events{/privacy}","received_events_url":"https://api.github.com/users/KuzhaliDhivyan/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-02-19T09:51:23Z","updated_at":"2019-08-16T13:09:39Z","closed_at":"2018-02-22T05:52:54Z","author_association":"NONE","body":"## Meta -\r\nOS:  Windows 7\r\n<!-- Windows 10? OSX? -->\r\nSelenium Version:  3.9.0 \r\n<!-- 2.52.0, IDE, etc -->\r\nBrowser:  Chrome\r\n<!-- Internet Explorer?  Firefox?\r\n\r\nSince Firefox version 48, Mozilla requires all add-ons to be signed. Until\r\nrecently, Firefox support in Selenium was exclusively provided by an add-on.\r\nAs this add-on is not currently signed, this solution does not work with the\r\nlatest Firefox releases. As an alternative, Mozilla are working on a WebDriver\r\nspecification compliant implementation named GeckoDriver. Please note that the specification is not complete, and that Selenium itself does not comply with\r\nthe specification at this time. This means that features previously available\r\nthrough Selenium will not be available using GeckoDriver.\r\n\r\nAny issue logged here for Firefox 48 or later will be closed as a duplicate of\r\n#2559. Our recommendation is to switch to GeckoDriver, or to continue testing\r\non Firefox 45 until GeckoDriver is a viable option for you. If you are\r\ninterested in helping us to sign the add-on to restore support for later\r\nFirefox versions, please see the following comment for what's needed:\r\nhttps://github.com/SeleniumHQ/selenium/issues/2942#issuecomment-259717567\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\nIf the issue is with PhantomJS consider logging an issue with Ghostdriver:\r\nhttps://github.com/detro/ghostdriver\r\n\r\n-->\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior - \r\nAll tests should pass with Okhttp in version 3.9.0\r\n\r\n## Actual Behavior -\r\nGetting org.openqa.selenium.WebDriverException: java.net.SocketException: Read Timeout error. And below error when run in AWS\r\n\r\norg.openqa.selenium.remote.UnreachableBrowserException: org.openqa.selenium.remote.UnreachableBrowserException: Could not start a new session. Possible causes are invalid address of the remote server or browser start-up failure.\r\nBuild info: version: '3.9.0', revision: '698b3178f0', time: '2018-02-05T14:26:55.441Z'\r\nSystem info: , os.name: 'Windows Server 2012 R2', os.arch: 'amd64', os.version: '6.3', java.version: '1.8.0_131'\r\nDriver info: driver.version: RemoteWebDriver\r\n\r\nTest passes on setting the below property:\r\n\r\nSystem.setProperty(\"webdriver.http.factory\", \"apache\"); \r\n\r\n## Steps to reproduce -\r\nOn executing the tests cases using selenium version 3.9.0 , Getting org.openqa.selenium.WebDriverException: java.net.SocketException: Read Timeout error.\r\n\r\nSame behavior is observed from CI  like Jenkins/Bamboo\r\n\r\nTest pass when run individually.\r\n\r\n<!--\r\nPlease be sure to include an SSCCE (Short, Self Contained, Correct [compilable] example) http://sscce.org/\r\nIf you can't provide a link to the page, consider creating a reproducible page on https://jsfiddle.net/\r\n-->\r\n","closed_by":{"login":"KuzhaliDhivyan","id":36621205,"node_id":"MDQ6VXNlcjM2NjIxMjA1","avatar_url":"https://avatars1.githubusercontent.com/u/36621205?v=4","gravatar_id":"","url":"https://api.github.com/users/KuzhaliDhivyan","html_url":"https://github.com/KuzhaliDhivyan","followers_url":"https://api.github.com/users/KuzhaliDhivyan/followers","following_url":"https://api.github.com/users/KuzhaliDhivyan/following{/other_user}","gists_url":"https://api.github.com/users/KuzhaliDhivyan/gists{/gist_id}","starred_url":"https://api.github.com/users/KuzhaliDhivyan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/KuzhaliDhivyan/subscriptions","organizations_url":"https://api.github.com/users/KuzhaliDhivyan/orgs","repos_url":"https://api.github.com/users/KuzhaliDhivyan/repos","events_url":"https://api.github.com/users/KuzhaliDhivyan/events{/privacy}","received_events_url":"https://api.github.com/users/KuzhaliDhivyan/received_events","type":"User","site_admin":false}}", "commentIds":["366667111","367229878","367575227"], "labels":[]}