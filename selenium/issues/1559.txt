{"id":"1559", "title":".NET Bindings: proposal to standardize \"localWebDriver\" /RemoteWebDriver capabilities management", "body":"I am not sure if I am using a correct term, for me there are \"LocalWebDriver\" bindings, which represent FirefoxDriver, ChromeDriver, InternetExplorerDriver classes and RemoteWebDriver binding implementation. 

The thing is \"LocalWebDriver\" and RemoteWebDriver use different ways to pass the desired capabilities to the driver. What is even worse, every driver uses it's own class, such as FirefoxOptions, ChromeOptions etc. to customize the capabilities. 

Eventually, \"LocalWebDrivers\" and RemoteWebDriver capabilities are being serialized to same JSON-wired data structure, so why do we have those multiple ways to set the capabilities inside the \"LocalWebDrivers\". 

The issue: 
- Having different capability implementations, I am not able to use the same Capability list for the local run (the run on the local machine, using \"LocalWebDrivers\") and RemoteWebDriver for same browser type. I have to implement the capability configuration individually for each browser type (e.g. Local Chrome and RemoteWebdriver). 

Proposal: 

1.Allow LocalWebDrivers to accept ICapabilities as a constructor parameters. Now only FirefoxDriver supports this, so I can use same list of capabilities for FirefoxDriver and for Firefox started via RemoteWebDriver. It is not possible to pass  ICapabilities to ChromeDriver or InternetExplorerDriver

```
        public FirefoxDriver(ICapabilities capabilities)
            : this(ExtractBinary(capabilities), ExtractProfile(capabilities), capabilities, RemoteWebDriver.DefaultCommandTimeout)
        {
        }
```

2.Remove runtime checks such as: 

```
        public void AddAdditionalCapability(string capabilityName, object capabilityValue, bool isGlobalCapability)
        {
            if (capabilityName == ChromeOptions.Capability ||
                capabilityName == CapabilityType.Proxy ||
                capabilityName == ChromeOptions.ArgumentsChromeOption ||
                capabilityName == ChromeOptions.BinaryChromeOption ||
                capabilityName == ChromeOptions.ExtensionsChromeOption ||
                capabilityName == ChromeOptions.LocalStateChromeOption ||
                capabilityName == ChromeOptions.PreferencesChromeOption ||
                capabilityName == ChromeOptions.DetachChromeOption ||
                capabilityName == ChromeOptions.DebuggerAddressChromeOption ||
                capabilityName == ChromeOptions.ExtensionsChromeOption ||
                capabilityName == ChromeOptions.ExcludeSwitchesChromeOption ||
                capabilityName == ChromeOptions.MinidumpPathChromeOption ||
                capabilityName == ChromeOptions.MobileEmulationChromeOption ||
                capabilityName == ChromeOptions.PerformanceLoggingPreferencesChromeOption ||
                capabilityName == ChromeOptions.WindowTypesChromeOption)
            {
                string message = string.Format(CultureInfo.InvariantCulture, \"There is already an option for the {0} capability. Please use that instead.\", capabilityName);
                throw new ArgumentException(message, \"capabilityName\");
            }
...
}
```

 which artificially prevent to set the capability and implement merging with ICapabilities. 

I believe this changes may reduce the configuration complexity for the WebDriver users and make the capability management more consistent. 

You can count on me for making the code changes and conducting the experiments. 
But, before doing anything, I would like to know what are @jimevans thoughts regarding this issue. 

Thank you, 
Dmytro
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1559","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1559/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1559/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1559/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1559","id":129651780,"node_id":"MDU6SXNzdWUxMjk2NTE3ODA=","number":1559,"title":".NET Bindings: proposal to standardize \"localWebDriver\" /RemoteWebDriver capabilities management","user":{"login":"dzharii","id":36020,"node_id":"MDQ6VXNlcjM2MDIw","avatar_url":"https://avatars3.githubusercontent.com/u/36020?v=4","gravatar_id":"","url":"https://api.github.com/users/dzharii","html_url":"https://github.com/dzharii","followers_url":"https://api.github.com/users/dzharii/followers","following_url":"https://api.github.com/users/dzharii/following{/other_user}","gists_url":"https://api.github.com/users/dzharii/gists{/gist_id}","starred_url":"https://api.github.com/users/dzharii/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dzharii/subscriptions","organizations_url":"https://api.github.com/users/dzharii/orgs","repos_url":"https://api.github.com/users/dzharii/repos","events_url":"https://api.github.com/users/dzharii/events{/privacy}","received_events_url":"https://api.github.com/users/dzharii/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-01-29T03:05:22Z","updated_at":"2019-08-20T16:09:49Z","closed_at":"2016-01-29T06:18:46Z","author_association":"NONE","body":"I am not sure if I am using a correct term, for me there are \"LocalWebDriver\" bindings, which represent FirefoxDriver, ChromeDriver, InternetExplorerDriver classes and RemoteWebDriver binding implementation. \n\nThe thing is \"LocalWebDriver\" and RemoteWebDriver use different ways to pass the desired capabilities to the driver. What is even worse, every driver uses it's own class, such as FirefoxOptions, ChromeOptions etc. to customize the capabilities. \n\nEventually, \"LocalWebDrivers\" and RemoteWebDriver capabilities are being serialized to same JSON-wired data structure, so why do we have those multiple ways to set the capabilities inside the \"LocalWebDrivers\". \n\nThe issue: \n- Having different capability implementations, I am not able to use the same Capability list for the local run (the run on the local machine, using \"LocalWebDrivers\") and RemoteWebDriver for same browser type. I have to implement the capability configuration individually for each browser type (e.g. Local Chrome and RemoteWebdriver). \n\nProposal: \n\n1.Allow LocalWebDrivers to accept ICapabilities as a constructor parameters. Now only FirefoxDriver supports this, so I can use same list of capabilities for FirefoxDriver and for Firefox started via RemoteWebDriver. It is not possible to pass  ICapabilities to ChromeDriver or InternetExplorerDriver\n\n```\n        public FirefoxDriver(ICapabilities capabilities)\n            : this(ExtractBinary(capabilities), ExtractProfile(capabilities), capabilities, RemoteWebDriver.DefaultCommandTimeout)\n        {\n        }\n```\n\n2.Remove runtime checks such as: \n\n```\n        public void AddAdditionalCapability(string capabilityName, object capabilityValue, bool isGlobalCapability)\n        {\n            if (capabilityName == ChromeOptions.Capability ||\n                capabilityName == CapabilityType.Proxy ||\n                capabilityName == ChromeOptions.ArgumentsChromeOption ||\n                capabilityName == ChromeOptions.BinaryChromeOption ||\n                capabilityName == ChromeOptions.ExtensionsChromeOption ||\n                capabilityName == ChromeOptions.LocalStateChromeOption ||\n                capabilityName == ChromeOptions.PreferencesChromeOption ||\n                capabilityName == ChromeOptions.DetachChromeOption ||\n                capabilityName == ChromeOptions.DebuggerAddressChromeOption ||\n                capabilityName == ChromeOptions.ExtensionsChromeOption ||\n                capabilityName == ChromeOptions.ExcludeSwitchesChromeOption ||\n                capabilityName == ChromeOptions.MinidumpPathChromeOption ||\n                capabilityName == ChromeOptions.MobileEmulationChromeOption ||\n                capabilityName == ChromeOptions.PerformanceLoggingPreferencesChromeOption ||\n                capabilityName == ChromeOptions.WindowTypesChromeOption)\n            {\n                string message = string.Format(CultureInfo.InvariantCulture, \"There is already an option for the {0} capability. Please use that instead.\", capabilityName);\n                throw new ArgumentException(message, \"capabilityName\");\n            }\n...\n}\n```\n\n which artificially prevent to set the capability and implement merging with ICapabilities. \n\nI believe this changes may reduce the configuration complexity for the WebDriver users and make the capability management more consistent. \n\nYou can count on me for making the code changes and conducting the experiments. \nBut, before doing anything, I would like to know what are @jimevans thoughts regarding this issue. \n\nThank you, \nDmytro\n","closed_by":{"login":"jimevans","id":352840,"node_id":"MDQ6VXNlcjM1Mjg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/352840?v=4","gravatar_id":"","url":"https://api.github.com/users/jimevans","html_url":"https://github.com/jimevans","followers_url":"https://api.github.com/users/jimevans/followers","following_url":"https://api.github.com/users/jimevans/following{/other_user}","gists_url":"https://api.github.com/users/jimevans/gists{/gist_id}","starred_url":"https://api.github.com/users/jimevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimevans/subscriptions","organizations_url":"https://api.github.com/users/jimevans/orgs","repos_url":"https://api.github.com/users/jimevans/repos","events_url":"https://api.github.com/users/jimevans/events{/privacy}","received_events_url":"https://api.github.com/users/jimevans/received_events","type":"User","site_admin":false}}", "commentIds":["176597788","176609153"], "labels":[]}