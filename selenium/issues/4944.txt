{"id":"4944", "title":"Can't run Firefox with Selenium Grid 3.6 implicit_wait did not match a known command", "body":"OS: OSX 10.12.5
Selenium Version: 3.3.1
Browser: Firefox 56
selenium-server-standalone: 3.6.0
Python 2.7.13
geckodriver 0.19.0

Hello everyone. I can't run firefox with selenium grid.

Python webdriver setup code: 
``
driver = webdriver.Remote(
    command_executor=\"http://localhost:4444/wd/hub\",
    desired_capabilities={
        \"browserName\": \"firefox\",
        \"marionette\": \"true\",
       \"javascriptEnabled\": True,
})
``
This is the traceback i get:

```
Traceback (most recent call last):
  File \"main_fb_user.py\", line 504, in <module>
    callback(message)
  File \"main_fb_user.py\", line 334, in callback
    wait_fb_login(browser)
  File \"/Users/eugenio/hoopygang/crawler_stage_fb/py_fb_crawler/selenium_utils.py\", line 47, in wait_fb_login
    browser.implicitly_wait(3)
  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 694, in implicitly_wait
    'ms': float(time_to_wait) * 1000})
  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 238, in execute
    self.error_handler.check_response(response)
  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/errorhandler.py\", line 193, in check_response
    raise exception_class(message, screen, stacktrace)
selenium.common.exceptions.WebDriverException: Message: POST /session/bfe672e0-a5cd-834b-b47b-f01baa195699/timeouts/implicit_wait did not match a known command
Build info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T16:15:40.131Z'
System info: host: 'MBP.station', ip: 'fe80:0:0:0:1068:d7a:cca3:e867%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.5', java.version: '9'
```
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4944","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4944/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4944/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4944/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4944","id":268209245,"node_id":"MDU6SXNzdWUyNjgyMDkyNDU=","number":4944,"title":"Can't run Firefox with Selenium Grid 3.6 implicit_wait did not match a known command","user":{"login":"eugeniominissale","id":24990712,"node_id":"MDQ6VXNlcjI0OTkwNzEy","avatar_url":"https://avatars3.githubusercontent.com/u/24990712?v=4","gravatar_id":"","url":"https://api.github.com/users/eugeniominissale","html_url":"https://github.com/eugeniominissale","followers_url":"https://api.github.com/users/eugeniominissale/followers","following_url":"https://api.github.com/users/eugeniominissale/following{/other_user}","gists_url":"https://api.github.com/users/eugeniominissale/gists{/gist_id}","starred_url":"https://api.github.com/users/eugeniominissale/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/eugeniominissale/subscriptions","organizations_url":"https://api.github.com/users/eugeniominissale/orgs","repos_url":"https://api.github.com/users/eugeniominissale/repos","events_url":"https://api.github.com/users/eugeniominissale/events{/privacy}","received_events_url":"https://api.github.com/users/eugeniominissale/received_events","type":"User","site_admin":false},"labels":[{"id":182484652,"node_id":"MDU6TGFiZWwxODI0ODQ2NTI=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-grid","name":"C-grid","color":"fbca04","default":false},{"id":182503854,"node_id":"MDU6TGFiZWwxODI1MDM4NTQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-py","name":"C-py","color":"fbca04","default":false},{"id":182486420,"node_id":"MDU6TGFiZWwxODI0ODY0MjA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/R-awaiting%20answer","name":"R-awaiting answer","color":"d4c5f9","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":9,"created_at":"2017-10-24T22:33:49Z","updated_at":"2019-08-17T03:09:47Z","closed_at":"2017-11-08T15:24:17Z","author_association":"NONE","body":"OS: OSX 10.12.5\r\nSelenium Version: 3.3.1\r\nBrowser: Firefox 56\r\nselenium-server-standalone: 3.6.0\r\nPython 2.7.13\r\ngeckodriver 0.19.0\r\n\r\nHello everyone. I can't run firefox with selenium grid.\r\n\r\nPython webdriver setup code: \r\n``\r\ndriver = webdriver.Remote(\r\n    command_executor=\"http://localhost:4444/wd/hub\",\r\n    desired_capabilities={\r\n        \"browserName\": \"firefox\",\r\n        \"marionette\": \"true\",\r\n       \"javascriptEnabled\": True,\r\n})\r\n``\r\nThis is the traceback i get:\r\n\r\n```\r\nTraceback (most recent call last):\r\n  File \"main_fb_user.py\", line 504, in <module>\r\n    callback(message)\r\n  File \"main_fb_user.py\", line 334, in callback\r\n    wait_fb_login(browser)\r\n  File \"/Users/eugenio/hoopygang/crawler_stage_fb/py_fb_crawler/selenium_utils.py\", line 47, in wait_fb_login\r\n    browser.implicitly_wait(3)\r\n  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 694, in implicitly_wait\r\n    'ms': float(time_to_wait) * 1000})\r\n  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/webdriver.py\", line 238, in execute\r\n    self.error_handler.check_response(response)\r\n  File \"/usr/local/lib/python2.7/site-packages/selenium/webdriver/remote/errorhandler.py\", line 193, in check_response\r\n    raise exception_class(message, screen, stacktrace)\r\nselenium.common.exceptions.WebDriverException: Message: POST /session/bfe672e0-a5cd-834b-b47b-f01baa195699/timeouts/implicit_wait did not match a known command\r\nBuild info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T16:15:40.131Z'\r\nSystem info: host: 'MBP.station', ip: 'fe80:0:0:0:1068:d7a:cca3:e867%en0', os.name: 'Mac OS X', os.arch: 'x86_64', os.version: '10.12.5', java.version: '9'\r\n```\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["339190378","339252134","339416350","339461908","339473418","339473767","339480147","339512259","342851133"], "labels":["C-grid","C-py","R-awaiting answer"]}