{"id":"1118", "title":"Safari can wait indefinitely page load (driver.get command)", "body":"> Safari: 8.0.8 (10600.8.9)
> MacBook Air: OS X Yosemite 10.10.5
> Java: JDK 1.7.0_79-b15
> WebDriver: 2.47.1
> SafariDriver: 2.45.0

One page of our SUT is loading very long time (>1 hour) and SafariDriver doesn't throw an exception. To be honest it is loaded, but safari says that loading still is in progress.

``` java
SafariOptions safariOptions = new SafariOptions();
safariOptions.setUseCleanSession(true);

DesiredCapabilities capabilities = DesiredCapabilities.safari();
capabilities.setCapability(SafariOptions.CAPABILITY, safariOptions);

WebDriver webdriver = new SafariDriver(capabilities);
webdriver.get(\"http://sut\");
```

I've tried add:

``` java
webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
```

and

``` java
webdriver.manage().timeouts().setScriptTimeout(10, TimeUnit.MILLISECONDS);
```

but It didn't help.
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1118","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1118/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1118/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/1118/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/1118","id":110257858,"node_id":"MDU6SXNzdWUxMTAyNTc4NTg=","number":1118,"title":"Safari can wait indefinitely page load (driver.get command)","user":{"login":"Malahovskys","id":9838360,"node_id":"MDQ6VXNlcjk4MzgzNjA=","avatar_url":"https://avatars2.githubusercontent.com/u/9838360?v=4","gravatar_id":"","url":"https://api.github.com/users/Malahovskys","html_url":"https://github.com/Malahovskys","followers_url":"https://api.github.com/users/Malahovskys/followers","following_url":"https://api.github.com/users/Malahovskys/following{/other_user}","gists_url":"https://api.github.com/users/Malahovskys/gists{/gist_id}","starred_url":"https://api.github.com/users/Malahovskys/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Malahovskys/subscriptions","organizations_url":"https://api.github.com/users/Malahovskys/orgs","repos_url":"https://api.github.com/users/Malahovskys/repos","events_url":"https://api.github.com/users/Malahovskys/events{/privacy}","received_events_url":"https://api.github.com/users/Malahovskys/received_events","type":"User","site_admin":false},"labels":[{"id":182486741,"node_id":"MDU6TGFiZWwxODI0ODY3NDE=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/A-needs%20new%20owner","name":"A-needs new owner","color":"e10c02","default":false},{"id":189833238,"node_id":"MDU6TGFiZWwxODk4MzMyMzg=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-safari","name":"D-safari","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":11,"created_at":"2015-10-07T15:54:24Z","updated_at":"2019-08-19T15:09:40Z","closed_at":"2016-09-23T20:18:12Z","author_association":"NONE","body":"> Safari: 8.0.8 (10600.8.9)\n> MacBook Air: OS X Yosemite 10.10.5\n> Java: JDK 1.7.0_79-b15\n> WebDriver: 2.47.1\n> SafariDriver: 2.45.0\n\nOne page of our SUT is loading very long time (>1 hour) and SafariDriver doesn't throw an exception. To be honest it is loaded, but safari says that loading still is in progress.\n\n``` java\nSafariOptions safariOptions = new SafariOptions();\nsafariOptions.setUseCleanSession(true);\n\nDesiredCapabilities capabilities = DesiredCapabilities.safari();\ncapabilities.setCapability(SafariOptions.CAPABILITY, safariOptions);\n\nWebDriver webdriver = new SafariDriver(capabilities);\nwebdriver.get(\"http://sut\");\n```\n\nI've tried add:\n\n``` java\nwebdriver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);\n```\n\nand\n\n``` java\nwebdriver.manage().timeouts().setScriptTimeout(10, TimeUnit.MILLISECONDS);\n```\n\nbut It didn't help.\n","closed_by":{"login":"lukeis","id":926454,"node_id":"MDQ6VXNlcjkyNjQ1NA==","avatar_url":"https://avatars0.githubusercontent.com/u/926454?v=4","gravatar_id":"","url":"https://api.github.com/users/lukeis","html_url":"https://github.com/lukeis","followers_url":"https://api.github.com/users/lukeis/followers","following_url":"https://api.github.com/users/lukeis/following{/other_user}","gists_url":"https://api.github.com/users/lukeis/gists{/gist_id}","starred_url":"https://api.github.com/users/lukeis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukeis/subscriptions","organizations_url":"https://api.github.com/users/lukeis/orgs","repos_url":"https://api.github.com/users/lukeis/repos","events_url":"https://api.github.com/users/lukeis/events{/privacy}","received_events_url":"https://api.github.com/users/lukeis/received_events","type":"User","site_admin":false}}", "commentIds":["146329485","146427047","146694529","146871529","147267225","147828259","147980565","148097867","217842683","226772096","249292115"], "labels":["A-needs new owner","D-safari"]}