{"id":"4187", "title":"selenium-webdriver 3.4.1 or above breaks Selenium::WebDriver.for", "body":"## Meta -
OS:  
Windows
Selenium Version:  
3.4.2
Browser:  
Chrome

Browser Version:  
<!-- e.g.: 49.0.2623.87 (64-bit) -->

## Expected Behavior -
Being able to launch another browser with default capabilities as supported by version 3.4.0, with the [syntax described on the wiki](https://github.com/SeleniumHQ/selenium/wiki/Ruby-Bindings)

driver = Selenium::WebDriver.for :remote, desired_capabilities: :firefox

or

driver = Selenium::WebDriver.for :remote, desired_capabilities: :chrome

## Actual Behavior -

NoMethodError:
       undefined method `capabilities' for :firefox:Symbol

or

NoMethodError:
       undefined method `capabilities' for :chrome:Symbol

This syntax used to work with version 3.4.0, but it does not work with 3.4.1 or 3.4.2
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4187","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4187/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4187/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/4187/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/4187","id":236161561,"node_id":"MDU6SXNzdWUyMzYxNjE1NjE=","number":4187,"title":"selenium-webdriver 3.4.1 or above breaks Selenium::WebDriver.for","user":{"login":"kev-in-shu","id":7252851,"node_id":"MDQ6VXNlcjcyNTI4NTE=","avatar_url":"https://avatars3.githubusercontent.com/u/7252851?v=4","gravatar_id":"","url":"https://api.github.com/users/kev-in-shu","html_url":"https://github.com/kev-in-shu","followers_url":"https://api.github.com/users/kev-in-shu/followers","following_url":"https://api.github.com/users/kev-in-shu/following{/other_user}","gists_url":"https://api.github.com/users/kev-in-shu/gists{/gist_id}","starred_url":"https://api.github.com/users/kev-in-shu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kev-in-shu/subscriptions","organizations_url":"https://api.github.com/users/kev-in-shu/orgs","repos_url":"https://api.github.com/users/kev-in-shu/repos","events_url":"https://api.github.com/users/kev-in-shu/events{/privacy}","received_events_url":"https://api.github.com/users/kev-in-shu/received_events","type":"User","site_admin":false},"labels":[{"id":182503883,"node_id":"MDU6TGFiZWwxODI1MDM4ODM=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/C-rb","name":"C-rb","color":"fbca04","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":9,"created_at":"2017-06-15T11:41:13Z","updated_at":"2019-08-17T22:09:57Z","closed_at":"2017-06-20T08:58:50Z","author_association":"NONE","body":"## Meta -\r\nOS:  \r\nWindows\r\nSelenium Version:  \r\n3.4.2\r\nBrowser:  \r\nChrome\r\n\r\nBrowser Version:  \r\n<!-- e.g.: 49.0.2623.87 (64-bit) -->\r\n\r\n## Expected Behavior -\r\nBeing able to launch another browser with default capabilities as supported by version 3.4.0, with the [syntax described on the wiki](https://github.com/SeleniumHQ/selenium/wiki/Ruby-Bindings)\r\n\r\ndriver = Selenium::WebDriver.for :remote, desired_capabilities: :firefox\r\n\r\nor\r\n\r\ndriver = Selenium::WebDriver.for :remote, desired_capabilities: :chrome\r\n\r\n## Actual Behavior -\r\n\r\nNoMethodError:\r\n       undefined method `capabilities' for :firefox:Symbol\r\n\r\nor\r\n\r\nNoMethodError:\r\n       undefined method `capabilities' for :chrome:Symbol\r\n\r\nThis syntax used to work with version 3.4.0, but it does not work with 3.4.1 or 3.4.2\r\n","closed_by":{"login":"p0deje","id":665846,"node_id":"MDQ6VXNlcjY2NTg0Ng==","avatar_url":"https://avatars3.githubusercontent.com/u/665846?v=4","gravatar_id":"","url":"https://api.github.com/users/p0deje","html_url":"https://github.com/p0deje","followers_url":"https://api.github.com/users/p0deje/followers","following_url":"https://api.github.com/users/p0deje/following{/other_user}","gists_url":"https://api.github.com/users/p0deje/gists{/gist_id}","starred_url":"https://api.github.com/users/p0deje/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/p0deje/subscriptions","organizations_url":"https://api.github.com/users/p0deje/orgs","repos_url":"https://api.github.com/users/p0deje/repos","events_url":"https://api.github.com/users/p0deje/events{/privacy}","received_events_url":"https://api.github.com/users/p0deje/received_events","type":"User","site_admin":false}}", "commentIds":["308933412","308943177","309373111","309690216","310425353","322016900","322023370","322107217","322111390"], "labels":["C-rb"]}