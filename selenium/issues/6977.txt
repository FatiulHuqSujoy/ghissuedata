{"id":"6977", "title":"Unable to interact with element that is present and enabled", "body":"## 🐛 Bug Report

Selenium is unable to interact with an element that is visible on the screen and enabled:
The element is an input text field. When retrieved, the element shows positive values on the rectangle:
![image](https://user-images.githubusercontent.com/531256/53518858-8e193480-3a97-11e9-9200-18a520eb15f1.png)
Querying for `is_enabled()` returns `True`
But querying for `is_displayed()` returns `False`
Trying to send any action to the element throws an exception: `ElementNotVisible`
It has a unique identifier and there are no other elements that match.
I noticed that this started happening when 2 specific CSS properties were added to the container of that element:
`overflow-y: auto;`
`display: contents;`
This was added in order to display the element properly on Edge, but it broke the interaction on Chrome.


## To Reproduce
Here is a short HTML with an example to reproduce the issue:
https://jsfiddle.net/k594wjgu/2/

```
<html>
  <body>
  <form style=\"display: flex;
  flex-direction: column;
  display: contents;
  overflow-y: auto;\">
    <input type=\"text\" name=\"first_name\"/>
  </form>
  </body>
</html>
```

Here is the code to reproduce (Uses Python selenium bindings)
```
from selenium import webdriver
driver = webdriver.Chrome()
driver.get(\"https://jsfiddle.net/k594wjgu/2/\")
driver.switch_to.frame(self.driver.find_element_by_name('result'))
driver.find_element_by_name('first_name').is_enabled()
# ^ Returns True
driver.find_element_by_name('first_name').is_displayed()
# ^ Returns False, however if either display: contents; or overflow-y: auto is removed from the form style #then the statement returns True and I can send click or send key actions
driver.find_element_by_name('first_name').send_keys('Hello')
# ^ Throws ElementNotVisibleException
```

## Expected behavior

I would expect that I can send actions to any element that is displayed on the viewport and that is enabled

## Environment

OS: MacOS Mojave 10.14.1
Browser: Chrome
Browser version: 72
Browser Driver version: 2.46
Language Bindings version: Python selenium 3.141.0
Selenium Grid version (if applicable): NA", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6977","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6977/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6977/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/6977/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/6977","id":415372903,"node_id":"MDU6SXNzdWU0MTUzNzI5MDM=","number":6977,"title":"Unable to interact with element that is present and enabled","user":{"login":"eduardoreynoso","id":531256,"node_id":"MDQ6VXNlcjUzMTI1Ng==","avatar_url":"https://avatars2.githubusercontent.com/u/531256?v=4","gravatar_id":"","url":"https://api.github.com/users/eduardoreynoso","html_url":"https://github.com/eduardoreynoso","followers_url":"https://api.github.com/users/eduardoreynoso/followers","following_url":"https://api.github.com/users/eduardoreynoso/following{/other_user}","gists_url":"https://api.github.com/users/eduardoreynoso/gists{/gist_id}","starred_url":"https://api.github.com/users/eduardoreynoso/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/eduardoreynoso/subscriptions","organizations_url":"https://api.github.com/users/eduardoreynoso/orgs","repos_url":"https://api.github.com/users/eduardoreynoso/repos","events_url":"https://api.github.com/users/eduardoreynoso/events{/privacy}","received_events_url":"https://api.github.com/users/eduardoreynoso/received_events","type":"User","site_admin":false},"labels":[{"id":182514864,"node_id":"MDU6TGFiZWwxODI1MTQ4NjQ=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-atoms","name":"D-atoms","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2019-02-27T23:20:31Z","updated_at":"2019-08-14T17:09:41Z","closed_at":"2019-03-29T21:26:48Z","author_association":"NONE","body":"## 🐛 Bug Report\r\n\r\nSelenium is unable to interact with an element that is visible on the screen and enabled:\r\nThe element is an input text field. When retrieved, the element shows positive values on the rectangle:\r\n![image](https://user-images.githubusercontent.com/531256/53518858-8e193480-3a97-11e9-9200-18a520eb15f1.png)\r\nQuerying for `is_enabled()` returns `True`\r\nBut querying for `is_displayed()` returns `False`\r\nTrying to send any action to the element throws an exception: `ElementNotVisible`\r\nIt has a unique identifier and there are no other elements that match.\r\nI noticed that this started happening when 2 specific CSS properties were added to the container of that element:\r\n`overflow-y: auto;`\r\n`display: contents;`\r\nThis was added in order to display the element properly on Edge, but it broke the interaction on Chrome.\r\n\r\n\r\n## To Reproduce\r\nHere is a short HTML with an example to reproduce the issue:\r\nhttps://jsfiddle.net/k594wjgu/2/\r\n\r\n```\r\n<html>\r\n  <body>\r\n  <form style=\"display: flex;\r\n  flex-direction: column;\r\n  display: contents;\r\n  overflow-y: auto;\">\r\n    <input type=\"text\" name=\"first_name\"/>\r\n  </form>\r\n  </body>\r\n</html>\r\n```\r\n\r\nHere is the code to reproduce (Uses Python selenium bindings)\r\n```\r\nfrom selenium import webdriver\r\ndriver = webdriver.Chrome()\r\ndriver.get(\"https://jsfiddle.net/k594wjgu/2/\")\r\ndriver.switch_to.frame(self.driver.find_element_by_name('result'))\r\ndriver.find_element_by_name('first_name').is_enabled()\r\n# ^ Returns True\r\ndriver.find_element_by_name('first_name').is_displayed()\r\n# ^ Returns False, however if either display: contents; or overflow-y: auto is removed from the form style #then the statement returns True and I can send click or send key actions\r\ndriver.find_element_by_name('first_name').send_keys('Hello')\r\n# ^ Throws ElementNotVisibleException\r\n```\r\n\r\n## Expected behavior\r\n\r\nI would expect that I can send actions to any element that is displayed on the viewport and that is enabled\r\n\r\n## Environment\r\n\r\nOS: MacOS Mojave 10.14.1\r\nBrowser: Chrome\r\nBrowser version: 72\r\nBrowser Driver version: 2.46\r\nLanguage Bindings version: Python selenium 3.141.0\r\nSelenium Grid version (if applicable): NA","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["468521796","468756296","468875204","471052715","471061447","471107365","473515218","478154960"], "labels":["D-atoms"]}