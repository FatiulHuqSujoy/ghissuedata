{"id":"862", "title":"synthetic events do not accurately simulate the events normally generated when a user clicks in an element that is removed from the DOM", "body":"### Versions

Selenium version: 2.45.0, 2.46.0, 2.46.1
Browser: FF, IE
OS: Debian Testing up to date as of 2015-07-31
### What steps will reproduce the problem?

To get the whole script to run you should run it on 2.45.0. See notes below regarding 2.46.x.
1. This code reproduces the problem 100% of the time here. Please read the comments at the start to learn how it can be run.
   
   ```
   import sys
   import collections
   
   from selenium import webdriver
   from selenium.webdriver.firefox.webdriver import FirefoxBinary
   from selenium.webdriver.common.action_chains import ActionChains
   
   # We have to use a Firefox binary for which native events are supported.
   binary = FirefoxBinary(\"/home/ldd/src/firefox-31/firefox\")
   
   # Ways to run this script:
   #
   # - To launch a local browser:
   #
   # ./issue.py local [ firefox | ff | chrome | ch ]
   #
   # - To launch a remote browser on Sauce labs:
   #
   # ./issue.py remote user:key 'os,browser,version'
   #
   # where:
   #
   # * `user` is your user id on Sauce Labs
   #
   # * `key` is the key for Sauce Labs
   #
   # * `os` is the OS you want Sauce Labs to run the browser on
   #
   # * `browser` is the browser you want. This has to equal to one of the
   #   fields of webdriver.DesiredCapabilities. (`INTERNETEXPLORER` and
   #   `internetexplorer` are fine but `ie` won't work.)
   #
   # * `version` is the version number you want.
   #
   # e.g. python ./issue.py remote foo:bar 'Windows 8.1,firefox,31'
   #
   # would launch Firefox 31 on Windows 8.1 with user id `foo` and API key `bar`.
   #
   
   def makedriver(caps):
       if sys.argv[1] == \"remote\":
           Config = collections.namedtuple(
               'Config', ('platform', 'browser', 'version'))
   
           cmd_line_config = sys.argv[3]
           split_config = cmd_line_config.split(\",\")
           split_config[1] = split_config[1].upper()
           config = Config(*split_config)
   
           # config = Config(\"Windows 8.1\", \"INTERNETEXPLORER\", \"11\")
           # config = Config(\"Windows 8.1\", \"CHROME\", \"41\")
   
           desired_capabilities = dict(getattr(webdriver.DesiredCapabilities,
                                               config.browser))
           desired_capabilities.update(caps)
   
           desired_capabilities[\"platform\"] = config.platform
           desired_capabilities[\"version\"] = config.version
           desired_capabilities[\"selenium-version\"] = \"2.46.0\"
           desired_capabilities[\"chromedriver-version\"] = \"2.16\"
           desired_capabilities[\"avoid-proxy\"] = \"true\"
           desired_capabilities[\"name\"] = \"ad-hoc test\"
   
           driver = webdriver.Remote(
               desired_capabilities=desired_capabilities,
               command_executor=\"http://\" + sys.argv[2] +
               \"@ondemand.saucelabs.com:80/wd/hub\")
       elif sys.argv[1] == \"local\":
           browser = sys.argv[2].lower()
           if browser in (\"firefox\", \"ff\"):
               driver = webdriver.Firefox(
                   capabilities=caps, firefox_binary=binary)
           elif browser in (\"chrome\", \"ch\"):
               driver = webdriver.Chrome(desired_capabilities=caps)
           else:
               raise ValueError(\"cannot process argument: \" + sys.argv[2])
       else:
           raise ValueError(\"cannot process argument: \" + sys.argv[1])
   
       print \"browser: \", driver.desired_capabilities[\"platform\"], driver.name, \\
           driver.desired_capabilities[\"version\"]
       print \"native events:\", driver.desired_capabilities[\"nativeEvents\"]
       return driver
   
   
   def perform(native):
       caps = {
           \"nativeEvents\": native
       }
   
       driver = makedriver(caps)
   
       driver.get(\"http://output.jsbin.com/reruke\")
   
       over = driver.find_element_by_id(\"over\")
       ActionChains(driver) \\
           .click(over) \\
           .perform()
   
       print driver.execute_script(\"\"\"
       return document.getElementById(\"log\").textContent;
       \"\"\")
   
       driver.quit()
   
   
   for native in (False, True):
       perform(native)
   ```
2. Save the script as `issue.py`. Edit the script so that the path given for the Firefox binary for which native events can be produced is set to a path that works on your system, and run:
   
   $ python ./issue.py local firefox
### What is the expected output?

```
browser:  Linux firefox 31.3.0
native events: False

    Log:
  mousedown in over (handled by over)
mousedown in over (handled by body)
mouseup in under (handled by under)
mouseup in under (handled by body)

browser:  Linux firefox 31.3.0
native events: True

    Log:
  mousedown in over (handled by over)
mousedown in over (handled by body)
mouseup in under (handled by under)
mouseup in under (handled by body)
```

Synthetic events should produce the same browser behavior as native events.
### What do you see instead?

```
browser:  Linux firefox 31.3.0
native events: False

    Log:
  mousedown in over (handled by over)
mousedown in over (handled by body)

browser:  Linux firefox 31.3.0
native events: True

    Log:
  mousedown in over (handled by over)
mousedown in over (handled by body)
mouseup in under (handled by under)
mouseup in under (handled by body)
```
### Observations

It is not possible to use 2.46.1 when running the test remotely as Sauce Labs does not have it installed yet. The latest version they have is 2.46.0.

It is not possible to run the whole script with Firefox with Selenium 2.46.0. The script is designed to show how the behavior of native events and synthetic events differ, with native events providing exactly what a developer would expect. Because 2.46.0 does not support native events on FF 31.3.0 (or 31.8.0, for that matter, or any other FF I tried), the attempt at running with native events fails. However, it is possible to run the script in 2.46.1 and see the incorrect behavior provided by synthetic events, which is the same as in 2.45.0.

The same problem occurs in IE 10 and IE 11: synthetic events fail to accurately simulate actual user interaction with the browser. The one difference is that in IE 10 or 11 and additional \"click in body\" event is produced. However, this extra event is also produced when I perform the operations manually on the jsbin page. Therefore, I conclude that this is not an inconsistency in Selenium but a discrepancy between how the browsers handle mouse events. **And this, by the way, is exactly why native events are so useful: they highlight such discrepancies among browsers.** To run it with IE, I do:

```
$ python ./issue.py remote user:key 'Windows 8,internetexplorer,10'
```

The expected results are exactly those that are produced if I open the jsbin in Firefox and do manually the operations performed by the script. Native events reproduce accurately what happens when I manually operate the browser, synthetic events do not.

Running the script with Chrome 44 (and chromedriver 2.16) produces the expected behavior, except for the fact that we get `native events: True` in both runs given that it is not possible to turn off native events in Chrome. There's one difference when I perform the run in Chrome: an additional \"click in body\" event is produced. However, my conclusions about this additional event are the same as for IE (see above).

I'm reporting the bug as requested [here](https://groups.google.com/d/msg/selenium-developers/DKnG2lA-KxM/_2xZsazCgs4J).
", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/862","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/862/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/862/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/862/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/862","id":98459762,"node_id":"MDU6SXNzdWU5ODQ1OTc2Mg==","number":862,"title":"synthetic events do not accurately simulate the events normally generated when a user clicks in an element that is removed from the DOM","user":{"login":"lddubeau","id":1963493,"node_id":"MDQ6VXNlcjE5NjM0OTM=","avatar_url":"https://avatars3.githubusercontent.com/u/1963493?v=4","gravatar_id":"","url":"https://api.github.com/users/lddubeau","html_url":"https://github.com/lddubeau","followers_url":"https://api.github.com/users/lddubeau/followers","following_url":"https://api.github.com/users/lddubeau/following{/other_user}","gists_url":"https://api.github.com/users/lddubeau/gists{/gist_id}","starred_url":"https://api.github.com/users/lddubeau/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lddubeau/subscriptions","organizations_url":"https://api.github.com/users/lddubeau/orgs","repos_url":"https://api.github.com/users/lddubeau/repos","events_url":"https://api.github.com/users/lddubeau/events{/privacy}","received_events_url":"https://api.github.com/users/lddubeau/received_events","type":"User","site_admin":false},"labels":[{"id":188189250,"node_id":"MDU6TGFiZWwxODgxODkyNTA=","url":"https://api.github.com/repos/SeleniumHQ/selenium/labels/D-firefox","name":"D-firefox","color":"0052cc","default":false}],"state":"closed","locked":true,"assignee":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false},"assignees":[{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2015-07-31T19:50:41Z","updated_at":"2019-08-21T03:09:39Z","closed_at":"2015-09-19T13:18:45Z","author_association":"NONE","body":"### Versions\n\nSelenium version: 2.45.0, 2.46.0, 2.46.1\nBrowser: FF, IE\nOS: Debian Testing up to date as of 2015-07-31\n### What steps will reproduce the problem?\n\nTo get the whole script to run you should run it on 2.45.0. See notes below regarding 2.46.x.\n1. This code reproduces the problem 100% of the time here. Please read the comments at the start to learn how it can be run.\n   \n   ```\n   import sys\n   import collections\n   \n   from selenium import webdriver\n   from selenium.webdriver.firefox.webdriver import FirefoxBinary\n   from selenium.webdriver.common.action_chains import ActionChains\n   \n   # We have to use a Firefox binary for which native events are supported.\n   binary = FirefoxBinary(\"/home/ldd/src/firefox-31/firefox\")\n   \n   # Ways to run this script:\n   #\n   # - To launch a local browser:\n   #\n   # ./issue.py local [ firefox | ff | chrome | ch ]\n   #\n   # - To launch a remote browser on Sauce labs:\n   #\n   # ./issue.py remote user:key 'os,browser,version'\n   #\n   # where:\n   #\n   # * `user` is your user id on Sauce Labs\n   #\n   # * `key` is the key for Sauce Labs\n   #\n   # * `os` is the OS you want Sauce Labs to run the browser on\n   #\n   # * `browser` is the browser you want. This has to equal to one of the\n   #   fields of webdriver.DesiredCapabilities. (`INTERNETEXPLORER` and\n   #   `internetexplorer` are fine but `ie` won't work.)\n   #\n   # * `version` is the version number you want.\n   #\n   # e.g. python ./issue.py remote foo:bar 'Windows 8.1,firefox,31'\n   #\n   # would launch Firefox 31 on Windows 8.1 with user id `foo` and API key `bar`.\n   #\n   \n   def makedriver(caps):\n       if sys.argv[1] == \"remote\":\n           Config = collections.namedtuple(\n               'Config', ('platform', 'browser', 'version'))\n   \n           cmd_line_config = sys.argv[3]\n           split_config = cmd_line_config.split(\",\")\n           split_config[1] = split_config[1].upper()\n           config = Config(*split_config)\n   \n           # config = Config(\"Windows 8.1\", \"INTERNETEXPLORER\", \"11\")\n           # config = Config(\"Windows 8.1\", \"CHROME\", \"41\")\n   \n           desired_capabilities = dict(getattr(webdriver.DesiredCapabilities,\n                                               config.browser))\n           desired_capabilities.update(caps)\n   \n           desired_capabilities[\"platform\"] = config.platform\n           desired_capabilities[\"version\"] = config.version\n           desired_capabilities[\"selenium-version\"] = \"2.46.0\"\n           desired_capabilities[\"chromedriver-version\"] = \"2.16\"\n           desired_capabilities[\"avoid-proxy\"] = \"true\"\n           desired_capabilities[\"name\"] = \"ad-hoc test\"\n   \n           driver = webdriver.Remote(\n               desired_capabilities=desired_capabilities,\n               command_executor=\"http://\" + sys.argv[2] +\n               \"@ondemand.saucelabs.com:80/wd/hub\")\n       elif sys.argv[1] == \"local\":\n           browser = sys.argv[2].lower()\n           if browser in (\"firefox\", \"ff\"):\n               driver = webdriver.Firefox(\n                   capabilities=caps, firefox_binary=binary)\n           elif browser in (\"chrome\", \"ch\"):\n               driver = webdriver.Chrome(desired_capabilities=caps)\n           else:\n               raise ValueError(\"cannot process argument: \" + sys.argv[2])\n       else:\n           raise ValueError(\"cannot process argument: \" + sys.argv[1])\n   \n       print \"browser: \", driver.desired_capabilities[\"platform\"], driver.name, \\\n           driver.desired_capabilities[\"version\"]\n       print \"native events:\", driver.desired_capabilities[\"nativeEvents\"]\n       return driver\n   \n   \n   def perform(native):\n       caps = {\n           \"nativeEvents\": native\n       }\n   \n       driver = makedriver(caps)\n   \n       driver.get(\"http://output.jsbin.com/reruke\")\n   \n       over = driver.find_element_by_id(\"over\")\n       ActionChains(driver) \\\n           .click(over) \\\n           .perform()\n   \n       print driver.execute_script(\"\"\"\n       return document.getElementById(\"log\").textContent;\n       \"\"\")\n   \n       driver.quit()\n   \n   \n   for native in (False, True):\n       perform(native)\n   ```\n2. Save the script as `issue.py`. Edit the script so that the path given for the Firefox binary for which native events can be produced is set to a path that works on your system, and run:\n   \n   $ python ./issue.py local firefox\n### What is the expected output?\n\n```\nbrowser:  Linux firefox 31.3.0\nnative events: False\n\n    Log:\n  mousedown in over (handled by over)\nmousedown in over (handled by body)\nmouseup in under (handled by under)\nmouseup in under (handled by body)\n\nbrowser:  Linux firefox 31.3.0\nnative events: True\n\n    Log:\n  mousedown in over (handled by over)\nmousedown in over (handled by body)\nmouseup in under (handled by under)\nmouseup in under (handled by body)\n```\n\nSynthetic events should produce the same browser behavior as native events.\n### What do you see instead?\n\n```\nbrowser:  Linux firefox 31.3.0\nnative events: False\n\n    Log:\n  mousedown in over (handled by over)\nmousedown in over (handled by body)\n\nbrowser:  Linux firefox 31.3.0\nnative events: True\n\n    Log:\n  mousedown in over (handled by over)\nmousedown in over (handled by body)\nmouseup in under (handled by under)\nmouseup in under (handled by body)\n```\n### Observations\n\nIt is not possible to use 2.46.1 when running the test remotely as Sauce Labs does not have it installed yet. The latest version they have is 2.46.0.\n\nIt is not possible to run the whole script with Firefox with Selenium 2.46.0. The script is designed to show how the behavior of native events and synthetic events differ, with native events providing exactly what a developer would expect. Because 2.46.0 does not support native events on FF 31.3.0 (or 31.8.0, for that matter, or any other FF I tried), the attempt at running with native events fails. However, it is possible to run the script in 2.46.1 and see the incorrect behavior provided by synthetic events, which is the same as in 2.45.0.\n\nThe same problem occurs in IE 10 and IE 11: synthetic events fail to accurately simulate actual user interaction with the browser. The one difference is that in IE 10 or 11 and additional \"click in body\" event is produced. However, this extra event is also produced when I perform the operations manually on the jsbin page. Therefore, I conclude that this is not an inconsistency in Selenium but a discrepancy between how the browsers handle mouse events. **And this, by the way, is exactly why native events are so useful: they highlight such discrepancies among browsers.** To run it with IE, I do:\n\n```\n$ python ./issue.py remote user:key 'Windows 8,internetexplorer,10'\n```\n\nThe expected results are exactly those that are produced if I open the jsbin in Firefox and do manually the operations performed by the script. Native events reproduce accurately what happens when I manually operate the browser, synthetic events do not.\n\nRunning the script with Chrome 44 (and chromedriver 2.16) produces the expected behavior, except for the fact that we get `native events: True` in both runs given that it is not possible to turn off native events in Chrome. There's one difference when I perform the run in Chrome: an additional \"click in body\" event is produced. However, my conclusions about this additional event are the same as for IE (see above).\n\nI'm reporting the bug as requested [here](https://groups.google.com/d/msg/selenium-developers/DKnG2lA-KxM/_2xZsazCgs4J).\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["141667673"], "labels":["D-firefox"]}