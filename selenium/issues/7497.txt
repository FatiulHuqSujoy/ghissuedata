{"id":"7497", "title":"Cannot open any URL (chromedriver & C#)", "body":"## 💥 Regression Report

Using latest webdriver library and chromedriver 76 , i wasn't able to open any URL in Chrome in C# , the command prompt and chrome window are shown but nothing happens.
Check my code below.

```
IWebDriver drv = new ChromeDriver();
drv.Url = \"http://google.com\";
```
I also tried to disable W3C mode but this time only the command prompt window is shown without Chrome window.

```
ChromeOptions op = new ChromeOptions();
op.UseSpecCompliantProtocol = false;
IWebDriver drv = new ChromeDriver(op);
drv.Url = \"http://google.com\";
```


<!-- NOTE
FIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.

If the issue is with Google Chrome consider logging an issue with chromedriver instead:
https://sites.google.com/a/chromium.org/chromedriver/help

If the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:
https://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette

If the issue is with Microsoft Edge consider logging an issue with Microsoft instead:
https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/

If the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:
https://bugreport.apple.com/

-->

## Last working Selenium version

Worked up to version: Selenium 3.141.59

Stopped working in version: selenium-4.0.0-alpha-2


## Environment

OS: Windows 10
Browser: Chrome
Browser version: 76.0.3809.100
Browser Driver version: ChromeDriver 76.0.3809.68

", "json":"{"url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7497","repository_url":"https://api.github.com/repos/SeleniumHQ/selenium","labels_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7497/labels{/name}","comments_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7497/comments","events_url":"https://api.github.com/repos/SeleniumHQ/selenium/issues/7497/events","html_url":"https://github.com/SeleniumHQ/selenium/issues/7497","id":481185195,"node_id":"MDU6SXNzdWU0ODExODUxOTU=","number":7497,"title":"Cannot open any URL (chromedriver & C#)","user":{"login":"mahmoud-hassan-911","id":17790069,"node_id":"MDQ6VXNlcjE3NzkwMDY5","avatar_url":"https://avatars2.githubusercontent.com/u/17790069?v=4","gravatar_id":"","url":"https://api.github.com/users/mahmoud-hassan-911","html_url":"https://github.com/mahmoud-hassan-911","followers_url":"https://api.github.com/users/mahmoud-hassan-911/followers","following_url":"https://api.github.com/users/mahmoud-hassan-911/following{/other_user}","gists_url":"https://api.github.com/users/mahmoud-hassan-911/gists{/gist_id}","starred_url":"https://api.github.com/users/mahmoud-hassan-911/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mahmoud-hassan-911/subscriptions","organizations_url":"https://api.github.com/users/mahmoud-hassan-911/orgs","repos_url":"https://api.github.com/users/mahmoud-hassan-911/repos","events_url":"https://api.github.com/users/mahmoud-hassan-911/events{/privacy}","received_events_url":"https://api.github.com/users/mahmoud-hassan-911/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":true,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-08-15T14:44:22Z","updated_at":"2019-10-01T19:09:36Z","closed_at":"2019-09-01T18:56:39Z","author_association":"NONE","body":"## 💥 Regression Report\r\n\r\nUsing latest webdriver library and chromedriver 76 , i wasn't able to open any URL in Chrome in C# , the command prompt and chrome window are shown but nothing happens.\r\nCheck my code below.\r\n\r\n```\r\nIWebDriver drv = new ChromeDriver();\r\ndrv.Url = \"http://google.com\";\r\n```\r\nI also tried to disable W3C mode but this time only the command prompt window is shown without Chrome window.\r\n\r\n```\r\nChromeOptions op = new ChromeOptions();\r\nop.UseSpecCompliantProtocol = false;\r\nIWebDriver drv = new ChromeDriver(op);\r\ndrv.Url = \"http://google.com\";\r\n```\r\n\r\n\r\n<!-- NOTE\r\nFIREFOX 48+ IS ONLY COMPATIBLE WITH GECKODRIVER.\r\n\r\nIf the issue is with Google Chrome consider logging an issue with chromedriver instead:\r\nhttps://sites.google.com/a/chromium.org/chromedriver/help\r\n\r\nIf the issue is with Firefox GeckoDriver (aka Marionette) consider logging an issue with Mozilla:\r\nhttps://bugzilla.mozilla.org/buglist.cgi?product=Testing&component=Marionette\r\n\r\nIf the issue is with Microsoft Edge consider logging an issue with Microsoft instead:\r\nhttps://developer.microsoft.com/en-us/microsoft-edge/platform/issues/\r\n\r\nIf the issue is with Safari, only Safari 10+ is supported. Please log any Safari issue with Apple:\r\nhttps://bugreport.apple.com/\r\n\r\n-->\r\n\r\n## Last working Selenium version\r\n\r\nWorked up to version: Selenium 3.141.59\r\n\r\nStopped working in version: selenium-4.0.0-alpha-2\r\n\r\n\r\n## Environment\r\n\r\nOS: Windows 10\r\nBrowser: Chrome\r\nBrowser version: 76.0.3809.100\r\nBrowser Driver version: ChromeDriver 76.0.3809.68\r\n\r\n","closed_by":{"login":"barancev","id":617090,"node_id":"MDQ6VXNlcjYxNzA5MA==","avatar_url":"https://avatars2.githubusercontent.com/u/617090?v=4","gravatar_id":"","url":"https://api.github.com/users/barancev","html_url":"https://github.com/barancev","followers_url":"https://api.github.com/users/barancev/followers","following_url":"https://api.github.com/users/barancev/following{/other_user}","gists_url":"https://api.github.com/users/barancev/gists{/gist_id}","starred_url":"https://api.github.com/users/barancev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/barancev/subscriptions","organizations_url":"https://api.github.com/users/barancev/orgs","repos_url":"https://api.github.com/users/barancev/repos","events_url":"https://api.github.com/users/barancev/events{/privacy}","received_events_url":"https://api.github.com/users/barancev/received_events","type":"User","site_admin":false}}", "commentIds":["526944331","537185056"], "labels":[]}