{"id":"190875417","body":"@barancev can you reassess this issue with the latest examples (specifically the one without using `getScreenshotAs`)? This is problematic for tools built around WebDriver. Thanks.\n","date":"Mar 2, 2016 1:53:26 AM","json":"{\"url\":\"https://api.github.com/repos/SeleniumHQ/selenium/issues/comments/190875417\",\"html_url\":\"https://github.com/SeleniumHQ/selenium/issues/950#issuecomment-190875417\",\"issue_url\":\"https://api.github.com/repos/SeleniumHQ/selenium/issues/950\",\"id\":190875417,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDE5MDg3NTQxNw\u003d\u003d\",\"user\":{\"login\":\"mfulton26\",\"id\":5588957,\"node_id\":\"MDQ6VXNlcjU1ODg5NTc\u003d\",\"avatar_url\":\"https://avatars0.githubusercontent.com/u/5588957?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/mfulton26\",\"html_url\":\"https://github.com/mfulton26\",\"followers_url\":\"https://api.github.com/users/mfulton26/followers\",\"following_url\":\"https://api.github.com/users/mfulton26/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/mfulton26/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/mfulton26/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/mfulton26/subscriptions\",\"organizations_url\":\"https://api.github.com/users/mfulton26/orgs\",\"repos_url\":\"https://api.github.com/users/mfulton26/repos\",\"events_url\":\"https://api.github.com/users/mfulton26/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/mfulton26/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2016-03-01T19:53:26Z\",\"updated_at\":\"2016-03-01T19:53:26Z\",\"author_association\":\"NONE\",\"body\":\"@barancev can you reassess this issue with the latest examples (specifically the one without using `getScreenshotAs`)? This is problematic for tools built around WebDriver. Thanks.\\n\"}"}