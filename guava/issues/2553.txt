{"id":"2553", "title":"Iterables.toString() throwing NPE", "body":"I think this code:

```
 public static String toString(Iterable<?> iterable) {
    return Iterators.toString(iterable.iterator());
  }
```

Should perform a Null check, like:

```
 public static String toString(Iterable<?> iterable) {
if (iterable == null) {
    return \"null\";
}
    return Iterators.toString(iterable.iterator());
  }
```

Currently I have a very complex object that I can't toString() because some field in its fifth hierarchy uses this toString() and I get a NPE.
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2553","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2553/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2553/comments","events_url":"https://api.github.com/repos/google/guava/issues/2553/events","html_url":"https://github.com/google/guava/issues/2553","id":172464749,"node_id":"MDU6SXNzdWUxNzI0NjQ3NDk=","number":2553,"title":"Iterables.toString() throwing NPE","user":{"login":"guyman","id":6471624,"node_id":"MDQ6VXNlcjY0NzE2MjQ=","avatar_url":"https://avatars0.githubusercontent.com/u/6471624?v=4","gravatar_id":"","url":"https://api.github.com/users/guyman","html_url":"https://github.com/guyman","followers_url":"https://api.github.com/users/guyman/followers","following_url":"https://api.github.com/users/guyman/following{/other_user}","gists_url":"https://api.github.com/users/guyman/gists{/gist_id}","starred_url":"https://api.github.com/users/guyman/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/guyman/subscriptions","organizations_url":"https://api.github.com/users/guyman/orgs","repos_url":"https://api.github.com/users/guyman/repos","events_url":"https://api.github.com/users/guyman/events{/privacy}","received_events_url":"https://api.github.com/users/guyman/received_events","type":"User","site_admin":false},"labels":[{"id":143505259,"node_id":"MDU6TGFiZWwxNDM1MDUyNTk=","url":"https://api.github.com/repos/google/guava/labels/package=collect","name":"package=collect","color":"62a0e5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2016-08-22T14:19:35Z","updated_at":"2016-08-23T17:51:11Z","closed_at":"2016-08-23T06:49:26Z","author_association":"NONE","body":"I think this code:\n\n```\n public static String toString(Iterable<?> iterable) {\n    return Iterators.toString(iterable.iterator());\n  }\n```\n\nShould perform a Null check, like:\n\n```\n public static String toString(Iterable<?> iterable) {\nif (iterable == null) {\n    return \"null\";\n}\n    return Iterators.toString(iterable.iterator());\n  }\n```\n\nCurrently I have a very complex object that I can't toString() because some field in its fifth hierarchy uses this toString() and I get a NPE.\n","closed_by":{"login":"guyman","id":6471624,"node_id":"MDQ6VXNlcjY0NzE2MjQ=","avatar_url":"https://avatars0.githubusercontent.com/u/6471624?v=4","gravatar_id":"","url":"https://api.github.com/users/guyman","html_url":"https://github.com/guyman","followers_url":"https://api.github.com/users/guyman/followers","following_url":"https://api.github.com/users/guyman/following{/other_user}","gists_url":"https://api.github.com/users/guyman/gists{/gist_id}","starred_url":"https://api.github.com/users/guyman/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/guyman/subscriptions","organizations_url":"https://api.github.com/users/guyman/orgs","repos_url":"https://api.github.com/users/guyman/repos","events_url":"https://api.github.com/users/guyman/events{/privacy}","received_events_url":"https://api.github.com/users/guyman/received_events","type":"User","site_admin":false}}", "commentIds":["241430324","241435779","241438041","241443004","241816819"], "labels":["package=collect"]}