{"id":"1296", "title":"TypeToInstanceMap Analogies", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1296) created by **jamie.spencer.86** on 2013-02-22 at 04:56 PM_

---

I've yet to see anything guava-related concerning a key-type-value-table, or an option-hash-like implementation; although the former two may serve a different purpose from the TypeToInstanceMap.

Generally, these types would provide the interface needed to index a value by both arbitrary key and assignable type, while also handling casting to the associated type. As such, restrictions of https://google.github.io/guava/apidocs/com/google/common/reflect/TypeToInstanceMap.html#getInstance%28com.google.common.reflect.TypeToken%29 apply.

An incredibly sparse API is attached.

Use cases for both:
- Maps of variable class properties; SocketChannel has the generic type SocketOption&lt;T> used to index the available socket properties.
- Option Hash isn't a Java thing per se; it seems preferably to creating classes that encapsulate needed parameters, but an option hash could possibly allow further flexibility with out too much cost... or, at least, ease method overloading for simple (contrived _winces_) cases:
  
  {
      void each(Iterable es)
      void each(Collection es)
      List asList(
              Object[] es,
              boolean immutable,
              boolean synchronized)
      List asList(
              Object[] es,
              boolean immutable)
      ...
      List asList(Object[] es)
  
  ```
  void each(OptionHash options) {
      if (options.containsKey(\"es\", Object[].class)) {
          for (
                  Object o :
                  options.get(\"es\", Object[].class).get()) {
              // manipulate
          }
      }
      else if (options.containsKey(\"es\", Collection.class)) {
          for (
                  Object o :
                  options.get(\"es\", Collection.class).get()) {
              // manipulate
          }
      }
      else {
         // handle; likely IllegalArgumentException
      }
  }
  
  List asList(OptionHash options) {
      Object[] o = options.get(\"es\", Object[].class).get();
      boolean immutable =
          options.get(\"immutable\", boolean.class).or(false);
      boolean synchronized =
          options.get(\"synchronized\", boolean.class).or(false);
      // etc...
  }
  ```
  
  }
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1296","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1296/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1296/comments","events_url":"https://api.github.com/repos/google/guava/issues/1296/events","html_url":"https://github.com/google/guava/issues/1296","id":47423527,"node_id":"MDU6SXNzdWU0NzQyMzUyNw==","number":1296,"title":"TypeToInstanceMap Analogies","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143505259,"node_id":"MDU6TGFiZWwxNDM1MDUyNTk=","url":"https://api.github.com/repos/google/guava/labels/package=collect","name":"package=collect","color":"62a0e5","default":false},{"id":143514951,"node_id":"MDU6TGFiZWwxNDM1MTQ5NTE=","url":"https://api.github.com/repos/google/guava/labels/status=will-not-fix","name":"status=will-not-fix","color":"c5c5c5","default":false},{"id":143505258,"node_id":"MDU6TGFiZWwxNDM1MDUyNTg=","url":"https://api.github.com/repos/google/guava/labels/type=addition","name":"type=addition","color":"f4d75f","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2014-10-31T17:41:56Z","updated_at":"2015-05-14T18:50:20Z","closed_at":"2015-05-14T18:50:09Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1296) created by **jamie.spencer.86** on 2013-02-22 at 04:56 PM_\n\n---\n\nI've yet to see anything guava-related concerning a key-type-value-table, or an option-hash-like implementation; although the former two may serve a different purpose from the TypeToInstanceMap.\n\nGenerally, these types would provide the interface needed to index a value by both arbitrary key and assignable type, while also handling casting to the associated type. As such, restrictions of https://google.github.io/guava/apidocs/com/google/common/reflect/TypeToInstanceMap.html#getInstance%28com.google.common.reflect.TypeToken%29 apply.\n\nAn incredibly sparse API is attached.\n\nUse cases for both:\n- Maps of variable class properties; SocketChannel has the generic type SocketOption&lt;T> used to index the available socket properties.\n- Option Hash isn't a Java thing per se; it seems preferably to creating classes that encapsulate needed parameters, but an option hash could possibly allow further flexibility with out too much cost... or, at least, ease method overloading for simple (contrived _winces_) cases:\n  \n  {\n      void each(Iterable es)\n      void each(Collection es)\n      List asList(\n              Object[] es,\n              boolean immutable,\n              boolean synchronized)\n      List asList(\n              Object[] es,\n              boolean immutable)\n      ...\n      List asList(Object[] es)\n  \n  ```\n  void each(OptionHash options) {\n      if (options.containsKey(\"es\", Object[].class)) {\n          for (\n                  Object o :\n                  options.get(\"es\", Object[].class).get()) {\n              // manipulate\n          }\n      }\n      else if (options.containsKey(\"es\", Collection.class)) {\n          for (\n                  Object o :\n                  options.get(\"es\", Collection.class).get()) {\n              // manipulate\n          }\n      }\n      else {\n         // handle; likely IllegalArgumentException\n      }\n  }\n  \n  List asList(OptionHash options) {\n      Object[] o = options.get(\"es\", Object[].class).get();\n      boolean immutable =\n          options.get(\"immutable\", boolean.class).or(false);\n      boolean synchronized =\n          options.get(\"synchronized\", boolean.class).or(false);\n      // etc...\n  }\n  ```\n  \n  }\n","closed_by":{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false}}", "commentIds":["61351092","61351093"], "labels":["package=collect","status=will-not-fix","type=addition"]}