{"id":"636", "title":"Key as Arraylist, cant be found, when softKey() is on", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=636) created by **bartlomiej.szejna** on 2011-06-01 at 01:00 PM_

---

<b>What steps will reproduce the problem?</b>
Simple example:

private ConcurrentMap&lt;Object, Object> map = new MapMaker().softKeys().makeMap();
private ArrayList&lt;Object> mapKey = new ArrayList&lt;Object>();

mapKey.add(\"key1\");
mapKey.add(1111);
map.put(mapKey, \"value1\");
System.out.println(\"value from map: \"+map.get(mapKey)+ \", hashCode of key \"+mapKey.hashCode());

mapKey = new ArrayList&lt;Object>();
mapKey.add(\"key1\");
mapKey.add(1111);
System.out.println(\"value from map: \"+map.get(mapKey)+ \", hashCode of key \"+mapKey.hashCode());

<b>What is the expected output? What do you see instead?</b>
Expected value - \"value1\"
Instead of it, get() returns - null

<b>What version of the product are you using? On what operating system?</b>
guava verions = r09
OS - win7
java- 1.6.0_25

<b>Please provide any additional information below.</b>
Only occurs when softKey() is on
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/636","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/636/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/636/comments","events_url":"https://api.github.com/repos/google/guava/issues/636/events","html_url":"https://github.com/google/guava/issues/636","id":47420262,"node_id":"MDU6SXNzdWU0NzQyMDI2Mg==","number":636,"title":"Key as Arraylist, cant be found, when softKey() is on","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143508181,"node_id":"MDU6TGFiZWwxNDM1MDgxODE=","url":"https://api.github.com/repos/google/guava/labels/status=working-as-intended","name":"status=working-as-intended","color":"c5c5c5","default":false},{"id":143499063,"node_id":"MDU6TGFiZWwxNDM0OTkwNjM=","url":"https://api.github.com/repos/google/guava/labels/type=defect","name":"type=defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2014-10-31T17:18:45Z","updated_at":"2014-10-31T19:22:01Z","closed_at":"2014-10-31T19:22:01Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=636) created by **bartlomiej.szejna** on 2011-06-01 at 01:00 PM_\n\n---\n\n<b>What steps will reproduce the problem?</b>\nSimple example:\n\nprivate ConcurrentMap&lt;Object, Object> map = new MapMaker().softKeys().makeMap();\nprivate ArrayList&lt;Object> mapKey = new ArrayList&lt;Object>();\n\nmapKey.add(\"key1\");\nmapKey.add(1111);\nmap.put(mapKey, \"value1\");\nSystem.out.println(\"value from map: \"+map.get(mapKey)+ \", hashCode of key \"+mapKey.hashCode());\n\nmapKey = new ArrayList&lt;Object>();\nmapKey.add(\"key1\");\nmapKey.add(1111);\nSystem.out.println(\"value from map: \"+map.get(mapKey)+ \", hashCode of key \"+mapKey.hashCode());\n\n<b>What is the expected output? What do you see instead?</b>\nExpected value - \"value1\"\nInstead of it, get() returns - null\n\n<b>What version of the product are you using? On what operating system?</b>\nguava verions = r09\nOS - win7\njava- 1.6.0_25\n\n<b>Please provide any additional information below.</b>\nOnly occurs when softKey() is on\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61315704"], "labels":["status=working-as-intended","type=defect"]}