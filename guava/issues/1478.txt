{"id":"1478", "title":"ServiceManager example deadlocks if service fails on startup ", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1478) created by **a...@relational.io** on 2013-07-17 at 07:26 AM_

---

The ServiceManager example located at https://google.github.io/guava/apidocs/com/google/common/util/concurrent/ServiceManager.html does not work.

The example deadlocks if any Service fails on startup. The call to `manager.stopAsync().awaitStopped(5, TimeUnit.SECONDS);`&nbsp;in the shutdown hook never returns.

I can make the example work if I swap the Executor given to manager.addListener from a `MoreExecutors.sameThreadExecutor()`&nbsp;to a `Executors.newSingleThreadExecutor()`&nbsp;(or other alternative)

Given the following code:

```
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.*;

import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class DeadlockTest {

    public static class ShutdownIgnoringLogManager extends LogManager {
        @Override
        public void reset() throws SecurityException {
            // override reset() so logging handlers still work in shutdown hooks
        }
    }

    static {
        System.setProperty(\"java.util.logging.manager\", ShutdownIgnoringLogManager.class.getName());
    }

    static class TestService extends AbstractIdleService {
        @Override
        protected void startUp() throws Exception {
            throw new IllegalStateException(\"failed\");
        }

        @Override
        protected void shutDown() {
        }
    }

    public static void main(String[] args) {
        final Logger logger = Logger.getLogger(DeadlockTest.class.getName());

        Set<Service> services = ImmutableSet.<Service>of(new TestService());
        final ServiceManager manager = new ServiceManager(services);
        manager.addListener(new ServiceManager.Listener() {
            public void stopped() {}
            public void healthy() {
                // Services have been initialized and are healthy, start accepting requests...
            }
            public void failure(Service service) {
                // Something failed, at this point we could log it, notify a load balancer, or take
                // some other action.  For now we will just exit.
                System.exit(1);
            }
        },
                MoreExecutors.sameThreadExecutor()
                //Executors.newSingleThreadExecutor()
        );

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                // Give the services 5 seconds to stop to ensure that we are responsive to shutdown
                // requests.
                try {
                    logger.info(\"Shutting down services\");
                    manager.stopAsync().awaitStopped(5, TimeUnit.SECONDS);
                    logger.info(\"Successfully shut down services\");
                } catch (TimeoutException timeout) {
                    logger.severe(\"Shutdown timeout reached\");
                }
            }
        });
        manager.startAsync();  // start all the services asynchronously
    }
}
```

Which is near-identical to the example — I've added a bit of logging and an actual Service to start.
This happens:

```
Jul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener startTimer
INFO: Starting TestService [NEW]
Jul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener failed
SEVERE: Service TestService [FAILED] has failed in the STARTING state.
java.lang.IllegalStateException: failed
    at DeadlockTest$TestService.startUp(DeadlockTest.java:29)
    at com.google.common.util.concurrent.AbstractIdleService$1$1.run(AbstractIdleService.java:43)
    at java.lang.Thread.run(Thread.java:724)

Jul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener finishedStarting
INFO: Started TestService [FAILED] in 33 ms.
Jul 17, 2013 5:24:06 PM DeadlockTest$2 run
INFO: Shutting down services
```

It never completes.

Swapping out the Executor for a different implementation allows the application gracefully shutdown.

System:
$ java -version
java version \"1.7.0_25\"
Java(TM) SE Runtime Environment (build 1.7.0_25-b15)
Java HotSpot(TM) 64-Bit Server VM (build 23.25-b01, mixed mode)

$ uname -a
Darwin Adams-Mac-mini.local 12.4.0 Darwin Kernel Version 12.4.0: Wed May  1 17:57:12 PDT 2013; root:xnu-2050.24.15~1/RELEASE_X86_64 x86_64
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1478","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1478/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1478/comments","events_url":"https://api.github.com/repos/google/guava/issues/1478/events","html_url":"https://github.com/google/guava/issues/1478","id":47424504,"node_id":"MDU6SXNzdWU0NzQyNDUwNA==","number":1478,"title":"ServiceManager example deadlocks if service fails on startup ","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143505670,"node_id":"MDU6TGFiZWwxNDM1MDU2NzA=","url":"https://api.github.com/repos/google/guava/labels/package=concurrent","name":"package=concurrent","color":"62a0e5","default":false},{"id":143499064,"node_id":"MDU6TGFiZWwxNDM0OTkwNjQ=","url":"https://api.github.com/repos/google/guava/labels/status=fixed","name":"status=fixed","color":"6eb26e","default":false},{"id":143499063,"node_id":"MDU6TGFiZWwxNDM0OTkwNjM=","url":"https://api.github.com/repos/google/guava/labels/type=defect","name":"type=defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"lukesandberg","id":210140,"node_id":"MDQ6VXNlcjIxMDE0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/210140?v=4","gravatar_id":"","url":"https://api.github.com/users/lukesandberg","html_url":"https://github.com/lukesandberg","followers_url":"https://api.github.com/users/lukesandberg/followers","following_url":"https://api.github.com/users/lukesandberg/following{/other_user}","gists_url":"https://api.github.com/users/lukesandberg/gists{/gist_id}","starred_url":"https://api.github.com/users/lukesandberg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukesandberg/subscriptions","organizations_url":"https://api.github.com/users/lukesandberg/orgs","repos_url":"https://api.github.com/users/lukesandberg/repos","events_url":"https://api.github.com/users/lukesandberg/events{/privacy}","received_events_url":"https://api.github.com/users/lukesandberg/received_events","type":"User","site_admin":false},"assignees":[{"login":"lukesandberg","id":210140,"node_id":"MDQ6VXNlcjIxMDE0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/210140?v=4","gravatar_id":"","url":"https://api.github.com/users/lukesandberg","html_url":"https://github.com/lukesandberg","followers_url":"https://api.github.com/users/lukesandberg/followers","following_url":"https://api.github.com/users/lukesandberg/following{/other_user}","gists_url":"https://api.github.com/users/lukesandberg/gists{/gist_id}","starred_url":"https://api.github.com/users/lukesandberg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukesandberg/subscriptions","organizations_url":"https://api.github.com/users/lukesandberg/orgs","repos_url":"https://api.github.com/users/lukesandberg/repos","events_url":"https://api.github.com/users/lukesandberg/events{/privacy}","received_events_url":"https://api.github.com/users/lukesandberg/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/google/guava/milestones/10","html_url":"https://github.com/google/guava/milestone/10","labels_url":"https://api.github.com/repos/google/guava/milestones/10/labels","id":849123,"node_id":"MDk6TWlsZXN0b25lODQ5MTIz","number":10,"title":"15.0","description":"","creator":{"login":"cgdecker","id":101568,"node_id":"MDQ6VXNlcjEwMTU2OA==","avatar_url":"https://avatars0.githubusercontent.com/u/101568?v=4","gravatar_id":"","url":"https://api.github.com/users/cgdecker","html_url":"https://github.com/cgdecker","followers_url":"https://api.github.com/users/cgdecker/followers","following_url":"https://api.github.com/users/cgdecker/following{/other_user}","gists_url":"https://api.github.com/users/cgdecker/gists{/gist_id}","starred_url":"https://api.github.com/users/cgdecker/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cgdecker/subscriptions","organizations_url":"https://api.github.com/users/cgdecker/orgs","repos_url":"https://api.github.com/users/cgdecker/repos","events_url":"https://api.github.com/users/cgdecker/events{/privacy}","received_events_url":"https://api.github.com/users/cgdecker/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":52,"state":"closed","created_at":"2014-11-01T03:47:07Z","updated_at":"2016-10-06T21:11:15Z","due_on":null,"closed_at":"2014-11-06T23:12:29Z"},"comments":4,"created_at":"2014-10-31T17:48:16Z","updated_at":"2017-01-26T19:38:15Z","closed_at":"2014-11-01T01:33:27Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1478) created by **a...@relational.io** on 2013-07-17 at 07:26 AM_\n\n---\n\nThe ServiceManager example located at https://google.github.io/guava/apidocs/com/google/common/util/concurrent/ServiceManager.html does not work.\n\nThe example deadlocks if any Service fails on startup. The call to `manager.stopAsync().awaitStopped(5, TimeUnit.SECONDS);`&nbsp;in the shutdown hook never returns.\n\nI can make the example work if I swap the Executor given to manager.addListener from a `MoreExecutors.sameThreadExecutor()`&nbsp;to a `Executors.newSingleThreadExecutor()`&nbsp;(or other alternative)\n\nGiven the following code:\n\n```\nimport com.google.common.collect.ImmutableSet;\nimport com.google.common.util.concurrent.*;\n\nimport java.util.Set;\nimport java.util.concurrent.Executors;\nimport java.util.concurrent.TimeUnit;\nimport java.util.concurrent.TimeoutException;\nimport java.util.logging.LogManager;\nimport java.util.logging.Logger;\n\npublic class DeadlockTest {\n\n    public static class ShutdownIgnoringLogManager extends LogManager {\n        @Override\n        public void reset() throws SecurityException {\n            // override reset() so logging handlers still work in shutdown hooks\n        }\n    }\n\n    static {\n        System.setProperty(\"java.util.logging.manager\", ShutdownIgnoringLogManager.class.getName());\n    }\n\n    static class TestService extends AbstractIdleService {\n        @Override\n        protected void startUp() throws Exception {\n            throw new IllegalStateException(\"failed\");\n        }\n\n        @Override\n        protected void shutDown() {\n        }\n    }\n\n    public static void main(String[] args) {\n        final Logger logger = Logger.getLogger(DeadlockTest.class.getName());\n\n        Set<Service> services = ImmutableSet.<Service>of(new TestService());\n        final ServiceManager manager = new ServiceManager(services);\n        manager.addListener(new ServiceManager.Listener() {\n            public void stopped() {}\n            public void healthy() {\n                // Services have been initialized and are healthy, start accepting requests...\n            }\n            public void failure(Service service) {\n                // Something failed, at this point we could log it, notify a load balancer, or take\n                // some other action.  For now we will just exit.\n                System.exit(1);\n            }\n        },\n                MoreExecutors.sameThreadExecutor()\n                //Executors.newSingleThreadExecutor()\n        );\n\n        Runtime.getRuntime().addShutdownHook(new Thread() {\n            public void run() {\n                // Give the services 5 seconds to stop to ensure that we are responsive to shutdown\n                // requests.\n                try {\n                    logger.info(\"Shutting down services\");\n                    manager.stopAsync().awaitStopped(5, TimeUnit.SECONDS);\n                    logger.info(\"Successfully shut down services\");\n                } catch (TimeoutException timeout) {\n                    logger.severe(\"Shutdown timeout reached\");\n                }\n            }\n        });\n        manager.startAsync();  // start all the services asynchronously\n    }\n}\n```\n\nWhich is near-identical to the example — I've added a bit of logging and an actual Service to start.\nThis happens:\n\n```\nJul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener startTimer\nINFO: Starting TestService [NEW]\nJul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener failed\nSEVERE: Service TestService [FAILED] has failed in the STARTING state.\njava.lang.IllegalStateException: failed\n    at DeadlockTest$TestService.startUp(DeadlockTest.java:29)\n    at com.google.common.util.concurrent.AbstractIdleService$1$1.run(AbstractIdleService.java:43)\n    at java.lang.Thread.run(Thread.java:724)\n\nJul 17, 2013 5:24:06 PM com.google.common.util.concurrent.ServiceManager$ServiceListener finishedStarting\nINFO: Started TestService [FAILED] in 33 ms.\nJul 17, 2013 5:24:06 PM DeadlockTest$2 run\nINFO: Shutting down services\n```\n\nIt never completes.\n\nSwapping out the Executor for a different implementation allows the application gracefully shutdown.\n\nSystem:\n$ java -version\njava version \"1.7.0_25\"\nJava(TM) SE Runtime Environment (build 1.7.0_25-b15)\nJava HotSpot(TM) 64-Bit Server VM (build 23.25-b01, mixed mode)\n\n$ uname -a\nDarwin Adams-Mac-mini.local 12.4.0 Darwin Kernel Version 12.4.0: Wed May  1 17:57:12 PDT 2013; root:xnu-2050.24.15~1/RELEASE_X86_64 x86_64\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61352435","61352436","61352536","61352539"], "labels":["package=concurrent","status=fixed","type=defect"]}