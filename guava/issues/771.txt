{"id":"771", "title":"Iterables.transform() with condition (Predicate)", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=771) created by **majaesch** on 2011-10-24 at 07:50 PM_

---

I think this would be useful for Maps, Lists, Iterables:

transform( Iterable&lt;T>, Function<? super F,? extends T>, Predicate<? super T> )

This is eqal to:

Iterable i = ...;
i = Iterables.removeIf(i, predicate);
i = Iterables.transform(i, function);

The advantage: just one iteration.

Example Code:

public class SelectableItemCollection&lt;T> {
&nbsp;&nbsp;private static class SelectableItem&lt;T> {
&nbsp;&nbsp;&nbsp;&nbsp;T value;
&nbsp;&nbsp;&nbsp;&nbsp;boolean selected;
&nbsp;&nbsp;}

&nbsp;&nbsp;public Iterable&lt;T> getSelection() {
&nbsp;&nbsp;&nbsp;&nbsp;return Iterables.transform( this, getTsFunction, mustBeSelectedPredicate );
&nbsp;&nbsp;}
}

PS: Why has Sets no transform method?
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/771","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/771/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/771/comments","events_url":"https://api.github.com/repos/google/guava/issues/771/events","html_url":"https://github.com/google/guava/issues/771","id":47420927,"node_id":"MDU6SXNzdWU0NzQyMDkyNw==","number":771,"title":"Iterables.transform() with condition (Predicate)","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143515873,"node_id":"MDU6TGFiZWwxNDM1MTU4NzM=","url":"https://api.github.com/repos/google/guava/labels/status=invalid","name":"status=invalid","color":"c5c5c5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2014-10-31T17:23:35Z","updated_at":"2014-10-31T19:58:50Z","closed_at":"2014-10-31T19:58:50Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=771) created by **majaesch** on 2011-10-24 at 07:50 PM_\n\n---\n\nI think this would be useful for Maps, Lists, Iterables:\n\ntransform( Iterable&lt;T>, Function<? super F,? extends T>, Predicate<? super T> )\n\nThis is eqal to:\n\nIterable i = ...;\ni = Iterables.removeIf(i, predicate);\ni = Iterables.transform(i, function);\n\nThe advantage: just one iteration.\n\nExample Code:\n\npublic class SelectableItemCollection&lt;T> {\n&nbsp;&nbsp;private static class SelectableItem&lt;T> {\n&nbsp;&nbsp;&nbsp;&nbsp;T value;\n&nbsp;&nbsp;&nbsp;&nbsp;boolean selected;\n&nbsp;&nbsp;}\n\n&nbsp;&nbsp;public Iterable&lt;T> getSelection() {\n&nbsp;&nbsp;&nbsp;&nbsp;return Iterables.transform( this, getTsFunction, mustBeSelectedPredicate );\n&nbsp;&nbsp;}\n}\n\nPS: Why has Sets no transform method?\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61321713","61321717","61321719"], "labels":["status=invalid"]}