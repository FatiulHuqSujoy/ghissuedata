{"id":"2964", "title":"ServiceManager.awaitHealthy method is not working when run jetty embed.", "body":"**app: guava
version: 20.0**

**code 1:**
```
public class EmbedMonitoredServices {

    // ------------------------------------------------------------------------

    private static final Logger logger = LoggerFactory.getLogger(EmbedMonitoredServices.class);

    private static final Set<Service> monitoredServices = Sets.newHashSet();

    private static ServiceManager monitoredServiceManager;

    private static MonitoredWebService monitoredWebService;

    // ------------------------------------------------------------------------

    private static MonitoredWebService getMonitoredWebService() {
        if (monitoredWebService == null) {
            monitoredWebService = new MonitoredWebService();
        }
        return monitoredWebService;
    }

    private static ServiceManager getMonitoredServiceManager() {
        if (monitoredServiceManager == null) {
            monitoredServices.add(getMonitoredWebService());
            monitoredServiceManager = new ServiceManager(monitoredServices);
        }
        return monitoredServiceManager;
    }

    // ------------------------------------------------------------------------
    
    public static ServiceManager manage() {
        return getMonitoredServiceManager();
    }

    // ------------------------------------------------------------------------

    public static class MonitoredWebService extends AbstractService {

        // --------------------------------------------------------------------

        private static final String CONTEXT_PATH = \"/scout-web-monitor\";

        private static final String PACKAGE_PATH = EnvironmentContents.PROJECT_HOME + \"/lib/ext/scout-web-monitor-1.0.0.war\";

        private static Server server;

        // --------------------------------------------------------------------

        @Override
        protected void doStart() {
            server = new Server(8080);
            MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
            server.addBean(mbContainer);
            WebAppContext webapp = new WebAppContext();
            webapp.setContextPath(CONTEXT_PATH);
            webapp.setWar(PACKAGE_PATH);
            webapp.addAliasCheck(new AllowSymLinkAliasChecker());
            webapp.addLifeCycleListener(new LifeCycle.Listener() {

                @Override
                public void lifeCycleStarting(LifeCycle event) {
                    LoggingUtils.info(logger, manage().servicesByState());
                }

                @Override
                public void lifeCycleStarted(LifeCycle event) {
                    LoggingUtils.info(logger, manage().servicesByState());
                    notifyStarted();
                    LoggingUtils.info(logger, manage().servicesByState());
                }

                @Override
                public void lifeCycleFailure(LifeCycle event, Throwable cause) {
                    notifyFailed(LoggingUtils.error(logger, cause));
                }

                @Override
                public void lifeCycleStopping(LifeCycle event) {}

                @Override
                public void lifeCycleStopped(LifeCycle event) {
                    notifyStopped();
                }

            });
            server.setHandler(webapp);
            try {
                server.start();
                // erver.dumpStdErr();
                server.join();
            } catch (Exception e) {
                throw LoggingUtils.error(logger, e);
            }
        }

        @Override
        protected void doStop() {
            try {
                server.stop();
            } catch (Exception e) {
                throw LoggingUtils.error(logger, e);
            }
        }

    }

}
```

**code 2:**
```
public static void startMonitoredService(PrintStream stdout, NmapProfile profile) throws Exception {
        showLogo(stdout);
        Environments.initializeLogback();
        LoggingUtils.info(logger, \"Start Monitored Service.\");
        EmbedMonitoredServices.manage().startAsync().awaitHealthy();
        LoggingUtils.info(logger, \"Monitored Service started.\");
        LoggingUtils.info(logger, \"Monitored Service started.\");
}
```

**print:**
```
[INFO] Start Monitored Service.
[INFO] Logging initialized @5375ms
[INFO] jetty-9.2.22.v20170606
[INFO] {NEW=[MonitoredWebService [STARTING]]}
[INFO] jetty-9.2.22.v20170606
[WARN] ServletContext@o.e.j.s.ServletContextHandler@542e560f{/,null,STARTING} has uncovered http methods for path: /
[INFO] Started ServerConnector@8c11eee{HTTP/1.1}{0.0.0.0:61614}
[INFO] Started @6749ms
[INFO] {NEW=[MonitoredWebService [STARTING]]}
[INFO] {NEW=[MonitoredWebService [RUNNING]]}
[INFO] Started ServerConnector@32a068d1{HTTP/1.1}{0.0.0.0:8080}
[INFO] Started @7163ms
```", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2964","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2964/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2964/comments","events_url":"https://api.github.com/repos/google/guava/issues/2964/events","html_url":"https://github.com/google/guava/issues/2964","id":264325731,"node_id":"MDU6SXNzdWUyNjQzMjU3MzE=","number":2964,"title":"ServiceManager.awaitHealthy method is not working when run jetty embed.","user":{"login":"deathknight0718","id":6452211,"node_id":"MDQ6VXNlcjY0NTIyMTE=","avatar_url":"https://avatars0.githubusercontent.com/u/6452211?v=4","gravatar_id":"","url":"https://api.github.com/users/deathknight0718","html_url":"https://github.com/deathknight0718","followers_url":"https://api.github.com/users/deathknight0718/followers","following_url":"https://api.github.com/users/deathknight0718/following{/other_user}","gists_url":"https://api.github.com/users/deathknight0718/gists{/gist_id}","starred_url":"https://api.github.com/users/deathknight0718/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/deathknight0718/subscriptions","organizations_url":"https://api.github.com/users/deathknight0718/orgs","repos_url":"https://api.github.com/users/deathknight0718/repos","events_url":"https://api.github.com/users/deathknight0718/events{/privacy}","received_events_url":"https://api.github.com/users/deathknight0718/received_events","type":"User","site_admin":false},"labels":[{"id":143505670,"node_id":"MDU6TGFiZWwxNDM1MDU2NzA=","url":"https://api.github.com/repos/google/guava/labels/package=concurrent","name":"package=concurrent","color":"62a0e5","default":false},{"id":143515873,"node_id":"MDU6TGFiZWwxNDM1MTU4NzM=","url":"https://api.github.com/repos/google/guava/labels/status=invalid","name":"status=invalid","color":"c5c5c5","default":false},{"id":143499063,"node_id":"MDU6TGFiZWwxNDM0OTkwNjM=","url":"https://api.github.com/repos/google/guava/labels/type=defect","name":"type=defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-10-10T18:13:31Z","updated_at":"2017-10-11T16:54:48Z","closed_at":"2017-10-11T16:53:50Z","author_association":"NONE","body":"**app: guava\r\nversion: 20.0**\r\n\r\n**code 1:**\r\n```\r\npublic class EmbedMonitoredServices {\r\n\r\n    // ------------------------------------------------------------------------\r\n\r\n    private static final Logger logger = LoggerFactory.getLogger(EmbedMonitoredServices.class);\r\n\r\n    private static final Set<Service> monitoredServices = Sets.newHashSet();\r\n\r\n    private static ServiceManager monitoredServiceManager;\r\n\r\n    private static MonitoredWebService monitoredWebService;\r\n\r\n    // ------------------------------------------------------------------------\r\n\r\n    private static MonitoredWebService getMonitoredWebService() {\r\n        if (monitoredWebService == null) {\r\n            monitoredWebService = new MonitoredWebService();\r\n        }\r\n        return monitoredWebService;\r\n    }\r\n\r\n    private static ServiceManager getMonitoredServiceManager() {\r\n        if (monitoredServiceManager == null) {\r\n            monitoredServices.add(getMonitoredWebService());\r\n            monitoredServiceManager = new ServiceManager(monitoredServices);\r\n        }\r\n        return monitoredServiceManager;\r\n    }\r\n\r\n    // ------------------------------------------------------------------------\r\n    \r\n    public static ServiceManager manage() {\r\n        return getMonitoredServiceManager();\r\n    }\r\n\r\n    // ------------------------------------------------------------------------\r\n\r\n    public static class MonitoredWebService extends AbstractService {\r\n\r\n        // --------------------------------------------------------------------\r\n\r\n        private static final String CONTEXT_PATH = \"/scout-web-monitor\";\r\n\r\n        private static final String PACKAGE_PATH = EnvironmentContents.PROJECT_HOME + \"/lib/ext/scout-web-monitor-1.0.0.war\";\r\n\r\n        private static Server server;\r\n\r\n        // --------------------------------------------------------------------\r\n\r\n        @Override\r\n        protected void doStart() {\r\n            server = new Server(8080);\r\n            MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());\r\n            server.addBean(mbContainer);\r\n            WebAppContext webapp = new WebAppContext();\r\n            webapp.setContextPath(CONTEXT_PATH);\r\n            webapp.setWar(PACKAGE_PATH);\r\n            webapp.addAliasCheck(new AllowSymLinkAliasChecker());\r\n            webapp.addLifeCycleListener(new LifeCycle.Listener() {\r\n\r\n                @Override\r\n                public void lifeCycleStarting(LifeCycle event) {\r\n                    LoggingUtils.info(logger, manage().servicesByState());\r\n                }\r\n\r\n                @Override\r\n                public void lifeCycleStarted(LifeCycle event) {\r\n                    LoggingUtils.info(logger, manage().servicesByState());\r\n                    notifyStarted();\r\n                    LoggingUtils.info(logger, manage().servicesByState());\r\n                }\r\n\r\n                @Override\r\n                public void lifeCycleFailure(LifeCycle event, Throwable cause) {\r\n                    notifyFailed(LoggingUtils.error(logger, cause));\r\n                }\r\n\r\n                @Override\r\n                public void lifeCycleStopping(LifeCycle event) {}\r\n\r\n                @Override\r\n                public void lifeCycleStopped(LifeCycle event) {\r\n                    notifyStopped();\r\n                }\r\n\r\n            });\r\n            server.setHandler(webapp);\r\n            try {\r\n                server.start();\r\n                // erver.dumpStdErr();\r\n                server.join();\r\n            } catch (Exception e) {\r\n                throw LoggingUtils.error(logger, e);\r\n            }\r\n        }\r\n\r\n        @Override\r\n        protected void doStop() {\r\n            try {\r\n                server.stop();\r\n            } catch (Exception e) {\r\n                throw LoggingUtils.error(logger, e);\r\n            }\r\n        }\r\n\r\n    }\r\n\r\n}\r\n```\r\n\r\n**code 2:**\r\n```\r\npublic static void startMonitoredService(PrintStream stdout, NmapProfile profile) throws Exception {\r\n        showLogo(stdout);\r\n        Environments.initializeLogback();\r\n        LoggingUtils.info(logger, \"Start Monitored Service.\");\r\n        EmbedMonitoredServices.manage().startAsync().awaitHealthy();\r\n        LoggingUtils.info(logger, \"Monitored Service started.\");\r\n        LoggingUtils.info(logger, \"Monitored Service started.\");\r\n}\r\n```\r\n\r\n**print:**\r\n```\r\n[INFO] Start Monitored Service.\r\n[INFO] Logging initialized @5375ms\r\n[INFO] jetty-9.2.22.v20170606\r\n[INFO] {NEW=[MonitoredWebService [STARTING]]}\r\n[INFO] jetty-9.2.22.v20170606\r\n[WARN] ServletContext@o.e.j.s.ServletContextHandler@542e560f{/,null,STARTING} has uncovered http methods for path: /\r\n[INFO] Started ServerConnector@8c11eee{HTTP/1.1}{0.0.0.0:61614}\r\n[INFO] Started @6749ms\r\n[INFO] {NEW=[MonitoredWebService [STARTING]]}\r\n[INFO] {NEW=[MonitoredWebService [RUNNING]]}\r\n[INFO] Started ServerConnector@32a068d1{HTTP/1.1}{0.0.0.0:8080}\r\n[INFO] Started @7163ms\r\n```","closed_by":{"login":"netdpb","id":4306377,"node_id":"MDQ6VXNlcjQzMDYzNzc=","avatar_url":"https://avatars0.githubusercontent.com/u/4306377?v=4","gravatar_id":"","url":"https://api.github.com/users/netdpb","html_url":"https://github.com/netdpb","followers_url":"https://api.github.com/users/netdpb/followers","following_url":"https://api.github.com/users/netdpb/following{/other_user}","gists_url":"https://api.github.com/users/netdpb/gists{/gist_id}","starred_url":"https://api.github.com/users/netdpb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/netdpb/subscriptions","organizations_url":"https://api.github.com/users/netdpb/orgs","repos_url":"https://api.github.com/users/netdpb/repos","events_url":"https://api.github.com/users/netdpb/events{/privacy}","received_events_url":"https://api.github.com/users/netdpb/received_events","type":"User","site_admin":false}}", "commentIds":["335562647","335875388"], "labels":["package=concurrent","status=invalid","type=defect"]}