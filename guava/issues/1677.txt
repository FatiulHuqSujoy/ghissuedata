{"id":"1677", "title":"Create abstract base classes for Function and Predicate to deal with @Nullable issues", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1677) created by **SeanPFloyd** on 2014-02-20 at 04:43 PM_

---

When implementing Functions or Predicates I often need to add special-case boilerplate for dealing with null inputs or be punished by findbugs for ignoring the @﻿Nullable parameter annotations. To deal with this, I find myself creating abstract classes for both Functions and Predicates in many different projects.

For predicates there is only one version that makes sense

public abstract class NullSafePredicate&lt;T> implements Predicate&lt;T> {
&nbsp;&nbsp;&nbsp;&nbsp;@﻿Override
&nbsp;&nbsp;&nbsp;&nbsp;public final boolean apply(@﻿Nullable final T input) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return input != null && doApply(input);
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;protected abstract boolean doApply(@﻿Nonnull T input);
}

For Functions I use two versions, null in -> null out and null in -> NPE

public abstract class NullSafeFunction&lt;I, O> implements Function&lt;I, O> {
&nbsp;&nbsp;&nbsp;&nbsp;@﻿Nullable @﻿Override
&nbsp;&nbsp;&nbsp;&nbsp;public final O apply(@﻿Nullable final I input) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return input == null ? null : doApply(input);
&nbsp;&nbsp;&nbsp;&nbsp;}

```
protected abstract O doApply(@Nonnull I input);
```

}

public abstract class NullRejectingFunction&lt;I, O> implements Function&lt;I, O> {
&nbsp;&nbsp;&nbsp;&nbsp;@﻿Nullable @﻿Override
&nbsp;&nbsp;&nbsp;&nbsp;public final O apply(@﻿Nullable final I input) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return doApply(checkNotNull(input));
&nbsp;&nbsp;&nbsp;&nbsp;}

```
protected abstract O doApply(@Nonnull I input);
```

}

What are your thoughts on adding such helper classes to Guava?
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1677","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1677/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1677/comments","events_url":"https://api.github.com/repos/google/guava/issues/1677/events","html_url":"https://github.com/google/guava/issues/1677","id":47425527,"node_id":"MDU6SXNzdWU0NzQyNTUyNw==","number":1677,"title":"Create abstract base classes for Function and Predicate to deal with @Nullable issues","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143505359,"node_id":"MDU6TGFiZWwxNDM1MDUzNTk=","url":"https://api.github.com/repos/google/guava/labels/package=base","name":"package=base","color":"62a0e5","default":false},{"id":143515008,"node_id":"MDU6TGFiZWwxNDM1MTUwMDg=","url":"https://api.github.com/repos/google/guava/labels/status=duplicate","name":"status=duplicate","color":"c5c5c5","default":false},{"id":143505258,"node_id":"MDU6TGFiZWwxNDM1MDUyNTg=","url":"https://api.github.com/repos/google/guava/labels/type=addition","name":"type=addition","color":"f4d75f","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2014-10-31T17:55:15Z","updated_at":"2014-11-01T02:21:48Z","closed_at":"2014-11-01T02:21:48Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1677) created by **SeanPFloyd** on 2014-02-20 at 04:43 PM_\n\n---\n\nWhen implementing Functions or Predicates I often need to add special-case boilerplate for dealing with null inputs or be punished by findbugs for ignoring the @﻿Nullable parameter annotations. To deal with this, I find myself creating abstract classes for both Functions and Predicates in many different projects.\n\nFor predicates there is only one version that makes sense\n\npublic abstract class NullSafePredicate&lt;T> implements Predicate&lt;T> {\n&nbsp;&nbsp;&nbsp;&nbsp;@﻿Override\n&nbsp;&nbsp;&nbsp;&nbsp;public final boolean apply(@﻿Nullable final T input) {\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return input != null && doApply(input);\n&nbsp;&nbsp;&nbsp;&nbsp;}\n&nbsp;&nbsp;&nbsp;&nbsp;protected abstract boolean doApply(@﻿Nonnull T input);\n}\n\nFor Functions I use two versions, null in -> null out and null in -> NPE\n\npublic abstract class NullSafeFunction&lt;I, O> implements Function&lt;I, O> {\n&nbsp;&nbsp;&nbsp;&nbsp;@﻿Nullable @﻿Override\n&nbsp;&nbsp;&nbsp;&nbsp;public final O apply(@﻿Nullable final I input) {\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return input == null ? null : doApply(input);\n&nbsp;&nbsp;&nbsp;&nbsp;}\n\n```\nprotected abstract O doApply(@Nonnull I input);\n```\n\n}\n\npublic abstract class NullRejectingFunction&lt;I, O> implements Function&lt;I, O> {\n&nbsp;&nbsp;&nbsp;&nbsp;@﻿Nullable @﻿Override\n&nbsp;&nbsp;&nbsp;&nbsp;public final O apply(@﻿Nullable final I input) {\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return doApply(checkNotNull(input));\n&nbsp;&nbsp;&nbsp;&nbsp;}\n\n```\nprotected abstract O doApply(@Nonnull I input);\n```\n\n}\n\nWhat are your thoughts on adding such helper classes to Guava?\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61354044","61354048","61354059","61354443","61355031"], "labels":["package=base","status=duplicate","type=addition"]}