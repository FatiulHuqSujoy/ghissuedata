{"id":"314", "title":" consider memoizing results of filtering a lazily filtered Iterable ", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=314) created by **ffaber** on 2010-01-15 at 04:53 AM_

---

consider this snippet:

```
return ImmutableSet.copyOf(Sets.filter(
    unfilteredSetOfThings,
    new Predicate<Thing>() {
      @Override
      public boolean apply(Thing thing) {
        return thing.isAcceptable();
      }
    }));
```

Q1:
&nbsp;&nbsp;how many times is \"isAcceptable\" invoked on each Thing in the list of 
unfiltered Things?

Q2:
&nbsp;&nbsp;how many times would you think it should be invoked?

For me, A2 was 1; alas, A1 disagreed!

In short, each Thing has \"isAcceptable\" invoked 3 times for this one 
copyOf() operation.  This happens each time .size() is called on the filtered 
view of the unfitered set, and then once while iterating.

Filing to solicit opinions on whether this is ok behavior.

thanks
-Fred
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/314","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/314/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/314/comments","events_url":"https://api.github.com/repos/google/guava/issues/314/events","html_url":"https://github.com/google/guava/issues/314","id":47418794,"node_id":"MDU6SXNzdWU0NzQxODc5NA==","number":314,"title":" consider memoizing results of filtering a lazily filtered Iterable ","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143514951,"node_id":"MDU6TGFiZWwxNDM1MTQ5NTE=","url":"https://api.github.com/repos/google/guava/labels/status=will-not-fix","name":"status=will-not-fix","color":"c5c5c5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2014-10-31T17:07:28Z","updated_at":"2014-10-31T18:20:12Z","closed_at":"2014-10-31T18:20:12Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=314) created by **ffaber** on 2010-01-15 at 04:53 AM_\n\n---\n\nconsider this snippet:\n\n```\nreturn ImmutableSet.copyOf(Sets.filter(\n    unfilteredSetOfThings,\n    new Predicate<Thing>() {\n      @Override\n      public boolean apply(Thing thing) {\n        return thing.isAcceptable();\n      }\n    }));\n```\n\nQ1:\n&nbsp;&nbsp;how many times is \"isAcceptable\" invoked on each Thing in the list of \nunfiltered Things?\n\nQ2:\n&nbsp;&nbsp;how many times would you think it should be invoked?\n\nFor me, A2 was 1; alas, A1 disagreed!\n\nIn short, each Thing has \"isAcceptable\" invoked 3 times for this one \ncopyOf() operation.  This happens each time .size() is called on the filtered \nview of the unfitered set, and then once while iterating.\n\nFiling to solicit opinions on whether this is ok behavior.\n\nthanks\n-Fred\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61305335","61305343","61305347"], "labels":["status=will-not-fix"]}