{"id":"2137", "title":"FluentIterable::toList doesn't infer return type of List<? super E> like Java 8 Stream can do", "body":"FluentIterable::toList doesn't infer return type of List<? super E> like Java 8 Stream can do.

```
class BaseClass {
    final String name;

BaseClass(String name) {
        this.name = name;
    }
}
class SubClass extends BaseClass {
    SubClass(String name) {
        super(name);
    }
}
// This should compile, but can't because the LHS is BaseClass, but after transform, we have FluentIterable<SubClass> and toList() can't adapt.
List<BaseClass> ret = FluentIterable.from(Splitter.on(CharMatcher.WHITESPACE).split(\"Foo bar baz\"))
    .transform(t -> new SubClass(t))
    .toList();

// This compiles, FWIW in Java 8
List<BaseClass> ret = Splitter.on(CharMatcher.WHITESPACE).splitToList(\"Foo bar baz\")
    .stream()
    .map(t -> new SubClass(t))
    .collect(Collectors.toList());
```

/cc @kuangchen
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2137","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2137/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2137/comments","events_url":"https://api.github.com/repos/google/guava/issues/2137/events","html_url":"https://github.com/google/guava/issues/2137","id":101958217,"node_id":"MDU6SXNzdWUxMDE5NTgyMTc=","number":2137,"title":"FluentIterable::toList doesn't infer return type of List<? super E> like Java 8 Stream can do","user":{"login":"scr","id":245840,"node_id":"MDQ6VXNlcjI0NTg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/245840?v=4","gravatar_id":"","url":"https://api.github.com/users/scr","html_url":"https://github.com/scr","followers_url":"https://api.github.com/users/scr/followers","following_url":"https://api.github.com/users/scr/following{/other_user}","gists_url":"https://api.github.com/users/scr/gists{/gist_id}","starred_url":"https://api.github.com/users/scr/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/scr/subscriptions","organizations_url":"https://api.github.com/users/scr/orgs","repos_url":"https://api.github.com/users/scr/repos","events_url":"https://api.github.com/users/scr/events{/privacy}","received_events_url":"https://api.github.com/users/scr/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-08-19T17:51:16Z","updated_at":"2015-08-21T01:16:25Z","closed_at":"2015-08-21T01:16:00Z","author_association":"NONE","body":"FluentIterable::toList doesn't infer return type of List<? super E> like Java 8 Stream can do.\n\n```\nclass BaseClass {\n    final String name;\n\nBaseClass(String name) {\n        this.name = name;\n    }\n}\nclass SubClass extends BaseClass {\n    SubClass(String name) {\n        super(name);\n    }\n}\n// This should compile, but can't because the LHS is BaseClass, but after transform, we have FluentIterable<SubClass> and toList() can't adapt.\nList<BaseClass> ret = FluentIterable.from(Splitter.on(CharMatcher.WHITESPACE).split(\"Foo bar baz\"))\n    .transform(t -> new SubClass(t))\n    .toList();\n\n// This compiles, FWIW in Java 8\nList<BaseClass> ret = Splitter.on(CharMatcher.WHITESPACE).splitToList(\"Foo bar baz\")\n    .stream()\n    .map(t -> new SubClass(t))\n    .collect(Collectors.toList());\n```\n\n/cc @kuangchen\n","closed_by":{"login":"scr","id":245840,"node_id":"MDQ6VXNlcjI0NTg0MA==","avatar_url":"https://avatars3.githubusercontent.com/u/245840?v=4","gravatar_id":"","url":"https://api.github.com/users/scr","html_url":"https://github.com/scr","followers_url":"https://api.github.com/users/scr/followers","following_url":"https://api.github.com/users/scr/following{/other_user}","gists_url":"https://api.github.com/users/scr/gists{/gist_id}","starred_url":"https://api.github.com/users/scr/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/scr/subscriptions","organizations_url":"https://api.github.com/users/scr/orgs","repos_url":"https://api.github.com/users/scr/repos","events_url":"https://api.github.com/users/scr/events{/privacy}","received_events_url":"https://api.github.com/users/scr/received_events","type":"User","site_admin":false}}", "commentIds":["132720132","133234880","133234916"], "labels":[]}