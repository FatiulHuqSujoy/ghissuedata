{"id":"1827", "title":"Multimaps.newListMultiMap(map, factory) does not work when factory supplies Doubles.asList(double[])", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1827) created by **geowerks** on 2014-08-05 at 03:36 PM_

---

If a factory supplies instances of Doubles.asList(double[]) to Multimaps.newListMultiMap(map, factory), and an EnumMap is used (I doubt this is relevant) calls to ListMultimap.get() return a  List&lt;Double>, but the list is never stored as it would be if the factory returned an ArrayList&lt;Double>

For example:

```
public static void main(String[] args) {

    Map<CaseFormat, Collection<Double>> map = Maps.newEnumMap(CaseFormat.class);
    final int size = 3;

    Supplier<List<Double>> factory = new Supplier<List<Double>>() {
        @Override public List<Double> get() {
            return Doubles.asList(new double[size]);
        }
    };

    ListMultimap<CaseFormat, Double> multimap = Multimaps.newListMultimap(map, factory);
    List<Double> list = multimap.get(CaseFormat.LOWER_UNDERSCORE);
    System.out.println(list);
    list.set(1, 5.0);
    System.out.println(list);
    System.out.println(multimap.get(CaseFormat.LOWER_UNDERSCORE));
    System.out.println(multimap);
}
```

produces:

[0.0, 0.0, 0.0]
[0.0, 5.0, 0.0]
[0.0, 0.0, 0.0]
{}
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1827","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1827/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1827/comments","events_url":"https://api.github.com/repos/google/guava/issues/1827/events","html_url":"https://github.com/google/guava/issues/1827","id":47426339,"node_id":"MDU6SXNzdWU0NzQyNjMzOQ==","number":1827,"title":"Multimaps.newListMultiMap(map, factory) does not work when factory supplies Doubles.asList(double[])","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143508181,"node_id":"MDU6TGFiZWwxNDM1MDgxODE=","url":"https://api.github.com/repos/google/guava/labels/status=working-as-intended","name":"status=working-as-intended","color":"c5c5c5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2014-10-31T18:00:31Z","updated_at":"2014-11-01T02:23:19Z","closed_at":"2014-11-01T02:23:19Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1827) created by **geowerks** on 2014-08-05 at 03:36 PM_\n\n---\n\nIf a factory supplies instances of Doubles.asList(double[]) to Multimaps.newListMultiMap(map, factory), and an EnumMap is used (I doubt this is relevant) calls to ListMultimap.get() return a  List&lt;Double>, but the list is never stored as it would be if the factory returned an ArrayList&lt;Double>\n\nFor example:\n\n```\npublic static void main(String[] args) {\n\n    Map<CaseFormat, Collection<Double>> map = Maps.newEnumMap(CaseFormat.class);\n    final int size = 3;\n\n    Supplier<List<Double>> factory = new Supplier<List<Double>>() {\n        @Override public List<Double> get() {\n            return Doubles.asList(new double[size]);\n        }\n    };\n\n    ListMultimap<CaseFormat, Double> multimap = Multimaps.newListMultimap(map, factory);\n    List<Double> list = multimap.get(CaseFormat.LOWER_UNDERSCORE);\n    System.out.println(list);\n    list.set(1, 5.0);\n    System.out.println(list);\n    System.out.println(multimap.get(CaseFormat.LOWER_UNDERSCORE));\n    System.out.println(multimap);\n}\n```\n\nproduces:\n\n[0.0, 0.0, 0.0]\n[0.0, 5.0, 0.0]\n[0.0, 0.0, 0.0]\n{}\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61355096"], "labels":["status=working-as-intended"]}