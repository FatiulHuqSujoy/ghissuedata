{"id":"2021", "title":"Maps.fromProperties() throws NPE if a value is null.", "body":"Maybe is intential.

I have that:

``` java
    Properties properties = new Properties();
    properties.put( \"key\" , null ); 
    // I know its impossible. It should show the problem
    // By me it happens because a Sytem.property value is null (with the time).
    // I´m not sure why?
    ...
    Map propertiesMap = Maps.fromProperties( properties );
```

Actual impl:

``` java
@GwtIncompatible(\"java.util.Properties\")
  public static ImmutableMap<String, String> fromProperties,    Properties properties) {
    ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();

    for (Enumeration<?> e = properties.propertyNames(); e.hasMoreElements();) {
      String key = (String) e.nextElement();
      builder.put(key, properties.getProperty(key));
    }

    return builder.build();
  }
```

The `properties.propertyNames()` is the problem. It throws a NPE.
The `properties.stringPropertyNames()` would be return a empty keySet(). 
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2021","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2021/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2021/comments","events_url":"https://api.github.com/repos/google/guava/issues/2021/events","html_url":"https://github.com/google/guava/issues/2021","id":65638553,"node_id":"MDU6SXNzdWU2NTYzODU1Mw==","number":2021,"title":"Maps.fromProperties() throws NPE if a value is null.","user":{"login":"jwausle","id":1932517,"node_id":"MDQ6VXNlcjE5MzI1MTc=","avatar_url":"https://avatars3.githubusercontent.com/u/1932517?v=4","gravatar_id":"","url":"https://api.github.com/users/jwausle","html_url":"https://github.com/jwausle","followers_url":"https://api.github.com/users/jwausle/followers","following_url":"https://api.github.com/users/jwausle/following{/other_user}","gists_url":"https://api.github.com/users/jwausle/gists{/gist_id}","starred_url":"https://api.github.com/users/jwausle/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jwausle/subscriptions","organizations_url":"https://api.github.com/users/jwausle/orgs","repos_url":"https://api.github.com/users/jwausle/repos","events_url":"https://api.github.com/users/jwausle/events{/privacy}","received_events_url":"https://api.github.com/users/jwausle/received_events","type":"User","site_admin":false},"labels":[{"id":143505259,"node_id":"MDU6TGFiZWwxNDM1MDUyNTk=","url":"https://api.github.com/repos/google/guava/labels/package=collect","name":"package=collect","color":"62a0e5","default":false},{"id":143508181,"node_id":"MDU6TGFiZWwxNDM1MDgxODE=","url":"https://api.github.com/repos/google/guava/labels/status=working-as-intended","name":"status=working-as-intended","color":"c5c5c5","default":false},{"id":143499130,"node_id":"MDU6TGFiZWwxNDM0OTkxMzA=","url":"https://api.github.com/repos/google/guava/labels/type=enhancement","name":"type=enhancement","color":"f4d75f","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-04-01T09:04:11Z","updated_at":"2015-05-20T19:10:31Z","closed_at":"2015-05-20T19:10:26Z","author_association":"NONE","body":"Maybe is intential.\n\nI have that:\n\n``` java\n    Properties properties = new Properties();\n    properties.put( \"key\" , null ); \n    // I know its impossible. It should show the problem\n    // By me it happens because a Sytem.property value is null (with the time).\n    // I´m not sure why?\n    ...\n    Map propertiesMap = Maps.fromProperties( properties );\n```\n\nActual impl:\n\n``` java\n@GwtIncompatible(\"java.util.Properties\")\n  public static ImmutableMap<String, String> fromProperties,    Properties properties) {\n    ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();\n\n    for (Enumeration<?> e = properties.propertyNames(); e.hasMoreElements();) {\n      String key = (String) e.nextElement();\n      builder.put(key, properties.getProperty(key));\n    }\n\n    return builder.build();\n  }\n```\n\nThe `properties.propertyNames()` is the problem. It throws a NPE.\nThe `properties.stringPropertyNames()` would be return a empty keySet(). \n","closed_by":{"login":"cgdecker","id":101568,"node_id":"MDQ6VXNlcjEwMTU2OA==","avatar_url":"https://avatars0.githubusercontent.com/u/101568?v=4","gravatar_id":"","url":"https://api.github.com/users/cgdecker","html_url":"https://github.com/cgdecker","followers_url":"https://api.github.com/users/cgdecker/followers","following_url":"https://api.github.com/users/cgdecker/following{/other_user}","gists_url":"https://api.github.com/users/cgdecker/gists{/gist_id}","starred_url":"https://api.github.com/users/cgdecker/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cgdecker/subscriptions","organizations_url":"https://api.github.com/users/cgdecker/orgs","repos_url":"https://api.github.com/users/cgdecker/repos","events_url":"https://api.github.com/users/cgdecker/events{/privacy}","received_events_url":"https://api.github.com/users/cgdecker/received_events","type":"User","site_admin":false}}", "commentIds":["103999506"], "labels":["package=collect","status=working-as-intended","type=enhancement"]}