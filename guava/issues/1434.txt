{"id":"1434", "title":"Joiner.skipNulls() does not skip nulls if they were returned by Object.toString()", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1434) created by **transparentpolitics** on 2013-05-30 at 04:41 AM_

---

In  public Joiner.skipNulls() this code:

```
    while (parts.hasNext()) {
      Object part = parts.next();
      if (part != null) {
        appendable.append(Joiner.this.toString(part));
        break;
      }
    }
    while (parts.hasNext()) {
      Object part = parts.next();
      if (part != null) {
        appendable.append(separator);
        appendable.append(Joiner.this.toString(part));
      }
    }
```

is wrong because it assumes that Joiner.this.toString(part) does not return null

In both loops there needs to be additional checks for Joiner.this.toString() returning a null
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1434","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1434/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1434/comments","events_url":"https://api.github.com/repos/google/guava/issues/1434/events","html_url":"https://github.com/google/guava/issues/1434","id":47424272,"node_id":"MDU6SXNzdWU0NzQyNDI3Mg==","number":1434,"title":"Joiner.skipNulls() does not skip nulls if they were returned by Object.toString()","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143505359,"node_id":"MDU6TGFiZWwxNDM1MDUzNTk=","url":"https://api.github.com/repos/google/guava/labels/package=base","name":"package=base","color":"62a0e5","default":false},{"id":143508181,"node_id":"MDU6TGFiZWwxNDM1MDgxODE=","url":"https://api.github.com/repos/google/guava/labels/status=working-as-intended","name":"status=working-as-intended","color":"c5c5c5","default":false},{"id":143499063,"node_id":"MDU6TGFiZWwxNDM0OTkwNjM=","url":"https://api.github.com/repos/google/guava/labels/type=defect","name":"type=defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2014-10-31T17:46:43Z","updated_at":"2014-11-01T01:28:24Z","closed_at":"2014-11-01T01:28:24Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1434) created by **transparentpolitics** on 2013-05-30 at 04:41 AM_\n\n---\n\nIn  public Joiner.skipNulls() this code:\n\n```\n    while (parts.hasNext()) {\n      Object part = parts.next();\n      if (part != null) {\n        appendable.append(Joiner.this.toString(part));\n        break;\n      }\n    }\n    while (parts.hasNext()) {\n      Object part = parts.next();\n      if (part != null) {\n        appendable.append(separator);\n        appendable.append(Joiner.this.toString(part));\n      }\n    }\n```\n\nis wrong because it assumes that Joiner.this.toString(part) does not return null\n\nIn both loops there needs to be additional checks for Joiner.this.toString() returning a null\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61352144","61352147","61352261","61352271","61352272"], "labels":["package=base","status=working-as-intended","type=defect"]}