{"id":"1266", "title":"Overload checkArgument() with versions that accept and return the argument", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1266) created by **dharkness** on 2013-01-23 at 08:50 PM_

---

This enhancement got lost in the debate over Ranges and Predicates in #﻿1038, but I feel it's a worthwhile addition. The page about preconditions even lists returning the argument from checkNotNull() as a reason for rolling your own preconditions.

If you want to use checkArgument() on a constructor argument when calling another constructor, you're currently forced to create a private helper method.

```
public Book(int price) {
    super(checkPrice(price));
}

private int checkPrice(int price) {
    checkArgument(price > 0);
    return price;
}
```

This isn't required with checkNotNull().

```
public Book(String title) {
    super(checkNotNull(title));
}
```

Overloading checkArgument() to accept the argument in addition to the boolean expression and return the argument would make the above more readable.

```
public Book(int price) {
    super(checkArgument(price, price > 0));
}
```

This would also make constructors with multiple preconditions read more clearly by putting the assignment and check on the same line, just as with checkNotNull().

This requires the same three methods with an additional (first) parameter.

```
public static <T> T checkArgument(T value, boolean expression) { ... }
... plus two more for passing a message ...
```

While that implementation would suffice and have the least surprise, it might be nice to alter the one that takes a message without any errorMessageArgs to pass the argument value to format() to avoid having to pass the argument twice.

```
public Book(int price) {
    super(checkArgument(price, price > 0, \"Price %s must be > 0\"));
}
```
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/1266","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/1266/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/1266/comments","events_url":"https://api.github.com/repos/google/guava/issues/1266/events","html_url":"https://github.com/google/guava/issues/1266","id":47423376,"node_id":"MDU6SXNzdWU0NzQyMzM3Ng==","number":1266,"title":"Overload checkArgument() with versions that accept and return the argument","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143505359,"node_id":"MDU6TGFiZWwxNDM1MDUzNTk=","url":"https://api.github.com/repos/google/guava/labels/package=base","name":"package=base","color":"62a0e5","default":false},{"id":143508181,"node_id":"MDU6TGFiZWwxNDM1MDgxODE=","url":"https://api.github.com/repos/google/guava/labels/status=working-as-intended","name":"status=working-as-intended","color":"c5c5c5","default":false},{"id":143505258,"node_id":"MDU6TGFiZWwxNDM1MDUyNTg=","url":"https://api.github.com/repos/google/guava/labels/type=addition","name":"type=addition","color":"f4d75f","default":false}],"state":"closed","locked":false,"assignee":{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false},"assignees":[{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false}],"milestone":null,"comments":10,"created_at":"2014-10-31T17:40:54Z","updated_at":"2018-06-13T20:09:59Z","closed_at":"2018-06-13T20:09:49Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=1266) created by **dharkness** on 2013-01-23 at 08:50 PM_\n\n---\n\nThis enhancement got lost in the debate over Ranges and Predicates in #﻿1038, but I feel it's a worthwhile addition. The page about preconditions even lists returning the argument from checkNotNull() as a reason for rolling your own preconditions.\n\nIf you want to use checkArgument() on a constructor argument when calling another constructor, you're currently forced to create a private helper method.\n\n```\npublic Book(int price) {\n    super(checkPrice(price));\n}\n\nprivate int checkPrice(int price) {\n    checkArgument(price > 0);\n    return price;\n}\n```\n\nThis isn't required with checkNotNull().\n\n```\npublic Book(String title) {\n    super(checkNotNull(title));\n}\n```\n\nOverloading checkArgument() to accept the argument in addition to the boolean expression and return the argument would make the above more readable.\n\n```\npublic Book(int price) {\n    super(checkArgument(price, price > 0));\n}\n```\n\nThis would also make constructors with multiple preconditions read more clearly by putting the assignment and check on the same line, just as with checkNotNull().\n\nThis requires the same three methods with an additional (first) parameter.\n\n```\npublic static <T> T checkArgument(T value, boolean expression) { ... }\n... plus two more for passing a message ...\n```\n\nWhile that implementation would suffice and have the least surprise, it might be nice to alter the one that takes a message without any errorMessageArgs to pass the argument value to format() to avoid having to pass the argument twice.\n\n```\npublic Book(int price) {\n    super(checkArgument(price, price > 0, \"Price %s must be > 0\"));\n}\n```\n","closed_by":{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false}}", "commentIds":["61350713","61350729","61350731","61351742","364483176","394144879","394215440","396651876","396666427","397069212"], "labels":["package=base","status=working-as-intended","type=addition"]}