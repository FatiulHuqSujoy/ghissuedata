{"id":"2860", "title":"Joiner withKeyValueSeparator: error when value is Array", "body":"I'm using Guava 20 because my runtime is fixed to Java7 (App Engine Standard)
```
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>20.0</version>
</dependency>
```

Here is a simple code
```
public static void main(String[] args) {
    Map<String, String[]> map = new HashMap<>();
    map.put(\"one\", new String[] {\"a\", \"b\"});
    map.put(\"two\", new String[] {\"c\", \"d\"});
    map.put(\"three\", new String[] {\"e\", \"f\"});

    String join = Joiner.on(\"\\n\").withKeyValueSeparator(\"/\").join(map);
    System.out.println(join);
}
```

And here is the output
```
one/[Ljava.lang.String;@53e25b76
two/[Ljava.lang.String;@73a8dfcc
three/[Ljava.lang.String;@ea30797
```

It seems that the `Arrays.toString()` method is not properly managed when an array need to be outputted


For the moment I managed the problem like this
```
Maps.EntryTransformer<String, String[], String> mapTransformer = new Maps.EntryTransformer<String, String[], String>() {
    @Override
    public String transformEntry(String key, String[] value) {
        return Arrays.toString(value);
    }
};

Map<String, String> map2 = Maps.transformEntries(map, mapTransformer);
String join = Joiner.on(\"\\n\").withKeyValueSeparator(\"/\").join(map2);
System.out.println(join);
```
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2860","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2860/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2860/comments","events_url":"https://api.github.com/repos/google/guava/issues/2860/events","html_url":"https://github.com/google/guava/issues/2860","id":239067872,"node_id":"MDU6SXNzdWUyMzkwNjc4NzI=","number":2860,"title":"Joiner withKeyValueSeparator: error when value is Array","user":{"login":"NicolaSpreafico","id":12278553,"node_id":"MDQ6VXNlcjEyMjc4NTUz","avatar_url":"https://avatars3.githubusercontent.com/u/12278553?v=4","gravatar_id":"","url":"https://api.github.com/users/NicolaSpreafico","html_url":"https://github.com/NicolaSpreafico","followers_url":"https://api.github.com/users/NicolaSpreafico/followers","following_url":"https://api.github.com/users/NicolaSpreafico/following{/other_user}","gists_url":"https://api.github.com/users/NicolaSpreafico/gists{/gist_id}","starred_url":"https://api.github.com/users/NicolaSpreafico/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/NicolaSpreafico/subscriptions","organizations_url":"https://api.github.com/users/NicolaSpreafico/orgs","repos_url":"https://api.github.com/users/NicolaSpreafico/repos","events_url":"https://api.github.com/users/NicolaSpreafico/events{/privacy}","received_events_url":"https://api.github.com/users/NicolaSpreafico/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-06-28T06:47:34Z","updated_at":"2017-06-29T19:38:03Z","closed_at":"2017-06-29T19:38:03Z","author_association":"NONE","body":"I'm using Guava 20 because my runtime is fixed to Java7 (App Engine Standard)\r\n```\r\n<dependency>\r\n    <groupId>com.google.guava</groupId>\r\n    <artifactId>guava</artifactId>\r\n    <version>20.0</version>\r\n</dependency>\r\n```\r\n\r\nHere is a simple code\r\n```\r\npublic static void main(String[] args) {\r\n    Map<String, String[]> map = new HashMap<>();\r\n    map.put(\"one\", new String[] {\"a\", \"b\"});\r\n    map.put(\"two\", new String[] {\"c\", \"d\"});\r\n    map.put(\"three\", new String[] {\"e\", \"f\"});\r\n\r\n    String join = Joiner.on(\"\\n\").withKeyValueSeparator(\"/\").join(map);\r\n    System.out.println(join);\r\n}\r\n```\r\n\r\nAnd here is the output\r\n```\r\none/[Ljava.lang.String;@53e25b76\r\ntwo/[Ljava.lang.String;@73a8dfcc\r\nthree/[Ljava.lang.String;@ea30797\r\n```\r\n\r\nIt seems that the `Arrays.toString()` method is not properly managed when an array need to be outputted\r\n\r\n\r\nFor the moment I managed the problem like this\r\n```\r\nMaps.EntryTransformer<String, String[], String> mapTransformer = new Maps.EntryTransformer<String, String[], String>() {\r\n    @Override\r\n    public String transformEntry(String key, String[] value) {\r\n        return Arrays.toString(value);\r\n    }\r\n};\r\n\r\nMap<String, String> map2 = Maps.transformEntries(map, mapTransformer);\r\nString join = Joiner.on(\"\\n\").withKeyValueSeparator(\"/\").join(map2);\r\nSystem.out.println(join);\r\n```\r\n","closed_by":{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false}}", "commentIds":["312047283","312079561"], "labels":[]}