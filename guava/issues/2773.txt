{"id":"2773", "title":"Issue while using method reference in Optional.transform in version 21.0", "body":"From an API I have a com.google.common.base.Optional object i.e. of the guava library and I am running my project against Java 8 version. Now, if I use transform(Optional.transform) method and use Java 8 syntax of method reference to implement a Function in transform method, it throws the error shown below.

### **Example code -**

```
package guava.optional;

import com.google.common.base.Function;
import com.google.common.base.Optional;

public class MethodReferenceIssue {

    public static void main(String[] args) {
        System.out.println(getValue());
    }
    static Optional<Long> getValue(){
        final Optional<Test> testOptional = Optional.of(new Test(2L));
        return testOptional.transform(Test::getCode);
    }
}

class Test {
    private long code;

    public Test(long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }
}
```
```
Error

error: method transform in class Optional<T> cannot be applied to given types;
[ERROR] ode
[ERROR] reason: cannot infer type-variable(s) V
[ERROR] (argument mismatch; invalid method reference
[ERROR] method getCode in class Test cannot be applied to given types
[ERROR] required: no arguments
[ERROR] found: ? super Test 
[ERROR] reason: actual and formal argument lists differ in length)
[ERROR] where V,T are type-variables:
[ERROR] V extends Object declared in method <V>transform(Function<? super T,V>)
[ERROR] T extends Object declared in class Optional
```

Now I have solved this for now by creating a variable of type Function and then pass it as a parameter then it works fine.

### **Example code -**

```
package guava.optional;

import com.google.common.base.Function;
import com.google.common.base.Optional;


public class MethodReferenceIssue {

    public static void main(String[] args) {
        System.out.println(getValue());
    }
    static Optional<Long> getValue(){
        final Optional<Test> testOptional = Optional.of(new Test(2L));
        Function<Test, Long> testOpt = Test::getCode;
        return testOptional.transform(testOpt);
    }
}

class Test {
    private long code;

    public Test(long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }
}
```

**NOTE :-** this occurs with Guava version 21.0 and it works fine with guava version 20.0", "json":"{"url":"https://api.github.com/repos/google/guava/issues/2773","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/2773/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/2773/comments","events_url":"https://api.github.com/repos/google/guava/issues/2773/events","html_url":"https://github.com/google/guava/issues/2773","id":217464308,"node_id":"MDU6SXNzdWUyMTc0NjQzMDg=","number":2773,"title":"Issue while using method reference in Optional.transform in version 21.0","user":{"login":"HiteshGarg","id":8066310,"node_id":"MDQ6VXNlcjgwNjYzMTA=","avatar_url":"https://avatars3.githubusercontent.com/u/8066310?v=4","gravatar_id":"","url":"https://api.github.com/users/HiteshGarg","html_url":"https://github.com/HiteshGarg","followers_url":"https://api.github.com/users/HiteshGarg/followers","following_url":"https://api.github.com/users/HiteshGarg/following{/other_user}","gists_url":"https://api.github.com/users/HiteshGarg/gists{/gist_id}","starred_url":"https://api.github.com/users/HiteshGarg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/HiteshGarg/subscriptions","organizations_url":"https://api.github.com/users/HiteshGarg/orgs","repos_url":"https://api.github.com/users/HiteshGarg/repos","events_url":"https://api.github.com/users/HiteshGarg/events{/privacy}","received_events_url":"https://api.github.com/users/HiteshGarg/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2017-03-28T06:53:54Z","updated_at":"2017-04-09T11:59:10Z","closed_at":"2017-03-28T16:02:39Z","author_association":"NONE","body":"From an API I have a com.google.common.base.Optional object i.e. of the guava library and I am running my project against Java 8 version. Now, if I use transform(Optional.transform) method and use Java 8 syntax of method reference to implement a Function in transform method, it throws the error shown below.\r\n\r\n### **Example code -**\r\n\r\n```\r\npackage guava.optional;\r\n\r\nimport com.google.common.base.Function;\r\nimport com.google.common.base.Optional;\r\n\r\npublic class MethodReferenceIssue {\r\n\r\n    public static void main(String[] args) {\r\n        System.out.println(getValue());\r\n    }\r\n    static Optional<Long> getValue(){\r\n        final Optional<Test> testOptional = Optional.of(new Test(2L));\r\n        return testOptional.transform(Test::getCode);\r\n    }\r\n}\r\n\r\nclass Test {\r\n    private long code;\r\n\r\n    public Test(long code) {\r\n        this.code = code;\r\n    }\r\n\r\n    public Long getCode() {\r\n        return code;\r\n    }\r\n}\r\n```\r\n```\r\nError\r\n\r\nerror: method transform in class Optional<T> cannot be applied to given types;\r\n[ERROR] ode\r\n[ERROR] reason: cannot infer type-variable(s) V\r\n[ERROR] (argument mismatch; invalid method reference\r\n[ERROR] method getCode in class Test cannot be applied to given types\r\n[ERROR] required: no arguments\r\n[ERROR] found: ? super Test \r\n[ERROR] reason: actual and formal argument lists differ in length)\r\n[ERROR] where V,T are type-variables:\r\n[ERROR] V extends Object declared in method <V>transform(Function<? super T,V>)\r\n[ERROR] T extends Object declared in class Optional\r\n```\r\n\r\nNow I have solved this for now by creating a variable of type Function and then pass it as a parameter then it works fine.\r\n\r\n### **Example code -**\r\n\r\n```\r\npackage guava.optional;\r\n\r\nimport com.google.common.base.Function;\r\nimport com.google.common.base.Optional;\r\n\r\n\r\npublic class MethodReferenceIssue {\r\n\r\n    public static void main(String[] args) {\r\n        System.out.println(getValue());\r\n    }\r\n    static Optional<Long> getValue(){\r\n        final Optional<Test> testOptional = Optional.of(new Test(2L));\r\n        Function<Test, Long> testOpt = Test::getCode;\r\n        return testOptional.transform(testOpt);\r\n    }\r\n}\r\n\r\nclass Test {\r\n    private long code;\r\n\r\n    public Test(long code) {\r\n        this.code = code;\r\n    }\r\n\r\n    public Long getCode() {\r\n        return code;\r\n    }\r\n}\r\n```\r\n\r\n**NOTE :-** this occurs with Guava version 21.0 and it works fine with guava version 20.0","closed_by":{"login":"kevinb9n","id":934551,"node_id":"MDQ6VXNlcjkzNDU1MQ==","avatar_url":"https://avatars2.githubusercontent.com/u/934551?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinb9n","html_url":"https://github.com/kevinb9n","followers_url":"https://api.github.com/users/kevinb9n/followers","following_url":"https://api.github.com/users/kevinb9n/following{/other_user}","gists_url":"https://api.github.com/users/kevinb9n/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinb9n/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinb9n/subscriptions","organizations_url":"https://api.github.com/users/kevinb9n/orgs","repos_url":"https://api.github.com/users/kevinb9n/repos","events_url":"https://api.github.com/users/kevinb9n/events{/privacy}","received_events_url":"https://api.github.com/users/kevinb9n/received_events","type":"User","site_admin":false}}", "commentIds":["289818743","289996158","290186788","290311867","292781424"], "labels":[]}