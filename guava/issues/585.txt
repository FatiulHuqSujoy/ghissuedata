{"id":"585", "title":"Feature: Iterables.once()", "body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=585) created by **t...@duh.org** on 2011-03-30 at 01:53 PM_

---

Sometimes, it's useful to provide an Iterable object that traverses a known Iterator only once, without writing the boilerplate wrapper for it. This makes it easier to write loops or return Iterable instances from methods in some cases.

I have created the following static utility method, contributed as public domain. The formatting might not match project standards, so reformat as you see fit.

```
/**
 * Provides an {@link Iterable} object that can be iterated only once, by
 * returning the provided iterator. Subsequent calls to
 * {@link Iterable#iterator()} will return an empty iterator.
 * 
 * @param <T>
 *            type of elements
 * @param iterator
 *            first iterator to return
 * @return iterable wrapper
 */
public static <T> Iterable<T> once(final Iterator<T> iterator) {
    return new Iterable<T>() {
        private Iterator<T> iter = iterator;

        @Override
        public Iterator<T> iterator() {
            if (iter == null)
                return Iterators.emptyIterator();

            Iterator<T> val = iter;
            iter = null;
            return val;
        }
    };
}
```
", "json":"{"url":"https://api.github.com/repos/google/guava/issues/585","repository_url":"https://api.github.com/repos/google/guava","labels_url":"https://api.github.com/repos/google/guava/issues/585/labels{/name}","comments_url":"https://api.github.com/repos/google/guava/issues/585/comments","events_url":"https://api.github.com/repos/google/guava/issues/585/events","html_url":"https://github.com/google/guava/issues/585","id":47420002,"node_id":"MDU6SXNzdWU0NzQyMDAwMg==","number":585,"title":"Feature: Iterables.once()","user":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false},"labels":[{"id":143514951,"node_id":"MDU6TGFiZWwxNDM1MTQ5NTE=","url":"https://api.github.com/repos/google/guava/labels/status=will-not-fix","name":"status=will-not-fix","color":"c5c5c5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2014-10-31T17:16:58Z","updated_at":"2017-01-26T19:42:49Z","closed_at":"2014-10-31T19:10:08Z","author_association":"NONE","body":"_[Original issue](https://code.google.com/p/guava-libraries/issues/detail?id=585) created by **t...@duh.org** on 2011-03-30 at 01:53 PM_\n\n---\n\nSometimes, it's useful to provide an Iterable object that traverses a known Iterator only once, without writing the boilerplate wrapper for it. This makes it easier to write loops or return Iterable instances from methods in some cases.\n\nI have created the following static utility method, contributed as public domain. The formatting might not match project standards, so reformat as you see fit.\n\n```\n/**\n * Provides an {@link Iterable} object that can be iterated only once, by\n * returning the provided iterator. Subsequent calls to\n * {@link Iterable#iterator()} will return an empty iterator.\n * \n * @param <T>\n *            type of elements\n * @param iterator\n *            first iterator to return\n * @return iterable wrapper\n */\npublic static <T> Iterable<T> once(final Iterator<T> iterator) {\n    return new Iterable<T>() {\n        private Iterator<T> iter = iterator;\n\n        @Override\n        public Iterator<T> iterator() {\n            if (iter == null)\n                return Iterators.emptyIterator();\n\n            Iterator<T> val = iter;\n            iter = null;\n            return val;\n        }\n    };\n}\n```\n","closed_by":{"login":"gissuebot","id":8091570,"node_id":"MDQ6VXNlcjgwOTE1NzA=","avatar_url":"https://avatars0.githubusercontent.com/u/8091570?v=4","gravatar_id":"","url":"https://api.github.com/users/gissuebot","html_url":"https://github.com/gissuebot","followers_url":"https://api.github.com/users/gissuebot/followers","following_url":"https://api.github.com/users/gissuebot/following{/other_user}","gists_url":"https://api.github.com/users/gissuebot/gists{/gist_id}","starred_url":"https://api.github.com/users/gissuebot/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/gissuebot/subscriptions","organizations_url":"https://api.github.com/users/gissuebot/orgs","repos_url":"https://api.github.com/users/gissuebot/repos","events_url":"https://api.github.com/users/gissuebot/events{/privacy}","received_events_url":"https://api.github.com/users/gissuebot/received_events","type":"User","site_admin":false}}", "commentIds":["61313665","61313785"], "labels":["status=will-not-fix"]}