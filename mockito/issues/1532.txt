{"id":"1532", "title":"Nested spies cause memory leaks ", "body":"Seems like nested spies can cause memory leaks since such objects are kept in memory without purging. Not sure if it can be resolved at all. Should it be avoided? Is there a mention about this in docs? Anyway, the code speaks better and fortunately I’ve been able to create a self-contained sample.

BTW I can provide a `.hprof` file if you are interested.

#### Versions

```
org.mockito:mockito-core:2.22.0
org.mockito:mockito-inline:2.22.0
```
```
java version \"1.8.0_181\"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```

#### Gradle

Heap is set to 64 MB.

```groovy
tasks.withType<Test> {
    maxHeapSize = \"64m\"
    jvmArgs(\"-XX:+HeapDumpOnOutOfMemoryError\")

    failFast = true
}
```
```
$ ./gradlew :module:cleanTestDebugUnitTest :module:testDebugUnitTest --tests \"com.github.sample.NestedSpiesMemoryLeakSpec\"
```

#### Code

```kotlin
package com.github.sample

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.functions.Consumer
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(JUnitPlatform::class)
class NestedSpiesMemoryLeakSpec : Spek({

    repeat(10_000) { iteration ->

        it(\"iteration [$iteration]\") {
            Mockito.spy(Service())
        }

    }

}) {

    class Service {
        // Remove Mockito.spy and OOM disappears.
        val value = Mockito.spy(Consumer<Int> {
            // This closure keeps a reference to Service.
            streams.size
        })

        // See at as a mass to fill the RAM.
        val streams = (0..1_000).map { BehaviorRelay.create<Int>() }
    }

}
```
```
> Task :module:testDebugUnitTest
java.lang.OutOfMemoryError: GC overhead limit exceeded
Dumping heap to java_pid23350.hprof ...
Heap dump file created [99857779 bytes in 0.356 secs]

com.github.sample.NestedSpiesMemoryLeakSpec > it iteration [187] STANDARD_ERROR
    java.lang.OutOfMemoryError: GC overhead limit exceeded
    	at com.jakewharton.rxrelay2.BehaviorRelay.<init>(BehaviorRelay.java:99)
    	at com.jakewharton.rxrelay2.BehaviorRelay.create(BehaviorRelay.java:77)
    	at com.github.sample.NestedSpiesMemoryLeakSpec$Service.<init>(NestedSpiesMemoryLeakSpec.kt:32)
    	at com.github.sample.NestedSpiesMemoryLeakSpec$1$1$1.invoke(NestedSpiesMemoryLeakSpec.kt:17)
    	at com.github.sample.NestedSpiesMemoryLeakSpec$1$1$1.invoke(NestedSpiesMemoryLeakSpec.kt:12)
    	at org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:102)
    	at org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:80)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:105)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/547193480.execute(Unknown Source)
    	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)

com.github.sample.NestedSpiesMemoryLeakSpec > it iteration [187] FAILED
    java.lang.OutOfMemoryError

> Task :module:testDebugUnitTest FAILED
```

#### Eclipse Memory Analyzer

<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 09 53\" src=\"https://user-images.githubusercontent.com/200401/48495384-e574fb00-e840-11e8-886e-c9ae00dcf291.png\">
<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 08 59\" src=\"https://user-images.githubusercontent.com/200401/48495395-ec9c0900-e840-11e8-9b02-cec1c8db43c0.png\">
<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 09 09\" src=\"https://user-images.githubusercontent.com/200401/48495404-f291ea00-e840-11e8-8c5f-2680047c6947.png\">

---

Seems like this happens:

* `Service` is a spy.
* `Service` contains a `Consumer`, it is a `spy` as well.
* `Consumer` is a closure and has an implicit reference to `Service`.
* Mockito keeps both spies and cannot remove them from memory since there is a cross-reference (I guess).


", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1532","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1532/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1532/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1532/events","html_url":"https://github.com/mockito/mockito/issues/1532","id":380776274,"node_id":"MDU6SXNzdWUzODA3NzYyNzQ=","number":1532,"title":"Nested spies cause memory leaks ","user":{"login":"ming13","id":200401,"node_id":"MDQ6VXNlcjIwMDQwMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/200401?v=4","gravatar_id":"","url":"https://api.github.com/users/ming13","html_url":"https://github.com/ming13","followers_url":"https://api.github.com/users/ming13/followers","following_url":"https://api.github.com/users/ming13/following{/other_user}","gists_url":"https://api.github.com/users/ming13/gists{/gist_id}","starred_url":"https://api.github.com/users/ming13/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ming13/subscriptions","organizations_url":"https://api.github.com/users/ming13/orgs","repos_url":"https://api.github.com/users/ming13/repos","events_url":"https://api.github.com/users/ming13/events{/privacy}","received_events_url":"https://api.github.com/users/ming13/received_events","type":"User","site_admin":false},"labels":[{"id":16375483,"node_id":"MDU6TGFiZWwxNjM3NTQ4Mw==","url":"https://api.github.com/repos/mockito/mockito/labels/bug","name":"bug","color":"fc2929","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-11-14T16:20:13Z","updated_at":"2019-04-18T09:32:45Z","closed_at":"2019-03-05T16:10:18Z","author_association":"NONE","body":"Seems like nested spies can cause memory leaks since such objects are kept in memory without purging. Not sure if it can be resolved at all. Should it be avoided? Is there a mention about this in docs? Anyway, the code speaks better and fortunately I’ve been able to create a self-contained sample.\r\n\r\nBTW I can provide a `.hprof` file if you are interested.\r\n\r\n#### Versions\r\n\r\n```\r\norg.mockito:mockito-core:2.22.0\r\norg.mockito:mockito-inline:2.22.0\r\n```\r\n```\r\njava version \"1.8.0_181\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_181-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)\r\n```\r\n\r\n#### Gradle\r\n\r\nHeap is set to 64 MB.\r\n\r\n```groovy\r\ntasks.withType<Test> {\r\n    maxHeapSize = \"64m\"\r\n    jvmArgs(\"-XX:+HeapDumpOnOutOfMemoryError\")\r\n\r\n    failFast = true\r\n}\r\n```\r\n```\r\n$ ./gradlew :module:cleanTestDebugUnitTest :module:testDebugUnitTest --tests \"com.github.sample.NestedSpiesMemoryLeakSpec\"\r\n```\r\n\r\n#### Code\r\n\r\n```kotlin\r\npackage com.github.sample\r\n\r\nimport com.jakewharton.rxrelay2.BehaviorRelay\r\nimport io.reactivex.functions.Consumer\r\nimport org.jetbrains.spek.api.Spek\r\nimport org.jetbrains.spek.api.dsl.it\r\nimport org.junit.platform.runner.JUnitPlatform\r\nimport org.junit.runner.RunWith\r\nimport org.mockito.Mockito\r\n\r\n@RunWith(JUnitPlatform::class)\r\nclass NestedSpiesMemoryLeakSpec : Spek({\r\n\r\n    repeat(10_000) { iteration ->\r\n\r\n        it(\"iteration [$iteration]\") {\r\n            Mockito.spy(Service())\r\n        }\r\n\r\n    }\r\n\r\n}) {\r\n\r\n    class Service {\r\n        // Remove Mockito.spy and OOM disappears.\r\n        val value = Mockito.spy(Consumer<Int> {\r\n            // This closure keeps a reference to Service.\r\n            streams.size\r\n        })\r\n\r\n        // See at as a mass to fill the RAM.\r\n        val streams = (0..1_000).map { BehaviorRelay.create<Int>() }\r\n    }\r\n\r\n}\r\n```\r\n```\r\n> Task :module:testDebugUnitTest\r\njava.lang.OutOfMemoryError: GC overhead limit exceeded\r\nDumping heap to java_pid23350.hprof ...\r\nHeap dump file created [99857779 bytes in 0.356 secs]\r\n\r\ncom.github.sample.NestedSpiesMemoryLeakSpec > it iteration [187] STANDARD_ERROR\r\n    java.lang.OutOfMemoryError: GC overhead limit exceeded\r\n    \tat com.jakewharton.rxrelay2.BehaviorRelay.<init>(BehaviorRelay.java:99)\r\n    \tat com.jakewharton.rxrelay2.BehaviorRelay.create(BehaviorRelay.java:77)\r\n    \tat com.github.sample.NestedSpiesMemoryLeakSpec$Service.<init>(NestedSpiesMemoryLeakSpec.kt:32)\r\n    \tat com.github.sample.NestedSpiesMemoryLeakSpec$1$1$1.invoke(NestedSpiesMemoryLeakSpec.kt:17)\r\n    \tat com.github.sample.NestedSpiesMemoryLeakSpec$1$1$1.invoke(NestedSpiesMemoryLeakSpec.kt:12)\r\n    \tat org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:102)\r\n    \tat org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:80)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:105)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/547193480.execute(Unknown Source)\r\n    \tat org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)\r\n\r\ncom.github.sample.NestedSpiesMemoryLeakSpec > it iteration [187] FAILED\r\n    java.lang.OutOfMemoryError\r\n\r\n> Task :module:testDebugUnitTest FAILED\r\n```\r\n\r\n#### Eclipse Memory Analyzer\r\n\r\n<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 09 53\" src=\"https://user-images.githubusercontent.com/200401/48495384-e574fb00-e840-11e8-886e-c9ae00dcf291.png\">\r\n<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 08 59\" src=\"https://user-images.githubusercontent.com/200401/48495395-ec9c0900-e840-11e8-9b02-cec1c8db43c0.png\">\r\n<img width=\"1238\" alt=\"screen shot 2018-11-14 at 19 09 09\" src=\"https://user-images.githubusercontent.com/200401/48495404-f291ea00-e840-11e8-8c5f-2680047c6947.png\">\r\n\r\n---\r\n\r\nSeems like this happens:\r\n\r\n* `Service` is a spy.\r\n* `Service` contains a `Consumer`, it is a `spy` as well.\r\n* `Consumer` is a closure and has an implicit reference to `Service`.\r\n* Mockito keeps both spies and cannot remove them from memory since there is a cross-reference (I guess).\r\n\r\n\r\n","closed_by":{"login":"mockitoguy","id":24743,"node_id":"MDQ6VXNlcjI0NzQz","avatar_url":"https://avatars2.githubusercontent.com/u/24743?v=4","gravatar_id":"","url":"https://api.github.com/users/mockitoguy","html_url":"https://github.com/mockitoguy","followers_url":"https://api.github.com/users/mockitoguy/followers","following_url":"https://api.github.com/users/mockitoguy/following{/other_user}","gists_url":"https://api.github.com/users/mockitoguy/gists{/gist_id}","starred_url":"https://api.github.com/users/mockitoguy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mockitoguy/subscriptions","organizations_url":"https://api.github.com/users/mockitoguy/orgs","repos_url":"https://api.github.com/users/mockitoguy/repos","events_url":"https://api.github.com/users/mockitoguy/events{/privacy}","received_events_url":"https://api.github.com/users/mockitoguy/received_events","type":"User","site_admin":false}}", "commentIds":["455433713","455662380","455707387","455746067","455786133","484426523"], "labels":["bug"]}