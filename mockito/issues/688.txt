{"id":"688", "title":"Enabled mocking interface clone method", "body":"Mocking a `clone()` method on an interface appears to no longer be working with **Mockito 2.1.0**. The below code works fine with **Mockito 1.10.19** but throws an `IllegalAccessError` with **Mockito 2.1.0**:

```
public interface CloneableInterface extends Cloneable {
   CloneableInterface clone();
}

public class CloneableInterfaceTest {

   @Test
   public void test() {      
      CloneableInterface i = Mockito.mock(CloneableInterface.class);
      Mockito.when(i.clone()).thenReturn(i); // Throws IllegalAccessError
   }
}
```

Result of running above test:

```
java.lang.IllegalAccessError: CloneableInterface$MockitoMock$833899610.clone()LCloneableInterface;
```

Environment: **JDK 1.8.0_102, JUnit 4.11, Mockito 2.1.0, Gradle 3.0, Windows 10**

EDIT: Link to [SO post](http://stackoverflow.com/questions/39967723/mocking-interface-clone-method)
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/688","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/688/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/688/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/688/events","html_url":"https://github.com/mockito/mockito/issues/688","id":182284472,"node_id":"MDU6SXNzdWUxODIyODQ0NzI=","number":688,"title":"Enabled mocking interface clone method","user":{"login":"jpenglert","id":1364490,"node_id":"MDQ6VXNlcjEzNjQ0OTA=","avatar_url":"https://avatars3.githubusercontent.com/u/1364490?v=4","gravatar_id":"","url":"https://api.github.com/users/jpenglert","html_url":"https://github.com/jpenglert","followers_url":"https://api.github.com/users/jpenglert/followers","following_url":"https://api.github.com/users/jpenglert/following{/other_user}","gists_url":"https://api.github.com/users/jpenglert/gists{/gist_id}","starred_url":"https://api.github.com/users/jpenglert/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jpenglert/subscriptions","organizations_url":"https://api.github.com/users/jpenglert/orgs","repos_url":"https://api.github.com/users/jpenglert/repos","events_url":"https://api.github.com/users/jpenglert/events{/privacy}","received_events_url":"https://api.github.com/users/jpenglert/received_events","type":"User","site_admin":false},"labels":[{"id":16375483,"node_id":"MDU6TGFiZWwxNjM3NTQ4Mw==","url":"https://api.github.com/repos/mockito/mockito/labels/bug","name":"bug","color":"fc2929","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":11,"created_at":"2016-10-11T14:48:16Z","updated_at":"2017-03-01T22:29:37Z","closed_at":"2016-10-11T18:12:25Z","author_association":"CONTRIBUTOR","body":"Mocking a `clone()` method on an interface appears to no longer be working with **Mockito 2.1.0**. The below code works fine with **Mockito 1.10.19** but throws an `IllegalAccessError` with **Mockito 2.1.0**:\n\n```\npublic interface CloneableInterface extends Cloneable {\n   CloneableInterface clone();\n}\n\npublic class CloneableInterfaceTest {\n\n   @Test\n   public void test() {      \n      CloneableInterface i = Mockito.mock(CloneableInterface.class);\n      Mockito.when(i.clone()).thenReturn(i); // Throws IllegalAccessError\n   }\n}\n```\n\nResult of running above test:\n\n```\njava.lang.IllegalAccessError: CloneableInterface$MockitoMock$833899610.clone()LCloneableInterface;\n```\n\nEnvironment: **JDK 1.8.0_102, JUnit 4.11, Mockito 2.1.0, Gradle 3.0, Windows 10**\n\nEDIT: Link to [SO post](http://stackoverflow.com/questions/39967723/mocking-interface-clone-method)\n","closed_by":{"login":"TimvdLippe","id":5948271,"node_id":"MDQ6VXNlcjU5NDgyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/5948271?v=4","gravatar_id":"","url":"https://api.github.com/users/TimvdLippe","html_url":"https://github.com/TimvdLippe","followers_url":"https://api.github.com/users/TimvdLippe/followers","following_url":"https://api.github.com/users/TimvdLippe/following{/other_user}","gists_url":"https://api.github.com/users/TimvdLippe/gists{/gist_id}","starred_url":"https://api.github.com/users/TimvdLippe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TimvdLippe/subscriptions","organizations_url":"https://api.github.com/users/TimvdLippe/orgs","repos_url":"https://api.github.com/users/TimvdLippe/repos","events_url":"https://api.github.com/users/TimvdLippe/events{/privacy}","received_events_url":"https://api.github.com/users/TimvdLippe/received_events","type":"User","site_admin":false}}", "commentIds":["252948202","252949988","252998483","253238847","253403180","253459347","283462301","283469722","283478339","283478862","283492201"], "labels":["bug"]}