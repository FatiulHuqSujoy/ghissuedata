{"id":"877", "title":"No way for user to tell Mockito about matchers that take matchers", "body":"I was trying to write a new Matcher that would take a lambda, and a matcher. This might sounds familiar because I am trying to create a PropertyMatcher for Mockito with lambdas. I made the following matcher.

```
public class PropertyMatcher<T, S> extends ArgumentMatcher<T> {

    @NonNull private final Func1<T, S> map;
    @NonNull private final S matcher;

    @SuppressWarnings(\"unchecked\")
    public static <T,S> T propertyThat(@NonNull Func1<T, S> map, @NonNull S matcher) {
        return Matchers.argThat(new PropertyMatcher<>(map, matcher));
    }

    public PropertyMatcher(@NonNull Func1<T, S> map, @NonNull S matcher) {
        this.map = map;
        this.matcher = matcher;
    }

    @SuppressWarnings(\"unchecked\")
    @Override
    public boolean matches(Object item) {
        try {
            // pretty sure this won't work because mockito doesn't work like hamcrest
            Matcher<S> m = (Matcher<S>) matcher;
            return m.matches(map.call((T) item));
        } catch (ClassCastException cce) {
            return matcher.equals(map.call((T) item));
        }
    }

    @Override
    public void describeTo(Description description) {
    }
}
```

Now this works ONLY if you use nothing but values. So a working example would be
```
verify(managerMock).manage(propertyThat(Request::id, 1)); // ok
```
However once I start to use a matcher, I get an error saying that I am mixing matchers and real values.
```
verify(managerMock).manage(propertyThat(Request::id, eq(1))); // error
```
This is because the argument matcher storage has no idea I am a matcher taking matchers. All of that is private so I don't really have a way to get to it (without reflection). That is only the part I know I would need to fix. I don't know the implementation all that well so I don't know if there is something else that would be required. Is there any expectation that this could be done? Should I just not even bother with this and just make a matcher that is a lambda with the checks? I will admit that I am still on version 1.10.19. When I went to figure this out I was quite surprised that 2 had been release. I haven't tried it yet, but I don't know if Android's sorta Java 8 would work with Mockito 2 anyway.", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/877","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/877/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/877/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/877/events","html_url":"https://github.com/mockito/mockito/issues/877","id":200274530,"node_id":"MDU6SXNzdWUyMDAyNzQ1MzA=","number":877,"title":"No way for user to tell Mockito about matchers that take matchers","user":{"login":"dmstocking","id":102381,"node_id":"MDQ6VXNlcjEwMjM4MQ==","avatar_url":"https://avatars0.githubusercontent.com/u/102381?v=4","gravatar_id":"","url":"https://api.github.com/users/dmstocking","html_url":"https://github.com/dmstocking","followers_url":"https://api.github.com/users/dmstocking/followers","following_url":"https://api.github.com/users/dmstocking/following{/other_user}","gists_url":"https://api.github.com/users/dmstocking/gists{/gist_id}","starred_url":"https://api.github.com/users/dmstocking/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dmstocking/subscriptions","organizations_url":"https://api.github.com/users/dmstocking/orgs","repos_url":"https://api.github.com/users/dmstocking/repos","events_url":"https://api.github.com/users/dmstocking/events{/privacy}","received_events_url":"https://api.github.com/users/dmstocking/received_events","type":"User","site_admin":false},"labels":[{"id":16375487,"node_id":"MDU6TGFiZWwxNjM3NTQ4Nw==","url":"https://api.github.com/repos/mockito/mockito/labels/question","name":"question","color":"cc317c","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-01-12T04:11:58Z","updated_at":"2017-01-13T10:39:57Z","closed_at":"2017-01-12T16:28:06Z","author_association":"NONE","body":"I was trying to write a new Matcher that would take a lambda, and a matcher. This might sounds familiar because I am trying to create a PropertyMatcher for Mockito with lambdas. I made the following matcher.\r\n\r\n```\r\npublic class PropertyMatcher<T, S> extends ArgumentMatcher<T> {\r\n\r\n    @NonNull private final Func1<T, S> map;\r\n    @NonNull private final S matcher;\r\n\r\n    @SuppressWarnings(\"unchecked\")\r\n    public static <T,S> T propertyThat(@NonNull Func1<T, S> map, @NonNull S matcher) {\r\n        return Matchers.argThat(new PropertyMatcher<>(map, matcher));\r\n    }\r\n\r\n    public PropertyMatcher(@NonNull Func1<T, S> map, @NonNull S matcher) {\r\n        this.map = map;\r\n        this.matcher = matcher;\r\n    }\r\n\r\n    @SuppressWarnings(\"unchecked\")\r\n    @Override\r\n    public boolean matches(Object item) {\r\n        try {\r\n            // pretty sure this won't work because mockito doesn't work like hamcrest\r\n            Matcher<S> m = (Matcher<S>) matcher;\r\n            return m.matches(map.call((T) item));\r\n        } catch (ClassCastException cce) {\r\n            return matcher.equals(map.call((T) item));\r\n        }\r\n    }\r\n\r\n    @Override\r\n    public void describeTo(Description description) {\r\n    }\r\n}\r\n```\r\n\r\nNow this works ONLY if you use nothing but values. So a working example would be\r\n```\r\nverify(managerMock).manage(propertyThat(Request::id, 1)); // ok\r\n```\r\nHowever once I start to use a matcher, I get an error saying that I am mixing matchers and real values.\r\n```\r\nverify(managerMock).manage(propertyThat(Request::id, eq(1))); // error\r\n```\r\nThis is because the argument matcher storage has no idea I am a matcher taking matchers. All of that is private so I don't really have a way to get to it (without reflection). That is only the part I know I would need to fix. I don't know the implementation all that well so I don't know if there is something else that would be required. Is there any expectation that this could be done? Should I just not even bother with this and just make a matcher that is a lambda with the checks? I will admit that I am still on version 1.10.19. When I went to figure this out I was quite surprised that 2 had been release. I haven't tried it yet, but I don't know if Android's sorta Java 8 would work with Mockito 2 anyway.","closed_by":{"login":"dmstocking","id":102381,"node_id":"MDQ6VXNlcjEwMjM4MQ==","avatar_url":"https://avatars0.githubusercontent.com/u/102381?v=4","gravatar_id":"","url":"https://api.github.com/users/dmstocking","html_url":"https://github.com/dmstocking","followers_url":"https://api.github.com/users/dmstocking/followers","following_url":"https://api.github.com/users/dmstocking/following{/other_user}","gists_url":"https://api.github.com/users/dmstocking/gists{/gist_id}","starred_url":"https://api.github.com/users/dmstocking/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dmstocking/subscriptions","organizations_url":"https://api.github.com/users/dmstocking/orgs","repos_url":"https://api.github.com/users/dmstocking/repos","events_url":"https://api.github.com/users/dmstocking/events{/privacy}","received_events_url":"https://api.github.com/users/dmstocking/received_events","type":"User","site_admin":false}}", "commentIds":["272108540","272210220","272413148"], "labels":["question"]}