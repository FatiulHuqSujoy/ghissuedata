{"id":"961", "title":"Let java.lang.StackOverflowError surface", "body":"`mockito-core-1.10.19`

`Spring Boot 1.4.4.RELEASE`
```
java -version
java version \"1.8.0_121\"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)
```
```
Spring Tool Suite 

Version: 3.8.3.RELEASE
Build Id: 201612191351
Platform: Eclipse Neon.2 (4.6.2)
```

Hello,

I did an infinite recursive call by accident inside the `thenReturn()` which gave me the output bellow. IMO you shouldn't catch `java.lang.StackOverflowError` if that is possible, let it surface so I can locate the mistake.

```
org.mockito.exceptions.misusing.UnfinishedStubbingException: 
Unfinished stubbing detected here:
-> at my.package.MyServiceImplTest.testMyMethod(MyServiceImplTest.java:173)

E.g. thenReturn() may be missing.
Examples of correct stubbing:
    when(mock.isOk()).thenReturn(true);
    when(mock.isOk()).thenThrow(exception);
    doThrow(exception).when(mock).someVoidMethod();
Hints:
 1. missing thenReturn()
 2. you are trying to stub a final method, you naughty developer!
 3: you are stubbing the behaviour of another mock inside before 'thenReturn' instruction if completed

	at org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.resetMocks(ResetMocksTestExecutionListener.java:70)
	at org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.resetMocks(ResetMocksTestExecutionListener.java:55)
	at org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.afterTestMethod(ResetMocksTestExecutionListener.java:50)
	at org.springframework.test.context.TestContextManager.afterTestMethod(TestContextManager.java:319)
	at org.springframework.test.context.junit4.statements.RunAfterTestMethodCallbacks.evaluate(RunAfterTestMethodCallbacks.java:94)
	at org.springframework.test.context.junit4.statements.SpringRepeat.evaluate(SpringRepeat.java:84)
	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)
	at org.springframework.test.context.junit4.SpringJUnit4ClassRunner.runChild(SpringJUnit4ClassRunner.java:252)
	at org.springframework.test.context.junit4.SpringJUnit4ClassRunner.runChild(SpringJUnit4ClassRunner.java:94)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at org.springframework.test.context.junit4.statements.RunBeforeTestClassCallbacks.evaluate(RunBeforeTestClassCallbacks.java:61)
	at org.springframework.test.context.junit4.statements.RunAfterTestClassCallbacks.evaluate(RunAfterTestClassCallbacks.java:70)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at org.springframework.test.context.junit4.SpringJUnit4ClassRunner.run(SpringJUnit4ClassRunner.java:191)
	at org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)
	at org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:459)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:678)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:382)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:192)
```
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/961","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/961/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/961/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/961/events","html_url":"https://github.com/mockito/mockito/issues/961","id":209715452,"node_id":"MDU6SXNzdWUyMDk3MTU0NTI=","number":961,"title":"Let java.lang.StackOverflowError surface","user":{"login":"pintiliea","id":23454490,"node_id":"MDQ6VXNlcjIzNDU0NDkw","avatar_url":"https://avatars2.githubusercontent.com/u/23454490?v=4","gravatar_id":"","url":"https://api.github.com/users/pintiliea","html_url":"https://github.com/pintiliea","followers_url":"https://api.github.com/users/pintiliea/followers","following_url":"https://api.github.com/users/pintiliea/following{/other_user}","gists_url":"https://api.github.com/users/pintiliea/gists{/gist_id}","starred_url":"https://api.github.com/users/pintiliea/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/pintiliea/subscriptions","organizations_url":"https://api.github.com/users/pintiliea/orgs","repos_url":"https://api.github.com/users/pintiliea/repos","events_url":"https://api.github.com/users/pintiliea/events{/privacy}","received_events_url":"https://api.github.com/users/pintiliea/received_events","type":"User","site_admin":false},"labels":[{"id":16375488,"node_id":"MDU6TGFiZWwxNjM3NTQ4OA==","url":"https://api.github.com/repos/mockito/mockito/labels/wontfix","name":"wontfix","color":"ffffff","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-02-23T10:03:26Z","updated_at":"2017-02-24T11:26:09Z","closed_at":"2017-02-24T11:26:09Z","author_association":"NONE","body":"`mockito-core-1.10.19`\r\n\r\n`Spring Boot 1.4.4.RELEASE`\r\n```\r\njava -version\r\njava version \"1.8.0_121\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_121-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)\r\n```\r\n```\r\nSpring Tool Suite \r\n\r\nVersion: 3.8.3.RELEASE\r\nBuild Id: 201612191351\r\nPlatform: Eclipse Neon.2 (4.6.2)\r\n```\r\n\r\nHello,\r\n\r\nI did an infinite recursive call by accident inside the `thenReturn()` which gave me the output bellow. IMO you shouldn't catch `java.lang.StackOverflowError` if that is possible, let it surface so I can locate the mistake.\r\n\r\n```\r\norg.mockito.exceptions.misusing.UnfinishedStubbingException: \r\nUnfinished stubbing detected here:\r\n-> at my.package.MyServiceImplTest.testMyMethod(MyServiceImplTest.java:173)\r\n\r\nE.g. thenReturn() may be missing.\r\nExamples of correct stubbing:\r\n    when(mock.isOk()).thenReturn(true);\r\n    when(mock.isOk()).thenThrow(exception);\r\n    doThrow(exception).when(mock).someVoidMethod();\r\nHints:\r\n 1. missing thenReturn()\r\n 2. you are trying to stub a final method, you naughty developer!\r\n 3: you are stubbing the behaviour of another mock inside before 'thenReturn' instruction if completed\r\n\r\n\tat org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.resetMocks(ResetMocksTestExecutionListener.java:70)\r\n\tat org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.resetMocks(ResetMocksTestExecutionListener.java:55)\r\n\tat org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener.afterTestMethod(ResetMocksTestExecutionListener.java:50)\r\n\tat org.springframework.test.context.TestContextManager.afterTestMethod(TestContextManager.java:319)\r\n\tat org.springframework.test.context.junit4.statements.RunAfterTestMethodCallbacks.evaluate(RunAfterTestMethodCallbacks.java:94)\r\n\tat org.springframework.test.context.junit4.statements.SpringRepeat.evaluate(SpringRepeat.java:84)\r\n\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\r\n\tat org.springframework.test.context.junit4.SpringJUnit4ClassRunner.runChild(SpringJUnit4ClassRunner.java:252)\r\n\tat org.springframework.test.context.junit4.SpringJUnit4ClassRunner.runChild(SpringJUnit4ClassRunner.java:94)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.springframework.test.context.junit4.statements.RunBeforeTestClassCallbacks.evaluate(RunBeforeTestClassCallbacks.java:61)\r\n\tat org.springframework.test.context.junit4.statements.RunAfterTestClassCallbacks.evaluate(RunAfterTestClassCallbacks.java:70)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.springframework.test.context.junit4.SpringJUnit4ClassRunner.run(SpringJUnit4ClassRunner.java:191)\r\n\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)\r\n\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:459)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:678)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:382)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:192)\r\n```\r\n","closed_by":{"login":"bric3","id":803621,"node_id":"MDQ6VXNlcjgwMzYyMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/803621?v=4","gravatar_id":"","url":"https://api.github.com/users/bric3","html_url":"https://github.com/bric3","followers_url":"https://api.github.com/users/bric3/followers","following_url":"https://api.github.com/users/bric3/following{/other_user}","gists_url":"https://api.github.com/users/bric3/gists{/gist_id}","starred_url":"https://api.github.com/users/bric3/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bric3/subscriptions","organizations_url":"https://api.github.com/users/bric3/orgs","repos_url":"https://api.github.com/users/bric3/repos","events_url":"https://api.github.com/users/bric3/events{/privacy}","received_events_url":"https://api.github.com/users/bric3/received_events","type":"User","site_admin":false}}", "commentIds":["281998299","282269457"], "labels":["wontfix"]}