{"id":"173", "title":"Ignoring invocations on a spy()?", "body":"Note that I am aware that the code I currently have has a design fault precisely because of this but here goes... Mockito core version 1.10.19

The project in question is [this](https://github.com/fge/java7-fs-base); I am trying to ease the development of new `FileSystem` instances and a pain point is the attributes.

The problem is in fact described in this question on StackOverflow:

http://stackoverflow.com/q/28573595/1093528

Basically, I'd like to be able to write the test, provided that there exists a (statically imported) `Mockito.ignoreInvocation`:

``` java
@Test
public void setOwnerTest()
    throws IOException
{
    final UserPrincipal owner = mock(UserPrincipal.class);

    ignoreInvocation(writer).setAttributeByName(\"owner\", owner);

    verify(writer, only()).setOwner(same(owner));
}
```

Maybe this is already possible? I have looked at the javadoc and so far it doesn't seem to me like it is.
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/173","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/173/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/173/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/173/events","html_url":"https://github.com/mockito/mockito/issues/173","id":58014097,"node_id":"MDU6SXNzdWU1ODAxNDA5Nw==","number":173,"title":"Ignoring invocations on a spy()?","user":{"login":"fge","id":550006,"node_id":"MDQ6VXNlcjU1MDAwNg==","avatar_url":"https://avatars0.githubusercontent.com/u/550006?v=4","gravatar_id":"","url":"https://api.github.com/users/fge","html_url":"https://github.com/fge","followers_url":"https://api.github.com/users/fge/followers","following_url":"https://api.github.com/users/fge/following{/other_user}","gists_url":"https://api.github.com/users/fge/gists{/gist_id}","starred_url":"https://api.github.com/users/fge/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/fge/subscriptions","organizations_url":"https://api.github.com/users/fge/orgs","repos_url":"https://api.github.com/users/fge/repos","events_url":"https://api.github.com/users/fge/events{/privacy}","received_events_url":"https://api.github.com/users/fge/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2015-02-18T01:56:07Z","updated_at":"2015-02-20T16:56:24Z","closed_at":"2015-02-19T12:49:53Z","author_association":"NONE","body":"Note that I am aware that the code I currently have has a design fault precisely because of this but here goes... Mockito core version 1.10.19\n\nThe project in question is [this](https://github.com/fge/java7-fs-base); I am trying to ease the development of new `FileSystem` instances and a pain point is the attributes.\n\nThe problem is in fact described in this question on StackOverflow:\n\nhttp://stackoverflow.com/q/28573595/1093528\n\nBasically, I'd like to be able to write the test, provided that there exists a (statically imported) `Mockito.ignoreInvocation`:\n\n``` java\n@Test\npublic void setOwnerTest()\n    throws IOException\n{\n    final UserPrincipal owner = mock(UserPrincipal.class);\n\n    ignoreInvocation(writer).setAttributeByName(\"owner\", owner);\n\n    verify(writer, only()).setOwner(same(owner));\n}\n```\n\nMaybe this is already possible? I have looked at the javadoc and so far it doesn't seem to me like it is.\n","closed_by":{"login":"bric3","id":803621,"node_id":"MDQ6VXNlcjgwMzYyMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/803621?v=4","gravatar_id":"","url":"https://api.github.com/users/bric3","html_url":"https://github.com/bric3","followers_url":"https://api.github.com/users/bric3/followers","following_url":"https://api.github.com/users/bric3/following{/other_user}","gists_url":"https://api.github.com/users/bric3/gists{/gist_id}","starred_url":"https://api.github.com/users/bric3/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bric3/subscriptions","organizations_url":"https://api.github.com/users/bric3/orgs","repos_url":"https://api.github.com/users/bric3/repos","events_url":"https://api.github.com/users/bric3/events{/privacy}","received_events_url":"https://api.github.com/users/bric3/received_events","type":"User","site_admin":false}}", "commentIds":["75046613","75272649"], "labels":[]}