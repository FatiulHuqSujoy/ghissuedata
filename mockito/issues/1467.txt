{"id":"1467", "title":"Mockito java.lang.NoSuchMethodError: org.mockito.internal.matchers.InstanceOf.<init>", "body":"I am trying to create a unit test for an stateless bean class that is calling CompletionService and returning a future object. I am working in Eclipse Oxygen with Junit 5 and Mockito 2.21 core. I cannot get the dependencies to be successfully completed it starts with error:

> java.lang.NoSuchMethodError: org.mockito.internal.matchers.InstanceOf.<init>(Ljava/lang/Class;Ljava/lang/String;)V
> 	at org.mockito.internal.matchers.InstanceOf$VarArgAware.<init>(InstanceOf.java:45)
> 	at org.mockito.ArgumentMatchers.any(ArgumentMatchers.java:207)
> 	at com.sbsa.psync.advise.generate.GetCustomerServiceV4Test4.testGetCustomer(GetCustomerServiceV4Test4.java:137)

The Unit test I am trying to execute
```
    @Test
public void testGetCustomer() 
{
    LogService logService = mock(LogService.class);
    doNothing().when(logService).addActivityLogEntry(any(LogCommonFields.class), any(String.class), any(String.class));
    when(logService.getCodeDetails(any(String.class))).thenReturn(\"SUCCESS\"); 
    doNothing().when(logService).logToPerformanceLogFormatted(any(String.class), any(String.class), any(Long.class));
}
```
 I am Using Eclipse Oxygen with Junit 5 and Mockito 2.21, Java 1.8, hamcrest 2.0 on Windows 10 
The only errors I could find that seem to be remotely related was related to the hamcrest dependency that might be shared between JUnit and Mockito.  I then added the below tags in the pom file with no effect.
```
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>java-hamcrest</artifactId>
			<version>2.0.0.0</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<version>2.21.0</version>
			<exclusions>
				<exclusion>
					<artifactId>hamcrest-core</artifactId>
					<groupId>org.hamcrest</groupId>
				</exclusion>
			</exclusions>
			<scope>test</scope>
		</dependency>
```

Below is a simple class and test that gives the same error.
```
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import com.sbsa.psync.common.LogCommonFields;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class LogService2 
{
  public void addActivityLogEntry(LogCommonFields logCommonFields, String code, String bpId) 
  {
    System.out.println(\"logging the error\");
  } 
}
```

```
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.Test;
import com.sbsa.psync.common.LogCommonFields;
import com.sbsa.psync.log.LogService2;

class GetCustomerServiceV4Test5 
{
 @Test
 public void testGetCustomer() 
 {
   LogService2 logService = mock(LogService2.class);
   doNothing().when(logService).addActivityLogEntry(any(LogCommonFields.class), any(String.class), any(String.class));
  }
}
```


", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1467","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1467/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1467/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1467/events","html_url":"https://github.com/mockito/mockito/issues/1467","id":347955837,"node_id":"MDU6SXNzdWUzNDc5NTU4Mzc=","number":1467,"title":"Mockito java.lang.NoSuchMethodError: org.mockito.internal.matchers.InstanceOf.<init>","user":{"login":"dirklombard","id":10023870,"node_id":"MDQ6VXNlcjEwMDIzODcw","avatar_url":"https://avatars1.githubusercontent.com/u/10023870?v=4","gravatar_id":"","url":"https://api.github.com/users/dirklombard","html_url":"https://github.com/dirklombard","followers_url":"https://api.github.com/users/dirklombard/followers","following_url":"https://api.github.com/users/dirklombard/following{/other_user}","gists_url":"https://api.github.com/users/dirklombard/gists{/gist_id}","starred_url":"https://api.github.com/users/dirklombard/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/dirklombard/subscriptions","organizations_url":"https://api.github.com/users/dirklombard/orgs","repos_url":"https://api.github.com/users/dirklombard/repos","events_url":"https://api.github.com/users/dirklombard/events{/privacy}","received_events_url":"https://api.github.com/users/dirklombard/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-08-06T14:48:47Z","updated_at":"2019-08-06T11:10:24Z","closed_at":"2019-04-30T16:37:05Z","author_association":"NONE","body":"I am trying to create a unit test for an stateless bean class that is calling CompletionService and returning a future object. I am working in Eclipse Oxygen with Junit 5 and Mockito 2.21 core. I cannot get the dependencies to be successfully completed it starts with error:\r\n\r\n> java.lang.NoSuchMethodError: org.mockito.internal.matchers.InstanceOf.<init>(Ljava/lang/Class;Ljava/lang/String;)V\r\n> \tat org.mockito.internal.matchers.InstanceOf$VarArgAware.<init>(InstanceOf.java:45)\r\n> \tat org.mockito.ArgumentMatchers.any(ArgumentMatchers.java:207)\r\n> \tat com.sbsa.psync.advise.generate.GetCustomerServiceV4Test4.testGetCustomer(GetCustomerServiceV4Test4.java:137)\r\n\r\nThe Unit test I am trying to execute\r\n```\r\n    @Test\r\npublic void testGetCustomer() \r\n{\r\n    LogService logService = mock(LogService.class);\r\n    doNothing().when(logService).addActivityLogEntry(any(LogCommonFields.class), any(String.class), any(String.class));\r\n    when(logService.getCodeDetails(any(String.class))).thenReturn(\"SUCCESS\"); \r\n    doNothing().when(logService).logToPerformanceLogFormatted(any(String.class), any(String.class), any(Long.class));\r\n}\r\n```\r\n I am Using Eclipse Oxygen with Junit 5 and Mockito 2.21, Java 1.8, hamcrest 2.0 on Windows 10 \r\nThe only errors I could find that seem to be remotely related was related to the hamcrest dependency that might be shared between JUnit and Mockito.  I then added the below tags in the pom file with no effect.\r\n```\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.hamcrest</groupId>\r\n\t\t\t<artifactId>java-hamcrest</artifactId>\r\n\t\t\t<version>2.0.0.0</version>\r\n\t\t\t<scope>test</scope>\r\n\t\t</dependency>\r\n\r\n\t\t<dependency>\r\n\t\t\t<groupId>org.mockito</groupId>\r\n\t\t\t<artifactId>mockito-core</artifactId>\r\n\t\t\t<version>2.21.0</version>\r\n\t\t\t<exclusions>\r\n\t\t\t\t<exclusion>\r\n\t\t\t\t\t<artifactId>hamcrest-core</artifactId>\r\n\t\t\t\t\t<groupId>org.hamcrest</groupId>\r\n\t\t\t\t</exclusion>\r\n\t\t\t</exclusions>\r\n\t\t\t<scope>test</scope>\r\n\t\t</dependency>\r\n```\r\n\r\nBelow is a simple class and test that gives the same error.\r\n```\r\nimport javax.ejb.LocalBean;\r\nimport javax.ejb.Stateless;\r\nimport javax.ejb.TransactionManagement;\r\nimport javax.ejb.TransactionManagementType;\r\nimport com.sbsa.psync.common.LogCommonFields;\r\n\r\n@Stateless\r\n@LocalBean\r\n@TransactionManagement(TransactionManagementType.BEAN)\r\npublic class LogService2 \r\n{\r\n  public void addActivityLogEntry(LogCommonFields logCommonFields, String code, String bpId) \r\n  {\r\n    System.out.println(\"logging the error\");\r\n  } \r\n}\r\n```\r\n\r\n```\r\nimport static org.mockito.ArgumentMatchers.any;\r\nimport static org.mockito.Mockito.doNothing;\r\nimport static org.mockito.Mockito.mock;\r\nimport org.junit.jupiter.api.Test;\r\nimport com.sbsa.psync.common.LogCommonFields;\r\nimport com.sbsa.psync.log.LogService2;\r\n\r\nclass GetCustomerServiceV4Test5 \r\n{\r\n @Test\r\n public void testGetCustomer() \r\n {\r\n   LogService2 logService = mock(LogService2.class);\r\n   doNothing().when(logService).addActivityLogEntry(any(LogCommonFields.class), any(String.class), any(String.class));\r\n  }\r\n}\r\n```\r\n\r\n\r\n","closed_by":{"login":"TimvdLippe","id":5948271,"node_id":"MDQ6VXNlcjU5NDgyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/5948271?v=4","gravatar_id":"","url":"https://api.github.com/users/TimvdLippe","html_url":"https://github.com/TimvdLippe","followers_url":"https://api.github.com/users/TimvdLippe/followers","following_url":"https://api.github.com/users/TimvdLippe/following{/other_user}","gists_url":"https://api.github.com/users/TimvdLippe/gists{/gist_id}","starred_url":"https://api.github.com/users/TimvdLippe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TimvdLippe/subscriptions","organizations_url":"https://api.github.com/users/TimvdLippe/orgs","repos_url":"https://api.github.com/users/TimvdLippe/repos","events_url":"https://api.github.com/users/TimvdLippe/events{/privacy}","received_events_url":"https://api.github.com/users/TimvdLippe/received_events","type":"User","site_admin":false}}", "commentIds":["444052772","457263820","463478643","488024182","518624487"], "labels":[]}