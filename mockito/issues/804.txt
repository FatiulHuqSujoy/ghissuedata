{"id":"804", "title":"Verify exception would be swallowed when specifing expected  parameter", "body":"Android Studio Version: 2.2.3
Mockito Version: org.mockito:mockito-core:2.2.29'
Here is my test:
```java
@Test(expected = IndexOutOfBoundsException.class)
public void test(){
    List mockedList = mock(LinkedList.class);

    final Matcher<Integer> matcher = new Matcher<Integer>() {
        @Override
        public boolean matches(Object item) {
            if(!(item instanceof Integer)){
                return false;
            }

            final Integer integer = (Integer) item;

            return integer >= 0 && integer <= 10;
        }

        @Override
        public void describeMismatch(Object item, Description mismatchDescription) {}

        @Override
        public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {}

        @Override
        public void describeTo(Description description) {}
    };

    when(mockedList.get(intThat(matcher))).thenReturn(\"Some Strings\");
    when(mockedList.get(intThat(not(matcher)))).thenThrow(new IndexOutOfBoundsException());
    
    mockedList.get(0);// 1
    mockedList.get(10);// 2
    mockedList.get(10);// 3
    mockedList.get(11);// 4

    verify(mockedList).get(10);
}
```
At the last line I verify `get(10)` will be invoked once time which is invoked twice in fact , this test should  fail but passed . 
Then I remove `mockedList.get(11);` , and it would throw correct exceptions.", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/804","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/804/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/804/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/804/events","html_url":"https://github.com/mockito/mockito/issues/804","id":194309788,"node_id":"MDU6SXNzdWUxOTQzMDk3ODg=","number":804,"title":"Verify exception would be swallowed when specifing expected  parameter","user":{"login":"huazhouwang","id":9532423,"node_id":"MDQ6VXNlcjk1MzI0MjM=","avatar_url":"https://avatars2.githubusercontent.com/u/9532423?v=4","gravatar_id":"","url":"https://api.github.com/users/huazhouwang","html_url":"https://github.com/huazhouwang","followers_url":"https://api.github.com/users/huazhouwang/followers","following_url":"https://api.github.com/users/huazhouwang/following{/other_user}","gists_url":"https://api.github.com/users/huazhouwang/gists{/gist_id}","starred_url":"https://api.github.com/users/huazhouwang/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/huazhouwang/subscriptions","organizations_url":"https://api.github.com/users/huazhouwang/orgs","repos_url":"https://api.github.com/users/huazhouwang/repos","events_url":"https://api.github.com/users/huazhouwang/events{/privacy}","received_events_url":"https://api.github.com/users/huazhouwang/received_events","type":"User","site_admin":false},"labels":[{"id":305646380,"node_id":"MDU6TGFiZWwzMDU2NDYzODA=","url":"https://api.github.com/repos/mockito/mockito/labels/android","name":"android","color":"009800","default":false},{"id":16375487,"node_id":"MDU6TGFiZWwxNjM3NTQ4Nw==","url":"https://api.github.com/repos/mockito/mockito/labels/question","name":"question","color":"cc317c","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2016-12-08T11:16:53Z","updated_at":"2017-01-19T13:13:26Z","closed_at":"2016-12-08T16:23:08Z","author_association":"NONE","body":"Android Studio Version: 2.2.3\r\nMockito Version: org.mockito:mockito-core:2.2.29'\r\nHere is my test:\r\n```java\r\n@Test(expected = IndexOutOfBoundsException.class)\r\npublic void test(){\r\n    List mockedList = mock(LinkedList.class);\r\n\r\n    final Matcher<Integer> matcher = new Matcher<Integer>() {\r\n        @Override\r\n        public boolean matches(Object item) {\r\n            if(!(item instanceof Integer)){\r\n                return false;\r\n            }\r\n\r\n            final Integer integer = (Integer) item;\r\n\r\n            return integer >= 0 && integer <= 10;\r\n        }\r\n\r\n        @Override\r\n        public void describeMismatch(Object item, Description mismatchDescription) {}\r\n\r\n        @Override\r\n        public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {}\r\n\r\n        @Override\r\n        public void describeTo(Description description) {}\r\n    };\r\n\r\n    when(mockedList.get(intThat(matcher))).thenReturn(\"Some Strings\");\r\n    when(mockedList.get(intThat(not(matcher)))).thenThrow(new IndexOutOfBoundsException());\r\n    \r\n    mockedList.get(0);// 1\r\n    mockedList.get(10);// 2\r\n    mockedList.get(10);// 3\r\n    mockedList.get(11);// 4\r\n\r\n    verify(mockedList).get(10);\r\n}\r\n```\r\nAt the last line I verify `get(10)` will be invoked once time which is invoked twice in fact , this test should  fail but passed . \r\nThen I remove `mockedList.get(11);` , and it would throw correct exceptions.","closed_by":{"login":"bric3","id":803621,"node_id":"MDQ6VXNlcjgwMzYyMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/803621?v=4","gravatar_id":"","url":"https://api.github.com/users/bric3","html_url":"https://github.com/bric3","followers_url":"https://api.github.com/users/bric3/followers","following_url":"https://api.github.com/users/bric3/following{/other_user}","gists_url":"https://api.github.com/users/bric3/gists{/gist_id}","starred_url":"https://api.github.com/users/bric3/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bric3/subscriptions","organizations_url":"https://api.github.com/users/bric3/orgs","repos_url":"https://api.github.com/users/bric3/repos","events_url":"https://api.github.com/users/bric3/events{/privacy}","received_events_url":"https://api.github.com/users/bric3/received_events","type":"User","site_admin":false}}", "commentIds":["265767080","265769431","265772615","265775214","265780960"], "labels":["android","question"]}