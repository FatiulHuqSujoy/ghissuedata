{"id":"817", "title":"Mockito mock-inline and Robolectric compatibility", "body":"Hello,
Seems, there is exception with mockito and robolectric if using **mock-maker-inline**
Without mock-maker-inline there is no such problem.
Any help?
Here is the code:
```
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MockInlineWithRobolectricTest {
    @Test
    public void sample() {
        Mockito.mock(SomeOrdinaryClass.class);
    }
}
```

Here is stacktrace
```
java.lang.ExceptionInInitializerError
	at org.mockito.internal.exceptions.stacktrace.ConditionalStackTraceFilter.<init>(ConditionalStackTraceFilter.java:17)
	at org.mockito.exceptions.base.MockitoException.filterStackTrace(MockitoException.java:41)
	at org.mockito.exceptions.base.MockitoException.<init>(MockitoException.java:35)
	at org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<init>(InlineByteBuddyMockMaker.java:123)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:422)
	at java.lang.Class.newInstance(Class.java:442)
	at org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:81)
	at org.mockito.internal.configuration.plugins.PluginLoader.loadPlugin(PluginLoader.java:40)
	at org.mockito.internal.configuration.plugins.PluginRegistry.<init>(PluginRegistry.java:17)
	at org.mockito.internal.configuration.plugins.Plugins.<clinit>(Plugins.java:16)
	at org.mockito.internal.util.MockUtil.<clinit>(MockUtil.java:24)
	at org.mockito.internal.util.MockCreationValidator.validateType(MockCreationValidator.java:22)
	at org.mockito.internal.creation.MockSettingsImpl.validatedSettings(MockSettingsImpl.java:168)
	at org.mockito.internal.creation.MockSettingsImpl.confirm(MockSettingsImpl.java:162)
	at org.mockito.internal.MockitoCore.mock(MockitoCore.java:62)
	at org.mockito.Mockito.mock(Mockito.java:1665)
	at org.mockito.Mockito.mock(Mockito.java:1578)
	at com.songsterr.db.MockInlineWithRobolectricTest.sample(MockInlineWithRobolectricTest.java:18)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)
	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)
	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
	at org.robolectric.RobolectricTestRunner$HelperTestRunner$1.evaluate(RobolectricTestRunner.java:498)
	at org.robolectric.RobolectricTestRunner$2.evaluate(RobolectricTestRunner.java:245)
	at org.robolectric.RobolectricTestRunner.runChild(RobolectricTestRunner.java:171)
	at org.robolectric.RobolectricTestRunner.runChild(RobolectricTestRunner.java:47)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at org.robolectric.RobolectricTestRunner$1.evaluate(RobolectricTestRunner.java:137)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at org.junit.runner.JUnitCore.run(JUnitCore.java:137)
	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:119)
	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:42)
	at com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:234)
	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:74)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at com.intellij.rt.execution.application.AppMain.main(AppMain.java:144)
Caused by: java.lang.NullPointerException
	at org.mockito.internal.configuration.plugins.Plugins.getStackTraceCleanerProvider(Plugins.java:22)
	at org.mockito.internal.exceptions.stacktrace.StackTraceFilter.<clinit>(StackTraceFilter.java:19)
	... 50 more
```
Versions
mockito-core 2.3.0
robolectric 3.1.4
jvm 1.8.0_45
macOS Sierra 10.12.1
check that

 - [ ] The mockito message in the stacktrace have useful information, but it didn't help
 - [x] The problematic code (if that's possible) is copied here;
       Note that some configuration are impossible to mock via Mockito
 - [x] Provide versions (mockito / jdk / os / any other relevant information)
 - [ ] Provide a [Short, Self Contained, Correct (Compilable), Example](http://sscce.org) of the issue
       (same as any question on stackoverflow.com)
 - [ ] Read the [contributing guide](https://github.com/mockito/mockito/blob/master/.github/CONTRIBUTING.md)


", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/817","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/817/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/817/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/817/events","html_url":"https://github.com/mockito/mockito/issues/817","id":195338538,"node_id":"MDU6SXNzdWUxOTUzMzg1Mzg=","number":817,"title":"Mockito mock-inline and Robolectric compatibility","user":{"login":"folkyatina","id":1414948,"node_id":"MDQ6VXNlcjE0MTQ5NDg=","avatar_url":"https://avatars0.githubusercontent.com/u/1414948?v=4","gravatar_id":"","url":"https://api.github.com/users/folkyatina","html_url":"https://github.com/folkyatina","followers_url":"https://api.github.com/users/folkyatina/followers","following_url":"https://api.github.com/users/folkyatina/following{/other_user}","gists_url":"https://api.github.com/users/folkyatina/gists{/gist_id}","starred_url":"https://api.github.com/users/folkyatina/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/folkyatina/subscriptions","organizations_url":"https://api.github.com/users/folkyatina/orgs","repos_url":"https://api.github.com/users/folkyatina/repos","events_url":"https://api.github.com/users/folkyatina/events{/privacy}","received_events_url":"https://api.github.com/users/folkyatina/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-12-13T18:59:28Z","updated_at":"2016-12-13T19:13:25Z","closed_at":"2016-12-13T19:13:25Z","author_association":"NONE","body":"Hello,\r\nSeems, there is exception with mockito and robolectric if using **mock-maker-inline**\r\nWithout mock-maker-inline there is no such problem.\r\nAny help?\r\nHere is the code:\r\n```\r\n@RunWith(RobolectricTestRunner.class)\r\n@Config(constants = BuildConfig.class, sdk = 21)\r\npublic class MockInlineWithRobolectricTest {\r\n    @Test\r\n    public void sample() {\r\n        Mockito.mock(SomeOrdinaryClass.class);\r\n    }\r\n}\r\n```\r\n\r\nHere is stacktrace\r\n```\r\njava.lang.ExceptionInInitializerError\r\n\tat org.mockito.internal.exceptions.stacktrace.ConditionalStackTraceFilter.<init>(ConditionalStackTraceFilter.java:17)\r\n\tat org.mockito.exceptions.base.MockitoException.filterStackTrace(MockitoException.java:41)\r\n\tat org.mockito.exceptions.base.MockitoException.<init>(MockitoException.java:35)\r\n\tat org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<init>(InlineByteBuddyMockMaker.java:123)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:422)\r\n\tat java.lang.Class.newInstance(Class.java:442)\r\n\tat org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:81)\r\n\tat org.mockito.internal.configuration.plugins.PluginLoader.loadPlugin(PluginLoader.java:40)\r\n\tat org.mockito.internal.configuration.plugins.PluginRegistry.<init>(PluginRegistry.java:17)\r\n\tat org.mockito.internal.configuration.plugins.Plugins.<clinit>(Plugins.java:16)\r\n\tat org.mockito.internal.util.MockUtil.<clinit>(MockUtil.java:24)\r\n\tat org.mockito.internal.util.MockCreationValidator.validateType(MockCreationValidator.java:22)\r\n\tat org.mockito.internal.creation.MockSettingsImpl.validatedSettings(MockSettingsImpl.java:168)\r\n\tat org.mockito.internal.creation.MockSettingsImpl.confirm(MockSettingsImpl.java:162)\r\n\tat org.mockito.internal.MockitoCore.mock(MockitoCore.java:62)\r\n\tat org.mockito.Mockito.mock(Mockito.java:1665)\r\n\tat org.mockito.Mockito.mock(Mockito.java:1578)\r\n\tat com.songsterr.db.MockInlineWithRobolectricTest.sample(MockInlineWithRobolectricTest.java:18)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:497)\r\n\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)\r\n\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)\r\n\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\r\n\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\r\n\tat org.robolectric.RobolectricTestRunner$HelperTestRunner$1.evaluate(RobolectricTestRunner.java:498)\r\n\tat org.robolectric.RobolectricTestRunner$2.evaluate(RobolectricTestRunner.java:245)\r\n\tat org.robolectric.RobolectricTestRunner.runChild(RobolectricTestRunner.java:171)\r\n\tat org.robolectric.RobolectricTestRunner.runChild(RobolectricTestRunner.java:47)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.robolectric.RobolectricTestRunner$1.evaluate(RobolectricTestRunner.java:137)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:119)\r\n\tat com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:42)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:234)\r\n\tat com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:74)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:497)\r\n\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:144)\r\nCaused by: java.lang.NullPointerException\r\n\tat org.mockito.internal.configuration.plugins.Plugins.getStackTraceCleanerProvider(Plugins.java:22)\r\n\tat org.mockito.internal.exceptions.stacktrace.StackTraceFilter.<clinit>(StackTraceFilter.java:19)\r\n\t... 50 more\r\n```\r\nVersions\r\nmockito-core 2.3.0\r\nrobolectric 3.1.4\r\njvm 1.8.0_45\r\nmacOS Sierra 10.12.1\r\ncheck that\r\n\r\n - [ ] The mockito message in the stacktrace have useful information, but it didn't help\r\n - [x] The problematic code (if that's possible) is copied here;\r\n       Note that some configuration are impossible to mock via Mockito\r\n - [x] Provide versions (mockito / jdk / os / any other relevant information)\r\n - [ ] Provide a [Short, Self Contained, Correct (Compilable), Example](http://sscce.org) of the issue\r\n       (same as any question on stackoverflow.com)\r\n - [ ] Read the [contributing guide](https://github.com/mockito/mockito/blob/master/.github/CONTRIBUTING.md)\r\n\r\n\r\n","closed_by":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}}", "commentIds":["266830722","266832682"], "labels":[]}