{"id":"150", "title":"mockito 1.9.5 or 1.10.17  can not passed junit test when use generic class", "body":"let's has below assumpution test environments :

your can get demo project at:
https://github.com/cloudtrends/jstudy

https://github.com/cloudtrends/jstudy/tree/master/src/com/test
1.  public abstract class ParentClz<T extends ParentClz<T,U>, U> 
2.  public class Child extends ParentClz<Child,String>
3. public class ChildApi 
    just include one method:
   
   public Child addChild(){
       return new Child();
   }
4. test code can not passed:
   ChildApi api = mock(ChildApi.class, RETURNS_DEEP_STUBS );
   Child c = mock(Child.class, withSettings().verboseLogging());
   
   Child cDirect = api.addChild();
   Child cu = cDirect.create();
   when( cu  ).thenReturn(c);
5. errors as :
   java.lang.StackOverflowError
   at sun.reflect.generics.reflectiveObjects.TypeVariableImpl.hashCode(Unknown Source)
   at java.util.HashMap.hash(Unknown Source)
   at java.util.HashMap.getEntry(Unknown Source)
   at java.util.HashMap.get(Unknown Source)
   at org.mockito.internal.util.reflection.GenericMetadataSupport.getActualTypeArgumentFor(GenericMetadataSupport.java:182)

or errors in 1.9.5

java.lang.ClassCastException: com.test.ParentClz$$EnhancerByMockitoWithCGLIB$$c2a01047 cannot be cast to com.test.Child
    at com.test.ChildTest.testCreate(ChildTest.java:56)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/150","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/150/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/150/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/150/events","html_url":"https://github.com/mockito/mockito/issues/150","id":53097711,"node_id":"MDU6SXNzdWU1MzA5NzcxMQ==","number":150,"title":"mockito 1.9.5 or 1.10.17  can not passed junit test when use generic class","user":{"login":"cloudtrends","id":2282880,"node_id":"MDQ6VXNlcjIyODI4ODA=","avatar_url":"https://avatars0.githubusercontent.com/u/2282880?v=4","gravatar_id":"","url":"https://api.github.com/users/cloudtrends","html_url":"https://github.com/cloudtrends","followers_url":"https://api.github.com/users/cloudtrends/followers","following_url":"https://api.github.com/users/cloudtrends/following{/other_user}","gists_url":"https://api.github.com/users/cloudtrends/gists{/gist_id}","starred_url":"https://api.github.com/users/cloudtrends/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cloudtrends/subscriptions","organizations_url":"https://api.github.com/users/cloudtrends/orgs","repos_url":"https://api.github.com/users/cloudtrends/repos","events_url":"https://api.github.com/users/cloudtrends/events{/privacy}","received_events_url":"https://api.github.com/users/cloudtrends/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2014-12-30T07:08:41Z","updated_at":"2015-02-21T13:37:06Z","closed_at":"2015-02-21T13:37:06Z","author_association":"NONE","body":"let's has below assumpution test environments :\n\nyour can get demo project at:\nhttps://github.com/cloudtrends/jstudy\n\nhttps://github.com/cloudtrends/jstudy/tree/master/src/com/test\n1.  public abstract class ParentClz<T extends ParentClz<T,U>, U> \n2.  public class Child extends ParentClz<Child,String>\n3. public class ChildApi \n    just include one method:\n   \n   public Child addChild(){\n       return new Child();\n   }\n4. test code can not passed:\n   ChildApi api = mock(ChildApi.class, RETURNS_DEEP_STUBS );\n   Child c = mock(Child.class, withSettings().verboseLogging());\n   \n   Child cDirect = api.addChild();\n   Child cu = cDirect.create();\n   when( cu  ).thenReturn(c);\n5. errors as :\n   java.lang.StackOverflowError\n   at sun.reflect.generics.reflectiveObjects.TypeVariableImpl.hashCode(Unknown Source)\n   at java.util.HashMap.hash(Unknown Source)\n   at java.util.HashMap.getEntry(Unknown Source)\n   at java.util.HashMap.get(Unknown Source)\n   at org.mockito.internal.util.reflection.GenericMetadataSupport.getActualTypeArgumentFor(GenericMetadataSupport.java:182)\n\nor errors in 1.9.5\n\njava.lang.ClassCastException: com.test.ParentClz$$EnhancerByMockitoWithCGLIB$$c2a01047 cannot be cast to com.test.Child\n    at com.test.ChildTest.testCreate(ChildTest.java:56)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n","closed_by":{"login":"bric3","id":803621,"node_id":"MDQ6VXNlcjgwMzYyMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/803621?v=4","gravatar_id":"","url":"https://api.github.com/users/bric3","html_url":"https://github.com/bric3","followers_url":"https://api.github.com/users/bric3/followers","following_url":"https://api.github.com/users/bric3/following{/other_user}","gists_url":"https://api.github.com/users/bric3/gists{/gist_id}","starred_url":"https://api.github.com/users/bric3/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bric3/subscriptions","organizations_url":"https://api.github.com/users/bric3/orgs","repos_url":"https://api.github.com/users/bric3/repos","events_url":"https://api.github.com/users/bric3/events{/privacy}","received_events_url":"https://api.github.com/users/bric3/received_events","type":"User","site_admin":false}}", "commentIds":["68727109","70393461","70401495","75371940"], "labels":[]}