{"id":"1541", "title":"Mock object premature garbage collected when using 'One-liner stubs'", "body":"### Problem:
'One-liner stubs' may throw an exception because the actual mock object is already garbage collected when trying to return the mock.

### Reproduce:
1. Mockito 2.23.5 using mockito-inline
2. -Xmx256m

```
public class OneLinerStubStressTest {

    public class TestClass {
        public String getStuff() {
            return \"A\";
        }
    }

    private static String generateString() {
        final int length = 10000;
        final StringBuilder stringBuilder = new StringBuilder(length);
        for (int i = 0; i <= length; i++) {
            stringBuilder.append(\"B\");
        }
        return stringBuilder.toString();
    }

    @Test
    public void call_a_lot_of_mocks() {
        final String returnValue = generateString();
        for (int i = 0; i < 40000; i++) {
            final TestClass mock = when(mock(TestClass.class).getStuff())
                                   .thenReturn(returnValue).getMock();
            assertEquals(returnValue, mock.getStuff());
        }
    }
}
```
#### Expected:
Test finishes successfully

#### Actual:
```
java.lang.IllegalStateException: The mock object was garbage collected. This should not happen in normal circumstances when using public API. Typically, the test class keeps strong reference to the mock object and it prevents getting the mock collected. Mockito internally needs to keep weak references to mock objects to avoid memory leaks for certain types of MockMaker implementations. If you see this exception using Mockito public API, please file a bug. For more information see issue #1313.
        at org.mockito.internal.invocation.mockref.MockWeakReference.get(MockWeakReference.java:32)
        at org.mockito.internal.invocation.InterceptedInvocation.getMock(InterceptedInvocation.java:103)
        at org.mockito.internal.stubbing.InvocationContainerImpl.invokedMock(InvocationContainerImpl.java:157)
        at org.mockito.internal.stubbing.ConsecutiveStubbing.getMock(ConsecutiveStubbing.java:28)
```

### Analysis:
As part of #1313 `DefaultInvocationFactory` and `InterceptedInvocation` were refactored to keep week reference to the mock. This was necessary in order to get rid of strong references by the handlers which caused memory leaks. On the other hand this requires to have a strong reference to the mock in the test class in order to prevent premature garbage collection. Unfortunately if using 'One-liner stubs' (https://static.javadoc.io/org.mockito/mockito-core/2.23.4/org/mockito/Mockito.html#one_liner_stub) the mock object may be premature cleaned up. This occurs because we gain access to the `InterceptedInvocation` when calling `getMock()` (which tries to get the mock from the invocation), without having any strong reference to the mock itself. At this point the mock may already be cleaned up.", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1541","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1541/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1541/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1541/events","html_url":"https://github.com/mockito/mockito/issues/1541","id":383666548,"node_id":"MDU6SXNzdWUzODM2NjY1NDg=","number":1541,"title":"Mock object premature garbage collected when using 'One-liner stubs'","user":{"login":"maxgrabenhorst","id":3321003,"node_id":"MDQ6VXNlcjMzMjEwMDM=","avatar_url":"https://avatars1.githubusercontent.com/u/3321003?v=4","gravatar_id":"","url":"https://api.github.com/users/maxgrabenhorst","html_url":"https://github.com/maxgrabenhorst","followers_url":"https://api.github.com/users/maxgrabenhorst/followers","following_url":"https://api.github.com/users/maxgrabenhorst/following{/other_user}","gists_url":"https://api.github.com/users/maxgrabenhorst/gists{/gist_id}","starred_url":"https://api.github.com/users/maxgrabenhorst/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/maxgrabenhorst/subscriptions","organizations_url":"https://api.github.com/users/maxgrabenhorst/orgs","repos_url":"https://api.github.com/users/maxgrabenhorst/repos","events_url":"https://api.github.com/users/maxgrabenhorst/events{/privacy}","received_events_url":"https://api.github.com/users/maxgrabenhorst/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-11-22T22:32:39Z","updated_at":"2018-11-26T15:59:14Z","closed_at":"2018-11-26T15:59:14Z","author_association":"CONTRIBUTOR","body":"### Problem:\r\n'One-liner stubs' may throw an exception because the actual mock object is already garbage collected when trying to return the mock.\r\n\r\n### Reproduce:\r\n1. Mockito 2.23.5 using mockito-inline\r\n2. -Xmx256m\r\n\r\n```\r\npublic class OneLinerStubStressTest {\r\n\r\n    public class TestClass {\r\n        public String getStuff() {\r\n            return \"A\";\r\n        }\r\n    }\r\n\r\n    private static String generateString() {\r\n        final int length = 10000;\r\n        final StringBuilder stringBuilder = new StringBuilder(length);\r\n        for (int i = 0; i <= length; i++) {\r\n            stringBuilder.append(\"B\");\r\n        }\r\n        return stringBuilder.toString();\r\n    }\r\n\r\n    @Test\r\n    public void call_a_lot_of_mocks() {\r\n        final String returnValue = generateString();\r\n        for (int i = 0; i < 40000; i++) {\r\n            final TestClass mock = when(mock(TestClass.class).getStuff())\r\n                                   .thenReturn(returnValue).getMock();\r\n            assertEquals(returnValue, mock.getStuff());\r\n        }\r\n    }\r\n}\r\n```\r\n#### Expected:\r\nTest finishes successfully\r\n\r\n#### Actual:\r\n```\r\njava.lang.IllegalStateException: The mock object was garbage collected. This should not happen in normal circumstances when using public API. Typically, the test class keeps strong reference to the mock object and it prevents getting the mock collected. Mockito internally needs to keep weak references to mock objects to avoid memory leaks for certain types of MockMaker implementations. If you see this exception using Mockito public API, please file a bug. For more information see issue #1313.\r\n        at org.mockito.internal.invocation.mockref.MockWeakReference.get(MockWeakReference.java:32)\r\n        at org.mockito.internal.invocation.InterceptedInvocation.getMock(InterceptedInvocation.java:103)\r\n        at org.mockito.internal.stubbing.InvocationContainerImpl.invokedMock(InvocationContainerImpl.java:157)\r\n        at org.mockito.internal.stubbing.ConsecutiveStubbing.getMock(ConsecutiveStubbing.java:28)\r\n```\r\n\r\n### Analysis:\r\nAs part of #1313 `DefaultInvocationFactory` and `InterceptedInvocation` were refactored to keep week reference to the mock. This was necessary in order to get rid of strong references by the handlers which caused memory leaks. On the other hand this requires to have a strong reference to the mock in the test class in order to prevent premature garbage collection. Unfortunately if using 'One-liner stubs' (https://static.javadoc.io/org.mockito/mockito-core/2.23.4/org/mockito/Mockito.html#one_liner_stub) the mock object may be premature cleaned up. This occurs because we gain access to the `InterceptedInvocation` when calling `getMock()` (which tries to get the mock from the invocation), without having any strong reference to the mock itself. At this point the mock may already be cleaned up.","closed_by":{"login":"mockitoguy","id":24743,"node_id":"MDQ6VXNlcjI0NzQz","avatar_url":"https://avatars2.githubusercontent.com/u/24743?v=4","gravatar_id":"","url":"https://api.github.com/users/mockitoguy","html_url":"https://github.com/mockitoguy","followers_url":"https://api.github.com/users/mockitoguy/followers","following_url":"https://api.github.com/users/mockitoguy/following{/other_user}","gists_url":"https://api.github.com/users/mockitoguy/gists{/gist_id}","starred_url":"https://api.github.com/users/mockitoguy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mockitoguy/subscriptions","organizations_url":"https://api.github.com/users/mockitoguy/orgs","repos_url":"https://api.github.com/users/mockitoguy/repos","events_url":"https://api.github.com/users/mockitoguy/events{/privacy}","received_events_url":"https://api.github.com/users/mockitoguy/received_events","type":"User","site_admin":false}}", "commentIds":["441296241","441384903"], "labels":[]}