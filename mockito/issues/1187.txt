{"id":"1187", "title":"Feature request: simultaneous stubbing", "body":"Here is an example:

```
public class NestedStubbingTest {

    @Mock Supplier<String> supplier;

    @Mock Function<String, String> converter;

    @Test public void test() {
        initMocks(this);
        when(supplier.get()).thenReturn(prepareOutput(\"abcde\", true));
        //...
    }

    private String prepareOutput(String output, boolean convertToUpperCase) {
        when(converter.apply(output)).thenReturn(convertToUpperCase ? output.toUpperCase() : output);
        return output;
    }
}
```

Currently the \"outer\" stubbing cannot be completed correctly because the \"inner\" one starts using `MockingProgress` before `thenReturn` of the first stubbing is called.
Result:
```
org.mockito.exceptions.misusing.UnfinishedStubbingException: 
Unfinished stubbing detected here:
-> at my.package.NestedStubbingTest.test(NestedStubbingTest.java:20)

E.g. thenReturn() may be missing.
Examples of correct stubbing:
    when(mock.isOk()).thenReturn(true);
    when(mock.isOk()).thenThrow(exception);
    doThrow(exception).when(mock).someVoidMethod();
Hints:
 1. missing thenReturn()
 2. you are trying to stub a final method, you naughty developer!
 3: you are stubbing the behaviour of another mock inside before 'thenReturn' instruction if completed


	at at my.package.NestedStubbingTest.prepareOutput(NestedStubbingTest.java:25)
	at at my.package.NestedStubbingTest.test(NestedStubbingTest.java:20)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)
	at org.testng.internal.Invoker.invokeMethod(Invoker.java:643)
	at org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)
	at org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)
	at org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)
	at org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)
	at org.testng.TestRunner.privateRun(TestRunner.java:782)
	at org.testng.TestRunner.run(TestRunner.java:632)
	at org.testng.SuiteRunner.runTest(SuiteRunner.java:366)
	at org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)
	at org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)
	at org.testng.SuiteRunner.run(SuiteRunner.java:268)
	at org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)
	at org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)
	at org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)
	at org.testng.TestNG.runSuitesLocally(TestNG.java:1169)
	at org.testng.TestNG.run(TestNG.java:1064)
	at org.testng.IDEARemoteTestNG.run(IDEARemoteTestNG.java:72)
	at org.testng.RemoteTestNGStarter.main(RemoteTestNGStarter.java:127)
```

An obvious workaround is to make those stubbing sequentially. But it makes such convenient methods as `prepareOutput()` impossible.

Mockito version: `1.10.19`.", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1187","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1187/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1187/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1187/events","html_url":"https://github.com/mockito/mockito/issues/1187","id":256232825,"node_id":"MDU6SXNzdWUyNTYyMzI4MjU=","number":1187,"title":"Feature request: simultaneous stubbing","user":{"login":"eugene-ivakhno","id":4425956,"node_id":"MDQ6VXNlcjQ0MjU5NTY=","avatar_url":"https://avatars1.githubusercontent.com/u/4425956?v=4","gravatar_id":"","url":"https://api.github.com/users/eugene-ivakhno","html_url":"https://github.com/eugene-ivakhno","followers_url":"https://api.github.com/users/eugene-ivakhno/followers","following_url":"https://api.github.com/users/eugene-ivakhno/following{/other_user}","gists_url":"https://api.github.com/users/eugene-ivakhno/gists{/gist_id}","starred_url":"https://api.github.com/users/eugene-ivakhno/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/eugene-ivakhno/subscriptions","organizations_url":"https://api.github.com/users/eugene-ivakhno/orgs","repos_url":"https://api.github.com/users/eugene-ivakhno/repos","events_url":"https://api.github.com/users/eugene-ivakhno/events{/privacy}","received_events_url":"https://api.github.com/users/eugene-ivakhno/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-09-08T12:03:49Z","updated_at":"2017-10-02T20:49:40Z","closed_at":"2017-10-02T20:49:40Z","author_association":"NONE","body":"Here is an example:\r\n\r\n```\r\npublic class NestedStubbingTest {\r\n\r\n    @Mock Supplier<String> supplier;\r\n\r\n    @Mock Function<String, String> converter;\r\n\r\n    @Test public void test() {\r\n        initMocks(this);\r\n        when(supplier.get()).thenReturn(prepareOutput(\"abcde\", true));\r\n        //...\r\n    }\r\n\r\n    private String prepareOutput(String output, boolean convertToUpperCase) {\r\n        when(converter.apply(output)).thenReturn(convertToUpperCase ? output.toUpperCase() : output);\r\n        return output;\r\n    }\r\n}\r\n```\r\n\r\nCurrently the \"outer\" stubbing cannot be completed correctly because the \"inner\" one starts using `MockingProgress` before `thenReturn` of the first stubbing is called.\r\nResult:\r\n```\r\norg.mockito.exceptions.misusing.UnfinishedStubbingException: \r\nUnfinished stubbing detected here:\r\n-> at my.package.NestedStubbingTest.test(NestedStubbingTest.java:20)\r\n\r\nE.g. thenReturn() may be missing.\r\nExamples of correct stubbing:\r\n    when(mock.isOk()).thenReturn(true);\r\n    when(mock.isOk()).thenThrow(exception);\r\n    doThrow(exception).when(mock).someVoidMethod();\r\nHints:\r\n 1. missing thenReturn()\r\n 2. you are trying to stub a final method, you naughty developer!\r\n 3: you are stubbing the behaviour of another mock inside before 'thenReturn' instruction if completed\r\n\r\n\r\n\tat at my.package.NestedStubbingTest.prepareOutput(NestedStubbingTest.java:25)\r\n\tat at my.package.NestedStubbingTest.test(NestedStubbingTest.java:20)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\r\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\r\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\r\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\r\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\r\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\r\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\r\n\tat org.testng.TestRunner.run(TestRunner.java:632)\r\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\r\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\r\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\r\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\r\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\r\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\r\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\r\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\r\n\tat org.testng.TestNG.run(TestNG.java:1064)\r\n\tat org.testng.IDEARemoteTestNG.run(IDEARemoteTestNG.java:72)\r\n\tat org.testng.RemoteTestNGStarter.main(RemoteTestNGStarter.java:127)\r\n```\r\n\r\nAn obvious workaround is to make those stubbing sequentially. But it makes such convenient methods as `prepareOutput()` impossible.\r\n\r\nMockito version: `1.10.19`.","closed_by":{"login":"TimvdLippe","id":5948271,"node_id":"MDQ6VXNlcjU5NDgyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/5948271?v=4","gravatar_id":"","url":"https://api.github.com/users/TimvdLippe","html_url":"https://github.com/TimvdLippe","followers_url":"https://api.github.com/users/TimvdLippe/followers","following_url":"https://api.github.com/users/TimvdLippe/following{/other_user}","gists_url":"https://api.github.com/users/TimvdLippe/gists{/gist_id}","starred_url":"https://api.github.com/users/TimvdLippe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TimvdLippe/subscriptions","organizations_url":"https://api.github.com/users/TimvdLippe/orgs","repos_url":"https://api.github.com/users/TimvdLippe/repos","events_url":"https://api.github.com/users/TimvdLippe/events{/privacy}","received_events_url":"https://api.github.com/users/TimvdLippe/received_events","type":"User","site_admin":false}}", "commentIds":["328296919","328461187","333660754"], "labels":[]}