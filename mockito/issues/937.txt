{"id":"937", "title":"'javadoc' task fails on Ubuntu 16.10", "body":"- Ubuntu 16.10
- Oracle JDK 1.8.0_121

```
$ git clone https://github.com/mockito/mockito.git
Cloning into 'mockito'...
remote: Counting objects: 62292, done.
remote: Compressing objects: 100% (169/169), done.
remote: Total 62292 (delta 37), reused 0 (delta 0), pack-reused 62097
Receiving objects: 100% (62292/62292), 42.02 MiB | 768.00 KiB/s, done.
Resolving deltas: 100% (28289/28289), done.
Checking connectivity... done.
$
$ cd mockito/
$ git branch -a
* release/2.x
  remotes/origin/HEAD -> origin/release/2.x
  remotes/origin/android-bundle
  remotes/origin/android-doc-improvement
  remotes/origin/benchmark-mockito-integration
  remotes/origin/feature/297-java8Marchers
  remotes/origin/feature/297-java8MarchersRafael
  remotes/origin/fix-for-issue-810
  remotes/origin/fix-javadoc-generation
  remotes/origin/gh-pages
  remotes/origin/issue-357
  remotes/origin/lambda-when
  remotes/origin/master
  remotes/origin/matcher-stack
  remotes/origin/release/2.x
  remotes/origin/strict-stubbing
  remotes/origin/tech/563-shouldRunInMultipleThreads
  remotes/origin/type-cache-failing-weirdness
$
$ java -version
java version \"1.8.0_121\"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)
$
$ ./gradlew clean build --stacktrace
Starting a new Gradle Daemon for this build (subsequent builds will be faster).
Parallel execution is an incubating feature.
Version: 2.7.6
:extTest:clean
:clean
:inline:clean
:android:clean
:extTest:compileJava
:testng:clean
:inline:processResources
:android:processResources
:extTest:compileJava UP-TO-DATE
:extTest:processResources UP-TO-DATE
:compileJava
:extTest:classes UP-TO-DATE
:inline:sourcesJar
:android:sourcesJar
:extTest:jar
:extTest:assemble
:extTest:processTestResources
:inline:processTestResources UP-TO-DATE
:inline:generatePomFileForMainJarPublication
:android:processTestResources UP-TO-DATE
:android:generatePomFileForMainJarPublication
:testng:processResources UP-TO-DATE
:testng:processTestResources
Note: Some input files use or override a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
:copyMockMethodDispatcher
:processResources UP-TO-DATE
:classes
:jar
:javadoc
:android:compileJava
:inline:compileJava UP-TO-DATE
:inline:classes
:testng:compileJava
:inline:jar
:inline:compileTestJava
:inline:testClasses
:inline:test
:android:classes
:android:jar
:android:compileTestJava UP-TO-DATE
:android:testClasses UP-TO-DATE
:android:test UP-TO-DATE
:android:check UP-TO-DATE
:testng:classes
:testng:jar
:testng:sourcesJar
:testng:assemble
:testng:compileTestJava
:testng:testClasses
:testng:test
:inline:check
:testng:check
:testng:build
:javadoc FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':javadoc'.
> Javadoc generation failed. Generated Javadoc options file (useful for troubleshooting): '/home/tmura/tmp/mockito/build/tmp/javadoc/javadoc.options'

* Try:
Run with --info or --debug option to get more log output.

* Exception is:
org.gradle.api.tasks.TaskExecutionException: Execution failed for task ':javadoc'.
        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeActions(ExecuteActionsTaskExecuter.java:69)
        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.execute(ExecuteActionsTaskExecuter.java:46)
        at org.gradle.api.internal.tasks.execution.PostExecutionAnalysisTaskExecuter.execute(PostExecutionAnalysisTaskExecuter.java:35)
        at org.gradle.api.internal.tasks.execution.SkipUpToDateTaskExecuter.execute(SkipUpToDateTaskExecuter.java:66)
        at org.gradle.api.internal.tasks.execution.ValidatingTaskExecuter.execute(ValidatingTaskExecuter.java:58)
        at org.gradle.api.internal.tasks.execution.SkipEmptySourceFilesTaskExecuter.execute(SkipEmptySourceFilesTaskExecuter.java:52)
        at org.gradle.api.internal.tasks.execution.SkipTaskWithNoActionsExecuter.execute(SkipTaskWithNoActionsExecuter.java:52)
        at org.gradle.api.internal.tasks.execution.SkipOnlyIfTaskExecuter.execute(SkipOnlyIfTaskExecuter.java:53)
        at org.gradle.api.internal.tasks.execution.ExecuteAtMostOnceTaskExecuter.execute(ExecuteAtMostOnceTaskExecuter.java:43)
        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter$EventFiringTaskWorker.execute(DefaultTaskGraphExecuter.java:203)
        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter$EventFiringTaskWorker.execute(DefaultTaskGraphExecuter.java:185)
        at org.gradle.execution.taskgraph.AbstractTaskPlanExecutor$TaskExecutorWorker.processTask(AbstractTaskPlanExecutor.java:66)
        at org.gradle.execution.taskgraph.AbstractTaskPlanExecutor$TaskExecutorWorker.run(AbstractTaskPlanExecutor.java:50)
        at org.gradle.execution.taskgraph.ParallelTaskPlanExecutor.process(ParallelTaskPlanExecutor.java:47)
        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter.execute(DefaultTaskGraphExecuter.java:110)
        at org.gradle.execution.SelectedTaskExecutionAction.execute(SelectedTaskExecutionAction.java:37)
        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:37)
        at org.gradle.execution.DefaultBuildExecuter.access$000(DefaultBuildExecuter.java:23)
        at org.gradle.execution.DefaultBuildExecuter$1.proceed(DefaultBuildExecuter.java:43)
        at org.gradle.execution.DryRunBuildExecutionAction.execute(DryRunBuildExecutionAction.java:32)
        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:37)
        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:30)
        at org.gradle.initialization.DefaultGradleLauncher$4.run(DefaultGradleLauncher.java:153)
        at org.gradle.internal.Factories$1.create(Factories.java:22)
        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:91)
        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:53)
        at org.gradle.initialization.DefaultGradleLauncher.doBuildStages(DefaultGradleLauncher.java:150)
        at org.gradle.initialization.DefaultGradleLauncher.access$200(DefaultGradleLauncher.java:32)
        at org.gradle.initialization.DefaultGradleLauncher$1.create(DefaultGradleLauncher.java:98)
        at org.gradle.initialization.DefaultGradleLauncher$1.create(DefaultGradleLauncher.java:92)
        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:91)
        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:63)
        at org.gradle.initialization.DefaultGradleLauncher.doBuild(DefaultGradleLauncher.java:92)
        at org.gradle.initialization.DefaultGradleLauncher.run(DefaultGradleLauncher.java:83)
        at org.gradle.launcher.exec.InProcessBuildActionExecuter$DefaultBuildController.run(InProcessBuildActionExecuter.java:99)
        at org.gradle.tooling.internal.provider.ExecuteBuildActionRunner.run(ExecuteBuildActionRunner.java:28)
        at org.gradle.launcher.exec.ChainingBuildActionRunner.run(ChainingBuildActionRunner.java:35)
        at org.gradle.launcher.exec.InProcessBuildActionExecuter.execute(InProcessBuildActionExecuter.java:48)
        at org.gradle.launcher.exec.InProcessBuildActionExecuter.execute(InProcessBuildActionExecuter.java:30)
        at org.gradle.launcher.exec.ContinuousBuildActionExecuter.execute(ContinuousBuildActionExecuter.java:81)
        at org.gradle.launcher.exec.ContinuousBuildActionExecuter.execute(ContinuousBuildActionExecuter.java:46)
        at org.gradle.launcher.daemon.server.exec.ExecuteBuild.doBuild(ExecuteBuild.java:52)
        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.WatchForDisconnection.execute(WatchForDisconnection.java:37)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.ResetDeprecationLogger.execute(ResetDeprecationLogger.java:26)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.RequestStopIfSingleUsedDaemon.execute(RequestStopIfSingleUsedDaemon.java:34)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:74)
        at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:72)
        at org.gradle.util.Swapper.swap(Swapper.java:38)
        at org.gradle.launcher.daemon.server.exec.ForwardClientInput.execute(ForwardClientInput.java:72)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.health.DaemonHealthTracker.execute(DaemonHealthTracker.java:47)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.LogToClient.doBuild(LogToClient.java:60)
        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.EstablishBuildEnvironment.doBuild(EstablishBuildEnvironment.java:72)
        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.health.HintGCAfterBuild.execute(HintGCAfterBuild.java:41)
        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)
        at org.gradle.launcher.daemon.server.exec.StartBuildOrRespondWithBusy$1.run(StartBuildOrRespondWithBusy.java:50)
        at org.gradle.launcher.daemon.server.DaemonStateCoordinator$1.run(DaemonStateCoordinator.java:237)
        at org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(ExecutorPolicy.java:54)
        at org.gradle.internal.concurrent.StoppableExecutorImpl$1.run(StoppableExecutorImpl.java:40)
Caused by: org.gradle.api.GradleException: Javadoc generation failed. Generated Javadoc options file (useful for troubleshooting): '/home/tmura/tmp/mockito/build/tmp/javadoc/javadoc.options'
        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:58)
        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:31)
        at org.gradle.api.tasks.javadoc.Javadoc.executeExternalJavadoc(Javadoc.java:143)
        at org.gradle.api.tasks.javadoc.Javadoc.generate(Javadoc.java:131)
        at org.gradle.internal.reflect.JavaMethod.invoke(JavaMethod.java:75)
        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.doExecute(AnnotationProcessingTaskFactory.java:228)
        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.execute(AnnotationProcessingTaskFactory.java:221)
        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.execute(AnnotationProcessingTaskFactory.java:210)
        at org.gradle.api.internal.AbstractTask$TaskActionWrapper.execute(AbstractTask.java:621)
        at org.gradle.api.internal.AbstractTask$TaskActionWrapper.execute(AbstractTask.java:604)
        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeAction(ExecuteActionsTaskExecuter.java:80)
        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeActions(ExecuteActionsTaskExecuter.java:61)
        ... 68 more
Caused by: org.gradle.process.internal.ExecException: Process 'command '/usr/lib/jvm/java-8-oracle/bin/javadoc'' finished with non-zero exit value 1
        at org.gradle.process.internal.DefaultExecHandle$ExecResultImpl.assertNormalExitValue(DefaultExecHandle.java:367)
        at org.gradle.process.internal.DefaultExecAction.execute(DefaultExecAction.java:31)
        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:53)
        ... 79 more


BUILD FAILED

Total time: 28.159 secs
$
$
```

javadoc.options:
[javadoc.options.txt](https://github.com/mockito/mockito/files/759699/javadoc.options.txt)

Do i need any additional settings?
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/937","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/937/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/937/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/937/events","html_url":"https://github.com/mockito/mockito/issues/937","id":206092358,"node_id":"MDU6SXNzdWUyMDYwOTIzNTg=","number":937,"title":"'javadoc' task fails on Ubuntu 16.10","user":{"login":"tmurakami","id":2504717,"node_id":"MDQ6VXNlcjI1MDQ3MTc=","avatar_url":"https://avatars3.githubusercontent.com/u/2504717?v=4","gravatar_id":"","url":"https://api.github.com/users/tmurakami","html_url":"https://github.com/tmurakami","followers_url":"https://api.github.com/users/tmurakami/followers","following_url":"https://api.github.com/users/tmurakami/following{/other_user}","gists_url":"https://api.github.com/users/tmurakami/gists{/gist_id}","starred_url":"https://api.github.com/users/tmurakami/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tmurakami/subscriptions","organizations_url":"https://api.github.com/users/tmurakami/orgs","repos_url":"https://api.github.com/users/tmurakami/repos","events_url":"https://api.github.com/users/tmurakami/events{/privacy}","received_events_url":"https://api.github.com/users/tmurakami/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-02-08T04:17:44Z","updated_at":"2017-02-08T10:09:56Z","closed_at":"2017-02-08T10:09:56Z","author_association":"CONTRIBUTOR","body":"- Ubuntu 16.10\r\n- Oracle JDK 1.8.0_121\r\n\r\n```\r\n$ git clone https://github.com/mockito/mockito.git\r\nCloning into 'mockito'...\r\nremote: Counting objects: 62292, done.\r\nremote: Compressing objects: 100% (169/169), done.\r\nremote: Total 62292 (delta 37), reused 0 (delta 0), pack-reused 62097\r\nReceiving objects: 100% (62292/62292), 42.02 MiB | 768.00 KiB/s, done.\r\nResolving deltas: 100% (28289/28289), done.\r\nChecking connectivity... done.\r\n$\r\n$ cd mockito/\r\n$ git branch -a\r\n* release/2.x\r\n  remotes/origin/HEAD -> origin/release/2.x\r\n  remotes/origin/android-bundle\r\n  remotes/origin/android-doc-improvement\r\n  remotes/origin/benchmark-mockito-integration\r\n  remotes/origin/feature/297-java8Marchers\r\n  remotes/origin/feature/297-java8MarchersRafael\r\n  remotes/origin/fix-for-issue-810\r\n  remotes/origin/fix-javadoc-generation\r\n  remotes/origin/gh-pages\r\n  remotes/origin/issue-357\r\n  remotes/origin/lambda-when\r\n  remotes/origin/master\r\n  remotes/origin/matcher-stack\r\n  remotes/origin/release/2.x\r\n  remotes/origin/strict-stubbing\r\n  remotes/origin/tech/563-shouldRunInMultipleThreads\r\n  remotes/origin/type-cache-failing-weirdness\r\n$\r\n$ java -version\r\njava version \"1.8.0_121\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_121-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)\r\n$\r\n$ ./gradlew clean build --stacktrace\r\nStarting a new Gradle Daemon for this build (subsequent builds will be faster).\r\nParallel execution is an incubating feature.\r\nVersion: 2.7.6\r\n:extTest:clean\r\n:clean\r\n:inline:clean\r\n:android:clean\r\n:extTest:compileJava\r\n:testng:clean\r\n:inline:processResources\r\n:android:processResources\r\n:extTest:compileJava UP-TO-DATE\r\n:extTest:processResources UP-TO-DATE\r\n:compileJava\r\n:extTest:classes UP-TO-DATE\r\n:inline:sourcesJar\r\n:android:sourcesJar\r\n:extTest:jar\r\n:extTest:assemble\r\n:extTest:processTestResources\r\n:inline:processTestResources UP-TO-DATE\r\n:inline:generatePomFileForMainJarPublication\r\n:android:processTestResources UP-TO-DATE\r\n:android:generatePomFileForMainJarPublication\r\n:testng:processResources UP-TO-DATE\r\n:testng:processTestResources\r\nNote: Some input files use or override a deprecated API.\r\nNote: Recompile with -Xlint:deprecation for details.\r\nNote: Some input files use unchecked or unsafe operations.\r\nNote: Recompile with -Xlint:unchecked for details.\r\n:copyMockMethodDispatcher\r\n:processResources UP-TO-DATE\r\n:classes\r\n:jar\r\n:javadoc\r\n:android:compileJava\r\n:inline:compileJava UP-TO-DATE\r\n:inline:classes\r\n:testng:compileJava\r\n:inline:jar\r\n:inline:compileTestJava\r\n:inline:testClasses\r\n:inline:test\r\n:android:classes\r\n:android:jar\r\n:android:compileTestJava UP-TO-DATE\r\n:android:testClasses UP-TO-DATE\r\n:android:test UP-TO-DATE\r\n:android:check UP-TO-DATE\r\n:testng:classes\r\n:testng:jar\r\n:testng:sourcesJar\r\n:testng:assemble\r\n:testng:compileTestJava\r\n:testng:testClasses\r\n:testng:test\r\n:inline:check\r\n:testng:check\r\n:testng:build\r\n:javadoc FAILED\r\n\r\nFAILURE: Build failed with an exception.\r\n\r\n* What went wrong:\r\nExecution failed for task ':javadoc'.\r\n> Javadoc generation failed. Generated Javadoc options file (useful for troubleshooting): '/home/tmura/tmp/mockito/build/tmp/javadoc/javadoc.options'\r\n\r\n* Try:\r\nRun with --info or --debug option to get more log output.\r\n\r\n* Exception is:\r\norg.gradle.api.tasks.TaskExecutionException: Execution failed for task ':javadoc'.\r\n        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeActions(ExecuteActionsTaskExecuter.java:69)\r\n        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.execute(ExecuteActionsTaskExecuter.java:46)\r\n        at org.gradle.api.internal.tasks.execution.PostExecutionAnalysisTaskExecuter.execute(PostExecutionAnalysisTaskExecuter.java:35)\r\n        at org.gradle.api.internal.tasks.execution.SkipUpToDateTaskExecuter.execute(SkipUpToDateTaskExecuter.java:66)\r\n        at org.gradle.api.internal.tasks.execution.ValidatingTaskExecuter.execute(ValidatingTaskExecuter.java:58)\r\n        at org.gradle.api.internal.tasks.execution.SkipEmptySourceFilesTaskExecuter.execute(SkipEmptySourceFilesTaskExecuter.java:52)\r\n        at org.gradle.api.internal.tasks.execution.SkipTaskWithNoActionsExecuter.execute(SkipTaskWithNoActionsExecuter.java:52)\r\n        at org.gradle.api.internal.tasks.execution.SkipOnlyIfTaskExecuter.execute(SkipOnlyIfTaskExecuter.java:53)\r\n        at org.gradle.api.internal.tasks.execution.ExecuteAtMostOnceTaskExecuter.execute(ExecuteAtMostOnceTaskExecuter.java:43)\r\n        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter$EventFiringTaskWorker.execute(DefaultTaskGraphExecuter.java:203)\r\n        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter$EventFiringTaskWorker.execute(DefaultTaskGraphExecuter.java:185)\r\n        at org.gradle.execution.taskgraph.AbstractTaskPlanExecutor$TaskExecutorWorker.processTask(AbstractTaskPlanExecutor.java:66)\r\n        at org.gradle.execution.taskgraph.AbstractTaskPlanExecutor$TaskExecutorWorker.run(AbstractTaskPlanExecutor.java:50)\r\n        at org.gradle.execution.taskgraph.ParallelTaskPlanExecutor.process(ParallelTaskPlanExecutor.java:47)\r\n        at org.gradle.execution.taskgraph.DefaultTaskGraphExecuter.execute(DefaultTaskGraphExecuter.java:110)\r\n        at org.gradle.execution.SelectedTaskExecutionAction.execute(SelectedTaskExecutionAction.java:37)\r\n        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:37)\r\n        at org.gradle.execution.DefaultBuildExecuter.access$000(DefaultBuildExecuter.java:23)\r\n        at org.gradle.execution.DefaultBuildExecuter$1.proceed(DefaultBuildExecuter.java:43)\r\n        at org.gradle.execution.DryRunBuildExecutionAction.execute(DryRunBuildExecutionAction.java:32)\r\n        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:37)\r\n        at org.gradle.execution.DefaultBuildExecuter.execute(DefaultBuildExecuter.java:30)\r\n        at org.gradle.initialization.DefaultGradleLauncher$4.run(DefaultGradleLauncher.java:153)\r\n        at org.gradle.internal.Factories$1.create(Factories.java:22)\r\n        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:91)\r\n        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:53)\r\n        at org.gradle.initialization.DefaultGradleLauncher.doBuildStages(DefaultGradleLauncher.java:150)\r\n        at org.gradle.initialization.DefaultGradleLauncher.access$200(DefaultGradleLauncher.java:32)\r\n        at org.gradle.initialization.DefaultGradleLauncher$1.create(DefaultGradleLauncher.java:98)\r\n        at org.gradle.initialization.DefaultGradleLauncher$1.create(DefaultGradleLauncher.java:92)\r\n        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:91)\r\n        at org.gradle.internal.progress.DefaultBuildOperationExecutor.run(DefaultBuildOperationExecutor.java:63)\r\n        at org.gradle.initialization.DefaultGradleLauncher.doBuild(DefaultGradleLauncher.java:92)\r\n        at org.gradle.initialization.DefaultGradleLauncher.run(DefaultGradleLauncher.java:83)\r\n        at org.gradle.launcher.exec.InProcessBuildActionExecuter$DefaultBuildController.run(InProcessBuildActionExecuter.java:99)\r\n        at org.gradle.tooling.internal.provider.ExecuteBuildActionRunner.run(ExecuteBuildActionRunner.java:28)\r\n        at org.gradle.launcher.exec.ChainingBuildActionRunner.run(ChainingBuildActionRunner.java:35)\r\n        at org.gradle.launcher.exec.InProcessBuildActionExecuter.execute(InProcessBuildActionExecuter.java:48)\r\n        at org.gradle.launcher.exec.InProcessBuildActionExecuter.execute(InProcessBuildActionExecuter.java:30)\r\n        at org.gradle.launcher.exec.ContinuousBuildActionExecuter.execute(ContinuousBuildActionExecuter.java:81)\r\n        at org.gradle.launcher.exec.ContinuousBuildActionExecuter.execute(ContinuousBuildActionExecuter.java:46)\r\n        at org.gradle.launcher.daemon.server.exec.ExecuteBuild.doBuild(ExecuteBuild.java:52)\r\n        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.WatchForDisconnection.execute(WatchForDisconnection.java:37)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.ResetDeprecationLogger.execute(ResetDeprecationLogger.java:26)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.RequestStopIfSingleUsedDaemon.execute(RequestStopIfSingleUsedDaemon.java:34)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:74)\r\n        at org.gradle.launcher.daemon.server.exec.ForwardClientInput$2.call(ForwardClientInput.java:72)\r\n        at org.gradle.util.Swapper.swap(Swapper.java:38)\r\n        at org.gradle.launcher.daemon.server.exec.ForwardClientInput.execute(ForwardClientInput.java:72)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.health.DaemonHealthTracker.execute(DaemonHealthTracker.java:47)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.LogToClient.doBuild(LogToClient.java:60)\r\n        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.EstablishBuildEnvironment.doBuild(EstablishBuildEnvironment.java:72)\r\n        at org.gradle.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:36)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.health.HintGCAfterBuild.execute(HintGCAfterBuild.java:41)\r\n        at org.gradle.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:120)\r\n        at org.gradle.launcher.daemon.server.exec.StartBuildOrRespondWithBusy$1.run(StartBuildOrRespondWithBusy.java:50)\r\n        at org.gradle.launcher.daemon.server.DaemonStateCoordinator$1.run(DaemonStateCoordinator.java:237)\r\n        at org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(ExecutorPolicy.java:54)\r\n        at org.gradle.internal.concurrent.StoppableExecutorImpl$1.run(StoppableExecutorImpl.java:40)\r\nCaused by: org.gradle.api.GradleException: Javadoc generation failed. Generated Javadoc options file (useful for troubleshooting): '/home/tmura/tmp/mockito/build/tmp/javadoc/javadoc.options'\r\n        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:58)\r\n        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:31)\r\n        at org.gradle.api.tasks.javadoc.Javadoc.executeExternalJavadoc(Javadoc.java:143)\r\n        at org.gradle.api.tasks.javadoc.Javadoc.generate(Javadoc.java:131)\r\n        at org.gradle.internal.reflect.JavaMethod.invoke(JavaMethod.java:75)\r\n        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.doExecute(AnnotationProcessingTaskFactory.java:228)\r\n        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.execute(AnnotationProcessingTaskFactory.java:221)\r\n        at org.gradle.api.internal.project.taskfactory.AnnotationProcessingTaskFactory$StandardTaskAction.execute(AnnotationProcessingTaskFactory.java:210)\r\n        at org.gradle.api.internal.AbstractTask$TaskActionWrapper.execute(AbstractTask.java:621)\r\n        at org.gradle.api.internal.AbstractTask$TaskActionWrapper.execute(AbstractTask.java:604)\r\n        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeAction(ExecuteActionsTaskExecuter.java:80)\r\n        at org.gradle.api.internal.tasks.execution.ExecuteActionsTaskExecuter.executeActions(ExecuteActionsTaskExecuter.java:61)\r\n        ... 68 more\r\nCaused by: org.gradle.process.internal.ExecException: Process 'command '/usr/lib/jvm/java-8-oracle/bin/javadoc'' finished with non-zero exit value 1\r\n        at org.gradle.process.internal.DefaultExecHandle$ExecResultImpl.assertNormalExitValue(DefaultExecHandle.java:367)\r\n        at org.gradle.process.internal.DefaultExecAction.execute(DefaultExecAction.java:31)\r\n        at org.gradle.api.tasks.javadoc.internal.JavadocGenerator.execute(JavadocGenerator.java:53)\r\n        ... 79 more\r\n\r\n\r\nBUILD FAILED\r\n\r\nTotal time: 28.159 secs\r\n$\r\n$\r\n```\r\n\r\njavadoc.options:\r\n[javadoc.options.txt](https://github.com/mockito/mockito/files/759699/javadoc.options.txt)\r\n\r\nDo i need any additional settings?\r\n","closed_by":{"login":"TimvdLippe","id":5948271,"node_id":"MDQ6VXNlcjU5NDgyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/5948271?v=4","gravatar_id":"","url":"https://api.github.com/users/TimvdLippe","html_url":"https://github.com/TimvdLippe","followers_url":"https://api.github.com/users/TimvdLippe/followers","following_url":"https://api.github.com/users/TimvdLippe/following{/other_user}","gists_url":"https://api.github.com/users/TimvdLippe/gists{/gist_id}","starred_url":"https://api.github.com/users/TimvdLippe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TimvdLippe/subscriptions","organizations_url":"https://api.github.com/users/TimvdLippe/orgs","repos_url":"https://api.github.com/users/TimvdLippe/repos","events_url":"https://api.github.com/users/TimvdLippe/events{/privacy}","received_events_url":"https://api.github.com/users/TimvdLippe/received_events","type":"User","site_admin":false}}", "commentIds":["278285809"], "labels":[]}