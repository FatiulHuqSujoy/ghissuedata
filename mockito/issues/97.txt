{"id":"97", "title":"Mockito won't build on a Windows machine", "body":"Executing gradlew pTLM results in the following:

```
(...)
:buildSrc:build UP-TO-DATE
Version: 1.10.45-dev
:compileJava UP-TO-DATE
:processResources UP-TO-DATE
:classes UP-TO-DATE
:allJar FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':allJar'.
> A problem occurred starting process 'command 'ant''

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

BUILD FAILED

Total time: 24.438 secs
```

The stacktrace ends with this:

```
Caused by: org.gradle.process.internal.ExecException: A problem occurred starting process 'command 'ant''
        at org.gradle.process.internal.DefaultExecHandle.setEndStateInfo(DefaultExecHandle.java:196)
        at org.gradle.process.internal.DefaultExecHandle.failed(DefaultExecHandle.java:325)
        at org.gradle.process.internal.ExecHandleRunner.run(ExecHandleRunner.java:83)
        ... 1 more
Caused by: net.rubygrapefruit.platform.NativeException: Could not start 'ant'
        at net.rubygrapefruit.platform.internal.DefaultProcessLauncher.start(DefaultProcessLauncher.java:27)
        at net.rubygrapefruit.platform.internal.WindowsProcessLauncher.start(WindowsProcessLauncher.java:22)
        at net.rubygrapefruit.platform.internal.WrapperProcessLauncher.start(WrapperProcessLauncher.java:36)
        at org.gradle.process.internal.ExecHandleRunner.run(ExecHandleRunner.java:65)
        ... 1 more
Caused by: java.io.IOException: Cannot run program \"ant\" (in directory \"C:\\projects\\mockito\\src\\mockito\"): CreateProcess
 error=2, The system cannot find the file specified
        at net.rubygrapefruit.platform.internal.DefaultProcessLauncher.start(DefaultProcessLauncher.java:25)
        ... 4 more
Caused by: java.io.IOException: CreateProcess error=2, The system cannot find the file specified
        ... 5 more
```

Not a big deal, but caused some trouble.
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/97","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/97/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/97/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/97/events","html_url":"https://github.com/mockito/mockito/issues/97","id":45148659,"node_id":"MDU6SXNzdWU0NTE0ODY1OQ==","number":97,"title":"Mockito won't build on a Windows machine","user":{"login":"jerzykrlk","id":4005877,"node_id":"MDQ6VXNlcjQwMDU4Nzc=","avatar_url":"https://avatars2.githubusercontent.com/u/4005877?v=4","gravatar_id":"","url":"https://api.github.com/users/jerzykrlk","html_url":"https://github.com/jerzykrlk","followers_url":"https://api.github.com/users/jerzykrlk/followers","following_url":"https://api.github.com/users/jerzykrlk/following{/other_user}","gists_url":"https://api.github.com/users/jerzykrlk/gists{/gist_id}","starred_url":"https://api.github.com/users/jerzykrlk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jerzykrlk/subscriptions","organizations_url":"https://api.github.com/users/jerzykrlk/orgs","repos_url":"https://api.github.com/users/jerzykrlk/repos","events_url":"https://api.github.com/users/jerzykrlk/events{/privacy}","received_events_url":"https://api.github.com/users/jerzykrlk/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2014-10-07T19:10:57Z","updated_at":"2014-10-09T20:40:52Z","closed_at":"2014-10-08T20:24:39Z","author_association":"CONTRIBUTOR","body":"Executing gradlew pTLM results in the following:\n\n```\n(...)\n:buildSrc:build UP-TO-DATE\nVersion: 1.10.45-dev\n:compileJava UP-TO-DATE\n:processResources UP-TO-DATE\n:classes UP-TO-DATE\n:allJar FAILED\n\nFAILURE: Build failed with an exception.\n\n* What went wrong:\nExecution failed for task ':allJar'.\n> A problem occurred starting process 'command 'ant''\n\n* Try:\nRun with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.\n\nBUILD FAILED\n\nTotal time: 24.438 secs\n```\n\nThe stacktrace ends with this:\n\n```\nCaused by: org.gradle.process.internal.ExecException: A problem occurred starting process 'command 'ant''\n        at org.gradle.process.internal.DefaultExecHandle.setEndStateInfo(DefaultExecHandle.java:196)\n        at org.gradle.process.internal.DefaultExecHandle.failed(DefaultExecHandle.java:325)\n        at org.gradle.process.internal.ExecHandleRunner.run(ExecHandleRunner.java:83)\n        ... 1 more\nCaused by: net.rubygrapefruit.platform.NativeException: Could not start 'ant'\n        at net.rubygrapefruit.platform.internal.DefaultProcessLauncher.start(DefaultProcessLauncher.java:27)\n        at net.rubygrapefruit.platform.internal.WindowsProcessLauncher.start(WindowsProcessLauncher.java:22)\n        at net.rubygrapefruit.platform.internal.WrapperProcessLauncher.start(WrapperProcessLauncher.java:36)\n        at org.gradle.process.internal.ExecHandleRunner.run(ExecHandleRunner.java:65)\n        ... 1 more\nCaused by: java.io.IOException: Cannot run program \"ant\" (in directory \"C:\\projects\\mockito\\src\\mockito\"): CreateProcess\n error=2, The system cannot find the file specified\n        at net.rubygrapefruit.platform.internal.DefaultProcessLauncher.start(DefaultProcessLauncher.java:25)\n        ... 4 more\nCaused by: java.io.IOException: CreateProcess error=2, The system cannot find the file specified\n        ... 5 more\n```\n\nNot a big deal, but caused some trouble.\n","closed_by":{"login":"mockitoguy","id":24743,"node_id":"MDQ6VXNlcjI0NzQz","avatar_url":"https://avatars2.githubusercontent.com/u/24743?v=4","gravatar_id":"","url":"https://api.github.com/users/mockitoguy","html_url":"https://github.com/mockitoguy","followers_url":"https://api.github.com/users/mockitoguy/followers","following_url":"https://api.github.com/users/mockitoguy/following{/other_user}","gists_url":"https://api.github.com/users/mockitoguy/gists{/gist_id}","starred_url":"https://api.github.com/users/mockitoguy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mockitoguy/subscriptions","organizations_url":"https://api.github.com/users/mockitoguy/orgs","repos_url":"https://api.github.com/users/mockitoguy/repos","events_url":"https://api.github.com/users/mockitoguy/events{/privacy}","received_events_url":"https://api.github.com/users/mockitoguy/received_events","type":"User","site_admin":false}}", "commentIds":["58408133","58416541","58421289","58559558","58574056"], "labels":[]}