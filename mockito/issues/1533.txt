{"id":"1533", "title":"Cross-references and a single spy cause memory leak", "body":"Not entirely sure, but I think that Mockito does not handle cross-references well. I have a semi-complex sample that proves this, but at the same time I’m not sure it is a Mockito failure and especially a fixable one. Suggestions how to avoid this behavior in general will be very helpful!

BTW I can provide a `.hprof` file if you are interested.

#### Versions

```
org.mockito:mockito-core:2.22.0
org.mockito:mockito-inline:2.22.0
```
```
java version \"1.8.0_181\"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```

#### Gradle

Heap is set to 64 MB.

```groovy
tasks.withType<Test> {
    maxHeapSize = \"64m\"
    jvmArgs(\"-XX:+HeapDumpOnOutOfMemoryError\")

    failFast = true
}
```
```
$ ./gradlew :module:cleanTestDebugUnitTest :module:testDebugUnitTest --tests \"com.github.sample.SubscriptionMemoryLeakSpec\"
```

#### Code

```kotlin
package com.github.sample

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(JUnitPlatform::class)
class SubscriptionMemoryLeakSpec : Spek({

    repeat(1_000) { iteration ->

        it(\"iteration $iteration\") {
            // Remove Mockito.spy and OOM disappears (even without component.unbind).
            val service = Mockito.spy(Service())
            val memoryConsumingService = MemoryConsumingService()

            val component = Component(service, memoryConsumingService)

            component.bind()

            // Uncomment the following line and OOM disappears.
            // component.unbind()
        }

    }

}) {

    class Service {
        val stream = PublishRelay.create<Unit>().toSerialized()
    }

    class MemoryConsumingService {
        // See at as a mass to fill the RAM.
        val streams = (0..1_000).map { BehaviorRelay.create<Int>() }
    }

    class Component(
            private val service: Service,
            private val memoryConsumingService: MemoryConsumingService
    ) {

        private val disposable = CompositeDisposable()

        fun bind() {
            disposable += service.stream.subscribe {
                // This closure keeps a reference to Component.
                memoryConsumingService.streams.size
            }
        }

        fun unbind() = disposable.clear()
    }

}
```
```
> Task :module:testDebugUnitTest
java.lang.OutOfMemoryError: GC overhead limit exceeded
Dumping heap to java_pid31753.hprof ...
Heap dump file created [96660586 bytes in 0.332 secs]

com.github.sample.SubscriptionMemoryLeakSpec > it iteration 260 STANDARD_ERROR
    java.lang.OutOfMemoryError: GC overhead limit exceeded
    	at java.util.concurrent.locks.ReentrantReadWriteLock$Sync.<init>(ReentrantReadWriteLock.java:338)
    	at java.util.concurrent.locks.ReentrantReadWriteLock$NonfairSync.<init>(ReentrantReadWriteLock.java:669)
    	at java.util.concurrent.locks.ReentrantReadWriteLock.<init>(ReentrantReadWriteLock.java:240)
    	at java.util.concurrent.locks.ReentrantReadWriteLock.<init>(ReentrantReadWriteLock.java:230)
    	at com.jakewharton.rxrelay2.BehaviorRelay.<init>(BehaviorRelay.java:99)
    	at com.jakewharton.rxrelay2.BehaviorRelay.create(BehaviorRelay.java:77)
    	at com.github.sample.SubscriptionMemoryLeakSpec$MemoryConsumingService.<init>(SubscriptionMemoryLeakSpec.kt:41)
*** java.lang.instrument ASSERTION FAILED ***: \"!errorOutstanding\" with message can't create byte arrau at JPLISAgent.c line: 813
    	at com.github.sample.SubscriptionMemoryLeakSpec$1$1$1.invoke(SubscriptionMemoryLeakSpec.kt:21)
    	at com.github.sample.SubscriptionMemoryLeakSpec$1$1$1.invoke(SubscriptionMemoryLeakSpec.kt:14)
    	at org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:102)
    	at org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:80)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:105)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)
    	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)
    	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService$$Lambda$89/1263089654.accept(Unknown Source)
    	at java.util.ArrayList.forEach(ArrayList.java:1257)
    	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:38)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:110)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)
    	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)
    	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService$$Lambda$89/1263089654.accept(Unknown Source)
    	at java.util.ArrayList.forEach(ArrayList.java:1257)
    	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:38)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:110)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)
    	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)
    	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)

com.github.sample.SubscriptionMemoryLeakSpec > it iteration 260 FAILED
    java.lang.OutOfMemoryError


261 tests completed, 1 failed
> Task :module:testDebugUnitTest FAILED
82 actionable tasks: 5 executed, 77 up-to-date
```

#### Eclipse Memory Analyzer

<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 57 52\" src=\"https://user-images.githubusercontent.com/200401/48541708-4991d080-e8ce-11e8-9120-7e1b2bfee689.png\">
<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 58 42\" src=\"https://user-images.githubusercontent.com/200401/48541709-4991d080-e8ce-11e8-8c3c-f427f92b4ca4.png\">
<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 59 17\" src=\"https://user-images.githubusercontent.com/200401/48541710-4991d080-e8ce-11e8-8927-ee310073996c.png\">
<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 59 58\" src=\"https://user-images.githubusercontent.com/200401/48541711-4a2a6700-e8ce-11e8-9f1f-40d61ff6e6b8.png\">
<img width=\"1513\" alt=\"screen shot 2018-11-15 at 12 01 25\" src=\"https://user-images.githubusercontent.com/200401/48541712-4a2a6700-e8ce-11e8-864c-287570e8de99.png\">

---

Seems like this happens:

* `Service` is a spy and is being passed to `Component` along with `MemoryConsumingService`.
* Since `Service` is a spy it is being held by Mockito.
* `Component` subscribes to `Service.stream`. The `subscribe` closure captures `Component`, `Service` and `MemoryConsumingService`. Due to RxJava specifics `Service.stream` holds all of these thanks to the closure.
* Since `Service` is being held by Mockito (since it is a spy) and by itself (due to the `subscribe` closure) Mockito does not release it.

However:

* Releasing the subscription via `component.unbind()` removes a reference, so Mockito releases it and there is no OOM.
* Avoiding making `Service` a spy eliminates OOM as well, i. e. cross-references are not an issue for the JVM itself.", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1533","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1533/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1533/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1533/events","html_url":"https://github.com/mockito/mockito/issues/1533","id":381065869,"node_id":"MDU6SXNzdWUzODEwNjU4Njk=","number":1533,"title":"Cross-references and a single spy cause memory leak","user":{"login":"ming13","id":200401,"node_id":"MDQ6VXNlcjIwMDQwMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/200401?v=4","gravatar_id":"","url":"https://api.github.com/users/ming13","html_url":"https://github.com/ming13","followers_url":"https://api.github.com/users/ming13/followers","following_url":"https://api.github.com/users/ming13/following{/other_user}","gists_url":"https://api.github.com/users/ming13/gists{/gist_id}","starred_url":"https://api.github.com/users/ming13/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ming13/subscriptions","organizations_url":"https://api.github.com/users/ming13/orgs","repos_url":"https://api.github.com/users/ming13/repos","events_url":"https://api.github.com/users/ming13/events{/privacy}","received_events_url":"https://api.github.com/users/ming13/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-11-15T09:13:45Z","updated_at":"2019-04-18T09:32:39Z","closed_at":"2019-03-05T16:10:24Z","author_association":"NONE","body":"Not entirely sure, but I think that Mockito does not handle cross-references well. I have a semi-complex sample that proves this, but at the same time I’m not sure it is a Mockito failure and especially a fixable one. Suggestions how to avoid this behavior in general will be very helpful!\r\n\r\nBTW I can provide a `.hprof` file if you are interested.\r\n\r\n#### Versions\r\n\r\n```\r\norg.mockito:mockito-core:2.22.0\r\norg.mockito:mockito-inline:2.22.0\r\n```\r\n```\r\njava version \"1.8.0_181\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_181-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)\r\n```\r\n\r\n#### Gradle\r\n\r\nHeap is set to 64 MB.\r\n\r\n```groovy\r\ntasks.withType<Test> {\r\n    maxHeapSize = \"64m\"\r\n    jvmArgs(\"-XX:+HeapDumpOnOutOfMemoryError\")\r\n\r\n    failFast = true\r\n}\r\n```\r\n```\r\n$ ./gradlew :module:cleanTestDebugUnitTest :module:testDebugUnitTest --tests \"com.github.sample.SubscriptionMemoryLeakSpec\"\r\n```\r\n\r\n#### Code\r\n\r\n```kotlin\r\npackage com.github.sample\r\n\r\nimport com.jakewharton.rxrelay2.BehaviorRelay\r\nimport com.jakewharton.rxrelay2.PublishRelay\r\nimport io.reactivex.disposables.CompositeDisposable\r\nimport org.jetbrains.spek.api.Spek\r\nimport org.jetbrains.spek.api.dsl.it\r\nimport org.junit.platform.runner.JUnitPlatform\r\nimport org.junit.runner.RunWith\r\nimport org.mockito.Mockito\r\n\r\n@RunWith(JUnitPlatform::class)\r\nclass SubscriptionMemoryLeakSpec : Spek({\r\n\r\n    repeat(1_000) { iteration ->\r\n\r\n        it(\"iteration $iteration\") {\r\n            // Remove Mockito.spy and OOM disappears (even without component.unbind).\r\n            val service = Mockito.spy(Service())\r\n            val memoryConsumingService = MemoryConsumingService()\r\n\r\n            val component = Component(service, memoryConsumingService)\r\n\r\n            component.bind()\r\n\r\n            // Uncomment the following line and OOM disappears.\r\n            // component.unbind()\r\n        }\r\n\r\n    }\r\n\r\n}) {\r\n\r\n    class Service {\r\n        val stream = PublishRelay.create<Unit>().toSerialized()\r\n    }\r\n\r\n    class MemoryConsumingService {\r\n        // See at as a mass to fill the RAM.\r\n        val streams = (0..1_000).map { BehaviorRelay.create<Int>() }\r\n    }\r\n\r\n    class Component(\r\n            private val service: Service,\r\n            private val memoryConsumingService: MemoryConsumingService\r\n    ) {\r\n\r\n        private val disposable = CompositeDisposable()\r\n\r\n        fun bind() {\r\n            disposable += service.stream.subscribe {\r\n                // This closure keeps a reference to Component.\r\n                memoryConsumingService.streams.size\r\n            }\r\n        }\r\n\r\n        fun unbind() = disposable.clear()\r\n    }\r\n\r\n}\r\n```\r\n```\r\n> Task :module:testDebugUnitTest\r\njava.lang.OutOfMemoryError: GC overhead limit exceeded\r\nDumping heap to java_pid31753.hprof ...\r\nHeap dump file created [96660586 bytes in 0.332 secs]\r\n\r\ncom.github.sample.SubscriptionMemoryLeakSpec > it iteration 260 STANDARD_ERROR\r\n    java.lang.OutOfMemoryError: GC overhead limit exceeded\r\n    \tat java.util.concurrent.locks.ReentrantReadWriteLock$Sync.<init>(ReentrantReadWriteLock.java:338)\r\n    \tat java.util.concurrent.locks.ReentrantReadWriteLock$NonfairSync.<init>(ReentrantReadWriteLock.java:669)\r\n    \tat java.util.concurrent.locks.ReentrantReadWriteLock.<init>(ReentrantReadWriteLock.java:240)\r\n    \tat java.util.concurrent.locks.ReentrantReadWriteLock.<init>(ReentrantReadWriteLock.java:230)\r\n    \tat com.jakewharton.rxrelay2.BehaviorRelay.<init>(BehaviorRelay.java:99)\r\n    \tat com.jakewharton.rxrelay2.BehaviorRelay.create(BehaviorRelay.java:77)\r\n    \tat com.github.sample.SubscriptionMemoryLeakSpec$MemoryConsumingService.<init>(SubscriptionMemoryLeakSpec.kt:41)\r\n*** java.lang.instrument ASSERTION FAILED ***: \"!errorOutstanding\" with message can't create byte arrau at JPLISAgent.c line: 813\r\n    \tat com.github.sample.SubscriptionMemoryLeakSpec$1$1$1.invoke(SubscriptionMemoryLeakSpec.kt:21)\r\n    \tat com.github.sample.SubscriptionMemoryLeakSpec$1$1$1.invoke(SubscriptionMemoryLeakSpec.kt:14)\r\n    \tat org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:102)\r\n    \tat org.jetbrains.spek.engine.Scope$Test.execute(Scope.kt:80)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:105)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)\r\n    \tat org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)\r\n    \tat org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService$$Lambda$89/1263089654.accept(Unknown Source)\r\n    \tat java.util.ArrayList.forEach(ArrayList.java:1257)\r\n    \tat org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:38)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:110)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)\r\n    \tat org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)\r\n    \tat org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService$$Lambda$89/1263089654.accept(Unknown Source)\r\n    \tat java.util.ArrayList.forEach(ArrayList.java:1257)\r\n    \tat org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:38)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$5(NodeTestTask.java:110)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask$$Lambda$82/1204954813.execute(Unknown Source)\r\n    \tat org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:72)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:95)\r\n    \tat org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:71)\r\n\r\ncom.github.sample.SubscriptionMemoryLeakSpec > it iteration 260 FAILED\r\n    java.lang.OutOfMemoryError\r\n\r\n\r\n261 tests completed, 1 failed\r\n> Task :module:testDebugUnitTest FAILED\r\n82 actionable tasks: 5 executed, 77 up-to-date\r\n```\r\n\r\n#### Eclipse Memory Analyzer\r\n\r\n<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 57 52\" src=\"https://user-images.githubusercontent.com/200401/48541708-4991d080-e8ce-11e8-9120-7e1b2bfee689.png\">\r\n<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 58 42\" src=\"https://user-images.githubusercontent.com/200401/48541709-4991d080-e8ce-11e8-8c3c-f427f92b4ca4.png\">\r\n<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 59 17\" src=\"https://user-images.githubusercontent.com/200401/48541710-4991d080-e8ce-11e8-8927-ee310073996c.png\">\r\n<img width=\"1513\" alt=\"screen shot 2018-11-15 at 11 59 58\" src=\"https://user-images.githubusercontent.com/200401/48541711-4a2a6700-e8ce-11e8-9f1f-40d61ff6e6b8.png\">\r\n<img width=\"1513\" alt=\"screen shot 2018-11-15 at 12 01 25\" src=\"https://user-images.githubusercontent.com/200401/48541712-4a2a6700-e8ce-11e8-864c-287570e8de99.png\">\r\n\r\n---\r\n\r\nSeems like this happens:\r\n\r\n* `Service` is a spy and is being passed to `Component` along with `MemoryConsumingService`.\r\n* Since `Service` is a spy it is being held by Mockito.\r\n* `Component` subscribes to `Service.stream`. The `subscribe` closure captures `Component`, `Service` and `MemoryConsumingService`. Due to RxJava specifics `Service.stream` holds all of these thanks to the closure.\r\n* Since `Service` is being held by Mockito (since it is a spy) and by itself (due to the `subscribe` closure) Mockito does not release it.\r\n\r\nHowever:\r\n\r\n* Releasing the subscription via `component.unbind()` removes a reference, so Mockito releases it and there is no OOM.\r\n* Avoiding making `Service` a spy eliminates OOM as well, i. e. cross-references are not an issue for the JVM itself.","closed_by":{"login":"mockitoguy","id":24743,"node_id":"MDQ6VXNlcjI0NzQz","avatar_url":"https://avatars2.githubusercontent.com/u/24743?v=4","gravatar_id":"","url":"https://api.github.com/users/mockitoguy","html_url":"https://github.com/mockitoguy","followers_url":"https://api.github.com/users/mockitoguy/followers","following_url":"https://api.github.com/users/mockitoguy/following{/other_user}","gists_url":"https://api.github.com/users/mockitoguy/gists{/gist_id}","starred_url":"https://api.github.com/users/mockitoguy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mockitoguy/subscriptions","organizations_url":"https://api.github.com/users/mockitoguy/orgs","repos_url":"https://api.github.com/users/mockitoguy/repos","events_url":"https://api.github.com/users/mockitoguy/events{/privacy}","received_events_url":"https://api.github.com/users/mockitoguy/received_events","type":"User","site_admin":false}}", "commentIds":["439057714","440997928","441297724","441381269","473190632","484426498"], "labels":[]}