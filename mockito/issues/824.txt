{"id":"824", "title":"Exception when using mock-maker-inline", "body":"I was trying out `mock-maker-inline` feature with the following example:
```java
import static org.mockito.Mockito.*;

final class FinalClass {
  int m1() {
    throw new RuntimeException(\"42\");
  }
}

public class TestMockFinal {
  public static void main(String[] args) {
    FinalClass cl = mock(FinalClass.class);
    when(cl.m1()).thenReturn(-1);
    if (cl.m1() != -1) {
      throw new Error(\"Not mocked!\");
    }
  }
}
```

When I run this code I get the following error:
```
Exception in thread \"main\" java.lang.ExceptionInInitializerError
	at org.mockito.internal.util.MockUtil.<clinit>(MockUtil.java:24)
	at org.mockito.internal.util.MockCreationValidator.validateType(MockCreationValidator.java:22)
	at org.mockito.internal.creation.MockSettingsImpl.validatedSettings(MockSettingsImpl.java:168)
	at org.mockito.internal.creation.MockSettingsImpl.confirm(MockSettingsImpl.java:162)
	at org.mockito.internal.MockitoCore.mock(MockitoCore.java:64)
	at org.mockito.Mockito.mock(Mockito.java:1665)
	at org.mockito.Mockito.mock(Mockito.java:1578)
	at TestMockFinal.main(TestMockFinal.java:11)
Caused by: java.lang.IllegalStateException: Failed to load interface org.mockito.plugins.MockMaker implementation declared in sun.misc.CompoundEnumeration@379619aa
	at org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:86)
	at org.mockito.internal.configuration.plugins.PluginLoader.loadPlugin(PluginLoader.java:40)
	at org.mockito.internal.configuration.plugins.PluginRegistry.<init>(PluginRegistry.java:18)
	at org.mockito.internal.configuration.plugins.Plugins.<clinit>(Plugins.java:17)
	... 8 more
Caused by: java.lang.IllegalStateException: Error during attachment using: ByteBuddyAgent.AttachmentProvider.Compound{attachmentProviders=[ByteBuddyAgent.AttachmentProvider.ForJigsawVm.INSTANCE, ByteBuddyAgent.AttachmentProvider.ForJ9Vm.INSTANCE, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.JVM_ROOT, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.JDK_ROOT, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.MACINTOSH]}
	at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:314)
	at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:280)
	at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:248)
	at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:234)
	at org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<init>(InlineByteBuddyMockMaker.java:101)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
	at java.lang.Class.newInstance(Class.java:442)
	at org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:81)
	... 11 more
Caused by: java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:301)
	... 21 more
Caused by: com.sun.tools.attach.AttachNotSupportedException: no providers installed
	at com.sun.tools.attach.VirtualMachine.attach(VirtualMachine.java:208)
	... 26 more
```

Environment:
- Windows 7 (64 bit)
- JDK 1.8.0_112 (64 bit)
```
java version \"1.8.0_112\"
Java(TM) SE Runtime Environment (build 1.8.0_112-b15)
Java HotSpot(TM) 64-Bit Server VM (build 25.112-b15, mixed mode)
```
- Mockito 2.3.4
- ByteBuddy 1.5.5 (transitive from Mockito)
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/824","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/824/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/824/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/824/events","html_url":"https://github.com/mockito/mockito/issues/824","id":195766713,"node_id":"MDU6SXNzdWUxOTU3NjY3MTM=","number":824,"title":"Exception when using mock-maker-inline","user":{"login":"vyazelenko","id":696855,"node_id":"MDQ6VXNlcjY5Njg1NQ==","avatar_url":"https://avatars0.githubusercontent.com/u/696855?v=4","gravatar_id":"","url":"https://api.github.com/users/vyazelenko","html_url":"https://github.com/vyazelenko","followers_url":"https://api.github.com/users/vyazelenko/followers","following_url":"https://api.github.com/users/vyazelenko/following{/other_user}","gists_url":"https://api.github.com/users/vyazelenko/gists{/gist_id}","starred_url":"https://api.github.com/users/vyazelenko/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/vyazelenko/subscriptions","organizations_url":"https://api.github.com/users/vyazelenko/orgs","repos_url":"https://api.github.com/users/vyazelenko/repos","events_url":"https://api.github.com/users/vyazelenko/events{/privacy}","received_events_url":"https://api.github.com/users/vyazelenko/received_events","type":"User","site_admin":false},"labels":[{"id":450180641,"node_id":"MDU6TGFiZWw0NTAxODA2NDE=","url":"https://api.github.com/repos/mockito/mockito/labels/final-class-or-methods","name":"final-class-or-methods","color":"fbca04","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":16,"created_at":"2016-12-15T10:18:03Z","updated_at":"2017-02-10T20:05:18Z","closed_at":"2016-12-16T23:43:21Z","author_association":"NONE","body":"I was trying out `mock-maker-inline` feature with the following example:\r\n```java\r\nimport static org.mockito.Mockito.*;\r\n\r\nfinal class FinalClass {\r\n  int m1() {\r\n    throw new RuntimeException(\"42\");\r\n  }\r\n}\r\n\r\npublic class TestMockFinal {\r\n  public static void main(String[] args) {\r\n    FinalClass cl = mock(FinalClass.class);\r\n    when(cl.m1()).thenReturn(-1);\r\n    if (cl.m1() != -1) {\r\n      throw new Error(\"Not mocked!\");\r\n    }\r\n  }\r\n}\r\n```\r\n\r\nWhen I run this code I get the following error:\r\n```\r\nException in thread \"main\" java.lang.ExceptionInInitializerError\r\n\tat org.mockito.internal.util.MockUtil.<clinit>(MockUtil.java:24)\r\n\tat org.mockito.internal.util.MockCreationValidator.validateType(MockCreationValidator.java:22)\r\n\tat org.mockito.internal.creation.MockSettingsImpl.validatedSettings(MockSettingsImpl.java:168)\r\n\tat org.mockito.internal.creation.MockSettingsImpl.confirm(MockSettingsImpl.java:162)\r\n\tat org.mockito.internal.MockitoCore.mock(MockitoCore.java:64)\r\n\tat org.mockito.Mockito.mock(Mockito.java:1665)\r\n\tat org.mockito.Mockito.mock(Mockito.java:1578)\r\n\tat TestMockFinal.main(TestMockFinal.java:11)\r\nCaused by: java.lang.IllegalStateException: Failed to load interface org.mockito.plugins.MockMaker implementation declared in sun.misc.CompoundEnumeration@379619aa\r\n\tat org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:86)\r\n\tat org.mockito.internal.configuration.plugins.PluginLoader.loadPlugin(PluginLoader.java:40)\r\n\tat org.mockito.internal.configuration.plugins.PluginRegistry.<init>(PluginRegistry.java:18)\r\n\tat org.mockito.internal.configuration.plugins.Plugins.<clinit>(Plugins.java:17)\r\n\t... 8 more\r\nCaused by: java.lang.IllegalStateException: Error during attachment using: ByteBuddyAgent.AttachmentProvider.Compound{attachmentProviders=[ByteBuddyAgent.AttachmentProvider.ForJigsawVm.INSTANCE, ByteBuddyAgent.AttachmentProvider.ForJ9Vm.INSTANCE, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.JVM_ROOT, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.JDK_ROOT, ByteBuddyAgent.AttachmentProvider.ForToolsJarVm.MACINTOSH]}\r\n\tat net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:314)\r\n\tat net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:280)\r\n\tat net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:248)\r\n\tat net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:234)\r\n\tat org.mockito.internal.creation.bytebuddy.InlineByteBuddyMockMaker.<init>(InlineByteBuddyMockMaker.java:101)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat java.lang.Class.newInstance(Class.java:442)\r\n\tat org.mockito.internal.configuration.plugins.PluginLoader.loadImpl(PluginLoader.java:81)\r\n\t... 11 more\r\nCaused by: java.lang.reflect.InvocationTargetException\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat net.bytebuddy.agent.ByteBuddyAgent.install(ByteBuddyAgent.java:301)\r\n\t... 21 more\r\nCaused by: com.sun.tools.attach.AttachNotSupportedException: no providers installed\r\n\tat com.sun.tools.attach.VirtualMachine.attach(VirtualMachine.java:208)\r\n\t... 26 more\r\n```\r\n\r\nEnvironment:\r\n- Windows 7 (64 bit)\r\n- JDK 1.8.0_112 (64 bit)\r\n```\r\njava version \"1.8.0_112\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_112-b15)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.112-b15, mixed mode)\r\n```\r\n- Mockito 2.3.4\r\n- ByteBuddy 1.5.5 (transitive from Mockito)\r\n","closed_by":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}}", "commentIds":["267337503","267447783","267455354","267456912","267558793","267559326","267571741","267572605","267573120","267632908","267684110","267720035","267724715","267761438","267818421","279052176"], "labels":["final-class-or-methods"]}