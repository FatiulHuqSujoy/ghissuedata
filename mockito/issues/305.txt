{"id":"305", "title":"mocking method an interface inherits from its parent interface is not working when the method is re-stated in extending interface", "body":"I was trying to build a function that can return a mock of any interface extending a generic parent interface. The extending interface specifies the generic parameters.

I've found that if the extending interface does not re-declare the method, then it works as expected (the mock object exhibits mocked behavior when the method is invoked.) 

If the extending interface does re-declare the method, then the mock object does not exhibit the mocked behavior when the method is invoked on the mock.

Hopefully the example below helps clarify:

```
package mockitotest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mockito;

// Test case demonstrating a perceived defect in Mockito
public class MockBuilderTest {

    // 1. Create an object to use for a return type
    public class ReturnType {

        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    // 2. Define an interface
    public interface OriginalInterface<N,O> {

        // contains a templated method
        O doSomething(N n);

    };

    // 3. Create an extending interface which specifies the templated arguments
    public interface ExtendingInterfaceObject extends OriginalInterface<String, ReturnType> {

        // 4. The method would be inherited, but I re-state it, mostly to add additional
        //      documentation inside the interface
        @Override
        ReturnType doSomething(String n);

    };

    // 5. For demonstration purposes, create an extending interface which does specify the
    //      templated arguments, but does not re-state the method
    public interface ExtendingInterfaceObjectNoRedef extends OriginalInterface<String, ReturnType> {}

    // A function to build a mocked object, the type of object is specified as a method argument
    // the value that should be returned (O) is also provided as an argument
    <N,O,T extends OriginalInterface<N,O>> T buildMock(Class<T> cls, final O o) {
        Class<N> n = null;

        // create a mocked object of the specified class
        T t = Mockito.mock(cls, Mockito.withSettings().verboseLogging());

        // override the doSomething function to return the desired value
        Mockito.when(t.doSomething(Mockito.any(n))).thenReturn(o);
        return t;
    }

    // Example A: I would expect that 'r' be returned by the resulting mocked object, but
    //      mockito doesn't seem to match up the invocation with an overriden method
    @Test
    public void testBuild_object() {
        ReturnType r = new ReturnType();
        r.setName(\"testBuild\");

        ExtendingInterfaceObject i = buildMock(ExtendingInterfaceObject.class, r);

        assertTrue(i instanceof ExtendingInterfaceObject);
        assertEquals(r, i.doSomething(\"TEST\"));
    }

    // Example B: like example A, but this uses the extending interface which *does not*
    //      re-state the method. This example completes successfully.
    @Test
    public void testBuild_object_no_redef() {
        ReturnType r = new ReturnType();
        r.setName(\"testBuild\");

        ExtendingInterfaceObjectNoRedef i = buildMock(ExtendingInterfaceObjectNoRedef.class, r);

        assertTrue(i instanceof ExtendingInterfaceObjectNoRedef);
        assertEquals(r, i.doSomething(\"TEST\"));
    }

}
```
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/305","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/305/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/305/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/305/events","html_url":"https://github.com/mockito/mockito/issues/305","id":112172296,"node_id":"MDU6SXNzdWUxMTIxNzIyOTY=","number":305,"title":"mocking method an interface inherits from its parent interface is not working when the method is re-stated in extending interface","user":{"login":"ryanluedders","id":10035257,"node_id":"MDQ6VXNlcjEwMDM1MjU3","avatar_url":"https://avatars0.githubusercontent.com/u/10035257?v=4","gravatar_id":"","url":"https://api.github.com/users/ryanluedders","html_url":"https://github.com/ryanluedders","followers_url":"https://api.github.com/users/ryanluedders/followers","following_url":"https://api.github.com/users/ryanluedders/following{/other_user}","gists_url":"https://api.github.com/users/ryanluedders/gists{/gist_id}","starred_url":"https://api.github.com/users/ryanluedders/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ryanluedders/subscriptions","organizations_url":"https://api.github.com/users/ryanluedders/orgs","repos_url":"https://api.github.com/users/ryanluedders/repos","events_url":"https://api.github.com/users/ryanluedders/events{/privacy}","received_events_url":"https://api.github.com/users/ryanluedders/received_events","type":"User","site_admin":false},"labels":[{"id":16375483,"node_id":"MDU6TGFiZWwxNjM3NTQ4Mw==","url":"https://api.github.com/repos/mockito/mockito/labels/bug","name":"bug","color":"fc2929","default":true}],"state":"closed","locked":false,"assignee":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false},"assignees":[{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2015-10-19T15:42:47Z","updated_at":"2015-11-10T12:32:00Z","closed_at":"2015-11-10T12:32:00Z","author_association":"NONE","body":"I was trying to build a function that can return a mock of any interface extending a generic parent interface. The extending interface specifies the generic parameters.\n\nI've found that if the extending interface does not re-declare the method, then it works as expected (the mock object exhibits mocked behavior when the method is invoked.) \n\nIf the extending interface does re-declare the method, then the mock object does not exhibit the mocked behavior when the method is invoked on the mock.\n\nHopefully the example below helps clarify:\n\n```\npackage mockitotest;\n\nimport static org.junit.Assert.assertEquals;\nimport static org.junit.Assert.assertTrue;\n\nimport org.junit.Test;\nimport org.mockito.Mockito;\n\n// Test case demonstrating a perceived defect in Mockito\npublic class MockBuilderTest {\n\n    // 1. Create an object to use for a return type\n    public class ReturnType {\n\n        String name;\n\n        public String getName() {\n            return name;\n        }\n\n        public void setName(String name) {\n            this.name = name;\n        }\n\n    }\n\n    // 2. Define an interface\n    public interface OriginalInterface<N,O> {\n\n        // contains a templated method\n        O doSomething(N n);\n\n    };\n\n    // 3. Create an extending interface which specifies the templated arguments\n    public interface ExtendingInterfaceObject extends OriginalInterface<String, ReturnType> {\n\n        // 4. The method would be inherited, but I re-state it, mostly to add additional\n        //      documentation inside the interface\n        @Override\n        ReturnType doSomething(String n);\n\n    };\n\n    // 5. For demonstration purposes, create an extending interface which does specify the\n    //      templated arguments, but does not re-state the method\n    public interface ExtendingInterfaceObjectNoRedef extends OriginalInterface<String, ReturnType> {}\n\n    // A function to build a mocked object, the type of object is specified as a method argument\n    // the value that should be returned (O) is also provided as an argument\n    <N,O,T extends OriginalInterface<N,O>> T buildMock(Class<T> cls, final O o) {\n        Class<N> n = null;\n\n        // create a mocked object of the specified class\n        T t = Mockito.mock(cls, Mockito.withSettings().verboseLogging());\n\n        // override the doSomething function to return the desired value\n        Mockito.when(t.doSomething(Mockito.any(n))).thenReturn(o);\n        return t;\n    }\n\n    // Example A: I would expect that 'r' be returned by the resulting mocked object, but\n    //      mockito doesn't seem to match up the invocation with an overriden method\n    @Test\n    public void testBuild_object() {\n        ReturnType r = new ReturnType();\n        r.setName(\"testBuild\");\n\n        ExtendingInterfaceObject i = buildMock(ExtendingInterfaceObject.class, r);\n\n        assertTrue(i instanceof ExtendingInterfaceObject);\n        assertEquals(r, i.doSomething(\"TEST\"));\n    }\n\n    // Example B: like example A, but this uses the extending interface which *does not*\n    //      re-state the method. This example completes successfully.\n    @Test\n    public void testBuild_object_no_redef() {\n        ReturnType r = new ReturnType();\n        r.setName(\"testBuild\");\n\n        ExtendingInterfaceObjectNoRedef i = buildMock(ExtendingInterfaceObjectNoRedef.class, r);\n\n        assertTrue(i instanceof ExtendingInterfaceObjectNoRedef);\n        assertEquals(r, i.doSomething(\"TEST\"));\n    }\n\n}\n```\n","closed_by":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}}", "commentIds":["149295035","149403888","155408621"], "labels":["bug"]}