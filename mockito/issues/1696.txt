{"id":"1696", "title":"Mocking classloader with Java 11 crashes JVM", "body":"Mocking a ClassLoader with Java 11 (I tested with Java 11.0.3 and Mockito 2.27.0) causes the JVM to crash.  This works with Java 8.  I raised an issue ([JDK-8223062](https://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8223062)) with Oracle and was told this is not a Java bug.

Here's the code that produces the crash with Java 11
```java
public class SomeClassToTest {
    private ClassLoader classLoader;

    public SomeClassToTest(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public Class<?> findStringClass() throws ClassNotFoundException {
        return Class.forName(\"java.lang.String\", true, classLoader);
    }
}
```
```java
public class ClassLoaderMockTest {
    @Test
    public void testMockClassLoader() throws ClassNotFoundException {
        SomeClassToTest subject = Mockito.spy(new SomeClassToTest(Mockito.mock(ClassLoader.class)));
        Assert.assertNotNull(subject.findStringClass());
    }
}
```

Here's an example project (with code above) that reproduces the issue: https://github.com/keeganwitt/mockito-mock-classloader-test

Here are the crash details
```
# A fatal error has been detected by the Java Runtime Environment:
#
#  Internal Error (t:/workspace/open/src/hotspot/share/classfile/moduleEntry.cpp:263), pid=15688, tid=22880
#  guarantee(java_lang_Module::is_instance(module)) failed: The unnamed module for ClassLoader org.mockito.codegen.ClassLoader$MockitoMock$1748890561, is null or not an instance of java.lang.Module. The class loader has not been initialized correctly.
#
# JRE version: Java(TM) SE Runtime Environment (11.0.3+12) (build 11.0.3+12-LTS)
# Java VM: Java HotSpot(TM) 64-Bit Server VM (11.0.3+12-LTS, mixed mode, tiered, compressed oops, g1 gc, windows-amd64)
# No core dump will be written. Minidumps are not enabled by default on client versions of Windows
#
# If you would like to submit a bug report, please visit:
#   http://bugreport.java.com/bugreport/crash.jsp
#

---------------  S U M M A R Y ------------

Command Line: -Dorg.gradle.native=false -Xmx512m -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant -ea worker.org.gradle.process.internal.worker.GradleWorkerMain 'Gradle Test Executor 3'

Host: Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz, 8 cores, 31G,  Windows 10 , 64 bit Build 17763 (10.0.17763.437)
Time: Tue Apr 30 10:34:06 2019 Eastern Daylight Time elapsed time: 2 seconds (0d 0h 0m 2s)

---------------  T H R E A D  ---------------

Current thread (0x000002064c567000):  JavaThread \"Test worker\" [_thread_in_vm, id=22880, stack(0x000000d444100000,0x000000d444200000)]

Stack: [0x000000d444100000,0x000000d444200000]
Native frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)
V  [jvm.dll+0x63582a]
V  [jvm.dll+0x77360f]
V  [jvm.dll+0x774cba]
V  [jvm.dll+0x775327]
V  [jvm.dll+0x25e91c]
V  [jvm.dll+0x5fd914]
V  [jvm.dll+0x1e5da0]
V  [jvm.dll+0x1e680e]
V  [jvm.dll+0x1e617f]
V  [jvm.dll+0x70ebcc]
V  [jvm.dll+0x70f2e3]
V  [jvm.dll+0x40deb4]
V  [jvm.dll+0x4173f0]
C  [java.dll+0x13e9]

Java frames: (J=compiled Java code, j=interpreted, Vv=VM code)
J 967  java.lang.Class.forName0(Ljava/lang/String;ZLjava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Class; java.base@11.0.3 (0 bytes) @ 0x000002063feed947 [0x000002063feed8c0+0x0000000000000087]
J 1407 c1 java.lang.Class.forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class; java.base@11.0.3 (47 bytes) @ 0x0000020638be281c [0x0000020638be2500+0x000000000000031c]
j  com.github.keeganwitt.mockclassloader.SomeClassToTest.findStringClass()Ljava/lang/Class;+7
j  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993.findStringClass$accessor$WfdM2b1q()Ljava/lang/Class;+1
j  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993$auxiliary$uOIDOuYv.call()Ljava/lang/Object;+4
j  org.mockito.internal.invocation.RealMethod$FromCallable$1.call()Ljava/lang/Object;+4
j  org.mockito.internal.invocation.RealMethod$FromBehavior.invoke()Ljava/lang/Object;+4
j  org.mockito.internal.invocation.InterceptedInvocation.callRealMethod()Ljava/lang/Object;+20
j  org.mockito.internal.stubbing.answers.CallsRealMethods.answer(Lorg/mockito/invocation/InvocationOnMock;)Ljava/lang/Object;+24
j  org.mockito.Answers.answer(Lorg/mockito/invocation/InvocationOnMock;)Ljava/lang/Object;+5
j  org.mockito.internal.handler.MockHandlerImpl.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+242
j  org.mockito.internal.handler.NullResultGuardian.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+5
j  org.mockito.internal.handler.InvocationNotifierHandler.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+5
j  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor.doIntercept(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;Lorg/mockito/internal/invocation/RealMethod;Lorg/mockito/invocation/Location;)Ljava/lang/Object;+18
j  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor.doIntercept(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;Lorg/mockito/internal/invocation/RealMethod;)Ljava/lang/Object;+13
j  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor$DispatcherDefaultingToRealMethod.interceptSuperCallable(Ljava/lang/Object;Lorg/mockito/internal/creation/bytebuddy/MockMethodInterceptor;Ljava/lang/reflect/Method;[Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;+25
j  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993.findStringClass()Ljava/lang/Class;+20
j  com.github.keeganwitt.mockclassloader.ClassLoaderMockTest.testMockClassLoader()V+23
v  ~StubRoutines::call_stub
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3
j  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3
j  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3
j  org.junit.runners.model.FrameworkMethod$1.runReflectiveCall()Ljava/lang/Object;+15
j  org.junit.internal.runners.model.ReflectiveCallable.run()Ljava/lang/Object;+1
j  org.junit.runners.model.FrameworkMethod.invokeExplosively(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+10
j  org.junit.internal.runners.statements.InvokeMethod.evaluate()V+12
j  org.junit.runners.ParentRunner.runLeaf(Lorg/junit/runners/model/Statement;Lorg/junit/runner/Description;Lorg/junit/runner/notification/RunNotifier;)V+17
j  org.junit.runners.BlockJUnit4ClassRunner.runChild(Lorg/junit/runners/model/FrameworkMethod;Lorg/junit/runner/notification/RunNotifier;)V+30
j  org.junit.runners.BlockJUnit4ClassRunner.runChild(Ljava/lang/Object;Lorg/junit/runner/notification/RunNotifier;)V+6
j  org.junit.runners.ParentRunner$3.run()V+12
j  org.junit.runners.ParentRunner$1.schedule(Ljava/lang/Runnable;)V+1
j  org.junit.runners.ParentRunner.runChildren(Lorg/junit/runner/notification/RunNotifier;)V+44
j  org.junit.runners.ParentRunner.access$000(Lorg/junit/runners/ParentRunner;Lorg/junit/runner/notification/RunNotifier;)V+2
j  org.junit.runners.ParentRunner$2.evaluate()V+8
j  org.junit.runners.ParentRunner.run(Lorg/junit/runner/notification/RunNotifier;)V+20
j  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.runTestClass(Ljava/lang/String;)V+296
j  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(Ljava/lang/String;)V+14
j  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(Ljava/lang/Object;)V+5
j  org.gradle.api.internal.tasks.testing.junit.AbstractJUnitTestClassProcessor.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+26
j  org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+5
v  ~StubRoutines::call_stub
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3
j  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3
j  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3
j  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+19
j  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Ljava/lang/Object;)V+5
j  org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(Ljava/lang/Object;)V+22
j  org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;+177
j  com.sun.proxy.$Proxy2.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+16
j  org.gradle.api.internal.tasks.testing.worker.TestWorker.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+13
v  ~StubRoutines::call_stub
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3
j  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3
j  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3
j  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3
j  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+19
j  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Ljava/lang/Object;)V+5
j  org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+5
j  org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(Ljava/lang/Object;)V+5
j  org.gradle.internal.remote.internal.hub.MessageHub$Handler.run()V+174
j  org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(Ljava/lang/Runnable;)V+1
j  org.gradle.internal.concurrent.ManagedExecutorImpl$1.run()V+25
j  java.util.concurrent.ThreadPoolExecutor.runWorker(Ljava/util/concurrent/ThreadPoolExecutor$Worker;)V+92 java.base@11.0.3
j  java.util.concurrent.ThreadPoolExecutor$Worker.run()V+5 java.base@11.0.3
j  org.gradle.internal.concurrent.ThreadFactoryImpl$ManagedThreadRunnable.run()V+7
j  java.lang.Thread.run()V+11 java.base@11.0.3
v  ~StubRoutines::call_stub
```

Full stack information can be seen [here](https://github.com/keeganwitt/mockito-mock-classloader-test/blob/master/hs_err_pid15688.log).", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1696","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1696/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1696/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1696/events","html_url":"https://github.com/mockito/mockito/issues/1696","id":438820265,"node_id":"MDU6SXNzdWU0Mzg4MjAyNjU=","number":1696,"title":"Mocking classloader with Java 11 crashes JVM","user":{"login":"keeganwitt","id":64612,"node_id":"MDQ6VXNlcjY0NjEy","avatar_url":"https://avatars1.githubusercontent.com/u/64612?v=4","gravatar_id":"","url":"https://api.github.com/users/keeganwitt","html_url":"https://github.com/keeganwitt","followers_url":"https://api.github.com/users/keeganwitt/followers","following_url":"https://api.github.com/users/keeganwitt/following{/other_user}","gists_url":"https://api.github.com/users/keeganwitt/gists{/gist_id}","starred_url":"https://api.github.com/users/keeganwitt/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/keeganwitt/subscriptions","organizations_url":"https://api.github.com/users/keeganwitt/orgs","repos_url":"https://api.github.com/users/keeganwitt/repos","events_url":"https://api.github.com/users/keeganwitt/events{/privacy}","received_events_url":"https://api.github.com/users/keeganwitt/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2019-04-30T14:51:44Z","updated_at":"2019-05-01T16:38:37Z","closed_at":"2019-05-01T16:38:37Z","author_association":"NONE","body":"Mocking a ClassLoader with Java 11 (I tested with Java 11.0.3 and Mockito 2.27.0) causes the JVM to crash.  This works with Java 8.  I raised an issue ([JDK-8223062](https://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8223062)) with Oracle and was told this is not a Java bug.\r\n\r\nHere's the code that produces the crash with Java 11\r\n```java\r\npublic class SomeClassToTest {\r\n    private ClassLoader classLoader;\r\n\r\n    public SomeClassToTest(ClassLoader classLoader) {\r\n        this.classLoader = classLoader;\r\n    }\r\n\r\n    public Class<?> findStringClass() throws ClassNotFoundException {\r\n        return Class.forName(\"java.lang.String\", true, classLoader);\r\n    }\r\n}\r\n```\r\n```java\r\npublic class ClassLoaderMockTest {\r\n    @Test\r\n    public void testMockClassLoader() throws ClassNotFoundException {\r\n        SomeClassToTest subject = Mockito.spy(new SomeClassToTest(Mockito.mock(ClassLoader.class)));\r\n        Assert.assertNotNull(subject.findStringClass());\r\n    }\r\n}\r\n```\r\n\r\nHere's an example project (with code above) that reproduces the issue: https://github.com/keeganwitt/mockito-mock-classloader-test\r\n\r\nHere are the crash details\r\n```\r\n# A fatal error has been detected by the Java Runtime Environment:\r\n#\r\n#  Internal Error (t:/workspace/open/src/hotspot/share/classfile/moduleEntry.cpp:263), pid=15688, tid=22880\r\n#  guarantee(java_lang_Module::is_instance(module)) failed: The unnamed module for ClassLoader org.mockito.codegen.ClassLoader$MockitoMock$1748890561, is null or not an instance of java.lang.Module. The class loader has not been initialized correctly.\r\n#\r\n# JRE version: Java(TM) SE Runtime Environment (11.0.3+12) (build 11.0.3+12-LTS)\r\n# Java VM: Java HotSpot(TM) 64-Bit Server VM (11.0.3+12-LTS, mixed mode, tiered, compressed oops, g1 gc, windows-amd64)\r\n# No core dump will be written. Minidumps are not enabled by default on client versions of Windows\r\n#\r\n# If you would like to submit a bug report, please visit:\r\n#   http://bugreport.java.com/bugreport/crash.jsp\r\n#\r\n\r\n---------------  S U M M A R Y ------------\r\n\r\nCommand Line: -Dorg.gradle.native=false -Xmx512m -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant -ea worker.org.gradle.process.internal.worker.GradleWorkerMain 'Gradle Test Executor 3'\r\n\r\nHost: Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz, 8 cores, 31G,  Windows 10 , 64 bit Build 17763 (10.0.17763.437)\r\nTime: Tue Apr 30 10:34:06 2019 Eastern Daylight Time elapsed time: 2 seconds (0d 0h 0m 2s)\r\n\r\n---------------  T H R E A D  ---------------\r\n\r\nCurrent thread (0x000002064c567000):  JavaThread \"Test worker\" [_thread_in_vm, id=22880, stack(0x000000d444100000,0x000000d444200000)]\r\n\r\nStack: [0x000000d444100000,0x000000d444200000]\r\nNative frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)\r\nV  [jvm.dll+0x63582a]\r\nV  [jvm.dll+0x77360f]\r\nV  [jvm.dll+0x774cba]\r\nV  [jvm.dll+0x775327]\r\nV  [jvm.dll+0x25e91c]\r\nV  [jvm.dll+0x5fd914]\r\nV  [jvm.dll+0x1e5da0]\r\nV  [jvm.dll+0x1e680e]\r\nV  [jvm.dll+0x1e617f]\r\nV  [jvm.dll+0x70ebcc]\r\nV  [jvm.dll+0x70f2e3]\r\nV  [jvm.dll+0x40deb4]\r\nV  [jvm.dll+0x4173f0]\r\nC  [java.dll+0x13e9]\r\n\r\nJava frames: (J=compiled Java code, j=interpreted, Vv=VM code)\r\nJ 967  java.lang.Class.forName0(Ljava/lang/String;ZLjava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Class; java.base@11.0.3 (0 bytes) @ 0x000002063feed947 [0x000002063feed8c0+0x0000000000000087]\r\nJ 1407 c1 java.lang.Class.forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class; java.base@11.0.3 (47 bytes) @ 0x0000020638be281c [0x0000020638be2500+0x000000000000031c]\r\nj  com.github.keeganwitt.mockclassloader.SomeClassToTest.findStringClass()Ljava/lang/Class;+7\r\nj  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993.findStringClass$accessor$WfdM2b1q()Ljava/lang/Class;+1\r\nj  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993$auxiliary$uOIDOuYv.call()Ljava/lang/Object;+4\r\nj  org.mockito.internal.invocation.RealMethod$FromCallable$1.call()Ljava/lang/Object;+4\r\nj  org.mockito.internal.invocation.RealMethod$FromBehavior.invoke()Ljava/lang/Object;+4\r\nj  org.mockito.internal.invocation.InterceptedInvocation.callRealMethod()Ljava/lang/Object;+20\r\nj  org.mockito.internal.stubbing.answers.CallsRealMethods.answer(Lorg/mockito/invocation/InvocationOnMock;)Ljava/lang/Object;+24\r\nj  org.mockito.Answers.answer(Lorg/mockito/invocation/InvocationOnMock;)Ljava/lang/Object;+5\r\nj  org.mockito.internal.handler.MockHandlerImpl.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+242\r\nj  org.mockito.internal.handler.NullResultGuardian.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+5\r\nj  org.mockito.internal.handler.InvocationNotifierHandler.handle(Lorg/mockito/invocation/Invocation;)Ljava/lang/Object;+5\r\nj  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor.doIntercept(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;Lorg/mockito/internal/invocation/RealMethod;Lorg/mockito/invocation/Location;)Ljava/lang/Object;+18\r\nj  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor.doIntercept(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;Lorg/mockito/internal/invocation/RealMethod;)Ljava/lang/Object;+13\r\nj  org.mockito.internal.creation.bytebuddy.MockMethodInterceptor$DispatcherDefaultingToRealMethod.interceptSuperCallable(Ljava/lang/Object;Lorg/mockito/internal/creation/bytebuddy/MockMethodInterceptor;Ljava/lang/reflect/Method;[Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;+25\r\nj  com.github.keeganwitt.mockclassloader.SomeClassToTest$MockitoMock$1537668993.findStringClass()Ljava/lang/Class;+20\r\nj  com.github.keeganwitt.mockclassloader.ClassLoaderMockTest.testMockClassLoader()V+23\r\nv  ~StubRoutines::call_stub\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3\r\nj  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3\r\nj  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3\r\nj  org.junit.runners.model.FrameworkMethod$1.runReflectiveCall()Ljava/lang/Object;+15\r\nj  org.junit.internal.runners.model.ReflectiveCallable.run()Ljava/lang/Object;+1\r\nj  org.junit.runners.model.FrameworkMethod.invokeExplosively(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+10\r\nj  org.junit.internal.runners.statements.InvokeMethod.evaluate()V+12\r\nj  org.junit.runners.ParentRunner.runLeaf(Lorg/junit/runners/model/Statement;Lorg/junit/runner/Description;Lorg/junit/runner/notification/RunNotifier;)V+17\r\nj  org.junit.runners.BlockJUnit4ClassRunner.runChild(Lorg/junit/runners/model/FrameworkMethod;Lorg/junit/runner/notification/RunNotifier;)V+30\r\nj  org.junit.runners.BlockJUnit4ClassRunner.runChild(Ljava/lang/Object;Lorg/junit/runner/notification/RunNotifier;)V+6\r\nj  org.junit.runners.ParentRunner$3.run()V+12\r\nj  org.junit.runners.ParentRunner$1.schedule(Ljava/lang/Runnable;)V+1\r\nj  org.junit.runners.ParentRunner.runChildren(Lorg/junit/runner/notification/RunNotifier;)V+44\r\nj  org.junit.runners.ParentRunner.access$000(Lorg/junit/runners/ParentRunner;Lorg/junit/runner/notification/RunNotifier;)V+2\r\nj  org.junit.runners.ParentRunner$2.evaluate()V+8\r\nj  org.junit.runners.ParentRunner.run(Lorg/junit/runner/notification/RunNotifier;)V+20\r\nj  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.runTestClass(Ljava/lang/String;)V+296\r\nj  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(Ljava/lang/String;)V+14\r\nj  org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(Ljava/lang/Object;)V+5\r\nj  org.gradle.api.internal.tasks.testing.junit.AbstractJUnitTestClassProcessor.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+26\r\nj  org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+5\r\nv  ~StubRoutines::call_stub\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3\r\nj  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3\r\nj  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3\r\nj  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+19\r\nj  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Ljava/lang/Object;)V+5\r\nj  org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(Ljava/lang/Object;)V+22\r\nj  org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;+177\r\nj  com.sun.proxy.$Proxy2.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+16\r\nj  org.gradle.api.internal.tasks.testing.worker.TestWorker.processTestClass(Lorg/gradle/api/internal/tasks/testing/TestClassRunInfo;)V+13\r\nv  ~StubRoutines::call_stub\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+0 java.base@11.0.3\r\nj  jdk.internal.reflect.NativeMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+100 java.base@11.0.3\r\nj  jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+6 java.base@11.0.3\r\nj  java.lang.reflect.Method.invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;+59 java.base@11.0.3\r\nj  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+19\r\nj  org.gradle.internal.dispatch.ReflectionDispatch.dispatch(Ljava/lang/Object;)V+5\r\nj  org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(Lorg/gradle/internal/dispatch/MethodInvocation;)V+5\r\nj  org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(Ljava/lang/Object;)V+5\r\nj  org.gradle.internal.remote.internal.hub.MessageHub$Handler.run()V+174\r\nj  org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(Ljava/lang/Runnable;)V+1\r\nj  org.gradle.internal.concurrent.ManagedExecutorImpl$1.run()V+25\r\nj  java.util.concurrent.ThreadPoolExecutor.runWorker(Ljava/util/concurrent/ThreadPoolExecutor$Worker;)V+92 java.base@11.0.3\r\nj  java.util.concurrent.ThreadPoolExecutor$Worker.run()V+5 java.base@11.0.3\r\nj  org.gradle.internal.concurrent.ThreadFactoryImpl$ManagedThreadRunnable.run()V+7\r\nj  java.lang.Thread.run()V+11 java.base@11.0.3\r\nv  ~StubRoutines::call_stub\r\n```\r\n\r\nFull stack information can be seen [here](https://github.com/keeganwitt/mockito-mock-classloader-test/blob/master/hs_err_pid15688.log).","closed_by":{"login":"TimvdLippe","id":5948271,"node_id":"MDQ6VXNlcjU5NDgyNzE=","avatar_url":"https://avatars3.githubusercontent.com/u/5948271?v=4","gravatar_id":"","url":"https://api.github.com/users/TimvdLippe","html_url":"https://github.com/TimvdLippe","followers_url":"https://api.github.com/users/TimvdLippe/followers","following_url":"https://api.github.com/users/TimvdLippe/following{/other_user}","gists_url":"https://api.github.com/users/TimvdLippe/gists{/gist_id}","starred_url":"https://api.github.com/users/TimvdLippe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/TimvdLippe/subscriptions","organizations_url":"https://api.github.com/users/TimvdLippe/orgs","repos_url":"https://api.github.com/users/TimvdLippe/repos","events_url":"https://api.github.com/users/TimvdLippe/events{/privacy}","received_events_url":"https://api.github.com/users/TimvdLippe/received_events","type":"User","site_admin":false}}", "commentIds":["487995394","488031778","488032323","488042524","488052223","488327994","488335965"], "labels":[]}