{"id":"1254", "title":"Wanted but not invoked on 2.12.0, but not on 2.11.0", "body":"After updating Mockito from 2.11.0 to 2.12.0 the following test fails with `Wanted but not invoked`.

The weird thing is this happens only when running tests using Gradle. Android Studio runs the same test as successful. Just like #1183.

```java
package package.redacted;

import org.junit.Test;
import org.mockito.Mockito;

public class ViewModelTest {

    class View {
        private Runnable actionCallback = null;

        void callAction(Runnable callback) {
            actionCallback = callback;
        }

        void simulateActionCalled() {
            if (actionCallback != null) {
                actionCallback.run();
            }
        }

        void showResult() {
        }
    }

    class ViewModel {

        public ViewModel(final View view) {
            view.callAction(new Runnable() {
                @Override
                public void run() {
                    view.showResult();
                }
            });
        }
    }

    @Test public void test() {
        View view = Mockito.spy(new View());
        ViewModel viewModel = new ViewModel(view);

        view.simulateActionCalled();

        Mockito.verify(view).showResult();
    }
}
```
```
Wanted but not invoked:
view.showResult();
-> at package.redacted.ViewModelTest$View.showResult(ViewModelTest.java:22)

However, there were exactly 2 interactions with this mock:
view.callAction(
    package.redacted.ViewModelTest$ViewModel$1@26645040
);
-> at package.redacted.ViewModelTest$ViewModel.<init>(ViewModelTest.java:28)

view.simulateActionCalled();
-> at package.redacted.ViewModelTest.test(ViewModelTest.java:41)


	at package.redacted.ViewModelTest$View.showResult(ViewModelTest.java:22)
	at package.redacted.ViewModelTest.test(ViewModelTest.java:43)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)
	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)
	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecuter.runTestClass(JUnitTestClassExecuter.java:114)
	at org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecuter.execute(JUnitTestClassExecuter.java:57)
	at org.gradle.api.internal.tasks.testing.junit.JUnitTestClassProcessor.processTestClass(JUnitTestClassProcessor.java:66)
	at org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(SuiteTestClassProcessor.java:51)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:35)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)
	at org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(ContextClassLoaderDispatch.java:32)
	at org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:93)
	at com.sun.proxy.$Proxy1.processTestClass(Unknown Source)
	at org.gradle.api.internal.tasks.testing.worker.TestWorker.processTestClass(TestWorker.java:108)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:35)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)
	at org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(MessageHubBackedObjectConnection.java:146)
	at org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(MessageHubBackedObjectConnection.java:128)
	at org.gradle.internal.remote.internal.hub.MessageHub$Handler.run(MessageHub.java:404)
	at org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(ExecutorPolicy.java:63)
	at org.gradle.internal.concurrent.ManagedExecutorImpl$1.run(ManagedExecutorImpl.java:46)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at org.gradle.internal.concurrent.ThreadFactoryImpl$ManagedThreadRunnable.run(ThreadFactoryImpl.java:55)
	at java.lang.Thread.run(Thread.java:748)
```", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/1254","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/1254/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/1254/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/1254/events","html_url":"https://github.com/mockito/mockito/issues/1254","id":272881116,"node_id":"MDU6SXNzdWUyNzI4ODExMTY=","number":1254,"title":"Wanted but not invoked on 2.12.0, but not on 2.11.0","user":{"login":"ming13","id":200401,"node_id":"MDQ6VXNlcjIwMDQwMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/200401?v=4","gravatar_id":"","url":"https://api.github.com/users/ming13","html_url":"https://github.com/ming13","followers_url":"https://api.github.com/users/ming13/followers","following_url":"https://api.github.com/users/ming13/following{/other_user}","gists_url":"https://api.github.com/users/ming13/gists{/gist_id}","starred_url":"https://api.github.com/users/ming13/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ming13/subscriptions","organizations_url":"https://api.github.com/users/ming13/orgs","repos_url":"https://api.github.com/users/ming13/repos","events_url":"https://api.github.com/users/ming13/events{/privacy}","received_events_url":"https://api.github.com/users/ming13/received_events","type":"User","site_admin":false},"labels":[{"id":16375483,"node_id":"MDU6TGFiZWwxNjM3NTQ4Mw==","url":"https://api.github.com/repos/mockito/mockito/labels/bug","name":"bug","color":"fc2929","default":true}],"state":"closed","locked":false,"assignee":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false},"assignees":[{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}],"milestone":null,"comments":12,"created_at":"2017-11-10T10:07:26Z","updated_at":"2017-12-10T16:13:44Z","closed_at":"2017-11-28T21:54:53Z","author_association":"NONE","body":"After updating Mockito from 2.11.0 to 2.12.0 the following test fails with `Wanted but not invoked`.\r\n\r\nThe weird thing is this happens only when running tests using Gradle. Android Studio runs the same test as successful. Just like #1183.\r\n\r\n```java\r\npackage package.redacted;\r\n\r\nimport org.junit.Test;\r\nimport org.mockito.Mockito;\r\n\r\npublic class ViewModelTest {\r\n\r\n    class View {\r\n        private Runnable actionCallback = null;\r\n\r\n        void callAction(Runnable callback) {\r\n            actionCallback = callback;\r\n        }\r\n\r\n        void simulateActionCalled() {\r\n            if (actionCallback != null) {\r\n                actionCallback.run();\r\n            }\r\n        }\r\n\r\n        void showResult() {\r\n        }\r\n    }\r\n\r\n    class ViewModel {\r\n\r\n        public ViewModel(final View view) {\r\n            view.callAction(new Runnable() {\r\n                @Override\r\n                public void run() {\r\n                    view.showResult();\r\n                }\r\n            });\r\n        }\r\n    }\r\n\r\n    @Test public void test() {\r\n        View view = Mockito.spy(new View());\r\n        ViewModel viewModel = new ViewModel(view);\r\n\r\n        view.simulateActionCalled();\r\n\r\n        Mockito.verify(view).showResult();\r\n    }\r\n}\r\n```\r\n```\r\nWanted but not invoked:\r\nview.showResult();\r\n-> at package.redacted.ViewModelTest$View.showResult(ViewModelTest.java:22)\r\n\r\nHowever, there were exactly 2 interactions with this mock:\r\nview.callAction(\r\n    package.redacted.ViewModelTest$ViewModel$1@26645040\r\n);\r\n-> at package.redacted.ViewModelTest$ViewModel.<init>(ViewModelTest.java:28)\r\n\r\nview.simulateActionCalled();\r\n-> at package.redacted.ViewModelTest.test(ViewModelTest.java:41)\r\n\r\n\r\n\tat package.redacted.ViewModelTest$View.showResult(ViewModelTest.java:22)\r\n\tat package.redacted.ViewModelTest.test(ViewModelTest.java:43)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)\r\n\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)\r\n\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\r\n\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\r\n\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\r\n\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)\r\n\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecuter.runTestClass(JUnitTestClassExecuter.java:114)\r\n\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecuter.execute(JUnitTestClassExecuter.java:57)\r\n\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassProcessor.processTestClass(JUnitTestClassProcessor.java:66)\r\n\tat org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(SuiteTestClassProcessor.java:51)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:35)\r\n\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)\r\n\tat org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(ContextClassLoaderDispatch.java:32)\r\n\tat org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:93)\r\n\tat com.sun.proxy.$Proxy1.processTestClass(Unknown Source)\r\n\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.processTestClass(TestWorker.java:108)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:35)\r\n\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)\r\n\tat org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(MessageHubBackedObjectConnection.java:146)\r\n\tat org.gradle.internal.remote.internal.hub.MessageHubBackedObjectConnection$DispatchWrapper.dispatch(MessageHubBackedObjectConnection.java:128)\r\n\tat org.gradle.internal.remote.internal.hub.MessageHub$Handler.run(MessageHub.java:404)\r\n\tat org.gradle.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(ExecutorPolicy.java:63)\r\n\tat org.gradle.internal.concurrent.ManagedExecutorImpl$1.run(ManagedExecutorImpl.java:46)\r\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\r\n\tat org.gradle.internal.concurrent.ThreadFactoryImpl$ManagedThreadRunnable.run(ThreadFactoryImpl.java:55)\r\n\tat java.lang.Thread.run(Thread.java:748)\r\n```","closed_by":{"login":"raphw","id":4489328,"node_id":"MDQ6VXNlcjQ0ODkzMjg=","avatar_url":"https://avatars3.githubusercontent.com/u/4489328?v=4","gravatar_id":"","url":"https://api.github.com/users/raphw","html_url":"https://github.com/raphw","followers_url":"https://api.github.com/users/raphw/followers","following_url":"https://api.github.com/users/raphw/following{/other_user}","gists_url":"https://api.github.com/users/raphw/gists{/gist_id}","starred_url":"https://api.github.com/users/raphw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/raphw/subscriptions","organizations_url":"https://api.github.com/users/raphw/orgs","repos_url":"https://api.github.com/users/raphw/repos","events_url":"https://api.github.com/users/raphw/events{/privacy}","received_events_url":"https://api.github.com/users/raphw/received_events","type":"User","site_admin":false}}", "commentIds":["343460400","343466056","343755172","344061585","345250871","349342237","349422044","349422605","349602464","349627553","349886406","350559106"], "labels":["bug"]}