{"id":"78", "title":"Defining a new mocked method invokes previously defined one", "body":"For instance, the following test fails with java.lang.RuntimeException: Init mock exception:

```
interface Mockable {
    public int method(int param);
}

public class MockitoTest {
    private Mockable myMock;

    @Before
    public void init() {
        myMock = Mockito.mock(Mockable.class);
        when(myMock.method(anyInt())).thenThrow(new RuntimeException(\"Init mock\"));
    }

    @Test
    public void test() {
        when(myMock.method(eq(1))).thenReturn(1);
    }
}
```

So, the parameter in when statement in test actualy invoked previously defined mock method from init.

Consequently, this means that I can't define more abstract mock in my init method and override it, if necessary, in some of my tests.
", "json":"{"url":"https://api.github.com/repos/mockito/mockito/issues/78","repository_url":"https://api.github.com/repos/mockito/mockito","labels_url":"https://api.github.com/repos/mockito/mockito/issues/78/labels{/name}","comments_url":"https://api.github.com/repos/mockito/mockito/issues/78/comments","events_url":"https://api.github.com/repos/mockito/mockito/issues/78/events","html_url":"https://github.com/mockito/mockito/issues/78","id":41638190,"node_id":"MDU6SXNzdWU0MTYzODE5MA==","number":78,"title":"Defining a new mocked method invokes previously defined one","user":{"login":"Antolius","id":5374391,"node_id":"MDQ6VXNlcjUzNzQzOTE=","avatar_url":"https://avatars0.githubusercontent.com/u/5374391?v=4","gravatar_id":"","url":"https://api.github.com/users/Antolius","html_url":"https://github.com/Antolius","followers_url":"https://api.github.com/users/Antolius/followers","following_url":"https://api.github.com/users/Antolius/following{/other_user}","gists_url":"https://api.github.com/users/Antolius/gists{/gist_id}","starred_url":"https://api.github.com/users/Antolius/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Antolius/subscriptions","organizations_url":"https://api.github.com/users/Antolius/orgs","repos_url":"https://api.github.com/users/Antolius/repos","events_url":"https://api.github.com/users/Antolius/events{/privacy}","received_events_url":"https://api.github.com/users/Antolius/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2014-09-01T13:38:40Z","updated_at":"2015-06-05T15:25:43Z","closed_at":"2015-06-05T15:25:43Z","author_association":"NONE","body":"For instance, the following test fails with java.lang.RuntimeException: Init mock exception:\n\n```\ninterface Mockable {\n    public int method(int param);\n}\n\npublic class MockitoTest {\n    private Mockable myMock;\n\n    @Before\n    public void init() {\n        myMock = Mockito.mock(Mockable.class);\n        when(myMock.method(anyInt())).thenThrow(new RuntimeException(\"Init mock\"));\n    }\n\n    @Test\n    public void test() {\n        when(myMock.method(eq(1))).thenReturn(1);\n    }\n}\n```\n\nSo, the parameter in when statement in test actualy invoked previously defined mock method from init.\n\nConsequently, this means that I can't define more abstract mock in my init method and override it, if necessary, in some of my tests.\n","closed_by":{"login":"bric3","id":803621,"node_id":"MDQ6VXNlcjgwMzYyMQ==","avatar_url":"https://avatars1.githubusercontent.com/u/803621?v=4","gravatar_id":"","url":"https://api.github.com/users/bric3","html_url":"https://github.com/bric3","followers_url":"https://api.github.com/users/bric3/followers","following_url":"https://api.github.com/users/bric3/following{/other_user}","gists_url":"https://api.github.com/users/bric3/gists{/gist_id}","starred_url":"https://api.github.com/users/bric3/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bric3/subscriptions","organizations_url":"https://api.github.com/users/bric3/orgs","repos_url":"https://api.github.com/users/bric3/repos","events_url":"https://api.github.com/users/bric3/events{/privacy}","received_events_url":"https://api.github.com/users/bric3/received_events","type":"User","site_admin":false}}", "commentIds":["69854873","89855043","89859342"], "labels":[]}