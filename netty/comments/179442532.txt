{"id":"179442532","body":"@carl-mastrangelo this one: https://github.com/google/java-thread-sanitizer ? Never heard of it but looks interesting.\n","date":"Feb 4, 2016, 2:23:09 AM","json":"{\"url\":\"https://api.github.com/repos/netty/netty/issues/comments/179442532\",\"html_url\":\"https://github.com/netty/netty/issues/4830#issuecomment-179442532\",\"issue_url\":\"https://api.github.com/repos/netty/netty/issues/4830\",\"id\":179442532,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDE3OTQ0MjUzMg\u003d\u003d\",\"user\":{\"login\":\"normanmaurer\",\"id\":439362,\"node_id\":\"MDQ6VXNlcjQzOTM2Mg\u003d\u003d\",\"avatar_url\":\"https://avatars3.githubusercontent.com/u/439362?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/normanmaurer\",\"html_url\":\"https://github.com/normanmaurer\",\"followers_url\":\"https://api.github.com/users/normanmaurer/followers\",\"following_url\":\"https://api.github.com/users/normanmaurer/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/normanmaurer/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/normanmaurer/subscriptions\",\"organizations_url\":\"https://api.github.com/users/normanmaurer/orgs\",\"repos_url\":\"https://api.github.com/users/normanmaurer/repos\",\"events_url\":\"https://api.github.com/users/normanmaurer/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/normanmaurer/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2016-02-03T20:23:09Z\",\"updated_at\":\"2016-02-03T20:23:09Z\",\"author_association\":\"MEMBER\",\"body\":\"@carl-mastrangelo this one: https://github.com/google/java-thread-sanitizer ? Never heard of it but looks interesting.\\n\"}"}