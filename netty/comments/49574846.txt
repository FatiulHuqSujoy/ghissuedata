{"id":"49574846","body":"@idelpivnitskiy @fredericBregier fixed by synchonize directly on this via method modifier.\n","date":"Jul 21, 2014, 12:24:16 PM","json":"{\"url\":\"https://api.github.com/repos/netty/netty/issues/comments/49574846\",\"html_url\":\"https://github.com/netty/netty/issues/2675#issuecomment-49574846\",\"issue_url\":\"https://api.github.com/repos/netty/netty/issues/2675\",\"id\":49574846,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDQ5NTc0ODQ2\",\"user\":{\"login\":\"normanmaurer\",\"id\":439362,\"node_id\":\"MDQ6VXNlcjQzOTM2Mg\u003d\u003d\",\"avatar_url\":\"https://avatars3.githubusercontent.com/u/439362?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/normanmaurer\",\"html_url\":\"https://github.com/normanmaurer\",\"followers_url\":\"https://api.github.com/users/normanmaurer/followers\",\"following_url\":\"https://api.github.com/users/normanmaurer/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/normanmaurer/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/normanmaurer/subscriptions\",\"organizations_url\":\"https://api.github.com/users/normanmaurer/orgs\",\"repos_url\":\"https://api.github.com/users/normanmaurer/repos\",\"events_url\":\"https://api.github.com/users/normanmaurer/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/normanmaurer/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2014-07-21T06:24:16Z\",\"updated_at\":\"2014-07-21T06:24:16Z\",\"author_association\":\"MEMBER\",\"body\":\"@idelpivnitskiy @fredericBregier fixed by synchonize directly on this via method modifier.\\n\"}"}