{"id":"180475210","body":"@Scottmitch - I\u0027ve run a few scenarios with that benchmark. The occasional exception being thrown vs. constant double checking is very difficult for the latter to make up. I can fabricate scenarios where the two start showing similar Op/s but it does feel very fabricated. It\u0027s hard to quantify the memory cost from these StackTraceElements that need to be created and thrown away.\n\nI think the key takeaway for me is that it\u0027d be great if AppendableCharSequence\u0027s initial size was configurable. It\u0027s currently hard coded to 128 (HttpObjectDecoder, line 184). From our use-case I can already tell upfront that setting it to 256 would cut the number of exceptions in half and with basically no additional memory cost as most requests will have on header that is in the 400-500 character range.\n","date":"Feb 6, 2016, 12:08:44 AM","json":"{\"url\":\"https://api.github.com/repos/netty/netty/issues/comments/180475210\",\"html_url\":\"https://github.com/netty/netty/issues/4807#issuecomment-180475210\",\"issue_url\":\"https://api.github.com/repos/netty/netty/issues/4807\",\"id\":180475210,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDE4MDQ3NTIxMA\u003d\u003d\",\"user\":{\"login\":\"rkapsi\",\"id\":191635,\"node_id\":\"MDQ6VXNlcjE5MTYzNQ\u003d\u003d\",\"avatar_url\":\"https://avatars0.githubusercontent.com/u/191635?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/rkapsi\",\"html_url\":\"https://github.com/rkapsi\",\"followers_url\":\"https://api.github.com/users/rkapsi/followers\",\"following_url\":\"https://api.github.com/users/rkapsi/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/rkapsi/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/rkapsi/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/rkapsi/subscriptions\",\"organizations_url\":\"https://api.github.com/users/rkapsi/orgs\",\"repos_url\":\"https://api.github.com/users/rkapsi/repos\",\"events_url\":\"https://api.github.com/users/rkapsi/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/rkapsi/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2016-02-05T18:08:44Z\",\"updated_at\":\"2016-02-05T18:08:44Z\",\"author_association\":\"MEMBER\",\"body\":\"@Scottmitch - I\u0027ve run a few scenarios with that benchmark. The occasional exception being thrown vs. constant double checking is very difficult for the latter to make up. I can fabricate scenarios where the two start showing similar Op/s but it does feel very fabricated. It\u0027s hard to quantify the memory cost from these StackTraceElements that need to be created and thrown away.\\n\\nI think the key takeaway for me is that it\u0027d be great if AppendableCharSequence\u0027s initial size was configurable. It\u0027s currently hard coded to 128 (HttpObjectDecoder, line 184). From our use-case I can already tell upfront that setting it to 256 would cut the number of exceptions in half and with basically no additional memory cost as most requests will have on header that is in the 400-500 character range.\\n\"}"}