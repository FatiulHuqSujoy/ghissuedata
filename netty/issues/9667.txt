{"id":"9667", "title":"FlowControllerHandler swallows read-complete event when auto-read is disabled", "body":"### Expected behavior

IdleStateHandler should trigger read-timeout events when registered after FlowControlHandler.

### Actual behavior

IdleStateHandler does NOT trigger read-timeout event.

### Steps to reproduce

Server pipeline:

    p.addLast(\"flow-control\", new FlowControlHandler)
    p.addLast(\"keep-alive\", new PingInactiveHandler(readTimeout))// extends IdleStateHandler
    p.addLast(\"handler\", new AppHandler) // trigger read

When no upstream messages from the client within time limit, then READER_IDLE event should be triggered. However it does NOT happen.

### Netty version

* Version 4.1.31 - no problem, example code works.
* Version 4.1.39 - has a problem, example code does NOT work.

Seems like related to PR #9664 

### JVM version (e.g. `java -version`)

Oracle JDK 1.8.0_221-b27

### OS version (e.g. `uname -a`)

* Windows  7 Enterprise
* Linux gmafxu42 3.0.101-108.98-default #1 SMP Mon Jul 15 13:58:06 UTC 2019 (262a94d) x86_64 x86_64 x86_64 GNU/Linux

### How to fix

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
    ---    // Don't relay completion events from upstream as they
    ---    // make no sense in this context. See dequeue() where
    ---    // a new set of completion events is being produced.
    +++    if (isQueueEmpty()) {
    +++        ctx.fireChannelReadComplete();
    +++    }
    }
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/9667","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/9667/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/9667/comments","events_url":"https://api.github.com/repos/netty/netty/issues/9667/events","html_url":"https://github.com/netty/netty/issues/9667","id":507138963,"node_id":"MDU6SXNzdWU1MDcxMzg5NjM=","number":9667,"title":"FlowControllerHandler swallows read-complete event when auto-read is disabled","user":{"login":"ursaj","id":3481450,"node_id":"MDQ6VXNlcjM0ODE0NTA=","avatar_url":"https://avatars2.githubusercontent.com/u/3481450?v=4","gravatar_id":"","url":"https://api.github.com/users/ursaj","html_url":"https://github.com/ursaj","followers_url":"https://api.github.com/users/ursaj/followers","following_url":"https://api.github.com/users/ursaj/following{/other_user}","gists_url":"https://api.github.com/users/ursaj/gists{/gist_id}","starred_url":"https://api.github.com/users/ursaj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ursaj/subscriptions","organizations_url":"https://api.github.com/users/ursaj/orgs","repos_url":"https://api.github.com/users/ursaj/repos","events_url":"https://api.github.com/users/ursaj/events{/privacy}","received_events_url":"https://api.github.com/users/ursaj/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/217","html_url":"https://github.com/netty/netty/milestone/217","labels_url":"https://api.github.com/repos/netty/netty/milestones/217/labels","id":4690000,"node_id":"MDk6TWlsZXN0b25lNDY5MDAwMA==","number":217,"title":"4.1.43.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":63,"state":"closed","created_at":"2019-09-25T07:10:03Z","updated_at":"2019-10-24T16:29:39Z","due_on":null,"closed_at":"2019-10-24T16:29:39Z"},"comments":1,"created_at":"2019-10-15T09:59:54Z","updated_at":"2019-10-23T08:47:59Z","closed_at":"2019-10-23T08:47:59Z","author_association":"CONTRIBUTOR","body":"### Expected behavior\r\n\r\nIdleStateHandler should trigger read-timeout events when registered after FlowControlHandler.\r\n\r\n### Actual behavior\r\n\r\nIdleStateHandler does NOT trigger read-timeout event.\r\n\r\n### Steps to reproduce\r\n\r\nServer pipeline:\r\n\r\n    p.addLast(\"flow-control\", new FlowControlHandler)\r\n    p.addLast(\"keep-alive\", new PingInactiveHandler(readTimeout))// extends IdleStateHandler\r\n    p.addLast(\"handler\", new AppHandler) // trigger read\r\n\r\nWhen no upstream messages from the client within time limit, then READER_IDLE event should be triggered. However it does NOT happen.\r\n\r\n### Netty version\r\n\r\n* Version 4.1.31 - no problem, example code works.\r\n* Version 4.1.39 - has a problem, example code does NOT work.\r\n\r\nSeems like related to PR #9664 \r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\nOracle JDK 1.8.0_221-b27\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\n* Windows  7 Enterprise\r\n* Linux gmafxu42 3.0.101-108.98-default #1 SMP Mon Jul 15 13:58:06 UTC 2019 (262a94d) x86_64 x86_64 x86_64 GNU/Linux\r\n\r\n### How to fix\r\n\r\n    @Override\r\n    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {\r\n    ---    // Don't relay completion events from upstream as they\r\n    ---    // make no sense in this context. See dequeue() where\r\n    ---    // a new set of completion events is being produced.\r\n    +++    if (isQueueEmpty()) {\r\n    +++        ctx.fireChannelReadComplete();\r\n    +++    }\r\n    }\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["542968934"], "labels":[]}