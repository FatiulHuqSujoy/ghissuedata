{"id":"8100", "title":"Snapshot publication for native library", "body":"### Expected behavior

Snapshot version of kqueue available at Sonatype snapshot.

### Actual behavior

For 4.1.26.Final-SNAPSHOT, the kqueue artifact with classifier `osx-x86_64` is not published (the one with the `META-INF/native`), whereas it seems ok for epoll.

### Steps to reproduce

`sbt update`

### Minimal yet complete reproducer code (or URL to code)

https://github.com/cchantep/netty-native-shading

### Netty version

4.1.26.Final-SNAPSHOT

### JVM version (e.g. `java -version`)

1.{8,9}

### OS version (e.g. `uname -a`)

OS X (high sierra)", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8100","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8100/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8100/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8100/events","html_url":"https://github.com/netty/netty/issues/8100","id":338189929,"node_id":"MDU6SXNzdWUzMzgxODk5Mjk=","number":8100,"title":"Snapshot publication for native library","user":{"login":"cchantep","id":166062,"node_id":"MDQ6VXNlcjE2NjA2Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/166062?v=4","gravatar_id":"","url":"https://api.github.com/users/cchantep","html_url":"https://github.com/cchantep","followers_url":"https://api.github.com/users/cchantep/followers","following_url":"https://api.github.com/users/cchantep/following{/other_user}","gists_url":"https://api.github.com/users/cchantep/gists{/gist_id}","starred_url":"https://api.github.com/users/cchantep/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cchantep/subscriptions","organizations_url":"https://api.github.com/users/cchantep/orgs","repos_url":"https://api.github.com/users/cchantep/repos","events_url":"https://api.github.com/users/cchantep/events{/privacy}","received_events_url":"https://api.github.com/users/cchantep/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-07-04T09:02:55Z","updated_at":"2019-10-15T16:49:28Z","closed_at":"2019-10-11T08:44:07Z","author_association":"NONE","body":"### Expected behavior\r\n\r\nSnapshot version of kqueue available at Sonatype snapshot.\r\n\r\n### Actual behavior\r\n\r\nFor 4.1.26.Final-SNAPSHOT, the kqueue artifact with classifier `osx-x86_64` is not published (the one with the `META-INF/native`), whereas it seems ok for epoll.\r\n\r\n### Steps to reproduce\r\n\r\n`sbt update`\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\nhttps://github.com/cchantep/netty-native-shading\r\n\r\n### Netty version\r\n\r\n4.1.26.Final-SNAPSHOT\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\n1.{8,9}\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\nOS X (high sierra)","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["540974914","542305733"], "labels":[]}