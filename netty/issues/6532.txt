{"id":"6532", "title":"Meet a problem when using netty with http POST method(Decode body, headers mistake or incorrect)", "body":"Hi!

I using netty version 4.1.4 --> 4.1.9 and meet a problem when i try decode body and headers. it is mistake or incorrect albeit with small in number. I do not know how to fix it. below code i using netty:

### body mistake or incorrect
/?id=9faf008486456ï¿½ï¿½Oï¿½ï¿½8ï¿½ï¿½ï¿½
     ï¿½ï¿½Rkï¿½A@ï¿½eï¿½ï¿½&ï¿½^Oï¿½2ï¿½ï¿½ï¿½ï¿½/uï¿½mkï¿½ï¿½,ï¿½ï¿½G×žï¿½'ï¿½ï¿½ï¿½ï¿½×¸ï¿½Wï¿½<q(ï¿½ï¿½U\\k

### headers mistake or incorrect
content-Type=nection: close|User-Agent=Datext/plain; charset=utf-8|Conlvik/1.6.0=|Host=myhost:myport|Accept-Encoding=gzip|Content-Length=251

Content-Type=text/plain; charset=utf-8|Connection=close|User-Agent=Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-J111F Build/LMY47V)|Host=|ï¾°ï¿©ï¿…\"ï¾‹ï¾€ï¿¼ï¾£ï¿§%ï¿¯ï¿‹Rï¾¥%ï¾®ï¿½U.ï¿‘?ï¿žï¿¿ï¿§X\\.ï¿lï¾‰ï¿µï¾€ï¾¤dï¾œï¾—ï¾¶%;QXï¿¯ï¾ ï¾œï¿±7ï¿¥ï¿¡ï¿ºï¾¶ï¿½]ï¾€ï¾‹ï¿188|content-length=0|

### using code with epoll transport:

> ServerBootstrap b = new ServerBootstrap();
> b.group(bossGroup, workerGroup)
> 	.channel(EpollServerSocketChannel.class)
> 	.option(EpollChannelOption.SO_RCVBUF, this.mReceiveBufferSize)
> 	.option(EpollChannelOption.SO_REUSEADDR, this.mReuseAddress)
> 	.option(EpollChannelOption.SO_REUSEPORT, this.mReusePort)
> 	.option(EpollChannelOption.IP_FREEBIND, this.mFreeBind)
> 	.option(EpollChannelOption.SO_BACKLOG, this.backlog)
> 
> 	.childOption(EpollChannelOption.TCP_NODELAY, this.mTcpNoDelay)
> 	.childOption(EpollChannelOption.SO_REUSEADDR, this.mReuseAddress)
> 	.childOption(EpollChannelOption.SO_KEEPALIVE, this.keepAlive)
> 	.childOption(EpollChannelOption.CONNECT_TIMEOUT_MILLIS, this.mConnectTimeout)
> 	.childOption(EpollChannelOption.TCP_USER_TIMEOUT, this.tcpUserTimeout)
> 
> 	.childOption(EpollChannelOption.SO_RCVBUF, this.mReceiveBufferSize)
> 	.childOption(EpollChannelOption.SO_SNDBUF, this.mReceiveBufferSize)
> 	.childHandler(new HttpServerInitializer());
> b.bind(new InetSocketAddress(this.mPort)).sync().channel().closeFuture().sync();

HttpServerInitializer.class
> public class HttpServerInitializer extends ChannelInitializer<SocketChannel> {
>     @Override
>     public void initChannel(SocketChannel ch) {
>         ChannelPipeline p = ch.pipeline();
>         p.addLast(\"server_codec\", new HttpServerCodec());
>         p.addLast(\"aggregator\", new HttpObjectAggregator();
>         p.addLast(\"handler\", new HttpSimpleServerHandler(workflow));
>     }
> }

HttpSimpleServerHandler .class
> public class HttpSimpleServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
>     @Override
>     public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
>         if ((!request.decoderResult().isSuccess()) || (HttpUtil.is100ContinueExpected(request))) {
>             return;
>         }
>         if (request.method() == HttpMethod.POST) {
>             ByteBuf content = request.content();
>             if ((content != null) && (content.isReadable())) {
>                 Log.info(content.toString(CharsetUtil.UTF_8));
>                 Log.info(dumpHttpHeaders(request));
>             }
>         }
>    }
> 
> private StringBuilder dumpHttpHeaders(HttpRequest request) {
>         StringBuilder builder = new StringBuilder();
>         HttpHeaders headers = request.headers();
>         if ((headers != null) && (!headers.isEmpty())) {
>             for (Map.Entry<String, String> h : headers) {
>                 builder.append(h.getKey()).append(\"=\").append(h.getValue()).append(\"|\");
>             }
>         }
>         return builder;
>     }
> }

### TCP status
142 CLOSING
29168 ESTABLISHED
1604 FIN_WAIT1
121 FIN_WAIT2
6 LAST_ACK
1 LISTEN
2618 SYN_RECV
13424 TIME_WAIT

### JVM version (e.g. `java -version`)
openjdk version \"1.8.0_111\"
OpenJDK Runtime Environment (build 1.8.0_111-b15)
OpenJDK 64-Bit Server VM (build 25.111-b15, mixed mode

### OS version (e.g. `uname -a`)
Linux vps 3.10.0-514.2.2.el7.x86_64 #1 SMP Tue Dec 6 23:06:41 UTC 2016 x86_64 x86_64 x86_64 
GNU/Linux
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6532","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6532/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6532/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6532/events","html_url":"https://github.com/netty/netty/issues/6532","id":213532737,"node_id":"MDU6SXNzdWUyMTM1MzI3Mzc=","number":6532,"title":"Meet a problem when using netty with http POST method(Decode body, headers mistake or incorrect)","user":{"login":"ductrungcnt","id":13083140,"node_id":"MDQ6VXNlcjEzMDgzMTQw","avatar_url":"https://avatars3.githubusercontent.com/u/13083140?v=4","gravatar_id":"","url":"https://api.github.com/users/ductrungcnt","html_url":"https://github.com/ductrungcnt","followers_url":"https://api.github.com/users/ductrungcnt/followers","following_url":"https://api.github.com/users/ductrungcnt/following{/other_user}","gists_url":"https://api.github.com/users/ductrungcnt/gists{/gist_id}","starred_url":"https://api.github.com/users/ductrungcnt/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ductrungcnt/subscriptions","organizations_url":"https://api.github.com/users/ductrungcnt/orgs","repos_url":"https://api.github.com/users/ductrungcnt/repos","events_url":"https://api.github.com/users/ductrungcnt/events{/privacy}","received_events_url":"https://api.github.com/users/ductrungcnt/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-03-11T14:45:50Z","updated_at":"2017-03-31T00:36:45Z","closed_at":"2017-03-31T00:36:45Z","author_association":"NONE","body":"Hi!\r\n\r\nI using netty version 4.1.4 --> 4.1.9 and meet a problem when i try decode body and headers. it is mistake or incorrect albeit with small in number. I do not know how to fix it. below code i using netty:\r\n\r\n### body mistake or incorrect\r\n/?id=9faf008486456ï¿½ï¿½Oï¿½ï¿½8ï¿½ï¿½ï¿½\r\n     ï¿½ï¿½Rkï¿½A\u0004@ï¿½eï¿½ï¿½&ï¿½^Oï¿½2ï¿½ï¿½ï¿½ï¿½/u\u0014ï¿½mkï¿½ï¿½,ï¿½ï¿½G×žï¿½'ï¿½\u0001ï¿½ï¿½ï¿½×¸ï¿½\u0002Wï¿½<q(ï¿½ï¿½U\u0016\\k\r\n\r\n### headers mistake or incorrect\r\ncontent-Type=nection: close|User-Agent=Datext/plain; charset=utf-8|Conlvik/1.6.0=|Host=myhost:myport|Accept-Encoding=gzip|Content-Length=251\r\n\r\nContent-Type=text/plain; charset=utf-8|Connection=close|User-Agent=Dalvik/2.1.0 (Linux; U; Android 5.1.1; SM-J111F Build/LMY47V)|Host=|ï¾°ï¿©ï¿…\"ï¾‹ï¾€ï¿¼ï¾£ï¿§%ï¿¯ï¿‹Rï¾¥%ï¾®ï¿½U.ï¿‘?ï¿žï¿¿ï¿§X\\\u0010.ï¿lï¾‰ï¿µï¾€ï¾¤dï¾œï¾—ï¾¶%;QXï¿¯ï¾ ï¾œï¿±7ï¿¥ï¿¡ï¿ºï¾¶ï¿½]ï¾€ï¾‹ï¿188|content-length=0|\r\n\r\n### using code with epoll transport:\r\n\r\n> ServerBootstrap b = new ServerBootstrap();\r\n> b.group(bossGroup, workerGroup)\r\n> \t.channel(EpollServerSocketChannel.class)\r\n> \t.option(EpollChannelOption.SO_RCVBUF, this.mReceiveBufferSize)\r\n> \t.option(EpollChannelOption.SO_REUSEADDR, this.mReuseAddress)\r\n> \t.option(EpollChannelOption.SO_REUSEPORT, this.mReusePort)\r\n> \t.option(EpollChannelOption.IP_FREEBIND, this.mFreeBind)\r\n> \t.option(EpollChannelOption.SO_BACKLOG, this.backlog)\r\n> \r\n> \t.childOption(EpollChannelOption.TCP_NODELAY, this.mTcpNoDelay)\r\n> \t.childOption(EpollChannelOption.SO_REUSEADDR, this.mReuseAddress)\r\n> \t.childOption(EpollChannelOption.SO_KEEPALIVE, this.keepAlive)\r\n> \t.childOption(EpollChannelOption.CONNECT_TIMEOUT_MILLIS, this.mConnectTimeout)\r\n> \t.childOption(EpollChannelOption.TCP_USER_TIMEOUT, this.tcpUserTimeout)\r\n> \r\n> \t.childOption(EpollChannelOption.SO_RCVBUF, this.mReceiveBufferSize)\r\n> \t.childOption(EpollChannelOption.SO_SNDBUF, this.mReceiveBufferSize)\r\n> \t.childHandler(new HttpServerInitializer());\r\n> b.bind(new InetSocketAddress(this.mPort)).sync().channel().closeFuture().sync();\r\n\r\nHttpServerInitializer.class\r\n> public class HttpServerInitializer extends ChannelInitializer<SocketChannel> {\r\n>     @Override\r\n>     public void initChannel(SocketChannel ch) {\r\n>         ChannelPipeline p = ch.pipeline();\r\n>         p.addLast(\"server_codec\", new HttpServerCodec());\r\n>         p.addLast(\"aggregator\", new HttpObjectAggregator();\r\n>         p.addLast(\"handler\", new HttpSimpleServerHandler(workflow));\r\n>     }\r\n> }\r\n\r\nHttpSimpleServerHandler .class\r\n> public class HttpSimpleServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {\r\n>     @Override\r\n>     public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {\r\n>         if ((!request.decoderResult().isSuccess()) || (HttpUtil.is100ContinueExpected(request))) {\r\n>             return;\r\n>         }\r\n>         if (request.method() == HttpMethod.POST) {\r\n>             ByteBuf content = request.content();\r\n>             if ((content != null) && (content.isReadable())) {\r\n>                 Log.info(content.toString(CharsetUtil.UTF_8));\r\n>                 Log.info(dumpHttpHeaders(request));\r\n>             }\r\n>         }\r\n>    }\r\n> \r\n> private StringBuilder dumpHttpHeaders(HttpRequest request) {\r\n>         StringBuilder builder = new StringBuilder();\r\n>         HttpHeaders headers = request.headers();\r\n>         if ((headers != null) && (!headers.isEmpty())) {\r\n>             for (Map.Entry<String, String> h : headers) {\r\n>                 builder.append(h.getKey()).append(\"=\").append(h.getValue()).append(\"|\");\r\n>             }\r\n>         }\r\n>         return builder;\r\n>     }\r\n> }\r\n\r\n### TCP status\r\n142 CLOSING\r\n29168 ESTABLISHED\r\n1604 FIN_WAIT1\r\n121 FIN_WAIT2\r\n6 LAST_ACK\r\n1 LISTEN\r\n2618 SYN_RECV\r\n13424 TIME_WAIT\r\n\r\n### JVM version (e.g. `java -version`)\r\nopenjdk version \"1.8.0_111\"\r\nOpenJDK Runtime Environment (build 1.8.0_111-b15)\r\nOpenJDK 64-Bit Server VM (build 25.111-b15, mixed mode\r\n\r\n### OS version (e.g. `uname -a`)\r\nLinux vps 3.10.0-514.2.2.el7.x86_64 #1 SMP Tue Dec 6 23:06:41 UTC 2016 x86_64 x86_64 x86_64 \r\nGNU/Linux\r\n","closed_by":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}}", "commentIds":["289642926","289653095","290583637"], "labels":[]}