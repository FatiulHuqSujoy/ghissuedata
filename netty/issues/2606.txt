{"id":"2606", "title":"UnsupportedOperationException if HTTP server receives 'Expect: 100-continue' header", "body":"Netty version: 4.0.20.Final

Hello, here is my problem:

My server pipeline:

``` java
channel.pipeline().addLast(MvcConfig.CNL_INFO_HANDLER, new InfoHandler(0, 0));  // handler which collects information for server
channel.pipeline().addLast(new HttpRequestDecoder());  // netty handler
channel.pipeline().addLast(new HttpObjectAggregator(8388608));  // netty handler
channel.pipeline().addLast(MvcConfig.CNL_SERVER_HANDLER, new HttpServerHandler(server));  // my server handler
channel.pipeline().addLast(new HttpResponseEncoder());  // netty handler
```

If server receives http header 'Expect: 100-continue' HttpObjectAggregator invokes next code:

``` java
            if (is100ContinueExpected(m)) {
                ctx.writeAndFlush(CONTINUE).addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        if (!future.isSuccess()) {
                            ctx.fireExceptionCaught(future.cause());
                        }
                    }
                });
            }
```

It tries to send CONTINUE response status to the client and I'm getting exception:

``` java
java.lang.UnsupportedOperationException: unsupported message type: DefaultFullHttpResponse
    at io.netty.channel.nio.AbstractNioByteChannel.doWrite(AbstractNioByteChannel.java:261) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.socket.nio.NioSocketChannel.doWrite(NioSocketChannel.java:248) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at fw.network.shared.StatefulNioSocketChannel.doWrite(StatefulNioSocketChannel.java:49) ~[classes/:na]
    at io.netty.channel.AbstractChannel$AbstractUnsafe.flush0(AbstractChannel.java:694) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.AbstractNioChannel$AbstractNioUnsafe.flush0(AbstractNioChannel.java:315) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannel$AbstractUnsafe.flush(AbstractChannel.java:663) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.DefaultChannelPipeline$HeadContext.flush(DefaultChannelPipeline.java:1059) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.invokeFlush(AbstractChannelHandlerContext.java:687) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.flush(AbstractChannelHandlerContext.java:668) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.ChannelDuplexHandler.flush(ChannelDuplexHandler.java:117) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.invokeFlush(AbstractChannelHandlerContext.java:687) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.write(AbstractChannelHandlerContext.java:717) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.writeAndFlush(AbstractChannelHandlerContext.java:705) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.writeAndFlush(AbstractChannelHandlerContext.java:740) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:129) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:52) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:89) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:163) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.handler.traffic.AbstractTrafficShapingHandler.channelRead(AbstractTrafficShapingHandler.java:223) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at fw.network.mvc.InfoHandler.channelRead(InfoHandler.java:28) [classes/:na]
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:787) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:125) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:507) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized(NioEventLoop.java:464) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:378) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:350) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:116) [netty-all-4.0.20.Final.jar:4.0.20.Final]
    at java.lang.Thread.run(Thread.java:744) [na:1.8.0]
```

Looks like this response doesn't go through HttpResponseEncoder and hence this exceptions occurs. When I changed writing to the pipeline, instead of direct writing to the ctx, everything goes well:

``` java
....
 ctx.pipeline().writeAndFlush(CONTINUE).addListener(....)
....
```

$ java -version
java version \"1.8.0\"
Java(TM) SE Runtime Environment (build 1.8.0-b132)
Java HotSpot(TM) 64-Bit Server VM (build 25.0-b70, mixed mode)

$ uname -a
Linux denary 3.5.0-17-generic #28-Ubuntu SMP Tue Oct 9 19:31:23 UTC 2012 x86_64 x86_64 x86_64 GNU/Linux
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2606","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2606/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2606/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2606/events","html_url":"https://github.com/netty/netty/issues/2606","id":36569856,"node_id":"MDU6SXNzdWUzNjU2OTg1Ng==","number":2606,"title":"UnsupportedOperationException if HTTP server receives 'Expect: 100-continue' header","user":{"login":"DendromusDenary","id":4179649,"node_id":"MDQ6VXNlcjQxNzk2NDk=","avatar_url":"https://avatars0.githubusercontent.com/u/4179649?v=4","gravatar_id":"","url":"https://api.github.com/users/DendromusDenary","html_url":"https://github.com/DendromusDenary","followers_url":"https://api.github.com/users/DendromusDenary/followers","following_url":"https://api.github.com/users/DendromusDenary/following{/other_user}","gists_url":"https://api.github.com/users/DendromusDenary/gists{/gist_id}","starred_url":"https://api.github.com/users/DendromusDenary/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/DendromusDenary/subscriptions","organizations_url":"https://api.github.com/users/DendromusDenary/orgs","repos_url":"https://api.github.com/users/DendromusDenary/repos","events_url":"https://api.github.com/users/DendromusDenary/events{/privacy}","received_events_url":"https://api.github.com/users/DendromusDenary/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2014-06-26T12:11:50Z","updated_at":"2014-10-04T04:07:19Z","closed_at":"2014-06-26T12:13:29Z","author_association":"NONE","body":"Netty version: 4.0.20.Final\n\nHello, here is my problem:\n\nMy server pipeline:\n\n``` java\nchannel.pipeline().addLast(MvcConfig.CNL_INFO_HANDLER, new InfoHandler(0, 0));  // handler which collects information for server\nchannel.pipeline().addLast(new HttpRequestDecoder());  // netty handler\nchannel.pipeline().addLast(new HttpObjectAggregator(8388608));  // netty handler\nchannel.pipeline().addLast(MvcConfig.CNL_SERVER_HANDLER, new HttpServerHandler(server));  // my server handler\nchannel.pipeline().addLast(new HttpResponseEncoder());  // netty handler\n```\n\nIf server receives http header 'Expect: 100-continue' HttpObjectAggregator invokes next code:\n\n``` java\n            if (is100ContinueExpected(m)) {\n                ctx.writeAndFlush(CONTINUE).addListener(new ChannelFutureListener() {\n                    @Override\n                    public void operationComplete(ChannelFuture future) throws Exception {\n                        if (!future.isSuccess()) {\n                            ctx.fireExceptionCaught(future.cause());\n                        }\n                    }\n                });\n            }\n```\n\nIt tries to send CONTINUE response status to the client and I'm getting exception:\n\n``` java\njava.lang.UnsupportedOperationException: unsupported message type: DefaultFullHttpResponse\n    at io.netty.channel.nio.AbstractNioByteChannel.doWrite(AbstractNioByteChannel.java:261) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.socket.nio.NioSocketChannel.doWrite(NioSocketChannel.java:248) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at fw.network.shared.StatefulNioSocketChannel.doWrite(StatefulNioSocketChannel.java:49) ~[classes/:na]\n    at io.netty.channel.AbstractChannel$AbstractUnsafe.flush0(AbstractChannel.java:694) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.AbstractNioChannel$AbstractNioUnsafe.flush0(AbstractNioChannel.java:315) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannel$AbstractUnsafe.flush(AbstractChannel.java:663) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.DefaultChannelPipeline$HeadContext.flush(DefaultChannelPipeline.java:1059) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.invokeFlush(AbstractChannelHandlerContext.java:687) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.flush(AbstractChannelHandlerContext.java:668) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.ChannelDuplexHandler.flush(ChannelDuplexHandler.java:117) ~[netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.invokeFlush(AbstractChannelHandlerContext.java:687) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.write(AbstractChannelHandlerContext.java:717) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.writeAndFlush(AbstractChannelHandlerContext.java:705) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.writeAndFlush(AbstractChannelHandlerContext.java:740) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:129) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:52) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:89) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:163) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.handler.traffic.AbstractTrafficShapingHandler.channelRead(AbstractTrafficShapingHandler.java:223) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at fw.network.mvc.InfoHandler.channelRead(InfoHandler.java:28) [classes/:na]\n    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:332) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:318) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:787) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:125) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:507) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized(NioEventLoop.java:464) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:378) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:350) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:116) [netty-all-4.0.20.Final.jar:4.0.20.Final]\n    at java.lang.Thread.run(Thread.java:744) [na:1.8.0]\n```\n\nLooks like this response doesn't go through HttpResponseEncoder and hence this exceptions occurs. When I changed writing to the pipeline, instead of direct writing to the ctx, everything goes well:\n\n``` java\n....\n ctx.pipeline().writeAndFlush(CONTINUE).addListener(....)\n....\n```\n\n$ java -version\njava version \"1.8.0\"\nJava(TM) SE Runtime Environment (build 1.8.0-b132)\nJava HotSpot(TM) 64-Bit Server VM (build 25.0-b70, mixed mode)\n\n$ uname -a\nLinux denary 3.5.0-17-generic #28-Ubuntu SMP Tue Oct 9 19:31:23 UTC 2012 x86_64 x86_64 x86_64 GNU/Linux\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["47218058"], "labels":["not a bug"]}