{"id":"6008", "title":"value setContentLength is not a member of object io.netty.handler.codec.http.HttpUtil", "body":"Hi, I am using SBT together with IDEA to work on a Scala project using Netty. Netty included in SBT configuration as a dependency to be downloaded from Maven.

I got this compilation error:  value setContentLength is not a member of object io.netty.handler.codec.http.HttpUtil

Within IDEA I checked HttpUtil's source code,which is as below, clearly not as described 
in Netty API documentation for class io.netty.handler.codec.http.HttpUtil.

I downloaded netty's jar and put it in SBT build directory as a local library. Then the compilation passed.

I guess some how the source file downloaded by IDEA is out of sync with the actual compiled distribution. 

/*
 * Copyright 2015 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the \"License\"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.handler.codec.http;

import java.net.URI;

/**
 * Utility methods useful in the HTTP context.
 */
public final class HttpUtil {
    private HttpUtil() { }

    /**
     * Determine if a uri is in origin-form according to
     * <a href=\"https://tools.ietf.org/html/rfc7230#section-5.3\">rfc7230, 5.3</a>.
     */
    public static boolean isOriginForm(URI uri) {
        return uri.getScheme() == null && uri.getSchemeSpecificPart() == null &&
               uri.getHost() == null && uri.getAuthority() == null;
    }

    /**
     * Determine if a uri is in asteric-form according to
     * <a href=\"https://tools.ietf.org/html/rfc7230#section-5.3\">rfc7230, 5.3</a>.
     */
    public static boolean isAsteriskForm(URI uri) {
        return \"*\".equals(uri.getPath()) &&
                uri.getScheme() == null && uri.getSchemeSpecificPart() == null &&
                uri.getHost() == null && uri.getAuthority() == null && uri.getQuery() == null &&
                uri.getFragment() == null;
    }
}
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6008","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6008/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6008/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6008/events","html_url":"https://github.com/netty/netty/issues/6008","id":188674450,"node_id":"MDU6SXNzdWUxODg2NzQ0NTA=","number":6008,"title":"value setContentLength is not a member of object io.netty.handler.codec.http.HttpUtil","user":{"login":"fangqiao","id":17473990,"node_id":"MDQ6VXNlcjE3NDczOTkw","avatar_url":"https://avatars3.githubusercontent.com/u/17473990?v=4","gravatar_id":"","url":"https://api.github.com/users/fangqiao","html_url":"https://github.com/fangqiao","followers_url":"https://api.github.com/users/fangqiao/followers","following_url":"https://api.github.com/users/fangqiao/following{/other_user}","gists_url":"https://api.github.com/users/fangqiao/gists{/gist_id}","starred_url":"https://api.github.com/users/fangqiao/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/fangqiao/subscriptions","organizations_url":"https://api.github.com/users/fangqiao/orgs","repos_url":"https://api.github.com/users/fangqiao/repos","events_url":"https://api.github.com/users/fangqiao/events{/privacy}","received_events_url":"https://api.github.com/users/fangqiao/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-11-11T03:03:46Z","updated_at":"2016-11-14T12:46:38Z","closed_at":"2016-11-14T12:46:38Z","author_association":"NONE","body":"Hi, I am using SBT together with IDEA to work on a Scala project using Netty. Netty included in SBT configuration as a dependency to be downloaded from Maven.\r\n\r\nI got this compilation error:  value setContentLength is not a member of object io.netty.handler.codec.http.HttpUtil\r\n\r\nWithin IDEA I checked HttpUtil's source code,which is as below, clearly not as described \r\nin Netty API documentation for class io.netty.handler.codec.http.HttpUtil.\r\n\r\nI downloaded netty's jar and put it in SBT build directory as a local library. Then the compilation passed.\r\n\r\nI guess some how the source file downloaded by IDEA is out of sync with the actual compiled distribution. \r\n\r\n/*\r\n * Copyright 2015 The Netty Project\r\n *\r\n * The Netty Project licenses this file to you under the Apache License,\r\n * version 2.0 (the \"License\"); you may not use this file except in compliance\r\n * with the License. You may obtain a copy of the License at:\r\n *\r\n *   http://www.apache.org/licenses/LICENSE-2.0\r\n *\r\n * Unless required by applicable law or agreed to in writing, software\r\n * distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT\r\n * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the\r\n * License for the specific language governing permissions and limitations\r\n * under the License.\r\n */\r\npackage io.netty.handler.codec.http;\r\n\r\nimport java.net.URI;\r\n\r\n/**\r\n * Utility methods useful in the HTTP context.\r\n */\r\npublic final class HttpUtil {\r\n    private HttpUtil() { }\r\n\r\n    /**\r\n     * Determine if a uri is in origin-form according to\r\n     * <a href=\"https://tools.ietf.org/html/rfc7230#section-5.3\">rfc7230, 5.3</a>.\r\n     */\r\n    public static boolean isOriginForm(URI uri) {\r\n        return uri.getScheme() == null && uri.getSchemeSpecificPart() == null &&\r\n               uri.getHost() == null && uri.getAuthority() == null;\r\n    }\r\n\r\n    /**\r\n     * Determine if a uri is in asteric-form according to\r\n     * <a href=\"https://tools.ietf.org/html/rfc7230#section-5.3\">rfc7230, 5.3</a>.\r\n     */\r\n    public static boolean isAsteriskForm(URI uri) {\r\n        return \"*\".equals(uri.getPath()) &&\r\n                uri.getScheme() == null && uri.getSchemeSpecificPart() == null &&\r\n                uri.getHost() == null && uri.getAuthority() == null && uri.getQuery() == null &&\r\n                uri.getFragment() == null;\r\n    }\r\n}\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["260326177"], "labels":[]}