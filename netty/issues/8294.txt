{"id":"8294", "title":"can not reopen tcp connection on the netty 4.1.19.Final", "body":"### Expected behavior
Just want to make any connection,but it does't work.

### Actual behavior
1. make a connection 
2. close the connection
3. make a connection again
4. fail with exception

### Steps to reproduce
look above

### Minimal yet complete reproducer code (or URL to code)
**QuoteClient#onConnect(String hostname,int port)**
```java
       // event loop
        EventLoopGroup worker = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(worker);

        // declare to be client
        bootstrap.channel(NioSocketChannel.class);

        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                ChannelPipeline pipeline = socketChannel.pipeline();
                pipeline.addLast(new HsMessageDecoder());
                pipeline.addLast(new HsMessageEncoder());
                pipeline.addLast(new HsPacketEncoder());
                pipeline.addLast(handler);
            }
        });

        // make a connection
        ChannelFuture connectFuture = bootstrap.connect(new InetSocketAddress(hostname, port));
        connectFuture.syncUninterruptibly();
        Channel channel = connectFuture.channel();

       return channel ;
```

**close()**
```java
    public void close(Channel channel) {
            ChannelFuture closeFuture = channel.disconnect();
            closeFuture.syncUninterruptibly();
            System.out.println(\"Is close the channel?-->\" + !closeFuture.channel().isActive());
```

**ClientMain.java**
```java
    public static void main(String[] args) {
        QuoteClient client = QuoteClient.getInstance() ;
        try {
            Channel channel = client.onConnect(\"hq.hscloud.cn\" , 9999) ;
            Thread.sleep(5000);

            client.close(channelId);

            Thread.sleep(5000);

            channel = client.onConnect(\"hq.hscloud.cn\" , 9999) ;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```
the result with exception below
```java
Is close the channel?-->true
java.nio.channels.ClosedChannelException
	at io.netty.channel.AbstractChannel$AbstractUnsafe.ensureOpen(...)(Unknown Source)
```

### Netty version
v4.1.19.Final

### JVM version (e.g. `java -version`)
JDK1.8

### OS version (e.g. `uname -a`)
Window 10+", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8294","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8294/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8294/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8294/events","html_url":"https://github.com/netty/netty/issues/8294","id":361105080,"node_id":"MDU6SXNzdWUzNjExMDUwODA=","number":8294,"title":"can not reopen tcp connection on the netty 4.1.19.Final","user":{"login":"jtjsir","id":17779449,"node_id":"MDQ6VXNlcjE3Nzc5NDQ5","avatar_url":"https://avatars3.githubusercontent.com/u/17779449?v=4","gravatar_id":"","url":"https://api.github.com/users/jtjsir","html_url":"https://github.com/jtjsir","followers_url":"https://api.github.com/users/jtjsir/followers","following_url":"https://api.github.com/users/jtjsir/following{/other_user}","gists_url":"https://api.github.com/users/jtjsir/gists{/gist_id}","starred_url":"https://api.github.com/users/jtjsir/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jtjsir/subscriptions","organizations_url":"https://api.github.com/users/jtjsir/orgs","repos_url":"https://api.github.com/users/jtjsir/repos","events_url":"https://api.github.com/users/jtjsir/events{/privacy}","received_events_url":"https://api.github.com/users/jtjsir/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-09-18T01:48:27Z","updated_at":"2018-09-18T02:30:30Z","closed_at":"2018-09-18T02:30:30Z","author_association":"NONE","body":"### Expected behavior\r\nJust want to make any connection,but it does't work.\r\n\r\n### Actual behavior\r\n1. make a connection \r\n2. close the connection\r\n3. make a connection again\r\n4. fail with exception\r\n\r\n### Steps to reproduce\r\nlook above\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n**QuoteClient#onConnect(String hostname,int port)**\r\n```java\r\n       // event loop\r\n        EventLoopGroup worker = new NioEventLoopGroup();\r\n        Bootstrap bootstrap = new Bootstrap();\r\n        bootstrap.group(worker);\r\n\r\n        // declare to be client\r\n        bootstrap.channel(NioSocketChannel.class);\r\n\r\n        bootstrap.handler(new ChannelInitializer<SocketChannel>() {\r\n            @Override\r\n            protected void initChannel(SocketChannel socketChannel) throws Exception {\r\n                ChannelPipeline pipeline = socketChannel.pipeline();\r\n                pipeline.addLast(new HsMessageDecoder());\r\n                pipeline.addLast(new HsMessageEncoder());\r\n                pipeline.addLast(new HsPacketEncoder());\r\n                pipeline.addLast(handler);\r\n            }\r\n        });\r\n\r\n        // make a connection\r\n        ChannelFuture connectFuture = bootstrap.connect(new InetSocketAddress(hostname, port));\r\n        connectFuture.syncUninterruptibly();\r\n        Channel channel = connectFuture.channel();\r\n\r\n       return channel ;\r\n```\r\n\r\n**close()**\r\n```java\r\n    public void close(Channel channel) {\r\n            ChannelFuture closeFuture = channel.disconnect();\r\n            closeFuture.syncUninterruptibly();\r\n            System.out.println(\"Is close the channel?-->\" + !closeFuture.channel().isActive());\r\n```\r\n\r\n**ClientMain.java**\r\n```java\r\n    public static void main(String[] args) {\r\n        QuoteClient client = QuoteClient.getInstance() ;\r\n        try {\r\n            Channel channel = client.onConnect(\"hq.hscloud.cn\" , 9999) ;\r\n            Thread.sleep(5000);\r\n\r\n            client.close(channelId);\r\n\r\n            Thread.sleep(5000);\r\n\r\n            channel = client.onConnect(\"hq.hscloud.cn\" , 9999) ;\r\n\r\n        } catch (Exception e) {\r\n            e.printStackTrace();\r\n        }\r\n    }\r\n```\r\nthe result with exception below\r\n```java\r\nIs close the channel?-->true\r\njava.nio.channels.ClosedChannelException\r\n\tat io.netty.channel.AbstractChannel$AbstractUnsafe.ensureOpen(...)(Unknown Source)\r\n```\r\n\r\n### Netty version\r\nv4.1.19.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\nJDK1.8\r\n\r\n### OS version (e.g. `uname -a`)\r\nWindow 10+","closed_by":{"login":"jtjsir","id":17779449,"node_id":"MDQ6VXNlcjE3Nzc5NDQ5","avatar_url":"https://avatars3.githubusercontent.com/u/17779449?v=4","gravatar_id":"","url":"https://api.github.com/users/jtjsir","html_url":"https://github.com/jtjsir","followers_url":"https://api.github.com/users/jtjsir/followers","following_url":"https://api.github.com/users/jtjsir/following{/other_user}","gists_url":"https://api.github.com/users/jtjsir/gists{/gist_id}","starred_url":"https://api.github.com/users/jtjsir/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jtjsir/subscriptions","organizations_url":"https://api.github.com/users/jtjsir/orgs","repos_url":"https://api.github.com/users/jtjsir/repos","events_url":"https://api.github.com/users/jtjsir/events{/privacy}","received_events_url":"https://api.github.com/users/jtjsir/received_events","type":"User","site_admin":false}}", "commentIds":["422227878"], "labels":[]}