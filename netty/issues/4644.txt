{"id":"4644", "title":"Netty 4.0.33Final UDT Transport doesnt work", "body":"Hi,

Maybe this is only an problem with our server and client, but it can also be an bug in netty.

We have written an server and client with netty 4.0.33Final, but connection doesnt work, it says ERROR_READ and ERROR_WRITE.
Server and client are like netty examples.

```
329 [main] INFO com.barchart.udt.lib.LibraryLoaderUDT  - Release libraries loaded.
333 [main] DEBUG com.barchart.udt.SocketUDT  - native library load & init OK
334 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10fa] DATAGRAM INIT bind=null:0 peer=null:0
335 [main] DEBUG com.barchart.udt.EpollUDT  - ep 1 create
338 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10f9] DATAGRAM INIT bind=null:0 peer=null:0
338 [main] DEBUG com.barchart.udt.EpollUDT  - ep 2 create
340 [main] DEBUG root  - 2 servers registered.
340 [main] INFO root  - Connect to server 127.0.0.1:5026.
354 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10f8] DATAGRAM INIT bind=null:0 peer=null:0
377 [main] DEBUG io.netty.util.internal.ThreadLocalRandom  - -Dio.netty.initialSeedUniquifier: 0x09ac55297b04521f (took 15 ms)
431 [main] DEBUG io.netty.buffer.ByteBufUtil  - -Dio.netty.allocator.type: unpooled
431 [main] DEBUG io.netty.buffer.ByteBufUtil  - -Dio.netty.threadLocalDirectBufferSize: 65536
448 [main] DEBUG root  - pipeline initialized.
454 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM INIT bind=null:0 peer=null:0 ERROR
457 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca] REGISTERED
457 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca] CONNECT(/127.0.0.1:5026, null)
460 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTING bind=/0.0.0.0:60161 peer=null:0
460 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM CONNECTING bind=/0.0.0.0:60161 peer=null:0 ERROR_WRITE
494 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026
494 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca, /127.0.0.1:60161 => /127.0.0.1:5026] ACTIVE
495 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026
495 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026 ERROR_READ
```

Server:

``` java
//create thread factory for event loop groups
        final ThreadFactory acceptFactory = new DefaultThreadFactory(\"accept\");
        final ThreadFactory connectFactory = new DefaultThreadFactory(\"connect\");

        //create new bootstrap
        bootstrap = new ServerBootstrap();
        bossGroup = new NioEventLoopGroup(nOfBossThreads, acceptFactory, NioUdtProvider.BYTE_PROVIDER);
        workerGroup = new NioEventLoopGroup(nOfWorkerThreads, connectFactory, NioUdtProvider.BYTE_PROVIDER);
        bootstrap.group(bossGroup, workerGroup);

        //use NIO UDT Provider
        bootstrap.channelFactory(NioUdtProvider.BYTE_ACCEPTOR)
                .option(ChannelOption.SO_BACKLOG, 10)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<UdtChannel>() {
                    @Override
                    public void initChannel(final UdtChannel ch)
                            throws Exception {
                        /**
                        * initPipeline() is abstract and has to be implemented by extended class to initialize pipeline
                        */
                        Pipeline pipeline = ch.pipeline();

                        //use gzip compression
                        pipeline.addLast(\"deflater\", ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
                        pipeline.addLast(\"inflater\", ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));

                        pipeline.addLast(\"logger\", new LoggingHandler(LogLevel.DEBUG));

                        //add specific message encoder and decoder for client <--> proxy server connection
                        pipeline.addLast(\"encoder\", new ProxyMessageEncoder());
                        pipeline.addLast(\"decoder\", new ProxyMessageDecoder());

                        //add channel handler to pipeline
                        pipeline.addLast(\"message\", new ProxyChannelHandler(this.authorizationManager, proxyFirewall));
                    }
                });

                //...
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4644","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4644/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4644/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4644/events","html_url":"https://github.com/netty/netty/issues/4644","id":124551307,"node_id":"MDU6SXNzdWUxMjQ1NTEzMDc=","number":4644,"title":"Netty 4.0.33Final UDT Transport doesnt work","user":{"login":"JuKu","id":1138046,"node_id":"MDQ6VXNlcjExMzgwNDY=","avatar_url":"https://avatars3.githubusercontent.com/u/1138046?v=4","gravatar_id":"","url":"https://api.github.com/users/JuKu","html_url":"https://github.com/JuKu","followers_url":"https://api.github.com/users/JuKu/followers","following_url":"https://api.github.com/users/JuKu/following{/other_user}","gists_url":"https://api.github.com/users/JuKu/gists{/gist_id}","starred_url":"https://api.github.com/users/JuKu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/JuKu/subscriptions","organizations_url":"https://api.github.com/users/JuKu/orgs","repos_url":"https://api.github.com/users/JuKu/repos","events_url":"https://api.github.com/users/JuKu/events{/privacy}","received_events_url":"https://api.github.com/users/JuKu/received_events","type":"User","site_admin":false},"labels":[{"id":6731717,"node_id":"MDU6TGFiZWw2NzMxNzE3","url":"https://api.github.com/repos/netty/netty/labels/won't%20fix","name":"won't fix","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":null,"comments":16,"created_at":"2016-01-01T17:17:53Z","updated_at":"2017-03-01T05:24:47Z","closed_at":"2017-03-01T05:24:34Z","author_association":"NONE","body":"Hi,\n\nMaybe this is only an problem with our server and client, but it can also be an bug in netty.\n\nWe have written an server and client with netty 4.0.33Final, but connection doesnt work, it says ERROR_READ and ERROR_WRITE.\nServer and client are like netty examples.\n\n```\n329 [main] INFO com.barchart.udt.lib.LibraryLoaderUDT  - Release libraries loaded.\n333 [main] DEBUG com.barchart.udt.SocketUDT  - native library load & init OK\n334 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10fa] DATAGRAM INIT bind=null:0 peer=null:0\n335 [main] DEBUG com.barchart.udt.EpollUDT  - ep 1 create\n338 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10f9] DATAGRAM INIT bind=null:0 peer=null:0\n338 [main] DEBUG com.barchart.udt.EpollUDT  - ep 2 create\n340 [main] DEBUG root  - 2 servers registered.\n340 [main] INFO root  - Connect to server 127.0.0.1:5026.\n354 [main] DEBUG com.barchart.udt.SocketUDT  - init : [id: 0x087d10f8] DATAGRAM INIT bind=null:0 peer=null:0\n377 [main] DEBUG io.netty.util.internal.ThreadLocalRandom  - -Dio.netty.initialSeedUniquifier: 0x09ac55297b04521f (took 15 ms)\n431 [main] DEBUG io.netty.buffer.ByteBufUtil  - -Dio.netty.allocator.type: unpooled\n431 [main] DEBUG io.netty.buffer.ByteBufUtil  - -Dio.netty.threadLocalDirectBufferSize: 65536\n448 [main] DEBUG root  - pipeline initialized.\n454 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM INIT bind=null:0 peer=null:0 ERROR\n457 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca] REGISTERED\n457 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca] CONNECT(/127.0.0.1:5026, null)\n460 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTING bind=/0.0.0.0:60161 peer=null:0\n460 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM CONNECTING bind=/0.0.0.0:60161 peer=null:0 ERROR_WRITE\n494 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026\n494 [connect-3-1] DEBUG io.netty.handler.logging.LoggingHandler  - [id: 0x148edaca, /127.0.0.1:60161 => /127.0.0.1:5026] ACTIVE\n495 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 rem [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026\n495 [connect-3-1] DEBUG com.barchart.udt.EpollUDT  - ep 1 add [id: 0x087d10f8] DATAGRAM CONNECTED bind=/127.0.0.1:60161 peer=/127.0.0.1:5026 ERROR_READ\n```\n\nServer:\n\n``` java\n//create thread factory for event loop groups\n        final ThreadFactory acceptFactory = new DefaultThreadFactory(\"accept\");\n        final ThreadFactory connectFactory = new DefaultThreadFactory(\"connect\");\n\n        //create new bootstrap\n        bootstrap = new ServerBootstrap();\n        bossGroup = new NioEventLoopGroup(nOfBossThreads, acceptFactory, NioUdtProvider.BYTE_PROVIDER);\n        workerGroup = new NioEventLoopGroup(nOfWorkerThreads, connectFactory, NioUdtProvider.BYTE_PROVIDER);\n        bootstrap.group(bossGroup, workerGroup);\n\n        //use NIO UDT Provider\n        bootstrap.channelFactory(NioUdtProvider.BYTE_ACCEPTOR)\n                .option(ChannelOption.SO_BACKLOG, 10)\n                .handler(new LoggingHandler(LogLevel.INFO))\n                .childHandler(new ChannelInitializer<UdtChannel>() {\n                    @Override\n                    public void initChannel(final UdtChannel ch)\n                            throws Exception {\n                        /**\n                        * initPipeline() is abstract and has to be implemented by extended class to initialize pipeline\n                        */\n                        Pipeline pipeline = ch.pipeline();\n\n                        //use gzip compression\n                        pipeline.addLast(\"deflater\", ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));\n                        pipeline.addLast(\"inflater\", ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));\n\n                        pipeline.addLast(\"logger\", new LoggingHandler(LogLevel.DEBUG));\n\n                        //add specific message encoder and decoder for client <--> proxy server connection\n                        pipeline.addLast(\"encoder\", new ProxyMessageEncoder());\n                        pipeline.addLast(\"decoder\", new ProxyMessageDecoder());\n\n                        //add channel handler to pipeline\n                        pipeline.addLast(\"message\", new ProxyChannelHandler(this.authorizationManager, proxyFirewall));\n                    }\n                });\n\n                //...\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["168999802","169057114","169057537","175293659","239127582","239129580","239129974","239393713","239394600","282731952","282732917","282913116","282955705","282985513","283039446","283248192"], "labels":["won't fix"]}