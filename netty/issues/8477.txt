{"id":"8477", "title":"SSLSession#getCipherSuite() returns SSL_NULL_WITH_NULL_NULL with TLS 1.3", "body":"I have a `FutureListener` which I'm adding to `SslHandler#handshakeFuture()` with the purpose gathering statistics on TLS protocols and ciphers. Something like this:

```java
void log(SslHandler handler) {
  SSLEngine sslEngine = handler.engine();
    
  Future<Channel> handshakeFuture = handler.handshakeFuture();
  handshakeFuture.addListener(new MySslLogger(sslEngine));
}

class MySslLogger implements FutureListener<Channel> {
  private final SSLEngine sslEngine;

  public HandshakeListener(SSLEngine sslEngine) {
    this.sslEngine = sslEngine;
  }

  @Override
  public void operationComplete(Future<Channel> future) throws Exception {
    SSLSession sslSession = sslEngine.getSession();
    String protocol = sslSession.getProtocol();
    String cipher = sslSession.getCipherSuite();

    System.out.println(\"protocol=\" + protocol + \", cipher=\" + cipher);
  }
}
```

It appears the reported cipher suite as returned by `SSLSession#getCipherSuite()` remains `SSL_NULL_WITH_NULL_NULL` in conjunction with TLS 1.3. It works fine with TLS 1.2 and the handshake itself is fine otherwise.

### Expected behavior

### Actual behavior

### Steps to reproduce

### Minimal yet complete reproducer code (or URL to code)

### Netty version

4.1.31

### JVM version (e.g. `java -version`)

Tried OpenJDK 1.8.0_192 and 11.0.1

### OS version (e.g. `uname -a`)
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8477","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8477/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8477/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8477/events","html_url":"https://github.com/netty/netty/issues/8477","id":378466551,"node_id":"MDU6SXNzdWUzNzg0NjY1NTE=","number":8477,"title":"SSLSession#getCipherSuite() returns SSL_NULL_WITH_NULL_NULL with TLS 1.3","user":{"login":"rkapsi","id":191635,"node_id":"MDQ6VXNlcjE5MTYzNQ==","avatar_url":"https://avatars0.githubusercontent.com/u/191635?v=4","gravatar_id":"","url":"https://api.github.com/users/rkapsi","html_url":"https://github.com/rkapsi","followers_url":"https://api.github.com/users/rkapsi/followers","following_url":"https://api.github.com/users/rkapsi/following{/other_user}","gists_url":"https://api.github.com/users/rkapsi/gists{/gist_id}","starred_url":"https://api.github.com/users/rkapsi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rkapsi/subscriptions","organizations_url":"https://api.github.com/users/rkapsi/orgs","repos_url":"https://api.github.com/users/rkapsi/repos","events_url":"https://api.github.com/users/rkapsi/events{/privacy}","received_events_url":"https://api.github.com/users/rkapsi/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/194","html_url":"https://github.com/netty/netty/milestone/194","labels_url":"https://api.github.com/repos/netty/netty/milestones/194/labels","id":3780424,"node_id":"MDk6TWlsZXN0b25lMzc4MDQyNA==","number":194,"title":"4.1.32.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":71,"state":"closed","created_at":"2018-10-30T07:16:13Z","updated_at":"2018-12-06T08:04:49Z","due_on":null,"closed_at":"2018-11-29T16:43:13Z"},"comments":12,"created_at":"2018-11-07T20:59:14Z","updated_at":"2018-11-14T07:49:14Z","closed_at":"2018-11-14T07:49:14Z","author_association":"MEMBER","body":"I have a `FutureListener` which I'm adding to `SslHandler#handshakeFuture()` with the purpose gathering statistics on TLS protocols and ciphers. Something like this:\r\n\r\n```java\r\nvoid log(SslHandler handler) {\r\n  SSLEngine sslEngine = handler.engine();\r\n    \r\n  Future<Channel> handshakeFuture = handler.handshakeFuture();\r\n  handshakeFuture.addListener(new MySslLogger(sslEngine));\r\n}\r\n\r\nclass MySslLogger implements FutureListener<Channel> {\r\n  private final SSLEngine sslEngine;\r\n\r\n  public HandshakeListener(SSLEngine sslEngine) {\r\n    this.sslEngine = sslEngine;\r\n  }\r\n\r\n  @Override\r\n  public void operationComplete(Future<Channel> future) throws Exception {\r\n    SSLSession sslSession = sslEngine.getSession();\r\n    String protocol = sslSession.getProtocol();\r\n    String cipher = sslSession.getCipherSuite();\r\n\r\n    System.out.println(\"protocol=\" + protocol + \", cipher=\" + cipher);\r\n  }\r\n}\r\n```\r\n\r\nIt appears the reported cipher suite as returned by `SSLSession#getCipherSuite()` remains `SSL_NULL_WITH_NULL_NULL` in conjunction with TLS 1.3. It works fine with TLS 1.2 and the handshake itself is fine otherwise.\r\n\r\n### Expected behavior\r\n\r\n### Actual behavior\r\n\r\n### Steps to reproduce\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\n\r\n4.1.31\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\nTried OpenJDK 1.8.0_192 and 11.0.1\r\n\r\n### OS version (e.g. `uname -a`)\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["436775758","436776426","436776633","436776722","436777930","436778260","436898991","437034176","437040386","437044650","437278333","437358531"], "labels":["defect"]}