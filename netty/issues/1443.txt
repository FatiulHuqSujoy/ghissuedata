{"id":"1443", "title":"Pooled buffer leak warning", "body":"-   OS X 10.8.4, Java 1.7.0u21, 2.6GHz i7 (4 cores, hyperthreaded)
-   Stack trace of the warning below.
-   Simulated load with 5 clients sending 4 messages each simultaneously.
-   Versions of the test using local and NIO connections work fine with no warnings.
-   OIO version always shows the warning and drops 3-4 of the 20 messages.
-   Can't share the full code, unfortunately, but the pipelines were very simple -- just a simple echo message from an OIO client to a server.  The entire echo handler is below.
-   Log message that Netty generates about threading are at the bottom.

```
@Override
public void messageReceived(ChannelHandlerContext ctx, MessageList<Object> msgs) {
  LOG.debug(\"Server received {} request{}.\", msgs.size(), msgs.size() > 1 ? \"s\" : \"\");
  for (Object msg : msgs) {
    if (LOG.isTraceEnabled()) {
      if (msg instanceof ByteBuf) {
        LOG.trace(\"Server received {} byte request.\", ((ByteBuf) msg).readableBytes());
      }
    }
    ctx.write(msg).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE)
        .addListener(new ChannelFutureListener() {
          @Override
          public void operationComplete(ChannelFuture future) throws Exception {
            LOG.debug(\"Server counting down.\");
            ServerPipeline.this.latch.countDown();
          }
        });
  }
  // Do not forward because we should be last.
}
```

```
13:19:45.509 [OIO Client Event Loop thread #0] WARN  io.netty.util.ResourceLeakDetector - LEAK: ByteBuf was GC'd before being released correctly.
io.netty.util.ResourceLeakException: io.netty.buffer.PooledUnsafeDirectByteBuf@632b2d25
    at io.netty.util.ResourceLeakDetector$DefaultResourceLeak.<init>(ResourceLeakDetector.java:158) ~[netty-common-4.0.0.CR4.jar:na]
    at io.netty.util.ResourceLeakDetector.open(ResourceLeakDetector.java:103) ~[netty-common-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledByteBuf.<init>(PooledByteBuf.java:42) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledUnsafeDirectByteBuf.<init>(PooledUnsafeDirectByteBuf.java:51) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledUnsafeDirectByteBuf.<init>(PooledUnsafeDirectByteBuf.java:31) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledUnsafeDirectByteBuf$1.newObject(PooledUnsafeDirectByteBuf.java:38) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledUnsafeDirectByteBuf$1.newObject(PooledUnsafeDirectByteBuf.java:35) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.util.Recycler.get(Recycler.java:40) ~[netty-common-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledUnsafeDirectByteBuf.newInstance(PooledUnsafeDirectByteBuf.java:43) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PoolArena$DirectArena.newByteBuf(PoolArena.java:388) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PoolArena.allocate(PoolArena.java:93) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.PooledByteBufAllocator.newDirectBuffer(PooledByteBufAllocator.java:217) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.AbstractByteBufAllocator.directBuffer(AbstractByteBufAllocator.java:130) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.AbstractByteBufAllocator.directBuffer(AbstractByteBufAllocator.java:116) ~[netty-buffer-4.0.0.CR4.jar:na]
    at io.netty.buffer.AbstractByteBufAllocator.buffer(AbstractByteBufAllocator.java:50) ~[netty-buffer-4.0.0.CR4.jar:na]
    at com.ticomgeo.rns.bootstrap.BootstrapFactoryTest.getTestMessage(BootstrapFactoryTest.java:204) ~[test-classes/:na]
    at com.ticomgeo.rns.bootstrap.BootstrapFactoryTest.testBootstrap(BootstrapFactoryTest.java:107) ~[test-classes/:na]
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.7.0_21]
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57) ~[na:1.7.0_21]
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.7.0_21]
    at java.lang.reflect.Method.invoke(Method.java:601) ~[na:1.7.0_21]
    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:47) ~[junit-4.11.jar:na]
    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12) ~[junit-4.11.jar:na]
    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:44) ~[junit-4.11.jar:na]
    at mockit.integration.junit4.internal.JUnit4TestRunnerDecorator.executeTestMethod(JUnit4TestRunnerDecorator.java:120) ~[jmockit-1.2.jar:na]
    at mockit.integration.junit4.internal.JUnit4TestRunnerDecorator.invokeExplosively(JUnit4TestRunnerDecorator.java:65) ~[jmockit-1.2.jar:na]
    at mockit.integration.junit4.internal.MockFrameworkMethod.invokeExplosively(MockFrameworkMethod.java:29) ~[jmockit-1.2.jar:na]
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.7.0_21]
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57) ~[na:1.7.0_21]
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.7.0_21]
    at java.lang.reflect.Method.invoke(Method.java:601) ~[na:1.7.0_21]
    at mockit.internal.util.MethodReflection.invokeWithCheckedThrows(MethodReflection.java:95) ~[jmockit-1.2.jar:na]
    at mockit.internal.annotations.MockMethodBridge.callMock(MockMethodBridge.java:76) ~[jmockit-1.2.jar:na]
    at mockit.internal.annotations.MockMethodBridge.invoke(MockMethodBridge.java:41) ~[jmockit-1.2.jar:na]
    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java) ~[junit-4.11.jar:na]
    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17) ~[junit-4.11.jar:na]
    at org.junit.internal.runners.statements.FailOnTimeout$StatementThread.run(FailOnTimeout.java:74) ~[junit-4.11.jar:na]
```

```

13:19:36.380 [Thread-1] DEBUG i.n.c.MultithreadEventLoopGroup - io.netty.eventLoopThreads: 16
13:19:36.398 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - UID: 501
13:19:36.399 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - Java version: 7
13:19:36.401 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.ByteBuffer.cleaner: available
13:19:36.402 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.Buffer.address: available
13:19:36.402 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - sun.misc.Unsafe.theUnsafe: available
13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - sun.misc.Unsafe.copyMemory: available
13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.Bits.unaligned: true
13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - sun.misc.Unsafe: available
13:19:36.490 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - Javassist: available
13:19:36.490 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - io.netty.noPreferDirect: false
13:19:36.539 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.numHeapArenas: 8
13:19:36.539 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.numDirectArenas: 8
13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.pageSize: 8192
13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.maxOrder: 8
13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.chunkSize: 2097152
13:19:36.598 [Thread-1] DEBUG io.netty.util.ResourceLeakDetector - io.netty.noResourceLeakDetection: false
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1443","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1443/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1443/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1443/events","html_url":"https://github.com/netty/netty/issues/1443","id":15521516,"node_id":"MDU6SXNzdWUxNTUyMTUxNg==","number":1443,"title":"Pooled buffer leak warning","user":{"login":"mkw","id":451208,"node_id":"MDQ6VXNlcjQ1MTIwOA==","avatar_url":"https://avatars0.githubusercontent.com/u/451208?v=4","gravatar_id":"","url":"https://api.github.com/users/mkw","html_url":"https://github.com/mkw","followers_url":"https://api.github.com/users/mkw/followers","following_url":"https://api.github.com/users/mkw/following{/other_user}","gists_url":"https://api.github.com/users/mkw/gists{/gist_id}","starred_url":"https://api.github.com/users/mkw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mkw/subscriptions","organizations_url":"https://api.github.com/users/mkw/orgs","repos_url":"https://api.github.com/users/mkw/repos","events_url":"https://api.github.com/users/mkw/events{/privacy}","received_events_url":"https://api.github.com/users/mkw/received_events","type":"User","site_admin":false},"labels":[{"id":6731767,"node_id":"MDU6TGFiZWw2NzMxNzY3","url":"https://api.github.com/repos/netty/netty/labels/needs%20info","name":"needs info","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":10,"created_at":"2013-06-13T18:31:36Z","updated_at":"2013-08-09T13:59:55Z","closed_at":"2013-06-25T09:53:37Z","author_association":"CONTRIBUTOR","body":"-   OS X 10.8.4, Java 1.7.0u21, 2.6GHz i7 (4 cores, hyperthreaded)\n-   Stack trace of the warning below.\n-   Simulated load with 5 clients sending 4 messages each simultaneously.\n-   Versions of the test using local and NIO connections work fine with no warnings.\n-   OIO version always shows the warning and drops 3-4 of the 20 messages.\n-   Can't share the full code, unfortunately, but the pipelines were very simple -- just a simple echo message from an OIO client to a server.  The entire echo handler is below.\n-   Log message that Netty generates about threading are at the bottom.\n\n```\n@Override\npublic void messageReceived(ChannelHandlerContext ctx, MessageList<Object> msgs) {\n  LOG.debug(\"Server received {} request{}.\", msgs.size(), msgs.size() > 1 ? \"s\" : \"\");\n  for (Object msg : msgs) {\n    if (LOG.isTraceEnabled()) {\n      if (msg instanceof ByteBuf) {\n        LOG.trace(\"Server received {} byte request.\", ((ByteBuf) msg).readableBytes());\n      }\n    }\n    ctx.write(msg).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE)\n        .addListener(new ChannelFutureListener() {\n          @Override\n          public void operationComplete(ChannelFuture future) throws Exception {\n            LOG.debug(\"Server counting down.\");\n            ServerPipeline.this.latch.countDown();\n          }\n        });\n  }\n  // Do not forward because we should be last.\n}\n```\n\n```\n13:19:45.509 [OIO Client Event Loop thread #0] WARN  io.netty.util.ResourceLeakDetector - LEAK: ByteBuf was GC'd before being released correctly.\nio.netty.util.ResourceLeakException: io.netty.buffer.PooledUnsafeDirectByteBuf@632b2d25\n    at io.netty.util.ResourceLeakDetector$DefaultResourceLeak.<init>(ResourceLeakDetector.java:158) ~[netty-common-4.0.0.CR4.jar:na]\n    at io.netty.util.ResourceLeakDetector.open(ResourceLeakDetector.java:103) ~[netty-common-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledByteBuf.<init>(PooledByteBuf.java:42) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledUnsafeDirectByteBuf.<init>(PooledUnsafeDirectByteBuf.java:51) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledUnsafeDirectByteBuf.<init>(PooledUnsafeDirectByteBuf.java:31) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledUnsafeDirectByteBuf$1.newObject(PooledUnsafeDirectByteBuf.java:38) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledUnsafeDirectByteBuf$1.newObject(PooledUnsafeDirectByteBuf.java:35) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.util.Recycler.get(Recycler.java:40) ~[netty-common-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledUnsafeDirectByteBuf.newInstance(PooledUnsafeDirectByteBuf.java:43) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PoolArena$DirectArena.newByteBuf(PoolArena.java:388) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PoolArena.allocate(PoolArena.java:93) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.PooledByteBufAllocator.newDirectBuffer(PooledByteBufAllocator.java:217) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.AbstractByteBufAllocator.directBuffer(AbstractByteBufAllocator.java:130) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.AbstractByteBufAllocator.directBuffer(AbstractByteBufAllocator.java:116) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at io.netty.buffer.AbstractByteBufAllocator.buffer(AbstractByteBufAllocator.java:50) ~[netty-buffer-4.0.0.CR4.jar:na]\n    at com.ticomgeo.rns.bootstrap.BootstrapFactoryTest.getTestMessage(BootstrapFactoryTest.java:204) ~[test-classes/:na]\n    at com.ticomgeo.rns.bootstrap.BootstrapFactoryTest.testBootstrap(BootstrapFactoryTest.java:107) ~[test-classes/:na]\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.7.0_21]\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57) ~[na:1.7.0_21]\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.7.0_21]\n    at java.lang.reflect.Method.invoke(Method.java:601) ~[na:1.7.0_21]\n    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:47) ~[junit-4.11.jar:na]\n    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12) ~[junit-4.11.jar:na]\n    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:44) ~[junit-4.11.jar:na]\n    at mockit.integration.junit4.internal.JUnit4TestRunnerDecorator.executeTestMethod(JUnit4TestRunnerDecorator.java:120) ~[jmockit-1.2.jar:na]\n    at mockit.integration.junit4.internal.JUnit4TestRunnerDecorator.invokeExplosively(JUnit4TestRunnerDecorator.java:65) ~[jmockit-1.2.jar:na]\n    at mockit.integration.junit4.internal.MockFrameworkMethod.invokeExplosively(MockFrameworkMethod.java:29) ~[jmockit-1.2.jar:na]\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.7.0_21]\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57) ~[na:1.7.0_21]\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.7.0_21]\n    at java.lang.reflect.Method.invoke(Method.java:601) ~[na:1.7.0_21]\n    at mockit.internal.util.MethodReflection.invokeWithCheckedThrows(MethodReflection.java:95) ~[jmockit-1.2.jar:na]\n    at mockit.internal.annotations.MockMethodBridge.callMock(MockMethodBridge.java:76) ~[jmockit-1.2.jar:na]\n    at mockit.internal.annotations.MockMethodBridge.invoke(MockMethodBridge.java:41) ~[jmockit-1.2.jar:na]\n    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java) ~[junit-4.11.jar:na]\n    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17) ~[junit-4.11.jar:na]\n    at org.junit.internal.runners.statements.FailOnTimeout$StatementThread.run(FailOnTimeout.java:74) ~[junit-4.11.jar:na]\n```\n\n```\n\n13:19:36.380 [Thread-1] DEBUG i.n.c.MultithreadEventLoopGroup - io.netty.eventLoopThreads: 16\n13:19:36.398 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - UID: 501\n13:19:36.399 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - Java version: 7\n13:19:36.401 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.ByteBuffer.cleaner: available\n13:19:36.402 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.Buffer.address: available\n13:19:36.402 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - sun.misc.Unsafe.theUnsafe: available\n13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - sun.misc.Unsafe.copyMemory: available\n13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent0 - java.nio.Bits.unaligned: true\n13:19:36.403 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - sun.misc.Unsafe: available\n13:19:36.490 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - Javassist: available\n13:19:36.490 [Thread-1] DEBUG i.n.util.internal.PlatformDependent - io.netty.noPreferDirect: false\n13:19:36.539 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.numHeapArenas: 8\n13:19:36.539 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.numDirectArenas: 8\n13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.pageSize: 8192\n13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.maxOrder: 8\n13:19:36.540 [Thread-1] DEBUG i.n.buffer.PooledByteBufAllocator - io.netty.allocator.chunkSize: 2097152\n13:19:36.598 [Thread-1] DEBUG io.netty.util.ResourceLeakDetector - io.netty.noResourceLeakDetection: false\n```\n","closed_by":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}}", "commentIds":["19414077","19414276","19414429","19414630","19415262","19461286","19963138","19964431","19971151","22395978"], "labels":["needs info"]}