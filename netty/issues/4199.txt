{"id":"4199", "title":"Netty deadlock with custom OIO transports", "body":"Netty version: 5.0.0-Alpha2

I have an application that uses Android Bluetooth as transport. Looking into code for netty-rxtx it happened to be possible to adopt Android Bluetooth API as a transport for Netty.

here's the source for BluetoothChannel: https://gist.github.com/flirtomania/42d0d36668e4176ae2f7

The only issue was some kind of deadlock, when I called `ctx.writeAndFlush()` in handlers nothing had happened. The problem was that Android's inputstream.read() for Bluetooth was blocking the whole eventloop..

The following workaround helped. I'm just trying not to call read() for Bluetooth inputstream until there are some bytes received:

```
@Override
protected int doReadBytes(ByteBuf buf) throws Exception {
  long timestamp = System.nanoTime();
  while (true) {
    if (available() > 0) {
      break;
    }
    Thread.sleep(config.getAvailableSleepTimeMillis());
    if (System.nanoTime() - timestamp > config().getReadTimeoutNanos()) {
      return 0;
    }
  }
  return super.doReadBytes(buf);
}
```

Then https://github.com/jkschneider/netty-jssc this library also implements channel for Netty over serial ports using jSSC library. And again the write operations was not performed because everything was locked, until channel was closed or some bytes were received.
Luckily the same workaround helped for this jssc transport too:
 https://github.com/flirtomania/netty-jssc/blob/master/src/main/java/com/github/jkschneider/netty/jssc/JsscChannel.java

The first handler in the pipeline is LoggingHandler():

```
return new Bootstrap()
  .group(workerGroup)
  .channel(channelClass)
  .handler(new ChannelInitializer<AbstractChannel>() {
    @Override
    public void initChannel(AbstractChannel ch) throws Exception {
      log.info(\"initChannel {}\", ch);
      ch.pipeline()
        .addLast(new LoggingHandler(LogLevel.TRACE))
        .addLast(new LengthFieldBasedFrameDecoder(2048, 4, 2, -6, 0))
        ...
```

Can you please explain why is this happening? Is the workaround ok? What if `Thread.sleep(config.getAvailableSleepTimeMillis());` will be removed? Will the application consume a lot of CPU then?

Maybe the default implementation of OioByteStreamChannel.doReadBytes() is not ok?
This implementation returns at least 1 always, but it does not work with Android Bluetooth inputstream, causing it to lock also same for netty-jssc library.

```
@Override
protected int doReadBytes(ByteBuf buf) throws Exception {
    int length = Math.max(1, Math.min(available(), buf.maxWritableBytes()));
    return buf.writeBytes(is, length);
}
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4199","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4199/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4199/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4199/events","html_url":"https://github.com/netty/netty/issues/4199","id":105109664,"node_id":"MDU6SXNzdWUxMDUxMDk2NjQ=","number":4199,"title":"Netty deadlock with custom OIO transports","user":{"login":"tttomat19","id":6590011,"node_id":"MDQ6VXNlcjY1OTAwMTE=","avatar_url":"https://avatars2.githubusercontent.com/u/6590011?v=4","gravatar_id":"","url":"https://api.github.com/users/tttomat19","html_url":"https://github.com/tttomat19","followers_url":"https://api.github.com/users/tttomat19/followers","following_url":"https://api.github.com/users/tttomat19/following{/other_user}","gists_url":"https://api.github.com/users/tttomat19/gists{/gist_id}","starred_url":"https://api.github.com/users/tttomat19/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tttomat19/subscriptions","organizations_url":"https://api.github.com/users/tttomat19/orgs","repos_url":"https://api.github.com/users/tttomat19/repos","events_url":"https://api.github.com/users/tttomat19/events{/privacy}","received_events_url":"https://api.github.com/users/tttomat19/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-09-06T18:39:07Z","updated_at":"2018-05-18T21:41:11Z","closed_at":"2018-05-18T21:41:11Z","author_association":"NONE","body":"Netty version: 5.0.0-Alpha2\n\nI have an application that uses Android Bluetooth as transport. Looking into code for netty-rxtx it happened to be possible to adopt Android Bluetooth API as a transport for Netty.\n\nhere's the source for BluetoothChannel: https://gist.github.com/flirtomania/42d0d36668e4176ae2f7\n\nThe only issue was some kind of deadlock, when I called `ctx.writeAndFlush()` in handlers nothing had happened. The problem was that Android's inputstream.read() for Bluetooth was blocking the whole eventloop..\n\nThe following workaround helped. I'm just trying not to call read() for Bluetooth inputstream until there are some bytes received:\n\n```\n@Override\nprotected int doReadBytes(ByteBuf buf) throws Exception {\n  long timestamp = System.nanoTime();\n  while (true) {\n    if (available() > 0) {\n      break;\n    }\n    Thread.sleep(config.getAvailableSleepTimeMillis());\n    if (System.nanoTime() - timestamp > config().getReadTimeoutNanos()) {\n      return 0;\n    }\n  }\n  return super.doReadBytes(buf);\n}\n```\n\nThen https://github.com/jkschneider/netty-jssc this library also implements channel for Netty over serial ports using jSSC library. And again the write operations was not performed because everything was locked, until channel was closed or some bytes were received.\nLuckily the same workaround helped for this jssc transport too:\n https://github.com/flirtomania/netty-jssc/blob/master/src/main/java/com/github/jkschneider/netty/jssc/JsscChannel.java\n\nThe first handler in the pipeline is LoggingHandler():\n\n```\nreturn new Bootstrap()\n  .group(workerGroup)\n  .channel(channelClass)\n  .handler(new ChannelInitializer<AbstractChannel>() {\n    @Override\n    public void initChannel(AbstractChannel ch) throws Exception {\n      log.info(\"initChannel {}\", ch);\n      ch.pipeline()\n        .addLast(new LoggingHandler(LogLevel.TRACE))\n        .addLast(new LengthFieldBasedFrameDecoder(2048, 4, 2, -6, 0))\n        ...\n```\n\nCan you please explain why is this happening? Is the workaround ok? What if `Thread.sleep(config.getAvailableSleepTimeMillis());` will be removed? Will the application consume a lot of CPU then?\n\nMaybe the default implementation of OioByteStreamChannel.doReadBytes() is not ok?\nThis implementation returns at least 1 always, but it does not work with Android Bluetooth inputstream, causing it to lock also same for netty-jssc library.\n\n```\n@Override\nprotected int doReadBytes(ByteBuf buf) throws Exception {\n    int length = Math.max(1, Math.min(available(), buf.maxWritableBytes()));\n    return buf.writeBytes(is, length);\n}\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["390340074"], "labels":[]}