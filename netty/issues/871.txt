{"id":"871", "title":"Typo in documentation", "body":"Minor, yet typos that can be fixed:

On this page:  http://static.netty.io/3.6/guide/#preface.2

Section:  1.1. Combining and Slicing ChannelBuffers

Please change from (2 typos - spelling of traditionally and copying):

\"Tranditionally, data from the multiple packages are combined by coping them into a new byte buffer.\"

to

\"Traditionally, data from the multiple packages are combined by copying them into a new byte buffer.\"

Section 4. Advanced Components for More Rapid Development

Please change from (should be pace and not page):

\"Netty provides a set of advanced features to accelerate the page of development even more\"
to
\"Netty provides a set of advanced features to accelerate the pace of development even more\"

-Hari
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/871","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/871/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/871/comments","events_url":"https://api.github.com/repos/netty/netty/issues/871/events","html_url":"https://github.com/netty/netty/issues/871","id":9578758,"node_id":"MDU6SXNzdWU5NTc4NzU4","number":871,"title":"Typo in documentation","user":{"login":"harikoduru","id":3150821,"node_id":"MDQ6VXNlcjMxNTA4MjE=","avatar_url":"https://avatars3.githubusercontent.com/u/3150821?v=4","gravatar_id":"","url":"https://api.github.com/users/harikoduru","html_url":"https://github.com/harikoduru","followers_url":"https://api.github.com/users/harikoduru/followers","following_url":"https://api.github.com/users/harikoduru/following{/other_user}","gists_url":"https://api.github.com/users/harikoduru/gists{/gist_id}","starred_url":"https://api.github.com/users/harikoduru/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/harikoduru/subscriptions","organizations_url":"https://api.github.com/users/harikoduru/orgs","repos_url":"https://api.github.com/users/harikoduru/repos","events_url":"https://api.github.com/users/harikoduru/events{/privacy}","received_events_url":"https://api.github.com/users/harikoduru/received_events","type":"User","site_admin":false},"labels":[{"id":22125096,"node_id":"MDU6TGFiZWwyMjEyNTA5Ng==","url":"https://api.github.com/repos/netty/netty/labels/documentation","name":"documentation","color":"d4c5f9","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2012-12-30T00:28:40Z","updated_at":"2013-01-04T07:47:15Z","closed_at":"2013-01-04T07:47:15Z","author_association":"NONE","body":"Minor, yet typos that can be fixed:\n\nOn this page:  http://static.netty.io/3.6/guide/#preface.2\n\nSection:  1.1. Combining and Slicing ChannelBuffers\n\nPlease change from (2 typos - spelling of traditionally and copying):\n\n\"Tranditionally, data from the multiple packages are combined by coping them into a new byte buffer.\"\n\nto\n\n\"Traditionally, data from the multiple packages are combined by copying them into a new byte buffer.\"\n\nSection 4. Advanced Components for More Rapid Development\n\nPlease change from (should be pace and not page):\n\n\"Netty provides a set of advanced features to accelerate the page of development even more\"\nto\n\"Netty provides a set of advanced features to accelerate the pace of development even more\"\n\n-Hari\n","closed_by":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}}", "commentIds":["11874786"], "labels":["documentation"]}