{"id":"614", "title":"NioServerSocketChannelFactory.<init> is very slow in 3.5.7", "body":"Recently I have upgraded from Netty 3.5.5 to 3.5.7 and noticed that binding to a port became very slow.

I am using code like this

``` java
logger.debug(\"Port {} starting\", port);
bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newFixedThreadPool(Settings.getDemuxPoolSize()),
                Settings.getDemuxPoolSize()));   
logger.debug(\"Port {} started\", port);  
 ... continue inittialization of bootstrap ...
```

The delay between \"Port {} starting\" and \"Port {} started\" is about 5-8 seconds on my machine, which was not hte case with Netty 3.5.5

most of the thread dumps during binding to a port looked like this:

```
\"main@1\" prio=5 tid=0x1 nid=NA runnable
  java.lang.Thread.State: RUNNABLE
      at java.lang.String.length(String.java:651)
      at java.net.InetAddress.getAllByName(InetAddress.java:1026)
      at java.net.InetAddress.getAllByName(InetAddress.java:1020)
      at java.net.InetAddress.getByName(InetAddress.java:970)
      at sun.nio.ch.PipeImpl$Initializer.run(PipeImpl.java:67)
      at java.security.AccessController.doPrivileged(AccessController.java:-1)
      at sun.nio.ch.PipeImpl.<init>(PipeImpl.java:122)
      at sun.nio.ch.SelectorProviderImpl.openPipe(SelectorProviderImpl.java:27)
      at java.nio.channels.Pipe.open(Pipe.java:133)
      at sun.nio.ch.WindowsSelectorImpl.<init>(WindowsSelectorImpl.java:104)
      at sun.nio.ch.WindowsSelectorProvider.openSelector(WindowsSelectorProvider.java:26)
      at java.nio.channels.Selector.open(Selector.java:209)
      at org.jboss.netty.channel.socket.nio.AbstractNioWorker.openSelector(AbstractNioWorker.java:196)
      at org.jboss.netty.channel.socket.nio.AbstractNioWorker.<init>(AbstractNioWorker.java:127)
      at org.jboss.netty.channel.socket.nio.NioWorker.<init>(NioWorker.java:40)
      at org.jboss.netty.channel.socket.nio.NioWorkerPool.createWorker(NioWorkerPool.java:34)
      at org.jboss.netty.channel.socket.nio.NioWorkerPool.createWorker(NioWorkerPool.java:26)
      at org.jboss.netty.channel.socket.nio.AbstractNioWorkerPool.<init>(AbstractNioWorkerPool.java:56)
      at org.jboss.netty.channel.socket.nio.NioWorkerPool.<init>(NioWorkerPool.java:29)
      at org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory.<init>(NioServerSocketChannelFactory.java:131)
     <call from my code>(NettyPort.java:35)
```

So I guess the problem is in NioServerSocketChannelFactory.<init> method.

Tested on Windows 7, JDK 1.6.0u26
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/614","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/614/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/614/comments","events_url":"https://api.github.com/repos/netty/netty/issues/614/events","html_url":"https://github.com/netty/netty/issues/614","id":6943505,"node_id":"MDU6SXNzdWU2OTQzNTA1","number":614,"title":"NioServerSocketChannelFactory.<init> is very slow in 3.5.7","user":{"login":"deinlandel","id":2368101,"node_id":"MDQ6VXNlcjIzNjgxMDE=","avatar_url":"https://avatars2.githubusercontent.com/u/2368101?v=4","gravatar_id":"","url":"https://api.github.com/users/deinlandel","html_url":"https://github.com/deinlandel","followers_url":"https://api.github.com/users/deinlandel/followers","following_url":"https://api.github.com/users/deinlandel/following{/other_user}","gists_url":"https://api.github.com/users/deinlandel/gists{/gist_id}","starred_url":"https://api.github.com/users/deinlandel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/deinlandel/subscriptions","organizations_url":"https://api.github.com/users/deinlandel/orgs","repos_url":"https://api.github.com/users/deinlandel/repos","events_url":"https://api.github.com/users/deinlandel/events{/privacy}","received_events_url":"https://api.github.com/users/deinlandel/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2012-09-18T07:13:58Z","updated_at":"2019-02-25T14:50:52Z","closed_at":"2012-09-18T19:33:18Z","author_association":"NONE","body":"Recently I have upgraded from Netty 3.5.5 to 3.5.7 and noticed that binding to a port became very slow.\n\nI am using code like this\n\n``` java\nlogger.debug(\"Port {} starting\", port);\nbootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(\n                Executors.newCachedThreadPool(), Executors.newFixedThreadPool(Settings.getDemuxPoolSize()),\n                Settings.getDemuxPoolSize()));   \nlogger.debug(\"Port {} started\", port);  \n ... continue inittialization of bootstrap ...\n```\n\nThe delay between \"Port {} starting\" and \"Port {} started\" is about 5-8 seconds on my machine, which was not hte case with Netty 3.5.5\n\nmost of the thread dumps during binding to a port looked like this:\n\n```\n\"main@1\" prio=5 tid=0x1 nid=NA runnable\n  java.lang.Thread.State: RUNNABLE\n      at java.lang.String.length(String.java:651)\n      at java.net.InetAddress.getAllByName(InetAddress.java:1026)\n      at java.net.InetAddress.getAllByName(InetAddress.java:1020)\n      at java.net.InetAddress.getByName(InetAddress.java:970)\n      at sun.nio.ch.PipeImpl$Initializer.run(PipeImpl.java:67)\n      at java.security.AccessController.doPrivileged(AccessController.java:-1)\n      at sun.nio.ch.PipeImpl.<init>(PipeImpl.java:122)\n      at sun.nio.ch.SelectorProviderImpl.openPipe(SelectorProviderImpl.java:27)\n      at java.nio.channels.Pipe.open(Pipe.java:133)\n      at sun.nio.ch.WindowsSelectorImpl.<init>(WindowsSelectorImpl.java:104)\n      at sun.nio.ch.WindowsSelectorProvider.openSelector(WindowsSelectorProvider.java:26)\n      at java.nio.channels.Selector.open(Selector.java:209)\n      at org.jboss.netty.channel.socket.nio.AbstractNioWorker.openSelector(AbstractNioWorker.java:196)\n      at org.jboss.netty.channel.socket.nio.AbstractNioWorker.<init>(AbstractNioWorker.java:127)\n      at org.jboss.netty.channel.socket.nio.NioWorker.<init>(NioWorker.java:40)\n      at org.jboss.netty.channel.socket.nio.NioWorkerPool.createWorker(NioWorkerPool.java:34)\n      at org.jboss.netty.channel.socket.nio.NioWorkerPool.createWorker(NioWorkerPool.java:26)\n      at org.jboss.netty.channel.socket.nio.AbstractNioWorkerPool.<init>(AbstractNioWorkerPool.java:56)\n      at org.jboss.netty.channel.socket.nio.NioWorkerPool.<init>(NioWorkerPool.java:29)\n      at org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory.<init>(NioServerSocketChannelFactory.java:131)\n     <call from my code>(NettyPort.java:35)\n```\n\nSo I guess the problem is in NioServerSocketChannelFactory.<init> method.\n\nTested on Windows 7, JDK 1.6.0u26\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["8665692","8675906","8680496","8680593","8680704","8785312","8785320","467039432"], "labels":["not a bug"]}