{"id":"7070", "title":"Netty client not receiving full response", "body":"I wrote a simple Netty client that connects to a Server designed using QuickFix/J library. Now the problem is whenever the Server sends Strings at a very high speed the client sometimes not receiving the full strings. That means generally, Server sends a string starting with \"8=FIX.4.2 | 9=123 | ....| 10=21 |\" Every string starts with '8=' and ends with tag '10='. Now at high speed the client is showing up the strings are cut to a particular size and the remaining string is sent in the next retrieval of the Buffer. 

**Below is the output at Netty client:**

> Incoming From Exchange >> 8=FIX.4.29=24735=834=5449=orderSimulator52=20170809-07:38:30.24056=orderGateway1=orderGateway6=1250511=12695039514=017=505320=131=1250532=137=838=139=440=241=11616711044=1250554=155=GC60=20170809-07:38:30.240150=4151=0200=201708207=CME10=255
> 
> Incoming From Exchange >> 8=FIX.4.29=24735=834=5549=orderSimulator52=20170809-07:38:30.26056=orderGateway1=orderGateway6=1250511=10926793114=017=505420=131=1250532=137=538=139=440=241=18595658744=1250554=155=GC60=20170809-07:38:30.260150=4151=0200=201708207=CME10=030
> 
> Incoming From Exchange >> 8=FIX.4.29=24735=834=5649=orderSimulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=10479037514=017=505520=131=1250532=137=238=139=440=241=16819718944=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=0278=FIX.4.29=24735
> 
> Incoming From Exchange >> =834=5749=orderSimulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=17021962114=017=505620=131=1250532=137=738=139=440=241=15642709944=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=0208=FIX.4.29=24735=834=5849=orderS
> 
> Incoming From Exchange >> imulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=14044819714=017=505720=131=1250532=137=438=139=440=241=16279265744=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=030
> 
> Incoming From Exchange >> 8=FIX.4.29=24835=834=5949=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=14108324114=017=505820=131=1250532=137=1138=139=440=241=15954718144=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=073
> 
> Incoming From Exchange >> 8=FIX.4.29=24835=834=6049=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=12836588814=017=505920=131=1250532=137=1238=139=440=241=14005997644=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=092
> 
> Incoming From Exchange >> 8=FIX.4.29=24635=834=6149=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=9376296914=017=506020=131=1250532=137=638=139=440=241=11452227544=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=236
> 

You can clearly see that first 2 responses from the server are in correct format that is starting with \"8=\" and ended exactly with \"10=\". But in the third response, next string got appended to the current string and is cut. In the fourth response I am getting the remaining part of the string. How can I eliminate this? The strings coming from server can be of any size. 

**Below is my client code:**
```
public  class SendStringToExchangeClient {
    public static int port;
    public static int reconnectTime = 1;
    public int MAX_RECONNECTIONS = GetPropertiesFromFile.MAX_RECONNECTIONS;
    Channel channel;
    EventLoopGroup workGroup = new NioEventLoopGroup();
    static ChannelFuture channelFuture = null;
    String message;
    int rcvBuf, sndBuf, lowWaterMark, highWaterMark;
    public SendStringToExchangeClient(int port) throws Exception{
        SendStringToExchangeClient.port = port;
        rcvBuf = 2048;
        sndBuf =   2048;
        lowWaterMark = 1024;
        highWaterMark = 2048;
      //  channelFuture = connectLoop();
    }

    public final void connectLoop() throws InterruptedException {
    try{
            Bootstrap b = new Bootstrap();
            b.group(workGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true)
             .option(ChannelOption.SO_RCVBUF, rcvBuf * 2048)
             .option(ChannelOption.SO_SNDBUF, sndBuf * 2048)
             .option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(lowWaterMark * 2048, highWaterMark * 2048))
             .option(ChannelOption.TCP_NODELAY, true)
            .handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new SendStringToExchangeClientHandler());
                }
            });

            channelFuture = b.connect(GetPropertiesFromFile.SendToExchageIP.trim(), port).sync();
            this.channel = channelFuture.channel();

        } 
            catch(Exception ex){
                System.err.println(\"ERROR : Exchange Not In Connection\");
                Logger.error(\"Exchange not in connection\"+ex.getMessage());
            }
         //   return channelFuture;
        }


    public void shutdown(){
        workGroup.shutdownGracefully();
    }

}


**My client Handler code:**

public class SendStringToExchangeClientHandler extends ChannelInboundHandlerAdapter {
    static int count =0;
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        ByteBuf byteBuf = (ByteBuf) msg;
        //receive from EXCHANGE
        String message = byteBuf.toString(Charset.defaultCharset());
        System.out.println(\"Incoming From Exchange >> \"+message);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

}
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7070","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7070/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7070/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7070/events","html_url":"https://github.com/netty/netty/issues/7070","id":248980841,"node_id":"MDU6SXNzdWUyNDg5ODA4NDE=","number":7070,"title":"Netty client not receiving full response","user":{"login":"Hema-Chandra","id":29230554,"node_id":"MDQ6VXNlcjI5MjMwNTU0","avatar_url":"https://avatars2.githubusercontent.com/u/29230554?v=4","gravatar_id":"","url":"https://api.github.com/users/Hema-Chandra","html_url":"https://github.com/Hema-Chandra","followers_url":"https://api.github.com/users/Hema-Chandra/followers","following_url":"https://api.github.com/users/Hema-Chandra/following{/other_user}","gists_url":"https://api.github.com/users/Hema-Chandra/gists{/gist_id}","starred_url":"https://api.github.com/users/Hema-Chandra/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Hema-Chandra/subscriptions","organizations_url":"https://api.github.com/users/Hema-Chandra/orgs","repos_url":"https://api.github.com/users/Hema-Chandra/repos","events_url":"https://api.github.com/users/Hema-Chandra/events{/privacy}","received_events_url":"https://api.github.com/users/Hema-Chandra/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-08-09T10:07:19Z","updated_at":"2017-08-09T10:37:20Z","closed_at":"2017-08-09T10:37:20Z","author_association":"NONE","body":"I wrote a simple Netty client that connects to a Server designed using QuickFix/J library. Now the problem is whenever the Server sends Strings at a very high speed the client sometimes not receiving the full strings. That means generally, Server sends a string starting with \"8=FIX.4.2 | 9=123 | ....| 10=21 |\" Every string starts with '8=' and ends with tag '10='. Now at high speed the client is showing up the strings are cut to a particular size and the remaining string is sent in the next retrieval of the Buffer. \r\n\r\n**Below is the output at Netty client:**\r\n\r\n> Incoming From Exchange >> 8=FIX.4.29=24735=834=5449=orderSimulator52=20170809-07:38:30.24056=orderGateway1=orderGateway6=1250511=12695039514=017=505320=131=1250532=137=838=139=440=241=11616711044=1250554=155=GC60=20170809-07:38:30.240150=4151=0200=201708207=CME10=255\r\n> \r\n> Incoming From Exchange >> 8=FIX.4.29=24735=834=5549=orderSimulator52=20170809-07:38:30.26056=orderGateway1=orderGateway6=1250511=10926793114=017=505420=131=1250532=137=538=139=440=241=18595658744=1250554=155=GC60=20170809-07:38:30.260150=4151=0200=201708207=CME10=030\r\n> \r\n> Incoming From Exchange >> 8=FIX.4.29=24735=834=5649=orderSimulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=10479037514=017=505520=131=1250532=137=238=139=440=241=16819718944=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=0278=FIX.4.29=24735\r\n> \r\n> Incoming From Exchange >> =834=5749=orderSimulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=17021962114=017=505620=131=1250532=137=738=139=440=241=15642709944=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=0208=FIX.4.29=24735=834=5849=orderS\r\n> \r\n> Incoming From Exchange >> imulator52=20170809-07:38:30.26256=orderGateway1=orderGateway6=1250511=14044819714=017=505720=131=1250532=137=438=139=440=241=16279265744=1250554=155=GC60=20170809-07:38:30.262150=4151=0200=201708207=CME10=030\r\n> \r\n> Incoming From Exchange >> 8=FIX.4.29=24835=834=5949=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=14108324114=017=505820=131=1250532=137=1138=139=440=241=15954718144=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=073\r\n> \r\n> Incoming From Exchange >> 8=FIX.4.29=24835=834=6049=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=12836588814=017=505920=131=1250532=137=1238=139=440=241=14005997644=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=092\r\n> \r\n> Incoming From Exchange >> 8=FIX.4.29=24635=834=6149=orderSimulator52=20170809-07:38:30.27756=orderGateway1=orderGateway6=1250511=9376296914=017=506020=131=1250532=137=638=139=440=241=11452227544=1250554=155=GC60=20170809-07:38:30.277150=4151=0200=201708207=CME10=236\r\n> \r\n\r\nYou can clearly see that first 2 responses from the server are in correct format that is starting with \"8=\" and ended exactly with \"10=\". But in the third response, next string got appended to the current string and is cut. In the fourth response I am getting the remaining part of the string. How can I eliminate this? The strings coming from server can be of any size. \r\n\r\n**Below is my client code:**\r\n```\r\npublic  class SendStringToExchangeClient {\r\n    public static int port;\r\n    public static int reconnectTime = 1;\r\n    public int MAX_RECONNECTIONS = GetPropertiesFromFile.MAX_RECONNECTIONS;\r\n    Channel channel;\r\n    EventLoopGroup workGroup = new NioEventLoopGroup();\r\n    static ChannelFuture channelFuture = null;\r\n    String message;\r\n    int rcvBuf, sndBuf, lowWaterMark, highWaterMark;\r\n    public SendStringToExchangeClient(int port) throws Exception{\r\n        SendStringToExchangeClient.port = port;\r\n        rcvBuf = 2048;\r\n        sndBuf =   2048;\r\n        lowWaterMark = 1024;\r\n        highWaterMark = 2048;\r\n      //  channelFuture = connectLoop();\r\n    }\r\n\r\n    public final void connectLoop() throws InterruptedException {\r\n    try{\r\n            Bootstrap b = new Bootstrap();\r\n            b.group(workGroup);\r\n            b.channel(NioSocketChannel.class);\r\n            b.option(ChannelOption.SO_KEEPALIVE, true)\r\n             .option(ChannelOption.SO_RCVBUF, rcvBuf * 2048)\r\n             .option(ChannelOption.SO_SNDBUF, sndBuf * 2048)\r\n             .option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(lowWaterMark * 2048, highWaterMark * 2048))\r\n             .option(ChannelOption.TCP_NODELAY, true)\r\n            .handler(new ChannelInitializer<SocketChannel>() {\r\n                @Override\r\n                protected void initChannel(SocketChannel socketChannel) throws Exception {\r\n                    socketChannel.pipeline().addLast(new SendStringToExchangeClientHandler());\r\n                }\r\n            });\r\n\r\n            channelFuture = b.connect(GetPropertiesFromFile.SendToExchageIP.trim(), port).sync();\r\n            this.channel = channelFuture.channel();\r\n\r\n        } \r\n            catch(Exception ex){\r\n                System.err.println(\"ERROR : Exchange Not In Connection\");\r\n                Logger.error(\"Exchange not in connection\"+ex.getMessage());\r\n            }\r\n         //   return channelFuture;\r\n        }\r\n\r\n\r\n    public void shutdown(){\r\n        workGroup.shutdownGracefully();\r\n    }\r\n\r\n}\r\n\r\n\r\n**My client Handler code:**\r\n\r\npublic class SendStringToExchangeClientHandler extends ChannelInboundHandlerAdapter {\r\n    static int count =0;\r\n    @Override\r\n    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {\r\n        super.channelReadComplete(ctx);\r\n    }\r\n\r\n    @Override\r\n    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {\r\n\r\n        ByteBuf byteBuf = (ByteBuf) msg;\r\n        //receive from EXCHANGE\r\n        String message = byteBuf.toString(Charset.defaultCharset());\r\n        System.out.println(\"Incoming From Exchange >> \"+message);\r\n    }\r\n\r\n    @Override\r\n    public void channelActive(ChannelHandlerContext ctx) throws Exception {\r\n        super.channelActive(ctx);\r\n    }\r\n\r\n}\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["321218708"], "labels":[]}