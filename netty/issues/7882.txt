{"id":"7882", "title":"AddressResolver.resolveAll doesn't return consistent result", "body":"### Expected behavior
AddressResolver.resolveAll always returns full list of ips.

### Actual behavior
AddressResolver.resolveAll returns some part of full list ips.

### Steps to reproduce
`dev.myredis.com` host has two ips: 127.0.0.2, 127.0.0.3.
I guess AddressResolver.resolveAll should always return these two ips, but it's not true on practice if DnsAddressResolverGroup object shared between multiple threads

If this object created per invocation then the problem is gone.
Seems like DnsAddressResolverGroup is not thread safe!

### Minimal yet complete reproducer code (or URL to code)
Here is my test case:
```java
        NioEventLoopGroup niogroup = new NioEventLoopGroup();
        
        DnsAddressResolverGroup group = new DnsAddressResolverGroup(NioDatagramChannel.class, DnsServerAddressStreamProviders.platformDefault());
        ExecutorService es = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 100; i++) {
            es.execute(new Runnable() {
                @Override
                public void run() {
                    AddressResolver<InetSocketAddress> resolver = group.getResolver(niogroup.next());
                    try {
                        URI uri = new URI(\"redis://dev.myredis.com:6379\");
                        Future<List<InetSocketAddress>> allNodes = resolver.resolveAll(InetSocketAddress.createUnresolved(uri.getHost(), uri.getPort()));
                        List<InetSocketAddress> list = allNodes.syncUninterruptibly().getNow();
                        System.out.println(list);
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
        }

        Thread.sleep(10000);
        group.close();
        es.shutdown();
        niogroup.shutdownGracefully();
```

Output is:

[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.2:6379]
[dev.myredis.com/127.0.0.3:6379]
[dev.myredis.com/127.0.0.3:6379]

But it always should be: [dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]

### Netty version
4.1.23.Final

### JVM version (e.g. `java -version`)
java version \"1.8.0_102\"
Java(TM) SE Runtime Environment (build 1.8.0_102-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.102-b14, mixed mode)

### OS version (e.g. `uname -a`)
Windows 7", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7882","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7882/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7882/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7882/events","html_url":"https://github.com/netty/netty/issues/7882","id":316469273,"node_id":"MDU6SXNzdWUzMTY0NjkyNzM=","number":7882,"title":"AddressResolver.resolveAll doesn't return consistent result","user":{"login":"mrniko","id":1104661,"node_id":"MDQ6VXNlcjExMDQ2NjE=","avatar_url":"https://avatars0.githubusercontent.com/u/1104661?v=4","gravatar_id":"","url":"https://api.github.com/users/mrniko","html_url":"https://github.com/mrniko","followers_url":"https://api.github.com/users/mrniko/followers","following_url":"https://api.github.com/users/mrniko/following{/other_user}","gists_url":"https://api.github.com/users/mrniko/gists{/gist_id}","starred_url":"https://api.github.com/users/mrniko/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mrniko/subscriptions","organizations_url":"https://api.github.com/users/mrniko/orgs","repos_url":"https://api.github.com/users/mrniko/repos","events_url":"https://api.github.com/users/mrniko/events{/privacy}","received_events_url":"https://api.github.com/users/mrniko/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/187","html_url":"https://github.com/netty/netty/milestone/187","labels_url":"https://api.github.com/repos/netty/netty/milestones/187/labels","id":3280974,"node_id":"MDk6TWlsZXN0b25lMzI4MDk3NA==","number":187,"title":"4.1.25.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":43,"state":"closed","created_at":"2018-04-19T13:10:45Z","updated_at":"2018-05-24T13:16:21Z","due_on":null,"closed_at":"2018-05-14T12:11:58Z"},"comments":20,"created_at":"2018-04-21T07:15:32Z","updated_at":"2018-05-04T13:39:42Z","closed_at":"2018-05-04T12:19:26Z","author_association":"NONE","body":"### Expected behavior\r\nAddressResolver.resolveAll always returns full list of ips.\r\n\r\n### Actual behavior\r\nAddressResolver.resolveAll returns some part of full list ips.\r\n\r\n### Steps to reproduce\r\n`dev.myredis.com` host has two ips: 127.0.0.2, 127.0.0.3.\r\nI guess AddressResolver.resolveAll should always return these two ips, but it's not true on practice if DnsAddressResolverGroup object shared between multiple threads\r\n\r\nIf this object created per invocation then the problem is gone.\r\nSeems like DnsAddressResolverGroup is not thread safe!\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\nHere is my test case:\r\n```java\r\n        NioEventLoopGroup niogroup = new NioEventLoopGroup();\r\n        \r\n        DnsAddressResolverGroup group = new DnsAddressResolverGroup(NioDatagramChannel.class, DnsServerAddressStreamProviders.platformDefault());\r\n        ExecutorService es = Executors.newFixedThreadPool(2);\r\n        for (int i = 0; i < 100; i++) {\r\n            es.execute(new Runnable() {\r\n                @Override\r\n                public void run() {\r\n                    AddressResolver<InetSocketAddress> resolver = group.getResolver(niogroup.next());\r\n                    try {\r\n                        URI uri = new URI(\"redis://dev.myredis.com:6379\");\r\n                        Future<List<InetSocketAddress>> allNodes = resolver.resolveAll(InetSocketAddress.createUnresolved(uri.getHost(), uri.getPort()));\r\n                        List<InetSocketAddress> list = allNodes.syncUninterruptibly().getNow();\r\n                        System.out.println(list);\r\n                    } catch (URISyntaxException e) {\r\n                        // TODO Auto-generated catch block\r\n                        e.printStackTrace();\r\n                    }\r\n                }\r\n            });\r\n        }\r\n\r\n        Thread.sleep(10000);\r\n        group.close();\r\n        es.shutdown();\r\n        niogroup.shutdownGracefully();\r\n```\r\n\r\nOutput is:\r\n\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379, dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.2:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n[dev.myredis.com/127.0.0.3:6379]\r\n\r\nBut it always should be: [dev.myredis.com/127.0.0.2:6379, dev.myredis.com/127.0.0.3:6379]\r\n\r\n### Netty version\r\n4.1.23.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\njava version \"1.8.0_102\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_102-b14)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.102-b14, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nWindows 7","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["383824236","383826922","383829043","383837019","384004058","384259744","386303737","386424804","386524373","386593149","386593851","386594403","386594669","386594873","386596265","386598650","386599329","386599476","386599768","386604375"], "labels":[]}