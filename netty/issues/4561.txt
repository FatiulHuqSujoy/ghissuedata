{"id":"4561", "title":"Question: ReferenceCounted.retain(void/int) have nearly identical impls", "body":"Hello,

I assume there is some history behind this code.  At first blush, a developer / reader says: \"Hey, that's duplicated code.\"  Can you explain why `ReferenceCounted.retain(void)` and `retain(int)` have nearly identical impls in `AbstractReferenceCounted` and `AbstractReferenceCountedByteBuf`?

(There is similar overlap for `ReferenceCounted.release(void)` and `release(int)`.)

Sample code from 4.0.33.Final / `AbstractReferenceCountedByteBuf`:

``` java
    @Override
    public ByteBuf retain() {
        for (;;) {
            int refCnt = this.refCnt;
            if (refCnt == 0) {
                throw new IllegalReferenceCountException(0, 1);
            }
            if (refCnt == Integer.MAX_VALUE) {
                throw new IllegalReferenceCountException(Integer.MAX_VALUE, 1);
            }
            if (refCntUpdater.compareAndSet(this, refCnt, refCnt + 1)) {
                break;
            }
        }
        return this;
    }

    @Override
    public ByteBuf retain(int increment) {
        if (increment <= 0) {
            throw new IllegalArgumentException(\"increment: \" + increment + \" (expected: > 0)\");
        }

        for (;;) {
            int refCnt = this.refCnt;
            if (refCnt == 0) {
                throw new IllegalReferenceCountException(0, increment);
            }
            if (refCnt > Integer.MAX_VALUE - increment) {
                throw new IllegalReferenceCountException(refCnt, increment);
            }
            if (refCntUpdater.compareAndSet(this, refCnt, refCnt + increment)) {
                break;
            }
        }
        return this;
    }
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4561","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4561/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4561/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4561/events","html_url":"https://github.com/netty/netty/issues/4561","id":121653825,"node_id":"MDU6SXNzdWUxMjE2NTM4MjU=","number":4561,"title":"Question: ReferenceCounted.retain(void/int) have nearly identical impls","user":{"login":"kevinarpe","id":4367212,"node_id":"MDQ6VXNlcjQzNjcyMTI=","avatar_url":"https://avatars1.githubusercontent.com/u/4367212?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinarpe","html_url":"https://github.com/kevinarpe","followers_url":"https://api.github.com/users/kevinarpe/followers","following_url":"https://api.github.com/users/kevinarpe/following{/other_user}","gists_url":"https://api.github.com/users/kevinarpe/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinarpe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinarpe/subscriptions","organizations_url":"https://api.github.com/users/kevinarpe/orgs","repos_url":"https://api.github.com/users/kevinarpe/repos","events_url":"https://api.github.com/users/kevinarpe/events{/privacy}","received_events_url":"https://api.github.com/users/kevinarpe/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-12-11T07:23:43Z","updated_at":"2015-12-14T12:34:39Z","closed_at":"2015-12-12T17:27:33Z","author_association":"NONE","body":"Hello,\n\nI assume there is some history behind this code.  At first blush, a developer / reader says: \"Hey, that's duplicated code.\"  Can you explain why `ReferenceCounted.retain(void)` and `retain(int)` have nearly identical impls in `AbstractReferenceCounted` and `AbstractReferenceCountedByteBuf`?\n\n(There is similar overlap for `ReferenceCounted.release(void)` and `release(int)`.)\n\nSample code from 4.0.33.Final / `AbstractReferenceCountedByteBuf`:\n\n``` java\n    @Override\n    public ByteBuf retain() {\n        for (;;) {\n            int refCnt = this.refCnt;\n            if (refCnt == 0) {\n                throw new IllegalReferenceCountException(0, 1);\n            }\n            if (refCnt == Integer.MAX_VALUE) {\n                throw new IllegalReferenceCountException(Integer.MAX_VALUE, 1);\n            }\n            if (refCntUpdater.compareAndSet(this, refCnt, refCnt + 1)) {\n                break;\n            }\n        }\n        return this;\n    }\n\n    @Override\n    public ByteBuf retain(int increment) {\n        if (increment <= 0) {\n            throw new IllegalArgumentException(\"increment: \" + increment + \" (expected: > 0)\");\n        }\n\n        for (;;) {\n            int refCnt = this.refCnt;\n            if (refCnt == 0) {\n                throw new IllegalReferenceCountException(0, increment);\n            }\n            if (refCnt > Integer.MAX_VALUE - increment) {\n                throw new IllegalReferenceCountException(refCnt, increment);\n            }\n            if (refCntUpdater.compareAndSet(this, refCnt, refCnt + increment)) {\n                break;\n            }\n        }\n        return this;\n    }\n```\n","closed_by":{"login":"kevinarpe","id":4367212,"node_id":"MDQ6VXNlcjQzNjcyMTI=","avatar_url":"https://avatars1.githubusercontent.com/u/4367212?v=4","gravatar_id":"","url":"https://api.github.com/users/kevinarpe","html_url":"https://github.com/kevinarpe","followers_url":"https://api.github.com/users/kevinarpe/followers","following_url":"https://api.github.com/users/kevinarpe/following{/other_user}","gists_url":"https://api.github.com/users/kevinarpe/gists{/gist_id}","starred_url":"https://api.github.com/users/kevinarpe/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kevinarpe/subscriptions","organizations_url":"https://api.github.com/users/kevinarpe/orgs","repos_url":"https://api.github.com/users/kevinarpe/repos","events_url":"https://api.github.com/users/kevinarpe/events{/privacy}","received_events_url":"https://api.github.com/users/kevinarpe/received_events","type":"User","site_admin":false}}", "commentIds":["163882641","164170164","164426043"], "labels":[]}