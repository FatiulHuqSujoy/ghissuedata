{"id":"5990", "title":"build fails with and without tests (-DskipTests)", "body":"Hi,
I cloned netty sources from git repo and attempted building it.

But build fails both with tests (mvn clean install) and without tests (mvn clean install -DskipTests):

[INFO] Reactor Summary:
[INFO] 
[INFO] Netty .............................................. SUCCESS [  8.208 s]
[INFO] Netty/Common ....................................... SUCCESS [ 24.477 s]
[INFO] Netty/Buffer ....................................... SUCCESS [ 10.972 s]
[INFO] Netty/Resolver ..................................... SUCCESS [  3.388 s]
[INFO] Netty/Transport .................................... SUCCESS [ 11.660 s]
[INFO] Netty/Codec ........................................ SUCCESS [ 10.157 s]
[INFO] Netty/Codec/DNS .................................... SUCCESS [  4.670 s]
[INFO] Netty/Codec/HAProxy ................................ SUCCESS [  3.823 s]
[INFO] Netty/Handler ...................................... SUCCESS [  9.985 s]
[INFO] Netty/Codec/HTTP ................................... SUCCESS [ 13.872 s]
[INFO] Netty/Codec/HTTP2 .................................. SUCCESS [ 11.299 s]
[INFO] Netty/Codec/Memcache ............................... SUCCESS [  4.509 s]
[INFO] Netty/Codec/MQTT ................................... SUCCESS [  3.705 s]
[INFO] Netty/Codec/Redis .................................. SUCCESS [  3.857 s]
[INFO] Netty/Codec/SMTP ................................... SUCCESS [  3.506 s]
[INFO] Netty/Codec/Socks .................................. SUCCESS [  4.627 s]
[INFO] Netty/Codec/Stomp .................................. SUCCESS [  3.747 s]
[INFO] Netty/Codec/XML .................................... SUCCESS [  3.722 s]
[INFO] Netty/Resolver/DNS ................................. SUCCESS [  4.684 s]
[INFO] Netty/Transport/RXTX ............................... SUCCESS [  2.449 s]
[INFO] Netty/Transport/SCTP ............................... SUCCESS [  3.393 s]
[INFO] Netty/Transport/UDT ................................ SUCCESS [  4.118 s]
[INFO] Netty/Handler/Proxy ................................ SUCCESS [  4.080 s]
[INFO] Netty/Example ...................................... SUCCESS [  7.905 s]
[INFO] Netty/Testsuite .................................... SUCCESS [  5.584 s]
[INFO] Netty/Testsuite/OSGI ............................... SUCCESS [ 12.819 s]
[INFO] Netty/Transport/Native/Epoll ....................... FAILURE [  9.657 s]
[INFO] Netty/Microbench ................................... SKIPPED
[INFO] Netty/All-in-One ................................... SKIPPED
[INFO] Netty/Tarball ...................................... SKIPPED
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 03:16 min
[INFO] Finished at: 2016-11-08T14:15:18+01:00

Cheers,
GM

P.S. why do you close issues (e.g. 5979)  without resolving them???
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/5990","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/5990/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/5990/comments","events_url":"https://api.github.com/repos/netty/netty/issues/5990/events","html_url":"https://github.com/netty/netty/issues/5990","id":187990888,"node_id":"MDU6SXNzdWUxODc5OTA4ODg=","number":5990,"title":"build fails with and without tests (-DskipTests)","user":{"login":"GeorgeMatveev","id":16880261,"node_id":"MDQ6VXNlcjE2ODgwMjYx","avatar_url":"https://avatars0.githubusercontent.com/u/16880261?v=4","gravatar_id":"","url":"https://api.github.com/users/GeorgeMatveev","html_url":"https://github.com/GeorgeMatveev","followers_url":"https://api.github.com/users/GeorgeMatveev/followers","following_url":"https://api.github.com/users/GeorgeMatveev/following{/other_user}","gists_url":"https://api.github.com/users/GeorgeMatveev/gists{/gist_id}","starred_url":"https://api.github.com/users/GeorgeMatveev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/GeorgeMatveev/subscriptions","organizations_url":"https://api.github.com/users/GeorgeMatveev/orgs","repos_url":"https://api.github.com/users/GeorgeMatveev/repos","events_url":"https://api.github.com/users/GeorgeMatveev/events{/privacy}","received_events_url":"https://api.github.com/users/GeorgeMatveev/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-11-08T13:25:21Z","updated_at":"2016-11-08T13:28:15Z","closed_at":"2016-11-08T13:28:15Z","author_association":"NONE","body":"Hi,\r\nI cloned netty sources from git repo and attempted building it.\r\n\r\nBut build fails both with tests (mvn clean install) and without tests (mvn clean install -DskipTests):\r\n\r\n[INFO] Reactor Summary:\r\n[INFO] \r\n[INFO] Netty .............................................. SUCCESS [  8.208 s]\r\n[INFO] Netty/Common ....................................... SUCCESS [ 24.477 s]\r\n[INFO] Netty/Buffer ....................................... SUCCESS [ 10.972 s]\r\n[INFO] Netty/Resolver ..................................... SUCCESS [  3.388 s]\r\n[INFO] Netty/Transport .................................... SUCCESS [ 11.660 s]\r\n[INFO] Netty/Codec ........................................ SUCCESS [ 10.157 s]\r\n[INFO] Netty/Codec/DNS .................................... SUCCESS [  4.670 s]\r\n[INFO] Netty/Codec/HAProxy ................................ SUCCESS [  3.823 s]\r\n[INFO] Netty/Handler ...................................... SUCCESS [  9.985 s]\r\n[INFO] Netty/Codec/HTTP ................................... SUCCESS [ 13.872 s]\r\n[INFO] Netty/Codec/HTTP2 .................................. SUCCESS [ 11.299 s]\r\n[INFO] Netty/Codec/Memcache ............................... SUCCESS [  4.509 s]\r\n[INFO] Netty/Codec/MQTT ................................... SUCCESS [  3.705 s]\r\n[INFO] Netty/Codec/Redis .................................. SUCCESS [  3.857 s]\r\n[INFO] Netty/Codec/SMTP ................................... SUCCESS [  3.506 s]\r\n[INFO] Netty/Codec/Socks .................................. SUCCESS [  4.627 s]\r\n[INFO] Netty/Codec/Stomp .................................. SUCCESS [  3.747 s]\r\n[INFO] Netty/Codec/XML .................................... SUCCESS [  3.722 s]\r\n[INFO] Netty/Resolver/DNS ................................. SUCCESS [  4.684 s]\r\n[INFO] Netty/Transport/RXTX ............................... SUCCESS [  2.449 s]\r\n[INFO] Netty/Transport/SCTP ............................... SUCCESS [  3.393 s]\r\n[INFO] Netty/Transport/UDT ................................ SUCCESS [  4.118 s]\r\n[INFO] Netty/Handler/Proxy ................................ SUCCESS [  4.080 s]\r\n[INFO] Netty/Example ...................................... SUCCESS [  7.905 s]\r\n[INFO] Netty/Testsuite .................................... SUCCESS [  5.584 s]\r\n[INFO] Netty/Testsuite/OSGI ............................... SUCCESS [ 12.819 s]\r\n[INFO] Netty/Transport/Native/Epoll ....................... FAILURE [  9.657 s]\r\n[INFO] Netty/Microbench ................................... SKIPPED\r\n[INFO] Netty/All-in-One ................................... SKIPPED\r\n[INFO] Netty/Tarball ...................................... SKIPPED\r\n[INFO] ------------------------------------------------------------------------\r\n[INFO] BUILD FAILURE\r\n[INFO] ------------------------------------------------------------------------\r\n[INFO] Total time: 03:16 min\r\n[INFO] Finished at: 2016-11-08T14:15:18+01:00\r\n\r\nCheers,\r\nGM\r\n\r\nP.S. why do you close issues (e.g. 5979)  without resolving them???\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["259135774","259135866"], "labels":[]}