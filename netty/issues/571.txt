{"id":"571", "title":"[3.5.5] file download with HttpSnoopClient on jdk1.7", "body":"Hi,

I hit a problem with using jdk1.7 in Windows 7 and running the HttpSnoopClient example code.

I've made a copy of the [snoop client example](https://github.com/netty/netty/tree/3/src/main/java/org/jboss/netty/example/http/snoop) and the [file example](https://github.com/netty/netty/tree/3/src/main/java/org/jboss/netty/example/http/file)

After starting the HttpStaticFileServer, I can download the files with my browers.

When running the HttpSnoopClient with jdk1.7 

java -cp netty-3.5.5.Final.jar;target\\classes org.jboss.netty.example.http.snoop.HttpSnoopClient http://localhost:8080/netty-3.5.5.Final.jar

the client hangs after downloading 8Kb of the file. The HttpStaticFileServer does not emit any output on stdout. Files smaller then 8Kb are retrived correctly. No CPU is used after 8Kb is retrived.

The jdk1.7 reports

java version \"1.7.0_06\"
Java(TM) SE Runtime Environment (build 1.7.0_06-b24)
Java HotSpot(TM) 64-Bit Server VM (build 23.2-b09, mixed mode)

When switching to jdk1.6:

java version \"1.6.0_31\"
Java(TM) SE Runtime Environment (build 1.6.0_31-b05)
Java HotSpot(TM) 64-Bit Server VM (build 20.6-b01, mixed mode)

then HttpSnoopClient  works without any problem.

Any ideas?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/571","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/571/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/571/comments","events_url":"https://api.github.com/repos/netty/netty/issues/571/events","html_url":"https://github.com/netty/netty/issues/571","id":6498307,"node_id":"MDU6SXNzdWU2NDk4MzA3","number":571,"title":"[3.5.5] file download with HttpSnoopClient on jdk1.7","user":{"login":"bckfnn","id":1541887,"node_id":"MDQ6VXNlcjE1NDE4ODc=","avatar_url":"https://avatars3.githubusercontent.com/u/1541887?v=4","gravatar_id":"","url":"https://api.github.com/users/bckfnn","html_url":"https://github.com/bckfnn","followers_url":"https://api.github.com/users/bckfnn/followers","following_url":"https://api.github.com/users/bckfnn/following{/other_user}","gists_url":"https://api.github.com/users/bckfnn/gists{/gist_id}","starred_url":"https://api.github.com/users/bckfnn/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bckfnn/subscriptions","organizations_url":"https://api.github.com/users/bckfnn/orgs","repos_url":"https://api.github.com/users/bckfnn/repos","events_url":"https://api.github.com/users/bckfnn/events{/privacy}","received_events_url":"https://api.github.com/users/bckfnn/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2012-08-28T13:15:39Z","updated_at":"2012-08-29T14:47:19Z","closed_at":"2012-08-29T14:47:18Z","author_association":"NONE","body":"Hi,\n\nI hit a problem with using jdk1.7 in Windows 7 and running the HttpSnoopClient example code.\n\nI've made a copy of the [snoop client example](https://github.com/netty/netty/tree/3/src/main/java/org/jboss/netty/example/http/snoop) and the [file example](https://github.com/netty/netty/tree/3/src/main/java/org/jboss/netty/example/http/file)\n\nAfter starting the HttpStaticFileServer, I can download the files with my browers.\n\nWhen running the HttpSnoopClient with jdk1.7 \n\njava -cp netty-3.5.5.Final.jar;target\\classes org.jboss.netty.example.http.snoop.HttpSnoopClient http://localhost:8080/netty-3.5.5.Final.jar\n\nthe client hangs after downloading 8Kb of the file. The HttpStaticFileServer does not emit any output on stdout. Files smaller then 8Kb are retrived correctly. No CPU is used after 8Kb is retrived.\n\nThe jdk1.7 reports\n\njava version \"1.7.0_06\"\nJava(TM) SE Runtime Environment (build 1.7.0_06-b24)\nJava HotSpot(TM) 64-Bit Server VM (build 23.2-b09, mixed mode)\n\nWhen switching to jdk1.6:\n\njava version \"1.6.0_31\"\nJava(TM) SE Runtime Environment (build 1.6.0_31-b05)\nJava HotSpot(TM) 64-Bit Server VM (build 20.6-b01, mixed mode)\n\nthen HttpSnoopClient  works without any problem.\n\nAny ideas?\n","closed_by":{"login":"bckfnn","id":1541887,"node_id":"MDQ6VXNlcjE1NDE4ODc=","avatar_url":"https://avatars3.githubusercontent.com/u/1541887?v=4","gravatar_id":"","url":"https://api.github.com/users/bckfnn","html_url":"https://github.com/bckfnn","followers_url":"https://api.github.com/users/bckfnn/followers","following_url":"https://api.github.com/users/bckfnn/following{/other_user}","gists_url":"https://api.github.com/users/bckfnn/gists{/gist_id}","starred_url":"https://api.github.com/users/bckfnn/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bckfnn/subscriptions","organizations_url":"https://api.github.com/users/bckfnn/orgs","repos_url":"https://api.github.com/users/bckfnn/repos","events_url":"https://api.github.com/users/bckfnn/events{/privacy}","received_events_url":"https://api.github.com/users/bckfnn/received_events","type":"User","site_admin":false}}", "commentIds":["8092973","8099120","8106087","8127728"], "labels":[]}