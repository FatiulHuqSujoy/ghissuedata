{"id":"2158", "title":"BytBuf leak in HttpObjectAggregator", "body":"I'm using netty 4.0.14 and my pipeline is constructed this way:

``` java
    class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            ChannelPipeline pipeline = ch.pipeline();

            pipeline.addLast(\"codec\", new HttpServerCodec());
            pipeline.addLast(\"aggregator\", new HttpObjectAggregator(165536));

            pipeline.addLast(\"handler\", new RequestHandler());
        }
    }
```

And when I start my server at one of the first requests I see in my logs

```
ERROR io.netty.util.ResourceLeakDetector - LEAK: ByteBuf.release() was not called before it's garbage-collected.
Recent access records: 0
Created at:
        io.netty.util.ResourceLeakDetector.open(ResourceLeakDetector.java:190)
        io.netty.buffer.CompositeByteBuf.<init>(CompositeByteBuf.java:59)
        io.netty.buffer.Unpooled.compositeBuffer(Unpooled.java:355)
        io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:145)
        io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:50)
        io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:89)
        io.netty.channel.DefaultChannelHandlerContext.invokeChannelRead(DefaultChannelHandlerContext.java:338)
        io.netty.channel.DefaultChannelHandlerContext.fireChannelRead(DefaultChannelHandlerContext.java:324)
        io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:153)
        io.netty.channel.CombinedChannelDuplexHandler.channelRead(CombinedChannelDuplexHandler.java:148)
        io.netty.channel.DefaultChannelHandlerContext.invokeChannelRead(DefaultChannelHandlerContext.java:338)
        io.netty.channel.DefaultChannelHandlerContext.fireChannelRead(DefaultChannelHandlerContext.java:324)
        io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:785)
        io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:126)
        io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:485)
        io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized(NioEventLoop.java:452)
        io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:346)
        io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)
        java.lang.Thread.run(Thread.java:724)
```

Am I doing something wrong or is this a leak in HttpObjectAggregator?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2158","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2158/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2158/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2158/events","html_url":"https://github.com/netty/netty/issues/2158","id":26362949,"node_id":"MDU6SXNzdWUyNjM2Mjk0OQ==","number":2158,"title":"BytBuf leak in HttpObjectAggregator","user":{"login":"daniilguit","id":231284,"node_id":"MDQ6VXNlcjIzMTI4NA==","avatar_url":"https://avatars1.githubusercontent.com/u/231284?v=4","gravatar_id":"","url":"https://api.github.com/users/daniilguit","html_url":"https://github.com/daniilguit","followers_url":"https://api.github.com/users/daniilguit/followers","following_url":"https://api.github.com/users/daniilguit/following{/other_user}","gists_url":"https://api.github.com/users/daniilguit/gists{/gist_id}","starred_url":"https://api.github.com/users/daniilguit/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/daniilguit/subscriptions","organizations_url":"https://api.github.com/users/daniilguit/orgs","repos_url":"https://api.github.com/users/daniilguit/repos","events_url":"https://api.github.com/users/daniilguit/events{/privacy}","received_events_url":"https://api.github.com/users/daniilguit/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2014-01-27T16:06:06Z","updated_at":"2014-01-28T05:59:11Z","closed_at":"2014-01-28T05:58:58Z","author_association":"NONE","body":"I'm using netty 4.0.14 and my pipeline is constructed this way:\n\n``` java\n    class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {\n        @Override\n        protected void initChannel(SocketChannel ch) throws Exception {\n            ChannelPipeline pipeline = ch.pipeline();\n\n            pipeline.addLast(\"codec\", new HttpServerCodec());\n            pipeline.addLast(\"aggregator\", new HttpObjectAggregator(165536));\n\n            pipeline.addLast(\"handler\", new RequestHandler());\n        }\n    }\n```\n\nAnd when I start my server at one of the first requests I see in my logs\n\n```\nERROR io.netty.util.ResourceLeakDetector - LEAK: ByteBuf.release() was not called before it's garbage-collected.\nRecent access records: 0\nCreated at:\n        io.netty.util.ResourceLeakDetector.open(ResourceLeakDetector.java:190)\n        io.netty.buffer.CompositeByteBuf.<init>(CompositeByteBuf.java:59)\n        io.netty.buffer.Unpooled.compositeBuffer(Unpooled.java:355)\n        io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:145)\n        io.netty.handler.codec.http.HttpObjectAggregator.decode(HttpObjectAggregator.java:50)\n        io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:89)\n        io.netty.channel.DefaultChannelHandlerContext.invokeChannelRead(DefaultChannelHandlerContext.java:338)\n        io.netty.channel.DefaultChannelHandlerContext.fireChannelRead(DefaultChannelHandlerContext.java:324)\n        io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:153)\n        io.netty.channel.CombinedChannelDuplexHandler.channelRead(CombinedChannelDuplexHandler.java:148)\n        io.netty.channel.DefaultChannelHandlerContext.invokeChannelRead(DefaultChannelHandlerContext.java:338)\n        io.netty.channel.DefaultChannelHandlerContext.fireChannelRead(DefaultChannelHandlerContext.java:324)\n        io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:785)\n        io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:126)\n        io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:485)\n        io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized(NioEventLoop.java:452)\n        io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:346)\n        io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)\n        java.lang.Thread.run(Thread.java:724)\n```\n\nAm I doing something wrong or is this a leak in HttpObjectAggregator?\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["33382296","33397352","33399815","33453330"], "labels":[]}