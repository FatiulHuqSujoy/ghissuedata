{"id":"667", "title":"Safari/Opera + HaProxy + netty(3/4) + websockets = connection hangs", "body":"I'm migrating my game server from Jetty to Netty. Reverse proxy is required, cause i want to have nginx and game server on the same port.

Websockets 76 handshake request/response consist of two parts: headers and binary data.
HaProxy detects binary data as the next http request!

How jetty handles it: 
**take request, send response, take hixie 8 bytes, send hixie 16 bytes**

In most cases, browsers send 8 bytes before the response happens. HaProxy with http mode sends 8 bytes AFTER backend sends response.

How netty handles:
**if there is \"Upgrade:websockets\" in header then content-length = 8.
take request headers, wait for 8 bytes until timeout**

Same problem as in http://lists.jboss.org/pipermail/netty-users/2011-June/004630.html

That's my test **/etc/haproxy/haproxy.cfg** :

```
global
    maxconn 30000
    nbproc 2

defaults
    mode http

frontend all 0.0.0.0:80
    timeout client 20s
    default_backend www_backend

backend www_backend
    balance roundrobin
    timeout server 15s
    timeout connect 3s
    server server1 10.0.1.20:8081 weight 1 maxconn 30000 check
```

Request:

```
Connection:Upgrade
Host:10.0.1.20:80
Origin:http://localhost:8080
Sec-WebSocket-Key1:134  8|4 8 RM E* Z29* 4 4
Sec-WebSocket-Key2:2   9 ,  14 826  ]4 ~ 4 8
Upgrade:WebSocket
(Key3):87:90:DC:28:9C:61:4F:BB
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/667","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/667/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/667/comments","events_url":"https://api.github.com/repos/netty/netty/issues/667/events","html_url":"https://github.com/netty/netty/issues/667","id":7743319,"node_id":"MDU6SXNzdWU3NzQzMzE5","number":667,"title":"Safari/Opera + HaProxy + netty(3/4) + websockets = connection hangs","user":{"login":"ivanpopelyshev","id":695831,"node_id":"MDQ6VXNlcjY5NTgzMQ==","avatar_url":"https://avatars2.githubusercontent.com/u/695831?v=4","gravatar_id":"","url":"https://api.github.com/users/ivanpopelyshev","html_url":"https://github.com/ivanpopelyshev","followers_url":"https://api.github.com/users/ivanpopelyshev/followers","following_url":"https://api.github.com/users/ivanpopelyshev/following{/other_user}","gists_url":"https://api.github.com/users/ivanpopelyshev/gists{/gist_id}","starred_url":"https://api.github.com/users/ivanpopelyshev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ivanpopelyshev/subscriptions","organizations_url":"https://api.github.com/users/ivanpopelyshev/orgs","repos_url":"https://api.github.com/users/ivanpopelyshev/repos","events_url":"https://api.github.com/users/ivanpopelyshev/events{/privacy}","received_events_url":"https://api.github.com/users/ivanpopelyshev/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":12,"created_at":"2012-10-20T23:39:28Z","updated_at":"2012-10-25T06:14:21Z","closed_at":"2012-10-25T06:14:21Z","author_association":"NONE","body":"I'm migrating my game server from Jetty to Netty. Reverse proxy is required, cause i want to have nginx and game server on the same port.\n\nWebsockets 76 handshake request/response consist of two parts: headers and binary data.\nHaProxy detects binary data as the next http request!\n\nHow jetty handles it: \n**take request, send response, take hixie 8 bytes, send hixie 16 bytes**\n\nIn most cases, browsers send 8 bytes before the response happens. HaProxy with http mode sends 8 bytes AFTER backend sends response.\n\nHow netty handles:\n**if there is \"Upgrade:websockets\" in header then content-length = 8.\ntake request headers, wait for 8 bytes until timeout**\n\nSame problem as in http://lists.jboss.org/pipermail/netty-users/2011-June/004630.html\n\nThat's my test **/etc/haproxy/haproxy.cfg** :\n\n```\nglobal\n    maxconn 30000\n    nbproc 2\n\ndefaults\n    mode http\n\nfrontend all 0.0.0.0:80\n    timeout client 20s\n    default_backend www_backend\n\nbackend www_backend\n    balance roundrobin\n    timeout server 15s\n    timeout connect 3s\n    server server1 10.0.1.20:8081 weight 1 maxconn 30000 check\n```\n\nRequest:\n\n```\nConnection:Upgrade\nHost:10.0.1.20:80\nOrigin:http://localhost:8080\nSec-WebSocket-Key1:134  8|4 8 RM E* Z29* 4 4\nSec-WebSocket-Key2:2   9 ,  14 826  ]4 ~ 4 8\nUpgrade:WebSocket\n(Key3):87:90:DC:28:9C:61:4F:BB\n```\n","closed_by":{"login":"ivanpopelyshev","id":695831,"node_id":"MDQ6VXNlcjY5NTgzMQ==","avatar_url":"https://avatars2.githubusercontent.com/u/695831?v=4","gravatar_id":"","url":"https://api.github.com/users/ivanpopelyshev","html_url":"https://github.com/ivanpopelyshev","followers_url":"https://api.github.com/users/ivanpopelyshev/followers","following_url":"https://api.github.com/users/ivanpopelyshev/following{/other_user}","gists_url":"https://api.github.com/users/ivanpopelyshev/gists{/gist_id}","starred_url":"https://api.github.com/users/ivanpopelyshev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ivanpopelyshev/subscriptions","organizations_url":"https://api.github.com/users/ivanpopelyshev/orgs","repos_url":"https://api.github.com/users/ivanpopelyshev/repos","events_url":"https://api.github.com/users/ivanpopelyshev/events{/privacy}","received_events_url":"https://api.github.com/users/ivanpopelyshev/received_events","type":"User","site_admin":false}}", "commentIds":["9638092","9640425","9640555","9640683","9641271","9644113","9644752","9645172","9648921","9649092","9759949","9767150"], "labels":[]}