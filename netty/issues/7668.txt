{"id":"7668", "title":"HttpProxyHandler should use/allow hostname in initial CONNECT request", "body":"### Expected behavior

The `HttpProxyHandler` should include the hostname of the requested site in it's initial `CONNECT` request.

The hostname of the requested site is usually required by some proxies to apply ACLs (e.g. Squid)

### Actual behavior

The `HttpProxyHandler` sends the resolved IP address of the requested host

### Steps to reproduce

Add `HttpProxyHandler` to the pipeline

### Minimal yet complete reproducer code (or URL to code)

When a `InetSocketAddress` is created as unresolved, [the function](https://github.com/netty/netty/blob/4.1/handler-proxy/src/main/java/io/netty/handler/proxy/HttpProxyHandler.java#L130) used by `HttpProxyHandler` to get the requested host will return the hostname
```java
InetSocketAddress testAddress = InetSocketAddress.createUnresolved(\"example.com\", 80);
System.out.println(testAddress);
System.out.println(NetUtil.toSocketAddressString(testAddress));
// Output:
//  example.com:80
//  example.com:80
```
But when the address is resolved (like it will usually be in a pipeline) the IP address of the host is returned

```java
InetSocketAddress testAddress = new InetSocketAddress(\"example.com\", 80);
System.out.println(testAddress);
System.out.println(NetUtil.toSocketAddressString(testAddress));
// Output:
//  example.com/93.184.216.34:80
//  93.184.216.34:80
```
### Netty version
`4.1.17.Final`
### JVM version (e.g. `java -version`)
```
java version \"1.8.0_162\"
Java(TM) SE Runtime Environment (build 1.8.0_162-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.162-b12, mixed mode)
```
### OS version (e.g. `uname -a`)
```
Darwin ##### 17.3.0 Darwin Kernel Version 17.3.0: Thu Nov  9 18:09:22 PST 2017; root:xnu-4570.31.3~1/RELEASE_X86_64 x86_64
```", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7668","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7668/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7668/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7668/events","html_url":"https://github.com/netty/netty/issues/7668","id":293361831,"node_id":"MDU6SXNzdWUyOTMzNjE4MzE=","number":7668,"title":"HttpProxyHandler should use/allow hostname in initial CONNECT request","user":{"login":"LiamHaworth","id":3233620,"node_id":"MDQ6VXNlcjMyMzM2MjA=","avatar_url":"https://avatars0.githubusercontent.com/u/3233620?v=4","gravatar_id":"","url":"https://api.github.com/users/LiamHaworth","html_url":"https://github.com/LiamHaworth","followers_url":"https://api.github.com/users/LiamHaworth/followers","following_url":"https://api.github.com/users/LiamHaworth/following{/other_user}","gists_url":"https://api.github.com/users/LiamHaworth/gists{/gist_id}","starred_url":"https://api.github.com/users/LiamHaworth/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/LiamHaworth/subscriptions","organizations_url":"https://api.github.com/users/LiamHaworth/orgs","repos_url":"https://api.github.com/users/LiamHaworth/repos","events_url":"https://api.github.com/users/LiamHaworth/events{/privacy}","received_events_url":"https://api.github.com/users/LiamHaworth/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-02-01T00:28:17Z","updated_at":"2018-02-01T05:12:09Z","closed_at":"2018-02-01T05:12:09Z","author_association":"NONE","body":"### Expected behavior\r\n\r\nThe `HttpProxyHandler` should include the hostname of the requested site in it's initial `CONNECT` request.\r\n\r\nThe hostname of the requested site is usually required by some proxies to apply ACLs (e.g. Squid)\r\n\r\n### Actual behavior\r\n\r\nThe `HttpProxyHandler` sends the resolved IP address of the requested host\r\n\r\n### Steps to reproduce\r\n\r\nAdd `HttpProxyHandler` to the pipeline\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\nWhen a `InetSocketAddress` is created as unresolved, [the function](https://github.com/netty/netty/blob/4.1/handler-proxy/src/main/java/io/netty/handler/proxy/HttpProxyHandler.java#L130) used by `HttpProxyHandler` to get the requested host will return the hostname\r\n```java\r\nInetSocketAddress testAddress = InetSocketAddress.createUnresolved(\"example.com\", 80);\r\nSystem.out.println(testAddress);\r\nSystem.out.println(NetUtil.toSocketAddressString(testAddress));\r\n// Output:\r\n//  example.com:80\r\n//  example.com:80\r\n```\r\nBut when the address is resolved (like it will usually be in a pipeline) the IP address of the host is returned\r\n\r\n```java\r\nInetSocketAddress testAddress = new InetSocketAddress(\"example.com\", 80);\r\nSystem.out.println(testAddress);\r\nSystem.out.println(NetUtil.toSocketAddressString(testAddress));\r\n// Output:\r\n//  example.com/93.184.216.34:80\r\n//  93.184.216.34:80\r\n```\r\n### Netty version\r\n`4.1.17.Final`\r\n### JVM version (e.g. `java -version`)\r\n```\r\njava version \"1.8.0_162\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_162-b12)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.162-b12, mixed mode)\r\n```\r\n### OS version (e.g. `uname -a`)\r\n```\r\nDarwin ##### 17.3.0 Darwin Kernel Version 17.3.0: Thu Nov  9 18:09:22 PST 2017; root:xnu-4570.31.3~1/RELEASE_X86_64 x86_64\r\n```","closed_by":{"login":"LiamHaworth","id":3233620,"node_id":"MDQ6VXNlcjMyMzM2MjA=","avatar_url":"https://avatars0.githubusercontent.com/u/3233620?v=4","gravatar_id":"","url":"https://api.github.com/users/LiamHaworth","html_url":"https://github.com/LiamHaworth","followers_url":"https://api.github.com/users/LiamHaworth/followers","following_url":"https://api.github.com/users/LiamHaworth/following{/other_user}","gists_url":"https://api.github.com/users/LiamHaworth/gists{/gist_id}","starred_url":"https://api.github.com/users/LiamHaworth/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/LiamHaworth/subscriptions","organizations_url":"https://api.github.com/users/LiamHaworth/orgs","repos_url":"https://api.github.com/users/LiamHaworth/repos","events_url":"https://api.github.com/users/LiamHaworth/events{/privacy}","received_events_url":"https://api.github.com/users/LiamHaworth/received_events","type":"User","site_admin":false}}", "commentIds":["362152898","362160534"], "labels":[]}