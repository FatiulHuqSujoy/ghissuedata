{"id":"5962", "title":"Don't log the exception object while FD is full", "body":"check this stack

``` java
Exception in thread \"HSF-Boss-15-thread-1\" java.lang.NoClassDefFoundError: ch/qos/logback/core/pattern/SpacePadder
        at ch.qos.logback.core.pattern.FormattingConverter.write(FormattingConverter.java:66)
        at ch.qos.logback.core.pattern.PatternLayoutBase.writeLoopOnConverters(PatternLayoutBase.java:119)
        at ch.qos.logback.classic.PatternLayout.doLayout(PatternLayout.java:168)
        at ch.qos.logback.classic.PatternLayout.doLayout(PatternLayout.java:59)
        at ch.qos.logback.core.encoder.LayoutWrappingEncoder.doEncode(LayoutWrappingEncoder.java:134)
        at ch.qos.logback.core.OutputStreamAppender.writeOut(OutputStreamAppender.java:188)
        at ch.qos.logback.core.OutputStreamAppender.subAppend(OutputStreamAppender.java:212)
        at ch.qos.logback.core.OutputStreamAppender.append(OutputStreamAppender.java:103)
        at ch.qos.logback.core.UnsynchronizedAppenderBase.doAppend(UnsynchronizedAppenderBase.java:88)
        at ch.qos.logback.core.spi.AppenderAttachableImpl.appendLoopOnAppenders(AppenderAttachableImpl.java:48)
        at ch.qos.logback.classic.Logger.appendLoopOnAppenders(Logger.java:272)
        at ch.qos.logback.classic.Logger.callAppenders(Logger.java:259)
        at ch.qos.logback.classic.Logger.buildLoggingEventAndAppend(Logger.java:441)
        at ch.qos.logback.classic.Logger.filterAndLog_0_Or3Plus(Logger.java:395)
        at ch.qos.logback.classic.Logger.warn(Logger.java:712)
        at io.netty.util.internal.logging.Slf4JLogger.warn(Slf4JLogger.java:151)
        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:115)

```

When vm 's fd is full, if you try to log the object of throwable,the JVM may try to do some class loading.Because of \"too many open files\", my JVM she cannot load any class then.

see this:

![image](https://cloud.githubusercontent.com/assets/4310631/19845965/d3ddc5bc-9f76-11e6-9d24-2653fa6e0465.png)


So my boss worker is down... 

Can we judge the throwable is some \"Too many open files\" exception or not ,to decide if we should log or not the object.






 ### Update ###


Actually , the NoClassDefFoundError is throw by NioEventLoop

![image](https://cloud.githubusercontent.com/assets/4310631/20549452/3f3971ec-b167-11e6-8e5a-3e78498bef23.png)


> logger.warn(\"Unexpected exception in the selector loop.\", t); 

This line throws an exception , and caught by SingleThreadEventExecutor , at the thread created by  SingleThreadEventExecutor , it wants to log the \"NoClassDefFoundError\" and trigger this error again...", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/5962","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/5962/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/5962/comments","events_url":"https://api.github.com/repos/netty/netty/issues/5962/events","html_url":"https://github.com/netty/netty/issues/5962","id":186215131,"node_id":"MDU6SXNzdWUxODYyMTUxMzE=","number":5962,"title":"Don't log the exception object while FD is full","user":{"login":"Hanson1129","id":4310631,"node_id":"MDQ6VXNlcjQzMTA2MzE=","avatar_url":"https://avatars1.githubusercontent.com/u/4310631?v=4","gravatar_id":"","url":"https://api.github.com/users/Hanson1129","html_url":"https://github.com/Hanson1129","followers_url":"https://api.github.com/users/Hanson1129/followers","following_url":"https://api.github.com/users/Hanson1129/following{/other_user}","gists_url":"https://api.github.com/users/Hanson1129/gists{/gist_id}","starred_url":"https://api.github.com/users/Hanson1129/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Hanson1129/subscriptions","organizations_url":"https://api.github.com/users/Hanson1129/orgs","repos_url":"https://api.github.com/users/Hanson1129/repos","events_url":"https://api.github.com/users/Hanson1129/events{/privacy}","received_events_url":"https://api.github.com/users/Hanson1129/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-10-31T06:34:42Z","updated_at":"2016-11-23T02:29:02Z","closed_at":"2016-11-04T07:33:13Z","author_association":"CONTRIBUTOR","body":"check this stack\r\n\r\n``` java\r\nException in thread \"HSF-Boss-15-thread-1\" java.lang.NoClassDefFoundError: ch/qos/logback/core/pattern/SpacePadder\r\n        at ch.qos.logback.core.pattern.FormattingConverter.write(FormattingConverter.java:66)\r\n        at ch.qos.logback.core.pattern.PatternLayoutBase.writeLoopOnConverters(PatternLayoutBase.java:119)\r\n        at ch.qos.logback.classic.PatternLayout.doLayout(PatternLayout.java:168)\r\n        at ch.qos.logback.classic.PatternLayout.doLayout(PatternLayout.java:59)\r\n        at ch.qos.logback.core.encoder.LayoutWrappingEncoder.doEncode(LayoutWrappingEncoder.java:134)\r\n        at ch.qos.logback.core.OutputStreamAppender.writeOut(OutputStreamAppender.java:188)\r\n        at ch.qos.logback.core.OutputStreamAppender.subAppend(OutputStreamAppender.java:212)\r\n        at ch.qos.logback.core.OutputStreamAppender.append(OutputStreamAppender.java:103)\r\n        at ch.qos.logback.core.UnsynchronizedAppenderBase.doAppend(UnsynchronizedAppenderBase.java:88)\r\n        at ch.qos.logback.core.spi.AppenderAttachableImpl.appendLoopOnAppenders(AppenderAttachableImpl.java:48)\r\n        at ch.qos.logback.classic.Logger.appendLoopOnAppenders(Logger.java:272)\r\n        at ch.qos.logback.classic.Logger.callAppenders(Logger.java:259)\r\n        at ch.qos.logback.classic.Logger.buildLoggingEventAndAppend(Logger.java:441)\r\n        at ch.qos.logback.classic.Logger.filterAndLog_0_Or3Plus(Logger.java:395)\r\n        at ch.qos.logback.classic.Logger.warn(Logger.java:712)\r\n        at io.netty.util.internal.logging.Slf4JLogger.warn(Slf4JLogger.java:151)\r\n        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:115)\r\n\r\n```\r\n\r\nWhen vm 's fd is full, if you try to log the object of throwable,the JVM may try to do some class loading.Because of \"too many open files\", my JVM she cannot load any class then.\r\n\r\nsee this:\r\n\r\n![image](https://cloud.githubusercontent.com/assets/4310631/19845965/d3ddc5bc-9f76-11e6-9d24-2653fa6e0465.png)\r\n\r\n\r\nSo my boss worker is down... \r\n\r\nCan we judge the throwable is some \"Too many open files\" exception or not ,to decide if we should log or not the object.\r\n\r\n\r\n\r\n\r\n\r\n\r\n ### Update ###\r\n\r\n\r\nActually , the NoClassDefFoundError is throw by NioEventLoop\r\n\r\n![image](https://cloud.githubusercontent.com/assets/4310631/20549452/3f3971ec-b167-11e6-8e5a-3e78498bef23.png)\r\n\r\n\r\n> logger.warn(\"Unexpected exception in the selector loop.\", t); \r\n\r\nThis line throws an exception , and caught by SingleThreadEventExecutor , at the thread created by  SingleThreadEventExecutor , it wants to log the \"NoClassDefFoundError\" and trigger this error again...","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["257234888","258362186"], "labels":[]}