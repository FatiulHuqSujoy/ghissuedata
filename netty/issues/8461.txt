{"id":"8461", "title":"Netty SSL Handler does not fail the SSL handshake even if the certificate is expired", "body":"### Expected behavior
SSL handshake should fail if the certificate is expired.

### Steps to reproduce
I have a client which uses netty SSL handler. I have used this site to test my SSL client.
https://badssl.com/ 
I tested it with the HttpSnoopClient which is given as an HTTP example client in Netty. I exported the certificate of https://expired.badssl.com/ and used it for building the SslContext.

### Minimal yet complete reproducer code (or URL to code)

SnoopClient class

```
package io.netty.example.http.snoop;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.cookie.ClientCookieEncoder;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;

import java.io.File;
import java.net.URI;

/**
 * A simple HTTP client that prints out the content of the HTTP response to
 * {@link System#out} to test {@link HttpSnoopServer}.
 */
public final class HttpSnoopClient {

    static final String URL = System.getProperty(\"url\", \"https://expired.badssl.com/\");

    public static void main(String[] args) throws Exception {
        URI uri = new URI(URL);
        String scheme = uri.getScheme() == null? \"http\" : uri.getScheme();
        String host = uri.getHost() == null? \"127.0.0.1\" : uri.getHost();
        int port = uri.getPort();
        if (port == -1) {
            if (\"http\".equalsIgnoreCase(scheme)) {
                port = 80;
            } else if (\"https\".equalsIgnoreCase(scheme)) {
                port = 443;
            }
        }

        if (!\"http\".equalsIgnoreCase(scheme) && !\"https\".equalsIgnoreCase(scheme)) {
            System.err.println(\"Only HTTP(S) is supported.\");
            return;
        }

        // Configure SSL context if necessary.
        final boolean ssl = \"https\".equalsIgnoreCase(scheme);
        final SslContext sslCtx;
        if (ssl) {
            // This is the certificate of the https://expired.badssl.com/. I exported it from the site.
            File certificate =  new File(\"/home/bhashinee/Documents/expiredCert/_.badssl.com\");
            sslCtx = SslContextBuilder.forClient().trustManager(certificate).build();
        } else {
            sslCtx = null;
        }

        // Configure the client.
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
             .channel(NioSocketChannel.class)
             .handler(new HttpSnoopClientInitializer(sslCtx));

            // Make the connection attempt.
            Channel ch = b.connect(host, port).sync().channel();

            // Prepare the HTTP request.
            HttpRequest request = new DefaultFullHttpRequest(
                    HttpVersion.HTTP_1_1, HttpMethod.GET, uri.getRawPath());
            request.headers().set(HttpHeaderNames.HOST, host);
            request.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.CLOSE);
            request.headers().set(HttpHeaderNames.ACCEPT_ENCODING, HttpHeaderValues.GZIP);

            // Set some example cookies.
            request.headers().set(
                    HttpHeaderNames.COOKIE,
                    ClientCookieEncoder.STRICT.encode(
                            new DefaultCookie(\"my-cookie\", \"foo\"),
                            new DefaultCookie(\"another-cookie\", \"bar\")));

            // Send the HTTP request.
            ch.writeAndFlush(request);

            // Wait for the server to close the connection.
            ch.closeFuture().sync();
        } finally {
            // Shut down executor threads to exit.
            group.shutdownGracefully();
        }
    }
}
```

HttpSnoopClientInitializer class

```
package io.netty.example.http.snoop;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpContentDecompressor;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslHandler;

public class HttpSnoopClientInitializer extends ChannelInitializer<SocketChannel> {

    private final SslContext sslCtx;

    public HttpSnoopClientInitializer(SslContext sslCtx) {
        this.sslCtx = sslCtx;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline p = ch.pipeline();

        // Enable HTTPS if necessary.
        if (sslCtx != null) {
            SslHandler sslHandler = sslCtx.newHandler(ch.alloc(), \"expired.badssl.com\", 443);
            p.addLast(\"ssl\", sslHandler);
        }

        p.addLast(new HttpClientCodec());

        // Remove the following line if you don't want automatic content decompression.
        p.addLast(new HttpContentDecompressor());

        // Uncomment the following line if you don't want to handle HttpContents.
        //p.addLast(new HttpObjectAggregator(1048576));

        p.addLast(new HttpSnoopClientHandler());
    }
}
```

### Netty version
4.1.19.Final

### JVM version (e.g. `java -version`)
1.8

### OS version (e.g. `uname -a`)
Ubuntu

This is the same issue I reported in https://github.com/netty/netty/issues/8455. Creating another one as I cannot re-open it. 
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8461","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8461/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8461/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8461/events","html_url":"https://github.com/netty/netty/issues/8461","id":377222257,"node_id":"MDU6SXNzdWUzNzcyMjIyNTc=","number":8461,"title":"Netty SSL Handler does not fail the SSL handshake even if the certificate is expired","user":{"login":"Bhashinee","id":11239305,"node_id":"MDQ6VXNlcjExMjM5MzA1","avatar_url":"https://avatars1.githubusercontent.com/u/11239305?v=4","gravatar_id":"","url":"https://api.github.com/users/Bhashinee","html_url":"https://github.com/Bhashinee","followers_url":"https://api.github.com/users/Bhashinee/followers","following_url":"https://api.github.com/users/Bhashinee/following{/other_user}","gists_url":"https://api.github.com/users/Bhashinee/gists{/gist_id}","starred_url":"https://api.github.com/users/Bhashinee/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Bhashinee/subscriptions","organizations_url":"https://api.github.com/users/Bhashinee/orgs","repos_url":"https://api.github.com/users/Bhashinee/repos","events_url":"https://api.github.com/users/Bhashinee/events{/privacy}","received_events_url":"https://api.github.com/users/Bhashinee/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-11-05T01:46:53Z","updated_at":"2018-11-26T19:40:07Z","closed_at":"2018-11-26T19:40:06Z","author_association":"NONE","body":"### Expected behavior\r\nSSL handshake should fail if the certificate is expired.\r\n\r\n### Steps to reproduce\r\nI have a client which uses netty SSL handler. I have used this site to test my SSL client.\r\nhttps://badssl.com/ \r\nI tested it with the HttpSnoopClient which is given as an HTTP example client in Netty. I exported the certificate of https://expired.badssl.com/ and used it for building the SslContext.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\nSnoopClient class\r\n\r\n```\r\npackage io.netty.example.http.snoop;\r\n\r\nimport io.netty.bootstrap.Bootstrap;\r\nimport io.netty.channel.Channel;\r\nimport io.netty.channel.EventLoopGroup;\r\nimport io.netty.channel.nio.NioEventLoopGroup;\r\nimport io.netty.channel.socket.nio.NioSocketChannel;\r\nimport io.netty.handler.codec.http.DefaultFullHttpRequest;\r\nimport io.netty.handler.codec.http.HttpHeaderNames;\r\nimport io.netty.handler.codec.http.HttpHeaderValues;\r\nimport io.netty.handler.codec.http.HttpMethod;\r\nimport io.netty.handler.codec.http.HttpRequest;\r\nimport io.netty.handler.codec.http.HttpVersion;\r\nimport io.netty.handler.codec.http.cookie.ClientCookieEncoder;\r\nimport io.netty.handler.codec.http.cookie.DefaultCookie;\r\nimport io.netty.handler.ssl.SslContext;\r\nimport io.netty.handler.ssl.SslContextBuilder;\r\n\r\nimport java.io.File;\r\nimport java.net.URI;\r\n\r\n/**\r\n * A simple HTTP client that prints out the content of the HTTP response to\r\n * {@link System#out} to test {@link HttpSnoopServer}.\r\n */\r\npublic final class HttpSnoopClient {\r\n\r\n    static final String URL = System.getProperty(\"url\", \"https://expired.badssl.com/\");\r\n\r\n    public static void main(String[] args) throws Exception {\r\n        URI uri = new URI(URL);\r\n        String scheme = uri.getScheme() == null? \"http\" : uri.getScheme();\r\n        String host = uri.getHost() == null? \"127.0.0.1\" : uri.getHost();\r\n        int port = uri.getPort();\r\n        if (port == -1) {\r\n            if (\"http\".equalsIgnoreCase(scheme)) {\r\n                port = 80;\r\n            } else if (\"https\".equalsIgnoreCase(scheme)) {\r\n                port = 443;\r\n            }\r\n        }\r\n\r\n        if (!\"http\".equalsIgnoreCase(scheme) && !\"https\".equalsIgnoreCase(scheme)) {\r\n            System.err.println(\"Only HTTP(S) is supported.\");\r\n            return;\r\n        }\r\n\r\n        // Configure SSL context if necessary.\r\n        final boolean ssl = \"https\".equalsIgnoreCase(scheme);\r\n        final SslContext sslCtx;\r\n        if (ssl) {\r\n            // This is the certificate of the https://expired.badssl.com/. I exported it from the site.\r\n            File certificate =  new File(\"/home/bhashinee/Documents/expiredCert/_.badssl.com\");\r\n            sslCtx = SslContextBuilder.forClient().trustManager(certificate).build();\r\n        } else {\r\n            sslCtx = null;\r\n        }\r\n\r\n        // Configure the client.\r\n        EventLoopGroup group = new NioEventLoopGroup();\r\n        try {\r\n            Bootstrap b = new Bootstrap();\r\n            b.group(group)\r\n             .channel(NioSocketChannel.class)\r\n             .handler(new HttpSnoopClientInitializer(sslCtx));\r\n\r\n            // Make the connection attempt.\r\n            Channel ch = b.connect(host, port).sync().channel();\r\n\r\n            // Prepare the HTTP request.\r\n            HttpRequest request = new DefaultFullHttpRequest(\r\n                    HttpVersion.HTTP_1_1, HttpMethod.GET, uri.getRawPath());\r\n            request.headers().set(HttpHeaderNames.HOST, host);\r\n            request.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.CLOSE);\r\n            request.headers().set(HttpHeaderNames.ACCEPT_ENCODING, HttpHeaderValues.GZIP);\r\n\r\n            // Set some example cookies.\r\n            request.headers().set(\r\n                    HttpHeaderNames.COOKIE,\r\n                    ClientCookieEncoder.STRICT.encode(\r\n                            new DefaultCookie(\"my-cookie\", \"foo\"),\r\n                            new DefaultCookie(\"another-cookie\", \"bar\")));\r\n\r\n            // Send the HTTP request.\r\n            ch.writeAndFlush(request);\r\n\r\n            // Wait for the server to close the connection.\r\n            ch.closeFuture().sync();\r\n        } finally {\r\n            // Shut down executor threads to exit.\r\n            group.shutdownGracefully();\r\n        }\r\n    }\r\n}\r\n```\r\n\r\nHttpSnoopClientInitializer class\r\n\r\n```\r\npackage io.netty.example.http.snoop;\r\n\r\nimport io.netty.channel.ChannelInitializer;\r\nimport io.netty.channel.ChannelPipeline;\r\nimport io.netty.channel.socket.SocketChannel;\r\nimport io.netty.handler.codec.http.HttpClientCodec;\r\nimport io.netty.handler.codec.http.HttpContentDecompressor;\r\nimport io.netty.handler.ssl.SslContext;\r\nimport io.netty.handler.ssl.SslHandler;\r\n\r\npublic class HttpSnoopClientInitializer extends ChannelInitializer<SocketChannel> {\r\n\r\n    private final SslContext sslCtx;\r\n\r\n    public HttpSnoopClientInitializer(SslContext sslCtx) {\r\n        this.sslCtx = sslCtx;\r\n    }\r\n\r\n    @Override\r\n    public void initChannel(SocketChannel ch) {\r\n        ChannelPipeline p = ch.pipeline();\r\n\r\n        // Enable HTTPS if necessary.\r\n        if (sslCtx != null) {\r\n            SslHandler sslHandler = sslCtx.newHandler(ch.alloc(), \"expired.badssl.com\", 443);\r\n            p.addLast(\"ssl\", sslHandler);\r\n        }\r\n\r\n        p.addLast(new HttpClientCodec());\r\n\r\n        // Remove the following line if you don't want automatic content decompression.\r\n        p.addLast(new HttpContentDecompressor());\r\n\r\n        // Uncomment the following line if you don't want to handle HttpContents.\r\n        //p.addLast(new HttpObjectAggregator(1048576));\r\n\r\n        p.addLast(new HttpSnoopClientHandler());\r\n    }\r\n}\r\n```\r\n\r\n### Netty version\r\n4.1.19.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n1.8\r\n\r\n### OS version (e.g. `uname -a`)\r\nUbuntu\r\n\r\nThis is the same issue I reported in https://github.com/netty/netty/issues/8455. Creating another one as I cannot re-open it. \r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["435873934","435982207","436202852","436296455","441770754"], "labels":[]}