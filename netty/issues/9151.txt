{"id":"9151", "title":"Change ReadTimeoutException and WriteTimeoutException to non-singleton class", "body":"### Request

Change ReadTimeoutException and WriteTimeoutException to non-singleton class.

### Reason

OOME is occurred by increasing suppressedExceptions because other libraries call `Throwable#addSuppressed` for ReadTimeoutExeption.

For example, please see reactor-core's code.
https://github.com/reactor/reactor-core/blob/master/reactor-core/src/main/java/reactor/core/publisher/BlockingSingleSubscriber.java#L132-L133

I think it should be an immutable class to be a singleton.
How do you think?

### Minimal yet complete reproducer code (or URL to code)

### Netty version
4.1.34.Final

### JVM version (e.g. `java -version`)

```
openjdk version \"1.8.0_171\"
OpenJDK Runtime Environment (build 1.8.0_171-b10)
OpenJDK 64-Bit Server VM (build 25.171-b10, mixed mode)
```

### OS version (e.g. `uname -a`)

```
openjdk version \"1.8.0_171\"
OpenJDK Runtime Environment (build 1.8.0_171-b10)
OpenJDK 64-Bit Server VM (build 25.171-b10, mixed mode)
```", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/9151","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/9151/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/9151/comments","events_url":"https://api.github.com/repos/netty/netty/issues/9151/events","html_url":"https://github.com/netty/netty/issues/9151","id":444780722,"node_id":"MDU6SXNzdWU0NDQ3ODA3MjI=","number":9151,"title":"Change ReadTimeoutException and WriteTimeoutException to non-singleton class","user":{"login":"be-hase","id":903482,"node_id":"MDQ6VXNlcjkwMzQ4Mg==","avatar_url":"https://avatars0.githubusercontent.com/u/903482?v=4","gravatar_id":"","url":"https://api.github.com/users/be-hase","html_url":"https://github.com/be-hase","followers_url":"https://api.github.com/users/be-hase/followers","following_url":"https://api.github.com/users/be-hase/following{/other_user}","gists_url":"https://api.github.com/users/be-hase/gists{/gist_id}","starred_url":"https://api.github.com/users/be-hase/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/be-hase/subscriptions","organizations_url":"https://api.github.com/users/be-hase/orgs","repos_url":"https://api.github.com/users/be-hase/repos","events_url":"https://api.github.com/users/be-hase/events{/privacy}","received_events_url":"https://api.github.com/users/be-hase/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2019-05-16T06:44:31Z","updated_at":"2019-05-17T20:23:02Z","closed_at":"2019-05-17T20:23:02Z","author_association":"NONE","body":"### Request\r\n\r\nChange ReadTimeoutException and WriteTimeoutException to non-singleton class.\r\n\r\n### Reason\r\n\r\nOOME is occurred by increasing suppressedExceptions because other libraries call `Throwable#addSuppressed` for ReadTimeoutExeption.\r\n\r\nFor example, please see reactor-core's code.\r\nhttps://github.com/reactor/reactor-core/blob/master/reactor-core/src/main/java/reactor/core/publisher/BlockingSingleSubscriber.java#L132-L133\r\n\r\nI think it should be an immutable class to be a singleton.\r\nHow do you think?\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\n4.1.34.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\n```\r\nopenjdk version \"1.8.0_171\"\r\nOpenJDK Runtime Environment (build 1.8.0_171-b10)\r\nOpenJDK 64-Bit Server VM (build 25.171-b10, mixed mode)\r\n```\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\n```\r\nopenjdk version \"1.8.0_171\"\r\nOpenJDK Runtime Environment (build 1.8.0_171-b10)\r\nOpenJDK 64-Bit Server VM (build 25.171-b10, mixed mode)\r\n```","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["492941317","492976034","492979509","493043861"], "labels":[]}