{"id":"6815", "title":"ChannelFuture stop awaitUninterruptibly causing deadlocks", "body":"### Netty version
4.0.32.Final

### JVM version
1.7.0_80

### OS version
Red Hat Enterprise Linux Server release 6.7

I found a issue very similar with https://github.com/netty/netty/issues/3662 but using version 4.0.32. 
I'm doing a awaitUninterruptibly and the thread hangs forever. 

```
public void close() {
    if(nettyChannel != null) {
	nettyChannel.close().awaitUninterruptibly();
    }
}
```
This happended on a production system and we were not able to gather alot of information. 
We had some TCP issues that increased the load of the platform (x10 times the normal traffic) and the connections to the clients were disconnecting/connecting all the time, but after that the system never recovered. 

One of the things that we did was a thread dump and it lead me to https://github.com/netty/netty/issues/3662

Thread dump:
```
   java.lang.Thread.State: WAITING
        at java.lang.Object.wait(Native Method)
        at java.lang.Object.wait(Object.java:503)
        at io.netty.util.concurrent.DefaultPromise.awaitUninterruptibly(DefaultPromise.java:286)
        at io.netty.channel.DefaultChannelPromise.awaitUninterruptibly(DefaultChannelPromise.java:135)
        at io.netty.channel.DefaultChannelPromise.awaitUninterruptibly(DefaultChannelPromise.java:28)
        at XXXXXXXX.close(XXXXXXXX.java:340)
```

After the restart the system started working corretly and since then we were not able to replicate the problem in our lab enviroment.

I search the issues of versions 4.0.32+ and didn't find anything related and google didn't help also.

I understand that without replicate the problem any possible correction will be very difficult. Does anyone had some similar issue that can help me solve this problem? Or at least replicate it?

Thanks in advance!", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6815","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6815/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6815/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6815/events","html_url":"https://github.com/netty/netty/issues/6815","id":233632449,"node_id":"MDU6SXNzdWUyMzM2MzI0NDk=","number":6815,"title":"ChannelFuture stop awaitUninterruptibly causing deadlocks","user":{"login":"ejsfreitas","id":3447368,"node_id":"MDQ6VXNlcjM0NDczNjg=","avatar_url":"https://avatars0.githubusercontent.com/u/3447368?v=4","gravatar_id":"","url":"https://api.github.com/users/ejsfreitas","html_url":"https://github.com/ejsfreitas","followers_url":"https://api.github.com/users/ejsfreitas/followers","following_url":"https://api.github.com/users/ejsfreitas/following{/other_user}","gists_url":"https://api.github.com/users/ejsfreitas/gists{/gist_id}","starred_url":"https://api.github.com/users/ejsfreitas/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ejsfreitas/subscriptions","organizations_url":"https://api.github.com/users/ejsfreitas/orgs","repos_url":"https://api.github.com/users/ejsfreitas/repos","events_url":"https://api.github.com/users/ejsfreitas/events{/privacy}","received_events_url":"https://api.github.com/users/ejsfreitas/received_events","type":"User","site_admin":false},"labels":[{"id":6731767,"node_id":"MDU6TGFiZWw2NzMxNzY3","url":"https://api.github.com/repos/netty/netty/labels/needs%20info","name":"needs info","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-06-05T15:53:11Z","updated_at":"2017-08-06T13:04:56Z","closed_at":"2017-08-06T13:04:55Z","author_association":"NONE","body":"### Netty version\r\n4.0.32.Final\r\n\r\n### JVM version\r\n1.7.0_80\r\n\r\n### OS version\r\nRed Hat Enterprise Linux Server release 6.7\r\n\r\nI found a issue very similar with https://github.com/netty/netty/issues/3662 but using version 4.0.32. \r\nI'm doing a awaitUninterruptibly and the thread hangs forever. \r\n\r\n```\r\npublic void close() {\r\n    if(nettyChannel != null) {\r\n\tnettyChannel.close().awaitUninterruptibly();\r\n    }\r\n}\r\n```\r\nThis happended on a production system and we were not able to gather alot of information. \r\nWe had some TCP issues that increased the load of the platform (x10 times the normal traffic) and the connections to the clients were disconnecting/connecting all the time, but after that the system never recovered. \r\n\r\nOne of the things that we did was a thread dump and it lead me to https://github.com/netty/netty/issues/3662\r\n\r\nThread dump:\r\n```\r\n   java.lang.Thread.State: WAITING\r\n        at java.lang.Object.wait(Native Method)\r\n        at java.lang.Object.wait(Object.java:503)\r\n        at io.netty.util.concurrent.DefaultPromise.awaitUninterruptibly(DefaultPromise.java:286)\r\n        at io.netty.channel.DefaultChannelPromise.awaitUninterruptibly(DefaultChannelPromise.java:135)\r\n        at io.netty.channel.DefaultChannelPromise.awaitUninterruptibly(DefaultChannelPromise.java:28)\r\n        at XXXXXXXX.close(XXXXXXXX.java:340)\r\n```\r\n\r\nAfter the restart the system started working corretly and since then we were not able to replicate the problem in our lab enviroment.\r\n\r\nI search the issues of versions 4.0.32+ and didn't find anything related and google didn't help also.\r\n\r\nI understand that without replicate the problem any possible correction will be very difficult. Does anyone had some similar issue that can help me solve this problem? Or at least replicate it?\r\n\r\nThanks in advance!","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["306332214","320505782"], "labels":["needs info"]}