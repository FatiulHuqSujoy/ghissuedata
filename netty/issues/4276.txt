{"id":"4276", "title":"Avoid adding same pipeline ", "body":"I am using Port unification example, I  had customize the program in a way that stream comes and with to new handler based on stream input. Then new string comes then it again add same pipeline . actually didn't remove P.remove(this). Because  my stream are continuously coming and with to different  pipeline based on that.

@Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        //Ultra_Protocol_Pipeline(ctx);

```
    REQUEST_DATA=convertToHex(buffer.array(),5);
    DATA_STRING_FIRST_4_CHAR = REQUEST_DATA.substring(0, 4);
    switch (DATA_STRING_FIRST_4_CHAR) {

    case \"6868\":
        Ultra_Protocol_Pipeline(ctx) ;
        break;
    case \"7878\":                            
        //NewUltra_Protocol_Pipeline(ctx);
        break;
    case \"2929\":                            
        Lite_Protocol_Pipeline(ctx);
        break;

    default :

        break;
    }
    int l =buffer.readableBytes(); 
    //buffer.readBytes(buffer.readableBytes());
    return buffer.readBytes(buffer.readableBytes());
    //return buffer;
}
private void Ultra_Protocol_Pipeline(ChannelHandlerContext ctx) {
    ChannelPipeline p = ctx.getPipeline();

    p.addLast(\"Ultradecoder\",new Ultra_Decoder());
    p.addLast( \"executor\",new ExecutionHandler(new OrderedMemoryAwareThreadPoolExecutor( 16, 1048576, 1048576) ) );
    p.addLast(\"handler\",new DatabaseUpdate());
    //p.remove(this);
}

private void Lite_Protocol_Pipeline(ChannelHandlerContext ctx) {
    ChannelPipeline p = ctx.getPipeline();
    p.addLast(\"Litedecoder\", new Lite_Decoder());
    p.addLast( \"executor\", new ExecutionHandler(new OrderedMemoryAwareThreadPoolExecutor( 16, 1048576, 1048576) ) );
    p.addLast(\"handler\", new DatabaseUpdate());
    //p.remove(this);
}
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4276","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4276/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4276/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4276/events","html_url":"https://github.com/netty/netty/issues/4276","id":108298051,"node_id":"MDU6SXNzdWUxMDgyOTgwNTE=","number":4276,"title":"Avoid adding same pipeline ","user":{"login":"VivekBohra","id":8447520,"node_id":"MDQ6VXNlcjg0NDc1MjA=","avatar_url":"https://avatars1.githubusercontent.com/u/8447520?v=4","gravatar_id":"","url":"https://api.github.com/users/VivekBohra","html_url":"https://github.com/VivekBohra","followers_url":"https://api.github.com/users/VivekBohra/followers","following_url":"https://api.github.com/users/VivekBohra/following{/other_user}","gists_url":"https://api.github.com/users/VivekBohra/gists{/gist_id}","starred_url":"https://api.github.com/users/VivekBohra/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/VivekBohra/subscriptions","organizations_url":"https://api.github.com/users/VivekBohra/orgs","repos_url":"https://api.github.com/users/VivekBohra/repos","events_url":"https://api.github.com/users/VivekBohra/events{/privacy}","received_events_url":"https://api.github.com/users/VivekBohra/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-09-25T10:04:45Z","updated_at":"2016-05-06T18:35:06Z","closed_at":"2016-05-06T18:35:06Z","author_association":"NONE","body":"I am using Port unification example, I  had customize the program in a way that stream comes and with to new handler based on stream input. Then new string comes then it again add same pipeline . actually didn't remove P.remove(this). Because  my stream are continuously coming and with to different  pipeline based on that.\n\n@Override\n    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {\n        //Ultra_Protocol_Pipeline(ctx);\n\n```\n    REQUEST_DATA=convertToHex(buffer.array(),5);\n    DATA_STRING_FIRST_4_CHAR = REQUEST_DATA.substring(0, 4);\n    switch (DATA_STRING_FIRST_4_CHAR) {\n\n    case \"6868\":\n        Ultra_Protocol_Pipeline(ctx) ;\n        break;\n    case \"7878\":                            \n        //NewUltra_Protocol_Pipeline(ctx);\n        break;\n    case \"2929\":                            \n        Lite_Protocol_Pipeline(ctx);\n        break;\n\n    default :\n\n        break;\n    }\n    int l =buffer.readableBytes(); \n    //buffer.readBytes(buffer.readableBytes());\n    return buffer.readBytes(buffer.readableBytes());\n    //return buffer;\n}\nprivate void Ultra_Protocol_Pipeline(ChannelHandlerContext ctx) {\n    ChannelPipeline p = ctx.getPipeline();\n\n    p.addLast(\"Ultradecoder\",new Ultra_Decoder());\n    p.addLast( \"executor\",new ExecutionHandler(new OrderedMemoryAwareThreadPoolExecutor( 16, 1048576, 1048576) ) );\n    p.addLast(\"handler\",new DatabaseUpdate());\n    //p.remove(this);\n}\n\nprivate void Lite_Protocol_Pipeline(ChannelHandlerContext ctx) {\n    ChannelPipeline p = ctx.getPipeline();\n    p.addLast(\"Litedecoder\", new Lite_Decoder());\n    p.addLast( \"executor\", new ExecutionHandler(new OrderedMemoryAwareThreadPoolExecutor( 16, 1048576, 1048576) ) );\n    p.addLast(\"handler\", new DatabaseUpdate());\n    //p.remove(this);\n}\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["144492156","144626920","217524059"], "labels":[]}