{"id":"6949", "title":"pthread_create (1040KB stack) failed: Out of memory", "body":"### Expected behavior
 my program  throw \"pthread_create (1040KB stack) failed: Out of memory\" exception when it's run in mWorkerGroup.shutdownGracefully() ,here is my code:
` mWorkerGroup = new NioEventLoopGroup(0, Executors.newFixedThreadPool(5));
        try {
            Bootstrap mBootstrap = new Bootstrap();
            mBootstrap.group(mWorkerGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new LengthFieldBasedFrameDecoder(1024 * 4, 0, 4, 0, 0));
                            pipeline.addLast(new IdleStateHandler(HEART_BEAT_WAIT_SECOND, HEART_BEAT_WAIT_SECOND, HEART_BEAT_WAIT_SECOND + 3, TimeUnit.SECONDS));
                            pipeline.addLast(new LoggingHandler(LogLevel.INFO));
                            pipeline.addLast(new ClientChannelHandler());
                        }
                    })
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 4000);

            mBootstrap.connect(new InetSocketAddress(host, port)).addListener(mConnectFutureListener)
                    .sync().channel().config().setAllocator(PooledByteBufAllocator.DEFAULT);
        } catch (Throwable e) {
            L.e(TAG, \"Exception: \" + e.getMessage());
            netState = Constant.NETTY_STATE_DISCONNECT;
            HeartThreadResease();
            if (mWorkerGroup != null)
                mWorkerGroup.shutdownGracefully();
            Constant.connectCount++;
        }`
### Actual behavior

### Steps to reproduce

### Minimal yet complete reproducer code (or URL to code)

### Netty version
netty-all-4.1.12.Final

### JVM version (e.g. `java -version`)
--
### OS version (e.g. `uname -a`)
android ", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6949","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6949/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6949/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6949/events","html_url":"https://github.com/netty/netty/issues/6949","id":240984273,"node_id":"MDU6SXNzdWUyNDA5ODQyNzM=","number":6949,"title":"pthread_create (1040KB stack) failed: Out of memory","user":{"login":"Tipsyume","id":6085592,"node_id":"MDQ6VXNlcjYwODU1OTI=","avatar_url":"https://avatars3.githubusercontent.com/u/6085592?v=4","gravatar_id":"","url":"https://api.github.com/users/Tipsyume","html_url":"https://github.com/Tipsyume","followers_url":"https://api.github.com/users/Tipsyume/followers","following_url":"https://api.github.com/users/Tipsyume/following{/other_user}","gists_url":"https://api.github.com/users/Tipsyume/gists{/gist_id}","starred_url":"https://api.github.com/users/Tipsyume/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Tipsyume/subscriptions","organizations_url":"https://api.github.com/users/Tipsyume/orgs","repos_url":"https://api.github.com/users/Tipsyume/repos","events_url":"https://api.github.com/users/Tipsyume/events{/privacy}","received_events_url":"https://api.github.com/users/Tipsyume/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-07-06T14:54:04Z","updated_at":"2017-07-07T15:33:13Z","closed_at":"2017-07-07T09:56:24Z","author_association":"NONE","body":"### Expected behavior\r\n my program  throw \"pthread_create (1040KB stack) failed: Out of memory\" exception when it's run in mWorkerGroup.shutdownGracefully() ,here is my code:\r\n` mWorkerGroup = new NioEventLoopGroup(0, Executors.newFixedThreadPool(5));\r\n        try {\r\n            Bootstrap mBootstrap = new Bootstrap();\r\n            mBootstrap.group(mWorkerGroup)\r\n                    .channel(NioSocketChannel.class)\r\n                    .option(ChannelOption.TCP_NODELAY, true)\r\n                    .option(ChannelOption.SO_KEEPALIVE, true)\r\n                    .handler(new ChannelInitializer<SocketChannel>() {\r\n                        @Override\r\n                        protected void initChannel(SocketChannel socketChannel) throws Exception {\r\n                            ChannelPipeline pipeline = socketChannel.pipeline();\r\n                            pipeline.addLast(new LengthFieldBasedFrameDecoder(1024 * 4, 0, 4, 0, 0));\r\n                            pipeline.addLast(new IdleStateHandler(HEART_BEAT_WAIT_SECOND, HEART_BEAT_WAIT_SECOND, HEART_BEAT_WAIT_SECOND + 3, TimeUnit.SECONDS));\r\n                            pipeline.addLast(new LoggingHandler(LogLevel.INFO));\r\n                            pipeline.addLast(new ClientChannelHandler());\r\n                        }\r\n                    })\r\n                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 4000);\r\n\r\n            mBootstrap.connect(new InetSocketAddress(host, port)).addListener(mConnectFutureListener)\r\n                    .sync().channel().config().setAllocator(PooledByteBufAllocator.DEFAULT);\r\n        } catch (Throwable e) {\r\n            L.e(TAG, \"Exception: \" + e.getMessage());\r\n            netState = Constant.NETTY_STATE_DISCONNECT;\r\n            HeartThreadResease();\r\n            if (mWorkerGroup != null)\r\n                mWorkerGroup.shutdownGracefully();\r\n            Constant.connectCount++;\r\n        }`\r\n### Actual behavior\r\n\r\n### Steps to reproduce\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\nnetty-all-4.1.12.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n--\r\n### OS version (e.g. `uname -a`)\r\nandroid ","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["313639943","313714446","313715500"], "labels":[]}