{"id":"2451", "title":"HttpPostRequestDecoder fails to decode non-multipart, non-chunked body", "body":"Netty version: 3.9.0.Final

Context:
I encountered a bug when writing a POC using Twitter Finagle and a Sinatra like layer called Finatra (https://github.com/twitter/finatra).

When an xml body was POST'ed to the server that contained an equal sign '=' followed anywhere later by the string '20%', the request would 500.

Finatra checks to see whether it needs to process a Multipart request by:

``` scala
    val dec = new HttpPostRequestDecoder(request) //<-- Fails here
      if (dec.isMultipart) {
         // fill a Map of MultipartItems
      }
```

The request was failed on the construction of HttpPostRequestDecoder. The stack looks like:

```
05:35:12.855 [New I/O worker #1] ERROR c.h.h.a.i.filters.ExceptionFilter - Exception thrown Bad string: '\"20%\"'
org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder$ErrorDataDecoderException: Bad string: '\"20%\"'
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:528) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.setFinalBuffer(HttpPostStandardRequestDecoder.java:506) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBodyAttributes(HttpPostStandardRequestDecoder.java:472) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBody(HttpPostStandardRequestDecoder.java:232) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.<init>(HttpPostStandardRequestDecoder.java:150) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:88) ~[netty-3.9.0.Final.jar:na]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:46) ~[netty-3.9.0.Final.jar:na]
    at com.twitter.finatra.MultipartParsing$.apply(MultipartParsing.scala:28) ~[finatra_2.10-1.5.3.jar:1.5.3]
....
Caused by: java.lang.IllegalArgumentException: URLDecoder: Incomplete trailing escape (%) pattern
    at java.net.URLDecoder.decode(URLDecoder.java:187) ~[na:1.7.0_51]
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:524) ~[netty-3.9.0.Final.jar:na]
    ... 149 common frames omitted
```

Steps to reproduce:
Add the following test to org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoderTest

``` java
    @Test
    public void testNonMultipartNonChunkedWithBodyContainingEqualsAnd20Percent() throws Exception {
        String payload = \"<foo Text=\\\"Saving 20% off awesome price!\\\"/></foo>\";
        DefaultHttpRequest defaultHttpRequest =
                new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, \"/\");
        defaultHttpRequest.setContent(ChannelBuffers.wrappedBuffer(payload.getBytes()));

        HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(defaultHttpRequest);
        assertFalse(decoder.isMultipart());
    }

```

The test fails with:

```
org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder$ErrorDataDecoderException: Bad string: '\"Saving 20% off awesome price!\"/></foo>'
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:541)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.setFinalBuffer(HttpPostStandardRequestDecoder.java:519)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBodyAttributes(HttpPostStandardRequestDecoder.java:481)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBody(HttpPostStandardRequestDecoder.java:232)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.<init>(HttpPostStandardRequestDecoder.java:150)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:88)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:46)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoderTest.testNonMultipartNonChunkedWithBodyContainingEqualsAnd20Percent(HttpPostRequestDecoderTest.java:135)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:45)
    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:15)
    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:42)
    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:20)
    at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:263)
    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:68)
    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:47)
    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:231)
    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:60)
    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:229)
    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:50)
    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:222)
    at org.junit.runners.ParentRunner.run(ParentRunner.java:300)
    at org.junit.runner.JUnitCore.run(JUnitCore.java:157)
    at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:74)
    at com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:211)
    at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:67)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:134)
Caused by: java.lang.IllegalArgumentException: URLDecoder: Illegal hex characters in escape (%) pattern - For input string: \" o\"
    at java.net.URLDecoder.decode(URLDecoder.java:192)
    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:537)
    ... 33 more

```

$ java -version
java version \"1.7.0_51\"
Java(TM) SE Runtime Environment (build 1.7.0_51-b13)
Java HotSpot(TM) 64-Bit Server VM (build 24.51-b03, mixed mode)

Operating system: OSX 10.9.2

$ uname -a
Darwin Ashley-MacBook-Pro.local 13.1.0 Darwin Kernel Version 13.1.0: Wed Apr  2 23:52:02 PDT 2014; root:xnu-2422.92.1~2/RELEASE_X86_64 x86_64
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2451","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2451/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2451/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2451/events","html_url":"https://github.com/netty/netty/issues/2451","id":32641794,"node_id":"MDU6SXNzdWUzMjY0MTc5NA==","number":2451,"title":"HttpPostRequestDecoder fails to decode non-multipart, non-chunked body","user":{"login":"Freaky-namuH","id":1662662,"node_id":"MDQ6VXNlcjE2NjI2NjI=","avatar_url":"https://avatars0.githubusercontent.com/u/1662662?v=4","gravatar_id":"","url":"https://api.github.com/users/Freaky-namuH","html_url":"https://github.com/Freaky-namuH","followers_url":"https://api.github.com/users/Freaky-namuH/followers","following_url":"https://api.github.com/users/Freaky-namuH/following{/other_user}","gists_url":"https://api.github.com/users/Freaky-namuH/gists{/gist_id}","starred_url":"https://api.github.com/users/Freaky-namuH/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Freaky-namuH/subscriptions","organizations_url":"https://api.github.com/users/Freaky-namuH/orgs","repos_url":"https://api.github.com/users/Freaky-namuH/repos","events_url":"https://api.github.com/users/Freaky-namuH/events{/privacy}","received_events_url":"https://api.github.com/users/Freaky-namuH/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2014-05-01T19:43:41Z","updated_at":"2014-07-18T03:53:26Z","closed_at":"2014-05-02T20:54:20Z","author_association":"NONE","body":"Netty version: 3.9.0.Final\n\nContext:\nI encountered a bug when writing a POC using Twitter Finagle and a Sinatra like layer called Finatra (https://github.com/twitter/finatra).\n\nWhen an xml body was POST'ed to the server that contained an equal sign '=' followed anywhere later by the string '20%', the request would 500.\n\nFinatra checks to see whether it needs to process a Multipart request by:\n\n``` scala\n    val dec = new HttpPostRequestDecoder(request) //<-- Fails here\n      if (dec.isMultipart) {\n         // fill a Map of MultipartItems\n      }\n```\n\nThe request was failed on the construction of HttpPostRequestDecoder. The stack looks like:\n\n```\n05:35:12.855 [New I/O worker #1] ERROR c.h.h.a.i.filters.ExceptionFilter - Exception thrown Bad string: '\"20%\"'\norg.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder$ErrorDataDecoderException: Bad string: '\"20%\"'\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:528) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.setFinalBuffer(HttpPostStandardRequestDecoder.java:506) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBodyAttributes(HttpPostStandardRequestDecoder.java:472) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBody(HttpPostStandardRequestDecoder.java:232) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.<init>(HttpPostStandardRequestDecoder.java:150) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:88) ~[netty-3.9.0.Final.jar:na]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:46) ~[netty-3.9.0.Final.jar:na]\n    at com.twitter.finatra.MultipartParsing$.apply(MultipartParsing.scala:28) ~[finatra_2.10-1.5.3.jar:1.5.3]\n....\nCaused by: java.lang.IllegalArgumentException: URLDecoder: Incomplete trailing escape (%) pattern\n    at java.net.URLDecoder.decode(URLDecoder.java:187) ~[na:1.7.0_51]\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:524) ~[netty-3.9.0.Final.jar:na]\n    ... 149 common frames omitted\n```\n\nSteps to reproduce:\nAdd the following test to org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoderTest\n\n``` java\n    @Test\n    public void testNonMultipartNonChunkedWithBodyContainingEqualsAnd20Percent() throws Exception {\n        String payload = \"<foo Text=\\\"Saving 20% off awesome price!\\\"/></foo>\";\n        DefaultHttpRequest defaultHttpRequest =\n                new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, \"/\");\n        defaultHttpRequest.setContent(ChannelBuffers.wrappedBuffer(payload.getBytes()));\n\n        HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(defaultHttpRequest);\n        assertFalse(decoder.isMultipart());\n    }\n\n```\n\nThe test fails with:\n\n```\norg.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder$ErrorDataDecoderException: Bad string: '\"Saving 20% off awesome price!\"/></foo>'\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:541)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.setFinalBuffer(HttpPostStandardRequestDecoder.java:519)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBodyAttributes(HttpPostStandardRequestDecoder.java:481)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.parseBody(HttpPostStandardRequestDecoder.java:232)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.<init>(HttpPostStandardRequestDecoder.java:150)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:88)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.<init>(HttpPostRequestDecoder.java:46)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoderTest.testNonMultipartNonChunkedWithBodyContainingEqualsAnd20Percent(HttpPostRequestDecoderTest.java:135)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:45)\n    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:15)\n    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:42)\n    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:20)\n    at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:263)\n    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:68)\n    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:47)\n    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:231)\n    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:60)\n    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:229)\n    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:50)\n    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:222)\n    at org.junit.runners.ParentRunner.run(ParentRunner.java:300)\n    at org.junit.runner.JUnitCore.run(JUnitCore.java:157)\n    at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:74)\n    at com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:211)\n    at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:67)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\n    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:134)\nCaused by: java.lang.IllegalArgumentException: URLDecoder: Illegal hex characters in escape (%) pattern - For input string: \" o\"\n    at java.net.URLDecoder.decode(URLDecoder.java:192)\n    at org.jboss.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder.decodeAttribute(HttpPostStandardRequestDecoder.java:537)\n    ... 33 more\n\n```\n\n$ java -version\njava version \"1.7.0_51\"\nJava(TM) SE Runtime Environment (build 1.7.0_51-b13)\nJava HotSpot(TM) 64-Bit Server VM (build 24.51-b03, mixed mode)\n\nOperating system: OSX 10.9.2\n\n$ uname -a\nDarwin Ashley-MacBook-Pro.local 13.1.0 Darwin Kernel Version 13.1.0: Wed Apr  2 23:52:02 PDT 2014; root:xnu-2422.92.1~2/RELEASE_X86_64 x86_64\n","closed_by":{"login":"fredericBregier","id":477131,"node_id":"MDQ6VXNlcjQ3NzEzMQ==","avatar_url":"https://avatars3.githubusercontent.com/u/477131?v=4","gravatar_id":"","url":"https://api.github.com/users/fredericBregier","html_url":"https://github.com/fredericBregier","followers_url":"https://api.github.com/users/fredericBregier/followers","following_url":"https://api.github.com/users/fredericBregier/following{/other_user}","gists_url":"https://api.github.com/users/fredericBregier/gists{/gist_id}","starred_url":"https://api.github.com/users/fredericBregier/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/fredericBregier/subscriptions","organizations_url":"https://api.github.com/users/fredericBregier/orgs","repos_url":"https://api.github.com/users/fredericBregier/repos","events_url":"https://api.github.com/users/fredericBregier/events{/privacy}","received_events_url":"https://api.github.com/users/fredericBregier/received_events","type":"User","site_admin":false}}", "commentIds":["41997216","42024483","42025762","42076181","42077990"], "labels":[]}