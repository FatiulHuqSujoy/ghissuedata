{"id":"2522", "title":"Lingering `ThreadLocals` after graceful shutdown", "body":"After gracefully shutting down all Netty resources (channels, event loop groups, etc.), Netty leaves behind a number of `ThreadLocal` instances. In some contexts (e.g. servlet containers), this will yield complaints about potential memory leaks. I'm not sure if resources are actually leaking or if the warning is harmless.

Writing a concise, standalone example that detects lingering `ThreadLocal` instances would be a little dicey, so I've opted to demo this with Tomcat 7. Here's an example listener that minimally reproduces the problem:

``` java
@WebListener
public class ContextLifecycleListener implements ServletContextListener {

    private NioEventLoopGroup eventLoopGroup;
    private Channel channel;

    @Override
    public void contextInitialized(final ServletContextEvent context) {
        this.eventLoopGroup = new NioEventLoopGroup();

        final Bootstrap bootstrap = new Bootstrap()
        .group(this.eventLoopGroup)
        .channel(NioSocketChannel.class)
        .handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(final SocketChannel channel) {}
        });

        final ChannelFuture connectFuture = bootstrap.connect(\"netty.io\", 80);

        try {
            connectFuture.await();
            this.channel = connectFuture.channel();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void contextDestroyed(final ServletContextEvent context) {
        if (this.channel != null) {
            try {
                this.channel.close().await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        if (this.eventLoopGroup != null) {
            try {
                this.eventLoopGroup.shutdownGracefully().await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            // Sleep to let the GlobalEventExecutor finish up.
            // See https://github.com/netty/netty/issues/2084.
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Tomcat complains about lingering `ThreadLocal` instances when the server is shut down:

```
INFO: Stopping service Catalina
May 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks
SEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.util.Recycler$1] (value [io.netty.util.Recycler$1@61beb1cd]) and a value of type [io.netty.util.Recycler.Stack] (value [io.netty.util.Recycler$Stack@2e8b3bdd]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.
May 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks
SEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.channel.ChannelHandlerAdapter$1] (value [io.netty.channel.ChannelHandlerAdapter$1@3707420a]) and a value of type [java.util.WeakHashMap] (value [{class ContextLifecycleListener$1=true}]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.
May 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks
SEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.util.internal.ThreadLocalRandom$3] (value [io.netty.util.internal.ThreadLocalRandom$3@6ed33792]) and a value of type [io.netty.util.internal.ThreadLocalRandom] (value [io.netty.util.internal.ThreadLocalRandom@6ce58b15]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.
```

This seems similar to #2084.

_EDIT: This happens with Netty 4.0.19._
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2522","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2522/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2522/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2522/events","html_url":"https://github.com/netty/netty/issues/2522","id":34709772,"node_id":"MDU6SXNzdWUzNDcwOTc3Mg==","number":2522,"title":"Lingering `ThreadLocals` after graceful shutdown","user":{"login":"jchambers","id":31352,"node_id":"MDQ6VXNlcjMxMzUy","avatar_url":"https://avatars3.githubusercontent.com/u/31352?v=4","gravatar_id":"","url":"https://api.github.com/users/jchambers","html_url":"https://github.com/jchambers","followers_url":"https://api.github.com/users/jchambers/followers","following_url":"https://api.github.com/users/jchambers/following{/other_user}","gists_url":"https://api.github.com/users/jchambers/gists{/gist_id}","starred_url":"https://api.github.com/users/jchambers/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jchambers/subscriptions","organizations_url":"https://api.github.com/users/jchambers/orgs","repos_url":"https://api.github.com/users/jchambers/repos","events_url":"https://api.github.com/users/jchambers/events{/privacy}","received_events_url":"https://api.github.com/users/jchambers/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false},"assignees":[{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/96","html_url":"https://github.com/netty/netty/milestone/96","labels_url":"https://api.github.com/repos/netty/netty/milestones/96/labels","id":689309,"node_id":"MDk6TWlsZXN0b25lNjg5MzA5","number":96,"title":"4.0.21.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":35,"state":"closed","created_at":"2014-06-12T13:44:57Z","updated_at":"2014-07-03T19:33:10Z","due_on":null,"closed_at":"2014-07-01T16:17:24Z"},"comments":11,"created_at":"2014-05-31T16:26:05Z","updated_at":"2016-06-27T16:03:02Z","closed_at":"2014-06-24T10:21:29Z","author_association":"CONTRIBUTOR","body":"After gracefully shutting down all Netty resources (channels, event loop groups, etc.), Netty leaves behind a number of `ThreadLocal` instances. In some contexts (e.g. servlet containers), this will yield complaints about potential memory leaks. I'm not sure if resources are actually leaking or if the warning is harmless.\n\nWriting a concise, standalone example that detects lingering `ThreadLocal` instances would be a little dicey, so I've opted to demo this with Tomcat 7. Here's an example listener that minimally reproduces the problem:\n\n``` java\n@WebListener\npublic class ContextLifecycleListener implements ServletContextListener {\n\n    private NioEventLoopGroup eventLoopGroup;\n    private Channel channel;\n\n    @Override\n    public void contextInitialized(final ServletContextEvent context) {\n        this.eventLoopGroup = new NioEventLoopGroup();\n\n        final Bootstrap bootstrap = new Bootstrap()\n        .group(this.eventLoopGroup)\n        .channel(NioSocketChannel.class)\n        .handler(new ChannelInitializer<SocketChannel>() {\n            @Override\n            protected void initChannel(final SocketChannel channel) {}\n        });\n\n        final ChannelFuture connectFuture = bootstrap.connect(\"netty.io\", 80);\n\n        try {\n            connectFuture.await();\n            this.channel = connectFuture.channel();\n        } catch (InterruptedException e) {\n            throw new RuntimeException(e);\n        }\n    }\n\n    @Override\n    public void contextDestroyed(final ServletContextEvent context) {\n        if (this.channel != null) {\n            try {\n                this.channel.close().await();\n            } catch (InterruptedException e) {\n                throw new RuntimeException(e);\n            }\n        }\n\n        if (this.eventLoopGroup != null) {\n            try {\n                this.eventLoopGroup.shutdownGracefully().await();\n            } catch (InterruptedException e) {\n                throw new RuntimeException(e);\n            }\n        }\n\n        try {\n            // Sleep to let the GlobalEventExecutor finish up.\n            // See https://github.com/netty/netty/issues/2084.\n            Thread.sleep(5000);\n        } catch (InterruptedException e) {\n            throw new RuntimeException(e);\n        }\n    }\n}\n```\n\nTomcat complains about lingering `ThreadLocal` instances when the server is shut down:\n\n```\nINFO: Stopping service Catalina\nMay 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks\nSEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.util.Recycler$1] (value [io.netty.util.Recycler$1@61beb1cd]) and a value of type [io.netty.util.Recycler.Stack] (value [io.netty.util.Recycler$Stack@2e8b3bdd]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.\nMay 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks\nSEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.channel.ChannelHandlerAdapter$1] (value [io.netty.channel.ChannelHandlerAdapter$1@3707420a]) and a value of type [java.util.WeakHashMap] (value [{class ContextLifecycleListener$1=true}]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.\nMay 31, 2014 12:21:24 PM org.apache.catalina.loader.WebappClassLoader checkThreadLocalMapForLeaks\nSEVERE: The web application [/NettyThreadLocalTest] created a ThreadLocal with key of type [io.netty.util.internal.ThreadLocalRandom$3] (value [io.netty.util.internal.ThreadLocalRandom$3@6ed33792]) and a value of type [io.netty.util.internal.ThreadLocalRandom] (value [io.netty.util.internal.ThreadLocalRandom@6ce58b15]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.\n```\n\nThis seems similar to #2084.\n\n_EDIT: This happens with Netty 4.0.19._\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["46096473","46100403","46106363","46292889","46954751","58223667","58226916","58228238","61172786","61268634","228790550"], "labels":["defect"]}