{"id":"1317", "title":"Cannot Reuse ChannelPromises for writes", "body":"Swear @normanmaurer said I could do this to reduce GC pressure, only the first write works. Any idea of limiting the absurd amount of futures created? (thousand(s) a second under high load)

````package net.md_5.bungee.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelPromise;

public class ChannelWrapper
{

```
private final Channel ch;
private final ChannelPromise defaultPromise;

public ChannelWrapper(Channel ch)
{
    this.ch = ch;
    this.defaultPromise = ch.newPromise();    }

public void write(Object packet)
{
    ch.write( packet, defaultPromise );
}

public Channel getHandle()
{
    return ch;
}
```

}

```
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1317","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1317/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1317/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1317/events","html_url":"https://github.com/netty/netty/issues/1317","id":13718433,"node_id":"MDU6SXNzdWUxMzcxODQzMw==","number":1317,"title":"Cannot Reuse ChannelPromises for writes","user":{"login":"md-5","id":1007849,"node_id":"MDQ6VXNlcjEwMDc4NDk=","avatar_url":"https://avatars2.githubusercontent.com/u/1007849?v=4","gravatar_id":"","url":"https://api.github.com/users/md-5","html_url":"https://github.com/md-5","followers_url":"https://api.github.com/users/md-5/followers","following_url":"https://api.github.com/users/md-5/following{/other_user}","gists_url":"https://api.github.com/users/md-5/gists{/gist_id}","starred_url":"https://api.github.com/users/md-5/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/md-5/subscriptions","organizations_url":"https://api.github.com/users/md-5/orgs","repos_url":"https://api.github.com/users/md-5/repos","events_url":"https://api.github.com/users/md-5/events{/privacy}","received_events_url":"https://api.github.com/users/md-5/received_events","type":"User","site_admin":false},"labels":[{"id":185730,"node_id":"MDU6TGFiZWwxODU3MzA=","url":"https://api.github.com/repos/netty/netty/labels/feature","name":"feature","color":"009800","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/51","html_url":"https://github.com/netty/netty/milestone/51","labels_url":"https://api.github.com/repos/netty/netty/milestones/51/labels","id":329712,"node_id":"MDk6TWlsZXN0b25lMzI5NzEy","number":51,"title":"4.0.0.CR3","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":19,"state":"closed","created_at":"2013-05-08T13:27:10Z","updated_at":"2013-09-05T14:19:21Z","due_on":"2013-05-18T07:00:00Z","closed_at":"2013-05-18T07:38:47Z"},"comments":18,"created_at":"2013-04-27T08:49:15Z","updated_at":"2013-05-17T21:35:39Z","closed_at":"2013-05-17T13:51:47Z","author_association":"CONTRIBUTOR","body":"Swear @normanmaurer said I could do this to reduce GC pressure, only the first write works. Any idea of limiting the absurd amount of futures created? (thousand(s) a second under high load)\n\n````package net.md_5.bungee.netty;\n\nimport io.netty.channel.Channel;\nimport io.netty.channel.ChannelFuture;\nimport io.netty.channel.ChannelFutureListener;\nimport io.netty.channel.ChannelPromise;\n\npublic class ChannelWrapper\n{\n\n```\nprivate final Channel ch;\nprivate final ChannelPromise defaultPromise;\n\npublic ChannelWrapper(Channel ch)\n{\n    this.ch = ch;\n    this.defaultPromise = ch.newPromise();    }\n\npublic void write(Object packet)\n{\n    ch.write( packet, defaultPromise );\n}\n\npublic Channel getHandle()\n{\n    return ch;\n}\n```\n\n}\n\n```\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["17113107","17113364","17115317","17120159","17123829","17124950","17182281","17195204","17760323","17793584","17793642","17793769","17793820","17794016","17794033","17874792","18062428","18087336"], "labels":["feature"]}