{"id":"6618", "title":"Maven build failure on MacOX Sierra 10.12.4", "body":"### Expected behavior
Building successful in command line using command \"mvn clean package -DskipTests\".
### Actual behavior
Current netty is ok to build, but we will occur build failure if we clone a new netty project.

Failure while building Netty/Handler module.
Here is the error output:

[ERROR] Failed to execute goal on project netty-handler: Could not resolve dependencies for project io.netty:netty-handler:jar:4.1.10.Final-SNAPSHOT: Failure to find io.netty:netty-tcnative:jar:osx-x86_64:2.0.1.Final-SNAPSHOT in https://oss.sonatype.org/content/repositories/snapshots was cached in the local repository, resolution will not be reattempted until the update interval of sonatype-nexus-snapshots has elapsed or updates are forced -> [Help 1]

### Steps to reproduce
1. mkdir tmp & cd tmp  Or   just remove current netty:  rm -rf netty/
2. clone a new netty project:   git clone https://github.com/netty/netty.git
3. cd netty
4. mvn clean package -DskipTests
### Minimal yet complete reproducer code (or URL to code)

### Netty version
4.0 && 4.1
### JVM version (e.g. `java -version`)

java version \"1.8.0_40\"
Java(TM) SE Runtime Environment (build 1.8.0_40-b27)
Java HotSpot(TM) 64-Bit Server VM (build 25.40-b25, mixed mode)

### OS version (e.g. `uname -a`)

Darwin kenny.local 16.5.0 Darwin Kernel Version 16.5.0: Fri Mar  3 16:52:33 PST 2017; root:xnu-3789.51.2~3/RELEASE_X86_64 x86_64

### Maven version
Apache Maven 3.3.9

### Location
China

### NOTE
I have built successfully a week ago, Is that possible for the commit https://github.com/netty/netty/commit/077a1988b9c96b5f3e5693a3bb1b2e118992f3a8  ?

I tried to download the missing jar Manually. However the latest version of netty-handler is 4.1.9.Final and netty-tcnative is 2.0.0 Final

netty-handler:    https://mvnrepository.com/artifact/io.netty/netty-handler
netty-tcnative:    https://mvnrepository.com/artifact/io.netty/netty-tcnative


", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6618","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6618/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6618/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6618/events","html_url":"https://github.com/netty/netty/issues/6618","id":220155240,"node_id":"MDU6SXNzdWUyMjAxNTUyNDA=","number":6618,"title":"Maven build failure on MacOX Sierra 10.12.4","user":{"login":"kennylbj","id":6435874,"node_id":"MDQ6VXNlcjY0MzU4NzQ=","avatar_url":"https://avatars0.githubusercontent.com/u/6435874?v=4","gravatar_id":"","url":"https://api.github.com/users/kennylbj","html_url":"https://github.com/kennylbj","followers_url":"https://api.github.com/users/kennylbj/followers","following_url":"https://api.github.com/users/kennylbj/following{/other_user}","gists_url":"https://api.github.com/users/kennylbj/gists{/gist_id}","starred_url":"https://api.github.com/users/kennylbj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kennylbj/subscriptions","organizations_url":"https://api.github.com/users/kennylbj/orgs","repos_url":"https://api.github.com/users/kennylbj/repos","events_url":"https://api.github.com/users/kennylbj/events{/privacy}","received_events_url":"https://api.github.com/users/kennylbj/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2017-04-07T09:10:48Z","updated_at":"2017-04-17T05:47:02Z","closed_at":"2017-04-17T05:47:02Z","author_association":"CONTRIBUTOR","body":"### Expected behavior\r\nBuilding successful in command line using command \"mvn clean package -DskipTests\".\r\n### Actual behavior\r\nCurrent netty is ok to build, but we will occur build failure if we clone a new netty project.\r\n\r\nFailure while building Netty/Handler module.\r\nHere is the error output:\r\n\r\n[ERROR] Failed to execute goal on project netty-handler: Could not resolve dependencies for project io.netty:netty-handler:jar:4.1.10.Final-SNAPSHOT: Failure to find io.netty:netty-tcnative:jar:osx-x86_64:2.0.1.Final-SNAPSHOT in https://oss.sonatype.org/content/repositories/snapshots was cached in the local repository, resolution will not be reattempted until the update interval of sonatype-nexus-snapshots has elapsed or updates are forced -> [Help 1]\r\n\r\n### Steps to reproduce\r\n1. mkdir tmp & cd tmp  Or   just remove current netty:  rm -rf netty/\r\n2. clone a new netty project:   git clone https://github.com/netty/netty.git\r\n3. cd netty\r\n4. mvn clean package -DskipTests\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\n4.0 && 4.1\r\n### JVM version (e.g. `java -version`)\r\n\r\njava version \"1.8.0_40\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_40-b27)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.40-b25, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\nDarwin kenny.local 16.5.0 Darwin Kernel Version 16.5.0: Fri Mar  3 16:52:33 PST 2017; root:xnu-3789.51.2~3/RELEASE_X86_64 x86_64\r\n\r\n### Maven version\r\nApache Maven 3.3.9\r\n\r\n### Location\r\nChina\r\n\r\n### NOTE\r\nI have built successfully a week ago, Is that possible for the commit https://github.com/netty/netty/commit/077a1988b9c96b5f3e5693a3bb1b2e118992f3a8  ?\r\n\r\nI tried to download the missing jar Manually. However the latest version of netty-handler is 4.1.9.Final and netty-tcnative is 2.0.0 Final\r\n\r\nnetty-handler:    https://mvnrepository.com/artifact/io.netty/netty-handler\r\nnetty-tcnative:    https://mvnrepository.com/artifact/io.netty/netty-tcnative\r\n\r\n\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["292537139","292540703","292541515","292543253","292610239","292685858","294409652"], "labels":[]}