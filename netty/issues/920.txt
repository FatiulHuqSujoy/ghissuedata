{"id":"920", "title":"DefaultChannelHandlerContext uses too much memory", "body":"keepalive, a connection gen 5 `DefaultChannelHandlerContext` object, and the `DefaultChannelHandlerContext` object instance can not be gc.

before a request recieved:

![QQ20130110-16](https://f.cloud.github.com/assets/648508/56184/885081c0-5b0a-11e2-938f-5c6d3cfa5114.png)

after a request recieved:
![QQ20130110-15](https://f.cloud.github.com/assets/648508/56182/66800b7e-5b0a-11e2-8c98-c504a6849f8a.png)

question:
1. DefaultChannelPipeline hold two `DefaultChannelHandlerContext` object, why evry thread hold five
1. evry request gen a `DefaultChannelHandlerContext`? 

this is my program run a moment, the heap dump

```
3062837 instances of class java.lang.Object 
2625047 instances of class java.util.Collections$UnmodifiableSet 
2624969 instances of class java.util.RegularEnumSet 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$1 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$10 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$2 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$3 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$4 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$5 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$6 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$7 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$8 
2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$9 

...
437495 instances of class io.netty.channel.DefaultChannelPipeline 
437495 instances of class io.netty.channel.DefaultChannelPipeline$HeadHandler 

```

a obj, References to this object: `DefaultChannelHandlerContext` 

```

Class 0x50619a828

class io.netty.channel.DefaultChannelHandlerContext$2

Superclass:

class java.lang.Object
Loader Details

ClassLoader:

sun.misc.Launcher$AppClassLoader@0x51cadeb00 (122 bytes)
Signers:

<null>
Protection Domain:

java.security.ProtectionDomain@0x51cadc850 (58 bytes)
Subclasses:

Instance Data Members:

this$0 (L)
Static Data Members:

Instances

Exclude subclasses
Include subclasses
References summary by Type

References summary by type
References to this object:

io.netty.channel.DefaultChannelHandlerContext$2@0x58f8200a8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5452dbb18 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x52e31a3d8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x537ff0d68 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x58f2e7ab8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x53fc8eac0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x589d51758 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x580ae9200 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x531477618 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x574904f88 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x598fa1b70 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5459c9750 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5949953c8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x52a1c8e88 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x56bbab320 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x571bdd478 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x59b9432e0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x553279fa0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a17d8f90 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x59e6951f8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x58f1a73d0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x545d15750 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x522546790 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x558d77408 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x524d39490 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x537903c50 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5556813b0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x55db0ca50 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x525557328 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x56ecf9798 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a2ed9970 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x551f4a308 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x58276fa80 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a7832ed0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x58f5e1958 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x54e558238 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x567545c40 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x58fdbea18 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a08b77c0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x51f059000 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a78f3550 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a1a52778 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x526ad1de0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x529b9f6a8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a3fbb908 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x59240d840 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a3127350 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x582c94c60 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x59e441d48 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a8d84eb8 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x5a07c6750 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x553a859a0 (24 bytes) : ??
io.netty.channel.DefaultChannelHandlerContext$2@0x541a592d8 (24 bytes) : ??

```

is it normal?  @trustin @gresrun  @normanmaurer
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/920","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/920/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/920/comments","events_url":"https://api.github.com/repos/netty/netty/issues/920/events","html_url":"https://github.com/netty/netty/issues/920","id":9836354,"node_id":"MDU6SXNzdWU5ODM2MzU0","number":920,"title":"DefaultChannelHandlerContext uses too much memory","user":{"login":"shijinkui","id":648508,"node_id":"MDQ6VXNlcjY0ODUwOA==","avatar_url":"https://avatars3.githubusercontent.com/u/648508?v=4","gravatar_id":"","url":"https://api.github.com/users/shijinkui","html_url":"https://github.com/shijinkui","followers_url":"https://api.github.com/users/shijinkui/followers","following_url":"https://api.github.com/users/shijinkui/following{/other_user}","gists_url":"https://api.github.com/users/shijinkui/gists{/gist_id}","starred_url":"https://api.github.com/users/shijinkui/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/shijinkui/subscriptions","organizations_url":"https://api.github.com/users/shijinkui/orgs","repos_url":"https://api.github.com/users/shijinkui/repos","events_url":"https://api.github.com/users/shijinkui/events{/privacy}","received_events_url":"https://api.github.com/users/shijinkui/received_events","type":"User","site_admin":false},"labels":[{"id":185729,"node_id":"MDU6TGFiZWwxODU3Mjk=","url":"https://api.github.com/repos/netty/netty/labels/improvement","name":"improvement","color":"0052cc","default":false}],"state":"closed","locked":false,"assignee":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false},"assignees":[{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/38","html_url":"https://github.com/netty/netty/milestone/38","labels_url":"https://api.github.com/repos/netty/netty/milestones/38/labels","id":223478,"node_id":"MDk6TWlsZXN0b25lMjIzNDc4","number":38,"title":"4.0.0.Beta1","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":150,"state":"closed","created_at":"2012-12-04T08:11:26Z","updated_at":"2013-09-05T14:18:37Z","due_on":"2013-02-14T08:00:00Z","closed_at":"2013-04-13T09:29:12Z"},"comments":18,"created_at":"2013-01-10T09:58:18Z","updated_at":"2016-11-04T08:07:28Z","closed_at":"2013-01-15T15:39:10Z","author_association":"NONE","body":"keepalive, a connection gen 5 `DefaultChannelHandlerContext` object, and the `DefaultChannelHandlerContext` object instance can not be gc.\n\nbefore a request recieved:\n\n![QQ20130110-16](https://f.cloud.github.com/assets/648508/56184/885081c0-5b0a-11e2-938f-5c6d3cfa5114.png)\n\nafter a request recieved:\n![QQ20130110-15](https://f.cloud.github.com/assets/648508/56182/66800b7e-5b0a-11e2-8c98-c504a6849f8a.png)\n\nquestion:\n1. DefaultChannelPipeline hold two `DefaultChannelHandlerContext` object, why evry thread hold five\n1. evry request gen a `DefaultChannelHandlerContext`? \n\nthis is my program run a moment, the heap dump\n\n```\n3062837 instances of class java.lang.Object \n2625047 instances of class java.util.Collections$UnmodifiableSet \n2624969 instances of class java.util.RegularEnumSet \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$1 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$10 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$2 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$3 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$4 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$5 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$6 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$7 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$8 \n2624968 instances of class io.netty.channel.DefaultChannelHandlerContext$9 \n\n...\n437495 instances of class io.netty.channel.DefaultChannelPipeline \n437495 instances of class io.netty.channel.DefaultChannelPipeline$HeadHandler \n\n```\n\na obj, References to this object: `DefaultChannelHandlerContext` \n\n```\n\nClass 0x50619a828\n\nclass io.netty.channel.DefaultChannelHandlerContext$2\n\nSuperclass:\n\nclass java.lang.Object\nLoader Details\n\nClassLoader:\n\nsun.misc.Launcher$AppClassLoader@0x51cadeb00 (122 bytes)\nSigners:\n\n<null>\nProtection Domain:\n\njava.security.ProtectionDomain@0x51cadc850 (58 bytes)\nSubclasses:\n\nInstance Data Members:\n\nthis$0 (L)\nStatic Data Members:\n\nInstances\n\nExclude subclasses\nInclude subclasses\nReferences summary by Type\n\nReferences summary by type\nReferences to this object:\n\nio.netty.channel.DefaultChannelHandlerContext$2@0x58f8200a8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5452dbb18 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x52e31a3d8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x537ff0d68 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x58f2e7ab8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x53fc8eac0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x589d51758 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x580ae9200 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x531477618 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x574904f88 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x598fa1b70 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5459c9750 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5949953c8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x52a1c8e88 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x56bbab320 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x571bdd478 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x59b9432e0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x553279fa0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a17d8f90 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x59e6951f8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x58f1a73d0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x545d15750 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x522546790 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x558d77408 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x524d39490 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x537903c50 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5556813b0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x55db0ca50 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x525557328 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x56ecf9798 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a2ed9970 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x551f4a308 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x58276fa80 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a7832ed0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x58f5e1958 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x54e558238 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x567545c40 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x58fdbea18 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a08b77c0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x51f059000 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a78f3550 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a1a52778 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x526ad1de0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x529b9f6a8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a3fbb908 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x59240d840 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a3127350 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x582c94c60 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x59e441d48 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a8d84eb8 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x5a07c6750 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x553a859a0 (24 bytes) : ??\nio.netty.channel.DefaultChannelHandlerContext$2@0x541a592d8 (24 bytes) : ??\n\n```\n\nis it normal?  @trustin @gresrun  @normanmaurer\n","closed_by":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}}", "commentIds":["12104500","12129954","12130759","12219737","12240917","12252637","12253952","12253975","12254511","12256078","12256137","12258768","12272846","12272979","12303011","12484945","12496473","258366923"], "labels":["improvement"]}