{"id":"2352", "title":"4.0.18.Final: Channel.write() can invoke a handler that has been removed from pipeline", "body":"I was trying to write a channel handler that removed itself from the pipeline on channelActive(), but found that sometimes writes would still end up invoking the handler even after it had been removed. 

Here is a simple test program that demonstrates the issue: 

``` java
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.util.concurrent.CountDownLatch;

public class HandlerRemoveRace {

    public static void main(String[] args) throws Exception {
        NioEventLoopGroup eventLoop = new NioEventLoopGroup(1);

        try {
            // Dummy server that just accepts connections, nothing more
            ServerBootstrap server = new ServerBootstrap();
            server.group(eventLoop)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInboundHandlerAdapter() {
                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) { }
                    });
            server.bind(9999).sync();

            // Signals to coordinate threads and force the race condition
            CountDownLatch startWriting = new CountDownLatch(1);
            CountDownLatch doneWriting = new CountDownLatch(1);

            Bootstrap client = new Bootstrap()
                    .group(eventLoop)
                    .channel(NioSocketChannel.class)
                    .handler(new RemoveAfterConnect(startWriting, doneWriting));
            Channel ch = client.connect(new InetSocketAddress(\"127.0.0.1\", 9999)).channel();

            startWriting.await();
            // IO thread is in channelActive, write immediately before IO thread removes handler
            ChannelFuture writeFuture = ch.write(\"BOOM\");
            doneWriting.countDown();
            writeFuture.sync(); // should throw IllegalStateException from the write
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            eventLoop.shutdownGracefully();
        }
    }

    static class RemoveAfterConnect extends ChannelDuplexHandler {
        private final CountDownLatch startWriting, doneWriting;

        RemoveAfterConnect(CountDownLatch startWriting, CountDownLatch doneWriting) {
            this.startWriting = startWriting;
            this.doneWriting = doneWriting;
        }

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.printf(\"write: %s, (removed=%b)\\n\", msg, ctx.isRemoved());
            if (ctx.isRemoved()) {
                promise.setFailure(new IllegalStateException(\"write called after handler removed!\"));
            }
            else promise.setSuccess();
        }

        @Override
        public void channelActive(ChannelHandlerContext ctx) throws Exception {
            System.out.println(\"channel active, signal writers\");
            startWriting.countDown(); // start writing
            doneWriting.await(); // wait for writes
            System.out.println(\"now removing this handler\");
            ctx.pipeline().remove(this);
            // Should find that the write tasks end up invoking this handler
            // because they were instantiated with a ref to this ChannelHandlerContext
            // just before the handler was removed.
        }
    }
}
```

Running this program produces the output:

```
channel active, signal writers
now removing this handler
write: BOOM, (removed=true)
java.lang.IllegalStateException: write called after handler removed!
    at HandlerRemoveRace$RemoveAfterConnect.write(HandlerRemoveRace.java:44)
    at io.netty.channel.DefaultChannelHandlerContext.invokeWrite(DefaultChannelHandlerContext.java:666)
    at io.netty.channel.DefaultChannelHandlerContext.access$2000(DefaultChannelHandlerContext.java:30)
    at io.netty.channel.DefaultChannelHandlerContext$AbstractWriteTask.write(DefaultChannelHandlerContext.java:945)
    at io.netty.channel.DefaultChannelHandlerContext$AbstractWriteTask.run(DefaultChannelHandlerContext.java:934)
    at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:370)
    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:353)
    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:116)
    at java.lang.Thread.run(Thread.java:744)
```

This program uses CountDownLatches to force a race between the main thread and the I/O thread. When the RemoveAfterConnect handler gets the channelActive callback in the I/O thread, it signals the main thread to start writing, then it removes itself from the pipeline after the write was enqueued. 

As far as I can tell, when the main thread calls Channel.write(), a WriteTask is created with a reference to the handler because it's still in the pipeline at that time. So when the I/O thread processes the WriteTask, the removed handler's write() method still gets invoked, even though it's not actually in the pipeline anymore.

I thought this might be a bug because it seems to violate the channel handler thread model, i.e. if a handler was removed in the I/O thread, then it shouldn't be invoked in the I/O thread again.

(Originally reported here: https://groups.google.com/d/msg/netty/ttdksfSHdSg/yY8wfjXF_jsJ)
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2352","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2352/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2352/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2352/events","html_url":"https://github.com/netty/netty/issues/2352","id":30614499,"node_id":"MDU6SXNzdWUzMDYxNDQ5OQ==","number":2352,"title":"4.0.18.Final: Channel.write() can invoke a handler that has been removed from pipeline","user":{"login":"benevans","id":443101,"node_id":"MDQ6VXNlcjQ0MzEwMQ==","avatar_url":"https://avatars2.githubusercontent.com/u/443101?v=4","gravatar_id":"","url":"https://api.github.com/users/benevans","html_url":"https://github.com/benevans","followers_url":"https://api.github.com/users/benevans/followers","following_url":"https://api.github.com/users/benevans/following{/other_user}","gists_url":"https://api.github.com/users/benevans/gists{/gist_id}","starred_url":"https://api.github.com/users/benevans/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/benevans/subscriptions","organizations_url":"https://api.github.com/users/benevans/orgs","repos_url":"https://api.github.com/users/benevans/repos","events_url":"https://api.github.com/users/benevans/events{/privacy}","received_events_url":"https://api.github.com/users/benevans/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/197","html_url":"https://github.com/netty/netty/milestone/197","labels_url":"https://api.github.com/repos/netty/netty/milestones/197/labels","id":3817792,"node_id":"MDk6TWlsZXN0b25lMzgxNzc5Mg==","number":197,"title":"[Netty 5] ChannelPipeline enhancements","description":"Topics related to ChannelPipeline API and sementics.","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":4,"closed_issues":7,"state":"open","created_at":"2018-11-13T13:19:06Z","updated_at":"2019-11-08T12:34:20Z","due_on":null,"closed_at":null},"comments":4,"created_at":"2014-04-01T16:25:13Z","updated_at":"2019-11-08T11:17:20Z","closed_at":"2019-11-08T11:17:11Z","author_association":"CONTRIBUTOR","body":"I was trying to write a channel handler that removed itself from the pipeline on channelActive(), but found that sometimes writes would still end up invoking the handler even after it had been removed. \n\nHere is a simple test program that demonstrates the issue: \n\n``` java\nimport io.netty.bootstrap.Bootstrap;\nimport io.netty.bootstrap.ServerBootstrap;\nimport io.netty.channel.*;\nimport io.netty.channel.nio.NioEventLoopGroup;\nimport io.netty.channel.socket.nio.NioServerSocketChannel;\nimport io.netty.channel.socket.nio.NioSocketChannel;\n\nimport java.net.InetSocketAddress;\nimport java.util.concurrent.CountDownLatch;\n\npublic class HandlerRemoveRace {\n\n    public static void main(String[] args) throws Exception {\n        NioEventLoopGroup eventLoop = new NioEventLoopGroup(1);\n\n        try {\n            // Dummy server that just accepts connections, nothing more\n            ServerBootstrap server = new ServerBootstrap();\n            server.group(eventLoop)\n                    .channel(NioServerSocketChannel.class)\n                    .childHandler(new ChannelInboundHandlerAdapter() {\n                        @Override\n                        public void channelRead(ChannelHandlerContext ctx, Object msg) { }\n                    });\n            server.bind(9999).sync();\n\n            // Signals to coordinate threads and force the race condition\n            CountDownLatch startWriting = new CountDownLatch(1);\n            CountDownLatch doneWriting = new CountDownLatch(1);\n\n            Bootstrap client = new Bootstrap()\n                    .group(eventLoop)\n                    .channel(NioSocketChannel.class)\n                    .handler(new RemoveAfterConnect(startWriting, doneWriting));\n            Channel ch = client.connect(new InetSocketAddress(\"127.0.0.1\", 9999)).channel();\n\n            startWriting.await();\n            // IO thread is in channelActive, write immediately before IO thread removes handler\n            ChannelFuture writeFuture = ch.write(\"BOOM\");\n            doneWriting.countDown();\n            writeFuture.sync(); // should throw IllegalStateException from the write\n        } catch (Exception e) {\n            e.printStackTrace(System.out);\n        } finally {\n            eventLoop.shutdownGracefully();\n        }\n    }\n\n    static class RemoveAfterConnect extends ChannelDuplexHandler {\n        private final CountDownLatch startWriting, doneWriting;\n\n        RemoveAfterConnect(CountDownLatch startWriting, CountDownLatch doneWriting) {\n            this.startWriting = startWriting;\n            this.doneWriting = doneWriting;\n        }\n\n        @Override\n        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {\n            System.out.printf(\"write: %s, (removed=%b)\\n\", msg, ctx.isRemoved());\n            if (ctx.isRemoved()) {\n                promise.setFailure(new IllegalStateException(\"write called after handler removed!\"));\n            }\n            else promise.setSuccess();\n        }\n\n        @Override\n        public void channelActive(ChannelHandlerContext ctx) throws Exception {\n            System.out.println(\"channel active, signal writers\");\n            startWriting.countDown(); // start writing\n            doneWriting.await(); // wait for writes\n            System.out.println(\"now removing this handler\");\n            ctx.pipeline().remove(this);\n            // Should find that the write tasks end up invoking this handler\n            // because they were instantiated with a ref to this ChannelHandlerContext\n            // just before the handler was removed.\n        }\n    }\n}\n```\n\nRunning this program produces the output:\n\n```\nchannel active, signal writers\nnow removing this handler\nwrite: BOOM, (removed=true)\njava.lang.IllegalStateException: write called after handler removed!\n    at HandlerRemoveRace$RemoveAfterConnect.write(HandlerRemoveRace.java:44)\n    at io.netty.channel.DefaultChannelHandlerContext.invokeWrite(DefaultChannelHandlerContext.java:666)\n    at io.netty.channel.DefaultChannelHandlerContext.access$2000(DefaultChannelHandlerContext.java:30)\n    at io.netty.channel.DefaultChannelHandlerContext$AbstractWriteTask.write(DefaultChannelHandlerContext.java:945)\n    at io.netty.channel.DefaultChannelHandlerContext$AbstractWriteTask.run(DefaultChannelHandlerContext.java:934)\n    at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:370)\n    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:353)\n    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:116)\n    at java.lang.Thread.run(Thread.java:744)\n```\n\nThis program uses CountDownLatches to force a race between the main thread and the I/O thread. When the RemoveAfterConnect handler gets the channelActive callback in the I/O thread, it signals the main thread to start writing, then it removes itself from the pipeline after the write was enqueued. \n\nAs far as I can tell, when the main thread calls Channel.write(), a WriteTask is created with a reference to the handler because it's still in the pipeline at that time. So when the I/O thread processes the WriteTask, the removed handler's write() method still gets invoked, even though it's not actually in the pipeline anymore.\n\nI thought this might be a bug because it seems to violate the channel handler thread model, i.e. if a handler was removed in the I/O thread, then it shouldn't be invoked in the I/O thread again.\n\n(Originally reported here: https://groups.google.com/d/msg/netty/ttdksfSHdSg/yY8wfjXF_jsJ)\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["39444110","39695602","39773832","551607589"], "labels":[]}