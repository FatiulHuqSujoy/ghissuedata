{"id":"7131", "title":"io.netty.util.IllegalReferenceCountException: refCnt: 0, increment: 1", "body":"I have following things in pipeline 
```java
            ch.pipeline().addLast(\"decoder\", new HttpRequestDecoder());
            ch.pipeline().addLast(\"encoder\", new HttpResponseEncoder());
            ch.pipeline().addLast(\"aggregator\", new HttpObjectAggregator(1048576));
            ch.pipeline().addLast(new MyServerInboundhandler());
         
```
and in MyServerInboundhandler I have:
```java
@Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    super.channelRead(ctx, msg);
    if (msg instanceof FullHttpRequest) {
      final FullHttpRequest request = (FullHttpRequest) msg;
      String bodyString = request.content().retain().toString(CharsetUtil.UTF_8); //ERROR HERE
     }
}
```

This code produces an error 
```
io.netty.util.IllegalReferenceCountException: refCnt: 0, increment: 1
	at io.netty.buffer.AbstractReferenceCountedByteBuf.retain0(AbstractReferenceCountedByteBuf.java:68)
	at io.netty.buffer.AbstractReferenceCountedByteBuf.retain(AbstractReferenceCountedByteBuf.java:53)
	at io.netty.buffer.CompositeByteBuf.retain(CompositeByteBuf.java:1906)
	at io.netty.buffer.CompositeByteBuf.retain(CompositeByteBuf.java:44)
	at io.netty.buffer.AbstractDerivedByteBuf.retain0(AbstractDerivedByteBuf.java:49)
	at io.netty.buffer.AbstractDerivedByteBuf.retain(AbstractDerivedByteBuf.java:45)
	at io.netty.buffer.AbstractByteBuf.retainedDuplicate(AbstractByteBuf.java:1173)
	at my.class.MyServerInboundhandler.channelRead(MyServerInboundhandler.java:118)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)
	at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)
	at io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:293)
	at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:267)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)
	at io.netty.channel.DefaultChannelPipeline$HeadContext.channelRead(DefaultChannelPipeline.java:1334)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)
	at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:926)
	at io.netty.channel.epoll.AbstractEpollStreamChannel$EpollStreamUnsafe.epollInReady(AbstractEpollStreamChannel.java:1018)
	at io.netty.channel.epoll.EpollEventLoop.processReady(EpollEventLoop.java:394)
	at io.netty.channel.epoll.EpollEventLoop.run(EpollEventLoop.java:299)
	at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:858)
	at io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run(DefaultThreadFactory.java:144)
	at java.lang.Thread.run(Thread.java:745)
```
### Expected behavior
I m expect that all should be okay :)
### Actual behavior
An error occur
### Steps to reproduce

### Minimal yet complete reproducer code (or URL to code)

### Netty version
4.1.8.Final
### JVM version (e.g. `java -version`)
openjdk_8_131
### OS version (e.g. `uname -a`)
Linux4.10.0-32-generic #36~16.04.1-Ubuntu SMP Wed Aug 9 09:19:02 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7131","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7131/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7131/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7131/events","html_url":"https://github.com/netty/netty/issues/7131","id":251421176,"node_id":"MDU6SXNzdWUyNTE0MjExNzY=","number":7131,"title":"io.netty.util.IllegalReferenceCountException: refCnt: 0, increment: 1","user":{"login":"Sammers21","id":16746106,"node_id":"MDQ6VXNlcjE2NzQ2MTA2","avatar_url":"https://avatars2.githubusercontent.com/u/16746106?v=4","gravatar_id":"","url":"https://api.github.com/users/Sammers21","html_url":"https://github.com/Sammers21","followers_url":"https://api.github.com/users/Sammers21/followers","following_url":"https://api.github.com/users/Sammers21/following{/other_user}","gists_url":"https://api.github.com/users/Sammers21/gists{/gist_id}","starred_url":"https://api.github.com/users/Sammers21/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Sammers21/subscriptions","organizations_url":"https://api.github.com/users/Sammers21/orgs","repos_url":"https://api.github.com/users/Sammers21/repos","events_url":"https://api.github.com/users/Sammers21/events{/privacy}","received_events_url":"https://api.github.com/users/Sammers21/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2017-08-19T12:13:25Z","updated_at":"2017-08-19T17:08:56Z","closed_at":"2017-08-19T12:18:24Z","author_association":"CONTRIBUTOR","body":"I have following things in pipeline \r\n```java\r\n            ch.pipeline().addLast(\"decoder\", new HttpRequestDecoder());\r\n            ch.pipeline().addLast(\"encoder\", new HttpResponseEncoder());\r\n            ch.pipeline().addLast(\"aggregator\", new HttpObjectAggregator(1048576));\r\n            ch.pipeline().addLast(new MyServerInboundhandler());\r\n         \r\n```\r\nand in MyServerInboundhandler I have:\r\n```java\r\n@Override\r\n  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {\r\n    super.channelRead(ctx, msg);\r\n    if (msg instanceof FullHttpRequest) {\r\n      final FullHttpRequest request = (FullHttpRequest) msg;\r\n      String bodyString = request.content().retain().toString(CharsetUtil.UTF_8); //ERROR HERE\r\n     }\r\n}\r\n```\r\n\r\nThis code produces an error \r\n```\r\nio.netty.util.IllegalReferenceCountException: refCnt: 0, increment: 1\r\n\tat io.netty.buffer.AbstractReferenceCountedByteBuf.retain0(AbstractReferenceCountedByteBuf.java:68)\r\n\tat io.netty.buffer.AbstractReferenceCountedByteBuf.retain(AbstractReferenceCountedByteBuf.java:53)\r\n\tat io.netty.buffer.CompositeByteBuf.retain(CompositeByteBuf.java:1906)\r\n\tat io.netty.buffer.CompositeByteBuf.retain(CompositeByteBuf.java:44)\r\n\tat io.netty.buffer.AbstractDerivedByteBuf.retain0(AbstractDerivedByteBuf.java:49)\r\n\tat io.netty.buffer.AbstractDerivedByteBuf.retain(AbstractDerivedByteBuf.java:45)\r\n\tat io.netty.buffer.AbstractByteBuf.retainedDuplicate(AbstractByteBuf.java:1173)\r\n\tat my.class.MyServerInboundhandler.channelRead(MyServerInboundhandler.java:118)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)\r\n\tat io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:293)\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:267)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:341)\r\n\tat io.netty.channel.DefaultChannelPipeline$HeadContext.channelRead(DefaultChannelPipeline.java:1334)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:363)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:349)\r\n\tat io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:926)\r\n\tat io.netty.channel.epoll.AbstractEpollStreamChannel$EpollStreamUnsafe.epollInReady(AbstractEpollStreamChannel.java:1018)\r\n\tat io.netty.channel.epoll.EpollEventLoop.processReady(EpollEventLoop.java:394)\r\n\tat io.netty.channel.epoll.EpollEventLoop.run(EpollEventLoop.java:299)\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:858)\r\n\tat io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run(DefaultThreadFactory.java:144)\r\n\tat java.lang.Thread.run(Thread.java:745)\r\n```\r\n### Expected behavior\r\nI m expect that all should be okay :)\r\n### Actual behavior\r\nAn error occur\r\n### Steps to reproduce\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\n4.1.8.Final\r\n### JVM version (e.g. `java -version`)\r\nopenjdk_8_131\r\n### OS version (e.g. `uname -a`)\r\nLinux4.10.0-32-generic #36~16.04.1-Ubuntu SMP Wed Aug 9 09:19:02 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["323519644","323519785","323520137","323535478"], "labels":[]}