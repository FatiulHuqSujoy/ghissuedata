{"id":"2874", "title":"UdtChannel treated several data as one", "body":"Hi, All
 I used UdtChannel(Byte TCP like) to transfer data from client to server, in client side I used udtChannel.writeAndFlush(mydata), but on server side's channelRead method, sometimes received two or three or more data as one data. Is it normal for UdtChannel or I've miss something?

Every client side data:
2014-09-08 12:31:37,545 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcb3082d5, /60.10.97.119:58372 => /182.92.8.112:6112] WRITE(21B)
         +-------------------------------------------------+
         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |
+--------+-------------------------------------------------+----------------+
|00000000| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|
|00000010| 00 00 00 00 00                                  |.....           |
+--------+-------------------------------------------------+----------------+

Server received data:
2014-09-08 12:35:10,394 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcdd4138a, /60.10.97.119:6260 => /182.92.8.112:6112] RECEIVED(84B)
         +-------------------------------------------------+
         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |
+--------+-------------------------------------------------+----------------+
|00000000| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|
|00000010| 00 00 00 00 00 00 00 00 00 00 f7 2f 10 00 50 58 |.........../..PX|
|00000020| 33 57 18 00 00 00 00 00 00 00 00 00 00 00 00 f7 |3W..............|
|00000030| 2f 10 00 50 58 33 57 18 00 00 00 00 00 00 00 00 |/..PX3W.........|
|00000040| 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 00 |...../..PX3W....|
|00000050| 00 00 00 00                                     |....            |
+--------+-------------------------------------------------+----------------+

Sometimes, even lost part of data: one client data is 23 bytes, following are 4 data, summary lenth should be 92 bytes, but it received only 80 bytes, part of last one lost.

2014-09-08 14:56:16,325 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcda10184, /60.10.97.119:9095 => /182.92.8.112:6112] RECEIVED(80B)
         +-------------------------------------------------+
         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |
+--------+-------------------------------------------------+----------------+
|00000000| 00 17 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 |......../..PX3W.|
|00000010| 00 00 00 00 00 00 00 00 17 00 00 00 00 00 f7 2f |.............../|
|00000020| 10 00 50 58 33 57 18 00 00 00 00 00 00 00 00 17 |..PX3W..........|
|00000030| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|
|00000040| 00 00 00 00 00 00 17 00 00 00 00 00 f7 2f 10 00 |............./..|
+--------+-------------------------------------------------+----------------+

Thanks in advance.

Netty version: 4.0.23.Final
JDK: oracle 1.8.20
Client on windows
Server on linux(centOS)
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2874","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2874/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2874/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2874/events","html_url":"https://github.com/netty/netty/issues/2874","id":42163867,"node_id":"MDU6SXNzdWU0MjE2Mzg2Nw==","number":2874,"title":"UdtChannel treated several data as one","user":{"login":"yyq2013","id":4711455,"node_id":"MDQ6VXNlcjQ3MTE0NTU=","avatar_url":"https://avatars1.githubusercontent.com/u/4711455?v=4","gravatar_id":"","url":"https://api.github.com/users/yyq2013","html_url":"https://github.com/yyq2013","followers_url":"https://api.github.com/users/yyq2013/followers","following_url":"https://api.github.com/users/yyq2013/following{/other_user}","gists_url":"https://api.github.com/users/yyq2013/gists{/gist_id}","starred_url":"https://api.github.com/users/yyq2013/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yyq2013/subscriptions","organizations_url":"https://api.github.com/users/yyq2013/orgs","repos_url":"https://api.github.com/users/yyq2013/repos","events_url":"https://api.github.com/users/yyq2013/events{/privacy}","received_events_url":"https://api.github.com/users/yyq2013/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2014-09-08T04:36:39Z","updated_at":"2014-09-19T13:48:52Z","closed_at":"2014-09-19T13:48:52Z","author_association":"NONE","body":"Hi, All\n I used UdtChannel(Byte TCP like) to transfer data from client to server, in client side I used udtChannel.writeAndFlush(mydata), but on server side's channelRead method, sometimes received two or three or more data as one data. Is it normal for UdtChannel or I've miss something?\n\nEvery client side data:\n2014-09-08 12:31:37,545 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcb3082d5, /60.10.97.119:58372 => /182.92.8.112:6112] WRITE(21B)\n         +-------------------------------------------------+\n         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |\n+--------+-------------------------------------------------+----------------+\n|00000000| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|\n|00000010| 00 00 00 00 00                                  |.....           |\n+--------+-------------------------------------------------+----------------+\n\nServer received data:\n2014-09-08 12:35:10,394 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcdd4138a, /60.10.97.119:6260 => /182.92.8.112:6112] RECEIVED(84B)\n         +-------------------------------------------------+\n         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |\n+--------+-------------------------------------------------+----------------+\n|00000000| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|\n|00000010| 00 00 00 00 00 00 00 00 00 00 f7 2f 10 00 50 58 |.........../..PX|\n|00000020| 33 57 18 00 00 00 00 00 00 00 00 00 00 00 00 f7 |3W..............|\n|00000030| 2f 10 00 50 58 33 57 18 00 00 00 00 00 00 00 00 |/..PX3W.........|\n|00000040| 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 00 |...../..PX3W....|\n|00000050| 00 00 00 00                                     |....            |\n+--------+-------------------------------------------------+----------------+\n\nSometimes, even lost part of data: one client data is 23 bytes, following are 4 data, summary lenth should be 92 bytes, but it received only 80 bytes, part of last one lost.\n\n2014-09-08 14:56:16,325 [io.netty.handler.logging.LoggingHandler]-[INFO] [id: 0xcda10184, /60.10.97.119:9095 => /182.92.8.112:6112] RECEIVED(80B)\n         +-------------------------------------------------+\n         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |\n+--------+-------------------------------------------------+----------------+\n|00000000| 00 17 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 |......../..PX3W.|\n|00000010| 00 00 00 00 00 00 00 00 17 00 00 00 00 00 f7 2f |.............../|\n|00000020| 10 00 50 58 33 57 18 00 00 00 00 00 00 00 00 17 |..PX3W..........|\n|00000030| 00 00 00 00 00 f7 2f 10 00 50 58 33 57 18 00 00 |....../..PX3W...|\n|00000040| 00 00 00 00 00 00 17 00 00 00 00 00 f7 2f 10 00 |............./..|\n+--------+-------------------------------------------------+----------------+\n\nThanks in advance.\n\nNetty version: 4.0.23.Final\nJDK: oracle 1.8.20\nClient on windows\nServer on linux(centOS)\n","closed_by":{"login":"yyq2013","id":4711455,"node_id":"MDQ6VXNlcjQ3MTE0NTU=","avatar_url":"https://avatars1.githubusercontent.com/u/4711455?v=4","gravatar_id":"","url":"https://api.github.com/users/yyq2013","html_url":"https://github.com/yyq2013","followers_url":"https://api.github.com/users/yyq2013/followers","following_url":"https://api.github.com/users/yyq2013/following{/other_user}","gists_url":"https://api.github.com/users/yyq2013/gists{/gist_id}","starred_url":"https://api.github.com/users/yyq2013/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yyq2013/subscriptions","organizations_url":"https://api.github.com/users/yyq2013/orgs","repos_url":"https://api.github.com/users/yyq2013/repos","events_url":"https://api.github.com/users/yyq2013/events{/privacy}","received_events_url":"https://api.github.com/users/yyq2013/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":[]}