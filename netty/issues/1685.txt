{"id":"1685", "title":"io.netty.util.IllegalReferenceCountException with HttpPostRequestEncoder using  DefaultHttpDataFactory(false)", "body":"This is on netty-4.0.5.Final with the following patch to HttpUploadClient.java in the example module. 

See: https://gist.github.com/manjuraj/26ad9ebba16c70d27dc0

On running HttpUploadClient, I get a illegal reference count exception. Here is the output:

```
    $ java -cp $(cat classpath.txt) io.netty.example.http.upload.HttpUploadClient
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClient main
    INFO: Posting to http://localhost:8080. Using file /var/folders/p8/y6f46gkn0sgdf5y_3dhvk6140000gn/T/upload2897631490645916144.txt
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: STATUS: 200 OK
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: VERSION: HTTP/1.1
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: HEADER: Content-Type = text/plain; charset=UTF-8
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: HEADER: Set-Cookie = another-cookie=bar
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: HEADER: Set-Cookie = my-cookie=foo
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: HEADER: Transfer-Encoding = chunked
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: CHUNKED CONTENT {
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: WELCOME TO THE WILD WILD WEB SERVER
    ===================================
    VERSION: HTTP/1.1
    REQUEST_URI: http://localhost:8080/formget?getform=GET&info=first%20value&secondinfo=secondvalue%20%EF%BF%BD%EF%BF%BD%EF%BF%BD%26&thirdinfo=third%20value%0D%0Atest%20second%20line%0D%0A%0D%0Anew%20line%0D%0A&Send=Send



    HEADER: Host=localhost
    HEADER: Connection=close
    HEADER: Accept-Encoding=gzip,deflate
    HEADER: Accept-Charset=ISO-8859-1,utf-8;q=0.7,*;q=0.7
    HEADER: Accept-Language=fr
    HEADER: Referer=http://localhost:8080/formpost
    HEADER: User-Agent=Netty Simple Http Client side
    HEADER: Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    HEADER: Cookie=my-cookie=foo; another-cookie=bar


    COOKIE: another-cookie=bar
    COOKIE: my-cookie=foo


    URI: getform=GET
    URI: info=first value
    URI: secondinfo=secondvalue ���&
    URI: thirdinfo=third value
    test second line

    new line

    URI: Send=Send


    No Body to decode

    END OF GET CONTENT

    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: WELCOME TO THE WILD WILD WEB SERVER
    ===================================
    VERSION: HTTP/1.1
    REQUEST_URI: http://localhost:8080/formget?getform=GET&info=first%20value&secondinfo=secondvalue%20%EF%BF%BD%EF%BF%BD%EF%BF%BD%26&thirdinfo=third%20value%0D%0Atest%20second%20line%0D%0A%0D%0Anew%20line%0D%0A&Send=Send



    HEADER: Host=localhost
    HEADER: Connection=close
    HEADER: Accept-Encoding=gzip,deflate
    HEADER: Accept-Charset=ISO-8859-1,utf-8;q=0.7,*;q=0.7
    HEADER: Accept-Language=fr
    HEADER: Referer=http://localhost:8080/formpost
    HEADER: User-Agent=Netty Simple Http Client side
    HEADER: Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    HEADER: Cookie=my-cookie=foo; another-cookie=bar


    COOKIE: another-cookie=bar
    COOKIE: my-cookie=foo


    URI: getform=GET
    URI: info=first value
    URI: secondinfo=secondvalue ���&
    URI: thirdinfo=third value
    test second line

    new line

    URI: Send=Send


    No Body to decode

    END OF GET CONTENT

    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: 
    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0
    INFO: } END OF CHUNKED CONTENT
    io.netty.util.IllegalReferenceCountException: refCnt: 0
        at io.netty.buffer.AbstractByteBuf.ensureAccessible(AbstractByteBuf.java:1171)
        at io.netty.buffer.UnpooledHeapByteBuf.capacity(UnpooledHeapByteBuf.java:101)
        at io.netty.buffer.SlicedByteBuf.<init>(SlicedByteBuf.java:41)
        at io.netty.buffer.AbstractByteBuf.slice(AbstractByteBuf.java:924)
        at io.netty.handler.codec.http.multipart.AbstractMemoryHttpData.getChunk(AbstractMemoryHttpData.java:206)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.encodeNextChunkUrlEncoded(HttpPostRequestEncoder.java:876)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.nextChunk(HttpPostRequestEncoder.java:987)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:950)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:50)
        at io.netty.handler.stream.ChunkedWriteHandler.doFlush(ChunkedWriteHandler.java:225)
        at io.netty.handler.stream.ChunkedWriteHandler.flush(ChunkedWriteHandler.java:150)
        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush(DefaultChannelHandlerContext.java:764)
        at io.netty.channel.DefaultChannelHandlerContext.access$1700(DefaultChannelHandlerContext.java:28)
        at io.netty.channel.DefaultChannelHandlerContext$18.run(DefaultChannelHandlerContext.java:750)
        at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:354)
        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:366)
        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)
        at java.lang.Thread.run(Thread.java:722)
    io.netty.util.IllegalReferenceCountException: refCnt: 0
        at io.netty.buffer.AbstractByteBuf.ensureAccessible(AbstractByteBuf.java:1171)
        at io.netty.buffer.UnpooledHeapByteBuf.capacity(UnpooledHeapByteBuf.java:101)
        at io.netty.buffer.SlicedByteBuf.<init>(SlicedByteBuf.java:41)
        at io.netty.buffer.AbstractByteBuf.slice(AbstractByteBuf.java:924)
        at io.netty.handler.codec.http.multipart.AbstractMemoryHttpData.getChunk(AbstractMemoryHttpData.java:206)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.encodeNextChunkMultipart(HttpPostRequestEncoder.java:807)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.nextChunk(HttpPostRequestEncoder.java:982)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:950)
        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:50)
        at io.netty.handler.stream.ChunkedWriteHandler.doFlush(ChunkedWriteHandler.java:225)
        at io.netty.handler.stream.ChunkedWriteHandler.flush(ChunkedWriteHandler.java:150)
        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush(DefaultChannelHandlerContext.java:764)
        at io.netty.channel.DefaultChannelHandlerContext.access$1700(DefaultChannelHandlerContext.java:28)
        at io.netty.channel.DefaultChannelHandlerContext$18.run(DefaultChannelHandlerContext.java:750)
        at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:354)
        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:366)
        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)
        at java.lang.Thread.run(Thread.java:722)
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1685","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1685/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1685/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1685/events","html_url":"https://github.com/netty/netty/issues/1685","id":17485675,"node_id":"MDU6SXNzdWUxNzQ4NTY3NQ==","number":1685,"title":"io.netty.util.IllegalReferenceCountException with HttpPostRequestEncoder using  DefaultHttpDataFactory(false)","user":{"login":"manjuraj","id":349696,"node_id":"MDQ6VXNlcjM0OTY5Ng==","avatar_url":"https://avatars1.githubusercontent.com/u/349696?v=4","gravatar_id":"","url":"https://api.github.com/users/manjuraj","html_url":"https://github.com/manjuraj","followers_url":"https://api.github.com/users/manjuraj/followers","following_url":"https://api.github.com/users/manjuraj/following{/other_user}","gists_url":"https://api.github.com/users/manjuraj/gists{/gist_id}","starred_url":"https://api.github.com/users/manjuraj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/manjuraj/subscriptions","organizations_url":"https://api.github.com/users/manjuraj/orgs","repos_url":"https://api.github.com/users/manjuraj/repos","events_url":"https://api.github.com/users/manjuraj/events{/privacy}","received_events_url":"https://api.github.com/users/manjuraj/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/75","html_url":"https://github.com/netty/netty/milestone/75","labels_url":"https://api.github.com/repos/netty/netty/milestones/75/labels","id":460036,"node_id":"MDk6TWlsZXN0b25lNDYwMDM2","number":75,"title":"4.0.12.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":15,"state":"closed","created_at":"2013-10-21T07:29:28Z","updated_at":"2013-11-08T02:10:52Z","due_on":null,"closed_at":"2013-11-08T02:10:52Z"},"comments":1,"created_at":"2013-08-01T00:52:55Z","updated_at":"2013-11-07T10:28:37Z","closed_at":"2013-11-07T10:28:37Z","author_association":"NONE","body":"This is on netty-4.0.5.Final with the following patch to HttpUploadClient.java in the example module. \n\nSee: https://gist.github.com/manjuraj/26ad9ebba16c70d27dc0\n\nOn running HttpUploadClient, I get a illegal reference count exception. Here is the output:\n\n```\n    $ java -cp $(cat classpath.txt) io.netty.example.http.upload.HttpUploadClient\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClient main\n    INFO: Posting to http://localhost:8080. Using file /var/folders/p8/y6f46gkn0sgdf5y_3dhvk6140000gn/T/upload2897631490645916144.txt\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: STATUS: 200 OK\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: VERSION: HTTP/1.1\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: HEADER: Content-Type = text/plain; charset=UTF-8\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: HEADER: Set-Cookie = another-cookie=bar\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: HEADER: Set-Cookie = my-cookie=foo\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: HEADER: Transfer-Encoding = chunked\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: CHUNKED CONTENT {\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: WELCOME TO THE WILD WILD WEB SERVER\n    ===================================\n    VERSION: HTTP/1.1\n    REQUEST_URI: http://localhost:8080/formget?getform=GET&info=first%20value&secondinfo=secondvalue%20%EF%BF%BD%EF%BF%BD%EF%BF%BD%26&thirdinfo=third%20value%0D%0Atest%20second%20line%0D%0A%0D%0Anew%20line%0D%0A&Send=Send\n\n\n\n    HEADER: Host=localhost\n    HEADER: Connection=close\n    HEADER: Accept-Encoding=gzip,deflate\n    HEADER: Accept-Charset=ISO-8859-1,utf-8;q=0.7,*;q=0.7\n    HEADER: Accept-Language=fr\n    HEADER: Referer=http://localhost:8080/formpost\n    HEADER: User-Agent=Netty Simple Http Client side\n    HEADER: Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n    HEADER: Cookie=my-cookie=foo; another-cookie=bar\n\n\n    COOKIE: another-cookie=bar\n    COOKIE: my-cookie=foo\n\n\n    URI: getform=GET\n    URI: info=first value\n    URI: secondinfo=secondvalue ���&\n    URI: thirdinfo=third value\n    test second line\n\n    new line\n\n    URI: Send=Send\n\n\n    No Body to decode\n\n    END OF GET CONTENT\n\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: WELCOME TO THE WILD WILD WEB SERVER\n    ===================================\n    VERSION: HTTP/1.1\n    REQUEST_URI: http://localhost:8080/formget?getform=GET&info=first%20value&secondinfo=secondvalue%20%EF%BF%BD%EF%BF%BD%EF%BF%BD%26&thirdinfo=third%20value%0D%0Atest%20second%20line%0D%0A%0D%0Anew%20line%0D%0A&Send=Send\n\n\n\n    HEADER: Host=localhost\n    HEADER: Connection=close\n    HEADER: Accept-Encoding=gzip,deflate\n    HEADER: Accept-Charset=ISO-8859-1,utf-8;q=0.7,*;q=0.7\n    HEADER: Accept-Language=fr\n    HEADER: Referer=http://localhost:8080/formpost\n    HEADER: User-Agent=Netty Simple Http Client side\n    HEADER: Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n    HEADER: Cookie=my-cookie=foo; another-cookie=bar\n\n\n    COOKIE: another-cookie=bar\n    COOKIE: my-cookie=foo\n\n\n    URI: getform=GET\n    URI: info=first value\n    URI: secondinfo=secondvalue ���&\n    URI: thirdinfo=third value\n    test second line\n\n    new line\n\n    URI: Send=Send\n\n\n    No Body to decode\n\n    END OF GET CONTENT\n\n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: \n    Jul 31, 2013 5:46:38 PM io.netty.example.http.upload.HttpUploadClientHandler channelRead0\n    INFO: } END OF CHUNKED CONTENT\n    io.netty.util.IllegalReferenceCountException: refCnt: 0\n        at io.netty.buffer.AbstractByteBuf.ensureAccessible(AbstractByteBuf.java:1171)\n        at io.netty.buffer.UnpooledHeapByteBuf.capacity(UnpooledHeapByteBuf.java:101)\n        at io.netty.buffer.SlicedByteBuf.<init>(SlicedByteBuf.java:41)\n        at io.netty.buffer.AbstractByteBuf.slice(AbstractByteBuf.java:924)\n        at io.netty.handler.codec.http.multipart.AbstractMemoryHttpData.getChunk(AbstractMemoryHttpData.java:206)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.encodeNextChunkUrlEncoded(HttpPostRequestEncoder.java:876)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.nextChunk(HttpPostRequestEncoder.java:987)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:950)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:50)\n        at io.netty.handler.stream.ChunkedWriteHandler.doFlush(ChunkedWriteHandler.java:225)\n        at io.netty.handler.stream.ChunkedWriteHandler.flush(ChunkedWriteHandler.java:150)\n        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush(DefaultChannelHandlerContext.java:764)\n        at io.netty.channel.DefaultChannelHandlerContext.access$1700(DefaultChannelHandlerContext.java:28)\n        at io.netty.channel.DefaultChannelHandlerContext$18.run(DefaultChannelHandlerContext.java:750)\n        at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:354)\n        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:366)\n        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)\n        at java.lang.Thread.run(Thread.java:722)\n    io.netty.util.IllegalReferenceCountException: refCnt: 0\n        at io.netty.buffer.AbstractByteBuf.ensureAccessible(AbstractByteBuf.java:1171)\n        at io.netty.buffer.UnpooledHeapByteBuf.capacity(UnpooledHeapByteBuf.java:101)\n        at io.netty.buffer.SlicedByteBuf.<init>(SlicedByteBuf.java:41)\n        at io.netty.buffer.AbstractByteBuf.slice(AbstractByteBuf.java:924)\n        at io.netty.handler.codec.http.multipart.AbstractMemoryHttpData.getChunk(AbstractMemoryHttpData.java:206)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.encodeNextChunkMultipart(HttpPostRequestEncoder.java:807)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.nextChunk(HttpPostRequestEncoder.java:982)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:950)\n        at io.netty.handler.codec.http.multipart.HttpPostRequestEncoder.readChunk(HttpPostRequestEncoder.java:50)\n        at io.netty.handler.stream.ChunkedWriteHandler.doFlush(ChunkedWriteHandler.java:225)\n        at io.netty.handler.stream.ChunkedWriteHandler.flush(ChunkedWriteHandler.java:150)\n        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush(DefaultChannelHandlerContext.java:764)\n        at io.netty.channel.DefaultChannelHandlerContext.access$1700(DefaultChannelHandlerContext.java:28)\n        at io.netty.channel.DefaultChannelHandlerContext$18.run(DefaultChannelHandlerContext.java:750)\n        at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:354)\n        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:366)\n        at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:101)\n        at java.lang.Thread.run(Thread.java:722)\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["27951916"], "labels":[]}