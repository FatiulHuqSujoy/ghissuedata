{"id":"9297", "title":"Streaming large response affects other requests", "body":"### Expected behavior
Hi, I am using Netty as an HTTP Server. Since the last step in our pipeline may take a while, we have an configured an `EventLoopGroup` which is used on the call to `pipeline.addLast(group, ...)`. We are expecting any large responses (10 MBs or more) that are written as HTTP Chunks not to interfere with other requests, but that is not what we are seeing. 
### Actual behavior
After running for a short period of time (the below code) the script that repeatedly pings the server will hang until the large response is completed. It appears that any new channels that are directed to the same IO Thread are not handled until the large response is completed.

### Steps to reproduce

1. Compile and run the server code below
2. Run ping.sh and the server will respond
3. Run the following cURL ``` curl \"http://localhost:8080/chunking\">/dev/null``` The ping requests will eventually hang.
### Minimal yet complete reproducer code (or URL to code)
```

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.DefaultLastHttpContent;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpServerExpectContinueHandler;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

import java.io.IOException;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaderValues.CLOSE;
import static io.netty.handler.codec.http.HttpHeaderValues.KEEP_ALIVE;
import static io.netty.handler.codec.http.HttpHeaderValues.TEXT_PLAIN;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

public class HangingServer {

    public static void main(String[] args) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.option(ChannelOption.SO_BACKLOG, 1024);
            EventExecutorGroup group = new DefaultEventExecutorGroup(2);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new HangingServerInitializer(group));

            Channel ch = b.bind(8080).sync().channel();

            ch.closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    static class HangingServerInitializer extends ChannelInitializer<SocketChannel> {

        private final EventExecutorGroup group;

        HangingServerInitializer(EventExecutorGroup group) {
            this.group = group;
        }

        @Override
        public void initChannel(SocketChannel ch) {
            ChannelPipeline p = ch.pipeline();

            p.addLast(new HttpServerCodec());
            p.addLast(new HttpServerExpectContinueHandler());
            p.addLast(group, new HangingServerHandler());
        }
    }

    static class HangingServerHandler extends SimpleChannelInboundHandler<HttpObject> {
        private static final byte[] CONTENT = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd'};

        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) {
            ctx.flush();
        }

        @Override
        public void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws IOException, InterruptedException {
            if (msg instanceof HttpRequest) {
                HttpRequest req = (HttpRequest) msg;

                boolean keepAlive = HttpUtil.isKeepAlive(req);

                if (req.uri().contains(\"chunk\")) {
                    HttpResponse response = new DefaultHttpResponse(req.protocolVersion(), OK);
                    response.headers()
                            .set(CONTENT_TYPE, TEXT_PLAIN)
                            .set(HttpHeaderNames.TRANSFER_ENCODING, HttpHeaderValues.CHUNKED);

                    maybePopulateKeepAlive(keepAlive, req, response);

                    ctx.write(response);

                    // NOTE this is intentionally big to ensure the problem is shown
                    for (int i = 0; i < 500_000_000; i++) {
                        ctx.write(new DefaultHttpContent(Unpooled.copiedBuffer(i + \" hello there\", CharsetUtil.US_ASCII)));
                        if (i % 1000 == 0) {
                            ctx.flush();
                        }
                    }
                    ChannelFuture f = ctx.write(LastHttpContent.EMPTY_LAST_CONTENT);
                    if (!keepAlive) {
                        f.addListener(ChannelFutureListener.CLOSE);
                    }
                } else {

                    FullHttpResponse response = new DefaultFullHttpResponse(req.protocolVersion(), OK,
                            Unpooled.wrappedBuffer(CONTENT));
                    response.headers()
                            .set(CONTENT_TYPE, TEXT_PLAIN)
                            .setInt(CONTENT_LENGTH, response.content().readableBytes());

                    maybePopulateKeepAlive(keepAlive, req, response);

                    ChannelFuture f = ctx.write(response);

                    if (!keepAlive) {
                        f.addListener(ChannelFutureListener.CLOSE);
                    }
                }
            }
        }

        private static void maybePopulateKeepAlive(boolean keepAlive, HttpRequest req , HttpResponse response){
            if (keepAlive) {
                if (!req.protocolVersion().isKeepAliveDefault()) {
                    response.headers().set(CONNECTION, KEEP_ALIVE);
                }
            } else {
                // Tell the client we're going to close the connection.
                response.headers().set(CONNECTION, CLOSE);
            }
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            cause.printStackTrace();
            ctx.close();
        }
    }
}
```
ping.sh
```
#!/bin/bash

while :
do
        echo \"Before $(date)\"
        curl -v \"http://localhost:8080/ping\"
        echo \"After $(date)\"
        sleep 1
done
```
Long running response
```curl \"http://localhost:8080/chunking\">/dev/null```
### Netty version
4.1.36.Final
### JVM version (e.g. `java -version`)
java version \"9.0.1\"
Java(TM) SE Runtime Environment (build 9.0.1+11)
Java HotSpot(TM) 64-Bit Server VM (build 9.0.1+11, mixed mode)

### OS version (e.g. `uname -a`)
uname -a
Linux dave-XPS-15-9560 4.15.0-50-generic #54~16.04.1-Ubuntu SMP Wed May 8 15:55:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/9297","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/9297/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/9297/comments","events_url":"https://api.github.com/repos/netty/netty/issues/9297/events","html_url":"https://github.com/netty/netty/issues/9297","id":462065839,"node_id":"MDU6SXNzdWU0NjIwNjU4Mzk=","number":9297,"title":"Streaming large response affects other requests","user":{"login":"stampy88","id":333299,"node_id":"MDQ6VXNlcjMzMzI5OQ==","avatar_url":"https://avatars2.githubusercontent.com/u/333299?v=4","gravatar_id":"","url":"https://api.github.com/users/stampy88","html_url":"https://github.com/stampy88","followers_url":"https://api.github.com/users/stampy88/followers","following_url":"https://api.github.com/users/stampy88/following{/other_user}","gists_url":"https://api.github.com/users/stampy88/gists{/gist_id}","starred_url":"https://api.github.com/users/stampy88/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/stampy88/subscriptions","organizations_url":"https://api.github.com/users/stampy88/orgs","repos_url":"https://api.github.com/users/stampy88/repos","events_url":"https://api.github.com/users/stampy88/events{/privacy}","received_events_url":"https://api.github.com/users/stampy88/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2019-06-28T14:36:25Z","updated_at":"2019-07-05T15:53:33Z","closed_at":"2019-07-05T15:53:32Z","author_association":"NONE","body":"### Expected behavior\r\nHi, I am using Netty as an HTTP Server. Since the last step in our pipeline may take a while, we have an configured an `EventLoopGroup` which is used on the call to `pipeline.addLast(group, ...)`. We are expecting any large responses (10 MBs or more) that are written as HTTP Chunks not to interfere with other requests, but that is not what we are seeing. \r\n### Actual behavior\r\nAfter running for a short period of time (the below code) the script that repeatedly pings the server will hang until the large response is completed. It appears that any new channels that are directed to the same IO Thread are not handled until the large response is completed.\r\n\r\n### Steps to reproduce\r\n\r\n1. Compile and run the server code below\r\n2. Run ping.sh and the server will respond\r\n3. Run the following cURL ``` curl \"http://localhost:8080/chunking\">/dev/null``` The ping requests will eventually hang.\r\n### Minimal yet complete reproducer code (or URL to code)\r\n```\r\n\r\nimport io.netty.bootstrap.ServerBootstrap;\r\nimport io.netty.buffer.Unpooled;\r\nimport io.netty.channel.Channel;\r\nimport io.netty.channel.ChannelFuture;\r\nimport io.netty.channel.ChannelFutureListener;\r\nimport io.netty.channel.ChannelHandlerContext;\r\nimport io.netty.channel.ChannelInitializer;\r\nimport io.netty.channel.ChannelOption;\r\nimport io.netty.channel.ChannelPipeline;\r\nimport io.netty.channel.EventLoopGroup;\r\nimport io.netty.channel.SimpleChannelInboundHandler;\r\nimport io.netty.channel.nio.NioEventLoopGroup;\r\nimport io.netty.channel.socket.SocketChannel;\r\nimport io.netty.channel.socket.nio.NioServerSocketChannel;\r\nimport io.netty.handler.codec.http.DefaultFullHttpResponse;\r\nimport io.netty.handler.codec.http.DefaultHttpContent;\r\nimport io.netty.handler.codec.http.DefaultHttpResponse;\r\nimport io.netty.handler.codec.http.DefaultLastHttpContent;\r\nimport io.netty.handler.codec.http.FullHttpResponse;\r\nimport io.netty.handler.codec.http.HttpHeaderNames;\r\nimport io.netty.handler.codec.http.HttpHeaderValues;\r\nimport io.netty.handler.codec.http.HttpObject;\r\nimport io.netty.handler.codec.http.HttpRequest;\r\nimport io.netty.handler.codec.http.HttpResponse;\r\nimport io.netty.handler.codec.http.HttpServerCodec;\r\nimport io.netty.handler.codec.http.HttpServerExpectContinueHandler;\r\nimport io.netty.handler.codec.http.HttpUtil;\r\nimport io.netty.handler.codec.http.LastHttpContent;\r\nimport io.netty.handler.logging.LogLevel;\r\nimport io.netty.handler.logging.LoggingHandler;\r\nimport io.netty.util.CharsetUtil;\r\nimport io.netty.util.concurrent.DefaultEventExecutorGroup;\r\nimport io.netty.util.concurrent.EventExecutorGroup;\r\n\r\nimport java.io.IOException;\r\n\r\nimport static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;\r\nimport static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;\r\nimport static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;\r\nimport static io.netty.handler.codec.http.HttpHeaderValues.CLOSE;\r\nimport static io.netty.handler.codec.http.HttpHeaderValues.KEEP_ALIVE;\r\nimport static io.netty.handler.codec.http.HttpHeaderValues.TEXT_PLAIN;\r\nimport static io.netty.handler.codec.http.HttpResponseStatus.OK;\r\n\r\npublic class HangingServer {\r\n\r\n    public static void main(String[] args) throws Exception {\r\n        EventLoopGroup bossGroup = new NioEventLoopGroup(1);\r\n        EventLoopGroup workerGroup = new NioEventLoopGroup();\r\n        try {\r\n            ServerBootstrap b = new ServerBootstrap();\r\n            b.option(ChannelOption.SO_BACKLOG, 1024);\r\n            EventExecutorGroup group = new DefaultEventExecutorGroup(2);\r\n            b.group(bossGroup, workerGroup)\r\n                    .channel(NioServerSocketChannel.class)\r\n                    .handler(new LoggingHandler(LogLevel.INFO))\r\n                    .childHandler(new HangingServerInitializer(group));\r\n\r\n            Channel ch = b.bind(8080).sync().channel();\r\n\r\n            ch.closeFuture().sync();\r\n        } finally {\r\n            bossGroup.shutdownGracefully();\r\n            workerGroup.shutdownGracefully();\r\n        }\r\n    }\r\n\r\n    static class HangingServerInitializer extends ChannelInitializer<SocketChannel> {\r\n\r\n        private final EventExecutorGroup group;\r\n\r\n        HangingServerInitializer(EventExecutorGroup group) {\r\n            this.group = group;\r\n        }\r\n\r\n        @Override\r\n        public void initChannel(SocketChannel ch) {\r\n            ChannelPipeline p = ch.pipeline();\r\n\r\n            p.addLast(new HttpServerCodec());\r\n            p.addLast(new HttpServerExpectContinueHandler());\r\n            p.addLast(group, new HangingServerHandler());\r\n        }\r\n    }\r\n\r\n    static class HangingServerHandler extends SimpleChannelInboundHandler<HttpObject> {\r\n        private static final byte[] CONTENT = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd'};\r\n\r\n        @Override\r\n        public void channelReadComplete(ChannelHandlerContext ctx) {\r\n            ctx.flush();\r\n        }\r\n\r\n        @Override\r\n        public void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws IOException, InterruptedException {\r\n            if (msg instanceof HttpRequest) {\r\n                HttpRequest req = (HttpRequest) msg;\r\n\r\n                boolean keepAlive = HttpUtil.isKeepAlive(req);\r\n\r\n                if (req.uri().contains(\"chunk\")) {\r\n                    HttpResponse response = new DefaultHttpResponse(req.protocolVersion(), OK);\r\n                    response.headers()\r\n                            .set(CONTENT_TYPE, TEXT_PLAIN)\r\n                            .set(HttpHeaderNames.TRANSFER_ENCODING, HttpHeaderValues.CHUNKED);\r\n\r\n                    maybePopulateKeepAlive(keepAlive, req, response);\r\n\r\n                    ctx.write(response);\r\n\r\n                    // NOTE this is intentionally big to ensure the problem is shown\r\n                    for (int i = 0; i < 500_000_000; i++) {\r\n                        ctx.write(new DefaultHttpContent(Unpooled.copiedBuffer(i + \" hello there\", CharsetUtil.US_ASCII)));\r\n                        if (i % 1000 == 0) {\r\n                            ctx.flush();\r\n                        }\r\n                    }\r\n                    ChannelFuture f = ctx.write(LastHttpContent.EMPTY_LAST_CONTENT);\r\n                    if (!keepAlive) {\r\n                        f.addListener(ChannelFutureListener.CLOSE);\r\n                    }\r\n                } else {\r\n\r\n                    FullHttpResponse response = new DefaultFullHttpResponse(req.protocolVersion(), OK,\r\n                            Unpooled.wrappedBuffer(CONTENT));\r\n                    response.headers()\r\n                            .set(CONTENT_TYPE, TEXT_PLAIN)\r\n                            .setInt(CONTENT_LENGTH, response.content().readableBytes());\r\n\r\n                    maybePopulateKeepAlive(keepAlive, req, response);\r\n\r\n                    ChannelFuture f = ctx.write(response);\r\n\r\n                    if (!keepAlive) {\r\n                        f.addListener(ChannelFutureListener.CLOSE);\r\n                    }\r\n                }\r\n            }\r\n        }\r\n\r\n        private static void maybePopulateKeepAlive(boolean keepAlive, HttpRequest req , HttpResponse response){\r\n            if (keepAlive) {\r\n                if (!req.protocolVersion().isKeepAliveDefault()) {\r\n                    response.headers().set(CONNECTION, KEEP_ALIVE);\r\n                }\r\n            } else {\r\n                // Tell the client we're going to close the connection.\r\n                response.headers().set(CONNECTION, CLOSE);\r\n            }\r\n        }\r\n\r\n        @Override\r\n        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {\r\n            cause.printStackTrace();\r\n            ctx.close();\r\n        }\r\n    }\r\n}\r\n```\r\nping.sh\r\n```\r\n#!/bin/bash\r\n\r\nwhile :\r\ndo\r\n        echo \"Before $(date)\"\r\n        curl -v \"http://localhost:8080/ping\"\r\n        echo \"After $(date)\"\r\n        sleep 1\r\ndone\r\n```\r\nLong running response\r\n```curl \"http://localhost:8080/chunking\">/dev/null```\r\n### Netty version\r\n4.1.36.Final\r\n### JVM version (e.g. `java -version`)\r\njava version \"9.0.1\"\r\nJava(TM) SE Runtime Environment (build 9.0.1+11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 9.0.1+11, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nuname -a\r\nLinux dave-XPS-15-9560 4.15.0-50-generic #54~16.04.1-Ubuntu SMP Wed May 8 15:55:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["506767721","506768308","506769800","506771567","506772214","506776866","506779552","506785598","506785980","506794944","506797059","506921156","508801313"], "labels":[]}