{"id":"4958", "title":"Let OpenSslContext implement ReferenceCounted?", "body":"Hello,

would it make sense to let the `OpenSslContext` class implement the `ReferenceCounted` interface?

We have a dynamic and unbound set of OpenSslContext that are concurrently in use and it would be great if we can release the native resources in a timely manner. We're currently using a wrapper object that extends SslContext, implements ReferenceCounted and gets passed into the SniHandler.

Something along the lines of this pseudo code. I'd be happy to open a PR.

``` java
public class OpenSslContext extends SslContext implements ReferenceCounted {

  private final ReferenceCounted refCnt = new AbstractReferenceCounted() {

    @Override
    public ReferenceCounted touch(Object hint) {
      OpenSslContext.this.hint = hint;
      return this;
    }

    @Override
    protected void deallocate() {
      destroy();
    }
  };

  private Object hint = null;

  @Override
  public int refCnt() {
    return refCnt.refCnt();
  }

  @Override
  public OpenSslContext retain() {
    refCnt.retain();
    return this;
  }

  @Override
  public OpenSslContext retain(int increment) {
    refCnt.retain(increment);
    return this;
  }

  @Override
  public OpenSslContext touch() {
    refCnt.touch();
    return this;
  }

  @Override
  public OpenSslContext touch(Object hint) {
    refCnt.touch(hint);
    return this;
  }

  @Override
  public boolean release() {
    return refCnt.release();
  }

  @Override
  public boolean release(int decrement) {
    return refCnt.release(decrement);
  }

  protected final void destroy() {
    synchronized (OpenSslContext.class) {
      if (ctx != 0) {
        SSLContext.free(ctx);
        ctx = 0;
      }

      // Guard against multiple destroyPools() calls triggered by construction exception and finalize() later
      if (aprPool != 0) {
        Pool.destroy(aprPool);
        aprPool = 0;
      }
    }
  }
}
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4958","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4958/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4958/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4958/events","html_url":"https://github.com/netty/netty/issues/4958","id":139900598,"node_id":"MDU6SXNzdWUxMzk5MDA1OTg=","number":4958,"title":"Let OpenSslContext implement ReferenceCounted?","user":{"login":"rkapsi","id":191635,"node_id":"MDQ6VXNlcjE5MTYzNQ==","avatar_url":"https://avatars0.githubusercontent.com/u/191635?v=4","gravatar_id":"","url":"https://api.github.com/users/rkapsi","html_url":"https://github.com/rkapsi","followers_url":"https://api.github.com/users/rkapsi/followers","following_url":"https://api.github.com/users/rkapsi/following{/other_user}","gists_url":"https://api.github.com/users/rkapsi/gists{/gist_id}","starred_url":"https://api.github.com/users/rkapsi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rkapsi/subscriptions","organizations_url":"https://api.github.com/users/rkapsi/orgs","repos_url":"https://api.github.com/users/rkapsi/repos","events_url":"https://api.github.com/users/rkapsi/events{/privacy}","received_events_url":"https://api.github.com/users/rkapsi/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2016-03-10T14:20:12Z","updated_at":"2019-10-01T04:51:10Z","closed_at":"2016-08-05T07:57:47Z","author_association":"MEMBER","body":"Hello,\n\nwould it make sense to let the `OpenSslContext` class implement the `ReferenceCounted` interface?\n\nWe have a dynamic and unbound set of OpenSslContext that are concurrently in use and it would be great if we can release the native resources in a timely manner. We're currently using a wrapper object that extends SslContext, implements ReferenceCounted and gets passed into the SniHandler.\n\nSomething along the lines of this pseudo code. I'd be happy to open a PR.\n\n``` java\npublic class OpenSslContext extends SslContext implements ReferenceCounted {\n\n  private final ReferenceCounted refCnt = new AbstractReferenceCounted() {\n\n    @Override\n    public ReferenceCounted touch(Object hint) {\n      OpenSslContext.this.hint = hint;\n      return this;\n    }\n\n    @Override\n    protected void deallocate() {\n      destroy();\n    }\n  };\n\n  private Object hint = null;\n\n  @Override\n  public int refCnt() {\n    return refCnt.refCnt();\n  }\n\n  @Override\n  public OpenSslContext retain() {\n    refCnt.retain();\n    return this;\n  }\n\n  @Override\n  public OpenSslContext retain(int increment) {\n    refCnt.retain(increment);\n    return this;\n  }\n\n  @Override\n  public OpenSslContext touch() {\n    refCnt.touch();\n    return this;\n  }\n\n  @Override\n  public OpenSslContext touch(Object hint) {\n    refCnt.touch(hint);\n    return this;\n  }\n\n  @Override\n  public boolean release() {\n    return refCnt.release();\n  }\n\n  @Override\n  public boolean release(int decrement) {\n    return refCnt.release(decrement);\n  }\n\n  protected final void destroy() {\n    synchronized (OpenSslContext.class) {\n      if (ctx != 0) {\n        SSLContext.free(ctx);\n        ctx = 0;\n      }\n\n      // Guard against multiple destroyPools() calls triggered by construction exception and finalize() later\n      if (aprPool != 0) {\n        Pool.destroy(aprPool);\n        aprPool = 0;\n      }\n    }\n  }\n}\n```\n","closed_by":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}}", "commentIds":["196491901","196511169","536305841","536500416","536546929","536561222","536862926"], "labels":[]}