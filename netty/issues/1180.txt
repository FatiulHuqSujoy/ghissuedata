{"id":"1180", "title":"io.netty.buffer.IllegalBufferAccessException when running udt message example code", "body":"server trace:

mrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoServerHandler channelActive
INFO: ECHO active 
    [id: 0x13843678]
    0) Maximum_Transfer_Unit = 1.500
    1) Is_Send_Synchronous = false
    2) Is_Receive_Synchronous = false
    3) Custom_Congestion_Control = null
    4) Flight_Window_Size = 25.600 (25 K)
    5) Protocol_Send_Buffer_Size = 12.058.624
    6) Protocol_Receive_Buffer_Size = 12.058.624
    7) Time_To_Linger_On_Close = 180
    8) System_Send_Buffer_Size = 65.536
    9) System_Receive_Buffer_Size = 12.288.000
    12) Is_Randezvous_Connect_Enabled = false
    13) Send_Timeout = -1
    14) Receive_Timeout = -1
    15) Is_Address_Reuse_Enabled = true
    16) Maximum_Bandwidth = -1
    17) Status_Code = 5
    18) Epoll_Event_Mask = 5
    19) Send_Buffer_Consumed = 0
    20) Receive_Buffer_Available = 1
mrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoServerHandler exceptionCaught
WARNING: close the connection when an exception is raised
io.netty.buffer.IllegalBufferAccessException
    at io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:107)
    at io.netty.buffer.DefaultByteBufHolder.release(DefaultByteBufHolder.java:69)
    at io.netty.buffer.BufUtil.release(BufUtil.java:75)
    at io.netty.channel.ChannelHandlerUtil.handleInboundBufferUpdated(ChannelHandlerUtil.java:62)
    at io.netty.channel.ChannelInboundMessageHandlerAdapter.inboundBufferUpdated(ChannelInboundMessageHandlerAdapter.java:78)
    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:896)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)
    at io.netty.handler.logging.LoggingHandler.inboundBufferUpdated(LoggingHandler.java:246)
    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:917)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)
    at io.netty.channel.DefaultChannelPipeline.fireInboundBufferUpdated(DefaultChannelPipeline.java:1017)
    at io.netty.channel.nio.AbstractNioMessageChannel$NioMessageUnsafe.read(AbstractNioMessageChannel.java:87)
    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:460)
    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:424)
    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:360)
    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:103)
    at java.lang.Thread.run(Thread.java:722)

mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler close
INFO: [id: 0x13843678, /127.0.0.1:53347 => /127.0.0.1:1234] CLOSE()
mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelInactive
INFO: [id: 0x13843678, /127.0.0.1:53347 :> /127.0.0.1:1234] INACTIVE
mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelUnregistered
INFO: [id: 0x13843678, /127.0.0.1:53347 :> /127.0.0.1:1234] UNREGISTERED

client trace:

mrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClientHandler channelActive
INFO: ECHO active 
    [id: 0x379d92dd]
    0) Maximum_Transfer_Unit = 1.500
    1) Is_Send_Synchronous = false
    2) Is_Receive_Synchronous = false
    3) Custom_Congestion_Control = null
    4) Flight_Window_Size = 25.600 (25 K)
    5) Protocol_Send_Buffer_Size = 10.485.056
    6) Protocol_Receive_Buffer_Size = 10.485.056
    7) Time_To_Linger_On_Close = 0
    8) System_Send_Buffer_Size = 1.048.576
    9) System_Receive_Buffer_Size = 1.048.576
    12) Is_Randezvous_Connect_Enabled = false
    13) Send_Timeout = -1
    14) Receive_Timeout = -1
    15) Is_Address_Reuse_Enabled = true
    16) Maximum_Bandwidth = -1
    17) Status_Code = 5
    18) Epoll_Event_Mask = 4
    19) Send_Buffer_Consumed = 0
    20) Receive_Buffer_Available = 0
mrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClientHandler exceptionCaught
WARNING: close the connection when an exception is raised
io.netty.buffer.IllegalBufferAccessException
    at io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:107)
    at io.netty.buffer.DefaultByteBufHolder.release(DefaultByteBufHolder.java:69)
    at io.netty.buffer.BufUtil.release(BufUtil.java:75)
    at io.netty.channel.ChannelHandlerUtil.handleInboundBufferUpdated(ChannelHandlerUtil.java:62)
    at io.netty.channel.ChannelInboundMessageHandlerAdapter.inboundBufferUpdated(ChannelInboundMessageHandlerAdapter.java:78)
    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:896)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)
    at io.netty.handler.logging.LoggingHandler.inboundBufferUpdated(LoggingHandler.java:246)
    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:917)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)
    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)
    at io.netty.channel.DefaultChannelPipeline.fireInboundBufferUpdated(DefaultChannelPipeline.java:1017)
    at io.netty.channel.nio.AbstractNioMessageChannel$NioMessageUnsafe.read(AbstractNioMessageChannel.java:87)
    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:460)
    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:424)
    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:360)
    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:103)
    at java.lang.Thread.run(Thread.java:722)

mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler close
INFO: [id: 0x379d92dd, /127.0.0.1:53347 => /127.0.0.1:1234] CLOSE()
mrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClient main
INFO: done
mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelInactive
INFO: [id: 0x379d92dd, /127.0.0.1:53347 :> /127.0.0.1:1234] INACTIVE
mrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelUnregistered
INFO: [id: 0x379d92dd, /127.0.0.1:53347 :> /127.0.0.1:1234] UNREGISTERED
20-3-13 12:56:22 
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1180","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1180/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1180/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1180/events","html_url":"https://github.com/netty/netty/issues/1180","id":12223709,"node_id":"MDU6SXNzdWUxMjIyMzcwOQ==","number":1180,"title":"io.netty.buffer.IllegalBufferAccessException when running udt message example code","user":{"login":"siteware","id":242948,"node_id":"MDQ6VXNlcjI0Mjk0OA==","avatar_url":"https://avatars3.githubusercontent.com/u/242948?v=4","gravatar_id":"","url":"https://api.github.com/users/siteware","html_url":"https://github.com/siteware","followers_url":"https://api.github.com/users/siteware/followers","following_url":"https://api.github.com/users/siteware/following{/other_user}","gists_url":"https://api.github.com/users/siteware/gists{/gist_id}","starred_url":"https://api.github.com/users/siteware/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/siteware/subscriptions","organizations_url":"https://api.github.com/users/siteware/orgs","repos_url":"https://api.github.com/users/siteware/repos","events_url":"https://api.github.com/users/siteware/events{/privacy}","received_events_url":"https://api.github.com/users/siteware/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2013-03-20T11:59:51Z","updated_at":"2013-03-20T12:15:48Z","closed_at":"2013-03-20T12:15:48Z","author_association":"CONTRIBUTOR","body":"server trace:\n\nmrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoServerHandler channelActive\nINFO: ECHO active \n    [id: 0x13843678]\n    0) Maximum_Transfer_Unit = 1.500\n    1) Is_Send_Synchronous = false\n    2) Is_Receive_Synchronous = false\n    3) Custom_Congestion_Control = null\n    4) Flight_Window_Size = 25.600 (25 K)\n    5) Protocol_Send_Buffer_Size = 12.058.624\n    6) Protocol_Receive_Buffer_Size = 12.058.624\n    7) Time_To_Linger_On_Close = 180\n    8) System_Send_Buffer_Size = 65.536\n    9) System_Receive_Buffer_Size = 12.288.000\n    12) Is_Randezvous_Connect_Enabled = false\n    13) Send_Timeout = -1\n    14) Receive_Timeout = -1\n    15) Is_Address_Reuse_Enabled = true\n    16) Maximum_Bandwidth = -1\n    17) Status_Code = 5\n    18) Epoll_Event_Mask = 5\n    19) Send_Buffer_Consumed = 0\n    20) Receive_Buffer_Available = 1\nmrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoServerHandler exceptionCaught\nWARNING: close the connection when an exception is raised\nio.netty.buffer.IllegalBufferAccessException\n    at io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:107)\n    at io.netty.buffer.DefaultByteBufHolder.release(DefaultByteBufHolder.java:69)\n    at io.netty.buffer.BufUtil.release(BufUtil.java:75)\n    at io.netty.channel.ChannelHandlerUtil.handleInboundBufferUpdated(ChannelHandlerUtil.java:62)\n    at io.netty.channel.ChannelInboundMessageHandlerAdapter.inboundBufferUpdated(ChannelInboundMessageHandlerAdapter.java:78)\n    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:896)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)\n    at io.netty.handler.logging.LoggingHandler.inboundBufferUpdated(LoggingHandler.java:246)\n    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:917)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)\n    at io.netty.channel.DefaultChannelPipeline.fireInboundBufferUpdated(DefaultChannelPipeline.java:1017)\n    at io.netty.channel.nio.AbstractNioMessageChannel$NioMessageUnsafe.read(AbstractNioMessageChannel.java:87)\n    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:460)\n    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:424)\n    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:360)\n    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:103)\n    at java.lang.Thread.run(Thread.java:722)\n\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler close\nINFO: [id: 0x13843678, /127.0.0.1:53347 => /127.0.0.1:1234] CLOSE()\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelInactive\nINFO: [id: 0x13843678, /127.0.0.1:53347 :> /127.0.0.1:1234] INACTIVE\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelUnregistered\nINFO: [id: 0x13843678, /127.0.0.1:53347 :> /127.0.0.1:1234] UNREGISTERED\n\nclient trace:\n\nmrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClientHandler channelActive\nINFO: ECHO active \n    [id: 0x379d92dd]\n    0) Maximum_Transfer_Unit = 1.500\n    1) Is_Send_Synchronous = false\n    2) Is_Receive_Synchronous = false\n    3) Custom_Congestion_Control = null\n    4) Flight_Window_Size = 25.600 (25 K)\n    5) Protocol_Send_Buffer_Size = 10.485.056\n    6) Protocol_Receive_Buffer_Size = 10.485.056\n    7) Time_To_Linger_On_Close = 0\n    8) System_Send_Buffer_Size = 1.048.576\n    9) System_Receive_Buffer_Size = 1.048.576\n    12) Is_Randezvous_Connect_Enabled = false\n    13) Send_Timeout = -1\n    14) Receive_Timeout = -1\n    15) Is_Address_Reuse_Enabled = true\n    16) Maximum_Bandwidth = -1\n    17) Status_Code = 5\n    18) Epoll_Event_Mask = 4\n    19) Send_Buffer_Consumed = 0\n    20) Receive_Buffer_Available = 0\nmrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClientHandler exceptionCaught\nWARNING: close the connection when an exception is raised\nio.netty.buffer.IllegalBufferAccessException\n    at io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:107)\n    at io.netty.buffer.DefaultByteBufHolder.release(DefaultByteBufHolder.java:69)\n    at io.netty.buffer.BufUtil.release(BufUtil.java:75)\n    at io.netty.channel.ChannelHandlerUtil.handleInboundBufferUpdated(ChannelHandlerUtil.java:62)\n    at io.netty.channel.ChannelInboundMessageHandlerAdapter.inboundBufferUpdated(ChannelInboundMessageHandlerAdapter.java:78)\n    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:896)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)\n    at io.netty.handler.logging.LoggingHandler.inboundBufferUpdated(LoggingHandler.java:246)\n    at io.netty.channel.DefaultChannelHandlerContext.invokeInboundBufferUpdated(DefaultChannelHandlerContext.java:917)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated0(DefaultChannelHandlerContext.java:864)\n    at io.netty.channel.DefaultChannelHandlerContext.fireInboundBufferUpdated(DefaultChannelHandlerContext.java:843)\n    at io.netty.channel.DefaultChannelPipeline.fireInboundBufferUpdated(DefaultChannelPipeline.java:1017)\n    at io.netty.channel.nio.AbstractNioMessageChannel$NioMessageUnsafe.read(AbstractNioMessageChannel.java:87)\n    at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:460)\n    at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:424)\n    at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:360)\n    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:103)\n    at java.lang.Thread.run(Thread.java:722)\n\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler close\nINFO: [id: 0x379d92dd, /127.0.0.1:53347 => /127.0.0.1:1234] CLOSE()\nmrt 20, 2013 12:56:19 PM io.netty.example.udt.echo.message.MsgEchoClient main\nINFO: done\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelInactive\nINFO: [id: 0x379d92dd, /127.0.0.1:53347 :> /127.0.0.1:1234] INACTIVE\nmrt 20, 2013 12:56:19 PM io.netty.handler.logging.LoggingHandler channelUnregistered\nINFO: [id: 0x379d92dd, /127.0.0.1:53347 :> /127.0.0.1:1234] UNREGISTERED\n20-3-13 12:56:22 \n","closed_by":{"login":"siteware","id":242948,"node_id":"MDQ6VXNlcjI0Mjk0OA==","avatar_url":"https://avatars3.githubusercontent.com/u/242948?v=4","gravatar_id":"","url":"https://api.github.com/users/siteware","html_url":"https://github.com/siteware","followers_url":"https://api.github.com/users/siteware/followers","following_url":"https://api.github.com/users/siteware/following{/other_user}","gists_url":"https://api.github.com/users/siteware/gists{/gist_id}","starred_url":"https://api.github.com/users/siteware/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/siteware/subscriptions","organizations_url":"https://api.github.com/users/siteware/orgs","repos_url":"https://api.github.com/users/siteware/repos","events_url":"https://api.github.com/users/siteware/events{/privacy}","received_events_url":"https://api.github.com/users/siteware/received_events","type":"User","site_admin":false}}", "commentIds":["15171760","15172070"], "labels":[]}