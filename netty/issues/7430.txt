{"id":"7430", "title":"Netty ChannelGroup size error (with same IP but different ports)", "body":"

My server is ready 9001 port to telnet, now just use netty's channelgroup to do local test:

### Expected behavior
The client:
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001

The server:
127.0.0.1:1301   <---- ChannelGroup size = 1
127.0.0.1:1302   <---- ChannelGroup size = 2
127.0.0.1:1303   <---- ChannelGroup size = 3
127.0.0.1:1304   <---- ChannelGroup size = 4

### Actual behavior
The client:
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001
telnet 127.0.0.1:9001

The server:
127.0.0.1:1301   <---- ChannelGroup size = 1
127.0.0.1:1302   <---- ChannelGroup size = 1
127.0.0.1:1303   <---- ChannelGroup size = 1
127.0.0.1:1304   <---- ChannelGroup size = 1

### Steps to reproduce
Using local PC to telnet server by 127.0.0.1:9001 then test it.

### Minimal yet complete reproducer code (or URL to code)

### Netty version
4.1.16 final (netty-all)

### JVM version (e.g. `java -version`)
java version \"1.8.0_65\"
Java(TM) SE Runtime Environment (build 1.8.0_65-b17)
Java HotSpot(TM) 64-Bit Server VM (build 25.65-b01, mixed mode)

### OS version (e.g. `uname -a`)
Windows 8.1 Pro + Netbeans 8.2 full", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7430","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7430/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7430/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7430/events","html_url":"https://github.com/netty/netty/issues/7430","id":275917198,"node_id":"MDU6SXNzdWUyNzU5MTcxOTg=","number":7430,"title":"Netty ChannelGroup size error (with same IP but different ports)","user":{"login":"andymacau853","id":1874941,"node_id":"MDQ6VXNlcjE4NzQ5NDE=","avatar_url":"https://avatars1.githubusercontent.com/u/1874941?v=4","gravatar_id":"","url":"https://api.github.com/users/andymacau853","html_url":"https://github.com/andymacau853","followers_url":"https://api.github.com/users/andymacau853/followers","following_url":"https://api.github.com/users/andymacau853/following{/other_user}","gists_url":"https://api.github.com/users/andymacau853/gists{/gist_id}","starred_url":"https://api.github.com/users/andymacau853/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/andymacau853/subscriptions","organizations_url":"https://api.github.com/users/andymacau853/orgs","repos_url":"https://api.github.com/users/andymacau853/repos","events_url":"https://api.github.com/users/andymacau853/events{/privacy}","received_events_url":"https://api.github.com/users/andymacau853/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2017-11-22T01:40:50Z","updated_at":"2018-02-04T13:57:19Z","closed_at":"2017-11-22T08:13:57Z","author_association":"NONE","body":"\r\n\r\nMy server is ready 9001 port to telnet, now just use netty's channelgroup to do local test:\r\n\r\n### Expected behavior\r\nThe client:\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\n\r\nThe server:\r\n127.0.0.1:1301   <---- ChannelGroup size = 1\r\n127.0.0.1:1302   <---- ChannelGroup size = 2\r\n127.0.0.1:1303   <---- ChannelGroup size = 3\r\n127.0.0.1:1304   <---- ChannelGroup size = 4\r\n\r\n### Actual behavior\r\nThe client:\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\ntelnet 127.0.0.1:9001\r\n\r\nThe server:\r\n127.0.0.1:1301   <---- ChannelGroup size = 1\r\n127.0.0.1:1302   <---- ChannelGroup size = 1\r\n127.0.0.1:1303   <---- ChannelGroup size = 1\r\n127.0.0.1:1304   <---- ChannelGroup size = 1\r\n\r\n### Steps to reproduce\r\nUsing local PC to telnet server by 127.0.0.1:9001 then test it.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n### Netty version\r\n4.1.16 final (netty-all)\r\n\r\n### JVM version (e.g. `java -version`)\r\njava version \"1.8.0_65\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_65-b17)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.65-b01, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nWindows 8.1 Pro + Netbeans 8.2 full","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["346217217","346217242","346274584","362908650"], "labels":["not a bug"]}