{"id":"4796", "title":"Using Socks4/5 Proxy Handlers in Netty Client (4.1b8)", "body":"Netty 4.1.0 Beta8
Tried a lot of proxies from free socks lists (like www.socks-proxy.net, http://sockslist.net/ etc) with no luck:

``` java
@Test
    public void testProxy() throws Exception {
        final String ua = \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36\";
        final String host = \"www.main.de\";
        final int port = 80;

        Bootstrap b = new Bootstrap();
        b.group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();

                        p.addLast(new HttpClientCodec());
                        p.addLast(new HttpContentDecompressor());
                        p.addLast(new HttpObjectAggregator(10_485_760));
                        p.addLast(new ChannelInboundHandlerAdapter() {
                            @Override
                            public void channelActive(final ChannelHandlerContext ctx) throws Exception {
                                HttpRequest request = new DefaultFullHttpRequest(HTTP_1_1, GET, \"/\");
                                request.headers().set(HOST, host + \":\" + port);
                                request.headers().set(USER_AGENT, ua);
                                request.headers().set(CONNECTION, CLOSE);

                                ctx.writeAndFlush(request);

                                System.out.println(\"!sent\");
                            }

                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                System.out.println(\"!answer\");
                                if (msg instanceof FullHttpResponse) {
                                    FullHttpResponse httpResp = (FullHttpResponse) msg;


                                    ByteBuf content = httpResp.content();
                                    String strContent = content.toString(UTF_8);
                                    System.out.println(\"body: \" + strContent);

                                    finish.countDown();
                                    return;
                                }

                                super.channelRead(ctx, msg);
                            }

                            @Override
                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
                                cause.printStackTrace(System.err);
                                ctx.close();
                                finish.countDown();
                            }
                        });

                        p.addLast(new Socks4ProxyHandler(new InetSocketAddress(\"149.202.68.167\", 37678)));
                    }
                });

        b.connect(host, port).awaitUninterruptibly();
        System.out.println(\"!connected\");

        finish.await(1, MINUTES);
    }
```

The connection hangs, resets or getting some strange exceptions.
What's wrong? How to use it?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4796","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4796/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4796/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4796/events","html_url":"https://github.com/netty/netty/issues/4796","id":130043474,"node_id":"MDU6SXNzdWUxMzAwNDM0NzQ=","number":4796,"title":"Using Socks4/5 Proxy Handlers in Netty Client (4.1b8)","user":{"login":"mbaturov","id":15051575,"node_id":"MDQ6VXNlcjE1MDUxNTc1","avatar_url":"https://avatars3.githubusercontent.com/u/15051575?v=4","gravatar_id":"","url":"https://api.github.com/users/mbaturov","html_url":"https://github.com/mbaturov","followers_url":"https://api.github.com/users/mbaturov/followers","following_url":"https://api.github.com/users/mbaturov/following{/other_user}","gists_url":"https://api.github.com/users/mbaturov/gists{/gist_id}","starred_url":"https://api.github.com/users/mbaturov/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mbaturov/subscriptions","organizations_url":"https://api.github.com/users/mbaturov/orgs","repos_url":"https://api.github.com/users/mbaturov/repos","events_url":"https://api.github.com/users/mbaturov/events{/privacy}","received_events_url":"https://api.github.com/users/mbaturov/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-01-30T21:56:15Z","updated_at":"2016-02-01T09:54:14Z","closed_at":"2016-02-01T09:54:14Z","author_association":"NONE","body":"Netty 4.1.0 Beta8\nTried a lot of proxies from free socks lists (like www.socks-proxy.net, http://sockslist.net/ etc) with no luck:\n\n``` java\n@Test\n    public void testProxy() throws Exception {\n        final String ua = \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36\";\n        final String host = \"www.main.de\";\n        final int port = 80;\n\n        Bootstrap b = new Bootstrap();\n        b.group(new NioEventLoopGroup())\n                .channel(NioSocketChannel.class)\n                .handler(new ChannelInitializer<SocketChannel>() {\n                    @Override\n                    protected void initChannel(SocketChannel ch) throws Exception {\n                        ChannelPipeline p = ch.pipeline();\n\n                        p.addLast(new HttpClientCodec());\n                        p.addLast(new HttpContentDecompressor());\n                        p.addLast(new HttpObjectAggregator(10_485_760));\n                        p.addLast(new ChannelInboundHandlerAdapter() {\n                            @Override\n                            public void channelActive(final ChannelHandlerContext ctx) throws Exception {\n                                HttpRequest request = new DefaultFullHttpRequest(HTTP_1_1, GET, \"/\");\n                                request.headers().set(HOST, host + \":\" + port);\n                                request.headers().set(USER_AGENT, ua);\n                                request.headers().set(CONNECTION, CLOSE);\n\n                                ctx.writeAndFlush(request);\n\n                                System.out.println(\"!sent\");\n                            }\n\n                            @Override\n                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {\n                                System.out.println(\"!answer\");\n                                if (msg instanceof FullHttpResponse) {\n                                    FullHttpResponse httpResp = (FullHttpResponse) msg;\n\n\n                                    ByteBuf content = httpResp.content();\n                                    String strContent = content.toString(UTF_8);\n                                    System.out.println(\"body: \" + strContent);\n\n                                    finish.countDown();\n                                    return;\n                                }\n\n                                super.channelRead(ctx, msg);\n                            }\n\n                            @Override\n                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {\n                                cause.printStackTrace(System.err);\n                                ctx.close();\n                                finish.countDown();\n                            }\n                        });\n\n                        p.addLast(new Socks4ProxyHandler(new InetSocketAddress(\"149.202.68.167\", 37678)));\n                    }\n                });\n\n        b.connect(host, port).awaitUninterruptibly();\n        System.out.println(\"!connected\");\n\n        finish.await(1, MINUTES);\n    }\n```\n\nThe connection hangs, resets or getting some strange exceptions.\nWhat's wrong? How to use it?\n","closed_by":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}}", "commentIds":["177779950","177838391"], "labels":["not a bug"]}