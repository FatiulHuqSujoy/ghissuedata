{"id":"6652", "title":"Misleading comment in `ServerBootstrap`", "body":"The mechanism of registering handlers seems to have been changed since #5566 has been fixed, making [this](https://github.com/netty/netty/blob/4.1/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java#L178) comment misleading.

I did not have time to inspect the whole history, but the user's `initChannel` is now not called after the current invocation returns. As an evidence, the code patched with the following change on tag `4.1.9.Final` also passes [this test case](https://github.com/netty/netty/blob/4.1/transport/src/test/java/io/netty/bootstrap/ServerBootstrapTest.java#L81), which was introduced to ensure that #5566 was fixed.

```
$ git diff
diff --git a/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java b/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java
index f9042af46..b73239ffa 100644
--- a/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java
+++ b/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java
@@ -174,7 +174,9 @@ public class ServerBootstrap extends AbstractBootstrap<ServerBootstrap, ServerCh
                 if (handler != null) {
                     pipeline.addLast(handler);
                 }
-
+                pipeline.addLast(new ServerBootstrapAcceptor(
+                        ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));
+                /*
                 // We add this handler via the EventLoop as the user may have used a ChannelInitializer as handler.
                 // In this case the initChannel(...) method will only be called after this method returns. Because
                 // of this we need to ensure we add our handler in a delayed fashion so all the users handler are
@@ -185,7 +187,7 @@ public class ServerBootstrap extends AbstractBootstrap<ServerBootstrap, ServerCh
                         pipeline.addLast(new ServerBootstrapAcceptor(
                                 ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));
                     }
-                });
+                });*/
             }
         });
     }
```

Please consider changing the comment, it could be very confusing for newcomers.

### Expected behavior

N/A

### Actual behavior

N/A

### Steps to reproduce

N/A

### Minimal yet complete reproducer code (or URL to code)

N/A

### Netty version

4.1.9.Final

### JVM version (e.g. `java -version`)

java version \"1.8.0_121\"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)

### OS version (e.g. `uname -a`)
 
Linux herberteuler 4.9.22 #1-NixOS SMP Wed Apr 12 10:41:42 UTC 2017 x86_64 GNU/Linux", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6652","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6652/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6652/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6652/events","html_url":"https://github.com/netty/netty/issues/6652","id":223422788,"node_id":"MDU6SXNzdWUyMjM0MjI3ODg=","number":6652,"title":"Misleading comment in `ServerBootstrap`","user":{"login":"herberteuler","id":1401179,"node_id":"MDQ6VXNlcjE0MDExNzk=","avatar_url":"https://avatars2.githubusercontent.com/u/1401179?v=4","gravatar_id":"","url":"https://api.github.com/users/herberteuler","html_url":"https://github.com/herberteuler","followers_url":"https://api.github.com/users/herberteuler/followers","following_url":"https://api.github.com/users/herberteuler/following{/other_user}","gists_url":"https://api.github.com/users/herberteuler/gists{/gist_id}","starred_url":"https://api.github.com/users/herberteuler/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/herberteuler/subscriptions","organizations_url":"https://api.github.com/users/herberteuler/orgs","repos_url":"https://api.github.com/users/herberteuler/repos","events_url":"https://api.github.com/users/herberteuler/events{/privacy}","received_events_url":"https://api.github.com/users/herberteuler/received_events","type":"User","site_admin":false},"labels":[{"id":185731,"node_id":"MDU6TGFiZWwxODU3MzE=","url":"https://api.github.com/repos/netty/netty/labels/cleanup","name":"cleanup","color":"5319e7","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/162","html_url":"https://github.com/netty/netty/milestone/162","labels_url":"https://api.github.com/repos/netty/netty/milestones/162/labels","id":2487479,"node_id":"MDk6TWlsZXN0b25lMjQ4NzQ3OQ==","number":162,"title":"4.0.47.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":20,"state":"closed","created_at":"2017-04-28T05:56:52Z","updated_at":"2017-05-12T06:26:07Z","due_on":null,"closed_at":"2017-05-11T17:54:34Z"},"comments":3,"created_at":"2017-04-21T15:45:33Z","updated_at":"2017-05-02T22:37:50Z","closed_at":"2017-05-02T22:36:50Z","author_association":"CONTRIBUTOR","body":"The mechanism of registering handlers seems to have been changed since #5566 has been fixed, making [this](https://github.com/netty/netty/blob/4.1/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java#L178) comment misleading.\r\n\r\nI did not have time to inspect the whole history, but the user's `initChannel` is now not called after the current invocation returns. As an evidence, the code patched with the following change on tag `4.1.9.Final` also passes [this test case](https://github.com/netty/netty/blob/4.1/transport/src/test/java/io/netty/bootstrap/ServerBootstrapTest.java#L81), which was introduced to ensure that #5566 was fixed.\r\n\r\n```\r\n$ git diff\r\ndiff --git a/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java b/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java\r\nindex f9042af46..b73239ffa 100644\r\n--- a/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java\r\n+++ b/transport/src/main/java/io/netty/bootstrap/ServerBootstrap.java\r\n@@ -174,7 +174,9 @@ public class ServerBootstrap extends AbstractBootstrap<ServerBootstrap, ServerCh\r\n                 if (handler != null) {\r\n                     pipeline.addLast(handler);\r\n                 }\r\n-\r\n+                pipeline.addLast(new ServerBootstrapAcceptor(\r\n+                        ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));\r\n+                /*\r\n                 // We add this handler via the EventLoop as the user may have used a ChannelInitializer as handler.\r\n                 // In this case the initChannel(...) method will only be called after this method returns. Because\r\n                 // of this we need to ensure we add our handler in a delayed fashion so all the users handler are\r\n@@ -185,7 +187,7 @@ public class ServerBootstrap extends AbstractBootstrap<ServerBootstrap, ServerCh\r\n                         pipeline.addLast(new ServerBootstrapAcceptor(\r\n                                 ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));\r\n                     }\r\n-                });\r\n+                });*/\r\n             }\r\n         });\r\n     }\r\n```\r\n\r\nPlease consider changing the comment, it could be very confusing for newcomers.\r\n\r\n### Expected behavior\r\n\r\nN/A\r\n\r\n### Actual behavior\r\n\r\nN/A\r\n\r\n### Steps to reproduce\r\n\r\nN/A\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\nN/A\r\n\r\n### Netty version\r\n\r\n4.1.9.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\njava version \"1.8.0_121\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_121-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\n \r\nLinux herberteuler 4.9.22 #1-NixOS SMP Wed Apr 12 10:41:42 UTC 2017 x86_64 GNU/Linux","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["296266179","296338334","298780942"], "labels":["cleanup"]}