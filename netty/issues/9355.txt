{"id":"9355", "title":"java.net.SocketException: recvfrom failed: ETIMEDOUT (Connection timed out)", "body":"### Expected behavior
work well

### Actual behavior


Appeared after about 12 hours of running on Android 7.0,it work well on android 6.0,After quitting the application, it still can't work, and the system restart can work normally.

### Steps to reproduce

### Minimal yet complete reproducer code (or URL to code)
client code:
       if (!isConnect) {
                isConnecting = true;
                group = new NioEventLoopGroup();
                StringEncoder = new StringEncoder(CharsetUtil.UTF_8);
                lineBasedFrameDecoder = new LineBasedFrameDecoder(1024);
                stringDecoder = new StringDecoder(CharsetUtil.UTF_8);
                nettyClientHandler = new NettyClientHandler(listener);
                channelInitializer = new ChannelInitializer<SocketChannel>() { // 5
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(\"ping\", new IdleStateHandler(0, 5, 0, TimeUnit.SECONDS));//5s未发送数据，回调userEventTriggered
                        ch.pipeline().addLast(StringEncoder);
                        ch.pipeline().addLast(lineBasedFrameDecoder);//黏包处理
                        ch.pipeline().addLast(stringDecoder);
                        ch.pipeline().addLast(nettyClientHandler);
                    }
                };

                bootstrap = new Bootstrap().group(group)
                        .option(ChannelOption.TCP_NODELAY, true)//屏蔽Nagle算法试图
                        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, CONNECT_TIMEOUT_MILLIS)
                        .channel(NioSocketChannel.class)
                        .handler(channelInitializer);

                try {
                    String ip = GlobalDataManager.getInstance().getConfig().getStringValue(IConfigField.LOCALIP);
                    Trace.i(TAG, \"conn remote ip=\" + ip);
                    channelFuture = bootstrap.connect(ip, tcp_port).addListener((ChannelFutureListener) channelFuture1 -> {
                        if (channelFuture1.isSuccess()) {
                            Trace.e(TAG, \"连接成功\");
                            isConnect = true;
                            channel = channelFuture1.channel();
                        } else {
                            Trace.e(TAG, \"连接失败\");
                            isConnect = false;
                        }
                        isConnecting = false;
                    }).sync();

                    // Wait until the connection is closed.
                    channelFuture.channel().closeFuture().sync();
                    Trace.e(TAG, \" 断开连接\");
                } catch (Exception e) {
                    e.printStackTrace();
                    Trace.i(TAG, \"netty client err=\" + e.getMessage());
                } finally {
                    isConnect = false;
                    listener.onServiceStatusConnectChanged(NettyListener.STATUS_CONNECT_CLOSED);
                    if (null != channelFuture) {
                        if (channelFuture.channel() != null && channelFuture.channel().isOpen()) {
                            channelFuture.channel().close();
                        }
                        channelFuture = null;
                    }
                    channel = null;
                    group.shutdownGracefully();
                    group = null;
                    bootstrap = null;
                    channelInitializer = null;
                    reconnect();
                }
            }
server code:

        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup();
        try {
            b = new ServerBootstrap();
            inetSocketAddress  = new InetSocketAddress(port);

            channelInitializer =  new ChannelInitializer<SocketChannel>() {

                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    Trace.i(TAG,\"initChannel ch:\" + ch);
                    stringEncoder = new StringEncoder(CharsetUtil.UTF_8);
                    lineBasedFrameDecoder =  new LineBasedFrameDecoder(1024);
                    stringDecoder = new StringDecoder(CharsetUtil.UTF_8);
                    echoServerHandler = new EchoServerHandler(listener);


                    ch.pipeline().addLast(stringEncoder);
                    ch.pipeline().addLast(lineBasedFrameDecoder);
                    ch.pipeline().addLast(stringDecoder);
                    ch.pipeline().addLast(echoServerHandler);
                }
            };

            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(inetSocketAddress)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childHandler(channelInitializer);

            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind().sync(); // 8

            Trace.i(TAG, EchoServer.class.getName() + \" started and listen on \" + f.channel().localAddress());
            isServerStart = true;
            listener.onStartServer();
            // Wait until the server socket is closed.
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            Trace.e(TAG, \"connection =\"+e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            isServerStart = false;
            listener.onStopServer();
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            b = null;
            inetSocketAddress =null;
            reconnect();
        }
### Netty version
netty-all-4.1.23.Final.jar
### JVM version (e.g. `java -version`)
java -1.8
### OS version (e.g. `uname -a`)
Android 7.0", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/9355","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/9355/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/9355/comments","events_url":"https://api.github.com/repos/netty/netty/issues/9355/events","html_url":"https://github.com/netty/netty/issues/9355","id":467182704,"node_id":"MDU6SXNzdWU0NjcxODI3MDQ=","number":9355,"title":"java.net.SocketException: recvfrom failed: ETIMEDOUT (Connection timed out)","user":{"login":"merryhe1990","id":52807125,"node_id":"MDQ6VXNlcjUyODA3MTI1","avatar_url":"https://avatars3.githubusercontent.com/u/52807125?v=4","gravatar_id":"","url":"https://api.github.com/users/merryhe1990","html_url":"https://github.com/merryhe1990","followers_url":"https://api.github.com/users/merryhe1990/followers","following_url":"https://api.github.com/users/merryhe1990/following{/other_user}","gists_url":"https://api.github.com/users/merryhe1990/gists{/gist_id}","starred_url":"https://api.github.com/users/merryhe1990/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/merryhe1990/subscriptions","organizations_url":"https://api.github.com/users/merryhe1990/orgs","repos_url":"https://api.github.com/users/merryhe1990/repos","events_url":"https://api.github.com/users/merryhe1990/events{/privacy}","received_events_url":"https://api.github.com/users/merryhe1990/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-07-12T01:57:12Z","updated_at":"2019-07-16T20:06:40Z","closed_at":"2019-07-16T20:06:39Z","author_association":"NONE","body":"### Expected behavior\r\nwork well\r\n\r\n### Actual behavior\r\n\r\n\r\nAppeared after about 12 hours of running on Android 7.0,it work well on android 6.0,After quitting the application, it still can't work, and the system restart can work normally.\r\n\r\n### Steps to reproduce\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\nclient code:\r\n       if (!isConnect) {\r\n                isConnecting = true;\r\n                group = new NioEventLoopGroup();\r\n                StringEncoder = new StringEncoder(CharsetUtil.UTF_8);\r\n                lineBasedFrameDecoder = new LineBasedFrameDecoder(1024);\r\n                stringDecoder = new StringDecoder(CharsetUtil.UTF_8);\r\n                nettyClientHandler = new NettyClientHandler(listener);\r\n                channelInitializer = new ChannelInitializer<SocketChannel>() { // 5\r\n                    @Override\r\n                    public void initChannel(SocketChannel ch) throws Exception {\r\n                        ch.pipeline().addLast(\"ping\", new IdleStateHandler(0, 5, 0, TimeUnit.SECONDS));//5s未发送数据，回调userEventTriggered\r\n                        ch.pipeline().addLast(StringEncoder);\r\n                        ch.pipeline().addLast(lineBasedFrameDecoder);//黏包处理\r\n                        ch.pipeline().addLast(stringDecoder);\r\n                        ch.pipeline().addLast(nettyClientHandler);\r\n                    }\r\n                };\r\n\r\n                bootstrap = new Bootstrap().group(group)\r\n                        .option(ChannelOption.TCP_NODELAY, true)//屏蔽Nagle算法试图\r\n                        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, CONNECT_TIMEOUT_MILLIS)\r\n                        .channel(NioSocketChannel.class)\r\n                        .handler(channelInitializer);\r\n\r\n                try {\r\n                    String ip = GlobalDataManager.getInstance().getConfig().getStringValue(IConfigField.LOCALIP);\r\n                    Trace.i(TAG, \"conn remote ip=\" + ip);\r\n                    channelFuture = bootstrap.connect(ip, tcp_port).addListener((ChannelFutureListener) channelFuture1 -> {\r\n                        if (channelFuture1.isSuccess()) {\r\n                            Trace.e(TAG, \"连接成功\");\r\n                            isConnect = true;\r\n                            channel = channelFuture1.channel();\r\n                        } else {\r\n                            Trace.e(TAG, \"连接失败\");\r\n                            isConnect = false;\r\n                        }\r\n                        isConnecting = false;\r\n                    }).sync();\r\n\r\n                    // Wait until the connection is closed.\r\n                    channelFuture.channel().closeFuture().sync();\r\n                    Trace.e(TAG, \" 断开连接\");\r\n                } catch (Exception e) {\r\n                    e.printStackTrace();\r\n                    Trace.i(TAG, \"netty client err=\" + e.getMessage());\r\n                } finally {\r\n                    isConnect = false;\r\n                    listener.onServiceStatusConnectChanged(NettyListener.STATUS_CONNECT_CLOSED);\r\n                    if (null != channelFuture) {\r\n                        if (channelFuture.channel() != null && channelFuture.channel().isOpen()) {\r\n                            channelFuture.channel().close();\r\n                        }\r\n                        channelFuture = null;\r\n                    }\r\n                    channel = null;\r\n                    group.shutdownGracefully();\r\n                    group = null;\r\n                    bootstrap = null;\r\n                    channelInitializer = null;\r\n                    reconnect();\r\n                }\r\n            }\r\nserver code:\r\n\r\n        bossGroup = new NioEventLoopGroup(1);\r\n        workerGroup = new NioEventLoopGroup();\r\n        try {\r\n            b = new ServerBootstrap();\r\n            inetSocketAddress  = new InetSocketAddress(port);\r\n\r\n            channelInitializer =  new ChannelInitializer<SocketChannel>() {\r\n\r\n                @Override\r\n                public void initChannel(SocketChannel ch) throws Exception {\r\n                    Trace.i(TAG,\"initChannel ch:\" + ch);\r\n                    stringEncoder = new StringEncoder(CharsetUtil.UTF_8);\r\n                    lineBasedFrameDecoder =  new LineBasedFrameDecoder(1024);\r\n                    stringDecoder = new StringDecoder(CharsetUtil.UTF_8);\r\n                    echoServerHandler = new EchoServerHandler(listener);\r\n\r\n\r\n                    ch.pipeline().addLast(stringEncoder);\r\n                    ch.pipeline().addLast(lineBasedFrameDecoder);\r\n                    ch.pipeline().addLast(stringDecoder);\r\n                    ch.pipeline().addLast(echoServerHandler);\r\n                }\r\n            };\r\n\r\n            b.group(bossGroup, workerGroup)\r\n                    .channel(NioServerSocketChannel.class)\r\n                    .localAddress(inetSocketAddress)\r\n                    .childOption(ChannelOption.SO_KEEPALIVE, true)\r\n                    .childOption(ChannelOption.SO_REUSEADDR, true)\r\n                    .childOption(ChannelOption.TCP_NODELAY, true)\r\n                    .childHandler(channelInitializer);\r\n\r\n            // Bind and start to accept incoming connections.\r\n            ChannelFuture f = b.bind().sync(); // 8\r\n\r\n            Trace.i(TAG, EchoServer.class.getName() + \" started and listen on \" + f.channel().localAddress());\r\n            isServerStart = true;\r\n            listener.onStartServer();\r\n            // Wait until the server socket is closed.\r\n            f.channel().closeFuture().sync();\r\n        } catch (Exception e) {\r\n            Trace.e(TAG, \"connection =\"+e.getLocalizedMessage());\r\n            e.printStackTrace();\r\n        } finally {\r\n            isServerStart = false;\r\n            listener.onStopServer();\r\n            workerGroup.shutdownGracefully();\r\n            bossGroup.shutdownGracefully();\r\n            b = null;\r\n            inetSocketAddress =null;\r\n            reconnect();\r\n        }\r\n### Netty version\r\nnetty-all-4.1.23.Final.jar\r\n### JVM version (e.g. `java -version`)\r\njava -1.8\r\n### OS version (e.g. `uname -a`)\r\nAndroid 7.0","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["511965539"], "labels":[]}