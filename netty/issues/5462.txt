{"id":"5462", "title":"FixedChannelPool concurrency issue", "body":"Netty 4.0.37.Final

Context:

```
    private EventLoopGroup workerGroup = new NioEventLoopGroup(2);

    private final ChannelPoolMap<HostAndPort, FixedChannelPool> channelPoolMap = new AbstractChannelPoolMap<HostAndPort, FixedChannelPool>() {
        @Override
        protected FixedChannelPool newPool(HostAndPort hostAndPort) {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap
                    .option(ChannelOption.TCP_NODELAY, httpClientConfig.isTcpNoDelay())
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, httpClientConfig.getConnectionTimeoutMs())
                    .option(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 8 * 1024)
                    .option(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 16 * 1024)
                    .group(workerGroup)
                    .channel(NioSocketChannel.class)
                    .remoteAddress(hostAndPort.getHostText(), hostAndPort.getPort());
            return new FixedChannelPool(bootstrap, httpClientChannelPoolHandler,
                    ChannelHealthChecker.ACTIVE, FixedChannelPool.AcquireTimeoutAction.FAIL, 1,
                    400, Integer.MAX_VALUE);
        }
    };
```

Problem:
I found concurrency issue with FixedChannelPool in case if we pass bootstrap configured with EventLoopGroup with more than 1 thread.

In case of above configuration we will have channel leaking in FixedChannelPool because code below will be executed in eventLoops of passed channels. Different channels from same pool could be bound to different eventLoops and `decrementAndRunTaskQueue()` could be executed concurrently in fact without any synchronization inside this method:

```
public final class FixedChannelPool extends SimpleChannelPool {
    @Override
    public Future<Void> release(final Channel channel, final Promise<Void> promise) {
        final Promise<Void> p = executor.newPromise();
        super.release(channel, p.addListener(new FutureListener<Void>() {
            @Override
            public void operationComplete(Future<Void> future) throws Exception {
                ...
                if (future.isSuccess()) {
                    decrementAndRunTaskQueue();
                    promise.setSuccess(null);
                } else {
                    Throwable cause = future.cause();
                    // Check if the exception was not because of we passed the Channel to the wrong pool.
                    if (!(cause instanceof IllegalArgumentException)) {
                        decrementAndRunTaskQueue();
                    }
                    promise.setFailure(future.cause());
                }
            }
        }));
        return p;
    }

    private void decrementAndRunTaskQueue() {
        --acquiredChannelCount;

        // We should never have a negative value.
        assert acquiredChannelCount >= 0;

        // Run the pending acquire tasks before notify the original promise so if the user would
        // try to acquire again from the ChannelFutureListener and the pendingAcquireCount is >=
        // maxPendingAcquires we may be able to run some pending tasks first and so allow to add
        // more.
        runTaskQueue();
    }
}
```

So, looks like for now it's not safe to use FixedChannelPool in case of EventLoopGroup size > 1.
Related to #5455
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/5462","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/5462/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/5462/comments","events_url":"https://api.github.com/repos/netty/netty/issues/5462/events","html_url":"https://github.com/netty/netty/issues/5462","id":162755394,"node_id":"MDU6SXNzdWUxNjI3NTUzOTQ=","number":5462,"title":"FixedChannelPool concurrency issue","user":{"login":"Spikhalskiy","id":532108,"node_id":"MDQ6VXNlcjUzMjEwOA==","avatar_url":"https://avatars2.githubusercontent.com/u/532108?v=4","gravatar_id":"","url":"https://api.github.com/users/Spikhalskiy","html_url":"https://github.com/Spikhalskiy","followers_url":"https://api.github.com/users/Spikhalskiy/followers","following_url":"https://api.github.com/users/Spikhalskiy/following{/other_user}","gists_url":"https://api.github.com/users/Spikhalskiy/gists{/gist_id}","starred_url":"https://api.github.com/users/Spikhalskiy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Spikhalskiy/subscriptions","organizations_url":"https://api.github.com/users/Spikhalskiy/orgs","repos_url":"https://api.github.com/users/Spikhalskiy/repos","events_url":"https://api.github.com/users/Spikhalskiy/events{/privacy}","received_events_url":"https://api.github.com/users/Spikhalskiy/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-06-28T18:34:46Z","updated_at":"2016-06-28T18:48:22Z","closed_at":"2016-06-28T18:48:22Z","author_association":"CONTRIBUTOR","body":"Netty 4.0.37.Final\n\nContext:\n\n```\n    private EventLoopGroup workerGroup = new NioEventLoopGroup(2);\n\n    private final ChannelPoolMap<HostAndPort, FixedChannelPool> channelPoolMap = new AbstractChannelPoolMap<HostAndPort, FixedChannelPool>() {\n        @Override\n        protected FixedChannelPool newPool(HostAndPort hostAndPort) {\n            Bootstrap bootstrap = new Bootstrap();\n            bootstrap\n                    .option(ChannelOption.TCP_NODELAY, httpClientConfig.isTcpNoDelay())\n                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, httpClientConfig.getConnectionTimeoutMs())\n                    .option(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 8 * 1024)\n                    .option(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 16 * 1024)\n                    .group(workerGroup)\n                    .channel(NioSocketChannel.class)\n                    .remoteAddress(hostAndPort.getHostText(), hostAndPort.getPort());\n            return new FixedChannelPool(bootstrap, httpClientChannelPoolHandler,\n                    ChannelHealthChecker.ACTIVE, FixedChannelPool.AcquireTimeoutAction.FAIL, 1,\n                    400, Integer.MAX_VALUE);\n        }\n    };\n```\n\nProblem:\nI found concurrency issue with FixedChannelPool in case if we pass bootstrap configured with EventLoopGroup with more than 1 thread.\n\nIn case of above configuration we will have channel leaking in FixedChannelPool because code below will be executed in eventLoops of passed channels. Different channels from same pool could be bound to different eventLoops and `decrementAndRunTaskQueue()` could be executed concurrently in fact without any synchronization inside this method:\n\n```\npublic final class FixedChannelPool extends SimpleChannelPool {\n    @Override\n    public Future<Void> release(final Channel channel, final Promise<Void> promise) {\n        final Promise<Void> p = executor.newPromise();\n        super.release(channel, p.addListener(new FutureListener<Void>() {\n            @Override\n            public void operationComplete(Future<Void> future) throws Exception {\n                ...\n                if (future.isSuccess()) {\n                    decrementAndRunTaskQueue();\n                    promise.setSuccess(null);\n                } else {\n                    Throwable cause = future.cause();\n                    // Check if the exception was not because of we passed the Channel to the wrong pool.\n                    if (!(cause instanceof IllegalArgumentException)) {\n                        decrementAndRunTaskQueue();\n                    }\n                    promise.setFailure(future.cause());\n                }\n            }\n        }));\n        return p;\n    }\n\n    private void decrementAndRunTaskQueue() {\n        --acquiredChannelCount;\n\n        // We should never have a negative value.\n        assert acquiredChannelCount >= 0;\n\n        // Run the pending acquire tasks before notify the original promise so if the user would\n        // try to acquire again from the ChannelFutureListener and the pendingAcquireCount is >=\n        // maxPendingAcquires we may be able to run some pending tasks first and so allow to add\n        // more.\n        runTaskQueue();\n    }\n}\n```\n\nSo, looks like for now it's not safe to use FixedChannelPool in case of EventLoopGroup size > 1.\nRelated to #5455\n","closed_by":{"login":"Spikhalskiy","id":532108,"node_id":"MDQ6VXNlcjUzMjEwOA==","avatar_url":"https://avatars2.githubusercontent.com/u/532108?v=4","gravatar_id":"","url":"https://api.github.com/users/Spikhalskiy","html_url":"https://github.com/Spikhalskiy","followers_url":"https://api.github.com/users/Spikhalskiy/followers","following_url":"https://api.github.com/users/Spikhalskiy/following{/other_user}","gists_url":"https://api.github.com/users/Spikhalskiy/gists{/gist_id}","starred_url":"https://api.github.com/users/Spikhalskiy/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Spikhalskiy/subscriptions","organizations_url":"https://api.github.com/users/Spikhalskiy/orgs","repos_url":"https://api.github.com/users/Spikhalskiy/repos","events_url":"https://api.github.com/users/Spikhalskiy/events{/privacy}","received_events_url":"https://api.github.com/users/Spikhalskiy/received_events","type":"User","site_admin":false}}", "commentIds":["229141902","229145249"], "labels":[]}