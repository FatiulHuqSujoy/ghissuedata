{"id":"8179", "title":"java.lang.UnsatisfiedLinkError: no netty_transport_native_kqueue_x86_64 in java.library.path", "body":"### Minimal yet complete reproducer code (or URL to code)
when use redisTemplate
here is maven pom.xml
<dependency>
            <groupId>io.netty</groupId>
            <artifactId>netty-transport-native-kqueue</artifactId>
            <version>${netty.version}</version>
            <classifier>${os.detected.name}-${os.detected.arch}</classifier>
</dependency>
<extensions>
            <extension>
                <groupId>kr.motd.maven</groupId>
                <artifactId>os-maven-plugin</artifactId>
                <version>1.5.0.Final</version>
            </extension>
</extensions>

### Netty version
4.1.28.Final
### JVM version (e.g. `java -version`)
java version \"1.8.0_171\"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
### OS version (e.g. `uname -a`)
ProductName:	Mac OS X
ProductVersion:	10.13.2
BuildVersion:	17C88", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8179","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8179/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8179/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8179/events","html_url":"https://github.com/netty/netty/issues/8179","id":348548638,"node_id":"MDU6SXNzdWUzNDg1NDg2Mzg=","number":8179,"title":"java.lang.UnsatisfiedLinkError: no netty_transport_native_kqueue_x86_64 in java.library.path","user":{"login":"Mrchenli","id":20294244,"node_id":"MDQ6VXNlcjIwMjk0MjQ0","avatar_url":"https://avatars0.githubusercontent.com/u/20294244?v=4","gravatar_id":"","url":"https://api.github.com/users/Mrchenli","html_url":"https://github.com/Mrchenli","followers_url":"https://api.github.com/users/Mrchenli/followers","following_url":"https://api.github.com/users/Mrchenli/following{/other_user}","gists_url":"https://api.github.com/users/Mrchenli/gists{/gist_id}","starred_url":"https://api.github.com/users/Mrchenli/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Mrchenli/subscriptions","organizations_url":"https://api.github.com/users/Mrchenli/orgs","repos_url":"https://api.github.com/users/Mrchenli/repos","events_url":"https://api.github.com/users/Mrchenli/events{/privacy}","received_events_url":"https://api.github.com/users/Mrchenli/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-08-08T02:08:14Z","updated_at":"2018-11-06T06:18:10Z","closed_at":"2018-08-10T07:39:34Z","author_association":"NONE","body":"### Minimal yet complete reproducer code (or URL to code)\r\nwhen use redisTemplate\r\nhere is maven pom.xml\r\n<dependency>\r\n            <groupId>io.netty</groupId>\r\n            <artifactId>netty-transport-native-kqueue</artifactId>\r\n            <version>${netty.version}</version>\r\n            <classifier>${os.detected.name}-${os.detected.arch}</classifier>\r\n</dependency>\r\n<extensions>\r\n            <extension>\r\n                <groupId>kr.motd.maven</groupId>\r\n                <artifactId>os-maven-plugin</artifactId>\r\n                <version>1.5.0.Final</version>\r\n            </extension>\r\n</extensions>\r\n\r\n### Netty version\r\n4.1.28.Final\r\n### JVM version (e.g. `java -version`)\r\njava version \"1.8.0_171\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_171-b11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)\r\n### OS version (e.g. `uname -a`)\r\nProductName:\tMac OS X\r\nProductVersion:\t10.13.2\r\nBuildVersion:\t17C88","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["411666205","411956608","411956929","412002541","412012319","436143309"], "labels":[]}