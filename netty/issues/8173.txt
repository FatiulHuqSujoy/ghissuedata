{"id":"8173", "title":"Connection closed prematurely while using netty under the hood.", "body":"In our gateway implementation we are handling ~500k req/hour and from time to time we are getting \"Connection closed prematurely\" exception with the stack trace given below.
```
java.io.IOException: Connection closed prematurely
	at reactor.ipc.netty.http.client.HttpClientOperations.onInboundClose(HttpClientOperations.java:269) [reactor-netty-0.7.8.RELEASE.jar:0.7.8.RELEASE]
	at reactor.ipc.netty.channel.ChannelOperationsHandler.channelInactive(ChannelOperationsHandler.java:113) [reactor-netty-0.7.8.RELEASE.jar:0.7.8.RELEASE]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.CombinedChannelDuplexHandler$DelegatingChannelHandlerContext.fireChannelInactive(CombinedChannelDuplexHandler.java:420) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.handler.codec.ByteToMessageDecoder.channelInputClosed(ByteToMessageDecoder.java:377) [netty-codec-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.handler.codec.ByteToMessageDecoder.channelInactive(ByteToMessageDecoder.java:342) [netty-codec-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.handler.codec.http.HttpClientCodec$Decoder.channelInactive(HttpClientCodec.java:282) [netty-codec-http-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.CombinedChannelDuplexHandler.channelInactive(CombinedChannelDuplexHandler.java:223) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.handler.logging.LoggingHandler.channelInactive(LoggingHandler.java:167) [netty-handler-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.DefaultChannelPipeline$HeadContext.channelInactive(DefaultChannelPipeline.java:1429) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.DefaultChannelPipeline.fireChannelInactive(DefaultChannelPipeline.java:947) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.AbstractChannel$AbstractUnsafe$8.run(AbstractChannel.java:822) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute$$$capture(AbstractEventExecutor.java:163) [netty-common-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java) [netty-common-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404) [netty-common-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:464) [netty-transport-4.1.27.Final.jar:4.1.27.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:884) [netty-common-4.1.27.Final.jar:4.1.27.Final]
	at java.lang.Thread.run(Thread.java:748) [?:1.8.0_171]
```

Our gateway implementation is based on Spring Cloud Gateway / Reactor Netty and Netty under the hood.

### spring cloud gateway version
```
2.0.1.RELEASE
```
### reactor netty version
```
0.7.8.RELEASE
```
### netty version
```
4.1.27.Final
```

### jvm version
```
java version \"1.8.0_171\"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
```

### os version
```
Darwin mertcaliskan 17.7.0 Darwin Kernel Version 17.7.0: Thu Jun 21 22:53:14 PDT 2018; root:xnu-4570.71.2~1/RELEASE_X86_64 x86_64
```

When we set logging level for netty to debug mode, we got the log excerpt given below for the channel 0x91dae710, which is between our _gateway_ and _origin_.

```
1. 18/08/03 14:41:37.520 DEBUG: (:) Created new pooled channel [id: 0x91dae710], now 20 active connections

2. 18/08/03 14:41:37.520 DEBUG: (:) [id: 0x91dae710] REGISTERED

3. 18/08/03 14:41:37.572 DEBUG: (:) [id: 0x91dae710] CONNECT: /192.168.2.59:8080

4. 18/08/03 14:41:37.589 DEBUG: (:) Acquired [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080], now 116 active connections

5. 18/08/03 14:41:37.591 DEBUG: (:) Acquired active channel: [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080]

6. 18/08/03 14:41:37.591 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] ACTIVE

7. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] USER_EVENT: reactor.ipc.netty.NettyPipeline$SendOptionsChangeEvent@759fc92a

8. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] New sending options

9. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] Writing object DefaultHttpRequest(decodeResult: success, version: HTTP/1.1)
GET /premature/05b7f188-034f-4f1f-b054-0289574b9521 HTTP/1.1
gatling-user-id: 05b7f188-034f-4f1f-b054-0289574b9521
Accept: */*
X-OG-Request-ID: d44730e4-c5ad-4612-98bd-f53c168ff994
Forwarded: proto=http;host=\"192.168.2.145:9009\";for=\"192.168.2.59:58021\"
X-Forwarded-For: 192.168.2.59
X-Forwarded-Proto: http
X-Forwarded-Port: 9009
X-Forwarded-Host: 192.168.2.145:9009
host: 192.168.2.59:8080

10. 18/08/03 14:41:37.608 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] WRITE: 404B
         +-------------------------------------------------+
         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |
+--------+-------------------------------------------------+----------------+
|00000000| 47 45 54 20 2f 70 72 65 6d 61 74 75 72 65 2f 30 |GET /premature/0|
|00000010| 35 62 37 66 31 38 38 2d 30 33 34 66 2d 34 66 31 |5b7f188-034f-4f1|
|00000020| 66 2d 62 30 35 34 2d 30 32 38 39 35 37 34 62 39 |f-b054-0289574b9|
|00000030| 35 32 31 20 48 54 54 50 2f 31 2e 31 0d 0a 67 61 |521 HTTP/1.1..ga|
|00000040| 74 6c 69 6e 67 2d 75 73 65 72 2d 69 64 3a 20 30 |tling-user-id: 0|
|00000050| 35 62 37 66 31 38 38 2d 30 33 34 66 2d 34 66 31 |5b7f188-034f-4f1|
|00000060| 66 2d 62 30 35 34 2d 30 32 38 39 35 37 34 62 39 |f-b054-0289574b9|
|00000070| 35 32 31 0d 0a 41 63 63 65 70 74 3a 20 2a 2f 2a |521..Accept: */*|
|00000080| 0d 0a 58 2d 4f 47 2d 52 65 71 75 65 73 74 2d 49 |..X-OG-Request-I|
|00000090| 44 3a 20 64 34 34 37 33 30 65 34 2d 63 35 61 64 |D: d44730e4-c5ad|
|000000a0| 2d 34 36 31 32 2d 39 38 62 64 2d 66 35 33 63 31 |-4612-98bd-f53c1|
|000000b0| 36 38 66 66 39 39 34 0d 0a 46 6f 72 77 61 72 64 |68ff994..Forward|
|000000c0| 65 64 3a 20 70 72 6f 74 6f 3d 68 74 74 70 3b 68 |ed: proto=http;h|
|000000d0| 6f 73 74 3d 22 31 39 32 2e 31 36 38 2e 32 2e 31 |ost=\"192.168.2.1|
|000000e0| 34 35 3a 39 30 30 39 22 3b 66 6f 72 3d 22 31 39 |45:9009\";for=\"19|
|000000f0| 32 2e 31 36 38 2e 32 2e 35 39 3a 35 38 30 32 31 |2.168.2.59:58021|
|00000100| 22 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 46 |\"..X-Forwarded-F|
|00000110| 6f 72 3a 20 31 39 32 2e 31 36 38 2e 32 2e 35 39 |or: 192.168.2.59|
|00000120| 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 50 72 |..X-Forwarded-Pr|
|00000130| 6f 74 6f 3a 20 68 74 74 70 0d 0a 58 2d 46 6f 72 |oto: http..X-For|
|00000140| 77 61 72 64 65 64 2d 50 6f 72 74 3a 20 39 30 30 |warded-Port: 900|
|00000150| 39 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 48 |9..X-Forwarded-H|
|00000160| 6f 73 74 3a 20 31 39 32 2e 31 36 38 2e 32 2e 31 |ost: 192.168.2.1|
|00000170| 34 35 3a 39 30 30 39 0d 0a 68 6f 73 74 3a 20 31 |45:9009..host: 1|
|00000180| 39 32 2e 31 36 38 2e 32 2e 35 39 3a 38 30 38 30 |92.168.2.59:8080|
|00000190| 0d 0a 0d 0a                                     |....            |
+--------+-------------------------------------------------+----------------+

11. 18/08/03 14:41:37.614 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] FLUSH

12. 18/08/03 14:41:37.614 DEBUG: (:) Released [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080], now 167 active connections

13. 18/08/03 14:41:37.724 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080] INACTIVE

14. 18/08/03 14:42:50.968 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080] UNREGISTERED
```

The content printed in **10** is incomplete, so I think I should also be seeing EmptyLastHttpContent to be sent to the origin like this (this is a sample from another channel): 
``` 
2018-08-03 17:27:59.161 DEBUG 73695 --- [ctor-http-nio-3] r.i.n.c.ChannelOperationsHandler         : [id: 0x0ed2ae25, L:/192.168.2.145:50793 - R:/192.168.2.59:8080] Writing object EmptyLastHttpContent
```
And before sending the request the channel gets flushed in **11**.", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8173","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8173/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8173/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8173/events","html_url":"https://github.com/netty/netty/issues/8173","id":347429615,"node_id":"MDU6SXNzdWUzNDc0Mjk2MTU=","number":8173,"title":"Connection closed prematurely while using netty under the hood.","user":{"login":"mulderbaba","id":603285,"node_id":"MDQ6VXNlcjYwMzI4NQ==","avatar_url":"https://avatars3.githubusercontent.com/u/603285?v=4","gravatar_id":"","url":"https://api.github.com/users/mulderbaba","html_url":"https://github.com/mulderbaba","followers_url":"https://api.github.com/users/mulderbaba/followers","following_url":"https://api.github.com/users/mulderbaba/following{/other_user}","gists_url":"https://api.github.com/users/mulderbaba/gists{/gist_id}","starred_url":"https://api.github.com/users/mulderbaba/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mulderbaba/subscriptions","organizations_url":"https://api.github.com/users/mulderbaba/orgs","repos_url":"https://api.github.com/users/mulderbaba/repos","events_url":"https://api.github.com/users/mulderbaba/events{/privacy}","received_events_url":"https://api.github.com/users/mulderbaba/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-08-03T14:57:16Z","updated_at":"2018-08-17T17:20:51Z","closed_at":"2018-08-17T10:17:44Z","author_association":"NONE","body":"In our gateway implementation we are handling ~500k req/hour and from time to time we are getting \"Connection closed prematurely\" exception with the stack trace given below.\r\n```\r\njava.io.IOException: Connection closed prematurely\r\n\tat reactor.ipc.netty.http.client.HttpClientOperations.onInboundClose(HttpClientOperations.java:269) [reactor-netty-0.7.8.RELEASE.jar:0.7.8.RELEASE]\r\n\tat reactor.ipc.netty.channel.ChannelOperationsHandler.channelInactive(ChannelOperationsHandler.java:113) [reactor-netty-0.7.8.RELEASE.jar:0.7.8.RELEASE]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.CombinedChannelDuplexHandler$DelegatingChannelHandlerContext.fireChannelInactive(CombinedChannelDuplexHandler.java:420) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.channelInputClosed(ByteToMessageDecoder.java:377) [netty-codec-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.channelInactive(ByteToMessageDecoder.java:342) [netty-codec-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.handler.codec.http.HttpClientCodec$Decoder.channelInactive(HttpClientCodec.java:282) [netty-codec-http-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.CombinedChannelDuplexHandler.channelInactive(CombinedChannelDuplexHandler.java:223) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.handler.logging.LoggingHandler.channelInactive(LoggingHandler.java:167) [netty-handler-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.DefaultChannelPipeline$HeadContext.channelInactive(DefaultChannelPipeline.java:1429) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.DefaultChannelPipeline.fireChannelInactive(DefaultChannelPipeline.java:947) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.AbstractChannel$AbstractUnsafe$8.run(AbstractChannel.java:822) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.util.concurrent.AbstractEventExecutor.safeExecute$$$capture(AbstractEventExecutor.java:163) [netty-common-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java) [netty-common-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404) [netty-common-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:464) [netty-transport-4.1.27.Final.jar:4.1.27.Final]\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:884) [netty-common-4.1.27.Final.jar:4.1.27.Final]\r\n\tat java.lang.Thread.run(Thread.java:748) [?:1.8.0_171]\r\n```\r\n\r\nOur gateway implementation is based on Spring Cloud Gateway / Reactor Netty and Netty under the hood.\r\n\r\n### spring cloud gateway version\r\n```\r\n2.0.1.RELEASE\r\n```\r\n### reactor netty version\r\n```\r\n0.7.8.RELEASE\r\n```\r\n### netty version\r\n```\r\n4.1.27.Final\r\n```\r\n\r\n### jvm version\r\n```\r\njava version \"1.8.0_171\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_171-b11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)\r\n```\r\n\r\n### os version\r\n```\r\nDarwin mertcaliskan 17.7.0 Darwin Kernel Version 17.7.0: Thu Jun 21 22:53:14 PDT 2018; root:xnu-4570.71.2~1/RELEASE_X86_64 x86_64\r\n```\r\n\r\nWhen we set logging level for netty to debug mode, we got the log excerpt given below for the channel 0x91dae710, which is between our _gateway_ and _origin_.\r\n\r\n```\r\n1. 18/08/03 14:41:37.520 DEBUG: (:) Created new pooled channel [id: 0x91dae710], now 20 active connections\r\n\r\n2. 18/08/03 14:41:37.520 DEBUG: (:) [id: 0x91dae710] REGISTERED\r\n\r\n3. 18/08/03 14:41:37.572 DEBUG: (:) [id: 0x91dae710] CONNECT: /192.168.2.59:8080\r\n\r\n4. 18/08/03 14:41:37.589 DEBUG: (:) Acquired [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080], now 116 active connections\r\n\r\n5. 18/08/03 14:41:37.591 DEBUG: (:) Acquired active channel: [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080]\r\n\r\n6. 18/08/03 14:41:37.591 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] ACTIVE\r\n\r\n7. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] USER_EVENT: reactor.ipc.netty.NettyPipeline$SendOptionsChangeEvent@759fc92a\r\n\r\n8. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] New sending options\r\n\r\n9. 18/08/03 14:41:37.607 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] Writing object DefaultHttpRequest(decodeResult: success, version: HTTP/1.1)\r\nGET /premature/05b7f188-034f-4f1f-b054-0289574b9521 HTTP/1.1\r\ngatling-user-id: 05b7f188-034f-4f1f-b054-0289574b9521\r\nAccept: */*\r\nX-OG-Request-ID: d44730e4-c5ad-4612-98bd-f53c168ff994\r\nForwarded: proto=http;host=\"192.168.2.145:9009\";for=\"192.168.2.59:58021\"\r\nX-Forwarded-For: 192.168.2.59\r\nX-Forwarded-Proto: http\r\nX-Forwarded-Port: 9009\r\nX-Forwarded-Host: 192.168.2.145:9009\r\nhost: 192.168.2.59:8080\r\n\r\n10. 18/08/03 14:41:37.608 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] WRITE: 404B\r\n         +-------------------------------------------------+\r\n         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |\r\n+--------+-------------------------------------------------+----------------+\r\n|00000000| 47 45 54 20 2f 70 72 65 6d 61 74 75 72 65 2f 30 |GET /premature/0|\r\n|00000010| 35 62 37 66 31 38 38 2d 30 33 34 66 2d 34 66 31 |5b7f188-034f-4f1|\r\n|00000020| 66 2d 62 30 35 34 2d 30 32 38 39 35 37 34 62 39 |f-b054-0289574b9|\r\n|00000030| 35 32 31 20 48 54 54 50 2f 31 2e 31 0d 0a 67 61 |521 HTTP/1.1..ga|\r\n|00000040| 74 6c 69 6e 67 2d 75 73 65 72 2d 69 64 3a 20 30 |tling-user-id: 0|\r\n|00000050| 35 62 37 66 31 38 38 2d 30 33 34 66 2d 34 66 31 |5b7f188-034f-4f1|\r\n|00000060| 66 2d 62 30 35 34 2d 30 32 38 39 35 37 34 62 39 |f-b054-0289574b9|\r\n|00000070| 35 32 31 0d 0a 41 63 63 65 70 74 3a 20 2a 2f 2a |521..Accept: */*|\r\n|00000080| 0d 0a 58 2d 4f 47 2d 52 65 71 75 65 73 74 2d 49 |..X-OG-Request-I|\r\n|00000090| 44 3a 20 64 34 34 37 33 30 65 34 2d 63 35 61 64 |D: d44730e4-c5ad|\r\n|000000a0| 2d 34 36 31 32 2d 39 38 62 64 2d 66 35 33 63 31 |-4612-98bd-f53c1|\r\n|000000b0| 36 38 66 66 39 39 34 0d 0a 46 6f 72 77 61 72 64 |68ff994..Forward|\r\n|000000c0| 65 64 3a 20 70 72 6f 74 6f 3d 68 74 74 70 3b 68 |ed: proto=http;h|\r\n|000000d0| 6f 73 74 3d 22 31 39 32 2e 31 36 38 2e 32 2e 31 |ost=\"192.168.2.1|\r\n|000000e0| 34 35 3a 39 30 30 39 22 3b 66 6f 72 3d 22 31 39 |45:9009\";for=\"19|\r\n|000000f0| 32 2e 31 36 38 2e 32 2e 35 39 3a 35 38 30 32 31 |2.168.2.59:58021|\r\n|00000100| 22 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 46 |\"..X-Forwarded-F|\r\n|00000110| 6f 72 3a 20 31 39 32 2e 31 36 38 2e 32 2e 35 39 |or: 192.168.2.59|\r\n|00000120| 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 50 72 |..X-Forwarded-Pr|\r\n|00000130| 6f 74 6f 3a 20 68 74 74 70 0d 0a 58 2d 46 6f 72 |oto: http..X-For|\r\n|00000140| 77 61 72 64 65 64 2d 50 6f 72 74 3a 20 39 30 30 |warded-Port: 900|\r\n|00000150| 39 0d 0a 58 2d 46 6f 72 77 61 72 64 65 64 2d 48 |9..X-Forwarded-H|\r\n|00000160| 6f 73 74 3a 20 31 39 32 2e 31 36 38 2e 32 2e 31 |ost: 192.168.2.1|\r\n|00000170| 34 35 3a 39 30 30 39 0d 0a 68 6f 73 74 3a 20 31 |45:9009..host: 1|\r\n|00000180| 39 32 2e 31 36 38 2e 32 2e 35 39 3a 38 30 38 30 |92.168.2.59:8080|\r\n|00000190| 0d 0a 0d 0a                                     |....            |\r\n+--------+-------------------------------------------------+----------------+\r\n\r\n11. 18/08/03 14:41:37.614 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 - R:/192.168.2.59:8080] FLUSH\r\n\r\n12. 18/08/03 14:41:37.614 DEBUG: (:) Released [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080], now 167 active connections\r\n\r\n13. 18/08/03 14:41:37.724 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080] INACTIVE\r\n\r\n14. 18/08/03 14:42:50.968 DEBUG: (:) [id: 0x91dae710, L:/192.168.2.145:61702 ! R:/192.168.2.59:8080] UNREGISTERED\r\n```\r\n\r\nThe content printed in **10** is incomplete, so I think I should also be seeing EmptyLastHttpContent to be sent to the origin like this (this is a sample from another channel): \r\n``` \r\n2018-08-03 17:27:59.161 DEBUG 73695 --- [ctor-http-nio-3] r.i.n.c.ChannelOperationsHandler         : [id: 0x0ed2ae25, L:/192.168.2.145:50793 - R:/192.168.2.59:8080] Writing object EmptyLastHttpContent\r\n```\r\nAnd before sending the request the channel gets flushed in **11**.","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["413822528","413932958"], "labels":[]}