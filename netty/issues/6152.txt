{"id":"6152", "title":"HttpProxyHandler incorrectly formats IPv6 host and port in CONNECT request", "body":"### Expected behavior
The HttpProxyHandler is expected to be capable of issuing a valid CONNECT request for a tunneled connection to an IPv6 host. In this case we are passing an IPv6 address (eg fd00:c0de:42::c:293a:5736) rather than a host name.

### Actual behavior
The HttpProxyHandler does not properly concatenate the IPv6 address and port. The resulting error after we fail to connect will show you the problem:

```
io.netty.handler.proxy.ProxyConnectException: http, none, /fd00:c0de:42:0:5:562d:d54:1:3128 => /fd00:c0de:42:0:50:5694:2fda:1:4287, status: 503 Service Unavailable
```

In both cases you can see the IPv6 address is formatted without brackets: `fd00:c0de:42:0:50:5694:2fda:1:4287` should be `[fd00:c0de:42:0:50:5694:2fda:1]:4287`. This is just an exception message so it doesn't prove it's formatting incorrectly. However, if you look at the request on the wire you can see it is certainly wrong:

```
    CONNECT fd00:c0de:42:0:50:5694:2fda:1:4287 HTTP/1.1\\r\\n
    host: fd00:c0de:42:0:50:5694:2fda:1:4287\\r\\n
    \\r\\n
```

Here is the problem method from HttpProxyHandler:

```
    @Override
    protected Object newInitialMessage(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress raddr = destinationAddress();
        String rhost;
        if (raddr.isUnresolved()) {
            rhost = raddr.getHostString();
        } else {
            rhost = raddr.getAddress().getHostAddress();
        }

        final String host = rhost + ':' + raddr.getPort();
        FullHttpRequest req = new DefaultFullHttpRequest(
                HttpVersion.HTTP_1_1, HttpMethod.CONNECT,
                host,
                Unpooled.EMPTY_BUFFER, false);

        req.headers().set(HttpHeaderNames.HOST, host);

        if (authorization != null) {
            req.headers().set(HttpHeaderNames.PROXY_AUTHORIZATION, authorization);
        }

        return req;
    }
```

Specifically: ` final String host = rhost + ':' + raddr.getPort();`

### Steps to reproduce
* Setup an HTTP Proxy with IPv6.
* Setup a target server with an IPv6 address.
* Attempt to establish a connection to the target server through the proxy, giving the HttpProxyHandler an IPv6 IP address for the target.
** We can successfully connect to the proxy server with IPv6. The problem seems to be specific to the target of the tunneled connection using an IPv6 address.

### Minimal yet complete reproducer code (or URL to code)
* I am not able to create a reproducer or patch with my company at this time. I think the issue is relatively straightforward though.

### Netty version
4.1.4.Final

### JVM version (e.g. `java -version`)
Java 8 update 92

### OS version (e.g. `uname -a`)
Attempted on Ubuntu Linux 14.04 and Mac OSX. I don't think the OS matters in this case.", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6152","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6152/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6152/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6152/events","html_url":"https://github.com/netty/netty/issues/6152","id":197017382,"node_id":"MDU6SXNzdWUxOTcwMTczODI=","number":6152,"title":"HttpProxyHandler incorrectly formats IPv6 host and port in CONNECT request","user":{"login":"ZuluForce","id":802769,"node_id":"MDQ6VXNlcjgwMjc2OQ==","avatar_url":"https://avatars0.githubusercontent.com/u/802769?v=4","gravatar_id":"","url":"https://api.github.com/users/ZuluForce","html_url":"https://github.com/ZuluForce","followers_url":"https://api.github.com/users/ZuluForce/followers","following_url":"https://api.github.com/users/ZuluForce/following{/other_user}","gists_url":"https://api.github.com/users/ZuluForce/gists{/gist_id}","starred_url":"https://api.github.com/users/ZuluForce/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ZuluForce/subscriptions","organizations_url":"https://api.github.com/users/ZuluForce/orgs","repos_url":"https://api.github.com/users/ZuluForce/repos","events_url":"https://api.github.com/users/ZuluForce/events{/privacy}","received_events_url":"https://api.github.com/users/ZuluForce/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/155","html_url":"https://github.com/netty/netty/milestone/155","labels_url":"https://api.github.com/repos/netty/netty/milestones/155/labels","id":2069740,"node_id":"MDk6TWlsZXN0b25lMjA2OTc0MA==","number":155,"title":"4.1.7.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":68,"state":"closed","created_at":"2016-10-14T09:12:18Z","updated_at":"2017-01-12T11:35:43Z","due_on":null,"closed_at":"2017-01-12T11:35:43Z"},"comments":7,"created_at":"2016-12-21T19:47:24Z","updated_at":"2017-01-12T06:52:00Z","closed_at":"2017-01-12T06:52:00Z","author_association":"NONE","body":"### Expected behavior\r\nThe HttpProxyHandler is expected to be capable of issuing a valid CONNECT request for a tunneled connection to an IPv6 host. In this case we are passing an IPv6 address (eg fd00:c0de:42::c:293a:5736) rather than a host name.\r\n\r\n### Actual behavior\r\nThe HttpProxyHandler does not properly concatenate the IPv6 address and port. The resulting error after we fail to connect will show you the problem:\r\n\r\n```\r\nio.netty.handler.proxy.ProxyConnectException: http, none, /fd00:c0de:42:0:5:562d:d54:1:3128 => /fd00:c0de:42:0:50:5694:2fda:1:4287, status: 503 Service Unavailable\r\n```\r\n\r\nIn both cases you can see the IPv6 address is formatted without brackets: `fd00:c0de:42:0:50:5694:2fda:1:4287` should be `[fd00:c0de:42:0:50:5694:2fda:1]:4287`. This is just an exception message so it doesn't prove it's formatting incorrectly. However, if you look at the request on the wire you can see it is certainly wrong:\r\n\r\n```\r\n    CONNECT fd00:c0de:42:0:50:5694:2fda:1:4287 HTTP/1.1\\r\\n\r\n    host: fd00:c0de:42:0:50:5694:2fda:1:4287\\r\\n\r\n    \\r\\n\r\n```\r\n\r\nHere is the problem method from HttpProxyHandler:\r\n\r\n```\r\n    @Override\r\n    protected Object newInitialMessage(ChannelHandlerContext ctx) throws Exception {\r\n        InetSocketAddress raddr = destinationAddress();\r\n        String rhost;\r\n        if (raddr.isUnresolved()) {\r\n            rhost = raddr.getHostString();\r\n        } else {\r\n            rhost = raddr.getAddress().getHostAddress();\r\n        }\r\n\r\n        final String host = rhost + ':' + raddr.getPort();\r\n        FullHttpRequest req = new DefaultFullHttpRequest(\r\n                HttpVersion.HTTP_1_1, HttpMethod.CONNECT,\r\n                host,\r\n                Unpooled.EMPTY_BUFFER, false);\r\n\r\n        req.headers().set(HttpHeaderNames.HOST, host);\r\n\r\n        if (authorization != null) {\r\n            req.headers().set(HttpHeaderNames.PROXY_AUTHORIZATION, authorization);\r\n        }\r\n\r\n        return req;\r\n    }\r\n```\r\n\r\nSpecifically: ` final String host = rhost + ':' + raddr.getPort();`\r\n\r\n### Steps to reproduce\r\n* Setup an HTTP Proxy with IPv6.\r\n* Setup a target server with an IPv6 address.\r\n* Attempt to establish a connection to the target server through the proxy, giving the HttpProxyHandler an IPv6 IP address for the target.\r\n** We can successfully connect to the proxy server with IPv6. The problem seems to be specific to the target of the tunneled connection using an IPv6 address.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n* I am not able to create a reproducer or patch with my company at this time. I think the issue is relatively straightforward though.\r\n\r\n### Netty version\r\n4.1.4.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\nJava 8 update 92\r\n\r\n### OS version (e.g. `uname -a`)\r\nAttempted on Ubuntu Linux 14.04 and Mac OSX. I don't think the OS matters in this case.","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["268621861","268627253","268627446","268628820","268747714","271574121","272090185"], "labels":["defect"]}