{"id":"4545", "title":"HTTP/2 UniformStreamByteDistributor stream window ignored", "body":"UniformStreamByteDistributor will write to a stream even if the stream's flow control window is negative. This is not allowed by the rfc.

https://tools.ietf.org/html/rfc7540#section-6.9.2

> A change to SETTINGS_INITIAL_WINDOW_SIZE can cause the available
>    space in a flow-control window to become negative.  A sender MUST
>    track the negative flow-control window and MUST NOT send new flow-
>    controlled frames until it receives WINDOW_UPDATE frames that cause
>    the flow-control window to become positive.

I guess the spec also means that you should not send new flow control frames until the flow-control window becomes non-negative? If the flow-control window is 0 and we have empty frames I don't think it should be a problem to send these.

While working on PR https://github.com/netty/netty/pull/4538 I developed the following test case to demonstrate the issue:

``` diff
diff --git a/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java b/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java
index 44010e4..c52b678 100644
--- a/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java
+++ b/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java
@@ -185,11 +185,29 @@ public class UniformStreamByteDistributorTest {
         verifyNoMoreInteractions(writer);
     }

+    @Test
+    public void streamWindowExhaustedDoesNotWrite() throws Http2Exception {
+        updateStream(STREAM_A, 0, true, false);
+        updateStream(STREAM_B, 0, true);
+        updateStream(STREAM_C, 0, true);
+        updateStream(STREAM_D, 0, true, false);
+
+        assertFalse(write(10));
+        verifyWrite(STREAM_B, 0);
+        verifyWrite(STREAM_C, 0);
+        verifyNoMoreInteractions(writer);
+    }
+
     private Http2Stream stream(int streamId) {
         return connection.stream(streamId);
     }

     private void updateStream(final int streamId, final int streamableBytes, final boolean hasFrame) {
+        updateStream(streamId, streamableBytes, hasFrame, hasFrame);
+    }
+
+    private void updateStream(final int streamId, final int streamableBytes, final boolean hasFrame,
+            final boolean isWriteAllowed) {
         final Http2Stream stream = stream(streamId);
         distributor.updateStreamableBytes(new StreamByteDistributor.StreamState() {
             @Override
@@ -206,6 +224,11 @@ public class UniformStreamByteDistributorTest {
             public boolean hasFrame() {
                 return hasFrame;
             }
+
+            @Override
+            public boolean isWriteAllowed() {
+                return isWriteAllowed;
+            }
         });
     }
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4545","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4545/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4545/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4545/events","html_url":"https://github.com/netty/netty/issues/4545","id":121146263,"node_id":"MDU6SXNzdWUxMjExNDYyNjM=","number":4545,"title":"HTTP/2 UniformStreamByteDistributor stream window ignored","user":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false},"assignees":[{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/138","html_url":"https://github.com/netty/netty/milestone/138","labels_url":"https://api.github.com/repos/netty/netty/milestones/138/labels","id":1671939,"node_id":"MDk6TWlsZXN0b25lMTY3MTkzOQ==","number":138,"title":"4.1.0.CR5","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":25,"state":"closed","created_at":"2016-03-29T14:33:08Z","updated_at":"2016-04-02T05:18:50Z","due_on":"2016-04-01T07:00:00Z","closed_at":"2016-03-29T19:16:49Z"},"comments":5,"created_at":"2015-12-09T02:25:07Z","updated_at":"2016-03-29T14:33:35Z","closed_at":"2015-12-23T18:14:36Z","author_association":"MEMBER","body":"UniformStreamByteDistributor will write to a stream even if the stream's flow control window is negative. This is not allowed by the rfc.\n\nhttps://tools.ietf.org/html/rfc7540#section-6.9.2\n\n> A change to SETTINGS_INITIAL_WINDOW_SIZE can cause the available\n>    space in a flow-control window to become negative.  A sender MUST\n>    track the negative flow-control window and MUST NOT send new flow-\n>    controlled frames until it receives WINDOW_UPDATE frames that cause\n>    the flow-control window to become positive.\n\nI guess the spec also means that you should not send new flow control frames until the flow-control window becomes non-negative? If the flow-control window is 0 and we have empty frames I don't think it should be a problem to send these.\n\nWhile working on PR https://github.com/netty/netty/pull/4538 I developed the following test case to demonstrate the issue:\n\n``` diff\ndiff --git a/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java b/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java\nindex 44010e4..c52b678 100644\n--- a/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java\n+++ b/codec-http2/src/test/java/io/netty/handler/codec/http2/UniformStreamByteDistributorTest.java\n@@ -185,11 +185,29 @@ public class UniformStreamByteDistributorTest {\n         verifyNoMoreInteractions(writer);\n     }\n\n+    @Test\n+    public void streamWindowExhaustedDoesNotWrite() throws Http2Exception {\n+        updateStream(STREAM_A, 0, true, false);\n+        updateStream(STREAM_B, 0, true);\n+        updateStream(STREAM_C, 0, true);\n+        updateStream(STREAM_D, 0, true, false);\n+\n+        assertFalse(write(10));\n+        verifyWrite(STREAM_B, 0);\n+        verifyWrite(STREAM_C, 0);\n+        verifyNoMoreInteractions(writer);\n+    }\n+\n     private Http2Stream stream(int streamId) {\n         return connection.stream(streamId);\n     }\n\n     private void updateStream(final int streamId, final int streamableBytes, final boolean hasFrame) {\n+        updateStream(streamId, streamableBytes, hasFrame, hasFrame);\n+    }\n+\n+    private void updateStream(final int streamId, final int streamableBytes, final boolean hasFrame,\n+            final boolean isWriteAllowed) {\n         final Http2Stream stream = stream(streamId);\n         distributor.updateStreamableBytes(new StreamByteDistributor.StreamState() {\n             @Override\n@@ -206,6 +224,11 @@ public class UniformStreamByteDistributorTest {\n             public boolean hasFrame() {\n                 return hasFrame;\n             }\n+\n+            @Override\n+            public boolean isWriteAllowed() {\n+                return isWriteAllowed;\n+            }\n         });\n     }\n```\n","closed_by":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}}", "commentIds":["163086422","163095098","163097075","163254537","165572835"], "labels":["defect"]}