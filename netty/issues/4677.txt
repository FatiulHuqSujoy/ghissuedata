{"id":"4677", "title":"HttpObjectEncoder can not encode chunked response", "body":"I'm tring HttpStaticFileServer with netty `5.0.0.Alpha2`, but not work, and I find that has two issues.
1. the `DefaultFullHttpResponse` implements `LastHttpContent`, after encode any of `DefaultFullHttpResponse` msg, `HttpObjectEncoder#encodeChunkedContent` will reset `state` to `ST_INIT`, then rest msg can not be encoded.
   
   ``` java
   private void encodeChunkedContent(ChannelHandlerContext ctx, Object msg, long contentLength, List<Object> out) {
       ...
       if (msg instanceof LastHttpContent) {
           ...
           state = ST_INIT; // Line 171
       } else {
           ...
       }
       ...
   }
   ```
2. the sample code not set `transfer-encoding` header to `chunked`. in HttpObjectEncoder will process as `chunked` response only the response msg has `chunked` header. 
   
   ``` java
   protected void encode(ChannelHandlerContext ctx, Object msg, List<Object> out) throws Exception {
       if (msg instanceof HttpMessage) {
           ...
           state = HttpHeaderUtil.isTransferEncodingChunked(m) ? ST_CONTENT_CHUNK : ST_CONTENT_NON_CHUNK; // Line 75
           ...
       }
   }
   ```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/4677","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/4677/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/4677/comments","events_url":"https://api.github.com/repos/netty/netty/issues/4677/events","html_url":"https://github.com/netty/netty/issues/4677","id":125628110,"node_id":"MDU6SXNzdWUxMjU2MjgxMTA=","number":4677,"title":"HttpObjectEncoder can not encode chunked response","user":{"login":"cpf624","id":2046027,"node_id":"MDQ6VXNlcjIwNDYwMjc=","avatar_url":"https://avatars2.githubusercontent.com/u/2046027?v=4","gravatar_id":"","url":"https://api.github.com/users/cpf624","html_url":"https://github.com/cpf624","followers_url":"https://api.github.com/users/cpf624/followers","following_url":"https://api.github.com/users/cpf624/following{/other_user}","gists_url":"https://api.github.com/users/cpf624/gists{/gist_id}","starred_url":"https://api.github.com/users/cpf624/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cpf624/subscriptions","organizations_url":"https://api.github.com/users/cpf624/orgs","repos_url":"https://api.github.com/users/cpf624/repos","events_url":"https://api.github.com/users/cpf624/events{/privacy}","received_events_url":"https://api.github.com/users/cpf624/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":12,"created_at":"2016-01-08T15:04:37Z","updated_at":"2016-01-11T09:37:42Z","closed_at":"2016-01-11T09:37:42Z","author_association":"NONE","body":"I'm tring HttpStaticFileServer with netty `5.0.0.Alpha2`, but not work, and I find that has two issues.\n1. the `DefaultFullHttpResponse` implements `LastHttpContent`, after encode any of `DefaultFullHttpResponse` msg, `HttpObjectEncoder#encodeChunkedContent` will reset `state` to `ST_INIT`, then rest msg can not be encoded.\n   \n   ``` java\n   private void encodeChunkedContent(ChannelHandlerContext ctx, Object msg, long contentLength, List<Object> out) {\n       ...\n       if (msg instanceof LastHttpContent) {\n           ...\n           state = ST_INIT; // Line 171\n       } else {\n           ...\n       }\n       ...\n   }\n   ```\n2. the sample code not set `transfer-encoding` header to `chunked`. in HttpObjectEncoder will process as `chunked` response only the response msg has `chunked` header. \n   \n   ``` java\n   protected void encode(ChannelHandlerContext ctx, Object msg, List<Object> out) throws Exception {\n       if (msg instanceof HttpMessage) {\n           ...\n           state = HttpHeaderUtil.isTransferEncodingChunked(m) ? ST_CONTENT_CHUNK : ST_CONTENT_NON_CHUNK; // Line 75\n           ...\n       }\n   }\n   ```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["170206253","170207986","170208150","170208212","170320657","170322285","170330002","170398977","170422522","170469781","170485839","170487740"], "labels":[]}