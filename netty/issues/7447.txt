{"id":"7447", "title":"Received Messages Look Like Certificate's Content in Android (SSL)", "body":"### Netty version
4.1.9
### OS version (e.g. `uname -a`)
Lollipop (5.0)

Please to following this code to initiate `SslContext` : 

`private void initContext () throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException {

        KeyStore key = KeyStore.getInstance(\"BKS\");
        key.load(keyIn, password.toCharArray());

        TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        factory.init(key);

        context = SslContextBuilder.forClient().trustManager(factory).build();
    }`

And the `pipeline` : 

`    public SslHandler getSslHandler (SocketChannel ch, String host, int port) {

        SslHandler handler = context.newHandler(ch.alloc(), host, port);
        handler.engine().setUseClientMode(true);

        return handler;

    }`

`    private ChannelInitializer<SocketChannel> createChannelInitializer(final SimpleChannelInboundHandler<String> clientHandler) {
        return new ChannelInitializer<SocketChannel>() {

            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                InputStream keyIn = context.getResources().openRawResource(R.raw.mtrax);
                SocketContextBuilder builder = new SocketContextBuilder(keyIn, \"changeit\");

                handler = builder.getSslHandler(ch, host, port);

                // Add the text line codec.
                pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
                pipeline.addLast(new StringDecoder(UTF8));
                pipeline.addLast(new StringEncoder(UTF8));
                pipeline.addLast(handler);

                // and then business logic.
                pipeline.addLast(clientHandler);
            }
        };
    }` 



Are there wrong implemetations in our code? I have been try all the way to solved our problem, because the received messages look like certificate's content when we use SSL. Please to be kindly to help us. 
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7447","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7447/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7447/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7447/events","html_url":"https://github.com/netty/netty/issues/7447","id":277021474,"node_id":"MDU6SXNzdWUyNzcwMjE0NzQ=","number":7447,"title":"Received Messages Look Like Certificate's Content in Android (SSL)","user":{"login":"kurniawanrizzki","id":12451603,"node_id":"MDQ6VXNlcjEyNDUxNjAz","avatar_url":"https://avatars3.githubusercontent.com/u/12451603?v=4","gravatar_id":"","url":"https://api.github.com/users/kurniawanrizzki","html_url":"https://github.com/kurniawanrizzki","followers_url":"https://api.github.com/users/kurniawanrizzki/followers","following_url":"https://api.github.com/users/kurniawanrizzki/following{/other_user}","gists_url":"https://api.github.com/users/kurniawanrizzki/gists{/gist_id}","starred_url":"https://api.github.com/users/kurniawanrizzki/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kurniawanrizzki/subscriptions","organizations_url":"https://api.github.com/users/kurniawanrizzki/orgs","repos_url":"https://api.github.com/users/kurniawanrizzki/repos","events_url":"https://api.github.com/users/kurniawanrizzki/events{/privacy}","received_events_url":"https://api.github.com/users/kurniawanrizzki/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2017-11-27T13:39:43Z","updated_at":"2017-12-20T06:34:33Z","closed_at":"2017-12-20T03:07:09Z","author_association":"NONE","body":"### Netty version\r\n4.1.9\r\n### OS version (e.g. `uname -a`)\r\nLollipop (5.0)\r\n\r\nPlease to following this code to initiate `SslContext` : \r\n\r\n`private void initContext () throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException {\r\n\r\n        KeyStore key = KeyStore.getInstance(\"BKS\");\r\n        key.load(keyIn, password.toCharArray());\r\n\r\n        TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());\r\n        factory.init(key);\r\n\r\n        context = SslContextBuilder.forClient().trustManager(factory).build();\r\n    }`\r\n\r\nAnd the `pipeline` : \r\n\r\n`    public SslHandler getSslHandler (SocketChannel ch, String host, int port) {\r\n\r\n        SslHandler handler = context.newHandler(ch.alloc(), host, port);\r\n        handler.engine().setUseClientMode(true);\r\n\r\n        return handler;\r\n\r\n    }`\r\n\r\n`    private ChannelInitializer<SocketChannel> createChannelInitializer(final SimpleChannelInboundHandler<String> clientHandler) {\r\n        return new ChannelInitializer<SocketChannel>() {\r\n\r\n            @Override\r\n            public void initChannel(SocketChannel ch) throws Exception {\r\n                ChannelPipeline pipeline = ch.pipeline();\r\n                InputStream keyIn = context.getResources().openRawResource(R.raw.mtrax);\r\n                SocketContextBuilder builder = new SocketContextBuilder(keyIn, \"changeit\");\r\n\r\n                handler = builder.getSslHandler(ch, host, port);\r\n\r\n                // Add the text line codec.\r\n                pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));\r\n                pipeline.addLast(new StringDecoder(UTF8));\r\n                pipeline.addLast(new StringEncoder(UTF8));\r\n                pipeline.addLast(handler);\r\n\r\n                // and then business logic.\r\n                pipeline.addLast(clientHandler);\r\n            }\r\n        };\r\n    }` \r\n\r\n\r\n\r\nAre there wrong implemetations in our code? I have been try all the way to solved our problem, because the received messages look like certificate's content when we use SSL. Please to be kindly to help us. \r\n","closed_by":{"login":"kurniawanrizzki","id":12451603,"node_id":"MDQ6VXNlcjEyNDUxNjAz","avatar_url":"https://avatars3.githubusercontent.com/u/12451603?v=4","gravatar_id":"","url":"https://api.github.com/users/kurniawanrizzki","html_url":"https://github.com/kurniawanrizzki","followers_url":"https://api.github.com/users/kurniawanrizzki/followers","following_url":"https://api.github.com/users/kurniawanrizzki/following{/other_user}","gists_url":"https://api.github.com/users/kurniawanrizzki/gists{/gist_id}","starred_url":"https://api.github.com/users/kurniawanrizzki/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kurniawanrizzki/subscriptions","organizations_url":"https://api.github.com/users/kurniawanrizzki/orgs","repos_url":"https://api.github.com/users/kurniawanrizzki/repos","events_url":"https://api.github.com/users/kurniawanrizzki/events{/privacy}","received_events_url":"https://api.github.com/users/kurniawanrizzki/received_events","type":"User","site_admin":false}}", "commentIds":["350739747","350938024","351953475","352952250","352977752"], "labels":[]}