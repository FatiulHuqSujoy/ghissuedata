{"id":"7977", "title":"[QUESTION] about the toHandle(int bitmapIdx)  method of PoolSubpage", "body":"    private long toHandle(int bitmapIdx) {
        return 0x4000000000000000L | (long) bitmapIdx << 32 | memoryMapIdx;
    }


When I study the code, at first I just cant figure out what is “ 0x4000000000000000L”  here for ??
Then I googled it, someone explained that it is used to help distinguish whether the init operation is for subpage or not:

    void initBuf(PooledByteBuf<T> buf, long handle, int reqCapacity) {
        int memoryMapIdx = memoryMapIdx(handle);
        int bitmapIdx = bitmapIdx(handle);
        if (bitmapIdx == 0) {
            byte val = value(memoryMapIdx);
            assert val == unusable : String.valueOf(val);
            buf.init(this, handle, runOffset(memoryMapIdx) + offset, reqCapacity, runLength(memoryMapIdx),
                     arena.parent.threadCache());
        } else {
            initBufWithSubpage(buf, handle, bitmapIdx, reqCapacity);
        }
    }

    private static int bitmapIdx(long handle) {
           return (int) (handle >>> Integer.SIZE);
    }

here if bitmapIdx == 0 means init normal buffer , if not then init subpage buffer.


    private void initBufWithSubpage(PooledByteBuf<T> buf, long handle, int bitmapIdx, int reqCapacity) {
        assert bitmapIdx != 0;
        int memoryMapIdx = memoryMapIdx(handle);PoolSubpage<T> subpage = subpages[subpageIdx(memoryMapIdx)];
        assert subpage.doNotDestroy;
        assert reqCapacity <= subpage.elemSize;

        buf.init(
            this, handle,
            runOffset(memoryMapIdx) + (bitmapIdx & 0x3FFFFFFF) * subpage.elemSize + offset,
                reqCapacity, subpage.elemSize, arena.parent.threadCache());
    }

so the only reason of this action( \"bitmapIdx & 0x3FFFFFFF\" ) is to eliminate the effect of  “ 0x4000000000000000L” , am I right？ 
Because there is no javadoc about this , so I think it's a little bit obscure to understand
Since I'm not a English native speaker ,maybe my expression is a bit clumsy，I‘m not sure if I make my point clearly?@trustin @normanmaurer ", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7977","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7977/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7977/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7977/events","html_url":"https://github.com/netty/netty/issues/7977","id":326906761,"node_id":"MDU6SXNzdWUzMjY5MDY3NjE=","number":7977,"title":"[QUESTION] about the toHandle(int bitmapIdx)  method of PoolSubpage","user":{"login":"zekaryu","id":3090134,"node_id":"MDQ6VXNlcjMwOTAxMzQ=","avatar_url":"https://avatars3.githubusercontent.com/u/3090134?v=4","gravatar_id":"","url":"https://api.github.com/users/zekaryu","html_url":"https://github.com/zekaryu","followers_url":"https://api.github.com/users/zekaryu/followers","following_url":"https://api.github.com/users/zekaryu/following{/other_user}","gists_url":"https://api.github.com/users/zekaryu/gists{/gist_id}","starred_url":"https://api.github.com/users/zekaryu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/zekaryu/subscriptions","organizations_url":"https://api.github.com/users/zekaryu/orgs","repos_url":"https://api.github.com/users/zekaryu/repos","events_url":"https://api.github.com/users/zekaryu/events{/privacy}","received_events_url":"https://api.github.com/users/zekaryu/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-05-28T06:05:29Z","updated_at":"2018-12-28T18:39:47Z","closed_at":"2018-12-28T18:39:47Z","author_association":"CONTRIBUTOR","body":"    private long toHandle(int bitmapIdx) {\r\n        return 0x4000000000000000L | (long) bitmapIdx << 32 | memoryMapIdx;\r\n    }\r\n\r\n\r\nWhen I study the code, at first I just cant figure out what is “ 0x4000000000000000L”  here for ??\r\nThen I googled it, someone explained that it is used to help distinguish whether the init operation is for subpage or not:\r\n\r\n    void initBuf(PooledByteBuf<T> buf, long handle, int reqCapacity) {\r\n        int memoryMapIdx = memoryMapIdx(handle);\r\n        int bitmapIdx = bitmapIdx(handle);\r\n        if (bitmapIdx == 0) {\r\n            byte val = value(memoryMapIdx);\r\n            assert val == unusable : String.valueOf(val);\r\n            buf.init(this, handle, runOffset(memoryMapIdx) + offset, reqCapacity, runLength(memoryMapIdx),\r\n                     arena.parent.threadCache());\r\n        } else {\r\n            initBufWithSubpage(buf, handle, bitmapIdx, reqCapacity);\r\n        }\r\n    }\r\n\r\n    private static int bitmapIdx(long handle) {\r\n           return (int) (handle >>> Integer.SIZE);\r\n    }\r\n\r\nhere if bitmapIdx == 0 means init normal buffer , if not then init subpage buffer.\r\n\r\n\r\n    private void initBufWithSubpage(PooledByteBuf<T> buf, long handle, int bitmapIdx, int reqCapacity) {\r\n        assert bitmapIdx != 0;\r\n        int memoryMapIdx = memoryMapIdx(handle);PoolSubpage<T> subpage = subpages[subpageIdx(memoryMapIdx)];\r\n        assert subpage.doNotDestroy;\r\n        assert reqCapacity <= subpage.elemSize;\r\n\r\n        buf.init(\r\n            this, handle,\r\n            runOffset(memoryMapIdx) + (bitmapIdx & 0x3FFFFFFF) * subpage.elemSize + offset,\r\n                reqCapacity, subpage.elemSize, arena.parent.threadCache());\r\n    }\r\n\r\nso the only reason of this action( \"bitmapIdx & 0x3FFFFFFF\" ) is to eliminate the effect of  “ 0x4000000000000000L” , am I right？ \r\nBecause there is no javadoc about this , so I think it's a little bit obscure to understand\r\nSince I'm not a English native speaker ,maybe my expression is a bit clumsy，I‘m not sure if I make my point clearly?@trustin @normanmaurer ","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["450407153"], "labels":[]}