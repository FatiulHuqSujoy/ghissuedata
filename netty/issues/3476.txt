{"id":"3476", "title":"Make TrafficCounter more general again", "body":"About a change in Netty 4.0.25:

I'm writing a **UDP** packet sender and I need to control the number of packets per second to send out.

I'm using TrafficCounter to control the rate to create packets. This rate controller doesn't use Netty's handler pipeline architecture. It's just a normal Java thread that loops forever. It:
- Sends a packet.
- Asks TrafficCounter how much time it should sleep.
- Sleeps.
- Send another packet.

TrafficCounter constructor:

``` java
TrafficCounter(
  AbstractTrafficShapingHandler trafficShapingHandler,
  ScheduledExecutorService executor,
  String name,
  long checkInterval
)
```

In Netty 4.0.24, I can create a TrafficCounter instance like this:

``` java
new TrafficCounter(
  null,
  Executors.newScheduledThreadPool(1),
  \"serviceName\",
  5000
)
```

From Netty 4.0.25, the above code will throw exception, saying that trafficShapingHandler is now required.

So I now have to create a dummy one like this:

``` java
new TrafficCounter(
  new ChannelTrafficShapingHandler(5000),
  Executors.newScheduledThreadPool(1),
  \"serviceName\",
  5000
)
```

Should trafficShapingHandler be allowed to be null as before? Or should another constructor that doesn't require a trafficShapingHandler be added to TrafficCounter? 
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/3476","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/3476/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/3476/comments","events_url":"https://api.github.com/repos/netty/netty/issues/3476/events","html_url":"https://github.com/netty/netty/issues/3476","id":60295935,"node_id":"MDU6SXNzdWU2MDI5NTkzNQ==","number":3476,"title":"Make TrafficCounter more general again","user":{"login":"ngocdaothanh","id":10095,"node_id":"MDQ6VXNlcjEwMDk1","avatar_url":"https://avatars1.githubusercontent.com/u/10095?v=4","gravatar_id":"","url":"https://api.github.com/users/ngocdaothanh","html_url":"https://github.com/ngocdaothanh","followers_url":"https://api.github.com/users/ngocdaothanh/followers","following_url":"https://api.github.com/users/ngocdaothanh/following{/other_user}","gists_url":"https://api.github.com/users/ngocdaothanh/gists{/gist_id}","starred_url":"https://api.github.com/users/ngocdaothanh/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ngocdaothanh/subscriptions","organizations_url":"https://api.github.com/users/ngocdaothanh/orgs","repos_url":"https://api.github.com/users/ngocdaothanh/repos","events_url":"https://api.github.com/users/ngocdaothanh/events{/privacy}","received_events_url":"https://api.github.com/users/ngocdaothanh/received_events","type":"User","site_admin":false},"labels":[{"id":185730,"node_id":"MDU6TGFiZWwxODU3MzA=","url":"https://api.github.com/repos/netty/netty/labels/feature","name":"feature","color":"009800","default":false}],"state":"closed","locked":false,"assignee":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false},"assignees":[{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/111","html_url":"https://github.com/netty/netty/milestone/111","labels_url":"https://api.github.com/repos/netty/netty/milestones/111/labels","id":999318,"node_id":"MDk6TWlsZXN0b25lOTk5MzE4","number":111,"title":"4.0.27.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":25,"state":"closed","created_at":"2015-02-27T20:06:24Z","updated_at":"2015-04-09T18:55:34Z","due_on":null,"closed_at":"2015-04-08T07:37:48Z"},"comments":3,"created_at":"2015-03-09T04:04:05Z","updated_at":"2015-03-09T10:32:41Z","closed_at":"2015-03-09T10:32:41Z","author_association":"CONTRIBUTOR","body":"About a change in Netty 4.0.25:\n\nI'm writing a **UDP** packet sender and I need to control the number of packets per second to send out.\n\nI'm using TrafficCounter to control the rate to create packets. This rate controller doesn't use Netty's handler pipeline architecture. It's just a normal Java thread that loops forever. It:\n- Sends a packet.\n- Asks TrafficCounter how much time it should sleep.\n- Sleeps.\n- Send another packet.\n\nTrafficCounter constructor:\n\n``` java\nTrafficCounter(\n  AbstractTrafficShapingHandler trafficShapingHandler,\n  ScheduledExecutorService executor,\n  String name,\n  long checkInterval\n)\n```\n\nIn Netty 4.0.24, I can create a TrafficCounter instance like this:\n\n``` java\nnew TrafficCounter(\n  null,\n  Executors.newScheduledThreadPool(1),\n  \"serviceName\",\n  5000\n)\n```\n\nFrom Netty 4.0.25, the above code will throw exception, saying that trafficShapingHandler is now required.\n\nSo I now have to create a dummy one like this:\n\n``` java\nnew TrafficCounter(\n  new ChannelTrafficShapingHandler(5000),\n  Executors.newScheduledThreadPool(1),\n  \"serviceName\",\n  5000\n)\n```\n\nShould trafficShapingHandler be allowed to be null as before? Or should another constructor that doesn't require a trafficShapingHandler be added to TrafficCounter? \n","closed_by":{"login":"ngocdaothanh","id":10095,"node_id":"MDQ6VXNlcjEwMDk1","avatar_url":"https://avatars1.githubusercontent.com/u/10095?v=4","gravatar_id":"","url":"https://api.github.com/users/ngocdaothanh","html_url":"https://github.com/ngocdaothanh","followers_url":"https://api.github.com/users/ngocdaothanh/followers","following_url":"https://api.github.com/users/ngocdaothanh/following{/other_user}","gists_url":"https://api.github.com/users/ngocdaothanh/gists{/gist_id}","starred_url":"https://api.github.com/users/ngocdaothanh/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ngocdaothanh/subscriptions","organizations_url":"https://api.github.com/users/ngocdaothanh/orgs","repos_url":"https://api.github.com/users/ngocdaothanh/repos","events_url":"https://api.github.com/users/ngocdaothanh/events{/privacy}","received_events_url":"https://api.github.com/users/ngocdaothanh/received_events","type":"User","site_admin":false}}", "commentIds":["77802250","77805705","77830497"], "labels":["feature"]}