{"id":"7538", "title":"Why custom ThreadFactory is slow ?", "body":"### Netty version: 4.1.18.Final
### JVM version: Java HotSpot(TM) 64-Bit Server VM (build 9+181, mixed mode)
### OS version: Windows 10

new NioEventLoopGroup(0, new io.netty.util.concurrent.DefaultThreadFactory(NioEventLoopGroup.class))
is slow
jmh:
Warmup Iteration   2: 71.169 ops/ms
Warmup Iteration   3: 80.204 ops/ms
Warmup Iteration   4: 85.780 ops/ms
Warmup Iteration   5: 81.681 ops/ms
Iteration   1: 84.452 ops/ms
Iteration   2: 84.801 ops/ms
Iteration   3: 85.283 ops/ms
Iteration   4: 85.253 ops/ms
Iteration   5: 84.905 ops/ms

eventLoopGroup = new NioEventLoopGroup(0);
is fast
jmh:
Warmup Iteration   2: 74.367 ops/ms
Warmup Iteration   3: 98.894 ops/ms
Warmup Iteration   4: 102.583 ops/ms
Warmup Iteration   5: 98.344 ops/ms
Iteration   1: 105.881 ops/ms
Iteration   2: 106.302 ops/ms
Iteration   3: 107.134 ops/ms
Iteration   4: 107.732 ops/ms
Iteration   5: 107.150 ops/ms

everything else is the same
what's the reason ?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7538","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7538/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7538/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7538/events","html_url":"https://github.com/netty/netty/issues/7538","id":284114984,"node_id":"MDU6SXNzdWUyODQxMTQ5ODQ=","number":7538,"title":"Why custom ThreadFactory is slow ?","user":{"login":"hank-whu","id":1773861,"node_id":"MDQ6VXNlcjE3NzM4NjE=","avatar_url":"https://avatars2.githubusercontent.com/u/1773861?v=4","gravatar_id":"","url":"https://api.github.com/users/hank-whu","html_url":"https://github.com/hank-whu","followers_url":"https://api.github.com/users/hank-whu/followers","following_url":"https://api.github.com/users/hank-whu/following{/other_user}","gists_url":"https://api.github.com/users/hank-whu/gists{/gist_id}","starred_url":"https://api.github.com/users/hank-whu/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hank-whu/subscriptions","organizations_url":"https://api.github.com/users/hank-whu/orgs","repos_url":"https://api.github.com/users/hank-whu/repos","events_url":"https://api.github.com/users/hank-whu/events{/privacy}","received_events_url":"https://api.github.com/users/hank-whu/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2017-12-22T09:30:15Z","updated_at":"2018-05-30T20:06:00Z","closed_at":"2018-05-30T20:06:00Z","author_association":"NONE","body":"### Netty version: 4.1.18.Final\r\n### JVM version: Java HotSpot(TM) 64-Bit Server VM (build 9+181, mixed mode)\r\n### OS version: Windows 10\r\n\r\nnew NioEventLoopGroup(0, new io.netty.util.concurrent.DefaultThreadFactory(NioEventLoopGroup.class))\r\nis slow\r\njmh:\r\nWarmup Iteration   2: 71.169 ops/ms\r\nWarmup Iteration   3: 80.204 ops/ms\r\nWarmup Iteration   4: 85.780 ops/ms\r\nWarmup Iteration   5: 81.681 ops/ms\r\nIteration   1: 84.452 ops/ms\r\nIteration   2: 84.801 ops/ms\r\nIteration   3: 85.283 ops/ms\r\nIteration   4: 85.253 ops/ms\r\nIteration   5: 84.905 ops/ms\r\n\r\neventLoopGroup = new NioEventLoopGroup(0);\r\nis fast\r\njmh:\r\nWarmup Iteration   2: 74.367 ops/ms\r\nWarmup Iteration   3: 98.894 ops/ms\r\nWarmup Iteration   4: 102.583 ops/ms\r\nWarmup Iteration   5: 98.344 ops/ms\r\nIteration   1: 105.881 ops/ms\r\nIteration   2: 106.302 ops/ms\r\nIteration   3: 107.134 ops/ms\r\nIteration   4: 107.732 ops/ms\r\nIteration   5: 107.150 ops/ms\r\n\r\neverything else is the same\r\nwhat's the reason ?\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["353559786","353563288","353563721","353574103","353578636","353701129","353780105","353827228","354278946","354420097","354426099","354523219","393299895"], "labels":[]}