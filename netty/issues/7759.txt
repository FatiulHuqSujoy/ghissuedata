{"id":"7759", "title":"ObjectCleanerThread leaking", "body":"### Expected behavior

After using Netty, all Netty threads should terminate. This is the behavior of Netty 4.1.19. But ObjectCleanerThread leaks since 4.1.20.

### Actual behavior

ObjectCleanerThread only terminates when JVM terminates.

### Steps to reproduce

1. Start to use Netty (NettyRuntime.setAvailableProcessors, EventLoopGroup etc.)
2. Use Netty
3. Stop using Netty by eventLoopGroup.shutdownGracefully() 
4. ObjectCleanerThread still in thread list

### Minimal yet complete reproducer code (or URL to code)

```
  public void shutdown() {
        eventLoopGroup.shutdownGracefully();
        Set<Thread> allThreads = Thread.getAllStackTraces().keySet();
        for (Thread thread : allThreads) {
            if (thread.getName().equals(ObjectCleaner.class.getSimpleName() + \"Thread\")) {
                logger.info(\"found Netty object cleaner thread after shutdown\");
            }
        }
    }
```

### Netty version
4.1.20, 4.1.21, 4.1.22

### JVM version (e.g. `java -version`)
openjdk version \"9.0.4.1\"
OpenJDK Runtime Environment (Zulu build 9.0.4.1+11)
OpenJDK 64-Bit Server VM (Zulu build 9.0.4.1+11, mixed mode)

### OS version (e.g. `uname -a`)
Darwin Jorg-Prantes-MacBook-Pro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jan 11 22:59:40 PST 2018; root:xnu-3789.73.8~1/RELEASE_X86_64 x86_64", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7759","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7759/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7759/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7759/events","html_url":"https://github.com/netty/netty/issues/7759","id":301430502,"node_id":"MDU6SXNzdWUzMDE0MzA1MDI=","number":7759,"title":"ObjectCleanerThread leaking","user":{"login":"jprante","id":635745,"node_id":"MDQ6VXNlcjYzNTc0NQ==","avatar_url":"https://avatars1.githubusercontent.com/u/635745?v=4","gravatar_id":"","url":"https://api.github.com/users/jprante","html_url":"https://github.com/jprante","followers_url":"https://api.github.com/users/jprante/followers","following_url":"https://api.github.com/users/jprante/following{/other_user}","gists_url":"https://api.github.com/users/jprante/gists{/gist_id}","starred_url":"https://api.github.com/users/jprante/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jprante/subscriptions","organizations_url":"https://api.github.com/users/jprante/orgs","repos_url":"https://api.github.com/users/jprante/repos","events_url":"https://api.github.com/users/jprante/events{/privacy}","received_events_url":"https://api.github.com/users/jprante/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2018-03-01T14:44:49Z","updated_at":"2018-04-19T19:00:58Z","closed_at":"2018-04-19T19:00:58Z","author_association":"NONE","body":"### Expected behavior\r\n\r\nAfter using Netty, all Netty threads should terminate. This is the behavior of Netty 4.1.19. But ObjectCleanerThread leaks since 4.1.20.\r\n\r\n### Actual behavior\r\n\r\nObjectCleanerThread only terminates when JVM terminates.\r\n\r\n### Steps to reproduce\r\n\r\n1. Start to use Netty (NettyRuntime.setAvailableProcessors, EventLoopGroup etc.)\r\n2. Use Netty\r\n3. Stop using Netty by eventLoopGroup.shutdownGracefully() \r\n4. ObjectCleanerThread still in thread list\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n```\r\n  public void shutdown() {\r\n        eventLoopGroup.shutdownGracefully();\r\n        Set<Thread> allThreads = Thread.getAllStackTraces().keySet();\r\n        for (Thread thread : allThreads) {\r\n            if (thread.getName().equals(ObjectCleaner.class.getSimpleName() + \"Thread\")) {\r\n                logger.info(\"found Netty object cleaner thread after shutdown\");\r\n            }\r\n        }\r\n    }\r\n```\r\n\r\n### Netty version\r\n4.1.20, 4.1.21, 4.1.22\r\n\r\n### JVM version (e.g. `java -version`)\r\nopenjdk version \"9.0.4.1\"\r\nOpenJDK Runtime Environment (Zulu build 9.0.4.1+11)\r\nOpenJDK 64-Bit Server VM (Zulu build 9.0.4.1+11, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nDarwin Jorg-Prantes-MacBook-Pro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jan 11 22:59:40 PST 2018; root:xnu-3789.73.8~1/RELEASE_X86_64 x86_64","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["369794349","369862771","370282258","370319442","371437570","371523109","377269182","382845930"], "labels":[]}