{"id":"2542", "title":"HTTP post request decoder does not support quoted boundaries", "body":"I am facing a problem submitting a multipart form data post to a netty-based HTTP server (vert.x-based, really). The client submitting the form is using Poco C++ libraries to make the POST request. 

After some debugging we found out the problem was due to the client issuing a properly-formed HTTP request, (but?) using double quotes to enclose the boundary part. We checked that looking into the Poco C++ lib source code. 

Now, it seems according to the spec. quotes are optional and should be accepted when specifying the boundary:

  http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html

Section 19.2, bullet (2): 

> ```
>  2) Although RFC 2046 [40] permits the boundary string to be
>    quoted, some existing implementations handle a quoted boundary
>    string incorrectly.
> ```

This is the relevant mentioned RFC (http://www.ietf.org/rfc/rfc2046.txt) part:

>   WARNING TO IMPLEMENTORS:  The grammar for parameters on the Content-
>   type field is such that it is often necessary to enclose the boundary
>   parameter values in quotes on the Content-type line.  This is not
>   always necessary, but never hurts. Implementors should be sure to
>   study the grammar carefully in order to avoid producing invalid
>   Content-type fields.  Thus, a typical \"multipart\" Content-Type header
>   field might look like this:
> 
> ```
> Content-Type: multipart/mixed; boundary=gc0p4Jq0M2Yt08j34c0p
> ```
> 
>   But the following is not valid:
> 
> ```
> Content-Type: multipart/mixed; boundary=gc0pJq0M:08jU534c0p
> ```
> 
>   (because of the colon) and must instead be represented as
> 
> ```
> Content-Type: multipart/mixed; boundary=\"gc0pJq0M:08jU534c0p\"
> ```

The code in https://github.com/netty/netty/blob/master/codec-http/src/main/java/io/netty/handler/codec/http/multipart/HttpPostRequestDecoder.java#L179 effectively shows that the quoting is unsupported.

I think (and will feel grateful if) Netty should support quoted boundary specifiers.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/2542","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/2542/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/2542/comments","events_url":"https://api.github.com/repos/netty/netty/issues/2542/events","html_url":"https://github.com/netty/netty/issues/2542","id":35094323,"node_id":"MDU6SXNzdWUzNTA5NDMyMw==","number":2542,"title":"HTTP post request decoder does not support quoted boundaries","user":{"login":"MiguelGL","id":642181,"node_id":"MDQ6VXNlcjY0MjE4MQ==","avatar_url":"https://avatars0.githubusercontent.com/u/642181?v=4","gravatar_id":"","url":"https://api.github.com/users/MiguelGL","html_url":"https://github.com/MiguelGL","followers_url":"https://api.github.com/users/MiguelGL/followers","following_url":"https://api.github.com/users/MiguelGL/following{/other_user}","gists_url":"https://api.github.com/users/MiguelGL/gists{/gist_id}","starred_url":"https://api.github.com/users/MiguelGL/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/MiguelGL/subscriptions","organizations_url":"https://api.github.com/users/MiguelGL/orgs","repos_url":"https://api.github.com/users/MiguelGL/repos","events_url":"https://api.github.com/users/MiguelGL/events{/privacy}","received_events_url":"https://api.github.com/users/MiguelGL/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/90","html_url":"https://github.com/netty/netty/milestone/90","labels_url":"https://api.github.com/repos/netty/netty/milestones/90/labels","id":645294,"node_id":"MDk6TWlsZXN0b25lNjQ1Mjk0","number":90,"title":"4.0.20.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":34,"state":"closed","created_at":"2014-04-30T18:13:34Z","updated_at":"2014-06-21T04:21:30Z","due_on":null,"closed_at":"2014-06-21T04:21:30Z"},"comments":16,"created_at":"2014-06-05T20:31:39Z","updated_at":"2014-06-09T09:08:08Z","closed_at":"2014-06-09T09:08:05Z","author_association":"NONE","body":"I am facing a problem submitting a multipart form data post to a netty-based HTTP server (vert.x-based, really). The client submitting the form is using Poco C++ libraries to make the POST request. \n\nAfter some debugging we found out the problem was due to the client issuing a properly-formed HTTP request, (but?) using double quotes to enclose the boundary part. We checked that looking into the Poco C++ lib source code. \n\nNow, it seems according to the spec. quotes are optional and should be accepted when specifying the boundary:\n\n  http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html\n\nSection 19.2, bullet (2): \n\n> ```\n>  2) Although RFC 2046 [40] permits the boundary string to be\n>    quoted, some existing implementations handle a quoted boundary\n>    string incorrectly.\n> ```\n\nThis is the relevant mentioned RFC (http://www.ietf.org/rfc/rfc2046.txt) part:\n\n>   WARNING TO IMPLEMENTORS:  The grammar for parameters on the Content-\n>   type field is such that it is often necessary to enclose the boundary\n>   parameter values in quotes on the Content-type line.  This is not\n>   always necessary, but never hurts. Implementors should be sure to\n>   study the grammar carefully in order to avoid producing invalid\n>   Content-type fields.  Thus, a typical \"multipart\" Content-Type header\n>   field might look like this:\n> \n> ```\n> Content-Type: multipart/mixed; boundary=gc0p4Jq0M2Yt08j34c0p\n> ```\n> \n>   But the following is not valid:\n> \n> ```\n> Content-Type: multipart/mixed; boundary=gc0pJq0M:08jU534c0p\n> ```\n> \n>   (because of the colon) and must instead be represented as\n> \n> ```\n> Content-Type: multipart/mixed; boundary=\"gc0pJq0M:08jU534c0p\"\n> ```\n\nThe code in https://github.com/netty/netty/blob/master/codec-http/src/main/java/io/netty/handler/codec/http/multipart/HttpPostRequestDecoder.java#L179 effectively shows that the quoting is unsupported.\n\nI think (and will feel grateful if) Netty should support quoted boundary specifiers.\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["45273447","45273901","45276990","45303423","45304739","45304905","45306727","45306994","45307230","45307359","45385459","45406767","45433346","45433540","45440482","45473101"], "labels":[]}