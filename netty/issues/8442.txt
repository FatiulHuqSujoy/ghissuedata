{"id":"8442", "title":"Custom cipher list with BoringSSL breaks in 4.1.31.Final ", "body":"### Expected behavior

TLS continues to work in 4.1.31.Final when providing an explicit list of ciphers via `SslContextBuilder.ciphers(...)`.

### Actual behavior

After upgrading, my `SslContextBuilder.build()` throws:

```
Caused by: java.lang.IllegalArgumentException: unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384(ECDHE-ECDSA-AES256-SHA384)
       at io.netty.handler.ssl.CipherSuiteConverter.convertToCipherStrings(CipherSuiteConverter.java:418)
       at io.netty.handler.ssl.ReferenceCountedOpenSslContext.<init>(ReferenceCountedOpenSslContext.java:252)
       ... 34 more
```

It looks like this is due to changes in #8293, in particular [this](https://github.com/netty/netty/blob/netty-4.1.31.Final/handler/src/main/java/io/netty/handler/ssl/CipherSuiteConverter.java#L418) check in `CipherSuiteConverter.convertToCipherStrings()` which throws if false. Should it just `continue` instead of throwing so that the current cipherSuite is excluded from the lists being built (since a later cipher in the list may be available)? I verified that it works as before with this modification.

### Minimal yet complete reproducer code (or URL to code)

```java
KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
kmf.init(null,null);
String[] ciphers = ((SSLSocketFactory)SSLSocketFactory.getDefault()).getDefaultCipherSuites();
SslContextBuilder.forServer(kmf).sslProvider(SslProvider.OPENSSL) 
    .ciphers(Arrays.asList(ciphers)).build();
```

### Netty version

4.1.31.Final

### JVM version (e.g. `java -version`)

```
java version \"1.8.0_181\"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```

### OS version (e.g. `uname -a`)

`Linux 3.10.0-862.14.4.el7.x86_64`
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8442","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8442/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8442/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8442/events","html_url":"https://github.com/netty/netty/issues/8442","id":375339355,"node_id":"MDU6SXNzdWUzNzUzMzkzNTU=","number":8442,"title":"Custom cipher list with BoringSSL breaks in 4.1.31.Final ","user":{"login":"njhill","id":16958488,"node_id":"MDQ6VXNlcjE2OTU4NDg4","avatar_url":"https://avatars3.githubusercontent.com/u/16958488?v=4","gravatar_id":"","url":"https://api.github.com/users/njhill","html_url":"https://github.com/njhill","followers_url":"https://api.github.com/users/njhill/followers","following_url":"https://api.github.com/users/njhill/following{/other_user}","gists_url":"https://api.github.com/users/njhill/gists{/gist_id}","starred_url":"https://api.github.com/users/njhill/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/njhill/subscriptions","organizations_url":"https://api.github.com/users/njhill/orgs","repos_url":"https://api.github.com/users/njhill/repos","events_url":"https://api.github.com/users/njhill/events{/privacy}","received_events_url":"https://api.github.com/users/njhill/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/194","html_url":"https://github.com/netty/netty/milestone/194","labels_url":"https://api.github.com/repos/netty/netty/milestones/194/labels","id":3780424,"node_id":"MDk6TWlsZXN0b25lMzc4MDQyNA==","number":194,"title":"4.1.32.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":71,"state":"closed","created_at":"2018-10-30T07:16:13Z","updated_at":"2018-12-06T08:04:49Z","due_on":null,"closed_at":"2018-11-29T16:43:13Z"},"comments":7,"created_at":"2018-10-30T06:27:51Z","updated_at":"2018-10-30T16:58:52Z","closed_at":"2018-10-30T16:58:43Z","author_association":"MEMBER","body":"### Expected behavior\r\n\r\nTLS continues to work in 4.1.31.Final when providing an explicit list of ciphers via `SslContextBuilder.ciphers(...)`.\r\n\r\n### Actual behavior\r\n\r\nAfter upgrading, my `SslContextBuilder.build()` throws:\r\n\r\n```\r\nCaused by: java.lang.IllegalArgumentException: unsupported cipher suite: TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384(ECDHE-ECDSA-AES256-SHA384)\r\n       at io.netty.handler.ssl.CipherSuiteConverter.convertToCipherStrings(CipherSuiteConverter.java:418)\r\n       at io.netty.handler.ssl.ReferenceCountedOpenSslContext.<init>(ReferenceCountedOpenSslContext.java:252)\r\n       ... 34 more\r\n```\r\n\r\nIt looks like this is due to changes in #8293, in particular [this](https://github.com/netty/netty/blob/netty-4.1.31.Final/handler/src/main/java/io/netty/handler/ssl/CipherSuiteConverter.java#L418) check in `CipherSuiteConverter.convertToCipherStrings()` which throws if false. Should it just `continue` instead of throwing so that the current cipherSuite is excluded from the lists being built (since a later cipher in the list may be available)? I verified that it works as before with this modification.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n```java\r\nKeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());\r\nkmf.init(null,null);\r\nString[] ciphers = ((SSLSocketFactory)SSLSocketFactory.getDefault()).getDefaultCipherSuites();\r\nSslContextBuilder.forServer(kmf).sslProvider(SslProvider.OPENSSL) \r\n    .ciphers(Arrays.asList(ciphers)).build();\r\n```\r\n\r\n### Netty version\r\n\r\n4.1.31.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\n```\r\njava version \"1.8.0_181\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_181-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)\r\n```\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\n`Linux 3.10.0-862.14.4.el7.x86_64`\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["434188804","434189496","434191032","434193052","434195248","434343388","434384557"], "labels":["not a bug"]}