{"id":"8287", "title":"Failed to mark a promise as success because it has failed already: DefaultChannelPromise@1f8f7703(failure: io.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1)", "body":"### Expected behavior
should not appear reference counting exception.

### Actual behavior

2018-09-14 00:54:48,492 WARN [client-io-thread-7]	Failed to mark a promise as success because it has failed already: DefaultChannelPromise@1f8f7703(failure: io.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1), unnotified cause:
io.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1
	at io.netty.handler.codec.MessageToMessageEncoder.write(MessageToMessageEncoder.java:106) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.CombinedChannelDuplexHandler.write(CombinedChannelDuplexHandler.java:348) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeWrite0(AbstractChannelHandlerContext.java:738) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeWrite(AbstractChannelHandlerContext.java:730) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext.access$1900(AbstractChannelHandlerContext.java:38) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext$AbstractWriteTask.write(AbstractChannelHandlerContext.java:1081) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext$WriteAndFlushTask.write(AbstractChannelHandlerContext.java:1128) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.AbstractChannelHandlerContext$AbstractWriteTask.run(AbstractChannelHandlerContext.java:1070) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java:163) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:446) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:884) [netty-all-4.1.29.Final.jar:4.1.29.Final]
	at java.lang.Thread.run(Thread.java:748) [?:1.8.0_171]
Caused by: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1
	at io.netty.buffer.AbstractReferenceCountedByteBuf.release0(AbstractReferenceCountedByteBuf.java:100) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:84) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.handler.codec.http.DefaultFullHttpRequest.release(DefaultFullHttpRequest.java:102) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.util.ReferenceCountUtil.release(ReferenceCountUtil.java:88) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	at io.netty.handler.codec.MessageToMessageEncoder.write(MessageToMessageEncoder.java:90) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]
	... 12 more

### Steps to reproduce
1. init bootstrap:
ch.pipeline().addLast(new HttpClientCodec());
ch.pipeline().addLast(new HttpObjectAggregator(10 * 1024 * 1024));
ch.pipeline().addLast(new RpcClientHandler(RpcClient.this));
2. channel.writeAndFlush(fullRequest);

### Minimal yet complete reproducer code (or URL to code)
// init netty bootstrap
        bootstrap = new Bootstrap();
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, rpcClientOptions.getConnectTimeoutMillis());
        bootstrap.option(ChannelOption.SO_KEEPALIVE, rpcClientOptions.isKeepAlive());
        bootstrap.option(ChannelOption.SO_REUSEADDR, rpcClientOptions.isReuseAddr());
        bootstrap.option(ChannelOption.TCP_NODELAY, rpcClientOptions.isTcpNoDelay());
        bootstrap.option(ChannelOption.SO_RCVBUF, rpcClientOptions.getReceiveBufferSize());
        bootstrap.option(ChannelOption.SO_SNDBUF, rpcClientOptions.getSendBufferSize());
        ChannelInitializer<SocketChannel> initializer = new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                if (rpcClientOptions.isHttp()) {
                    ch.pipeline().addLast(new HttpClientCodec());
                    ch.pipeline().addLast(new HttpObjectAggregator(10 * 1024 * 1024));
                } else {
                    ch.pipeline().addLast(new RpcRequestEncoder(RpcClient.this));
                    ch.pipeline().addLast(new RpcResponseDecoder(RpcClient.this));
                }
                ch.pipeline().addLast(new RpcClientHandler(RpcClient.this));
            }
        };
        bootstrap.group(new NioEventLoopGroup(
                options.getIoThreadNum(),
                new CustomThreadFactory(\"client-io-thread\")))
                .handler(initializer);

### Netty version
4.1.29

### JVM version (e.g. `java -version`)
java version \"1.8.0_171\"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)

### OS version (e.g. `uname -a`)
Darwin BDSHYF000127981 17.3.0 Darwin Kernel Version 17.3.0: Thu Nov  9 18:09:22 PST 2017; root:xnu-4570.31.3~1/RELEASE_X86_64 x86_64", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8287","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8287/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8287/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8287/events","html_url":"https://github.com/netty/netty/issues/8287","id":360130144,"node_id":"MDU6SXNzdWUzNjAxMzAxNDQ=","number":8287,"title":"Failed to mark a promise as success because it has failed already: DefaultChannelPromise@1f8f7703(failure: io.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1)","user":{"login":"wenweihu86","id":2144079,"node_id":"MDQ6VXNlcjIxNDQwNzk=","avatar_url":"https://avatars3.githubusercontent.com/u/2144079?v=4","gravatar_id":"","url":"https://api.github.com/users/wenweihu86","html_url":"https://github.com/wenweihu86","followers_url":"https://api.github.com/users/wenweihu86/followers","following_url":"https://api.github.com/users/wenweihu86/following{/other_user}","gists_url":"https://api.github.com/users/wenweihu86/gists{/gist_id}","starred_url":"https://api.github.com/users/wenweihu86/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wenweihu86/subscriptions","organizations_url":"https://api.github.com/users/wenweihu86/orgs","repos_url":"https://api.github.com/users/wenweihu86/repos","events_url":"https://api.github.com/users/wenweihu86/events{/privacy}","received_events_url":"https://api.github.com/users/wenweihu86/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2018-09-14T02:06:23Z","updated_at":"2018-10-11T16:44:03Z","closed_at":"2018-10-11T16:44:03Z","author_association":"NONE","body":"### Expected behavior\r\nshould not appear reference counting exception.\r\n\r\n### Actual behavior\r\n\r\n2018-09-14 00:54:48,492 WARN [client-io-thread-7]\tFailed to mark a promise as success because it has failed already: DefaultChannelPromise@1f8f7703(failure: io.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1), unnotified cause:\r\nio.netty.handler.codec.EncoderException: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1\r\n\tat io.netty.handler.codec.MessageToMessageEncoder.write(MessageToMessageEncoder.java:106) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.CombinedChannelDuplexHandler.write(CombinedChannelDuplexHandler.java:348) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeWrite0(AbstractChannelHandlerContext.java:738) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeWrite(AbstractChannelHandlerContext.java:730) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.access$1900(AbstractChannelHandlerContext.java:38) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext$AbstractWriteTask.write(AbstractChannelHandlerContext.java:1081) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext$WriteAndFlushTask.write(AbstractChannelHandlerContext.java:1128) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext$AbstractWriteTask.run(AbstractChannelHandlerContext.java:1070) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java:163) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:446) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:884) [netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat java.lang.Thread.run(Thread.java:748) [?:1.8.0_171]\r\nCaused by: io.netty.util.IllegalReferenceCountException: refCnt: 0, decrement: 1\r\n\tat io.netty.buffer.AbstractReferenceCountedByteBuf.release0(AbstractReferenceCountedByteBuf.java:100) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.buffer.AbstractReferenceCountedByteBuf.release(AbstractReferenceCountedByteBuf.java:84) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.handler.codec.http.DefaultFullHttpRequest.release(DefaultFullHttpRequest.java:102) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.util.ReferenceCountUtil.release(ReferenceCountUtil.java:88) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\tat io.netty.handler.codec.MessageToMessageEncoder.write(MessageToMessageEncoder.java:90) ~[netty-all-4.1.29.Final.jar:4.1.29.Final]\r\n\t... 12 more\r\n\r\n### Steps to reproduce\r\n1. init bootstrap:\r\nch.pipeline().addLast(new HttpClientCodec());\r\nch.pipeline().addLast(new HttpObjectAggregator(10 * 1024 * 1024));\r\nch.pipeline().addLast(new RpcClientHandler(RpcClient.this));\r\n2. channel.writeAndFlush(fullRequest);\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n// init netty bootstrap\r\n        bootstrap = new Bootstrap();\r\n        bootstrap.channel(NioSocketChannel.class);\r\n        bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, rpcClientOptions.getConnectTimeoutMillis());\r\n        bootstrap.option(ChannelOption.SO_KEEPALIVE, rpcClientOptions.isKeepAlive());\r\n        bootstrap.option(ChannelOption.SO_REUSEADDR, rpcClientOptions.isReuseAddr());\r\n        bootstrap.option(ChannelOption.TCP_NODELAY, rpcClientOptions.isTcpNoDelay());\r\n        bootstrap.option(ChannelOption.SO_RCVBUF, rpcClientOptions.getReceiveBufferSize());\r\n        bootstrap.option(ChannelOption.SO_SNDBUF, rpcClientOptions.getSendBufferSize());\r\n        ChannelInitializer<SocketChannel> initializer = new ChannelInitializer<SocketChannel>() {\r\n            @Override\r\n            protected void initChannel(SocketChannel ch) throws Exception {\r\n                if (rpcClientOptions.isHttp()) {\r\n                    ch.pipeline().addLast(new HttpClientCodec());\r\n                    ch.pipeline().addLast(new HttpObjectAggregator(10 * 1024 * 1024));\r\n                } else {\r\n                    ch.pipeline().addLast(new RpcRequestEncoder(RpcClient.this));\r\n                    ch.pipeline().addLast(new RpcResponseDecoder(RpcClient.this));\r\n                }\r\n                ch.pipeline().addLast(new RpcClientHandler(RpcClient.this));\r\n            }\r\n        };\r\n        bootstrap.group(new NioEventLoopGroup(\r\n                options.getIoThreadNum(),\r\n                new CustomThreadFactory(\"client-io-thread\")))\r\n                .handler(initializer);\r\n\r\n### Netty version\r\n4.1.29\r\n\r\n### JVM version (e.g. `java -version`)\r\njava version \"1.8.0_171\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_171-b11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nDarwin BDSHYF000127981 17.3.0 Darwin Kernel Version 17.3.0: Thu Nov  9 18:09:22 PST 2017; root:xnu-4570.31.3~1/RELEASE_X86_64 x86_64","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["421463732","421531479","422587650","429029562"], "labels":[]}