{"id":"603", "title":"Netty 3.x : A java.util.Locale encoded with ObjectEncoder in Java 1.6 cannot be decoded with ObjectDecoder in Java 1.7", "body":"A `java.util.Locale` encoded with `ObjectEncoder` in Java 1.6 cannot be decoded with `ObjectDecoder` in Java 1.7. However, if I use the `CompatibleObjectEncoder / CompatibleObjectDecoder` pair, it works. I have tried this with Netty 3.3.5.Final.jar and Netty 3.3.7.Final.jar.

Here's the test I used:

Server.java - run with Java 1.7

``` java
public class Server {
    public static void main(String[] args) {
        ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory());
        // If I use CompatibleObjectDecoder, there's no exception
        // bootstrap.getPipeline().addLast(\"decoder\", new CompatibleObjectDecoder());
        bootstrap.getPipeline().addLast(\"decoder\", new ObjectDecoder(ClassResolvers.softCachingResolver(Server.class.getClassLoader())));
        bootstrap.getPipeline().addLast(\"printer\", new SimpleChannelHandler() {
            @Override
            public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
                System.out.println(\"Received: \" + e);
            }
        });
        Channel channel = bootstrap.bind(new InetSocketAddress(3000));
        System.out.println(\"Server up at: \" + channel.getLocalAddress());
    }
}
```

Client.java - run with Java 1.6

``` java
public class Client {
    public static void main(String[] args) {
        final SocketAddress serverAddress = new InetSocketAddress(3000);
        ClientBootstrap client = new ClientBootstrap(new NioClientSocketChannelFactory());
        //client.getPipeline().addLast(\"encoder\", new CompatibleObjectEncoder());
        client.getPipeline().addLast(\"encoder\", new ObjectEncoder());

        client.connect(serverAddress).addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    future.getChannel().write(Locale.CANADA);
                } else {
                    System.out.println(\"Could not connect to \" + serverAddress);
                }
            }
        });
    }
}
```

When I run the Server in Java 1.7 and then Client in Java 1.6, I get the following on the server:

```
Server up at: /0.0.0.0:3000
Sep 14, 2012 3:28:41 PM org.jboss.netty.channel.SimpleChannelHandler
WARNING: EXCEPTION, please implement com.nettytests.Server$1.exceptionCaught() for proper handling.
java.io.EOFException
    at java.io.ObjectInputStream$BlockDataInputStream.peekByte(ObjectInputStream.java:2571)
    at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1315)
    at java.io.ObjectInputStream.access$300(ObjectInputStream.java:205)
    at java.io.ObjectInputStream$GetFieldImpl.readFields(ObjectInputStream.java:2126)
    at java.io.ObjectInputStream.readFields(ObjectInputStream.java:537)
    at java.util.Locale.readObject(Locale.java:2061)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:601)
    at java.io.ObjectStreamClass.invokeReadObject(ObjectStreamClass.java:991)
    at java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:1866)
    at java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:1771)
    at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1347)
    at java.io.ObjectInputStream.readObject(ObjectInputStream.java:369)
    at org.jboss.netty.handler.codec.serialization.ObjectDecoder.decode(ObjectDecoder.java:127)
    at org.jboss.netty.handler.codec.frame.FrameDecoder.callDecode(FrameDecoder.java:422)
    at org.jboss.netty.handler.codec.frame.FrameDecoder.messageReceived(FrameDecoder.java:303)
    at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:268)
    at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:255)
    at org.jboss.netty.channel.socket.nio.NioWorker.read(NioWorker.java:94)
    at org.jboss.netty.channel.socket.nio.AbstractNioWorker.processSelectedKeys(AbstractNioWorker.java:390)
    at org.jboss.netty.channel.socket.nio.AbstractNioWorker.run(AbstractNioWorker.java:261)
    at org.jboss.netty.channel.socket.nio.NioWorker.run(NioWorker.java:35)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1110)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:603)
    at java.lang.Thread.run(Thread.java:722)
```

AFAIK, Java 1.6 and Java 1.7 are binary-compatible (as also shown by the fact that `CompatibleObjectEncoder/Decoder` work fine), so I think it's something to do with `ObjectEncoder / ObjectDecoder`.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/603","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/603/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/603/comments","events_url":"https://api.github.com/repos/netty/netty/issues/603/events","html_url":"https://github.com/netty/netty/issues/603","id":6886902,"node_id":"MDU6SXNzdWU2ODg2OTAy","number":603,"title":"Netty 3.x : A java.util.Locale encoded with ObjectEncoder in Java 1.6 cannot be decoded with ObjectDecoder in Java 1.7","user":{"login":"amrittuladhar","id":207859,"node_id":"MDQ6VXNlcjIwNzg1OQ==","avatar_url":"https://avatars2.githubusercontent.com/u/207859?v=4","gravatar_id":"","url":"https://api.github.com/users/amrittuladhar","html_url":"https://github.com/amrittuladhar","followers_url":"https://api.github.com/users/amrittuladhar/followers","following_url":"https://api.github.com/users/amrittuladhar/following{/other_user}","gists_url":"https://api.github.com/users/amrittuladhar/gists{/gist_id}","starred_url":"https://api.github.com/users/amrittuladhar/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/amrittuladhar/subscriptions","organizations_url":"https://api.github.com/users/amrittuladhar/orgs","repos_url":"https://api.github.com/users/amrittuladhar/repos","events_url":"https://api.github.com/users/amrittuladhar/events{/privacy}","received_events_url":"https://api.github.com/users/amrittuladhar/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/31","html_url":"https://github.com/netty/netty/milestone/31","labels_url":"https://api.github.com/repos/netty/netty/milestones/31/labels","id":186607,"node_id":"MDk6TWlsZXN0b25lMTg2NjA3","number":31,"title":"3.5.9.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":15,"state":"closed","created_at":"2012-09-30T10:01:11Z","updated_at":"2013-09-05T14:40:08Z","due_on":"2012-10-25T07:00:00Z","closed_at":"2012-10-24T17:49:42Z"},"comments":4,"created_at":"2012-09-14T20:51:58Z","updated_at":"2013-03-22T18:44:48Z","closed_at":"2012-10-05T06:12:45Z","author_association":"NONE","body":"A `java.util.Locale` encoded with `ObjectEncoder` in Java 1.6 cannot be decoded with `ObjectDecoder` in Java 1.7. However, if I use the `CompatibleObjectEncoder / CompatibleObjectDecoder` pair, it works. I have tried this with Netty 3.3.5.Final.jar and Netty 3.3.7.Final.jar.\n\nHere's the test I used:\n\nServer.java - run with Java 1.7\n\n``` java\npublic class Server {\n    public static void main(String[] args) {\n        ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory());\n        // If I use CompatibleObjectDecoder, there's no exception\n        // bootstrap.getPipeline().addLast(\"decoder\", new CompatibleObjectDecoder());\n        bootstrap.getPipeline().addLast(\"decoder\", new ObjectDecoder(ClassResolvers.softCachingResolver(Server.class.getClassLoader())));\n        bootstrap.getPipeline().addLast(\"printer\", new SimpleChannelHandler() {\n            @Override\n            public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {\n                System.out.println(\"Received: \" + e);\n            }\n        });\n        Channel channel = bootstrap.bind(new InetSocketAddress(3000));\n        System.out.println(\"Server up at: \" + channel.getLocalAddress());\n    }\n}\n```\n\nClient.java - run with Java 1.6\n\n``` java\npublic class Client {\n    public static void main(String[] args) {\n        final SocketAddress serverAddress = new InetSocketAddress(3000);\n        ClientBootstrap client = new ClientBootstrap(new NioClientSocketChannelFactory());\n        //client.getPipeline().addLast(\"encoder\", new CompatibleObjectEncoder());\n        client.getPipeline().addLast(\"encoder\", new ObjectEncoder());\n\n        client.connect(serverAddress).addListener(new ChannelFutureListener() {\n\n            @Override\n            public void operationComplete(ChannelFuture future) throws Exception {\n                if (future.isSuccess()) {\n                    future.getChannel().write(Locale.CANADA);\n                } else {\n                    System.out.println(\"Could not connect to \" + serverAddress);\n                }\n            }\n        });\n    }\n}\n```\n\nWhen I run the Server in Java 1.7 and then Client in Java 1.6, I get the following on the server:\n\n```\nServer up at: /0.0.0.0:3000\nSep 14, 2012 3:28:41 PM org.jboss.netty.channel.SimpleChannelHandler\nWARNING: EXCEPTION, please implement com.nettytests.Server$1.exceptionCaught() for proper handling.\njava.io.EOFException\n    at java.io.ObjectInputStream$BlockDataInputStream.peekByte(ObjectInputStream.java:2571)\n    at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1315)\n    at java.io.ObjectInputStream.access$300(ObjectInputStream.java:205)\n    at java.io.ObjectInputStream$GetFieldImpl.readFields(ObjectInputStream.java:2126)\n    at java.io.ObjectInputStream.readFields(ObjectInputStream.java:537)\n    at java.util.Locale.readObject(Locale.java:2061)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:601)\n    at java.io.ObjectStreamClass.invokeReadObject(ObjectStreamClass.java:991)\n    at java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:1866)\n    at java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:1771)\n    at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1347)\n    at java.io.ObjectInputStream.readObject(ObjectInputStream.java:369)\n    at org.jboss.netty.handler.codec.serialization.ObjectDecoder.decode(ObjectDecoder.java:127)\n    at org.jboss.netty.handler.codec.frame.FrameDecoder.callDecode(FrameDecoder.java:422)\n    at org.jboss.netty.handler.codec.frame.FrameDecoder.messageReceived(FrameDecoder.java:303)\n    at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:268)\n    at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:255)\n    at org.jboss.netty.channel.socket.nio.NioWorker.read(NioWorker.java:94)\n    at org.jboss.netty.channel.socket.nio.AbstractNioWorker.processSelectedKeys(AbstractNioWorker.java:390)\n    at org.jboss.netty.channel.socket.nio.AbstractNioWorker.run(AbstractNioWorker.java:261)\n    at org.jboss.netty.channel.socket.nio.NioWorker.run(NioWorker.java:35)\n    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1110)\n    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:603)\n    at java.lang.Thread.run(Thread.java:722)\n```\n\nAFAIK, Java 1.6 and Java 1.7 are binary-compatible (as also shown by the fact that `CompatibleObjectEncoder/Decoder` work fine), so I think it's something to do with `ObjectEncoder / ObjectDecoder`.\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["8667337","9166459","13420319","15314581"], "labels":["defect"]}