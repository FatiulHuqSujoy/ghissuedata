{"id":"8094", "title":"NetworkInterface.getNetworkInterfaces() Blocked", "body":"### Expected behavior
Get network interfaces and return

### Actual behavior
The code block here, the thread can't continue, i use jstack, the thread is always here, but the thread state is runable:
\"init_context_comm_t0\" #14 daemon prio=5 os_prio=0 tid=0x00007f5fdc889800 nid=0x27 in Object.wait() [0x00007f5fd7ffc000]
   java.lang.Thread.State: RUNNABLE
  at io.netty.util.NetUtil.<clinit>(NetUtil.java:156)
  at java.lang.ClassLoader$NativeLibrary.load(Native Method)
  at java.lang.ClassLoader.loadLibrary0(ClassLoader.java:1941)
  - locked <0x00000000835e5868> (a java.util.Vector)
  - locked <0x00000000837abda8> (a java.util.Vector)
  at java.lang.ClassLoader.loadLibrary(ClassLoader.java:1824)
  at java.lang.Runtime.load0(Runtime.java:809)
  - locked <0x0000000083320da8> (a java.lang.Runtime)
  at java.lang.System.load(System.java:1086)
  at io.netty.util.internal.NativeLibraryLoader.load(NativeLibraryLoader.java:214)
  at io.netty.channel.epoll.Native.<clinit>(Native.java:60)
  at io.netty.channel.epoll.IovArray.<clinit>(IovArray.java:57)
  at io.netty.channel.epoll.EpollEventLoop.<init>(EpollEventLoop.java:60)
  at io.netty.channel.epoll.EpollEventLoopGroup.newChild(EpollEventLoopGroup.java:106)
  at io.netty.util.concurrent.MultithreadEventExecutorGroup.<init>(MultithreadEventExecutorGroup.java:64)
  at io.netty.channel.MultithreadEventLoopGroup.<init>(MultithreadEventLoopGroup.java:49)
  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:91)
  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:78)
  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:59)
  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:43)
  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:36)
  at com.sunyainfo.sunyacloud.communication.server.CommunicateEndpoint.<init>(CommunicateEndpoint.java:46)
  at com.sunyainfo.sunyacloud.communication.server.CommunicateEndpoint.init(CommunicateEndpoint.java:115)
  at com.sunyainfo.sunyacloud.mvc.ContextListener$1.run(ContextListener.java:39)
  at java.lang.Thread.run(Thread.java:748)

### Steps to reproduce
In the spring init code, call this:
group = SysUtil.isLinux()?new EpollEventLoopGroup():new NioEventLoopGroup();


### Minimal yet complete reproducer code (or URL to code)
group = SysUtil.isLinux()?new EpollEventLoopGroup():new NioEventLoopGroup();

### Netty version
4.0.36.Final

### JVM version (e.g. `java -version`)
openjdk version \"1.8.0_141\"
OpenJDK Runtime Environment (build 1.8.0_141-8u141-b15-1~deb9u1-b15)
OpenJDK 64-Bit Server VM (build 25.141-b15, mixed mode)

### OS version (e.g. `uname -a`)
Linux debian 4.9.0-6-amd64 #1 SMP Debian 4.9.82-1+deb9u3 (2018-03-02) x86_64 GNU/Linux

Tips: i run this code in docker", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8094","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8094/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8094/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8094/events","html_url":"https://github.com/netty/netty/issues/8094","id":337874762,"node_id":"MDU6SXNzdWUzMzc4NzQ3NjI=","number":8094,"title":"NetworkInterface.getNetworkInterfaces() Blocked","user":{"login":"wanlxp","id":14148233,"node_id":"MDQ6VXNlcjE0MTQ4MjMz","avatar_url":"https://avatars1.githubusercontent.com/u/14148233?v=4","gravatar_id":"","url":"https://api.github.com/users/wanlxp","html_url":"https://github.com/wanlxp","followers_url":"https://api.github.com/users/wanlxp/followers","following_url":"https://api.github.com/users/wanlxp/following{/other_user}","gists_url":"https://api.github.com/users/wanlxp/gists{/gist_id}","starred_url":"https://api.github.com/users/wanlxp/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wanlxp/subscriptions","organizations_url":"https://api.github.com/users/wanlxp/orgs","repos_url":"https://api.github.com/users/wanlxp/repos","events_url":"https://api.github.com/users/wanlxp/events{/privacy}","received_events_url":"https://api.github.com/users/wanlxp/received_events","type":"User","site_admin":false},"labels":[{"id":5175033,"node_id":"MDU6TGFiZWw1MTc1MDMz","url":"https://api.github.com/repos/netty/netty/labels/duplicate","name":"duplicate","color":"e6e6e6","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-07-03T12:19:46Z","updated_at":"2018-07-19T02:22:45Z","closed_at":"2018-07-07T18:37:23Z","author_association":"NONE","body":"### Expected behavior\r\nGet network interfaces and return\r\n\r\n### Actual behavior\r\nThe code block here, the thread can't continue, i use jstack, the thread is always here, but the thread state is runable:\r\n\"init_context_comm_t0\" #14 daemon prio=5 os_prio=0 tid=0x00007f5fdc889800 nid=0x27 in Object.wait() [0x00007f5fd7ffc000]\r\n   java.lang.Thread.State: RUNNABLE\r\n  at io.netty.util.NetUtil.<clinit>(NetUtil.java:156)\r\n  at java.lang.ClassLoader$NativeLibrary.load(Native Method)\r\n  at java.lang.ClassLoader.loadLibrary0(ClassLoader.java:1941)\r\n  - locked <0x00000000835e5868> (a java.util.Vector)\r\n  - locked <0x00000000837abda8> (a java.util.Vector)\r\n  at java.lang.ClassLoader.loadLibrary(ClassLoader.java:1824)\r\n  at java.lang.Runtime.load0(Runtime.java:809)\r\n  - locked <0x0000000083320da8> (a java.lang.Runtime)\r\n  at java.lang.System.load(System.java:1086)\r\n  at io.netty.util.internal.NativeLibraryLoader.load(NativeLibraryLoader.java:214)\r\n  at io.netty.channel.epoll.Native.<clinit>(Native.java:60)\r\n  at io.netty.channel.epoll.IovArray.<clinit>(IovArray.java:57)\r\n  at io.netty.channel.epoll.EpollEventLoop.<init>(EpollEventLoop.java:60)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.newChild(EpollEventLoopGroup.java:106)\r\n  at io.netty.util.concurrent.MultithreadEventExecutorGroup.<init>(MultithreadEventExecutorGroup.java:64)\r\n  at io.netty.channel.MultithreadEventLoopGroup.<init>(MultithreadEventLoopGroup.java:49)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:91)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:78)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:59)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:43)\r\n  at io.netty.channel.epoll.EpollEventLoopGroup.<init>(EpollEventLoopGroup.java:36)\r\n  at com.sunyainfo.sunyacloud.communication.server.CommunicateEndpoint.<init>(CommunicateEndpoint.java:46)\r\n  at com.sunyainfo.sunyacloud.communication.server.CommunicateEndpoint.init(CommunicateEndpoint.java:115)\r\n  at com.sunyainfo.sunyacloud.mvc.ContextListener$1.run(ContextListener.java:39)\r\n  at java.lang.Thread.run(Thread.java:748)\r\n\r\n### Steps to reproduce\r\nIn the spring init code, call this:\r\ngroup = SysUtil.isLinux()?new EpollEventLoopGroup():new NioEventLoopGroup();\r\n\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\ngroup = SysUtil.isLinux()?new EpollEventLoopGroup():new NioEventLoopGroup();\r\n\r\n### Netty version\r\n4.0.36.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\nopenjdk version \"1.8.0_141\"\r\nOpenJDK Runtime Environment (build 1.8.0_141-8u141-b15-1~deb9u1-b15)\r\nOpenJDK 64-Bit Server VM (build 25.141-b15, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nLinux debian 4.9.0-6-amd64 #1 SMP Debian 4.9.82-1+deb9u3 (2018-03-02) x86_64 GNU/Linux\r\n\r\nTips: i run this code in docker","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["403235215","403235225","406133684"], "labels":["duplicate"]}