{"id":"8199", "title":"Use netty 4.1+ TLS with Android 6", "body":"My project currently uses a modified version of `4.0.28.Final` which runs on Android 6.0.1. The diff is basically removing all classes which are not existent on Android 6.0.1 to be able to compile netty as a shared library for our Android build.
To use TLS with the parameters needed we construct the `SSLEngine` ourself and hand it over to the netty `SslHandler(SSLEngine engine)`.

We wanted to upgrade netty to `4.1.28.Final` which has better Android support and tons of fixes regarding TLS compared to version `4.0.28.Final`. Since we are using Android 6.0.1 with Conscrypt/BoringSSL the constructed `SSLEngine` is a `org.conscrypt.OpenSSLEngineImpl`.

### Expected behavior
When we init `SslHandler()` with `org.conscrypt.OpenSSLEngineImpl` we expect `SslHandler` to detect the given `SSLEngine` to be a `SslEngineType.CONSCRYPT`.

### Actual behavior
`SSLHandler` will fallback to `SslEngineType.JDK` due to:

    engineType = SslEngineType.forEngine(engine);

   which calls

    static SslEngineType forEngine(SSLEngine engine) {
        return engine instanceof ReferenceCountedOpenSslEngine ? TCNATIVE :
        engine instanceof ConscryptAlpnSslEngine ? CONSCRYPT : JDK;
    }

I also noticed `PlatformDependent0.javaVersion0()` will set to

    if (isAndroid0()) {
        majorVersion = 6;
    } 

but Android 6.0.1 will provide Java 1.7.

### Steps to reproduce
    SSLContext sslContext = SSLContext.getInstance(\"TLSv1.2\");
    sslContext.init(kmf.getKeyManagers(),tmf.getTrustManagers(), new SecureRandom());
    SSLEngine sslEngine = sslContext.createSSLEngine();
    sslEngine.setUseClientMode(true);
    SslHandler sslHandler = new SslHandler(sslEngine);
    channel.pipeline().addFirst(sslHandler)

### Minimal yet complete reproducer code (or URL to code)
N.A.

### Netty version
netty-4.1.28.Final

### JVM version (e.g. `java -version`)
SDK API-level 23
Java 1.7

### OS version (e.g. `uname -a`)
Linux localhost 4.4.111-1.1.1 #14 SMP PREEMPT Wed May 16 01:39:17 CEST 2018 armv7l", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8199","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8199/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8199/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8199/events","html_url":"https://github.com/netty/netty/issues/8199","id":351116311,"node_id":"MDU6SXNzdWUzNTExMTYzMTE=","number":8199,"title":"Use netty 4.1+ TLS with Android 6","user":{"login":"selop","id":5173254,"node_id":"MDQ6VXNlcjUxNzMyNTQ=","avatar_url":"https://avatars3.githubusercontent.com/u/5173254?v=4","gravatar_id":"","url":"https://api.github.com/users/selop","html_url":"https://github.com/selop","followers_url":"https://api.github.com/users/selop/followers","following_url":"https://api.github.com/users/selop/following{/other_user}","gists_url":"https://api.github.com/users/selop/gists{/gist_id}","starred_url":"https://api.github.com/users/selop/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/selop/subscriptions","organizations_url":"https://api.github.com/users/selop/orgs","repos_url":"https://api.github.com/users/selop/repos","events_url":"https://api.github.com/users/selop/events{/privacy}","received_events_url":"https://api.github.com/users/selop/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2018-08-16T09:01:24Z","updated_at":"2019-10-11T08:35:30Z","closed_at":"2019-10-11T08:35:29Z","author_association":"NONE","body":"My project currently uses a modified version of `4.0.28.Final` which runs on Android 6.0.1. The diff is basically removing all classes which are not existent on Android 6.0.1 to be able to compile netty as a shared library for our Android build.\r\nTo use TLS with the parameters needed we construct the `SSLEngine` ourself and hand it over to the netty `SslHandler(SSLEngine engine)`.\r\n\r\nWe wanted to upgrade netty to `4.1.28.Final` which has better Android support and tons of fixes regarding TLS compared to version `4.0.28.Final`. Since we are using Android 6.0.1 with Conscrypt/BoringSSL the constructed `SSLEngine` is a `org.conscrypt.OpenSSLEngineImpl`.\r\n\r\n### Expected behavior\r\nWhen we init `SslHandler()` with `org.conscrypt.OpenSSLEngineImpl` we expect `SslHandler` to detect the given `SSLEngine` to be a `SslEngineType.CONSCRYPT`.\r\n\r\n### Actual behavior\r\n`SSLHandler` will fallback to `SslEngineType.JDK` due to:\r\n\r\n    engineType = SslEngineType.forEngine(engine);\r\n\r\n   which calls\r\n\r\n    static SslEngineType forEngine(SSLEngine engine) {\r\n        return engine instanceof ReferenceCountedOpenSslEngine ? TCNATIVE :\r\n        engine instanceof ConscryptAlpnSslEngine ? CONSCRYPT : JDK;\r\n    }\r\n\r\nI also noticed `PlatformDependent0.javaVersion0()` will set to\r\n\r\n    if (isAndroid0()) {\r\n        majorVersion = 6;\r\n    } \r\n\r\nbut Android 6.0.1 will provide Java 1.7.\r\n\r\n### Steps to reproduce\r\n    SSLContext sslContext = SSLContext.getInstance(\"TLSv1.2\");\r\n    sslContext.init(kmf.getKeyManagers(),tmf.getTrustManagers(), new SecureRandom());\r\n    SSLEngine sslEngine = sslContext.createSSLEngine();\r\n    sslEngine.setUseClientMode(true);\r\n    SslHandler sslHandler = new SslHandler(sslEngine);\r\n    channel.pipeline().addFirst(sslHandler)\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\nN.A.\r\n\r\n### Netty version\r\nnetty-4.1.28.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\nSDK API-level 23\r\nJava 1.7\r\n\r\n### OS version (e.g. `uname -a`)\r\nLinux localhost 4.4.111-1.1.1 #14 SMP PREEMPT Wed May 16 01:39:17 CEST 2018 armv7l","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["413540599","413543302","413555698","413777381","413781850","413822196","415838726","416130034","417270230","429965412","437459885","437880264","540971754"], "labels":[]}