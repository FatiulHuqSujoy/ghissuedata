{"id":"8875", "title":"ChunkedWriteHandler does not close successful ChunkedInputs", "body":"### Expected behavior
`ChunkedWriteHandler` invokes `ChunkedInput#close()` after the input is fully consumed and `ChunkedInput#isEndOfInput()` returns `true.`

### Actual behavior
`ChunkedInput#close()` is not invoked when the input completes successfully. This can result in resource leaks when the input is based on an external resource, like a file handle.

### Steps to reproduce

Run the attached test.

### Minimal yet complete reproducer code (or URL to code)

```java
@Test
public void testCloseSuccessfulChunkedInput() throws Exception {
    final int totalChunks = 10;
    final AtomicBoolean closeInvoked = new AtomicBoolean();

    ChunkedInput<ByteBuf> input = new ChunkedInput<ByteBuf>() {
        int chunksProduced = 0;

        @Override
        public boolean isEndOfInput() throws Exception {
            return chunksProduced >= totalChunks;
        }

        @Override
        public void close() throws Exception {
            closeInvoked.set(true);
        }

        @Override
        public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {
            return readChunk(ctx.alloc());
        }

        @Override
        public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {
            ByteBuf buf = allocator.buffer(4);
            buf.writeInt(chunksProduced);
            chunksProduced++;
            return buf;
        }

        @Override
        public long length() {
            return totalChunks;
        }

        @Override
        public long progress() {
            return chunksProduced;
        }
    };

    EmbeddedChannel ch = new EmbeddedChannel(new ChunkedWriteHandler());

    ch.writeAndFlush(input).sync();

    for (int i = 0; i < totalChunks; i++) {
        ByteBuf buf = ch.readOutbound();
        assertEquals(i, buf.readInt());
    }

    assertTrue(closeInvoked.get()); // this assertion fails
}
```

### Netty version

Problem exists only in `4.1.33.Final`.

### JVM version (e.g. `java -version`)

```
java version \"1.8.0_201\"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
```

### OS version (e.g. `uname -a`)

```
Darwin MacBook-Pro.local 18.2.0 Darwin Kernel Version 18.2.0: Thu Dec 20 20:46:53 PST 2018; root:xnu-4903.241.1~1/RELEASE_X86_64 x86_64
```

This issue can be fixed by closing the input in a write future listener [here](https://github.com/netty/netty/blob/netty-4.1.33.Final/handler/src/main/java/io/netty/handler/stream/ChunkedWriteHandler.java#L286-L287) after the input is consumed. I have a branch with a tentative fix https://github.com/lutovich/netty/commit/1534f7778ad8c14d488fe17916c5baed5aaedc60 and can create a PR if it looks okay.", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8875","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8875/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8875/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8875/events","html_url":"https://github.com/netty/netty/issues/8875","id":411980021,"node_id":"MDU6SXNzdWU0MTE5ODAwMjE=","number":8875,"title":"ChunkedWriteHandler does not close successful ChunkedInputs","user":{"login":"lutovich","id":1638145,"node_id":"MDQ6VXNlcjE2MzgxNDU=","avatar_url":"https://avatars2.githubusercontent.com/u/1638145?v=4","gravatar_id":"","url":"https://api.github.com/users/lutovich","html_url":"https://github.com/lutovich","followers_url":"https://api.github.com/users/lutovich/followers","following_url":"https://api.github.com/users/lutovich/following{/other_user}","gists_url":"https://api.github.com/users/lutovich/gists{/gist_id}","starred_url":"https://api.github.com/users/lutovich/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lutovich/subscriptions","organizations_url":"https://api.github.com/users/lutovich/orgs","repos_url":"https://api.github.com/users/lutovich/repos","events_url":"https://api.github.com/users/lutovich/events{/privacy}","received_events_url":"https://api.github.com/users/lutovich/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/207","html_url":"https://github.com/netty/netty/milestone/207","labels_url":"https://api.github.com/repos/netty/netty/milestones/207/labels","id":3989720,"node_id":"MDk6TWlsZXN0b25lMzk4OTcyMA==","number":207,"title":"4.1.34.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":58,"state":"closed","created_at":"2019-01-21T14:07:13Z","updated_at":"2019-03-08T13:49:20Z","due_on":null,"closed_at":"2019-03-08T13:49:20Z"},"comments":2,"created_at":"2019-02-19T15:25:39Z","updated_at":"2019-02-28T20:14:27Z","closed_at":"2019-02-28T20:13:56Z","author_association":"CONTRIBUTOR","body":"### Expected behavior\r\n`ChunkedWriteHandler` invokes `ChunkedInput#close()` after the input is fully consumed and `ChunkedInput#isEndOfInput()` returns `true.`\r\n\r\n### Actual behavior\r\n`ChunkedInput#close()` is not invoked when the input completes successfully. This can result in resource leaks when the input is based on an external resource, like a file handle.\r\n\r\n### Steps to reproduce\r\n\r\nRun the attached test.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n```java\r\n@Test\r\npublic void testCloseSuccessfulChunkedInput() throws Exception {\r\n    final int totalChunks = 10;\r\n    final AtomicBoolean closeInvoked = new AtomicBoolean();\r\n\r\n    ChunkedInput<ByteBuf> input = new ChunkedInput<ByteBuf>() {\r\n        int chunksProduced = 0;\r\n\r\n        @Override\r\n        public boolean isEndOfInput() throws Exception {\r\n            return chunksProduced >= totalChunks;\r\n        }\r\n\r\n        @Override\r\n        public void close() throws Exception {\r\n            closeInvoked.set(true);\r\n        }\r\n\r\n        @Override\r\n        public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {\r\n            return readChunk(ctx.alloc());\r\n        }\r\n\r\n        @Override\r\n        public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {\r\n            ByteBuf buf = allocator.buffer(4);\r\n            buf.writeInt(chunksProduced);\r\n            chunksProduced++;\r\n            return buf;\r\n        }\r\n\r\n        @Override\r\n        public long length() {\r\n            return totalChunks;\r\n        }\r\n\r\n        @Override\r\n        public long progress() {\r\n            return chunksProduced;\r\n        }\r\n    };\r\n\r\n    EmbeddedChannel ch = new EmbeddedChannel(new ChunkedWriteHandler());\r\n\r\n    ch.writeAndFlush(input).sync();\r\n\r\n    for (int i = 0; i < totalChunks; i++) {\r\n        ByteBuf buf = ch.readOutbound();\r\n        assertEquals(i, buf.readInt());\r\n    }\r\n\r\n    assertTrue(closeInvoked.get()); // this assertion fails\r\n}\r\n```\r\n\r\n### Netty version\r\n\r\nProblem exists only in `4.1.33.Final`.\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\n```\r\njava version \"1.8.0_201\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_201-b09)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)\r\n```\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\n```\r\nDarwin MacBook-Pro.local 18.2.0 Darwin Kernel Version 18.2.0: Thu Dec 20 20:46:53 PST 2018; root:xnu-4903.241.1~1/RELEASE_X86_64 x86_64\r\n```\r\n\r\nThis issue can be fixed by closing the input in a write future listener [here](https://github.com/netty/netty/blob/netty-4.1.33.Final/handler/src/main/java/io/netty/handler/stream/ChunkedWriteHandler.java#L286-L287) after the input is consumed. I have a branch with a tentative fix https://github.com/lutovich/netty/commit/1534f7778ad8c14d488fe17916c5baed5aaedc60 and can create a PR if it looks okay.","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["465178385","465187645"], "labels":[]}