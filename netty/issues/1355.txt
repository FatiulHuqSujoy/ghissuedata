{"id":"1355", "title":"duplicate invocation of same plugins due to needless fork of maven-source-plugin", "body":"this issue makes build and release run longer

1) problem description
http://blog.peterlynch.ca/2010/05/maven-how-to-prevent-generate-sources.html

2) example work around
https://github.com/barchart/barchart-archon/blob/master/pom.xml#L435

3) duplicate invocation is marked by `>>>` and  `<<<` markers in the netty build log

for example, you can see `Starting audit... Audit done.` twice

```
[INFO] ------------------------------------------------------------------------
[INFO] Building Netty/Handler 4.0.0.CR2-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ netty-handler ---
[INFO] Deleting /work/git/netty/handler/target
[INFO] 
[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-maven) @ netty-handler ---
[INFO] 
[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-tools) @ netty-handler ---
[INFO] 
[INFO] --- maven-checkstyle-plugin:2.10:check (check-style) @ netty-handler ---
[INFO] Starting audit...
Audit done.

[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ netty-handler ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /work/git/netty/handler/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.0:compile (default-compile) @ netty-handler ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 32 source files to /work/git/netty/handler/target/classes
[INFO] 
[INFO] --- animal-sniffer-maven-plugin:1.9:check (default) @ netty-handler ---
[INFO] Checking unresolved references to org.codehaus.mojo.signature:java16:1.0
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ netty-handler ---
[INFO] Not copying test resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.0:testCompile (default-testCompile) @ netty-handler ---
[INFO] Not compiling test sources
[INFO] 
[INFO] --- maven-surefire-plugin:2.14:test (default-test) @ netty-handler ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-bundle-plugin:2.3.7:bundle (default-bundle) @ netty-handler ---
[INFO] 
[INFO] >>> maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler >>>
[INFO] 
[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-maven) @ netty-handler ---
[INFO] 
[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-tools) @ netty-handler ---
[INFO] 
[INFO] --- maven-checkstyle-plugin:2.10:check (check-style) @ netty-handler ---
[INFO] Starting audit...
Audit done.

[INFO] 
[INFO] <<< maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler <<<
[INFO] 
[INFO] --- maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler ---
[INFO] Building jar: /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ netty-handler ---
[INFO] Installing /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT.jar to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.jar
[INFO] Installing /work/git/netty/handler/pom.xml to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.pom
[INFO] Installing /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar
[INFO] 
[INFO] --- maven-bundle-plugin:2.3.7:install (default-install) @ netty-handler ---
[INFO] Installing io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.jar
[INFO] Writing OBR metadata
[INFO] 
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1355","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1355/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1355/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1355/events","html_url":"https://github.com/netty/netty/issues/1355","id":14223121,"node_id":"MDU6SXNzdWUxNDIyMzEyMQ==","number":1355,"title":"duplicate invocation of same plugins due to needless fork of maven-source-plugin","user":{"login":"Andrei-Pozolotin","id":1622151,"node_id":"MDQ6VXNlcjE2MjIxNTE=","avatar_url":"https://avatars0.githubusercontent.com/u/1622151?v=4","gravatar_id":"","url":"https://api.github.com/users/Andrei-Pozolotin","html_url":"https://github.com/Andrei-Pozolotin","followers_url":"https://api.github.com/users/Andrei-Pozolotin/followers","following_url":"https://api.github.com/users/Andrei-Pozolotin/following{/other_user}","gists_url":"https://api.github.com/users/Andrei-Pozolotin/gists{/gist_id}","starred_url":"https://api.github.com/users/Andrei-Pozolotin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Andrei-Pozolotin/subscriptions","organizations_url":"https://api.github.com/users/Andrei-Pozolotin/orgs","repos_url":"https://api.github.com/users/Andrei-Pozolotin/repos","events_url":"https://api.github.com/users/Andrei-Pozolotin/events{/privacy}","received_events_url":"https://api.github.com/users/Andrei-Pozolotin/received_events","type":"User","site_admin":false},"labels":[{"id":185731,"node_id":"MDU6TGFiZWwxODU3MzE=","url":"https://api.github.com/repos/netty/netty/labels/cleanup","name":"cleanup","color":"5319e7","default":false}],"state":"closed","locked":false,"assignee":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false},"assignees":[{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/51","html_url":"https://github.com/netty/netty/milestone/51","labels_url":"https://api.github.com/repos/netty/netty/milestones/51/labels","id":329712,"node_id":"MDk6TWlsZXN0b25lMzI5NzEy","number":51,"title":"4.0.0.CR3","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":19,"state":"closed","created_at":"2013-05-08T13:27:10Z","updated_at":"2013-09-05T14:19:21Z","due_on":"2013-05-18T07:00:00Z","closed_at":"2013-05-18T07:38:47Z"},"comments":1,"created_at":"2013-05-11T15:46:50Z","updated_at":"2013-05-13T14:39:57Z","closed_at":"2013-05-13T07:03:26Z","author_association":"MEMBER","body":"this issue makes build and release run longer\n\n1) problem description\nhttp://blog.peterlynch.ca/2010/05/maven-how-to-prevent-generate-sources.html\n\n2) example work around\nhttps://github.com/barchart/barchart-archon/blob/master/pom.xml#L435\n\n3) duplicate invocation is marked by `>>>` and  `<<<` markers in the netty build log\n\nfor example, you can see `Starting audit... Audit done.` twice\n\n```\n[INFO] ------------------------------------------------------------------------\n[INFO] Building Netty/Handler 4.0.0.CR2-SNAPSHOT\n[INFO] ------------------------------------------------------------------------\n[INFO] \n[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ netty-handler ---\n[INFO] Deleting /work/git/netty/handler/target\n[INFO] \n[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-maven) @ netty-handler ---\n[INFO] \n[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-tools) @ netty-handler ---\n[INFO] \n[INFO] --- maven-checkstyle-plugin:2.10:check (check-style) @ netty-handler ---\n[INFO] Starting audit...\nAudit done.\n\n[INFO] \n[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ netty-handler ---\n[INFO] Using 'UTF-8' encoding to copy filtered resources.\n[INFO] skip non existing resourceDirectory /work/git/netty/handler/src/main/resources\n[INFO] \n[INFO] --- maven-compiler-plugin:3.0:compile (default-compile) @ netty-handler ---\n[INFO] Changes detected - recompiling the module!\n[INFO] Compiling 32 source files to /work/git/netty/handler/target/classes\n[INFO] \n[INFO] --- animal-sniffer-maven-plugin:1.9:check (default) @ netty-handler ---\n[INFO] Checking unresolved references to org.codehaus.mojo.signature:java16:1.0\n[INFO] \n[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ netty-handler ---\n[INFO] Not copying test resources\n[INFO] \n[INFO] --- maven-compiler-plugin:3.0:testCompile (default-testCompile) @ netty-handler ---\n[INFO] Not compiling test sources\n[INFO] \n[INFO] --- maven-surefire-plugin:2.14:test (default-test) @ netty-handler ---\n[INFO] Tests are skipped.\n[INFO] \n[INFO] --- maven-bundle-plugin:2.3.7:bundle (default-bundle) @ netty-handler ---\n[INFO] \n[INFO] >>> maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler >>>\n[INFO] \n[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-maven) @ netty-handler ---\n[INFO] \n[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-tools) @ netty-handler ---\n[INFO] \n[INFO] --- maven-checkstyle-plugin:2.10:check (check-style) @ netty-handler ---\n[INFO] Starting audit...\nAudit done.\n\n[INFO] \n[INFO] <<< maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler <<<\n[INFO] \n[INFO] --- maven-source-plugin:2.2.1:jar (attach-sources) @ netty-handler ---\n[INFO] Building jar: /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar\n[INFO] \n[INFO] --- maven-install-plugin:2.4:install (default-install) @ netty-handler ---\n[INFO] Installing /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT.jar to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.jar\n[INFO] Installing /work/git/netty/handler/pom.xml to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.pom\n[INFO] Installing /work/git/netty/handler/target/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar to /home/user1/.m2/repository/io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT-sources.jar\n[INFO] \n[INFO] --- maven-bundle-plugin:2.3.7:install (default-install) @ netty-handler ---\n[INFO] Installing io/netty/netty-handler/4.0.0.CR2-SNAPSHOT/netty-handler-4.0.0.CR2-SNAPSHOT.jar\n[INFO] Writing OBR metadata\n[INFO] \n```\n","closed_by":{"login":"trustin","id":173918,"node_id":"MDQ6VXNlcjE3MzkxOA==","avatar_url":"https://avatars0.githubusercontent.com/u/173918?v=4","gravatar_id":"","url":"https://api.github.com/users/trustin","html_url":"https://github.com/trustin","followers_url":"https://api.github.com/users/trustin/followers","following_url":"https://api.github.com/users/trustin/following{/other_user}","gists_url":"https://api.github.com/users/trustin/gists{/gist_id}","starred_url":"https://api.github.com/users/trustin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/trustin/subscriptions","organizations_url":"https://api.github.com/users/trustin/orgs","repos_url":"https://api.github.com/users/trustin/repos","events_url":"https://api.github.com/users/trustin/events{/privacy}","received_events_url":"https://api.github.com/users/trustin/received_events","type":"User","site_admin":false}}", "commentIds":["17815816"], "labels":["cleanup"]}