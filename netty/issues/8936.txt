{"id":"8936", "title":"Run cloned code and get error \"java.lang.NoClassDefFoundError: io/netty/channel/EventLoopGroup\"", "body":"1 git clone the repo; add Native transport dependencies to pom.xml.
2 \"mvn clean package\" under \"netty/\" and \"netty/example/\" path
3 cd example/
4 run \"java -cp target/classes io.netty.example.echo.EchoServer\"
5 get error
> Error: A JNI error has occurred, please check your installation and try again
> Exception in thread \"main\" java.lang.NoClassDefFoundError: io/netty/channel/EventLoopGroup
> 	at java.lang.Class.getDeclaredMethods0(Native Method)
> 	at java.lang.Class.privateGetDeclaredMethods(Class.java:2701)
> 	at java.lang.Class.privateGetMethodRecursive(Class.java:3048)
> 	at java.lang.Class.getMethod0(Class.java:3018)
> 	at java.lang.Class.getMethod(Class.java:1784)
> 	at sun.launcher.LauncherHelper.validateMainClass(LauncherHelper.java:544)
> 	at sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:526)
> Caused by: java.lang.ClassNotFoundException: io.netty.channel.EventLoopGroup
> 	at java.net.URLClassLoader.findClass(URLClassLoader.java:382)
> 	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
> 	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:349)
> 	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
> 	... 7 more

### Netty version
4.1
### JVM version (e.g. `java -version`)

java version \"1.8.0_201\"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)

### OS version (e.g. `uname -a`)

Linux storage05 4.4.0-141-generic #167-Ubuntu SMP Wed Dec 5 10:40:15 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

I have already built and add native transports dependencies to pom.xml according to Netty docs, but when running the code it seems not working.


", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8936","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8936/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8936/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8936/events","html_url":"https://github.com/netty/netty/issues/8936","id":420572458,"node_id":"MDU6SXNzdWU0MjA1NzI0NTg=","number":8936,"title":"Run cloned code and get error \"java.lang.NoClassDefFoundError: io/netty/channel/EventLoopGroup\"","user":{"login":"Chenfengldw","id":12796718,"node_id":"MDQ6VXNlcjEyNzk2NzE4","avatar_url":"https://avatars1.githubusercontent.com/u/12796718?v=4","gravatar_id":"","url":"https://api.github.com/users/Chenfengldw","html_url":"https://github.com/Chenfengldw","followers_url":"https://api.github.com/users/Chenfengldw/followers","following_url":"https://api.github.com/users/Chenfengldw/following{/other_user}","gists_url":"https://api.github.com/users/Chenfengldw/gists{/gist_id}","starred_url":"https://api.github.com/users/Chenfengldw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Chenfengldw/subscriptions","organizations_url":"https://api.github.com/users/Chenfengldw/orgs","repos_url":"https://api.github.com/users/Chenfengldw/repos","events_url":"https://api.github.com/users/Chenfengldw/events{/privacy}","received_events_url":"https://api.github.com/users/Chenfengldw/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2019-03-13T15:42:09Z","updated_at":"2019-03-17T11:16:34Z","closed_at":"2019-03-17T11:16:34Z","author_association":"NONE","body":"1 git clone the repo; add Native transport dependencies to pom.xml.\r\n2 \"mvn clean package\" under \"netty/\" and \"netty/example/\" path\r\n3 cd example/\r\n4 run \"java -cp target/classes io.netty.example.echo.EchoServer\"\r\n5 get error\r\n> Error: A JNI error has occurred, please check your installation and try again\r\n> Exception in thread \"main\" java.lang.NoClassDefFoundError: io/netty/channel/EventLoopGroup\r\n> \tat java.lang.Class.getDeclaredMethods0(Native Method)\r\n> \tat java.lang.Class.privateGetDeclaredMethods(Class.java:2701)\r\n> \tat java.lang.Class.privateGetMethodRecursive(Class.java:3048)\r\n> \tat java.lang.Class.getMethod0(Class.java:3018)\r\n> \tat java.lang.Class.getMethod(Class.java:1784)\r\n> \tat sun.launcher.LauncherHelper.validateMainClass(LauncherHelper.java:544)\r\n> \tat sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:526)\r\n> Caused by: java.lang.ClassNotFoundException: io.netty.channel.EventLoopGroup\r\n> \tat java.net.URLClassLoader.findClass(URLClassLoader.java:382)\r\n> \tat java.lang.ClassLoader.loadClass(ClassLoader.java:424)\r\n> \tat sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:349)\r\n> \tat java.lang.ClassLoader.loadClass(ClassLoader.java:357)\r\n> \t... 7 more\r\n\r\n### Netty version\r\n4.1\r\n### JVM version (e.g. `java -version`)\r\n\r\njava version \"1.8.0_201\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_201-b09)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\nLinux storage05 4.4.0-141-generic #167-Ubuntu SMP Wed Dec 5 10:40:15 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux\r\n\r\nI have already built and add native transports dependencies to pom.xml according to Netty docs, but when running the code it seems not working.\r\n\r\n\r\n","closed_by":{"login":"Chenfengldw","id":12796718,"node_id":"MDQ6VXNlcjEyNzk2NzE4","avatar_url":"https://avatars1.githubusercontent.com/u/12796718?v=4","gravatar_id":"","url":"https://api.github.com/users/Chenfengldw","html_url":"https://github.com/Chenfengldw","followers_url":"https://api.github.com/users/Chenfengldw/followers","following_url":"https://api.github.com/users/Chenfengldw/following{/other_user}","gists_url":"https://api.github.com/users/Chenfengldw/gists{/gist_id}","starred_url":"https://api.github.com/users/Chenfengldw/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Chenfengldw/subscriptions","organizations_url":"https://api.github.com/users/Chenfengldw/orgs","repos_url":"https://api.github.com/users/Chenfengldw/repos","events_url":"https://api.github.com/users/Chenfengldw/events{/privacy}","received_events_url":"https://api.github.com/users/Chenfengldw/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":[]}