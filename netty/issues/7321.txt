{"id":"7321", "title":"NioSocketChannel leaks file descriptors even when closing", "body":"### Expected behavior
I expect that if an exception is thrown when calling `NioSocketChannel#close`, any open sockets should be closed. 

### Actual behavior
If I run the program below, it indicates that file descriptors are leaking. Similar problems are seen in larger applications.

### Steps to reproduce
Run the code below, observe the output.

### Minimal yet complete reproducer code (or URL to code)

#### Example java code

```java
public class NioSocketChannelLeaks
{
    public static void main(String[] args) throws NumberFormatException, IOException
    {
        // Get process ID (first comment in https://stackoverflow.com/a/6372205)
        int pid = Integer.parseInt(new File(\"/proc/self\").getCanonicalFile().getName());

        ProcessBuilder builder = new ProcessBuilder(\"lsof\", \"-p\", Integer.toString(pid));
        builder.inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);

        for (int i = 0; i < 1024; ++i)
        {
            try
            {
                NioSocketChannel channel = new NioSocketChannel();
                channel.close();
            }
            catch (IllegalStateException x)
            {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(builder.start().getInputStream())))
                {
                    System.out.printf(\"Got exception: '%s', current open file descriptors: %d%n\", x.getMessage(), reader.lines().count());
                }
            }
        }
    }
}
```
#### Output from above program when run in Eclipse 4.6
```
Got exception: 'channel not registered to an event loop', current open file descriptors: 217
Got exception: 'channel not registered to an event loop', current open file descriptors: 218
Got exception: 'channel not registered to an event loop', current open file descriptors: 219
Got exception: 'channel not registered to an event loop', current open file descriptors: 220
Got exception: 'channel not registered to an event loop', current open file descriptors: 221
Got exception: 'channel not registered to an event loop', current open file descriptors: 222
Got exception: 'channel not registered to an event loop', current open file descriptors: 223
Got exception: 'channel not registered to an event loop', current open file descriptors: 224
Got exception: 'channel not registered to an event loop', current open file descriptors: 225
[... output continues and descriptors increase ...]
```

### Netty version
Netty 4.0.47

### JVM version (e.g. `java -version`)
java version \"1.8.0_131\"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)

### OS version (e.g. `uname -a`)
Linux 4.4.0-97-generic #120-Ubuntu SMP Tue Sep 19 17:28:18 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/7321","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/7321/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/7321/comments","events_url":"https://api.github.com/repos/netty/netty/issues/7321/events","html_url":"https://github.com/netty/netty/issues/7321","id":267166929,"node_id":"MDU6SXNzdWUyNjcxNjY5Mjk=","number":7321,"title":"NioSocketChannel leaks file descriptors even when closing","user":{"login":"tobier","id":162911,"node_id":"MDQ6VXNlcjE2MjkxMQ==","avatar_url":"https://avatars0.githubusercontent.com/u/162911?v=4","gravatar_id":"","url":"https://api.github.com/users/tobier","html_url":"https://github.com/tobier","followers_url":"https://api.github.com/users/tobier/followers","following_url":"https://api.github.com/users/tobier/following{/other_user}","gists_url":"https://api.github.com/users/tobier/gists{/gist_id}","starred_url":"https://api.github.com/users/tobier/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tobier/subscriptions","organizations_url":"https://api.github.com/users/tobier/orgs","repos_url":"https://api.github.com/users/tobier/repos","events_url":"https://api.github.com/users/tobier/events{/privacy}","received_events_url":"https://api.github.com/users/tobier/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-10-20T12:56:30Z","updated_at":"2017-10-20T20:34:06Z","closed_at":"2017-10-20T17:12:06Z","author_association":"NONE","body":"### Expected behavior\r\nI expect that if an exception is thrown when calling `NioSocketChannel#close`, any open sockets should be closed. \r\n\r\n### Actual behavior\r\nIf I run the program below, it indicates that file descriptors are leaking. Similar problems are seen in larger applications.\r\n\r\n### Steps to reproduce\r\nRun the code below, observe the output.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n#### Example java code\r\n\r\n```java\r\npublic class NioSocketChannelLeaks\r\n{\r\n    public static void main(String[] args) throws NumberFormatException, IOException\r\n    {\r\n        // Get process ID (first comment in https://stackoverflow.com/a/6372205)\r\n        int pid = Integer.parseInt(new File(\"/proc/self\").getCanonicalFile().getName());\r\n\r\n        ProcessBuilder builder = new ProcessBuilder(\"lsof\", \"-p\", Integer.toString(pid));\r\n        builder.inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);\r\n\r\n        for (int i = 0; i < 1024; ++i)\r\n        {\r\n            try\r\n            {\r\n                NioSocketChannel channel = new NioSocketChannel();\r\n                channel.close();\r\n            }\r\n            catch (IllegalStateException x)\r\n            {\r\n                try (BufferedReader reader = new BufferedReader(new InputStreamReader(builder.start().getInputStream())))\r\n                {\r\n                    System.out.printf(\"Got exception: '%s', current open file descriptors: %d%n\", x.getMessage(), reader.lines().count());\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n```\r\n#### Output from above program when run in Eclipse 4.6\r\n```\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 217\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 218\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 219\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 220\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 221\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 222\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 223\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 224\r\nGot exception: 'channel not registered to an event loop', current open file descriptors: 225\r\n[... output continues and descriptors increase ...]\r\n```\r\n\r\n### Netty version\r\nNetty 4.0.47\r\n\r\n### JVM version (e.g. `java -version`)\r\njava version \"1.8.0_131\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_131-b11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)\r\n\r\n### OS version (e.g. `uname -a`)\r\nLinux 4.4.0-97-generic #120-Ubuntu SMP Tue Sep 19 17:28:18 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["338266576","338267140","338315556"], "labels":[]}