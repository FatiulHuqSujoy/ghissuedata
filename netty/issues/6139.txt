{"id":"6139", "title":"SSLHandler slows down large file transfer", "body":"I try to send a file larger then 5MB over the following pipeline:

1. SSLHandler
2. ByteToMessageCodec
3. IdleStateHandler
4. SimpleChannelInboundHandler

Sending a 10MB file takes arround 3Sekonds. When I remove the SSLHandler the file is transferred within 800ms. I found out that the SSLHandler Chunks the bytebuffer into very tiny pieces, that take forever to be sent.

I use the `UnpooledByteBufAllocator.DEFAULT` in my ChannelOptions. my encode method form the `ByteToMessageCodec` looks like this:

```java
protected void encode(ChannelHandlerContext channelHandlerContext, IMessage iMessage, ByteBuf byteBuf) throws Exception {
    if(byteBuf.isWritable()){
        byteBuf.writeBytes((serializer.serialize(iMessage) + RECORD_SEPARATOR).getBytes());
    }
}
```
Is there a way to tweek the SSLHandler?

Maybe the SSLEngine is the troublemaker? I found the following line of code inside the SSLHandler, but this constructor is depricated:

```java
wantsLargeOutboundNetworkBuffer = !opensslEngine;
```

### Netty version
4.0.40

### JVM version
java version \"1.8.0_111\"
Java(TM) SE Runtime Environment (build 1.8.0_111-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.111-b14, mixed mode)

### OS version
Win10

### StackOverflow
[netty-sslhandler-slows-down-file-transfer-5mb](http://stackoverflow.com/questions/41182586/netty-sslhandler-slows-down-file-transfer-5mb)", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6139","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6139/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6139/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6139/events","html_url":"https://github.com/netty/netty/issues/6139","id":196042344,"node_id":"MDU6SXNzdWUxOTYwNDIzNDQ=","number":6139,"title":"SSLHandler slows down large file transfer","user":{"login":"konsultaner","id":2699538,"node_id":"MDQ6VXNlcjI2OTk1Mzg=","avatar_url":"https://avatars1.githubusercontent.com/u/2699538?v=4","gravatar_id":"","url":"https://api.github.com/users/konsultaner","html_url":"https://github.com/konsultaner","followers_url":"https://api.github.com/users/konsultaner/followers","following_url":"https://api.github.com/users/konsultaner/following{/other_user}","gists_url":"https://api.github.com/users/konsultaner/gists{/gist_id}","starred_url":"https://api.github.com/users/konsultaner/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/konsultaner/subscriptions","organizations_url":"https://api.github.com/users/konsultaner/orgs","repos_url":"https://api.github.com/users/konsultaner/repos","events_url":"https://api.github.com/users/konsultaner/events{/privacy}","received_events_url":"https://api.github.com/users/konsultaner/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/176","html_url":"https://github.com/netty/netty/milestone/176","labels_url":"https://api.github.com/repos/netty/netty/milestones/176/labels","id":2886035,"node_id":"MDk6TWlsZXN0b25lMjg4NjAzNQ==","number":176,"title":"4.1.18.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":31,"state":"closed","created_at":"2017-11-03T14:17:43Z","updated_at":"2017-12-13T13:26:47Z","due_on":null,"closed_at":"2017-12-11T07:36:39Z"},"comments":24,"created_at":"2016-12-16T11:49:28Z","updated_at":"2017-12-13T13:26:47Z","closed_at":"2017-12-13T13:26:39Z","author_association":"NONE","body":"I try to send a file larger then 5MB over the following pipeline:\r\n\r\n1. SSLHandler\r\n2. ByteToMessageCodec\r\n3. IdleStateHandler\r\n4. SimpleChannelInboundHandler\r\n\r\nSending a 10MB file takes arround 3Sekonds. When I remove the SSLHandler the file is transferred within 800ms. I found out that the SSLHandler Chunks the bytebuffer into very tiny pieces, that take forever to be sent.\r\n\r\nI use the `UnpooledByteBufAllocator.DEFAULT` in my ChannelOptions. my encode method form the `ByteToMessageCodec` looks like this:\r\n\r\n```java\r\nprotected void encode(ChannelHandlerContext channelHandlerContext, IMessage iMessage, ByteBuf byteBuf) throws Exception {\r\n    if(byteBuf.isWritable()){\r\n        byteBuf.writeBytes((serializer.serialize(iMessage) + RECORD_SEPARATOR).getBytes());\r\n    }\r\n}\r\n```\r\nIs there a way to tweek the SSLHandler?\r\n\r\nMaybe the SSLEngine is the troublemaker? I found the following line of code inside the SSLHandler, but this constructor is depricated:\r\n\r\n```java\r\nwantsLargeOutboundNetworkBuffer = !opensslEngine;\r\n```\r\n\r\n### Netty version\r\n4.0.40\r\n\r\n### JVM version\r\njava version \"1.8.0_111\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_111-b14)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.111-b14, mixed mode)\r\n\r\n### OS version\r\nWin10\r\n\r\n### StackOverflow\r\n[netty-sslhandler-slows-down-file-transfer-5mb](http://stackoverflow.com/questions/41182586/netty-sslhandler-slows-down-file-transfer-5mb)","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["267708909","267712353","267714512","267821891","267822387","267835100","267842100","268619235","268738470","269362158","269476051","269476951","269515567","269532963","269610338","269614686","269802071","276891775","277229218","277542936","277946021","292226339","297600633","351390477"], "labels":[]}