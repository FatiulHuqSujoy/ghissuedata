{"id":"5009", "title":"Netty crashes VM on Raspberry PI 3", "body":"Hello. In my project I'm distributing my server as jar file for my users. One of them reported crash for Rapsberry PI 3. (But is works fine with rasp pi 2). Netty 4.0.33

```
Stack: [0x66110000,0x66160000], sp=0x6615e338, free space=312k
Native frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)
V [libjvm.so+0x4e1494] Unsafe_GetNativeByte+0x70

Java frames: (J=compiled Java code, j=interpreted, Vv=VM code)
j sun.misc.Unsafe.getByte(J)B+0
j io.netty.util.internal.PlatformDependent0.getByte(J)B+4
j io.netty.util.internal.PlatformDependent.getByte(J)B+1
j io.netty.buffer.UnsafeByteBufUtil.getShort([BI)S+29
j io.netty.buffer.UnpooledUnsafeHeapByteBuf._getShort(I)S+5
j io.netty.buffer.UnpooledUnsafeHeapByteBuf.getShort(I)S+8
j io.netty.buffer.AbstractByteBuf.getUnsignedShort(I)I+2
j io.netty.handler.ssl.SslHandler.getEncryptedPacketLength(Lio/netty/buffer/ByteBuf;I)I+66
j io.netty.handler.ssl.SslHandler.decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+89
j io.netty.handler.codec.ByteToMessageDecoder.callDecode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+46
j io.netty.handler.codec.ByteToMessageDecoder.channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V+81
j io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(Ljava/lang/Object;)V+9
j io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelHandlerContext;+35
j io.netty.handler.codec.ByteToMessageDecoder.handlerRemoved(Lio/netty/channel/ChannelHandlerContext;)V+29
j io.netty.channel.DefaultChannelPipeline.callHandlerRemoved0(Lio/netty/channel/AbstractChannelHandlerContext;)V+5
j io.netty.channel.DefaultChannelPipeline.callHandlerRemoved(Lio/netty/channel/AbstractChannelHandlerContext;)V+45
j io.netty.channel.DefaultChannelPipeline.replace0(Lio/netty/channel/AbstractChannelHandlerContext;Ljava/lang/String;Lio/netty/channel/AbstractChannelHandlerContext;)V+94
j io.netty.channel.DefaultChannelPipeline.replace(Lio/netty/channel/AbstractChannelHandlerContext;Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelHandler;+102
j io.netty.channel.DefaultChannelPipeline.replace(Lio/netty/channel/ChannelHandler;Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;+8
j io.netty.handler.ssl.SniHandler.decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+100
j io.netty.handler.codec.ByteToMessageDecoder.callDecode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+46
j io.netty.handler.codec.ByteToMessageDecoder.channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V+81
j io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(Ljava/lang/Object;)V+9
j io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelHandlerContext;+35
j io.netty.channel.DefaultChannelPipeline.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelPipeline;+5
j io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read()V+175
j io.netty.channel.nio.NioEventLoop.processSelectedKey(Ljava/nio/channels/SelectionKey;Lio/netty/channel/nio/AbstractNioChannel;)V+42
j io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized([Ljava/nio/channels/SelectionKey;)V+37
j io.netty.channel.nio.NioEventLoop.processSelectedKeys()V+15
j io.netty.channel.nio.NioEventLoop.run()V+84
j io.netty.util.concurrent.SingleThreadEventExecutor$2.run()V+13
j io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run()V+4
j java.lang.Thread.run()V+11
v ~StubRoutines::call_stub
```

```
A fatal error has been detected by the Java Runtime Environment:
SIGSEGV (0xb) at pc=0x768f0494, pid=3438, tid=1712714848
JRE version: Java(TM) SE Runtime Environment (8.0_73-b02) (build 1.8.0_73-b02)
Java VM: Java HotSpot(TM) Client VM (25.73-b02 mixed mode linux-arm )
Problematic frame:
V [libjvm.so+0x4e1494] Unsafe_GetNativeByte+0x70
```

```
From lsb_release
Distributor ID: Raspbian
Description: Raspbian GNU/Linux 8.0 (jessie)
Release: 8.0
Codename: jessie

From uname
Linux 4.1.19-v7+ armv7l
```

Should I change netty default buffers type to fix it?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/5009","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/5009/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/5009/comments","events_url":"https://api.github.com/repos/netty/netty/issues/5009/events","html_url":"https://github.com/netty/netty/issues/5009","id":142116035,"node_id":"MDU6SXNzdWUxNDIxMTYwMzU=","number":5009,"title":"Netty crashes VM on Raspberry PI 3","user":{"login":"doom369","id":1536494,"node_id":"MDQ6VXNlcjE1MzY0OTQ=","avatar_url":"https://avatars2.githubusercontent.com/u/1536494?v=4","gravatar_id":"","url":"https://api.github.com/users/doom369","html_url":"https://github.com/doom369","followers_url":"https://api.github.com/users/doom369/followers","following_url":"https://api.github.com/users/doom369/following{/other_user}","gists_url":"https://api.github.com/users/doom369/gists{/gist_id}","starred_url":"https://api.github.com/users/doom369/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/doom369/subscriptions","organizations_url":"https://api.github.com/users/doom369/orgs","repos_url":"https://api.github.com/users/doom369/repos","events_url":"https://api.github.com/users/doom369/events{/privacy}","received_events_url":"https://api.github.com/users/doom369/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2016-03-20T00:54:25Z","updated_at":"2016-03-20T13:43:02Z","closed_at":"2016-03-20T13:39:25Z","author_association":"CONTRIBUTOR","body":"Hello. In my project I'm distributing my server as jar file for my users. One of them reported crash for Rapsberry PI 3. (But is works fine with rasp pi 2). Netty 4.0.33\n\n```\nStack: [0x66110000,0x66160000], sp=0x6615e338, free space=312k\nNative frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)\nV [libjvm.so+0x4e1494] Unsafe_GetNativeByte+0x70\n\nJava frames: (J=compiled Java code, j=interpreted, Vv=VM code)\nj sun.misc.Unsafe.getByte(J)B+0\nj io.netty.util.internal.PlatformDependent0.getByte(J)B+4\nj io.netty.util.internal.PlatformDependent.getByte(J)B+1\nj io.netty.buffer.UnsafeByteBufUtil.getShort([BI)S+29\nj io.netty.buffer.UnpooledUnsafeHeapByteBuf._getShort(I)S+5\nj io.netty.buffer.UnpooledUnsafeHeapByteBuf.getShort(I)S+8\nj io.netty.buffer.AbstractByteBuf.getUnsignedShort(I)I+2\nj io.netty.handler.ssl.SslHandler.getEncryptedPacketLength(Lio/netty/buffer/ByteBuf;I)I+66\nj io.netty.handler.ssl.SslHandler.decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+89\nj io.netty.handler.codec.ByteToMessageDecoder.callDecode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+46\nj io.netty.handler.codec.ByteToMessageDecoder.channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V+81\nj io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(Ljava/lang/Object;)V+9\nj io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelHandlerContext;+35\nj io.netty.handler.codec.ByteToMessageDecoder.handlerRemoved(Lio/netty/channel/ChannelHandlerContext;)V+29\nj io.netty.channel.DefaultChannelPipeline.callHandlerRemoved0(Lio/netty/channel/AbstractChannelHandlerContext;)V+5\nj io.netty.channel.DefaultChannelPipeline.callHandlerRemoved(Lio/netty/channel/AbstractChannelHandlerContext;)V+45\nj io.netty.channel.DefaultChannelPipeline.replace0(Lio/netty/channel/AbstractChannelHandlerContext;Ljava/lang/String;Lio/netty/channel/AbstractChannelHandlerContext;)V+94\nj io.netty.channel.DefaultChannelPipeline.replace(Lio/netty/channel/AbstractChannelHandlerContext;Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelHandler;+102\nj io.netty.channel.DefaultChannelPipeline.replace(Lio/netty/channel/ChannelHandler;Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;+8\nj io.netty.handler.ssl.SniHandler.decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+100\nj io.netty.handler.codec.ByteToMessageDecoder.callDecode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V+46\nj io.netty.handler.codec.ByteToMessageDecoder.channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V+81\nj io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(Ljava/lang/Object;)V+9\nj io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelHandlerContext;+35\nj io.netty.channel.DefaultChannelPipeline.fireChannelRead(Ljava/lang/Object;)Lio/netty/channel/ChannelPipeline;+5\nj io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read()V+175\nj io.netty.channel.nio.NioEventLoop.processSelectedKey(Ljava/nio/channels/SelectionKey;Lio/netty/channel/nio/AbstractNioChannel;)V+42\nj io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized([Ljava/nio/channels/SelectionKey;)V+37\nj io.netty.channel.nio.NioEventLoop.processSelectedKeys()V+15\nj io.netty.channel.nio.NioEventLoop.run()V+84\nj io.netty.util.concurrent.SingleThreadEventExecutor$2.run()V+13\nj io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run()V+4\nj java.lang.Thread.run()V+11\nv ~StubRoutines::call_stub\n```\n\n```\nA fatal error has been detected by the Java Runtime Environment:\nSIGSEGV (0xb) at pc=0x768f0494, pid=3438, tid=1712714848\nJRE version: Java(TM) SE Runtime Environment (8.0_73-b02) (build 1.8.0_73-b02)\nJava VM: Java HotSpot(TM) Client VM (25.73-b02 mixed mode linux-arm )\nProblematic frame:\nV [libjvm.so+0x4e1494] Unsafe_GetNativeByte+0x70\n```\n\n```\nFrom lsb_release\nDistributor ID: Raspbian\nDescription: Raspbian GNU/Linux 8.0 (jessie)\nRelease: 8.0\nCodename: jessie\n\nFrom uname\nLinux 4.1.19-v7+ armv7l\n```\n\nShould I change netty default buffers type to fix it?\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["198931586","198931730","198931891"], "labels":[]}