{"id":"1689", "title":"did not read anything but decoded a message", "body":"```
protected void callDecode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
    try {
        while (in.isReadable()) {
            int outSize = out.size();
            int oldInputLength = in.readableBytes();
            decode(ctx, in, out);
            if (outSize == out.size()) {
                if (oldInputLength == in.readableBytes()) {
                    break;
                } else {
                    continue;
                }
            }

            if (oldInputLength == in.readableBytes()) {
                throw new DecoderException(
                        StringUtil.simpleClassName(getClass()) +
                        \".decode() did not read anything but decoded a message.\");
            }

            if (isSingleDecode()) {
                break;
            }
        }
    } catch (DecoderException e) {
        throw e;
    } catch (Throwable cause) {
        throw new DecoderException(cause);
    }
}
```

int oldInputLength = in.readableBytes();

Is this a bug?
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1689","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1689/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1689/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1689/events","html_url":"https://github.com/netty/netty/issues/1689","id":17497921,"node_id":"MDU6SXNzdWUxNzQ5NzkyMQ==","number":1689,"title":"did not read anything but decoded a message","user":{"login":"hyleeon","id":1011007,"node_id":"MDQ6VXNlcjEwMTEwMDc=","avatar_url":"https://avatars2.githubusercontent.com/u/1011007?v=4","gravatar_id":"","url":"https://api.github.com/users/hyleeon","html_url":"https://github.com/hyleeon","followers_url":"https://api.github.com/users/hyleeon/followers","following_url":"https://api.github.com/users/hyleeon/following{/other_user}","gists_url":"https://api.github.com/users/hyleeon/gists{/gist_id}","starred_url":"https://api.github.com/users/hyleeon/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hyleeon/subscriptions","organizations_url":"https://api.github.com/users/hyleeon/orgs","repos_url":"https://api.github.com/users/hyleeon/repos","events_url":"https://api.github.com/users/hyleeon/events{/privacy}","received_events_url":"https://api.github.com/users/hyleeon/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2013-08-01T09:24:25Z","updated_at":"2013-08-01T09:36:42Z","closed_at":"2013-08-01T09:36:42Z","author_association":"NONE","body":"```\nprotected void callDecode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {\n    try {\n        while (in.isReadable()) {\n            int outSize = out.size();\n            int oldInputLength = in.readableBytes();\n            decode(ctx, in, out);\n            if (outSize == out.size()) {\n                if (oldInputLength == in.readableBytes()) {\n                    break;\n                } else {\n                    continue;\n                }\n            }\n\n            if (oldInputLength == in.readableBytes()) {\n                throw new DecoderException(\n                        StringUtil.simpleClassName(getClass()) +\n                        \".decode() did not read anything but decoded a message.\");\n            }\n\n            if (isSingleDecode()) {\n                break;\n            }\n        }\n    } catch (DecoderException e) {\n        throw e;\n    } catch (Throwable cause) {\n        throw new DecoderException(cause);\n    }\n}\n```\n\nint oldInputLength = in.readableBytes();\n\nIs this a bug?\n","closed_by":{"login":"hyleeon","id":1011007,"node_id":"MDQ6VXNlcjEwMTEwMDc=","avatar_url":"https://avatars2.githubusercontent.com/u/1011007?v=4","gravatar_id":"","url":"https://api.github.com/users/hyleeon","html_url":"https://github.com/hyleeon","followers_url":"https://api.github.com/users/hyleeon/followers","following_url":"https://api.github.com/users/hyleeon/following{/other_user}","gists_url":"https://api.github.com/users/hyleeon/gists{/gist_id}","starred_url":"https://api.github.com/users/hyleeon/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hyleeon/subscriptions","organizations_url":"https://api.github.com/users/hyleeon/orgs","repos_url":"https://api.github.com/users/hyleeon/repos","events_url":"https://api.github.com/users/hyleeon/events{/privacy}","received_events_url":"https://api.github.com/users/hyleeon/received_events","type":"User","site_admin":false}}", "commentIds":["21924704","21924807","21925233"], "labels":[]}