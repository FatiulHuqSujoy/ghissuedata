{"id":"1688", "title":"NoClassDefFoundError for UDT using maven central repository", "body":"Running a simplified version of the UDT message example using the maven central repository a NoClassDefFoundError is thrown although it compiles without any issues:

```
Exception in thread \"main\" java.lang.NoClassDefFoundError: com/barchart/udt/nio/SelectorProviderUDT
    at com.example.netty.MsgEchoServer.run(MsgEchoServer.java:20)
    at com.example.netty.MsgEchoServer.main(MsgEchoServer.java:48)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:606)
    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:120)
Caused by: java.lang.ClassNotFoundException: com.barchart.udt.nio.SelectorProviderUDT
    at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
    at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
    at java.security.AccessController.doPrivileged(Native Method)
    at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
    at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
    at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
    at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
```

A solution is to add the missing dependency to maven but I don't think this is intended behavior as the download page states that \"Netty has no mandatory external dependencies.\".

```
<dependency>
    <groupId>com.barchart.udt</groupId>
    <artifactId>barchart-udt-bundle</artifactId>
    <version>2.3.0</version>
</dependency>
```

My maven file:

```
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<project xmlns=\"http://maven.apache.org/POM/4.0.0\"
         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
         xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">
    <modelVersion>4.0.0</modelVersion>

    <groupId>NettyTest</groupId>
    <artifactId>NettyTest</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <dependency>
            <groupId>io.netty</groupId>
            <artifactId>netty-all</artifactId>
            <version>4.0.5.Final</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>

</project>
```

And the simplified version of the example:

```
public class MsgEchoClient {

    public void run() throws Exception {
        final ThreadFactory connectFactory = new UtilThreadFactory(\"connect\");
        final NioEventLoopGroup connectGroup = new NioEventLoopGroup(1,
                connectFactory, NioUdtProvider.MESSAGE_PROVIDER);
        try {
            final Bootstrap boot = new Bootstrap();
            boot.group(connectGroup)
                    .channelFactory(NioUdtProvider.MESSAGE_CONNECTOR)
                    .handler(new ChannelInitializer<UdtChannel>() {
                        @Override
                        public void initChannel(final UdtChannel ch)
                                throws Exception {
                            ch.pipeline().addLast(
                                    new LoggingHandler(LogLevel.INFO),
                                    new MsgEchoClientHandler());
                        }
                    });
            final ChannelFuture f = boot.connect(\"localhost\", 1234).sync();
            f.channel().closeFuture().sync();
        } finally {
            connectGroup.shutdownGracefully();
        }
    }

    public static void main(final String[] args) throws Exception {
        new MsgEchoClient().run();
    }

}
```

```
public class MsgEchoServer {

    public void run() throws Exception {
        final ThreadFactory acceptFactory = new UtilThreadFactory(\"accept\");
        final ThreadFactory connectFactory = new UtilThreadFactory(\"connect\");
        final NioEventLoopGroup acceptGroup = new NioEventLoopGroup(1,
                acceptFactory, NioUdtProvider.MESSAGE_PROVIDER);
        final NioEventLoopGroup connectGroup = new NioEventLoopGroup(1,
                connectFactory, NioUdtProvider.MESSAGE_PROVIDER);
        try {
            final ServerBootstrap boot = new ServerBootstrap();
            boot.group(acceptGroup, connectGroup)
                    .channelFactory(NioUdtProvider.MESSAGE_ACCEPTOR)
                    .option(ChannelOption.SO_BACKLOG, 10)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<UdtChannel>() {
                        @Override
                        public void initChannel(final UdtChannel ch)
                                throws Exception {
                            ch.pipeline().addLast(
                                    new LoggingHandler(LogLevel.INFO),
                                    new MsgEchoServerHandler());
                        }
                    });
            final ChannelFuture future = boot.bind(1234).sync();
            future.channel().closeFuture().sync();
        } finally {
            acceptGroup.shutdownGracefully();
            connectGroup.shutdownGracefully();
        }
    }

    public static void main(final String[] args) throws Exception {
        new MsgEchoServer().run();
    }

}
```

```
public class MsgEchoServerHandler extends ChannelInboundHandlerAdapter {

}
```

```
public class MsgEchoClientHandler extends ChannelInboundHandlerAdapter {

}
```

```
public class UtilThreadFactory implements ThreadFactory {

    private static final AtomicInteger counter = new AtomicInteger();

    private final String name;

    public UtilThreadFactory(final String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(final Runnable runnable) {
        return new Thread(runnable, name + '-' + counter.getAndIncrement());
    }
}
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1688","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1688/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1688/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1688/events","html_url":"https://github.com/netty/netty/issues/1688","id":17497786,"node_id":"MDU6SXNzdWUxNzQ5Nzc4Ng==","number":1688,"title":"NoClassDefFoundError for UDT using maven central repository","user":{"login":"steffengr","id":4143920,"node_id":"MDQ6VXNlcjQxNDM5MjA=","avatar_url":"https://avatars0.githubusercontent.com/u/4143920?v=4","gravatar_id":"","url":"https://api.github.com/users/steffengr","html_url":"https://github.com/steffengr","followers_url":"https://api.github.com/users/steffengr/followers","following_url":"https://api.github.com/users/steffengr/following{/other_user}","gists_url":"https://api.github.com/users/steffengr/gists{/gist_id}","starred_url":"https://api.github.com/users/steffengr/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/steffengr/subscriptions","organizations_url":"https://api.github.com/users/steffengr/orgs","repos_url":"https://api.github.com/users/steffengr/repos","events_url":"https://api.github.com/users/steffengr/events{/privacy}","received_events_url":"https://api.github.com/users/steffengr/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-08-01T09:21:03Z","updated_at":"2013-08-01T09:23:04Z","closed_at":"2013-08-01T09:23:04Z","author_association":"NONE","body":"Running a simplified version of the UDT message example using the maven central repository a NoClassDefFoundError is thrown although it compiles without any issues:\n\n```\nException in thread \"main\" java.lang.NoClassDefFoundError: com/barchart/udt/nio/SelectorProviderUDT\n    at com.example.netty.MsgEchoServer.run(MsgEchoServer.java:20)\n    at com.example.netty.MsgEchoServer.main(MsgEchoServer.java:48)\n    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\n    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n    at java.lang.reflect.Method.invoke(Method.java:606)\n    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:120)\nCaused by: java.lang.ClassNotFoundException: com.barchart.udt.nio.SelectorProviderUDT\n    at java.net.URLClassLoader$1.run(URLClassLoader.java:366)\n    at java.net.URLClassLoader$1.run(URLClassLoader.java:355)\n    at java.security.AccessController.doPrivileged(Native Method)\n    at java.net.URLClassLoader.findClass(URLClassLoader.java:354)\n    at java.lang.ClassLoader.loadClass(ClassLoader.java:424)\n    at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)\n    at java.lang.ClassLoader.loadClass(ClassLoader.java:357)\n```\n\nA solution is to add the missing dependency to maven but I don't think this is intended behavior as the download page states that \"Netty has no mandatory external dependencies.\".\n\n```\n<dependency>\n    <groupId>com.barchart.udt</groupId>\n    <artifactId>barchart-udt-bundle</artifactId>\n    <version>2.3.0</version>\n</dependency>\n```\n\nMy maven file:\n\n```\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<project xmlns=\"http://maven.apache.org/POM/4.0.0\"\n         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n         xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">\n    <modelVersion>4.0.0</modelVersion>\n\n    <groupId>NettyTest</groupId>\n    <artifactId>NettyTest</artifactId>\n    <version>1.0-SNAPSHOT</version>\n\n    <dependencies>\n        <dependency>\n            <groupId>io.netty</groupId>\n            <artifactId>netty-all</artifactId>\n            <version>4.0.5.Final</version>\n            <scope>compile</scope>\n        </dependency>\n    </dependencies>\n\n</project>\n```\n\nAnd the simplified version of the example:\n\n```\npublic class MsgEchoClient {\n\n    public void run() throws Exception {\n        final ThreadFactory connectFactory = new UtilThreadFactory(\"connect\");\n        final NioEventLoopGroup connectGroup = new NioEventLoopGroup(1,\n                connectFactory, NioUdtProvider.MESSAGE_PROVIDER);\n        try {\n            final Bootstrap boot = new Bootstrap();\n            boot.group(connectGroup)\n                    .channelFactory(NioUdtProvider.MESSAGE_CONNECTOR)\n                    .handler(new ChannelInitializer<UdtChannel>() {\n                        @Override\n                        public void initChannel(final UdtChannel ch)\n                                throws Exception {\n                            ch.pipeline().addLast(\n                                    new LoggingHandler(LogLevel.INFO),\n                                    new MsgEchoClientHandler());\n                        }\n                    });\n            final ChannelFuture f = boot.connect(\"localhost\", 1234).sync();\n            f.channel().closeFuture().sync();\n        } finally {\n            connectGroup.shutdownGracefully();\n        }\n    }\n\n    public static void main(final String[] args) throws Exception {\n        new MsgEchoClient().run();\n    }\n\n}\n```\n\n```\npublic class MsgEchoServer {\n\n    public void run() throws Exception {\n        final ThreadFactory acceptFactory = new UtilThreadFactory(\"accept\");\n        final ThreadFactory connectFactory = new UtilThreadFactory(\"connect\");\n        final NioEventLoopGroup acceptGroup = new NioEventLoopGroup(1,\n                acceptFactory, NioUdtProvider.MESSAGE_PROVIDER);\n        final NioEventLoopGroup connectGroup = new NioEventLoopGroup(1,\n                connectFactory, NioUdtProvider.MESSAGE_PROVIDER);\n        try {\n            final ServerBootstrap boot = new ServerBootstrap();\n            boot.group(acceptGroup, connectGroup)\n                    .channelFactory(NioUdtProvider.MESSAGE_ACCEPTOR)\n                    .option(ChannelOption.SO_BACKLOG, 10)\n                    .handler(new LoggingHandler(LogLevel.INFO))\n                    .childHandler(new ChannelInitializer<UdtChannel>() {\n                        @Override\n                        public void initChannel(final UdtChannel ch)\n                                throws Exception {\n                            ch.pipeline().addLast(\n                                    new LoggingHandler(LogLevel.INFO),\n                                    new MsgEchoServerHandler());\n                        }\n                    });\n            final ChannelFuture future = boot.bind(1234).sync();\n            future.channel().closeFuture().sync();\n        } finally {\n            acceptGroup.shutdownGracefully();\n            connectGroup.shutdownGracefully();\n        }\n    }\n\n    public static void main(final String[] args) throws Exception {\n        new MsgEchoServer().run();\n    }\n\n}\n```\n\n```\npublic class MsgEchoServerHandler extends ChannelInboundHandlerAdapter {\n\n}\n```\n\n```\npublic class MsgEchoClientHandler extends ChannelInboundHandlerAdapter {\n\n}\n```\n\n```\npublic class UtilThreadFactory implements ThreadFactory {\n\n    private static final AtomicInteger counter = new AtomicInteger();\n\n    private final String name;\n\n    public UtilThreadFactory(final String name) {\n        this.name = name;\n    }\n\n    @Override\n    public Thread newThread(final Runnable runnable) {\n        return new Thread(runnable, name + '-' + counter.getAndIncrement());\n    }\n}\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["21924539"], "labels":[]}