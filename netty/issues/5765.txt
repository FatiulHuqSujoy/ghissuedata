{"id":"5765", "title":"channelRead0 method is fired two times for SimpleChannelInboundHandler in case of an HTTP request from Chrome", "body":"channel pipeline is prepared as follows:

```
public void initChannel(SocketChannel ch) {
        ChannelPipeline p = ch.pipeline();
        if (sslCtx != null) {
            p.addLast(sslCtx.newHandler(ch.alloc()));
        }
        p.addLast(new HttpRequestDecoder());
        p.addLast(new HttpObjectAggregator(1048576));
        p.addLast(new HttpResponseEncoder());
        p.addLast(new HttpSnoopServerHandler());
    }

```

and the handler : 

public class HttpSnoopServerHandler extends SimpleChannelInboundHandler<Object> {

```
public class HttpSnoopServerHandler extends SimpleChannelInboundHandler<Object> {
    private FullHttpRequest request;

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) {

        System.out.println(\"channelRead0\");

    }

}
```

However from **firefox**, this is firing just a single time as expected.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/5765","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/5765/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/5765/comments","events_url":"https://api.github.com/repos/netty/netty/issues/5765/events","html_url":"https://github.com/netty/netty/issues/5765","id":173948894,"node_id":"MDU6SXNzdWUxNzM5NDg4OTQ=","number":5765,"title":"channelRead0 method is fired two times for SimpleChannelInboundHandler in case of an HTTP request from Chrome","user":{"login":"arafat-al-mahmud","id":5181525,"node_id":"MDQ6VXNlcjUxODE1MjU=","avatar_url":"https://avatars1.githubusercontent.com/u/5181525?v=4","gravatar_id":"","url":"https://api.github.com/users/arafat-al-mahmud","html_url":"https://github.com/arafat-al-mahmud","followers_url":"https://api.github.com/users/arafat-al-mahmud/followers","following_url":"https://api.github.com/users/arafat-al-mahmud/following{/other_user}","gists_url":"https://api.github.com/users/arafat-al-mahmud/gists{/gist_id}","starred_url":"https://api.github.com/users/arafat-al-mahmud/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/arafat-al-mahmud/subscriptions","organizations_url":"https://api.github.com/users/arafat-al-mahmud/orgs","repos_url":"https://api.github.com/users/arafat-al-mahmud/repos","events_url":"https://api.github.com/users/arafat-al-mahmud/events{/privacy}","received_events_url":"https://api.github.com/users/arafat-al-mahmud/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2016-08-30T07:32:03Z","updated_at":"2016-09-05T10:51:10Z","closed_at":"2016-09-05T10:51:10Z","author_association":"NONE","body":"channel pipeline is prepared as follows:\n\n```\npublic void initChannel(SocketChannel ch) {\n        ChannelPipeline p = ch.pipeline();\n        if (sslCtx != null) {\n            p.addLast(sslCtx.newHandler(ch.alloc()));\n        }\n        p.addLast(new HttpRequestDecoder());\n        p.addLast(new HttpObjectAggregator(1048576));\n        p.addLast(new HttpResponseEncoder());\n        p.addLast(new HttpSnoopServerHandler());\n    }\n\n```\n\nand the handler : \n\npublic class HttpSnoopServerHandler extends SimpleChannelInboundHandler<Object> {\n\n```\npublic class HttpSnoopServerHandler extends SimpleChannelInboundHandler<Object> {\n    private FullHttpRequest request;\n\n    @Override\n    public void channelReadComplete(ChannelHandlerContext ctx) {\n        ctx.flush();\n    }\n\n    @Override\n    protected void channelRead0(ChannelHandlerContext ctx, Object msg) {\n\n        System.out.println(\"channelRead0\");\n\n    }\n\n}\n```\n\nHowever from **firefox**, this is firing just a single time as expected.\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["243356924","244718400"], "labels":[]}