{"id":"1643", "title":"Why is HashedWheelTimer not working correctly??", "body":"hi~~~

I try HashedWheelTimer in Netty.
This is performed by my code, but not repeat.
Just one, like this...

\"newTimerout : I'm here~!\"

I want to see continue \"newTimerout : I'm here~!\" in server when I use to HashedWheelTimer.
Like this...

\"newTimerout : I'm here~!\"
\"newTimerout : I'm here~!\"
\"newTimerout : I'm here~!\"
\"newTimerout : I'm here~!\"
\"newTimerout : I'm here~!\"
...

Please tell me wrong part.
Thank you for your kindness.
# Main_Server.java

public class Main_Server {

 HashedWheelTimer timer = new HashedWheelTimer();
 private final int port;

 public Main_Server(int port) {
  this.port = port;
 }

 public void run() {
  timer.start();
  ServerBootstrap bootstrap = new ServerBootstrap(
                new NioServerSocketChannelFactory(
                        Executors.newCachedThreadPool(),
                        Executors.newCachedThreadPool()));

  bootstrap.setPipelineFactory(new Test_Factory(timer));
  bootstrap.bind(new InetSocketAddress(port));
 }

 public static void main(String[] args) throws Exception {
  int port = 8080;
  new Main_Server(port).run();
 }
}
# Test_Factory.java

public class Test_Factory implements ChannelPipelineFactory{

 public HashedWheelTimer timer;

 public Test_Factory(HashedWheelTimer timer) {
  this.timer = timer;
 }

 public ChannelPipeline getPipeline() throws Exception {
  ChannelPipeline pipeline = Channels.pipeline();

  pipeline.addLast(\"framer\", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
  pipeline.addLast(\"decoder\", new StringDecoder());
  pipeline.addLast(\"encoder\", new StringEncoder());
  pipeline.addLast(\"handler\", new Test_Handler(timer));

  return pipeline;
 }
}
# Test_Handler.java

public class Test_Handler extends SimpleChannelUpstreamHandler{

 public HashedWheelTimer timer;
 public Channel myChannel;
 private static final ChannelGroup channels = new DefaultChannelGroup();

 public Test_Handler(HashedWheelTimer timer) {
  this.timer = timer;
 }

 @Override
 public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
  channels.add(ctx.getChannel());
  myChannel = e.getChannel();

  timer.newTimeout(
    new TimerTask() {
     public void run(Timeout timeout) throws Exception {
      System.out.println(\"newTimerout : I'm here~!\");
     }
    }
  , 500, TimeUnit.MILLISECONDS);
  timer.start();
 }

 @Override
 public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
  myChannel.write(request + '\\n');
 }
}
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1643","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1643/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1643/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1643/events","html_url":"https://github.com/netty/netty/issues/1643","id":17133180,"node_id":"MDU6SXNzdWUxNzEzMzE4MA==","number":1643,"title":"Why is HashedWheelTimer not working correctly??","user":{"login":"llldk8009","id":5063395,"node_id":"MDQ6VXNlcjUwNjMzOTU=","avatar_url":"https://avatars1.githubusercontent.com/u/5063395?v=4","gravatar_id":"","url":"https://api.github.com/users/llldk8009","html_url":"https://github.com/llldk8009","followers_url":"https://api.github.com/users/llldk8009/followers","following_url":"https://api.github.com/users/llldk8009/following{/other_user}","gists_url":"https://api.github.com/users/llldk8009/gists{/gist_id}","starred_url":"https://api.github.com/users/llldk8009/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/llldk8009/subscriptions","organizations_url":"https://api.github.com/users/llldk8009/orgs","repos_url":"https://api.github.com/users/llldk8009/repos","events_url":"https://api.github.com/users/llldk8009/events{/privacy}","received_events_url":"https://api.github.com/users/llldk8009/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2013-07-24T00:14:46Z","updated_at":"2013-07-25T13:18:17Z","closed_at":"2013-07-24T11:08:02Z","author_association":"NONE","body":"hi~~~\n\nI try HashedWheelTimer in Netty.\nThis is performed by my code, but not repeat.\nJust one, like this...\n\n\"newTimerout : I'm here~!\"\n\nI want to see continue \"newTimerout : I'm here~!\" in server when I use to HashedWheelTimer.\nLike this...\n\n\"newTimerout : I'm here~!\"\n\"newTimerout : I'm here~!\"\n\"newTimerout : I'm here~!\"\n\"newTimerout : I'm here~!\"\n\"newTimerout : I'm here~!\"\n...\n\nPlease tell me wrong part.\nThank you for your kindness.\n# Main_Server.java\n\npublic class Main_Server {\n\n HashedWheelTimer timer = new HashedWheelTimer();\n private final int port;\n\n public Main_Server(int port) {\n  this.port = port;\n }\n\n public void run() {\n  timer.start();\n  ServerBootstrap bootstrap = new ServerBootstrap(\n                new NioServerSocketChannelFactory(\n                        Executors.newCachedThreadPool(),\n                        Executors.newCachedThreadPool()));\n\n  bootstrap.setPipelineFactory(new Test_Factory(timer));\n  bootstrap.bind(new InetSocketAddress(port));\n }\n\n public static void main(String[] args) throws Exception {\n  int port = 8080;\n  new Main_Server(port).run();\n }\n}\n# Test_Factory.java\n\npublic class Test_Factory implements ChannelPipelineFactory{\n\n public HashedWheelTimer timer;\n\n public Test_Factory(HashedWheelTimer timer) {\n  this.timer = timer;\n }\n\n public ChannelPipeline getPipeline() throws Exception {\n  ChannelPipeline pipeline = Channels.pipeline();\n\n  pipeline.addLast(\"framer\", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));\n  pipeline.addLast(\"decoder\", new StringDecoder());\n  pipeline.addLast(\"encoder\", new StringEncoder());\n  pipeline.addLast(\"handler\", new Test_Handler(timer));\n\n  return pipeline;\n }\n}\n# Test_Handler.java\n\npublic class Test_Handler extends SimpleChannelUpstreamHandler{\n\n public HashedWheelTimer timer;\n public Channel myChannel;\n private static final ChannelGroup channels = new DefaultChannelGroup();\n\n public Test_Handler(HashedWheelTimer timer) {\n  this.timer = timer;\n }\n\n @Override\n public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {\n  channels.add(ctx.getChannel());\n  myChannel = e.getChannel();\n\n  timer.newTimeout(\n    new TimerTask() {\n     public void run(Timeout timeout) throws Exception {\n      System.out.println(\"newTimerout : I'm here~!\");\n     }\n    }\n  , 500, TimeUnit.MILLISECONDS);\n  timer.start();\n }\n\n @Override\n public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {\n  myChannel.write(request + '\\n');\n }\n}\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["21477671","21540028","21540134","21541949"], "labels":[]}