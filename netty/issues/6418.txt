{"id":"6418", "title":"Http server parsing post parameters", "body":"Parsing on disk
Http server parsing post parameters failed with \"file exists already\" error

### Steps to reproduce
Post empty value for a field

### Netty version
I am using 4.1.6. 

### I RESOLVED IT WITH OVERRIDING AbstractDiskHttpData->setContent
    public void setContent(ByteBuf buffer) throws IOException {
        if (buffer == null) {
            throw new NullPointerException(\"buffer\");
        }
        try {
            size = buffer.readableBytes();
            checkSize(size);
            if (definedSize > 0 && definedSize < size) {
                throw new IOException(\"Out of size: \" + size + \" > \" + definedSize);
            }
            if (file == null) {
                file = tempFile();
            }
            if (buffer.readableBytes() == 0) {
////////////////////// CHANGE BEGIN ////////////////////////
            	if(file.exists())
            		return;
////////////////////// CHANGE END ////////////////////////
                // empty file
                if (!file.createNewFile()) {
                    throw new IOException(\"file exists already: \" + file);
                }
                return;
            }
            FileOutputStream outputStream = new FileOutputStream(file);
            try {
                FileChannel localfileChannel = outputStream.getChannel();
                ByteBuffer byteBuffer = buffer.nioBuffer();
                int written = 0;
                while (written < size) {
                    written += localfileChannel.write(byteBuffer);
                }
                buffer.readerIndex(buffer.readerIndex() + written);
                localfileChannel.force(false);
            } finally {
                outputStream.close();
            }
            setCompleted();
        } finally {
            // Release the buffer as it was retained before and we not need a reference to it at all
            // See https://github.com/netty/netty/issues/1516
            buffer.release();
        }
    }
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6418","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6418/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6418/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6418/events","html_url":"https://github.com/netty/netty/issues/6418","id":208909043,"node_id":"MDU6SXNzdWUyMDg5MDkwNDM=","number":6418,"title":"Http server parsing post parameters","user":{"login":"yusufbulentavci","id":22977177,"node_id":"MDQ6VXNlcjIyOTc3MTc3","avatar_url":"https://avatars0.githubusercontent.com/u/22977177?v=4","gravatar_id":"","url":"https://api.github.com/users/yusufbulentavci","html_url":"https://github.com/yusufbulentavci","followers_url":"https://api.github.com/users/yusufbulentavci/followers","following_url":"https://api.github.com/users/yusufbulentavci/following{/other_user}","gists_url":"https://api.github.com/users/yusufbulentavci/gists{/gist_id}","starred_url":"https://api.github.com/users/yusufbulentavci/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/yusufbulentavci/subscriptions","organizations_url":"https://api.github.com/users/yusufbulentavci/orgs","repos_url":"https://api.github.com/users/yusufbulentavci/repos","events_url":"https://api.github.com/users/yusufbulentavci/events{/privacy}","received_events_url":"https://api.github.com/users/yusufbulentavci/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/187","html_url":"https://github.com/netty/netty/milestone/187","labels_url":"https://api.github.com/repos/netty/netty/milestones/187/labels","id":3280974,"node_id":"MDk6TWlsZXN0b25lMzI4MDk3NA==","number":187,"title":"4.1.25.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":43,"state":"closed","created_at":"2018-04-19T13:10:45Z","updated_at":"2018-05-24T13:16:21Z","due_on":null,"closed_at":"2018-05-14T12:11:58Z"},"comments":3,"created_at":"2017-02-20T15:18:46Z","updated_at":"2018-04-30T06:39:40Z","closed_at":"2018-04-30T06:39:25Z","author_association":"NONE","body":"Parsing on disk\r\nHttp server parsing post parameters failed with \"file exists already\" error\r\n\r\n### Steps to reproduce\r\nPost empty value for a field\r\n\r\n### Netty version\r\nI am using 4.1.6. \r\n\r\n### I RESOLVED IT WITH OVERRIDING AbstractDiskHttpData->setContent\r\n    public void setContent(ByteBuf buffer) throws IOException {\r\n        if (buffer == null) {\r\n            throw new NullPointerException(\"buffer\");\r\n        }\r\n        try {\r\n            size = buffer.readableBytes();\r\n            checkSize(size);\r\n            if (definedSize > 0 && definedSize < size) {\r\n                throw new IOException(\"Out of size: \" + size + \" > \" + definedSize);\r\n            }\r\n            if (file == null) {\r\n                file = tempFile();\r\n            }\r\n            if (buffer.readableBytes() == 0) {\r\n////////////////////// CHANGE BEGIN ////////////////////////\r\n            \tif(file.exists())\r\n            \t\treturn;\r\n////////////////////// CHANGE END ////////////////////////\r\n                // empty file\r\n                if (!file.createNewFile()) {\r\n                    throw new IOException(\"file exists already: \" + file);\r\n                }\r\n                return;\r\n            }\r\n            FileOutputStream outputStream = new FileOutputStream(file);\r\n            try {\r\n                FileChannel localfileChannel = outputStream.getChannel();\r\n                ByteBuffer byteBuffer = buffer.nioBuffer();\r\n                int written = 0;\r\n                while (written < size) {\r\n                    written += localfileChannel.write(byteBuffer);\r\n                }\r\n                buffer.readerIndex(buffer.readerIndex() + written);\r\n                localfileChannel.force(false);\r\n            } finally {\r\n                outputStream.close();\r\n            }\r\n            setCompleted();\r\n        } finally {\r\n            // Release the buffer as it was retained before and we not need a reference to it at all\r\n            // See https://github.com/netty/netty/issues/1516\r\n            buffer.release();\r\n        }\r\n    }\r\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["281284092","282818851","363945910"], "labels":["defect"]}