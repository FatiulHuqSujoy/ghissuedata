{"id":"1008", "title":"Problems with statefulness in HttpObjectEncoder", "body":"I'm running into trouble with a framework I've been modifying to keep track of changes in Netty 4.  Specifically, within the last few weeks, code which worked before now chokes on a DefaultFullHttpResponse.

Specifically what appears to be happening is that an HttpResponseEncoder appears to have become stateful (the pipeline is set up based on code from the HttpSnoopServer example):
pipeline.addLast(\"encoder\", new HttpResponseEncoder());

Where it is tripping up is this test in HttpObjectEncoder:

```
        if (state != ST_INIT) {
            throw new IllegalStateException(\"unexpected message type: \" + msg.getClass().getSimpleName());
        }
```

it seems odd and likely wrong that the encoder would need to be stateful at all.  Or am I using it wrong?  I could wrap HttpResponseEncoder in as delegate that is recreated on each call...

The source code to all this is available - to reproduce the problem:
1. Get the source code:
    hg clone http://timboudreau.com/useful
2. Checkout the netty 4 branch
    cd useful && hg checkout netty-4-alpha4
2. Build the master pom, or at least the dependencies of propeller/propeller
3. Open the unit test DemoApp in the project propeller/propeller and uncomment the Thread.sleep() line in test() so the app will stay up and running
4. Go to http://localhost:8080/index.html and watch the log

(you may need to build Guice 3.1.0-SNAPSHOT to do this)

```
io.netty.handler.codec.EncoderException: java.lang.IllegalStateException: unexpected message type: DefaultHttpResponse
        at io.netty.handler.codec.MessageToByteEncoder.flush(MessageToByteEncoder.java:81)
        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush0(DefaultChannelHandlerContext.java:1525)
        at io.netty.channel.DefaultChannelHandlerContext.write0(DefaultChannelHandlerContext.java:1663)
        at io.netty.channel.DefaultChannelHandlerContext.access$2700(DefaultChannelHandlerContext.java:39)
        at io.netty.channel.DefaultChannelHandlerContext$24.run(DefaultChannelHandlerContext.java:1638)
        at io.netty.channel.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:259)
        at io.netty.channel.socket.nio.NioEventLoop.run(NioEventLoop.java:298)
        at io.netty.channel.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:110)
        at java.lang.Thread.run(Thread.java:722)
Caused by: java.lang.IllegalStateException: unexpected message type: DefaultHttpResponse
        at io.netty.handler.codec.http.HttpObjectEncoder.encode(HttpObjectEncoder.java:63)
        at io.netty.handler.codec.MessageToByteEncoder.flush(MessageToByteEncoder.java:76)
        ... 8 more
```
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1008","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1008/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1008/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1008/events","html_url":"https://github.com/netty/netty/issues/1008","id":10528386,"node_id":"MDU6SXNzdWUxMDUyODM4Ng==","number":1008,"title":"Problems with statefulness in HttpObjectEncoder","user":{"login":"timboudreau","id":1901175,"node_id":"MDQ6VXNlcjE5MDExNzU=","avatar_url":"https://avatars3.githubusercontent.com/u/1901175?v=4","gravatar_id":"","url":"https://api.github.com/users/timboudreau","html_url":"https://github.com/timboudreau","followers_url":"https://api.github.com/users/timboudreau/followers","following_url":"https://api.github.com/users/timboudreau/following{/other_user}","gists_url":"https://api.github.com/users/timboudreau/gists{/gist_id}","starred_url":"https://api.github.com/users/timboudreau/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/timboudreau/subscriptions","organizations_url":"https://api.github.com/users/timboudreau/orgs","repos_url":"https://api.github.com/users/timboudreau/repos","events_url":"https://api.github.com/users/timboudreau/events{/privacy}","received_events_url":"https://api.github.com/users/timboudreau/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":14,"created_at":"2013-02-01T04:11:21Z","updated_at":"2014-05-05T04:42:28Z","closed_at":"2013-02-14T19:33:36Z","author_association":"CONTRIBUTOR","body":"I'm running into trouble with a framework I've been modifying to keep track of changes in Netty 4.  Specifically, within the last few weeks, code which worked before now chokes on a DefaultFullHttpResponse.\n\nSpecifically what appears to be happening is that an HttpResponseEncoder appears to have become stateful (the pipeline is set up based on code from the HttpSnoopServer example):\npipeline.addLast(\"encoder\", new HttpResponseEncoder());\n\nWhere it is tripping up is this test in HttpObjectEncoder:\n\n```\n        if (state != ST_INIT) {\n            throw new IllegalStateException(\"unexpected message type: \" + msg.getClass().getSimpleName());\n        }\n```\n\nit seems odd and likely wrong that the encoder would need to be stateful at all.  Or am I using it wrong?  I could wrap HttpResponseEncoder in as delegate that is recreated on each call...\n\nThe source code to all this is available - to reproduce the problem:\n1. Get the source code:\n    hg clone http://timboudreau.com/useful\n2. Checkout the netty 4 branch\n    cd useful && hg checkout netty-4-alpha4\n2. Build the master pom, or at least the dependencies of propeller/propeller\n3. Open the unit test DemoApp in the project propeller/propeller and uncomment the Thread.sleep() line in test() so the app will stay up and running\n4. Go to http://localhost:8080/index.html and watch the log\n\n(you may need to build Guice 3.1.0-SNAPSHOT to do this)\n\n```\nio.netty.handler.codec.EncoderException: java.lang.IllegalStateException: unexpected message type: DefaultHttpResponse\n        at io.netty.handler.codec.MessageToByteEncoder.flush(MessageToByteEncoder.java:81)\n        at io.netty.channel.DefaultChannelHandlerContext.invokeFlush0(DefaultChannelHandlerContext.java:1525)\n        at io.netty.channel.DefaultChannelHandlerContext.write0(DefaultChannelHandlerContext.java:1663)\n        at io.netty.channel.DefaultChannelHandlerContext.access$2700(DefaultChannelHandlerContext.java:39)\n        at io.netty.channel.DefaultChannelHandlerContext$24.run(DefaultChannelHandlerContext.java:1638)\n        at io.netty.channel.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:259)\n        at io.netty.channel.socket.nio.NioEventLoop.run(NioEventLoop.java:298)\n        at io.netty.channel.SingleThreadEventExecutor$2.run(SingleThreadEventExecutor.java:110)\n        at java.lang.Thread.run(Thread.java:722)\nCaused by: java.lang.IllegalStateException: unexpected message type: DefaultHttpResponse\n        at io.netty.handler.codec.http.HttpObjectEncoder.encode(HttpObjectEncoder.java:63)\n        at io.netty.handler.codec.MessageToByteEncoder.flush(MessageToByteEncoder.java:76)\n        ... 8 more\n```\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["12982162","12998924","13028420","13028620","13028866","13028948","13029261","13036560","13037758","13043687","13045932","13476952","13478328","13573865"], "labels":[]}