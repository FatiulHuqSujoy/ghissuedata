{"id":"6963", "title":"-Dio.netty.packagePrefix should not be required when shading", "body":"### Expected behavior

A library may use shading transparently to the user and would be able to be combined with other libraries that also use Netty. Similarly, an application should be able to use shading transparently to the user.

### Actual behavior

The shader has to choose whether to rename the native components of epoll and tcnative. If they are renamed, then `-Dio.netty.packagePrefix` must be set at runtime, which may not be possible programatically. If they aren't renamed, then epoll and tcnative can fail when mixed with other libraries because: 1) there is a name collision of the `META-INF/native/` files in the class path and 2) [`System.loadLibrary(String)` does nothing if it previously loaded a particular name](http://docs.oracle.com/javase/8/docs/api/java/lang/Runtime.html#loadLibrary-java.lang.String-).

### Possible fixes

`NativeLibraryLoader` [currently](https://github.com/netty/netty/blob/f208b147a6c1748092f1d23f71865e9082b00534/common/src/main/java/io/netty/util/internal/NativeLibraryLoader.java#L188) defaults to a package prefix of `\"\"`:
```java
String name = SystemPropertyUtil.get(\"io.netty.packagePrefix\", \"\").replace('.', '-') + originalName;
```

Instead of defaulting to `\"\"`, I think it should at least attempt to find a package prefix from the class path. There are many options. A sampling:
```java
// Semi-readable implicit
String maybeShaded = NativeLibraryLoader.class.getName();
// Use ! instead of . to avoid shading utilities from modifying the string
String expected = \"io!netty!util!internal!NativeLibraryLoader\".replace('!', '.');
if (maybeShaded.endsWith(expected)) {
  return maybeShaded.substring(0, maybeShaded.length() - expected.size());
} else {
  throw ...;
}

// Quick and dirty implicit
String name = NativeLibraryLoader.class.getName();
// 42 == \"io.netty.util.internal.NativeLibraryLoader\".length()
return name.substring(0, name.length() - 42);

// Pre-configured (explicit). User created a file for Netty to read
String resourceName = obfuscateConcat(\"/META-INF/native/\", \"io.netty.packagePrefix\");
// Could also be a relative, but then users would need to create a file in
// io/netty/util/internal or similar subpackage (I don't see any class in
// io.netty package).
// String resourceName = \"packagePrefix\";
InputStream is = NativeLibraryLoader.class.getResourceAsStream(resourceName);
if (is == null) {
  return \"\";
}
return asStringUtf8(is);
```

In my mind, when shaded, a `\"\"` fallback is counter-productive. It gives the appearance that Netty has been properly shaded when in fact it still collides. As a user, I'd much rather epoll and tcnative obviously fail when improperly shaded. So for me an implicit solution has the _benefit_ of failing when misconfigured. But we can mix-and-match behaviors pretty easily.

Depending on the solution, supporting `io.netty.packagePrefix` may not be necessary any more. An explicit configuration by the user (via `io.netty.packagePrefix` or similar) may be a necessary supplement to implicit solutions since shading may not use prefixing; `io.netty` may have been rewritten as `a.b` or `com.example.shadednetty`.

I'd be willing to submit a PR for necessary changes, although I'd hope we could agree on an approach first.", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/6963","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/6963/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/6963/comments","events_url":"https://api.github.com/repos/netty/netty/issues/6963/events","html_url":"https://github.com/netty/netty/issues/6963","id":242197038,"node_id":"MDU6SXNzdWUyNDIxOTcwMzg=","number":6963,"title":"-Dio.netty.packagePrefix should not be required when shading","user":{"login":"ejona86","id":2811396,"node_id":"MDQ6VXNlcjI4MTEzOTY=","avatar_url":"https://avatars2.githubusercontent.com/u/2811396?v=4","gravatar_id":"","url":"https://api.github.com/users/ejona86","html_url":"https://github.com/ejona86","followers_url":"https://api.github.com/users/ejona86/followers","following_url":"https://api.github.com/users/ejona86/following{/other_user}","gists_url":"https://api.github.com/users/ejona86/gists{/gist_id}","starred_url":"https://api.github.com/users/ejona86/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ejona86/subscriptions","organizations_url":"https://api.github.com/users/ejona86/orgs","repos_url":"https://api.github.com/users/ejona86/repos","events_url":"https://api.github.com/users/ejona86/events{/privacy}","received_events_url":"https://api.github.com/users/ejona86/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2017-07-11T22:00:16Z","updated_at":"2017-07-20T01:38:20Z","closed_at":"2017-07-20T01:38:20Z","author_association":"MEMBER","body":"### Expected behavior\r\n\r\nA library may use shading transparently to the user and would be able to be combined with other libraries that also use Netty. Similarly, an application should be able to use shading transparently to the user.\r\n\r\n### Actual behavior\r\n\r\nThe shader has to choose whether to rename the native components of epoll and tcnative. If they are renamed, then `-Dio.netty.packagePrefix` must be set at runtime, which may not be possible programatically. If they aren't renamed, then epoll and tcnative can fail when mixed with other libraries because: 1) there is a name collision of the `META-INF/native/` files in the class path and 2) [`System.loadLibrary(String)` does nothing if it previously loaded a particular name](http://docs.oracle.com/javase/8/docs/api/java/lang/Runtime.html#loadLibrary-java.lang.String-).\r\n\r\n### Possible fixes\r\n\r\n`NativeLibraryLoader` [currently](https://github.com/netty/netty/blob/f208b147a6c1748092f1d23f71865e9082b00534/common/src/main/java/io/netty/util/internal/NativeLibraryLoader.java#L188) defaults to a package prefix of `\"\"`:\r\n```java\r\nString name = SystemPropertyUtil.get(\"io.netty.packagePrefix\", \"\").replace('.', '-') + originalName;\r\n```\r\n\r\nInstead of defaulting to `\"\"`, I think it should at least attempt to find a package prefix from the class path. There are many options. A sampling:\r\n```java\r\n// Semi-readable implicit\r\nString maybeShaded = NativeLibraryLoader.class.getName();\r\n// Use ! instead of . to avoid shading utilities from modifying the string\r\nString expected = \"io!netty!util!internal!NativeLibraryLoader\".replace('!', '.');\r\nif (maybeShaded.endsWith(expected)) {\r\n  return maybeShaded.substring(0, maybeShaded.length() - expected.size());\r\n} else {\r\n  throw ...;\r\n}\r\n\r\n// Quick and dirty implicit\r\nString name = NativeLibraryLoader.class.getName();\r\n// 42 == \"io.netty.util.internal.NativeLibraryLoader\".length()\r\nreturn name.substring(0, name.length() - 42);\r\n\r\n// Pre-configured (explicit). User created a file for Netty to read\r\nString resourceName = obfuscateConcat(\"/META-INF/native/\", \"io.netty.packagePrefix\");\r\n// Could also be a relative, but then users would need to create a file in\r\n// io/netty/util/internal or similar subpackage (I don't see any class in\r\n// io.netty package).\r\n// String resourceName = \"packagePrefix\";\r\nInputStream is = NativeLibraryLoader.class.getResourceAsStream(resourceName);\r\nif (is == null) {\r\n  return \"\";\r\n}\r\nreturn asStringUtf8(is);\r\n```\r\n\r\nIn my mind, when shaded, a `\"\"` fallback is counter-productive. It gives the appearance that Netty has been properly shaded when in fact it still collides. As a user, I'd much rather epoll and tcnative obviously fail when improperly shaded. So for me an implicit solution has the _benefit_ of failing when misconfigured. But we can mix-and-match behaviors pretty easily.\r\n\r\nDepending on the solution, supporting `io.netty.packagePrefix` may not be necessary any more. An explicit configuration by the user (via `io.netty.packagePrefix` or similar) may be a necessary supplement to implicit solutions since shading may not use prefixing; `io.netty` may have been rewritten as `a.b` or `com.example.shadednetty`.\r\n\r\nI'd be willing to submit a PR for necessary changes, although I'd hope we could agree on an approach first.","closed_by":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}}", "commentIds":["314612803","314827598","314831085","315697869","315825615","315838590","315840922","315864387"], "labels":[]}