{"id":"87", "title":"Build Issue with maven-antrun-plugin: \"Plugin execution not covered by lifecycle configuration\"", "body":"Just installed Eclipse Indigo Service Release 1

It comes with M2E - Maven Integration for Eclipse 1.1

When I load up the Netty project, I get the following error:

```
Plugin execution not covered by lifecycle configuration: org.apache.maven.plugins:maven-antrun-plugin:1.7:run (execution: write-version, phase: validate)   pom.xml /netty  line 320    Maven Project Build Lifecycle Mapping Problem
```

```
      <plugin>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>1.7</version>
        <executions>
          <execution>
            <id>write-version</id>
            <phase>validate</phase>
            <goals>
              <goal>run</goal>
            </goals>
         ...
        </execution>
      </plugin>
```

Seems like `maven-antrun-plugin` does not like the the `validate` phase.  

The error goes away if I set the phase to `package` - but I am not sure if this is what is required.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/87","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/87/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/87/comments","events_url":"https://api.github.com/repos/netty/netty/issues/87/events","html_url":"https://github.com/netty/netty/issues/87","id":2396364,"node_id":"MDU6SXNzdWUyMzk2MzY0","number":87,"title":"Build Issue with maven-antrun-plugin: \"Plugin execution not covered by lifecycle configuration\"","user":{"login":"veebs","id":367073,"node_id":"MDQ6VXNlcjM2NzA3Mw==","avatar_url":"https://avatars3.githubusercontent.com/u/367073?v=4","gravatar_id":"","url":"https://api.github.com/users/veebs","html_url":"https://github.com/veebs","followers_url":"https://api.github.com/users/veebs/followers","following_url":"https://api.github.com/users/veebs/following{/other_user}","gists_url":"https://api.github.com/users/veebs/gists{/gist_id}","starred_url":"https://api.github.com/users/veebs/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/veebs/subscriptions","organizations_url":"https://api.github.com/users/veebs/orgs","repos_url":"https://api.github.com/users/veebs/repos","events_url":"https://api.github.com/users/veebs/events{/privacy}","received_events_url":"https://api.github.com/users/veebs/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":8,"created_at":"2011-11-30T01:54:58Z","updated_at":"2016-03-11T17:34:30Z","closed_at":"2011-12-02T04:40:40Z","author_association":"MEMBER","body":"Just installed Eclipse Indigo Service Release 1\n\nIt comes with M2E - Maven Integration for Eclipse 1.1\n\nWhen I load up the Netty project, I get the following error:\n\n```\nPlugin execution not covered by lifecycle configuration: org.apache.maven.plugins:maven-antrun-plugin:1.7:run (execution: write-version, phase: validate)   pom.xml /netty  line 320    Maven Project Build Lifecycle Mapping Problem\n```\n\n```\n      <plugin>\n        <artifactId>maven-antrun-plugin</artifactId>\n        <version>1.7</version>\n        <executions>\n          <execution>\n            <id>write-version</id>\n            <phase>validate</phase>\n            <goals>\n              <goal>run</goal>\n            </goals>\n         ...\n        </execution>\n      </plugin>\n```\n\nSeems like `maven-antrun-plugin` does not like the the `validate` phase.  \n\nThe error goes away if I set the phase to `package` - but I am not sure if this is what is required.\n","closed_by":{"login":"veebs","id":367073,"node_id":"MDQ6VXNlcjM2NzA3Mw==","avatar_url":"https://avatars3.githubusercontent.com/u/367073?v=4","gravatar_id":"","url":"https://api.github.com/users/veebs","html_url":"https://github.com/veebs","followers_url":"https://api.github.com/users/veebs/followers","following_url":"https://api.github.com/users/veebs/following{/other_user}","gists_url":"https://api.github.com/users/veebs/gists{/gist_id}","starred_url":"https://api.github.com/users/veebs/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/veebs/subscriptions","organizations_url":"https://api.github.com/users/veebs/orgs","repos_url":"https://api.github.com/users/veebs/repos","events_url":"https://api.github.com/users/veebs/events{/privacy}","received_events_url":"https://api.github.com/users/veebs/received_events","type":"User","site_admin":false}}", "commentIds":["2954289","2957524","2957977","27167302","28875540","143700353","143700443","195468923"], "labels":[]}