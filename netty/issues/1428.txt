{"id":"1428", "title":"JVM crash", "body":"Hi,
I am using Java 1.7.0_21 and Netty 4.0.0.Final-SNAPSHOT build. I am running load of 100 concurrent clients on a 2 core server and 8GB (4 virtual cores) which gives me ~2000 requests per second.
I am getting repeated JVM crashes with core dumps (I put 'ulimit -c unlimited' line before my java command to allow this).
My Java process does not eat up a lot of memory, and the CPU also does not show any stress signs (often below 200%).

I cannot understand what is wrong.

Here is my HttpServerHandler code:

```
@Override
public void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {
    log.trace(\"Received new message: [{}]; handling in thread [{}]\", new Object[] {msg, Thread.currentThread().getName()});
    try {
        assert msg instanceof HttpObject;
        if (msg instanceof HttpRequest) {
            HttpRequest httpRequest = (HttpRequest) msg;
            log.debug(\"HttpRequest: \\n{}\", httpRequest.toString());
            if (is100ContinueExpected(httpRequest)) {
                send100Continue(ctx);
            }
            // for load test
            if (httpRequest.getUri().toLowerCase().startsWith(\"/noop\")) {
                FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, HttpResponseStatus.OK);
                ctx.nextOutboundMessageBuffer().add(response);
                ctx.flush().addListener(ChannelFutureListener.CLOSE);
            }
            else if (!isPathSupported(httpRequest)) {
                log.debug(\"Path is not supported, cannot handle request: {}\", httpRequest.getUri());
                FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
                log.debug(\"Sending response {}\", response);
                ctx.nextOutboundMessageBuffer().add(response);
                ctx.flush().addListener(ChannelFutureListener.CLOSE);
            }
            else {
                MyRequestData requestData = new MyRequestData();
                new HttpRequestHandler().handleRequest(requestData);
                writeResponse(ctx, httpRequest, requestData);
            }
        }
    } catch (Throwable ex) {   
        log.error(\"Error handling message\", ex);
        exceptionCaught(ctx, ex);
    } 

}

private void writeResponse(ChannelHandlerContext ctx, HttpRequest request, 
                            MyRequestData requestData) {
    // Build the response object.
    assert requestData != null;
    FullHttpResponse response = requestData.buildResponse(request.getDecoderResult().isSuccess());
    response.headers().set(CONTENT_TYPE, requestData.getContentType());

    // Decide whether to close the connection or not.
    boolean keepAlive = isKeepAlive(request);
    if (keepAlive) {
        // TODO adi check if these headers need to be changed in case of 'no-close' (need the connection to push resp later)
        // Add 'Content-Length' header only for a keep-alive connection.
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        // Add keep alive header as per:
        // - http://www.w3.org/Protocols/HTTP/1.1/draft-ietf-http-v11-spec-01.html#Connection
        response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
    }

    // Encode the cookie.
    String cookieString = request.headers().get(COOKIE);
    if (cookieString != null) {
        Set<Cookie> cookies = CookieDecoder.decode(cookieString);
        if (!cookies.isEmpty()) {
            // Reset the cookies if necessary.
            for (Cookie cookie: cookies) {
                response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(cookie));
            }
        }
    } else {
        // Browser sent no cookie. Add some.
        response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(\"key1\", \"value1\"));
        response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(\"key2\", \"value2\"));
    }

    log.trace(\"Sending response [{}]\", response);
    // Write the response.
    ctx.nextOutboundMessageBuffer().add(response);

    // Close the non-keep-alive connection after the write operation is done.
    if (!keepAlive || !requestData.isCloseAllowed()) {
        ctx.flush().addListener(ChannelFutureListener.CLOSE);
    }
}

private static void send100Continue(ChannelHandlerContext ctx) {
    FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, CONTINUE);
    log.debug(\"Sending response {}\", response);
    ctx.nextOutboundMessageBuffer().add(response);
}

@Override
public void endMessageReceived(ChannelHandlerContext ctx) throws Exception {
    ctx.flush();
}

@Override
public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    log.error(cause.getMessage(), cause);
    HttpResponseStatus status = INTERNAL_SERVER_ERROR;
    if (cause instanceof MoonShineException) {
        status = ((MoonShineException) cause).getStatusCode();
    }
    HttpResponse httpResponse = new DefaultHttpResponse(HTTP_1_1, status);
    httpResponse.headers().set(CONTENT_TYPE, \"text/plain; charset=UTF-8\");
    httpResponse.headers().set(CONNECTION, HttpHeaders.Values.CLOSE);
    ctx.nextOutboundMessageBuffer().add(httpResponse);
    ctx.flush().addListener(ChannelFutureListener.CLOSE);
    ctx.close();
}    
```

Here is the prefix of my hs_err file created by the jvm:
# 
# A fatal error has been detected by the Java Runtime Environment:
# 
# SIGSEGV (0xb) at pc=0x00007f1ee901d37c, pid=2231, tid=139770478348032
# 
# JRE version: 7.0_21-b11
# Java VM: Java HotSpot(TM) 64-Bit Server VM (23.21-b01 mixed mode linux-amd64 compressed oops)
# Problematic frame:
# j  io.netty.channel.DefaultChannelHandlerContext.nextOutboundMessageBuffer()Lio/netty/buffer/MessageBuf;+21
# 
# Core dump written. Default location: /home/user/core or core.2231
# 
# If you would like to submit a bug report, please visit:
# http://bugreport.sun.com/bugreport/crash.jsp
# 

---------------  T H R E A D  ---------------

Current thread (0x00007f1eb4003800):  JavaThread \"nioEventLoopGroup-2-1\" [_thread_in_Java, id=2338, stack(0x00007f1ed9a64000,0x00007f1ed9b65000)]

siginfo:si_signo=SIGSEGV: si_errno=0, si_code=1 (SEGV_MAPERR), si_addr=0x0000000040000008

Registers:
RAX=0x00000000837f2840, RBX=0x0000000000000002, RCX=0x0000000040000000, RDX=0x0000000000800002
RSP=0x00007f1ed9b632e0, RBP=0x00007f1ed9b63338, RSI=0x00007f1ed9b632a0, RDI=0x00007f1eb800a2c0
R8 =0x00007f1ef274d220, R9 =0x0000000000000922, R10=0x00007f1ef273b4c0, R11=0x00007f1ef1d6c240
R12=0x0000000000000000, R13=0x00000000838fea35, R14=0x00007f1ed9b63358, R15=0x00007f1eb4003800
RIP=0x00007f1ee901d37c, EFLAGS=0x0000000000010246, CSGSFS=0x0000000000000033, ERR=0x0000000000000004
  TRAPNO=0x000000000000000e

Top of Stack: (sp=0x00007f1ed9b632e0)
0x00007f1ed9b632e0:   00007f1ee9006afe 0000000087ebab78
0x00007f1ed9b632f0:   0000000040000000 00007f1ed9b632f8
0x00007f1ed9b63300:   00000000838fea35 00007f1ed9b63358
0x00007f1ed9b63310:   000000008390a630 0000000083e59b58
0x00007f1ed9b63320:   00000000838feaa0 0000000000000000
0x00007f1ed9b63330:   00007f1ed9b63360 00000000fc38f690
0x00007f1ed9b63340:   00007f1ee968a720 0000000087ebab78
0x00007f1ed9b63350:   00000000fc38f628 00000000fc38f690
0x00007f1ed9b63360:   0000000000000000 00000000d7fc9500
0x00007f1ed9b63370:   00000000d7fc9a98 00000000fc38f690
0x00007f1ed9b63380:   00000000fc3aeb60 00000000d7fc9980
0x00007f1ed9b63390:   00000000d7fc8e80 00000000fc3aeb60
0x00007f1ed9b633a0:   0000000000000000 00000000d7fbb190
0x00007f1ed9b633b0:   00000000d7fba3e0 00000000d7fbb290
0x00007f1ed9b633c0:   00000000d7fbb220 ed59514d00000000
0x00007f1ed9b633d0:   00000000d7fc56a8 00000000d7fc8130
0x00007f1ed9b633e0:   000000000000000d 00000000d7fc56a8
0x00007f1ed9b633f0:   00000013ed595153 00000000880c4738
0x00007f1ed9b63400:   00000000d7fc56d8 0000000087f30f80
0x00007f1ed9b63410:   0000000087ee2380 0000000000000007
0x00007f1ed9b63420:   0000000000000001 000000000007553b
0x00007f1ed9b63430:   00000000d7fba3e0 00007f1ee9405ee8
0x00007f1ed9b63440:   0000000000000000 00000000dcd9c2d8
0x00007f1ed9b63450:   0000000087f59338 00000000fc38f828
0x00007f1ed9b63460:   00000000fc38f7c0 0000000900000000
0x00007f1ed9b63470:   00000000dcd9c230 00000000fc38f810
0x00007f1ed9b63480:   00000000fc38f690 00000000fc3aeb40
0x00007f1ed9b63490:   00000000fc3aeb60 00000000dcd9c270
0x00007f1ed9b634a0:   00000000d7fba3e0 00000000fc3aeb80
0x00007f1ed9b634b0:   00000000fc3aeb90 00000000fc3aeba0
0x00007f1ed9b634c0:   00000000dcd9b6c0 00000000dcd9c050
0x00007f1ed9b634d0:   00000000000009c4 00000000dcd9c270 

Instructions: (pc=0x00007f1ee901d37c)
0x00007f1ee901d35c:   e0 48 8b 9c d8 18 02 00 00 48 8b 53 60 4c 8d 6c
0x00007f1ee901d36c:   24 08 4c 89 6d f0 ff a3 80 00 00 00 4c 8b 75 d0
0x00007f1ee901d37c:   8b 51 08 4c 8b 6d e0 4d 85 ed 0f 84 97 00 00 00
0x00007f1ee901d38c:   4d 8b 75 10 4c 3b f2 0f 85 0f 00 00 00 49 83 45 

Register to memory mapping:

RAX=0x00000000837f2840 is an oop
{instance class} 
- klass: {other class}
  RBX=0x0000000000000002 is an unknown value
  RCX=0x0000000040000000 is an unknown value
  RDX=0x0000000000800002 is an unknown value
  RSP=0x00007f1ed9b632e0 is pointing into the stack for thread: 0x00007f1eb4003800
  RBP=0x00007f1ed9b63338 is pointing into the stack for thread: 0x00007f1eb4003800
  RSI=0x00007f1ed9b632a0 is pointing into the stack for thread: 0x00007f1eb4003800
  RDI=0x00007f1eb800a2c0 is an unknown value
  R8 =0x00007f1ef274d220: <offset 0xd10220> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000
  R9 =0x0000000000000922 is an unknown value
  R10=0x00007f1ef273b4c0: <offset 0xcfe4c0> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000
  R11=0x00007f1ef1d6c240: <offset 0x32f240> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000
  R12=0x0000000000000000 is an unknown value
  R13=0x00000000838fea35 is an oop
  {constMethod} 
- klass: {other class}
- method:       0x00000000838feaa0 {method} 'nextOutboundMessageBuffer' '()Lio/netty/buffer/MessageBuf;' in 'io/netty/channel/DefaultChannelHandlerContext'
- exceptions:   0x0000000082c01d50
- stackmap data:       [B
  bci_from(0x838fea35) = 21; print_codes():
  R14=0x00007f1ed9b63358 is pointing into the stack for thread: 0x00007f1eb4003800
  R15=0x00007f1eb4003800 is a thread

Stack: [0x00007f1ed9a64000,0x00007f1ed9b65000],  sp=0x00007f1ed9b632e0,  free space=1020k
Native frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)
j  io.netty.channel.DefaultChannelHandlerContext.nextOutboundMessageBuffer()Lio/netty/buffer/MessageBuf;+21

---------------  P R O C E S S  ---------------

Java Threads: ( => current thread )
  0x00007f1eb405f000 JavaThread \"pool-2-thread-1\" [_thread_blocked, id=2342, stack(0x00007f1ed855e000,0x00007f1ed865f000)]
  0x00007f1eb4006800 JavaThread \"nioEventLoopGroup-2-4\" [_thread_in_vm, id=2341, stack(0x00007f1ed865f000,0x00007f1ed8760000)]
  0x00007f1eb4005800 JavaThread \"nioEventLoopGroup-2-3\" [_thread_in_Java, id=2340, stack(0x00007f1ed8760000,0x00007f1ed8861000)]
  0x00007f1eb4004800 JavaThread \"nioEventLoopGroup-2-2\" [_thread_in_Java, id=2339, stack(0x00007f1eda3c6000,0x00007f1eda4c7000)]
=>0x00007f1eb4003800 JavaThread \"nioEventLoopGroup-2-1\" [_thread_in_Java, id=2338, stack(0x00007f1ed9a64000,0x00007f1ed9b65000)]
  0x00007f1eec560000 JavaThread \"nioEventLoopGroup-1-1\" [_thread_in_native, id=2272, stack(0x00007f1ed9862000,0x00007f1ed9963000)]
  0x00007f1ea000f000 JavaThread \"New I/O  worker #1\" [_thread_in_native, id=2251, stack(0x00007f1ed9963000,0x00007f1ed9a64000)]
  0x00007f1eac05f800 JavaThread \"I/O dispatcher 2\" [_thread_in_native, id=2249, stack(0x00007f1ed9b65000,0x00007f1ed9c66000)]
  0x00007f1eac062800 JavaThread \"I/O dispatcher 1\" [_thread_in_native, id=2248, stack(0x00007f1ed9c66000,0x00007f1ed9d67000)]
  0x00007f1eec40d800 JavaThread \"Couchbase View Thread for node localhost/127.0.0.1:8092\" [_thread_in_native, id=2247, stack(0x00007f1eda1c4000,0x00007f1eda2c5000)]
  0x00007f1eec361800 JavaThread \"Memcached IO over {MemcachedConnection to localhost/127.0.0.1:11210}\" [_thread_in_native, id=2245, stack(0x00007f1eda2c5000,0x00007f1eda3c6000)]
  0x00007f1eec173000 JavaThread \"Service Thread\" daemon [_thread_blocked, id=2241, stack(0x00007f1edaacb000,0x00007f1edabcc000)]
  0x00007f1eec170800 JavaThread \"C2 CompilerThread1\" daemon [_thread_blocked, id=2240, stack(0x00007f1edabcc000,0x00007f1edaccd000)]
  0x00007f1eec16e000 JavaThread \"C2 CompilerThread0\" daemon [_thread_blocked, id=2239, stack(0x00007f1edaccd000,0x00007f1edadce000)]
  0x00007f1eec16b800 JavaThread \"Signal Dispatcher\" daemon [_thread_blocked, id=2238, stack(0x00007f1edadce000,0x00007f1edaecf000)]
  0x00007f1eec11e800 JavaThread \"Finalizer\" daemon [_thread_blocked, id=2237, stack(0x00007f1ee005f000,0x00007f1ee0160000)]
  0x00007f1eec11c800 JavaThread \"Reference Handler\" daemon [_thread_blocked, id=2236, stack(0x00007f1ee80c6000,0x00007f1ee81c7000)]
  0x00007f1eec008800 JavaThread \"main\" [_thread_blocked, id=2232, stack(0x00007f1ef3270000,0x00007f1ef3371000)]

Other Threads:
  0x00007f1eec115000 VMThread [stack: 0x00007f1ee81c7000,0x00007f1ee82c8000] [id=2235]
  0x00007f1eec17d800 WatcherThread [stack: 0x00007f1eda9ca000,0x00007f1edaacb000] [id=2242]

VM state:not at safepoint (normal execution)

VM Mutex/Monitor currently owned by a thread: None
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/1428","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/1428/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/1428/comments","events_url":"https://api.github.com/repos/netty/netty/issues/1428/events","html_url":"https://github.com/netty/netty/issues/1428","id":15227789,"node_id":"MDU6SXNzdWUxNTIyNzc4OQ==","number":1428,"title":"JVM crash","user":{"login":"adilavismg","id":4310754,"node_id":"MDQ6VXNlcjQzMTA3NTQ=","avatar_url":"https://avatars0.githubusercontent.com/u/4310754?v=4","gravatar_id":"","url":"https://api.github.com/users/adilavismg","html_url":"https://github.com/adilavismg","followers_url":"https://api.github.com/users/adilavismg/followers","following_url":"https://api.github.com/users/adilavismg/following{/other_user}","gists_url":"https://api.github.com/users/adilavismg/gists{/gist_id}","starred_url":"https://api.github.com/users/adilavismg/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/adilavismg/subscriptions","organizations_url":"https://api.github.com/users/adilavismg/orgs","repos_url":"https://api.github.com/users/adilavismg/repos","events_url":"https://api.github.com/users/adilavismg/events{/privacy}","received_events_url":"https://api.github.com/users/adilavismg/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2013-06-06T15:07:49Z","updated_at":"2013-06-23T11:43:45Z","closed_at":"2013-06-10T07:28:45Z","author_association":"NONE","body":"Hi,\nI am using Java 1.7.0_21 and Netty 4.0.0.Final-SNAPSHOT build. I am running load of 100 concurrent clients on a 2 core server and 8GB (4 virtual cores) which gives me ~2000 requests per second.\nI am getting repeated JVM crashes with core dumps (I put 'ulimit -c unlimited' line before my java command to allow this).\nMy Java process does not eat up a lot of memory, and the CPU also does not show any stress signs (often below 200%).\n\nI cannot understand what is wrong.\n\nHere is my HttpServerHandler code:\n\n```\n@Override\npublic void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {\n    log.trace(\"Received new message: [{}]; handling in thread [{}]\", new Object[] {msg, Thread.currentThread().getName()});\n    try {\n        assert msg instanceof HttpObject;\n        if (msg instanceof HttpRequest) {\n            HttpRequest httpRequest = (HttpRequest) msg;\n            log.debug(\"HttpRequest: \\n{}\", httpRequest.toString());\n            if (is100ContinueExpected(httpRequest)) {\n                send100Continue(ctx);\n            }\n            // for load test\n            if (httpRequest.getUri().toLowerCase().startsWith(\"/noop\")) {\n                FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, HttpResponseStatus.OK);\n                ctx.nextOutboundMessageBuffer().add(response);\n                ctx.flush().addListener(ChannelFutureListener.CLOSE);\n            }\n            else if (!isPathSupported(httpRequest)) {\n                log.debug(\"Path is not supported, cannot handle request: {}\", httpRequest.getUri());\n                FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, HttpResponseStatus.BAD_REQUEST);\n                log.debug(\"Sending response {}\", response);\n                ctx.nextOutboundMessageBuffer().add(response);\n                ctx.flush().addListener(ChannelFutureListener.CLOSE);\n            }\n            else {\n                MyRequestData requestData = new MyRequestData();\n                new HttpRequestHandler().handleRequest(requestData);\n                writeResponse(ctx, httpRequest, requestData);\n            }\n        }\n    } catch (Throwable ex) {   \n        log.error(\"Error handling message\", ex);\n        exceptionCaught(ctx, ex);\n    } \n\n}\n\nprivate void writeResponse(ChannelHandlerContext ctx, HttpRequest request, \n                            MyRequestData requestData) {\n    // Build the response object.\n    assert requestData != null;\n    FullHttpResponse response = requestData.buildResponse(request.getDecoderResult().isSuccess());\n    response.headers().set(CONTENT_TYPE, requestData.getContentType());\n\n    // Decide whether to close the connection or not.\n    boolean keepAlive = isKeepAlive(request);\n    if (keepAlive) {\n        // TODO adi check if these headers need to be changed in case of 'no-close' (need the connection to push resp later)\n        // Add 'Content-Length' header only for a keep-alive connection.\n        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());\n        // Add keep alive header as per:\n        // - http://www.w3.org/Protocols/HTTP/1.1/draft-ietf-http-v11-spec-01.html#Connection\n        response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);\n    }\n\n    // Encode the cookie.\n    String cookieString = request.headers().get(COOKIE);\n    if (cookieString != null) {\n        Set<Cookie> cookies = CookieDecoder.decode(cookieString);\n        if (!cookies.isEmpty()) {\n            // Reset the cookies if necessary.\n            for (Cookie cookie: cookies) {\n                response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(cookie));\n            }\n        }\n    } else {\n        // Browser sent no cookie. Add some.\n        response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(\"key1\", \"value1\"));\n        response.headers().add(SET_COOKIE, ServerCookieEncoder.encode(\"key2\", \"value2\"));\n    }\n\n    log.trace(\"Sending response [{}]\", response);\n    // Write the response.\n    ctx.nextOutboundMessageBuffer().add(response);\n\n    // Close the non-keep-alive connection after the write operation is done.\n    if (!keepAlive || !requestData.isCloseAllowed()) {\n        ctx.flush().addListener(ChannelFutureListener.CLOSE);\n    }\n}\n\nprivate static void send100Continue(ChannelHandlerContext ctx) {\n    FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, CONTINUE);\n    log.debug(\"Sending response {}\", response);\n    ctx.nextOutboundMessageBuffer().add(response);\n}\n\n@Override\npublic void endMessageReceived(ChannelHandlerContext ctx) throws Exception {\n    ctx.flush();\n}\n\n@Override\npublic void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {\n    log.error(cause.getMessage(), cause);\n    HttpResponseStatus status = INTERNAL_SERVER_ERROR;\n    if (cause instanceof MoonShineException) {\n        status = ((MoonShineException) cause).getStatusCode();\n    }\n    HttpResponse httpResponse = new DefaultHttpResponse(HTTP_1_1, status);\n    httpResponse.headers().set(CONTENT_TYPE, \"text/plain; charset=UTF-8\");\n    httpResponse.headers().set(CONNECTION, HttpHeaders.Values.CLOSE);\n    ctx.nextOutboundMessageBuffer().add(httpResponse);\n    ctx.flush().addListener(ChannelFutureListener.CLOSE);\n    ctx.close();\n}    \n```\n\nHere is the prefix of my hs_err file created by the jvm:\n# \n# A fatal error has been detected by the Java Runtime Environment:\n# \n# SIGSEGV (0xb) at pc=0x00007f1ee901d37c, pid=2231, tid=139770478348032\n# \n# JRE version: 7.0_21-b11\n# Java VM: Java HotSpot(TM) 64-Bit Server VM (23.21-b01 mixed mode linux-amd64 compressed oops)\n# Problematic frame:\n# j  io.netty.channel.DefaultChannelHandlerContext.nextOutboundMessageBuffer()Lio/netty/buffer/MessageBuf;+21\n# \n# Core dump written. Default location: /home/user/core or core.2231\n# \n# If you would like to submit a bug report, please visit:\n# http://bugreport.sun.com/bugreport/crash.jsp\n# \n\n---------------  T H R E A D  ---------------\n\nCurrent thread (0x00007f1eb4003800):  JavaThread \"nioEventLoopGroup-2-1\" [_thread_in_Java, id=2338, stack(0x00007f1ed9a64000,0x00007f1ed9b65000)]\n\nsiginfo:si_signo=SIGSEGV: si_errno=0, si_code=1 (SEGV_MAPERR), si_addr=0x0000000040000008\n\nRegisters:\nRAX=0x00000000837f2840, RBX=0x0000000000000002, RCX=0x0000000040000000, RDX=0x0000000000800002\nRSP=0x00007f1ed9b632e0, RBP=0x00007f1ed9b63338, RSI=0x00007f1ed9b632a0, RDI=0x00007f1eb800a2c0\nR8 =0x00007f1ef274d220, R9 =0x0000000000000922, R10=0x00007f1ef273b4c0, R11=0x00007f1ef1d6c240\nR12=0x0000000000000000, R13=0x00000000838fea35, R14=0x00007f1ed9b63358, R15=0x00007f1eb4003800\nRIP=0x00007f1ee901d37c, EFLAGS=0x0000000000010246, CSGSFS=0x0000000000000033, ERR=0x0000000000000004\n  TRAPNO=0x000000000000000e\n\nTop of Stack: (sp=0x00007f1ed9b632e0)\n0x00007f1ed9b632e0:   00007f1ee9006afe 0000000087ebab78\n0x00007f1ed9b632f0:   0000000040000000 00007f1ed9b632f8\n0x00007f1ed9b63300:   00000000838fea35 00007f1ed9b63358\n0x00007f1ed9b63310:   000000008390a630 0000000083e59b58\n0x00007f1ed9b63320:   00000000838feaa0 0000000000000000\n0x00007f1ed9b63330:   00007f1ed9b63360 00000000fc38f690\n0x00007f1ed9b63340:   00007f1ee968a720 0000000087ebab78\n0x00007f1ed9b63350:   00000000fc38f628 00000000fc38f690\n0x00007f1ed9b63360:   0000000000000000 00000000d7fc9500\n0x00007f1ed9b63370:   00000000d7fc9a98 00000000fc38f690\n0x00007f1ed9b63380:   00000000fc3aeb60 00000000d7fc9980\n0x00007f1ed9b63390:   00000000d7fc8e80 00000000fc3aeb60\n0x00007f1ed9b633a0:   0000000000000000 00000000d7fbb190\n0x00007f1ed9b633b0:   00000000d7fba3e0 00000000d7fbb290\n0x00007f1ed9b633c0:   00000000d7fbb220 ed59514d00000000\n0x00007f1ed9b633d0:   00000000d7fc56a8 00000000d7fc8130\n0x00007f1ed9b633e0:   000000000000000d 00000000d7fc56a8\n0x00007f1ed9b633f0:   00000013ed595153 00000000880c4738\n0x00007f1ed9b63400:   00000000d7fc56d8 0000000087f30f80\n0x00007f1ed9b63410:   0000000087ee2380 0000000000000007\n0x00007f1ed9b63420:   0000000000000001 000000000007553b\n0x00007f1ed9b63430:   00000000d7fba3e0 00007f1ee9405ee8\n0x00007f1ed9b63440:   0000000000000000 00000000dcd9c2d8\n0x00007f1ed9b63450:   0000000087f59338 00000000fc38f828\n0x00007f1ed9b63460:   00000000fc38f7c0 0000000900000000\n0x00007f1ed9b63470:   00000000dcd9c230 00000000fc38f810\n0x00007f1ed9b63480:   00000000fc38f690 00000000fc3aeb40\n0x00007f1ed9b63490:   00000000fc3aeb60 00000000dcd9c270\n0x00007f1ed9b634a0:   00000000d7fba3e0 00000000fc3aeb80\n0x00007f1ed9b634b0:   00000000fc3aeb90 00000000fc3aeba0\n0x00007f1ed9b634c0:   00000000dcd9b6c0 00000000dcd9c050\n0x00007f1ed9b634d0:   00000000000009c4 00000000dcd9c270 \n\nInstructions: (pc=0x00007f1ee901d37c)\n0x00007f1ee901d35c:   e0 48 8b 9c d8 18 02 00 00 48 8b 53 60 4c 8d 6c\n0x00007f1ee901d36c:   24 08 4c 89 6d f0 ff a3 80 00 00 00 4c 8b 75 d0\n0x00007f1ee901d37c:   8b 51 08 4c 8b 6d e0 4d 85 ed 0f 84 97 00 00 00\n0x00007f1ee901d38c:   4d 8b 75 10 4c 3b f2 0f 85 0f 00 00 00 49 83 45 \n\nRegister to memory mapping:\n\nRAX=0x00000000837f2840 is an oop\n{instance class} \n- klass: {other class}\n  RBX=0x0000000000000002 is an unknown value\n  RCX=0x0000000040000000 is an unknown value\n  RDX=0x0000000000800002 is an unknown value\n  RSP=0x00007f1ed9b632e0 is pointing into the stack for thread: 0x00007f1eb4003800\n  RBP=0x00007f1ed9b63338 is pointing into the stack for thread: 0x00007f1eb4003800\n  RSI=0x00007f1ed9b632a0 is pointing into the stack for thread: 0x00007f1eb4003800\n  RDI=0x00007f1eb800a2c0 is an unknown value\n  R8 =0x00007f1ef274d220: <offset 0xd10220> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000\n  R9 =0x0000000000000922 is an unknown value\n  R10=0x00007f1ef273b4c0: <offset 0xcfe4c0> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000\n  R11=0x00007f1ef1d6c240: <offset 0x32f240> in /usr/lib/jvm/java-7-oracle/jre/lib/amd64/server/libjvm.so at 0x00007f1ef1a3d000\n  R12=0x0000000000000000 is an unknown value\n  R13=0x00000000838fea35 is an oop\n  {constMethod} \n- klass: {other class}\n- method:       0x00000000838feaa0 {method} 'nextOutboundMessageBuffer' '()Lio/netty/buffer/MessageBuf;' in 'io/netty/channel/DefaultChannelHandlerContext'\n- exceptions:   0x0000000082c01d50\n- stackmap data:       [B\n  bci_from(0x838fea35) = 21; print_codes():\n  R14=0x00007f1ed9b63358 is pointing into the stack for thread: 0x00007f1eb4003800\n  R15=0x00007f1eb4003800 is a thread\n\nStack: [0x00007f1ed9a64000,0x00007f1ed9b65000],  sp=0x00007f1ed9b632e0,  free space=1020k\nNative frames: (J=compiled Java code, j=interpreted, Vv=VM code, C=native code)\nj  io.netty.channel.DefaultChannelHandlerContext.nextOutboundMessageBuffer()Lio/netty/buffer/MessageBuf;+21\n\n---------------  P R O C E S S  ---------------\n\nJava Threads: ( => current thread )\n  0x00007f1eb405f000 JavaThread \"pool-2-thread-1\" [_thread_blocked, id=2342, stack(0x00007f1ed855e000,0x00007f1ed865f000)]\n  0x00007f1eb4006800 JavaThread \"nioEventLoopGroup-2-4\" [_thread_in_vm, id=2341, stack(0x00007f1ed865f000,0x00007f1ed8760000)]\n  0x00007f1eb4005800 JavaThread \"nioEventLoopGroup-2-3\" [_thread_in_Java, id=2340, stack(0x00007f1ed8760000,0x00007f1ed8861000)]\n  0x00007f1eb4004800 JavaThread \"nioEventLoopGroup-2-2\" [_thread_in_Java, id=2339, stack(0x00007f1eda3c6000,0x00007f1eda4c7000)]\n=>0x00007f1eb4003800 JavaThread \"nioEventLoopGroup-2-1\" [_thread_in_Java, id=2338, stack(0x00007f1ed9a64000,0x00007f1ed9b65000)]\n  0x00007f1eec560000 JavaThread \"nioEventLoopGroup-1-1\" [_thread_in_native, id=2272, stack(0x00007f1ed9862000,0x00007f1ed9963000)]\n  0x00007f1ea000f000 JavaThread \"New I/O  worker #1\" [_thread_in_native, id=2251, stack(0x00007f1ed9963000,0x00007f1ed9a64000)]\n  0x00007f1eac05f800 JavaThread \"I/O dispatcher 2\" [_thread_in_native, id=2249, stack(0x00007f1ed9b65000,0x00007f1ed9c66000)]\n  0x00007f1eac062800 JavaThread \"I/O dispatcher 1\" [_thread_in_native, id=2248, stack(0x00007f1ed9c66000,0x00007f1ed9d67000)]\n  0x00007f1eec40d800 JavaThread \"Couchbase View Thread for node localhost/127.0.0.1:8092\" [_thread_in_native, id=2247, stack(0x00007f1eda1c4000,0x00007f1eda2c5000)]\n  0x00007f1eec361800 JavaThread \"Memcached IO over {MemcachedConnection to localhost/127.0.0.1:11210}\" [_thread_in_native, id=2245, stack(0x00007f1eda2c5000,0x00007f1eda3c6000)]\n  0x00007f1eec173000 JavaThread \"Service Thread\" daemon [_thread_blocked, id=2241, stack(0x00007f1edaacb000,0x00007f1edabcc000)]\n  0x00007f1eec170800 JavaThread \"C2 CompilerThread1\" daemon [_thread_blocked, id=2240, stack(0x00007f1edabcc000,0x00007f1edaccd000)]\n  0x00007f1eec16e000 JavaThread \"C2 CompilerThread0\" daemon [_thread_blocked, id=2239, stack(0x00007f1edaccd000,0x00007f1edadce000)]\n  0x00007f1eec16b800 JavaThread \"Signal Dispatcher\" daemon [_thread_blocked, id=2238, stack(0x00007f1edadce000,0x00007f1edaecf000)]\n  0x00007f1eec11e800 JavaThread \"Finalizer\" daemon [_thread_blocked, id=2237, stack(0x00007f1ee005f000,0x00007f1ee0160000)]\n  0x00007f1eec11c800 JavaThread \"Reference Handler\" daemon [_thread_blocked, id=2236, stack(0x00007f1ee80c6000,0x00007f1ee81c7000)]\n  0x00007f1eec008800 JavaThread \"main\" [_thread_blocked, id=2232, stack(0x00007f1ef3270000,0x00007f1ef3371000)]\n\nOther Threads:\n  0x00007f1eec115000 VMThread [stack: 0x00007f1ee81c7000,0x00007f1ee82c8000] [id=2235]\n  0x00007f1eec17d800 WatcherThread [stack: 0x00007f1eda9ca000,0x00007f1edaacb000] [id=2242]\n\nVM state:not at safepoint (normal execution)\n\nVM Mutex/Monitor currently owned by a thread: None\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["19184028","19872671"], "labels":["defect"]}