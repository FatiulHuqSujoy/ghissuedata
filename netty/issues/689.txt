{"id":"689", "title":"Netty 4.0 & Applet", "body":"Applet freezes when I use Netty 4.0. If I use the Netty 3, the Applet starts correctly. My code is very simple, and I can not understand where the mistake.

public class Applet extends JApplet
{
        @Override
    public void init()
    {  
            try {
                new DiscardServer(1020).run();
            } catch (Exception ex) {
                Logger.getLogger(Applet.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}

public class DiscardServer {
    private final int port;
    public DiscardServer(int port) {
        this.port = port;
    }
    public void run() throws Exception {
        ServerBootstrap b = new ServerBootstrap();
        try {
            b.group(new NioEventLoopGroup(), new NioEventLoopGroup()).channel(NioServerSocketChannel.class).localAddress(port)
             .childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new DiscardServerHandler());
                }
             });
            ChannelFuture f = b.bind().sync();
            f.channel().closeFuture().sync();
        } finally {
            b.shutdown();
        }
    }
}

public class DiscardServerHandler extends ChannelInboundByteHandlerAdapter {
    private static final Logger logger = Logger.getLogger(DiscardServerHandler.class.getName());
    @Override
    public void inboundBufferUpdated(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        System.out.println(\"Message \" + new String(in.array()));
        in.clear();
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.log(Level.WARNING,\"Unexpected exception from downstream.\",cause);
        ctx.close();
    }
}

Last post in the console:
network: Cache entry not found [url: file:/D:/java/Applet/dist/lib/netty-4.0.0.Alpha6-20121021.180934-2.jar, version: null]
basic: Plugin2ClassLoader.getPermissions CeilingPolicy allPerms

If I remove the line:
ChannelFuture f = b.bind().sync();
, the applet starts, but the DiscardServer doesn't not work.

http://stackoverflow.com/questions/13098940/netty-4-0-applet
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/689","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/689/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/689/comments","events_url":"https://api.github.com/repos/netty/netty/issues/689/events","html_url":"https://github.com/netty/netty/issues/689","id":7917890,"node_id":"MDU6SXNzdWU3OTE3ODkw","number":689,"title":"Netty 4.0 & Applet","user":{"login":"kadet89","id":2650385,"node_id":"MDQ6VXNlcjI2NTAzODU=","avatar_url":"https://avatars2.githubusercontent.com/u/2650385?v=4","gravatar_id":"","url":"https://api.github.com/users/kadet89","html_url":"https://github.com/kadet89","followers_url":"https://api.github.com/users/kadet89/followers","following_url":"https://api.github.com/users/kadet89/following{/other_user}","gists_url":"https://api.github.com/users/kadet89/gists{/gist_id}","starred_url":"https://api.github.com/users/kadet89/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kadet89/subscriptions","organizations_url":"https://api.github.com/users/kadet89/orgs","repos_url":"https://api.github.com/users/kadet89/repos","events_url":"https://api.github.com/users/kadet89/events{/privacy}","received_events_url":"https://api.github.com/users/kadet89/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/38","html_url":"https://github.com/netty/netty/milestone/38","labels_url":"https://api.github.com/repos/netty/netty/milestones/38/labels","id":223478,"node_id":"MDk6TWlsZXN0b25lMjIzNDc4","number":38,"title":"4.0.0.Beta1","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":150,"state":"closed","created_at":"2012-12-04T08:11:26Z","updated_at":"2013-09-05T14:18:37Z","due_on":"2013-02-14T08:00:00Z","closed_at":"2013-04-13T09:29:12Z"},"comments":7,"created_at":"2012-10-28T08:55:03Z","updated_at":"2013-01-08T09:41:24Z","closed_at":"2013-01-08T09:41:16Z","author_association":"NONE","body":"Applet freezes when I use Netty 4.0. If I use the Netty 3, the Applet starts correctly. My code is very simple, and I can not understand where the mistake.\n\npublic class Applet extends JApplet\n{\n        @Override\n    public void init()\n    {  \n            try {\n                new DiscardServer(1020).run();\n            } catch (Exception ex) {\n                Logger.getLogger(Applet.class.getName()).log(Level.SEVERE, null, ex);\n            }\n    }\n}\n\npublic class DiscardServer {\n    private final int port;\n    public DiscardServer(int port) {\n        this.port = port;\n    }\n    public void run() throws Exception {\n        ServerBootstrap b = new ServerBootstrap();\n        try {\n            b.group(new NioEventLoopGroup(), new NioEventLoopGroup()).channel(NioServerSocketChannel.class).localAddress(port)\n             .childHandler(new ChannelInitializer<SocketChannel>() {\n                @Override\n                public void initChannel(SocketChannel ch) throws Exception {\n                    ch.pipeline().addLast(new DiscardServerHandler());\n                }\n             });\n            ChannelFuture f = b.bind().sync();\n            f.channel().closeFuture().sync();\n        } finally {\n            b.shutdown();\n        }\n    }\n}\n\npublic class DiscardServerHandler extends ChannelInboundByteHandlerAdapter {\n    private static final Logger logger = Logger.getLogger(DiscardServerHandler.class.getName());\n    @Override\n    public void inboundBufferUpdated(ChannelHandlerContext ctx, ByteBuf in) throws Exception {\n        System.out.println(\"Message \" + new String(in.array()));\n        in.clear();\n    }\n    @Override\n    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {\n        logger.log(Level.WARNING,\"Unexpected exception from downstream.\",cause);\n        ctx.close();\n    }\n}\n\nLast post in the console:\nnetwork: Cache entry not found [url: file:/D:/java/Applet/dist/lib/netty-4.0.0.Alpha6-20121021.180934-2.jar, version: null]\nbasic: Plugin2ClassLoader.getPermissions CeilingPolicy allPerms\n\nIf I remove the line:\nChannelFuture f = b.bind().sync();\n, the applet starts, but the DiscardServer doesn't not work.\n\nhttp://stackoverflow.com/questions/13098940/netty-4-0-applet\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["10037473","10037587","10057922","10154538","11171446","11561322","11990503"], "labels":["defect"]}