{"id":"3562", "title":"What's going on when I do a outbound pass a ByteBuf object", "body":"```
Apr 01, 2015 10:37:29 PM io.netty.util.ResourceLeakDetector reportLeak
SEVERE: LEAK: ByteBuf.release() was not called before it's garbage-collected. See http://netty.io/wiki/reference-counted-objects.html for more information.
Recent access records: 0
```

I'd extends DefaultFullHttpRequest and the calling code NEW a ByteBuf object from a string then call `channel.writeAndFlush(it)` such like:

```
        final ByteBuf buf = PooledByteBufAllocator.DEFAULT
                .directBuffer()              
                .alloc()
                .buffer(512)
                .writeBytes(data.getBytes(CharsetUtil.UTF_8));
        final FullHttpRequest request = new DefaultFullHttpRequest(
                HttpVersion.HTTP_1_1,
                HttpMethod.POST,
                _uri.getRawPath(),
                buf);

        request.headers().set(HttpHeaderNames.HOST, _uri.getHost())
                .set(HttpHeaderNames.ACCEPT_CHARSET, CharsetUtil.UTF_8)
                .set(HttpHeaderNames.USER_AGENT, USER_AGENT)
                .set(HttpHeaderNames.ACCEPT, ACCEPT_ALL)
                .set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes())
                ;
       //...
       channel.writeAndFlush(request);
```

JVM options: -Dio.netty.leakDetectionLevel=paranoid
The [netty guide](http://netty.io/wiki/reference-counted-objects.html) suggested don't to care about the outbound ByteBuf object, but I don't know when and how to release it.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/3562","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/3562/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/3562/comments","events_url":"https://api.github.com/repos/netty/netty/issues/3562/events","html_url":"https://github.com/netty/netty/issues/3562","id":65704322,"node_id":"MDU6SXNzdWU2NTcwNDMyMg==","number":3562,"title":"What's going on when I do a outbound pass a ByteBuf object","user":{"login":"junjiemars","id":1978181,"node_id":"MDQ6VXNlcjE5NzgxODE=","avatar_url":"https://avatars0.githubusercontent.com/u/1978181?v=4","gravatar_id":"","url":"https://api.github.com/users/junjiemars","html_url":"https://github.com/junjiemars","followers_url":"https://api.github.com/users/junjiemars/followers","following_url":"https://api.github.com/users/junjiemars/following{/other_user}","gists_url":"https://api.github.com/users/junjiemars/gists{/gist_id}","starred_url":"https://api.github.com/users/junjiemars/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/junjiemars/subscriptions","organizations_url":"https://api.github.com/users/junjiemars/orgs","repos_url":"https://api.github.com/users/junjiemars/repos","events_url":"https://api.github.com/users/junjiemars/events{/privacy}","received_events_url":"https://api.github.com/users/junjiemars/received_events","type":"User","site_admin":false},"labels":[{"id":6731742,"node_id":"MDU6TGFiZWw2NzMxNzQy","url":"https://api.github.com/repos/netty/netty/labels/not%20a%20bug","name":"not a bug","color":"e6e6e6","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-04-01T14:50:36Z","updated_at":"2015-04-01T15:24:24Z","closed_at":"2015-04-01T15:24:13Z","author_association":"NONE","body":"```\nApr 01, 2015 10:37:29 PM io.netty.util.ResourceLeakDetector reportLeak\nSEVERE: LEAK: ByteBuf.release() was not called before it's garbage-collected. See http://netty.io/wiki/reference-counted-objects.html for more information.\nRecent access records: 0\n```\n\nI'd extends DefaultFullHttpRequest and the calling code NEW a ByteBuf object from a string then call `channel.writeAndFlush(it)` such like:\n\n```\n        final ByteBuf buf = PooledByteBufAllocator.DEFAULT\n                .directBuffer()              \n                .alloc()\n                .buffer(512)\n                .writeBytes(data.getBytes(CharsetUtil.UTF_8));\n        final FullHttpRequest request = new DefaultFullHttpRequest(\n                HttpVersion.HTTP_1_1,\n                HttpMethod.POST,\n                _uri.getRawPath(),\n                buf);\n\n        request.headers().set(HttpHeaderNames.HOST, _uri.getHost())\n                .set(HttpHeaderNames.ACCEPT_CHARSET, CharsetUtil.UTF_8)\n                .set(HttpHeaderNames.USER_AGENT, USER_AGENT)\n                .set(HttpHeaderNames.ACCEPT, ACCEPT_ALL)\n                .set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes())\n                ;\n       //...\n       channel.writeAndFlush(request);\n```\n\nJVM options: -Dio.netty.leakDetectionLevel=paranoid\nThe [netty guide](http://netty.io/wiki/reference-counted-objects.html) suggested don't to care about the outbound ByteBuf object, but I don't know when and how to release it.\n","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["88522048"], "labels":["not a bug"]}