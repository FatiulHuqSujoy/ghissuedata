{"id":"8616", "title":"ChannelInitializer.initChannel() executed more than once when used with context executor", "body":"### Expected behavior

Expected ChannelInitializer.initChannel() to be executed one. 

### Actual behavior

A subsequent execution causes exception to be thrown because it can't find itself or remove itself again.

```
2018-12-01 01:22:02,527 [WARN] [pool-2-thread-1] ChannelInitializer ? Failed to initialize a channel. Closing: [id: 0xce54469f, L:local:test - R:local:E:d49bc512]
java.lang.AssertionError: expected object to not be null
	at org.testng.Assert.fail(Assert.java:93) ~[testng-6.11.jar:?]
	at org.testng.Assert.assertNotNull(Assert.java:422) ~[testng-6.11.jar:?]
	at org.testng.Assert.assertNotNull(Assert.java:407) ~[testng-6.11.jar:?]
	at test.TestChannelInitializer$2.initChannel(TestChannelInitializer.java:93) ~[espresso-router-impl/:?]
	at io.netty.channel.ChannelInitializer.initChannel(ChannelInitializer.java:113) [netty-all-4.1.22.Final.jar:4.1.22.Final]
	at io.netty.channel.ChannelInitializer.channelRegistered(ChannelInitializer.java:76) [netty-all-4.1.22.Final.jar:4.1.22.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRegistered(AbstractChannelHandlerContext.java:149) [netty-all-4.1.22.Final.jar:4.1.22.Final]
	at io.netty.channel.AbstractChannelHandlerContext.access$000(AbstractChannelHandlerContext.java:38) [netty-all-4.1.22.Final.jar:4.1.22.Final]
	at io.netty.channel.AbstractChannelHandlerContext$1.run(AbstractChannelHandlerContext.java:140) [netty-all-4.1.22.Final.jar:4.1.22.Final]
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [?:1.8.0_121]
	at java.util.concurrent.FutureTask.run(FutureTask.java:266) [?:1.8.0_121]
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$201(ScheduledThreadPoolExecutor.java:180) [?:1.8.0_121]
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:293) [?:1.8.0_121]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142) [?:1.8.0_121]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617) [?:1.8.0_121]
	at java.lang.Thread.run(Thread.java:745) [?:1.8.0_121]
2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] READ: Hello World
2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] READ COMPLETE
2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] ACTIVE
2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] REGISTERED
2018-12-01 01:22:02,534 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] INACTIVE
2018-12-01 01:22:02,534 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] UNREGISTERED

java.lang.AssertionError: expected [1] but found [2]
Expected :1
Actual   :2


	at org.testng.Assert.fail(Assert.java:93)
	at org.testng.Assert.failNotEquals(Assert.java:512)
	at org.testng.Assert.assertEqualsImpl(Assert.java:134)
	at org.testng.Assert.assertEquals(Assert.java:115)
	at org.testng.Assert.assertEquals(Assert.java:388)
	at org.testng.Assert.assertEquals(Assert.java:398)
	at test.TestChannelInitializer.test(TestChannelInitializer.java:137)

```

### Steps to reproduce

See repro code below.

### Minimal yet complete reproducer code (or URL to code)

```java
package test;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.local.LocalAddress;
import io.netty.channel.local.LocalChannel;
import io.netty.channel.local.LocalServerChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.AbstractEventExecutor;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.concurrent.Future;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestChannelInitializer {

  @Test
  public void test() throws InterruptedException {

    AtomicInteger invokeCount = new AtomicInteger();
    AtomicInteger completeCount = new AtomicInteger();

    LocalAddress addr = new LocalAddress(\"test\");

    ChannelHandler logger = new LoggingHandler(LogLevel.WARN);

    ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();

    EventExecutor exec = new AbstractEventExecutor() {
      @Override
      public void shutdown() {
        throw new IllegalStateException();
      }

      @Override
      public boolean inEventLoop(Thread thread) {
        return false;
      }

      @Override
      public boolean isShuttingDown() {
        return false;
      }

      @Override
      public Future<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {
        throw new IllegalStateException();
      }

      @Override
      public Future<?> terminationFuture() {
        throw new IllegalStateException();
      }

      @Override
      public boolean isShutdown() {
        return execService.isShutdown();
      }

      @Override
      public boolean isTerminated() {
        return execService.isTerminated();
      }

      @Override
      public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return execService.awaitTermination(timeout, unit);
      }

      @Override
      public void execute(Runnable command) {
        execService.schedule(command, 1, TimeUnit.NANOSECONDS);
      }
    };

    ChannelInitializer<Channel> otherInitializer = new ChannelInitializer<Channel>() {
      @Override
      protected void initChannel(Channel ch) throws Exception {
        invokeCount.incrementAndGet();
        ChannelHandlerContext ctx = ch.pipeline().context(this);

        Assert.assertNotNull(ctx); // FAILS HERE

        ch.pipeline().addAfter(ctx.executor(), ctx.name(), null, logger);
        completeCount.decrementAndGet();
      }
    };

    DefaultEventLoopGroup group = new DefaultEventLoopGroup(1);

    ServerBootstrap serverBootstrap = new ServerBootstrap()
        .channel(LocalServerChannel.class)
        .group(group)
        .localAddress(addr)
        .childHandler(new ChannelInitializer<LocalChannel>() {
          @Override
          protected void initChannel(LocalChannel ch) throws Exception {
            ch.pipeline().addLast(exec, otherInitializer);
          }
        });

    Channel server = serverBootstrap.bind().sync().channel();

    Bootstrap clientBootstrap = new Bootstrap()
        .channel(LocalChannel.class)
        .group(group)
        .remoteAddress(addr)
        .handler(new ChannelInitializer<LocalChannel>() {
          @Override
          protected void initChannel(LocalChannel ch) throws Exception {
          }
        });

    Channel client = clientBootstrap.connect().sync().channel();

    client.writeAndFlush(\"Hello World\").sync();

    Thread.sleep(1000);

    client.close().sync();
    server.close().sync();

    execService.shutdown();
    execService.awaitTermination(10, TimeUnit.SECONDS);

    Assert.assertEquals(invokeCount.get(), 1); // This seems to be 2 instead of 1
    Assert.assertEquals(invokeCount.get(), completeCount.get());
  }
}
```

### Netty version

4.1.22.Final

### JVM version (e.g. `java -version`)

```
java version \"1.8.0_121\"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)
```

### OS version (e.g. `uname -a`)

RHEL7
3.10.0-514.55.4.el7.x86_64 #1 SMP Fri Aug 10 17:03:02 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/8616","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/8616/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/8616/comments","events_url":"https://api.github.com/repos/netty/netty/issues/8616/events","html_url":"https://github.com/netty/netty/issues/8616","id":386441561,"node_id":"MDU6SXNzdWUzODY0NDE1NjE=","number":8616,"title":"ChannelInitializer.initChannel() executed more than once when used with context executor","user":{"login":"atcurtis","id":1650005,"node_id":"MDQ6VXNlcjE2NTAwMDU=","avatar_url":"https://avatars3.githubusercontent.com/u/1650005?v=4","gravatar_id":"","url":"https://api.github.com/users/atcurtis","html_url":"https://github.com/atcurtis","followers_url":"https://api.github.com/users/atcurtis/followers","following_url":"https://api.github.com/users/atcurtis/following{/other_user}","gists_url":"https://api.github.com/users/atcurtis/gists{/gist_id}","starred_url":"https://api.github.com/users/atcurtis/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/atcurtis/subscriptions","organizations_url":"https://api.github.com/users/atcurtis/orgs","repos_url":"https://api.github.com/users/atcurtis/repos","events_url":"https://api.github.com/users/atcurtis/events{/privacy}","received_events_url":"https://api.github.com/users/atcurtis/received_events","type":"User","site_admin":false},"labels":[{"id":185727,"node_id":"MDU6TGFiZWwxODU3Mjc=","url":"https://api.github.com/repos/netty/netty/labels/defect","name":"defect","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"assignees":[{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/netty/netty/milestones/206","html_url":"https://github.com/netty/netty/milestone/206","labels_url":"https://api.github.com/repos/netty/netty/milestones/206/labels","id":3857863,"node_id":"MDk6TWlsZXN0b25lMzg1Nzg2Mw==","number":206,"title":"4.1.33.Final","description":"","creator":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":76,"state":"closed","created_at":"2018-11-29T09:53:10Z","updated_at":"2019-01-21T14:07:24Z","due_on":null,"closed_at":"2019-01-21T14:07:24Z"},"comments":4,"created_at":"2018-12-01T09:28:21Z","updated_at":"2018-12-05T18:30:18Z","closed_at":"2018-12-05T18:30:18Z","author_association":"CONTRIBUTOR","body":"### Expected behavior\r\n\r\nExpected ChannelInitializer.initChannel() to be executed one. \r\n\r\n### Actual behavior\r\n\r\nA subsequent execution causes exception to be thrown because it can't find itself or remove itself again.\r\n\r\n```\r\n2018-12-01 01:22:02,527 [WARN] [pool-2-thread-1] ChannelInitializer ? Failed to initialize a channel. Closing: [id: 0xce54469f, L:local:test - R:local:E:d49bc512]\r\njava.lang.AssertionError: expected object to not be null\r\n\tat org.testng.Assert.fail(Assert.java:93) ~[testng-6.11.jar:?]\r\n\tat org.testng.Assert.assertNotNull(Assert.java:422) ~[testng-6.11.jar:?]\r\n\tat org.testng.Assert.assertNotNull(Assert.java:407) ~[testng-6.11.jar:?]\r\n\tat test.TestChannelInitializer$2.initChannel(TestChannelInitializer.java:93) ~[espresso-router-impl/:?]\r\n\tat io.netty.channel.ChannelInitializer.initChannel(ChannelInitializer.java:113) [netty-all-4.1.22.Final.jar:4.1.22.Final]\r\n\tat io.netty.channel.ChannelInitializer.channelRegistered(ChannelInitializer.java:76) [netty-all-4.1.22.Final.jar:4.1.22.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelRegistered(AbstractChannelHandlerContext.java:149) [netty-all-4.1.22.Final.jar:4.1.22.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext.access$000(AbstractChannelHandlerContext.java:38) [netty-all-4.1.22.Final.jar:4.1.22.Final]\r\n\tat io.netty.channel.AbstractChannelHandlerContext$1.run(AbstractChannelHandlerContext.java:140) [netty-all-4.1.22.Final.jar:4.1.22.Final]\r\n\tat java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [?:1.8.0_121]\r\n\tat java.util.concurrent.FutureTask.run(FutureTask.java:266) [?:1.8.0_121]\r\n\tat java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$201(ScheduledThreadPoolExecutor.java:180) [?:1.8.0_121]\r\n\tat java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:293) [?:1.8.0_121]\r\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142) [?:1.8.0_121]\r\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617) [?:1.8.0_121]\r\n\tat java.lang.Thread.run(Thread.java:745) [?:1.8.0_121]\r\n2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] READ: Hello World\r\n2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] READ COMPLETE\r\n2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] ACTIVE\r\n2018-12-01 01:22:02,533 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] REGISTERED\r\n2018-12-01 01:22:02,534 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] INACTIVE\r\n2018-12-01 01:22:02,534 [WARN] [pool-2-thread-1] LoggingHandler - [id: 0xce54469f, L:local:test ! R:local:E:d49bc512] UNREGISTERED\r\n\r\njava.lang.AssertionError: expected [1] but found [2]\r\nExpected :1\r\nActual   :2\r\n\r\n\r\n\tat org.testng.Assert.fail(Assert.java:93)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:512)\r\n\tat org.testng.Assert.assertEqualsImpl(Assert.java:134)\r\n\tat org.testng.Assert.assertEquals(Assert.java:115)\r\n\tat org.testng.Assert.assertEquals(Assert.java:388)\r\n\tat org.testng.Assert.assertEquals(Assert.java:398)\r\n\tat test.TestChannelInitializer.test(TestChannelInitializer.java:137)\r\n\r\n```\r\n\r\n### Steps to reproduce\r\n\r\nSee repro code below.\r\n\r\n### Minimal yet complete reproducer code (or URL to code)\r\n\r\n```java\r\npackage test;\r\n\r\nimport io.netty.bootstrap.Bootstrap;\r\nimport io.netty.bootstrap.ServerBootstrap;\r\nimport io.netty.channel.Channel;\r\nimport io.netty.channel.ChannelHandler;\r\nimport io.netty.channel.ChannelHandlerContext;\r\nimport io.netty.channel.ChannelInitializer;\r\nimport io.netty.channel.DefaultEventLoopGroup;\r\nimport io.netty.channel.local.LocalAddress;\r\nimport io.netty.channel.local.LocalChannel;\r\nimport io.netty.channel.local.LocalServerChannel;\r\nimport io.netty.handler.logging.LogLevel;\r\nimport io.netty.handler.logging.LoggingHandler;\r\nimport io.netty.util.concurrent.AbstractEventExecutor;\r\nimport io.netty.util.concurrent.EventExecutor;\r\nimport io.netty.util.concurrent.Future;\r\nimport java.util.concurrent.Executors;\r\nimport java.util.concurrent.ScheduledExecutorService;\r\nimport java.util.concurrent.TimeUnit;\r\nimport java.util.concurrent.atomic.AtomicInteger;\r\nimport org.testng.Assert;\r\nimport org.testng.annotations.Test;\r\n\r\n\r\npublic class TestChannelInitializer {\r\n\r\n  @Test\r\n  public void test() throws InterruptedException {\r\n\r\n    AtomicInteger invokeCount = new AtomicInteger();\r\n    AtomicInteger completeCount = new AtomicInteger();\r\n\r\n    LocalAddress addr = new LocalAddress(\"test\");\r\n\r\n    ChannelHandler logger = new LoggingHandler(LogLevel.WARN);\r\n\r\n    ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();\r\n\r\n    EventExecutor exec = new AbstractEventExecutor() {\r\n      @Override\r\n      public void shutdown() {\r\n        throw new IllegalStateException();\r\n      }\r\n\r\n      @Override\r\n      public boolean inEventLoop(Thread thread) {\r\n        return false;\r\n      }\r\n\r\n      @Override\r\n      public boolean isShuttingDown() {\r\n        return false;\r\n      }\r\n\r\n      @Override\r\n      public Future<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {\r\n        throw new IllegalStateException();\r\n      }\r\n\r\n      @Override\r\n      public Future<?> terminationFuture() {\r\n        throw new IllegalStateException();\r\n      }\r\n\r\n      @Override\r\n      public boolean isShutdown() {\r\n        return execService.isShutdown();\r\n      }\r\n\r\n      @Override\r\n      public boolean isTerminated() {\r\n        return execService.isTerminated();\r\n      }\r\n\r\n      @Override\r\n      public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {\r\n        return execService.awaitTermination(timeout, unit);\r\n      }\r\n\r\n      @Override\r\n      public void execute(Runnable command) {\r\n        execService.schedule(command, 1, TimeUnit.NANOSECONDS);\r\n      }\r\n    };\r\n\r\n    ChannelInitializer<Channel> otherInitializer = new ChannelInitializer<Channel>() {\r\n      @Override\r\n      protected void initChannel(Channel ch) throws Exception {\r\n        invokeCount.incrementAndGet();\r\n        ChannelHandlerContext ctx = ch.pipeline().context(this);\r\n\r\n        Assert.assertNotNull(ctx); // FAILS HERE\r\n\r\n        ch.pipeline().addAfter(ctx.executor(), ctx.name(), null, logger);\r\n        completeCount.decrementAndGet();\r\n      }\r\n    };\r\n\r\n    DefaultEventLoopGroup group = new DefaultEventLoopGroup(1);\r\n\r\n    ServerBootstrap serverBootstrap = new ServerBootstrap()\r\n        .channel(LocalServerChannel.class)\r\n        .group(group)\r\n        .localAddress(addr)\r\n        .childHandler(new ChannelInitializer<LocalChannel>() {\r\n          @Override\r\n          protected void initChannel(LocalChannel ch) throws Exception {\r\n            ch.pipeline().addLast(exec, otherInitializer);\r\n          }\r\n        });\r\n\r\n    Channel server = serverBootstrap.bind().sync().channel();\r\n\r\n    Bootstrap clientBootstrap = new Bootstrap()\r\n        .channel(LocalChannel.class)\r\n        .group(group)\r\n        .remoteAddress(addr)\r\n        .handler(new ChannelInitializer<LocalChannel>() {\r\n          @Override\r\n          protected void initChannel(LocalChannel ch) throws Exception {\r\n          }\r\n        });\r\n\r\n    Channel client = clientBootstrap.connect().sync().channel();\r\n\r\n    client.writeAndFlush(\"Hello World\").sync();\r\n\r\n    Thread.sleep(1000);\r\n\r\n    client.close().sync();\r\n    server.close().sync();\r\n\r\n    execService.shutdown();\r\n    execService.awaitTermination(10, TimeUnit.SECONDS);\r\n\r\n    Assert.assertEquals(invokeCount.get(), 1); // This seems to be 2 instead of 1\r\n    Assert.assertEquals(invokeCount.get(), completeCount.get());\r\n  }\r\n}\r\n```\r\n\r\n### Netty version\r\n\r\n4.1.22.Final\r\n\r\n### JVM version (e.g. `java -version`)\r\n\r\n```\r\njava version \"1.8.0_121\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_121-b13)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)\r\n```\r\n\r\n### OS version (e.g. `uname -a`)\r\n\r\nRHEL7\r\n3.10.0-514.55.4.el7.x86_64 #1 SMP Fri Aug 10 17:03:02 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux","closed_by":{"login":"normanmaurer","id":439362,"node_id":"MDQ6VXNlcjQzOTM2Mg==","avatar_url":"https://avatars3.githubusercontent.com/u/439362?v=4","gravatar_id":"","url":"https://api.github.com/users/normanmaurer","html_url":"https://github.com/normanmaurer","followers_url":"https://api.github.com/users/normanmaurer/followers","following_url":"https://api.github.com/users/normanmaurer/following{/other_user}","gists_url":"https://api.github.com/users/normanmaurer/gists{/gist_id}","starred_url":"https://api.github.com/users/normanmaurer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/normanmaurer/subscriptions","organizations_url":"https://api.github.com/users/normanmaurer/orgs","repos_url":"https://api.github.com/users/normanmaurer/repos","events_url":"https://api.github.com/users/normanmaurer/events{/privacy}","received_events_url":"https://api.github.com/users/normanmaurer/received_events","type":"User","site_admin":false}}", "commentIds":["443413276","443852188","443859730","444122243"], "labels":["defect"]}