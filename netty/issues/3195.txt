{"id":"3195", "title":"Netty Clients \"Fighting\" for data over the network.", "body":"So, I needed to test a feature in my application which requires me to have two clients open, both of which are connected via localhost. I'm sending all data through the channel, and it seems that the channels are different.

`Client One: [id: 0xd7f05dd1, /127.0.0.1:56571 => /127.0.0.1:5055]`

`Client Two: [id: 0xeb0a36e7, /127.0.0.1:56606 => /127.0.0.1:5055]`

However, when sending information over the network, and having multiple clients open, the clients compete for the data, here's an example:

Server sending info:

```
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote ID: 97271
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote X: 6.54161
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote Y: -8.271806
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote Z: 1.8378098
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote ROT: 0.0
[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote PRIV: 0
[DEBUG::PacketBuffer]: Packet with an id of 2 was constructed.
[DEBUG::PacketBuffer]: The packet was 28 bytes long.
[DEBUG::PacketBuffer]: The packet type was TCP
```

This packet was only sent to Client B, containing information about Client A, however Client A and Client B both attack the network to retrieve this data.

All data that was sent above is supposed to be read (in exact order) by client b, however here's the result.

Client One Read:

```
[2]Read ID: -1056679599
[2]Read X: 1.8378098
[2]Read Y: 0.0
[2]Read Z: 0.0
```

Client Two Read:

```
[2]Read ID: 1087460574
[2]Read X: -8.271806
[2]Read Y: 1.8378098
[2]Read Z: 0.0
[2]Read ROT: 0.0
```

As you can see, the client isn't even completing it's read in either scenario, because the stream is starved, due to both clients attempting to read from it.

When sending data:

`getChannel().writeAndFlush(buffer.retain());`

Where all this data is stored in a `ByteBuf`(Netty 4) AKA `ChannelBuffer`(Netty 3)

Debugging shows that we're only sending to Client Two, but both clients still try to read it, is this a bug, or a feature (to allow faster processing using multiple clients in a single application?) -- either way, how do I prevent this.
", "json":"{"url":"https://api.github.com/repos/netty/netty/issues/3195","repository_url":"https://api.github.com/repos/netty/netty","labels_url":"https://api.github.com/repos/netty/netty/issues/3195/labels{/name}","comments_url":"https://api.github.com/repos/netty/netty/issues/3195/comments","events_url":"https://api.github.com/repos/netty/netty/issues/3195/events","html_url":"https://github.com/netty/netty/issues/3195","id":50680077,"node_id":"MDU6SXNzdWU1MDY4MDA3Nw==","number":3195,"title":"Netty Clients \"Fighting\" for data over the network.","user":{"login":"ghost","id":10137,"node_id":"MDQ6VXNlcjEwMTM3","avatar_url":"https://avatars3.githubusercontent.com/u/10137?v=4","gravatar_id":"","url":"https://api.github.com/users/ghost","html_url":"https://github.com/ghost","followers_url":"https://api.github.com/users/ghost/followers","following_url":"https://api.github.com/users/ghost/following{/other_user}","gists_url":"https://api.github.com/users/ghost/gists{/gist_id}","starred_url":"https://api.github.com/users/ghost/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ghost/subscriptions","organizations_url":"https://api.github.com/users/ghost/orgs","repos_url":"https://api.github.com/users/ghost/repos","events_url":"https://api.github.com/users/ghost/events{/privacy}","received_events_url":"https://api.github.com/users/ghost/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2014-12-02T16:48:41Z","updated_at":"2014-12-03T02:06:10Z","closed_at":"2014-12-03T01:50:20Z","author_association":"NONE","body":"So, I needed to test a feature in my application which requires me to have two clients open, both of which are connected via localhost. I'm sending all data through the channel, and it seems that the channels are different.\n\n`Client One: [id: 0xd7f05dd1, /127.0.0.1:56571 => /127.0.0.1:5055]`\n\n`Client Two: [id: 0xeb0a36e7, /127.0.0.1:56606 => /127.0.0.1:5055]`\n\nHowever, when sending information over the network, and having multiple clients open, the clients compete for the data, here's an example:\n\nServer sending info:\n\n```\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote ID: 97271\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote X: 6.54161\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote Y: -8.271806\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote Z: 1.8378098\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote ROT: 0.0\n[DEBUG::Packet_02_RegisterRemotePlayer]: Wrote PRIV: 0\n[DEBUG::PacketBuffer]: Packet with an id of 2 was constructed.\n[DEBUG::PacketBuffer]: The packet was 28 bytes long.\n[DEBUG::PacketBuffer]: The packet type was TCP\n```\n\nThis packet was only sent to Client B, containing information about Client A, however Client A and Client B both attack the network to retrieve this data.\n\nAll data that was sent above is supposed to be read (in exact order) by client b, however here's the result.\n\nClient One Read:\n\n```\n[2]Read ID: -1056679599\n[2]Read X: 1.8378098\n[2]Read Y: 0.0\n[2]Read Z: 0.0\n```\n\nClient Two Read:\n\n```\n[2]Read ID: 1087460574\n[2]Read X: -8.271806\n[2]Read Y: 1.8378098\n[2]Read Z: 0.0\n[2]Read ROT: 0.0\n```\n\nAs you can see, the client isn't even completing it's read in either scenario, because the stream is starved, due to both clients attempting to read from it.\n\nWhen sending data:\n\n`getChannel().writeAndFlush(buffer.retain());`\n\nWhere all this data is stored in a `ByteBuf`(Netty 4) AKA `ChannelBuffer`(Netty 3)\n\nDebugging shows that we're only sending to Client Two, but both clients still try to read it, is this a bug, or a feature (to allow faster processing using multiple clients in a single application?) -- either way, how do I prevent this.\n","closed_by":{"login":"Scottmitch","id":7562868,"node_id":"MDQ6VXNlcjc1NjI4Njg=","avatar_url":"https://avatars0.githubusercontent.com/u/7562868?v=4","gravatar_id":"","url":"https://api.github.com/users/Scottmitch","html_url":"https://github.com/Scottmitch","followers_url":"https://api.github.com/users/Scottmitch/followers","following_url":"https://api.github.com/users/Scottmitch/following{/other_user}","gists_url":"https://api.github.com/users/Scottmitch/gists{/gist_id}","starred_url":"https://api.github.com/users/Scottmitch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Scottmitch/subscriptions","organizations_url":"https://api.github.com/users/Scottmitch/orgs","repos_url":"https://api.github.com/users/Scottmitch/repos","events_url":"https://api.github.com/users/Scottmitch/events{/privacy}","received_events_url":"https://api.github.com/users/Scottmitch/received_events","type":"User","site_admin":false}}", "commentIds":["65336690","65339641"], "labels":[]}