{"id":"345", "title":"Facebook Dialog open and close in 5 second", "body":"Hello I'm trying to show a dialog using the tutorial from https://developers.facebook.com/docs/android/share. The problem is when dialog appear, automatically closes. The example explain with an Activity but I'm using a fragment.

A little of code:

```
private UiLifecycleHelper uiHelper;
private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
    @Override
    public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
        Log.d(TAG_LOG, String.format(\"Error: %s\", error.toString()));
    }

    @Override
    public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
        Log.d(TAG_LOG, \"Success!\");
    }
};

private Session.StatusCallback callback = new Session.StatusCallback() {
    @Override
    public void call(Session session, SessionState state, Exception exception) {
        onSessionStateChange(session, state, exception);
    }
};

private void onSessionStateChange(Session session, SessionState state, Exception exception) {
    if (state.isOpened()) {
        Log.i(TAG_LOG, \"Logged in...\");
    } else if (state.isClosed()) {
        Log.i(TAG_LOG, \"Logged out...\");
    }
}
```

@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

```
@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
}

@Override
public void onResume() {
    super.onResume();
    uiHelper.onResume();
}

@Override
public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    uiHelper.onSaveInstanceState(outState);
}

@Override
public void onPause() {
    super.onPause();
    uiHelper.onPause();
}

@Override
public void onDestroy() {
    super.onDestroy();
    uiHelper.onDestroy();
}
```
", "json":"{"url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/345","repository_url":"https://api.github.com/repos/facebook/facebook-android-sdk","labels_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/345/labels{/name}","comments_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/345/comments","events_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/345/events","html_url":"https://github.com/facebook/facebook-android-sdk/issues/345","id":34103953,"node_id":"MDU6SXNzdWUzNDEwMzk1Mw==","number":345,"title":"Facebook Dialog open and close in 5 second","user":{"login":"Azrael94","id":2997846,"node_id":"MDQ6VXNlcjI5OTc4NDY=","avatar_url":"https://avatars3.githubusercontent.com/u/2997846?v=4","gravatar_id":"","url":"https://api.github.com/users/Azrael94","html_url":"https://github.com/Azrael94","followers_url":"https://api.github.com/users/Azrael94/followers","following_url":"https://api.github.com/users/Azrael94/following{/other_user}","gists_url":"https://api.github.com/users/Azrael94/gists{/gist_id}","starred_url":"https://api.github.com/users/Azrael94/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Azrael94/subscriptions","organizations_url":"https://api.github.com/users/Azrael94/orgs","repos_url":"https://api.github.com/users/Azrael94/repos","events_url":"https://api.github.com/users/Azrael94/events{/privacy}","received_events_url":"https://api.github.com/users/Azrael94/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2014-05-22T17:32:33Z","updated_at":"2014-10-01T13:45:24Z","closed_at":"2014-05-23T13:19:34Z","author_association":"NONE","body":"Hello I'm trying to show a dialog using the tutorial from https://developers.facebook.com/docs/android/share. The problem is when dialog appear, automatically closes. The example explain with an Activity but I'm using a fragment.\n\nA little of code:\n\n```\nprivate UiLifecycleHelper uiHelper;\nprivate FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {\n    @Override\n    public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {\n        Log.d(TAG_LOG, String.format(\"Error: %s\", error.toString()));\n    }\n\n    @Override\n    public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {\n        Log.d(TAG_LOG, \"Success!\");\n    }\n};\n\nprivate Session.StatusCallback callback = new Session.StatusCallback() {\n    @Override\n    public void call(Session session, SessionState state, Exception exception) {\n        onSessionStateChange(session, state, exception);\n    }\n};\n\nprivate void onSessionStateChange(Session session, SessionState state, Exception exception) {\n    if (state.isOpened()) {\n        Log.i(TAG_LOG, \"Logged in...\");\n    } else if (state.isClosed()) {\n        Log.i(TAG_LOG, \"Logged out...\");\n    }\n}\n```\n\n@Override\n    public void onCreate(Bundle savedInstanceState) {\n        super.onCreate(savedInstanceState);\n        uiHelper = new UiLifecycleHelper(getActivity(), callback);\n        uiHelper.onCreate(savedInstanceState);\n    }\n\n```\n@Override\npublic void onActivityResult(int requestCode, int resultCode, Intent data) {\n    super.onActivityResult(requestCode, resultCode, data);\n    uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);\n}\n\n@Override\npublic void onResume() {\n    super.onResume();\n    uiHelper.onResume();\n}\n\n@Override\npublic void onSaveInstanceState(Bundle outState) {\n    super.onSaveInstanceState(outState);\n    uiHelper.onSaveInstanceState(outState);\n}\n\n@Override\npublic void onPause() {\n    super.onPause();\n    uiHelper.onPause();\n}\n\n@Override\npublic void onDestroy() {\n    super.onDestroy();\n    uiHelper.onDestroy();\n}\n```\n","closed_by":{"login":"Azrael94","id":2997846,"node_id":"MDQ6VXNlcjI5OTc4NDY=","avatar_url":"https://avatars3.githubusercontent.com/u/2997846?v=4","gravatar_id":"","url":"https://api.github.com/users/Azrael94","html_url":"https://github.com/Azrael94","followers_url":"https://api.github.com/users/Azrael94/followers","following_url":"https://api.github.com/users/Azrael94/following{/other_user}","gists_url":"https://api.github.com/users/Azrael94/gists{/gist_id}","starred_url":"https://api.github.com/users/Azrael94/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/Azrael94/subscriptions","organizations_url":"https://api.github.com/users/Azrael94/orgs","repos_url":"https://api.github.com/users/Azrael94/repos","events_url":"https://api.github.com/users/Azrael94/events{/privacy}","received_events_url":"https://api.github.com/users/Azrael94/received_events","type":"User","site_admin":false}}", "commentIds":["43920317","43921244","43932025","44007474","57464897"], "labels":[]}