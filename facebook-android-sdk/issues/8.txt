{"id":"8", "title":"no permission to open page after success", "body":"Hello, 

I would like to use the new Facebook API in my Android program, but have a problem.

I use the simple code from readme-file to authorize a user:
    mFacebook = new Facebook();
    mFacebook.authorize(this, APP_ID, PERMISSIONS, new DialogListener() { ... });

The dialog seemed to enter a user name and password for Facebook, but than I get the following message: 
\"Advanced approval: An invalid parameter was detected, abort or continue.\"

Afterwards, I had tried around with the generated URL and found that the parameter \"display=touch\" is not recognized. So I commented out line 350 in file Facebook.java:

```
//parameters.putString(\"display\", \"touch\");
```

The login screen came again (saw something different) and the application seems to have been successful, because the API is now trying to open a page that can not open it. The following message is shown:
    \"You do not have permission to open page. fbconnect://succes/\"

The permissions are the same like in the example: 
    PERMISSIONS = new String[] {\"publish_stream\", \"read_stream\", \"offline_access\"};

In the DialogListener the callback method \"onComplete\" is not called, but \"onError\", if my Android device does not have Internet access. Can someone please help me?

I use the Motorola Milestone with Android 2.01, but I develop for Android 1.6.
", "json":"{"url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/8","repository_url":"https://api.github.com/repos/facebook/facebook-android-sdk","labels_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/8/labels{/name}","comments_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/8/comments","events_url":"https://api.github.com/repos/facebook/facebook-android-sdk/issues/8/events","html_url":"https://github.com/facebook/facebook-android-sdk/issues/8","id":211313,"node_id":"MDU6SXNzdWUyMTEzMTM=","number":8,"title":"no permission to open page after success","user":{"login":"ADietrich","id":293130,"node_id":"MDQ6VXNlcjI5MzEzMA==","avatar_url":"https://avatars0.githubusercontent.com/u/293130?v=4","gravatar_id":"","url":"https://api.github.com/users/ADietrich","html_url":"https://github.com/ADietrich","followers_url":"https://api.github.com/users/ADietrich/followers","following_url":"https://api.github.com/users/ADietrich/following{/other_user}","gists_url":"https://api.github.com/users/ADietrich/gists{/gist_id}","starred_url":"https://api.github.com/users/ADietrich/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ADietrich/subscriptions","organizations_url":"https://api.github.com/users/ADietrich/orgs","repos_url":"https://api.github.com/users/ADietrich/repos","events_url":"https://api.github.com/users/ADietrich/events{/privacy}","received_events_url":"https://api.github.com/users/ADietrich/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":11,"created_at":"2010-06-01T11:45:39Z","updated_at":"2014-10-02T07:11:56Z","closed_at":"2010-06-03T08:56:51Z","author_association":"NONE","body":"Hello, \n\nI would like to use the new Facebook API in my Android program, but have a problem.\n\nI use the simple code from readme-file to authorize a user:\n    mFacebook = new Facebook();\n    mFacebook.authorize(this, APP_ID, PERMISSIONS, new DialogListener() { ... });\n\nThe dialog seemed to enter a user name and password for Facebook, but than I get the following message: \n\"Advanced approval: An invalid parameter was detected, abort or continue.\"\n\nAfterwards, I had tried around with the generated URL and found that the parameter \"display=touch\" is not recognized. So I commented out line 350 in file Facebook.java:\n\n```\n//parameters.putString(\"display\", \"touch\");\n```\n\nThe login screen came again (saw something different) and the application seems to have been successful, because the API is now trying to open a page that can not open it. The following message is shown:\n    \"You do not have permission to open page. fbconnect://succes/\"\n\nThe permissions are the same like in the example: \n    PERMISSIONS = new String[] {\"publish_stream\", \"read_stream\", \"offline_access\"};\n\nIn the DialogListener the callback method \"onComplete\" is not called, but \"onError\", if my Android device does not have Internet access. Can someone please help me?\n\nI use the Motorola Milestone with Android 2.01, but I develop for Android 1.6.\n","closed_by":null}", "commentIds":["260743","261072","262205","262813","264247","275764","276362","288150","293310","293312","293400"], "labels":[]}