{"id":"7489", "title":"Commit does commit, rollback does not rollback with JDBC template [SPR-2802]", "body":"**[Oleksandr Alesinskyy](https://jira.spring.io/secure/ViewProfile.jspa?name=al0)** opened **[SPR-2802](https://jira.spring.io/browse/SPR-2802?redirect=false)** and commented

Hello,

I have fololwing configuration

    <bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">
       <property name=\"driverClassName\" 
           value=\"org.apache.derby.jdbc.ClientDriver\" />
       <property name=\"url\" 
           value=\"jdbc:derby://localhost:1527/d:/work/lms\" />
       <property name=\"username\" value=\"monitor\" />
       <property name=\"password\" value=\"secret\" />

\\</bean>
\\<bean id=\"txManager\" class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">
\\<property name=\"dataSource\"  ref=\"dataSource\" />
\\</bean>

    <bean id=\"monitorDao\"
        class=\"de.ntec.lms.monitor.MonitorJdbcDao\">
        <property name=\"dataSource\" ref=\"dataSource\" />
    </bean>
    
    <bean id=\"configPersister\" class=\"de.ntec.lms.monitor.ConfigPersisterJdbcImpl\">
        <constructor-arg index=\"0\" ref=\"monitorDao\"/>
        <constructor-arg index=\"1\" ref=\"txManager\"/>
    </bean>

and following method in the ConfigPersisterJdbcImpl

    public void saveConfig(String user, MonitorConfig config) {
        TransactionStatus tx= fTxManager.getTransaction(saveTx);
        try {
            System.out.println(\"Con1: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            fDao.getJdbcTemplate().update(\"update MONITOR_CFG_HEADER set panelWidth=301\");
            System.out.println(\"Con2: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            fTxManager.commit(tx);
            System.out.println(\"Con3: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            System.out.println(\"ConfigPersister.save: done \"+tx.isCompleted()+\" : \"+tx.isRollbackOnly());
        } catch (RuntimeException e) {
            System.out.println(\"Error in ConfigPersister.save: \"+e.getMessage());
            fTxManager.rollback(tx);
            throw e;
        }        
    }

After execution of this method table MONITOR_CFG_HEADER is still locked, which clearly shows that commit or rollback have not been executed.

If method is replaced with direct operation on JDBC connection as below, commit performs as it should and locks are released.
Just for information - another implementation of pooled connction (c3p0) behaves exectly the same.

    public void saveConfig(String user, MonitorConfig config) {
        TransactionStatus tx= fTxManager.getTransaction(saveTx);
        Map<String,Object> updateParams= new HashMap<String,Object>();
        updateParams.put(GRID_WIDTH,config.getGridWidth());
        updateParams.put(GRID_HEIGHT,config.getGridHeight());
        updateParams.put(PANEL_WIDTH,config.getPanelWidth());
        updateParams.put(PANEL_HEIGHT,config.getPanelHeight());
        updateParams.put(MONITOR_USER,user);
        try {
            System.out.println(\"Con1: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            System.out.println(\"Con2: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            Connection con= DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource()));
            try {
                con.createStatement().executeUpdate(\"update MONITOR_CFG_HEADER set panelWidth=301\");
                con.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(\"Con3: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            fTxManager.commit(tx);
            System.out.println(\"Con4: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));
            System.out.println(\"ConfigPersister.save: done \"+tx.isCompleted()+\" : \"+tx.isRollbackOnly());
        } catch (RuntimeException e) {
            System.out.println(\"Error in ConfigPersister.save: \"+e.getMessage());
            fTxManager.rollback(tx);
            throw e;
        }        
    }



---

**Affects:** 2.0 RC3, 2.0 RC4, 2.0 final
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7489","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7489/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7489/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7489/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7489","id":398072533,"node_id":"MDU6SXNzdWUzOTgwNzI1MzM=","number":7489,"title":"Commit does commit, rollback does not rollback with JDBC template [SPR-2802]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false,"description":"Issues in data modules (jdbc, orm, oxm, tx)"}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2006-11-05T09:07:00Z","updated_at":"2019-01-11T21:32:42Z","closed_at":"2006-11-12T21:30:31Z","author_association":"COLLABORATOR","body":"**[Oleksandr Alesinskyy](https://jira.spring.io/secure/ViewProfile.jspa?name=al0)** opened **[SPR-2802](https://jira.spring.io/browse/SPR-2802?redirect=false)** and commented\n\nHello,\n\nI have fololwing configuration\n\n    <bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">\n       <property name=\"driverClassName\" \n           value=\"org.apache.derby.jdbc.ClientDriver\" />\n       <property name=\"url\" \n           value=\"jdbc:derby://localhost:1527/d:/work/lms\" />\n       <property name=\"username\" value=\"monitor\" />\n       <property name=\"password\" value=\"secret\" />\n\n\\</bean>\n\\<bean id=\"txManager\" class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">\n\\<property name=\"dataSource\"  ref=\"dataSource\" />\n\\</bean>\n\n    <bean id=\"monitorDao\"\n        class=\"de.ntec.lms.monitor.MonitorJdbcDao\">\n        <property name=\"dataSource\" ref=\"dataSource\" />\n    </bean>\n    \n    <bean id=\"configPersister\" class=\"de.ntec.lms.monitor.ConfigPersisterJdbcImpl\">\n        <constructor-arg index=\"0\" ref=\"monitorDao\"/>\n        <constructor-arg index=\"1\" ref=\"txManager\"/>\n    </bean>\n\nand following method in the ConfigPersisterJdbcImpl\n\n    public void saveConfig(String user, MonitorConfig config) {\n        TransactionStatus tx= fTxManager.getTransaction(saveTx);\n        try {\n            System.out.println(\"Con1: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            fDao.getJdbcTemplate().update(\"update MONITOR_CFG_HEADER set panelWidth=301\");\n            System.out.println(\"Con2: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            fTxManager.commit(tx);\n            System.out.println(\"Con3: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            System.out.println(\"ConfigPersister.save: done \"+tx.isCompleted()+\" : \"+tx.isRollbackOnly());\n        } catch (RuntimeException e) {\n            System.out.println(\"Error in ConfigPersister.save: \"+e.getMessage());\n            fTxManager.rollback(tx);\n            throw e;\n        }        \n    }\n\nAfter execution of this method table MONITOR_CFG_HEADER is still locked, which clearly shows that commit or rollback have not been executed.\n\nIf method is replaced with direct operation on JDBC connection as below, commit performs as it should and locks are released.\nJust for information - another implementation of pooled connction (c3p0) behaves exectly the same.\n\n    public void saveConfig(String user, MonitorConfig config) {\n        TransactionStatus tx= fTxManager.getTransaction(saveTx);\n        Map<String,Object> updateParams= new HashMap<String,Object>();\n        updateParams.put(GRID_WIDTH,config.getGridWidth());\n        updateParams.put(GRID_HEIGHT,config.getGridHeight());\n        updateParams.put(PANEL_WIDTH,config.getPanelWidth());\n        updateParams.put(PANEL_HEIGHT,config.getPanelHeight());\n        updateParams.put(MONITOR_USER,user);\n        try {\n            System.out.println(\"Con1: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            System.out.println(\"Con2: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            Connection con= DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource()));\n            try {\n                con.createStatement().executeUpdate(\"update MONITOR_CFG_HEADER set panelWidth=301\");\n                con.commit();\n            } catch (SQLException e) {\n                e.printStackTrace();\n            }\n            System.out.println(\"Con3: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            fTxManager.commit(tx);\n            System.out.println(\"Con4: \"+DataSourceUtils.getTargetConnection(DataSourceUtils.getConnection(fDao.getDataSource())));\n            System.out.println(\"ConfigPersister.save: done \"+tx.isCompleted()+\" : \"+tx.isRollbackOnly());\n        } catch (RuntimeException e) {\n            System.out.println(\"Error in ConfigPersister.save: \"+e.getMessage());\n            fTxManager.rollback(tx);\n            throw e;\n        }        \n    }\n\n\n\n---\n\n**Affects:** 2.0 RC3, 2.0 RC4, 2.0 final\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453312044","453312046"], "labels":["in: data"]}