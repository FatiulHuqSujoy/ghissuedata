{"id":"13238", "title":"PersistenceAnnotationBeanPostProcessor does not operate on the most specific persistence annotation declaration [SPR-8594]", "body":"**[Loïc Frering](https://jira.spring.io/secure/ViewProfile.jspa?name=loicfrering)** opened **[SPR-8594](https://jira.spring.io/browse/SPR-8594?redirect=false)** and commented

Hello guys, I've got an issue with the PersistenceAnnotationBeanPostProcessor
when using multiple persistence units and inheritance. The problem is in
findPersistenceMetadata method which looks for persistence annotations in
registered beans.

The current implementation loops through **declared** fields and **declared**
methods of the actual class of the bean to detect eventual persistence
annotation and register a PersistenceElement for each.

And this process is repeated in a do ... while loop for each class in the
inheritance hierarchy of the bean's class.

Here is a simplified verion of the current algorithm:

Class<?> targetClass = clazz;do {
LinkedList<InjectionMetadata.InjectedElement> currElements = new LinkedList<InjectionMetadata.InjectedElement>();
for (Field field : targetClass.getDeclaredFields()) {
// ....        currElements.add(new PersistenceElement(field, null));
}
for (Method method : targetClass.getDeclaredMethods()) {
// ....        PropertyDescriptor pd = BeanUtils.findPropertyForMethod(method);
currElements.add(new PersistenceElement(method, pd));
}
elements.addAll(0, currElements);
targetClass = targetClass.getSuperclass();
}while (targetClass != null && targetClass != Object.class);
metadata = new InjectionMetadata(clazz, elements);

The problem is that a PersistentElement is registered for each definition of a
field or method in the class hierarchy : the most specific definition is not
the only one registered.

As a consequence having this:

public class GenericRepository {

    private EntityManager entityManager;
    
    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.entityManager = em;
    }
    
    // ....}

public class UserRepository extends GenericRepository {

    @Override
    @PersistenceContext(unitName=\"my-unit\")
    public void setEntityManager(EntityManager em) {
        parent.setEntityManager(em);
    }
    
    // ....}

Would leed to 2 injected elements in metadata:

* One for `@PersistenceContext` without unitName
* One for `@PersistenceContext` with unitName \"my-unit\"

Then Spring injects the EntityManager based on the first element definition and
skip the second one **which is the most specific**!

There might be a good reason for iterating through each declared field/method
of each super class of the bean's class but I don't get why you do not simply
use the getMethods() and getFields() reflection methods which return the most
specific definition of the field/method with annotations and would avoid having
multiple injectedElements for persistence matadata?


---

**Affects:** 3.0.5

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/65077d262c396ee919f84808bafb059ef3618e96, https://github.com/spring-projects/spring-framework/commit/49e61d2680c66e6d59aaa2d483300d4ac76cbc78
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13238","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13238/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13238/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13238/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13238","id":398113940,"node_id":"MDU6SXNzdWUzOTgxMTM5NDA=","number":13238,"title":"PersistenceAnnotationBeanPostProcessor does not operate on the most specific persistence annotation declaration [SPR-8594]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/73","html_url":"https://github.com/spring-projects/spring-framework/milestone/73","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/73/labels","id":3960846,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Ng==","number":73,"title":"3.0.6","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":59,"state":"closed","created_at":"2019-01-10T22:03:19Z","updated_at":"2019-01-11T11:17:00Z","due_on":"2011-08-17T07:00:00Z","closed_at":"2019-01-10T22:03:19Z"},"comments":2,"created_at":"2011-08-09T09:23:47Z","updated_at":"2012-06-19T03:39:38Z","closed_at":"2012-06-19T03:39:38Z","author_association":"COLLABORATOR","body":"**[Loïc Frering](https://jira.spring.io/secure/ViewProfile.jspa?name=loicfrering)** opened **[SPR-8594](https://jira.spring.io/browse/SPR-8594?redirect=false)** and commented\n\nHello guys, I've got an issue with the PersistenceAnnotationBeanPostProcessor\nwhen using multiple persistence units and inheritance. The problem is in\nfindPersistenceMetadata method which looks for persistence annotations in\nregistered beans.\n\nThe current implementation loops through **declared** fields and **declared**\nmethods of the actual class of the bean to detect eventual persistence\nannotation and register a PersistenceElement for each.\n\nAnd this process is repeated in a do ... while loop for each class in the\ninheritance hierarchy of the bean's class.\n\nHere is a simplified verion of the current algorithm:\n\nClass<?> targetClass = clazz;do {\nLinkedList<InjectionMetadata.InjectedElement> currElements = new LinkedList<InjectionMetadata.InjectedElement>();\nfor (Field field : targetClass.getDeclaredFields()) {\n// ....        currElements.add(new PersistenceElement(field, null));\n}\nfor (Method method : targetClass.getDeclaredMethods()) {\n// ....        PropertyDescriptor pd = BeanUtils.findPropertyForMethod(method);\ncurrElements.add(new PersistenceElement(method, pd));\n}\nelements.addAll(0, currElements);\ntargetClass = targetClass.getSuperclass();\n}while (targetClass != null && targetClass != Object.class);\nmetadata = new InjectionMetadata(clazz, elements);\n\nThe problem is that a PersistentElement is registered for each definition of a\nfield or method in the class hierarchy : the most specific definition is not\nthe only one registered.\n\nAs a consequence having this:\n\npublic class GenericRepository {\n\n    private EntityManager entityManager;\n    \n    @PersistenceContext\n    public void setEntityManager(EntityManager em) {\n        this.entityManager = em;\n    }\n    \n    // ....}\n\npublic class UserRepository extends GenericRepository {\n\n    @Override\n    @PersistenceContext(unitName=\"my-unit\")\n    public void setEntityManager(EntityManager em) {\n        parent.setEntityManager(em);\n    }\n    \n    // ....}\n\nWould leed to 2 injected elements in metadata:\n\n* One for `@PersistenceContext` without unitName\n* One for `@PersistenceContext` with unitName \"my-unit\"\n\nThen Spring injects the EntityManager based on the first element definition and\nskip the second one **which is the most specific**!\n\nThere might be a good reason for iterating through each declared field/method\nof each super class of the bean's class but I don't get why you do not simply\nuse the getMethods() and getFields() reflection methods which return the most\nspecific definition of the field/method with annotations and would avoid having\nmultiple injectedElements for persistence matadata?\n\n\n---\n\n**Affects:** 3.0.5\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/65077d262c396ee919f84808bafb059ef3618e96, https://github.com/spring-projects/spring-framework/commit/49e61d2680c66e6d59aaa2d483300d4ac76cbc78\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453361429","453361430"], "labels":["in: data","type: bug"]}