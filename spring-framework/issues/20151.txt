{"id":"20151", "title":"can not build the project. [SPR-15592]", "body":"**[D瓜哥](https://jira.spring.io/secure/ViewProfile.jspa?name=diguage)** opened **[SPR-15592](https://jira.spring.io/browse/SPR-15592?redirect=false)** and commented

I want to import the project to IDEA.

I follow the document  **import-into-idea.md**  to  run the command **./gradlew cleanIdea :spring-oxm:compileTestJava**.

Then, it throws errors.

**How do I import the v5.x project to IDEA?**

I try many times at different ways:

First, I checkout the code to **v4.3.8.RELEASE**, I run the command **./gradlew cleanIdea :spring-oxm:compileTestJava**. It is OK.

Second, I checkout the code to **v5.0.0.RC1**, I run the same command. It is OK. But, when I import the code to IDEA as a gradle project, the IDEA  cound find many class, such as **BeanFactory**.

Third, I run **./gradlew clean :spring-oxm:compileTestJava** (the code is **v5.0.0.RC1**), it throws many errors.

My develop environment:

```java
macOS 10.12.4

java -version
java version \"1.8.0_131\"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)
```

The log is as followings:

```java
:spring-oxm:compileTestJava
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:27: error: package org.springframework.oxm.jaxb.test does not exist
import org.springframework.oxm.jaxb.test.FlightType;
                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:28: error: package org.springframework.oxm.jaxb.test does not exist
import org.springframework.oxm.jaxb.test.Flights;
                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:29: error: package org.springframework.oxm.jaxb.test does not exist
import org.springframework.oxm.jaxb.test.ObjectFactory;
                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:85: error: cannot find symbol
        private Flights flights;
                ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:36: error: package org.springframework.oxm.jaxb.test does not exist
import org.springframework.oxm.jaxb.test.FlightType;
                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:37: error: package org.springframework.oxm.jaxb.test does not exist
import org.springframework.oxm.jaxb.test.Flights;
                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:98: error: cannot find symbol
                FlightType flight = new FlightType();
                ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:98: error: cannot find symbol
                FlightType flight = new FlightType();
                                        ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:100: error: cannot find symbol
                flights = new Flights();
                              ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:166: error: cannot find symbol
                marshaller.setClassesToBeBound(FlightType.class);
                                               ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:169: error: cannot find symbol
                Flights flights = new Flights();
                ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:169: error: cannot find symbol
                Flights flights = new Flights();
                                      ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:181: error: cannot find symbol
                marshaller.setClassesToBeBound(Flights.class, FlightType.class);
                                               ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:181: error: cannot find symbol
                marshaller.setClassesToBeBound(Flights.class, FlightType.class);
                                                              ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:195: error: cannot find symbol
                assertTrue(\"Jaxb2Marshaller does not support Flights class\", marshaller.supports(Flights.class));
                                                                                                 ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:196: error: cannot find symbol
                assertTrue(\"Jaxb2Marshaller does not support Flights generic type\", marshaller.supports((Type)Flights.class));
                                                                                                              ^
  symbol:   class Flights
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:196: warning: [cast] redundant cast to class
                assertTrue(\"Jaxb2Marshaller does not support Flights generic type\", marshaller.supports((Type)Flights.class));
                                                                                                        ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:198: error: cannot find symbol
                assertFalse(\"Jaxb2Marshaller supports FlightType class\", marshaller.supports(FlightType.class));
                                                                                             ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:199: error: cannot find symbol
                assertFalse(\"Jaxb2Marshaller supports FlightType type\", marshaller.supports((Type)FlightType.class));
                                                                                                  ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:199: warning: [cast] redundant cast to class
                assertFalse(\"Jaxb2Marshaller supports FlightType type\", marshaller.supports((Type)FlightType.class));
                                                                                            ^
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:201: error: cannot find symbol
                Method method = ObjectFactory.class.getDeclaredMethod(\"createFlight\", FlightType.class);
                                                                                      ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:201: error: cannot find symbol
                Method method = ObjectFactory.class.getDeclaredMethod(\"createFlight\", FlightType.class);
                                ^
  symbol:   class ObjectFactory
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:206: error: cannot find symbol
                JAXBElement<FlightType> flightTypeJAXBElement = new JAXBElement<>(new QName(\"http://springframework.org\", \"flight\"), FlightType.class,
                            ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:206: error: cannot find symbol
                JAXBElement<FlightType> flightTypeJAXBElement = new JAXBElement<>(new QName(\"http://springframework.org\", \"flight\"), FlightType.class,
                                                                                                                                     ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:207: error: cannot find symbol
                                new FlightType());
                                    ^
  symbol:   class FlightType
  location: class Jaxb2MarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:65: error: cannot find symbol
                Flights flights = (Flights) o;
                ^
  symbol:   class Flights
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:65: error: cannot find symbol
                Flights flights = (Flights) o;
                                   ^
  symbol:   class Flights
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:73: error: cannot find symbol
                FlightType flight = (FlightType) o;
                ^
  symbol:   class FlightType
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:73: error: cannot find symbol
                FlightType flight = (FlightType) o;
                                     ^
  symbol:   class FlightType
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:119: error: cannot find symbol
                JAXBElement<FlightType> element = (JAXBElement<FlightType>) unmarshaller.unmarshal(source);
                            ^
  symbol:   class FlightType
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:119: error: cannot find symbol
                JAXBElement<FlightType> element = (JAXBElement<FlightType>) unmarshaller.unmarshal(source);
                                                               ^
  symbol:   class FlightType
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:120: error: cannot find symbol
                FlightType flight = element.getValue();
                ^
  symbol:   class FlightType
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:143: error: cannot find symbol
                Flights f = (Flights) unmarshaller.unmarshal(new StreamSource(file));
                ^
  symbol:   class Flights
  location: class Jaxb2UnmarshallerTests
spring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:143: error: cannot find symbol
                Flights f = (Flights) unmarshaller.unmarshal(new StreamSource(file));
                             ^
  symbol:   class Flights
  location: class Jaxb2UnmarshallerTests
32 errors
2 warnings
:spring-oxm:compileTestJava FAILED
:spring-oxm:copyTestKotlinClasses SKIPPED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':spring-oxm:compileTestJava'.
> Compilation failed; see the compiler error output for details.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

BUILD FAILED

Total time: 1.718 secs
```



---

**Affects:** 5.0 RC1

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/beb7ed97155b9164797aec7024254e12b9050cd4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20151","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20151/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20151/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20151/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20151","id":398209930,"node_id":"MDU6SXNzdWUzOTgyMDk5MzA=","number":20151,"title":"can not build the project. [SPR-15592]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"snicoll","id":490484,"node_id":"MDQ6VXNlcjQ5MDQ4NA==","avatar_url":"https://avatars0.githubusercontent.com/u/490484?v=4","gravatar_id":"","url":"https://api.github.com/users/snicoll","html_url":"https://github.com/snicoll","followers_url":"https://api.github.com/users/snicoll/followers","following_url":"https://api.github.com/users/snicoll/following{/other_user}","gists_url":"https://api.github.com/users/snicoll/gists{/gist_id}","starred_url":"https://api.github.com/users/snicoll/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snicoll/subscriptions","organizations_url":"https://api.github.com/users/snicoll/orgs","repos_url":"https://api.github.com/users/snicoll/repos","events_url":"https://api.github.com/users/snicoll/events{/privacy}","received_events_url":"https://api.github.com/users/snicoll/received_events","type":"User","site_admin":false},"assignees":[{"login":"snicoll","id":490484,"node_id":"MDQ6VXNlcjQ5MDQ4NA==","avatar_url":"https://avatars0.githubusercontent.com/u/490484?v=4","gravatar_id":"","url":"https://api.github.com/users/snicoll","html_url":"https://github.com/snicoll","followers_url":"https://api.github.com/users/snicoll/followers","following_url":"https://api.github.com/users/snicoll/following{/other_user}","gists_url":"https://api.github.com/users/snicoll/gists{/gist_id}","starred_url":"https://api.github.com/users/snicoll/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snicoll/subscriptions","organizations_url":"https://api.github.com/users/snicoll/orgs","repos_url":"https://api.github.com/users/snicoll/repos","events_url":"https://api.github.com/users/snicoll/events{/privacy}","received_events_url":"https://api.github.com/users/snicoll/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2017-05-26T04:04:58Z","updated_at":"2019-01-11T17:03:21Z","closed_at":"2017-06-01T09:36:09Z","author_association":"COLLABORATOR","body":"**[D瓜哥](https://jira.spring.io/secure/ViewProfile.jspa?name=diguage)** opened **[SPR-15592](https://jira.spring.io/browse/SPR-15592?redirect=false)** and commented\n\nI want to import the project to IDEA.\n\nI follow the document  **import-into-idea.md**  to  run the command **./gradlew cleanIdea :spring-oxm:compileTestJava**.\n\nThen, it throws errors.\n\n**How do I import the v5.x project to IDEA?**\n\nI try many times at different ways:\n\nFirst, I checkout the code to **v4.3.8.RELEASE**, I run the command **./gradlew cleanIdea :spring-oxm:compileTestJava**. It is OK.\n\nSecond, I checkout the code to **v5.0.0.RC1**, I run the same command. It is OK. But, when I import the code to IDEA as a gradle project, the IDEA  cound find many class, such as **BeanFactory**.\n\nThird, I run **./gradlew clean :spring-oxm:compileTestJava** (the code is **v5.0.0.RC1**), it throws many errors.\n\nMy develop environment:\n\n```java\nmacOS 10.12.4\n\njava -version\njava version \"1.8.0_131\"\nJava(TM) SE Runtime Environment (build 1.8.0_131-b11)\nJava HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)\n```\n\nThe log is as followings:\n\n```java\n:spring-oxm:compileTestJava\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:27: error: package org.springframework.oxm.jaxb.test does not exist\nimport org.springframework.oxm.jaxb.test.FlightType;\n                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:28: error: package org.springframework.oxm.jaxb.test does not exist\nimport org.springframework.oxm.jaxb.test.Flights;\n                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:29: error: package org.springframework.oxm.jaxb.test does not exist\nimport org.springframework.oxm.jaxb.test.ObjectFactory;\n                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:85: error: cannot find symbol\n        private Flights flights;\n                ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:36: error: package org.springframework.oxm.jaxb.test does not exist\nimport org.springframework.oxm.jaxb.test.FlightType;\n                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:37: error: package org.springframework.oxm.jaxb.test does not exist\nimport org.springframework.oxm.jaxb.test.Flights;\n                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:98: error: cannot find symbol\n                FlightType flight = new FlightType();\n                ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:98: error: cannot find symbol\n                FlightType flight = new FlightType();\n                                        ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:100: error: cannot find symbol\n                flights = new Flights();\n                              ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:166: error: cannot find symbol\n                marshaller.setClassesToBeBound(FlightType.class);\n                                               ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:169: error: cannot find symbol\n                Flights flights = new Flights();\n                ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:169: error: cannot find symbol\n                Flights flights = new Flights();\n                                      ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:181: error: cannot find symbol\n                marshaller.setClassesToBeBound(Flights.class, FlightType.class);\n                                               ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:181: error: cannot find symbol\n                marshaller.setClassesToBeBound(Flights.class, FlightType.class);\n                                                              ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:195: error: cannot find symbol\n                assertTrue(\"Jaxb2Marshaller does not support Flights class\", marshaller.supports(Flights.class));\n                                                                                                 ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:196: error: cannot find symbol\n                assertTrue(\"Jaxb2Marshaller does not support Flights generic type\", marshaller.supports((Type)Flights.class));\n                                                                                                              ^\n  symbol:   class Flights\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:196: warning: [cast] redundant cast to class\n                assertTrue(\"Jaxb2Marshaller does not support Flights generic type\", marshaller.supports((Type)Flights.class));\n                                                                                                        ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:198: error: cannot find symbol\n                assertFalse(\"Jaxb2Marshaller supports FlightType class\", marshaller.supports(FlightType.class));\n                                                                                             ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:199: error: cannot find symbol\n                assertFalse(\"Jaxb2Marshaller supports FlightType type\", marshaller.supports((Type)FlightType.class));\n                                                                                                  ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:199: warning: [cast] redundant cast to class\n                assertFalse(\"Jaxb2Marshaller supports FlightType type\", marshaller.supports((Type)FlightType.class));\n                                                                                            ^\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:201: error: cannot find symbol\n                Method method = ObjectFactory.class.getDeclaredMethod(\"createFlight\", FlightType.class);\n                                                                                      ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:201: error: cannot find symbol\n                Method method = ObjectFactory.class.getDeclaredMethod(\"createFlight\", FlightType.class);\n                                ^\n  symbol:   class ObjectFactory\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:206: error: cannot find symbol\n                JAXBElement<FlightType> flightTypeJAXBElement = new JAXBElement<>(new QName(\"http://springframework.org\", \"flight\"), FlightType.class,\n                            ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:206: error: cannot find symbol\n                JAXBElement<FlightType> flightTypeJAXBElement = new JAXBElement<>(new QName(\"http://springframework.org\", \"flight\"), FlightType.class,\n                                                                                                                                     ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2MarshallerTests.java:207: error: cannot find symbol\n                                new FlightType());\n                                    ^\n  symbol:   class FlightType\n  location: class Jaxb2MarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:65: error: cannot find symbol\n                Flights flights = (Flights) o;\n                ^\n  symbol:   class Flights\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:65: error: cannot find symbol\n                Flights flights = (Flights) o;\n                                   ^\n  symbol:   class Flights\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:73: error: cannot find symbol\n                FlightType flight = (FlightType) o;\n                ^\n  symbol:   class FlightType\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:73: error: cannot find symbol\n                FlightType flight = (FlightType) o;\n                                     ^\n  symbol:   class FlightType\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:119: error: cannot find symbol\n                JAXBElement<FlightType> element = (JAXBElement<FlightType>) unmarshaller.unmarshal(source);\n                            ^\n  symbol:   class FlightType\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:119: error: cannot find symbol\n                JAXBElement<FlightType> element = (JAXBElement<FlightType>) unmarshaller.unmarshal(source);\n                                                               ^\n  symbol:   class FlightType\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:120: error: cannot find symbol\n                FlightType flight = element.getValue();\n                ^\n  symbol:   class FlightType\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:143: error: cannot find symbol\n                Flights f = (Flights) unmarshaller.unmarshal(new StreamSource(file));\n                ^\n  symbol:   class Flights\n  location: class Jaxb2UnmarshallerTests\nspring-framework/spring-oxm/src/test/java/org/springframework/oxm/jaxb/Jaxb2UnmarshallerTests.java:143: error: cannot find symbol\n                Flights f = (Flights) unmarshaller.unmarshal(new StreamSource(file));\n                             ^\n  symbol:   class Flights\n  location: class Jaxb2UnmarshallerTests\n32 errors\n2 warnings\n:spring-oxm:compileTestJava FAILED\n:spring-oxm:copyTestKotlinClasses SKIPPED\n\nFAILURE: Build failed with an exception.\n\n* What went wrong:\nExecution failed for task ':spring-oxm:compileTestJava'.\n> Compilation failed; see the compiler error output for details.\n\n* Try:\nRun with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.\n\nBUILD FAILED\n\nTotal time: 1.718 secs\n```\n\n\n\n---\n\n**Affects:** 5.0 RC1\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/beb7ed97155b9164797aec7024254e12b9050cd4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453455917","453455920","453455925","453455927"], "labels":["in: data"]}