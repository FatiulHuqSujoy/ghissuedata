{"id":"17836", "title":"Validator.validate method that calls nested class validate() gets wrong object in childs error.entity validation [SPR-13245]", "body":"**[Bruce Edge](https://jira.spring.io/secure/ViewProfile.jspa?name=bedge42)** opened **[SPR-13245](https://jira.spring.io/browse/SPR-13245?redirect=false)** and commented

I'm implementing a Validator for a class with a nested array of children objects and the child validator is failing because the context in which the child validator runs contains the parent's reference in the Error.entity instead of the child's.

I'm following the docs here: http://docs.spring.io/spring/docs/current/spring-framework-reference/html/validation.html

My data model, a parent Collection object with a child Collectionitem list.

```java
public class Collection {
    private String name;
    private List<CollectionItem> items;
}

public class CollectionItem {
   private String title;
}
```

My Collection parent and CollectionItem child validators:

```java
@Component
public class CollectionValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return Collection.class.equals(clazz);
	}
        // DI for nested object validator
       @Autowired CollectionItemValidator collectionItemValidator;

        @Override
	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, \"name\", \"empty\", \"missing\");
		try {
                       errors.pushNestedPath(\"items\");
			for(CollectionItem collectionItem : collection.getItems()) {  // Validate each Collectionitem object
				ValidationUtils.invokeValidator(collectionItemValidator, collectionItem, errors);
			}
		}
		finally {
			errors.popNestedPath();
		}
	}
}

@Component
public class CollectionItemValidator implements Validator {
	@Override
	public boolean supports(Class clazz) {
		return CollectionItem.class.equals(clazz);
	}
	@Override
	public void validate(Object obj, Errors e) {          
               // The Error e.entity here has the wrong (parent) object in it when called from 
               // ValidationUtils.invokeValidator(collectionItemValidator, collectionItem, errors);
               // Therefore \"title\" is never found, as the parent Collection object does not have a \"title\" field.
		ValidationUtils.rejectIfEmpty(e, \"title\", \"empty\", \"missing\")
	}
}
```

The problem is that when the child items array is validated and CollectionItemValidator.validate() is called for each ColectionItem by ValidationUtils.invokeValidator(collectionItemValidator...) , the Error e.entity passed into the child validator is a still the parent Collection, not the child CollectionItem object, so the check for a \"title\" field always fails because ValidationUtils.rejectIfEmpty is looking at the parent rather than the child object in the errors.getFieldValue(field);:

```java
	public static void rejectIfEmpty(
			Errors errors, String field, String errorCode, Object[] errorArgs, String defaultMessage) {
		Object value = errors.getFieldValue(field);
....
```

is using the parent Collection object in the errors.getFieldValue(), so it never finds the field as only the child has a \"title\" field, the parent does not.

Note that the ValidationUtils

```java
	public static void invokeValidator(Validator validator, Object obj, Errors errors, Object... validationHints) {

```

has the right obj in the args, the problem is that the errors.entity is never updated to point to the child element.


---

**Affects:** 4.2 RC2

1 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17836","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17836/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17836/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17836/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17836","id":398181874,"node_id":"MDU6SXNzdWUzOTgxODE4NzQ=","number":17836,"title":"Validator.validate method that calls nested class validate() gets wrong object in childs error.entity validation [SPR-13245]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2015-07-16T16:09:20Z","updated_at":"2019-01-12T02:26:54Z","closed_at":"2019-01-12T02:26:54Z","author_association":"COLLABORATOR","body":"**[Bruce Edge](https://jira.spring.io/secure/ViewProfile.jspa?name=bedge42)** opened **[SPR-13245](https://jira.spring.io/browse/SPR-13245?redirect=false)** and commented\n\nI'm implementing a Validator for a class with a nested array of children objects and the child validator is failing because the context in which the child validator runs contains the parent's reference in the Error.entity instead of the child's.\n\nI'm following the docs here: http://docs.spring.io/spring/docs/current/spring-framework-reference/html/validation.html\n\nMy data model, a parent Collection object with a child Collectionitem list.\n\n```java\npublic class Collection {\n    private String name;\n    private List<CollectionItem> items;\n}\n\npublic class CollectionItem {\n   private String title;\n}\n```\n\nMy Collection parent and CollectionItem child validators:\n\n```java\n@Component\npublic class CollectionValidator implements Validator {\n\n\t@Override\n\tpublic boolean supports(Class clazz) {\n\t\treturn Collection.class.equals(clazz);\n\t}\n        // DI for nested object validator\n       @Autowired CollectionItemValidator collectionItemValidator;\n\n        @Override\n\tpublic void validate(Object obj, Errors errors) {\n\t\tValidationUtils.rejectIfEmpty(errors, \"name\", \"empty\", \"missing\");\n\t\ttry {\n                       errors.pushNestedPath(\"items\");\n\t\t\tfor(CollectionItem collectionItem : collection.getItems()) {  // Validate each Collectionitem object\n\t\t\t\tValidationUtils.invokeValidator(collectionItemValidator, collectionItem, errors);\n\t\t\t}\n\t\t}\n\t\tfinally {\n\t\t\terrors.popNestedPath();\n\t\t}\n\t}\n}\n\n@Component\npublic class CollectionItemValidator implements Validator {\n\t@Override\n\tpublic boolean supports(Class clazz) {\n\t\treturn CollectionItem.class.equals(clazz);\n\t}\n\t@Override\n\tpublic void validate(Object obj, Errors e) {          \n               // The Error e.entity here has the wrong (parent) object in it when called from \n               // ValidationUtils.invokeValidator(collectionItemValidator, collectionItem, errors);\n               // Therefore \"title\" is never found, as the parent Collection object does not have a \"title\" field.\n\t\tValidationUtils.rejectIfEmpty(e, \"title\", \"empty\", \"missing\")\n\t}\n}\n```\n\nThe problem is that when the child items array is validated and CollectionItemValidator.validate() is called for each ColectionItem by ValidationUtils.invokeValidator(collectionItemValidator...) , the Error e.entity passed into the child validator is a still the parent Collection, not the child CollectionItem object, so the check for a \"title\" field always fails because ValidationUtils.rejectIfEmpty is looking at the parent rather than the child object in the errors.getFieldValue(field);:\n\n```java\n\tpublic static void rejectIfEmpty(\n\t\t\tErrors errors, String field, String errorCode, Object[] errorArgs, String defaultMessage) {\n\t\tObject value = errors.getFieldValue(field);\n....\n```\n\nis using the parent Collection object in the errors.getFieldValue(), so it never finds the field as only the child has a \"title\" field, the parent does not.\n\nNote that the ValidationUtils\n\n```java\n\tpublic static void invokeValidator(Validator validator, Object obj, Errors errors, Object... validationHints) {\n\n```\n\nhas the right obj in the args, the problem is that the errors.entity is never updated to point to the child element.\n\n\n---\n\n**Affects:** 4.2 RC2\n\n1 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453427942","453427943","453711958"], "labels":["in: core","status: bulk-closed"]}