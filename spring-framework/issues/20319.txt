{"id":"20319", "title":"ExchangeFilterFunctions Explicit Model For Basic Authentication Credentials [SPR-15764]", "body":"**[Rob Winch](https://jira.spring.io/secure/ViewProfile.jspa?name=rwinch)** opened **[SPR-15764](https://jira.spring.io/browse/SPR-15764?redirect=false)** and commented

Currently `ExchangeFilterFunctions` provides a `USERNAME_ATTRIBUTE` and a `PASSWORD_ATTRIBUTE` and will populate the credentials in a Basic authentication header if the attributes are found.

I think this will lead to issues if we provide other ways to authenticate. Consider a client that adds both basic authentication and digest authentication (if this is eventually supported). The user wants to specify to use basic authentication so the attributes are provided. However, digest authentication overrides the basic authentication header.

Instead, it might be better to use a richer domain model for the attributes to ensure the users choice is clear.

The domain model might even provide a way to add itself to read / write the model to the attributes. For example, something like this:

```java
public class BasicAuthenticationCredential implements Consumer<Map<String,Object>> {
	private static String ATTRIBUTE_NAME = BasicAuthenticationCredential.class.getName().concat(\".ATTRIBUTE_NAME\");

	private final String username;
	private final String password;


	BasicAuthenticationCredential(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public void accept(Map<String, Object> attributes) {
		attributes.put(ATTRIBUTE_NAME, this);
	}

	public static BasicAuthenticationCredential get(Map<String,Object> attributes) {
		return (BasicAuthenticationCredential) attributes.get(ATTRIBUTE_NAME);
	}
}
```

Consumers would then be able to use:

```java
client.get()
		.uri(\"/messages/20\")
		// perhaps add static factory method for BasicAuthenticationCredential
		.attributes(new BasicAuthenticationCredential(\"joe\", \"joe\"))
```

Comparing that vs:

```java
client.get()
	.uri(\"/messages/1\")
	.attribute(ExchangeFilterFunctions.USERNAME_ATTRIBUTE, \"rob\")
	.attribute(ExchangeFilterFunctions.PASSWORD_ATTRIBUTE, \"rob\")
```

Plus now we would be able to differentiate between the two credential types because we would add a `DigestAuthenticationCredential` if we supported it later on.


---

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1d86c9c3d128f64afa51460e3ed7900963755cfd
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20319","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20319/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20319/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20319/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20319","id":398212653,"node_id":"MDU6SXNzdWUzOTgyMTI2NTM=","number":20319,"title":"ExchangeFilterFunctions Explicit Model For Basic Authentication Credentials [SPR-15764]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/180","html_url":"https://github.com/spring-projects/spring-framework/milestone/180","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/180/labels","id":3960953,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1Mw==","number":180,"title":"5.0 RC4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":64,"state":"closed","created_at":"2019-01-10T22:05:28Z","updated_at":"2019-01-11T09:59:52Z","due_on":"2017-09-10T07:00:00Z","closed_at":"2019-01-10T22:05:28Z"},"comments":3,"created_at":"2017-07-12T17:07:27Z","updated_at":"2017-09-11T12:09:48Z","closed_at":"2017-09-11T12:09:48Z","author_association":"COLLABORATOR","body":"**[Rob Winch](https://jira.spring.io/secure/ViewProfile.jspa?name=rwinch)** opened **[SPR-15764](https://jira.spring.io/browse/SPR-15764?redirect=false)** and commented\n\nCurrently `ExchangeFilterFunctions` provides a `USERNAME_ATTRIBUTE` and a `PASSWORD_ATTRIBUTE` and will populate the credentials in a Basic authentication header if the attributes are found.\n\nI think this will lead to issues if we provide other ways to authenticate. Consider a client that adds both basic authentication and digest authentication (if this is eventually supported). The user wants to specify to use basic authentication so the attributes are provided. However, digest authentication overrides the basic authentication header.\n\nInstead, it might be better to use a richer domain model for the attributes to ensure the users choice is clear.\n\nThe domain model might even provide a way to add itself to read / write the model to the attributes. For example, something like this:\n\n```java\npublic class BasicAuthenticationCredential implements Consumer<Map<String,Object>> {\n\tprivate static String ATTRIBUTE_NAME = BasicAuthenticationCredential.class.getName().concat(\".ATTRIBUTE_NAME\");\n\n\tprivate final String username;\n\tprivate final String password;\n\n\n\tBasicAuthenticationCredential(String username, String password) {\n\t\tthis.username = username;\n\t\tthis.password = password;\n\t}\n\n\tpublic String getUsername() {\n\t\treturn username;\n\t}\n\n\tpublic String getPassword() {\n\t\treturn password;\n\t}\n\n\t@Override\n\tpublic void accept(Map<String, Object> attributes) {\n\t\tattributes.put(ATTRIBUTE_NAME, this);\n\t}\n\n\tpublic static BasicAuthenticationCredential get(Map<String,Object> attributes) {\n\t\treturn (BasicAuthenticationCredential) attributes.get(ATTRIBUTE_NAME);\n\t}\n}\n```\n\nConsumers would then be able to use:\n\n```java\nclient.get()\n\t\t.uri(\"/messages/20\")\n\t\t// perhaps add static factory method for BasicAuthenticationCredential\n\t\t.attributes(new BasicAuthenticationCredential(\"joe\", \"joe\"))\n```\n\nComparing that vs:\n\n```java\nclient.get()\n\t.uri(\"/messages/1\")\n\t.attribute(ExchangeFilterFunctions.USERNAME_ATTRIBUTE, \"rob\")\n\t.attribute(ExchangeFilterFunctions.PASSWORD_ATTRIBUTE, \"rob\")\n```\n\nPlus now we would be able to differentiate between the two credential types because we would add a `DigestAuthenticationCredential` if we supported it later on.\n\n\n---\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1d86c9c3d128f64afa51460e3ed7900963755cfd\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453458442","453458444","453458446"], "labels":["in: web","type: enhancement"]}