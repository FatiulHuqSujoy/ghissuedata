{"id":"14746", "title":"Add support for asserting JSON in ContentResultMatchers, just as there is for XML [SPR-10113]", "body":"**[Adarsh Ramamurthy](https://jira.spring.io/secure/ViewProfile.jspa?name=radarsh)** opened **[SPR-10113](https://jira.spring.io/browse/SPR-10113?redirect=false)** and commented

The `ContentResultMatchers` class should support asserting JSON strings directly. Using `jsonPath` isn't always helpful.

This is what I have done as a temporary workaround.

```
public class JsonAwareContentResultMatchers extends ContentResultMatchers {

    private final JsonExpectationsHelper jsonHelper = new JsonExpectationsHelper();

    public ResultMatcher json(final String jsonContent) {
        return new ResultMatcher() {

            @Override
            public void match(MvcResult result) throws Exception {
                String content = result.getResponse().getContentAsString();
                jsonHelper.assertJsonEqual(jsonContent, content);
            }
        };
    }
}
```

```
import org.skyscreamer.jsonassert.JSONAssert;

public class JsonExpectationsHelper {

    public void assertJsonEqual(String expected, String actual) throws Exception {
        JSONAssert.assertEquals(expected, actual, false);
    }
}
```

```
public static JsonAwareContentResultMatchers content2() {
    return new JsonAwareContentResultMatchers();
}
```

### Usage:

```
.andExpect(status().isOk())
.andExpect(content2().contentType(\"application/json\"))
.andExpect(content2().json(\"{\\\"message\\\":\\\"Hello\\\"}\"))
```

---

**Affects:** 3.2 GA

**Issue Links:**
- #18493 Add support for asserting JSON in ContentRequestMatchers, just as there is for XML

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/9e52004222addfe6498ed9eb6d8d8ad926ce21ea

1 votes, 7 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14746","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14746/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14746/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14746/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14746","id":398155821,"node_id":"MDU6SXNzdWUzOTgxNTU4MjE=","number":14746,"title":"Add support for asserting JSON in ContentResultMatchers, just as there is for XML [SPR-10113]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/122","html_url":"https://github.com/spring-projects/spring-framework/milestone/122","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/122/labels","id":3960895,"node_id":"MDk6TWlsZXN0b25lMzk2MDg5NQ==","number":122,"title":"4.1 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":151,"state":"closed","created_at":"2019-01-10T22:04:18Z","updated_at":"2019-01-11T08:19:09Z","due_on":"2014-07-20T07:00:00Z","closed_at":"2019-01-10T22:04:18Z"},"comments":3,"created_at":"2012-12-20T06:42:57Z","updated_at":"2019-01-11T21:25:42Z","closed_at":"2016-02-05T15:36:24Z","author_association":"COLLABORATOR","body":"**[Adarsh Ramamurthy](https://jira.spring.io/secure/ViewProfile.jspa?name=radarsh)** opened **[SPR-10113](https://jira.spring.io/browse/SPR-10113?redirect=false)** and commented\n\nThe `ContentResultMatchers` class should support asserting JSON strings directly. Using `jsonPath` isn't always helpful.\n\nThis is what I have done as a temporary workaround.\n\n```\npublic class JsonAwareContentResultMatchers extends ContentResultMatchers {\n\n    private final JsonExpectationsHelper jsonHelper = new JsonExpectationsHelper();\n\n    public ResultMatcher json(final String jsonContent) {\n        return new ResultMatcher() {\n\n            @Override\n            public void match(MvcResult result) throws Exception {\n                String content = result.getResponse().getContentAsString();\n                jsonHelper.assertJsonEqual(jsonContent, content);\n            }\n        };\n    }\n}\n```\n\n```\nimport org.skyscreamer.jsonassert.JSONAssert;\n\npublic class JsonExpectationsHelper {\n\n    public void assertJsonEqual(String expected, String actual) throws Exception {\n        JSONAssert.assertEquals(expected, actual, false);\n    }\n}\n```\n\n```\npublic static JsonAwareContentResultMatchers content2() {\n    return new JsonAwareContentResultMatchers();\n}\n```\n\n### Usage:\n\n```\n.andExpect(status().isOk())\n.andExpect(content2().contentType(\"application/json\"))\n.andExpect(content2().json(\"{\\\"message\\\":\\\"Hello\\\"}\"))\n```\n\n---\n\n**Affects:** 3.2 GA\n\n**Issue Links:**\n- #18493 Add support for asserting JSON in ContentRequestMatchers, just as there is for XML\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/9e52004222addfe6498ed9eb6d8d8ad926ce21ea\n\n1 votes, 7 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453399641","453399642","453399643"], "labels":["in: test","type: enhancement"]}