{"id":"11998", "title":"Minor content issues and typos in Spring Reference Documentation [SPR-7339]", "body":"**[David M. Karr](https://jira.spring.io/secure/ViewProfile.jspa?name=dkarr)** opened **[SPR-7339](https://jira.spring.io/browse/SPR-7339?redirect=false)** and commented

First, some minor content issues based on my opinion, then the pure typos.

p. 187, bullet for \"Join point\".  This ends with a statement about join points in Spring AOP always representing a method execution.  This might be a good point to add something like \"AspectJ provides other options\".  This is talked about in other sections, but it might be worth mentioning it here.

p. 306, the use of the word \"tropes\" is correct, but obscure.  The vast majority of people reading this won't know that word.

p. 333, the statement \"Any DAO or repository need to access to a persistence resource, depending on the persistence technology used.\" is messed up, but I don't know exactly what you meant to say.

p. 340.  In the \"CorporateEventDao\" example at the bottom of the page, I would have expected this to have a \"`@Repository`\" annotation.

p. 444, one paragraph ends with \"If they do not exist, Spring throws an Exception.\".  I think a more specific exception class was intended to be cited here, instead of just \"Exception\".  It's in code font, so it's obviously referring to a specific class.

p. 473, the bullet \"path=\"lastName*\" - displays all errors associated with the lastName field\" is confusing.  My impression was that just \"lastName\" would do this.  I don't know what \"lastName*\" would do.

p. 549 and the entire \"Exposing web services using XFire\" subsection of section 19.5: The XFire framework is obsolete.  It has been for more than 3 years now, and it even SAYS SO right on the front page of the XFire website, and that Apache CXF should be considered to be its replacement.  Apache CXF integrates very well with Spring, and is a very full implementation of various web services protocols and specifications.

p. 556, first paragraph of section 19.6.  This seems to imply that JMS support in Spring \"is pretty basic\" and is NOT transactional.  Chapter 21 pretty well refutes this, but this statement here seems to be misleading.

p. 561 has the statement \"... you can simply call the default constructor.\" This is a common miswording. It would be better as \"... you can simply call the no-arguments constructor.\".

p. 563, the \"HttpMessageConverter\" code sample. This has several minor violations of the common recommendations for method comments, in that the description should be in third person.  For instance, instead of \"Indicate whether the ...\" it should be \"Indicates whether the ...\".  In addition, in one of the comments, change \"... given type form ...\" to \"... given type from ...\".

p. 634, the phrase \"... without recourse to having to know Java.\" is very odd. Perhaps it would be better as \"... without having to use Java.\"

Concerning this last chapter 27, what is the point of it? This material is all repeated in other chapters, including the \"`@Required`\" annotation, which is the only annotation detailed in this chapter.

The following are pure typos or wordos:

p. 35, change \"defintions\" to \"definitions\" and change \"... can use of the ...\" to \"... can use the ...\".

p. 54, remove spurious footnote marker of \"2\" on paragraph beginning \"When using XML-based ...\".

p. 88, change \"... instead on angle-bracket ...\" to \"... instead of angle-bracket ...\" and change \"... using annotations on relevant ...\" to \"... using annotations on the relevant ...\".

p. 192, Note on \"Other pointcut types\".  Change \"... releases both to ...\" to \"... releases to ...\".

p. 214, change \"... may also be calling ...\" to \"... may also be called ...\".

p. 300, change \"The related The PlatformTransactionManager ...\" to \"The related PlatformTransactionManager ...\".

p. 304, change \"With EJB CMT, cannot ...\" to \"With EJB CMT, you cannot ...\".

p. 321, change \"... for each method that to which ...\" to \"... for each method for which ...\".

p. 325, change \"asepctj\" to \"aspectj\".

p. 345.  Change \"Any a custom ...\" to \"Any custom ...\".

p. 358, change \"StoredProcdedure\" to \"StoredProcedure\".

p. 365, change \"... query the returned a ...\" to \"... query that returned a ...\".

p. 370, change \"LobCreatorclass\" to \"LobCreator class\".

p. 371, change \"setBlobAsBinartStream\" to \"setBlobAsBinaryStream\" and change \"JdbcTempate\" to \"JdbcTemplate\".

p. 375, change \"... use the initialize-datasource tag ...\" to \"... use the initialize-database tag ...\".

p. 375, change \"... more control the creation and deletion of existing data the XML ...\" to \"... more control over the creation and deletion of existing data, the XML ...\" (over and comma).

p. 397, change \"instancebased\" to \"instance based\".

p. 398, change \"Refer to the section the section called ...\" to \"Refer to the section called ...\".

p. 429, change \"... you do have not debugging ...\" to \"... you do not have debugging ...\".

p. 433, change \"... object to access to the Servlet ...\" to \"... object to provide access to the Servlet ...\".

p. 447, change \"Context-Type\" to \"Content-Type\".

p. 453, the phrase \"normal annotated `@Controller`\" is highlighted blue, which I think is wrong.

p. 454, change \"... handling exception gives ...\" to \"... handling exceptions gives ...\".

p. 463, change \"... mainly cause by ...\" to \"... mainly caused by ...\".

p. 474, change \"... For each methods, ...\" to \"... For each method, ...\".

p. 477, change \"... two templating languages that can both be used ...\" to \"... two templating languages that can be used ...\".

p. 477, change \"... commons-collections.jar needs also to be available for Velocity.\" to \"... commons-collections.jar is required for Velocity.\"

p. 498, change \"MarhsallingView\" to \"MarshallingView\" in two places, and change \"marhsall\" to \"marshall\".

p. 546, change \"... for a service object much resembles the way you would do using Hessian ...\" to \"... for a service much resembles the way you would do the same using Hessian ...\".

p. 560, change \"unmarshall\" to \"unmarshal\".

p. 569, change \"... call unloadBeanFactory() and loadBeanFactory from ejbPassivate and
ejbActivate ...\" to \"... call unloadBeanFactory() and loadBeanFactory() from ejbPassivate() and
ejbActivate() ...\".  Also change \"... ContextJndiBeanFactoryLocator classes ...\" to \"... ContextJndiBeanFactoryLocator class ...\".

p. 577, change \"... constructor and connectionFactory / is ...\" to \"... constructor and connectionFactory is ...\". Also change \"... lets you send to a message ...\" to \"... lets you send a message ...\".

p. 578, change \"... popular implementations choices ...\" to \"... popular implementation choices ...\".

p. 579, change \"... Session / MessageProducer pair respectfully.\" to \"... Session / MessageProducer pair respectively.\".

p. 580, change \"... provides a similar contract the JMS ...\" to \"... provides a similar contract to the JMS ...\".

p. 597, change \"shouws\" to \"shows\".

p. 632, change \"... would be be images ...\" to \"... would be images ...\".

p. 633, change \"... The code in the previous examples explicitly has been creating the ...\" to \"... The code in the previous examples explicitly created the ...\". Also change \"... and Velocity becomes ...\" to \"... and Velocity it becomes ...\".

p. 647, change \"... allows to you invoke ...\" to \"... allows you to invoke ...\".

p. 653, change \"... no different to normal ...\" to \"... no different from normal ...\".

p. 667, change \"annoations\" to \"annotations\", \"embrassed\" to \"embraced\", \"thoughout\" to \"throughout\", and \"... other parts the ...\" to \"... other parts of the ...\".

---

**Affects:** 3.0.3

**Issue Links:**
- #12092 Remove XFire reference documentation
- #12091 Remove Chapter 27 from reference documentation

1 votes, 0 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11998","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11998/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11998/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11998/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11998","id":398106016,"node_id":"MDU6SXNzdWUzOTgxMDYwMTY=","number":11998,"title":"Minor content issues and typos in Spring Reference Documentation [SPR-7339]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511868,"node_id":"MDU6TGFiZWwxMTg4NTExODY4","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20documentation","name":"type: documentation","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/71","html_url":"https://github.com/spring-projects/spring-framework/milestone/71","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/71/labels","id":3960844,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0NA==","number":71,"title":"3.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":74,"state":"closed","created_at":"2019-01-10T22:03:17Z","updated_at":"2019-01-11T02:52:15Z","due_on":"2010-08-17T07:00:00Z","closed_at":"2019-01-10T22:03:17Z"},"comments":5,"created_at":"2010-06-30T02:56:57Z","updated_at":"2019-01-14T06:14:25Z","closed_at":"2012-06-19T03:42:56Z","author_association":"COLLABORATOR","body":"**[David M. Karr](https://jira.spring.io/secure/ViewProfile.jspa?name=dkarr)** opened **[SPR-7339](https://jira.spring.io/browse/SPR-7339?redirect=false)** and commented\n\nFirst, some minor content issues based on my opinion, then the pure typos.\n\np. 187, bullet for \"Join point\".  This ends with a statement about join points in Spring AOP always representing a method execution.  This might be a good point to add something like \"AspectJ provides other options\".  This is talked about in other sections, but it might be worth mentioning it here.\n\np. 306, the use of the word \"tropes\" is correct, but obscure.  The vast majority of people reading this won't know that word.\n\np. 333, the statement \"Any DAO or repository need to access to a persistence resource, depending on the persistence technology used.\" is messed up, but I don't know exactly what you meant to say.\n\np. 340.  In the \"CorporateEventDao\" example at the bottom of the page, I would have expected this to have a \"`@Repository`\" annotation.\n\np. 444, one paragraph ends with \"If they do not exist, Spring throws an Exception.\".  I think a more specific exception class was intended to be cited here, instead of just \"Exception\".  It's in code font, so it's obviously referring to a specific class.\n\np. 473, the bullet \"path=\"lastName*\" - displays all errors associated with the lastName field\" is confusing.  My impression was that just \"lastName\" would do this.  I don't know what \"lastName*\" would do.\n\np. 549 and the entire \"Exposing web services using XFire\" subsection of section 19.5: The XFire framework is obsolete.  It has been for more than 3 years now, and it even SAYS SO right on the front page of the XFire website, and that Apache CXF should be considered to be its replacement.  Apache CXF integrates very well with Spring, and is a very full implementation of various web services protocols and specifications.\n\np. 556, first paragraph of section 19.6.  This seems to imply that JMS support in Spring \"is pretty basic\" and is NOT transactional.  Chapter 21 pretty well refutes this, but this statement here seems to be misleading.\n\np. 561 has the statement \"... you can simply call the default constructor.\" This is a common miswording. It would be better as \"... you can simply call the no-arguments constructor.\".\n\np. 563, the \"HttpMessageConverter\" code sample. This has several minor violations of the common recommendations for method comments, in that the description should be in third person.  For instance, instead of \"Indicate whether the ...\" it should be \"Indicates whether the ...\".  In addition, in one of the comments, change \"... given type form ...\" to \"... given type from ...\".\n\np. 634, the phrase \"... without recourse to having to know Java.\" is very odd. Perhaps it would be better as \"... without having to use Java.\"\n\nConcerning this last chapter 27, what is the point of it? This material is all repeated in other chapters, including the \"`@Required`\" annotation, which is the only annotation detailed in this chapter.\n\nThe following are pure typos or wordos:\n\np. 35, change \"defintions\" to \"definitions\" and change \"... can use of the ...\" to \"... can use the ...\".\n\np. 54, remove spurious footnote marker of \"2\" on paragraph beginning \"When using XML-based ...\".\n\np. 88, change \"... instead on angle-bracket ...\" to \"... instead of angle-bracket ...\" and change \"... using annotations on relevant ...\" to \"... using annotations on the relevant ...\".\n\np. 192, Note on \"Other pointcut types\".  Change \"... releases both to ...\" to \"... releases to ...\".\n\np. 214, change \"... may also be calling ...\" to \"... may also be called ...\".\n\np. 300, change \"The related The PlatformTransactionManager ...\" to \"The related PlatformTransactionManager ...\".\n\np. 304, change \"With EJB CMT, cannot ...\" to \"With EJB CMT, you cannot ...\".\n\np. 321, change \"... for each method that to which ...\" to \"... for each method for which ...\".\n\np. 325, change \"asepctj\" to \"aspectj\".\n\np. 345.  Change \"Any a custom ...\" to \"Any custom ...\".\n\np. 358, change \"StoredProcdedure\" to \"StoredProcedure\".\n\np. 365, change \"... query the returned a ...\" to \"... query that returned a ...\".\n\np. 370, change \"LobCreatorclass\" to \"LobCreator class\".\n\np. 371, change \"setBlobAsBinartStream\" to \"setBlobAsBinaryStream\" and change \"JdbcTempate\" to \"JdbcTemplate\".\n\np. 375, change \"... use the initialize-datasource tag ...\" to \"... use the initialize-database tag ...\".\n\np. 375, change \"... more control the creation and deletion of existing data the XML ...\" to \"... more control over the creation and deletion of existing data, the XML ...\" (over and comma).\n\np. 397, change \"instancebased\" to \"instance based\".\n\np. 398, change \"Refer to the section the section called ...\" to \"Refer to the section called ...\".\n\np. 429, change \"... you do have not debugging ...\" to \"... you do not have debugging ...\".\n\np. 433, change \"... object to access to the Servlet ...\" to \"... object to provide access to the Servlet ...\".\n\np. 447, change \"Context-Type\" to \"Content-Type\".\n\np. 453, the phrase \"normal annotated `@Controller`\" is highlighted blue, which I think is wrong.\n\np. 454, change \"... handling exception gives ...\" to \"... handling exceptions gives ...\".\n\np. 463, change \"... mainly cause by ...\" to \"... mainly caused by ...\".\n\np. 474, change \"... For each methods, ...\" to \"... For each method, ...\".\n\np. 477, change \"... two templating languages that can both be used ...\" to \"... two templating languages that can be used ...\".\n\np. 477, change \"... commons-collections.jar needs also to be available for Velocity.\" to \"... commons-collections.jar is required for Velocity.\"\n\np. 498, change \"MarhsallingView\" to \"MarshallingView\" in two places, and change \"marhsall\" to \"marshall\".\n\np. 546, change \"... for a service object much resembles the way you would do using Hessian ...\" to \"... for a service much resembles the way you would do the same using Hessian ...\".\n\np. 560, change \"unmarshall\" to \"unmarshal\".\n\np. 569, change \"... call unloadBeanFactory() and loadBeanFactory from ejbPassivate and\nejbActivate ...\" to \"... call unloadBeanFactory() and loadBeanFactory() from ejbPassivate() and\nejbActivate() ...\".  Also change \"... ContextJndiBeanFactoryLocator classes ...\" to \"... ContextJndiBeanFactoryLocator class ...\".\n\np. 577, change \"... constructor and connectionFactory / is ...\" to \"... constructor and connectionFactory is ...\". Also change \"... lets you send to a message ...\" to \"... lets you send a message ...\".\n\np. 578, change \"... popular implementations choices ...\" to \"... popular implementation choices ...\".\n\np. 579, change \"... Session / MessageProducer pair respectfully.\" to \"... Session / MessageProducer pair respectively.\".\n\np. 580, change \"... provides a similar contract the JMS ...\" to \"... provides a similar contract to the JMS ...\".\n\np. 597, change \"shouws\" to \"shows\".\n\np. 632, change \"... would be be images ...\" to \"... would be images ...\".\n\np. 633, change \"... The code in the previous examples explicitly has been creating the ...\" to \"... The code in the previous examples explicitly created the ...\". Also change \"... and Velocity becomes ...\" to \"... and Velocity it becomes ...\".\n\np. 647, change \"... allows to you invoke ...\" to \"... allows you to invoke ...\".\n\np. 653, change \"... no different to normal ...\" to \"... no different from normal ...\".\n\np. 667, change \"annoations\" to \"annotations\", \"embrassed\" to \"embraced\", \"thoughout\" to \"throughout\", and \"... other parts the ...\" to \"... other parts of the ...\".\n\n---\n\n**Affects:** 3.0.3\n\n**Issue Links:**\n- #12092 Remove XFire reference documentation\n- #12091 Remove Chapter 27 from reference documentation\n\n1 votes, 0 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453351928","453351929","453351930","453351931","453351932"], "labels":["type: documentation"]}