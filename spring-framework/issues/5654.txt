{"id":"5654", "title":"Want to bind input in some attribute source to a backing form object? You want to be able to take into account multiple attribute sources in a configurable order? [SPR-934]", "body":"**[Keith Garry Boyce](https://jira.spring.io/secure/ViewProfile.jspa?name=garpinc)** opened **[SPR-934](https://jira.spring.io/browse/SPR-934?redirect=false)** and commented

Now what I want is as follows.
Attributes exist in session which apply to an object that may or may not exists in the scope I want. Similar to above I want the object of the specified class to be created if it doesn't exist in the scope I want and it's values to be populated from the session rather than request. Better yet rather than it explicitly being populated from request as above I would like something like:

Code:
\\<bean id=\"person.Search.criteria.formAction\"
class=\"org.springframework.web.flow.action.FormAction\">
\\<property name=\"populateFrom\" value=\"request,session,flow\"/>
\\<property name=\"formObjectName\" value=\"query\"/>
\\<property name=\"formObjectClass\" value=\"org.springframework.samples.phonebook.domain.PhoneBookQuery\"/>
\\<property name=\"formObjectScopeAsString\" value=\"flow\"/>
\\<property name=\"validator\">
\\<bean id=\"queryValidator\" class=\"org.springframework.samples.phonebook.domain.PhoneBookQueryValidator\"/>
\\</property>
\\</bean>

See \\<property name=\"populateFrom\" value=\"request,session\"/>

That way it will try to populate from request first and then from session or in what ever order I specify with the first in the order taking precedent if it exists in both places.

Then

Code:
PhoneBookQuery query = (PhoneBookQuery)context.getFlowScope().getAttribute(\"query\");

---

Here is the code I have so far to do resolution. It should be pretty easy to use reflection to bind objects using getObject to resolve object (BTW: maybe it should be renamed to resolveObject). Hopefully it will get the ball rolling...

package org.springframework.web.flow.action;

import org.springframework.web.flow.RequestContext;

public interface AttributeResolver {
Object getObject(String objectName, RequestContext context);

    void setAttributeSourceChain(String attributeSourceChain);

}

---

package org.springframework.web.flow.action;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.flow.RequestContext;

public class MultiActionExtended extends MultiAction implements InitializingBean {
private String attributeSourceChain;
private Map attributeResolvers = new HashMap();
private AttributeResolver DEFAULT_ATTRIBUTE_RESOLVER = new DefaultAttributeResolverImpl();

    /**
     * @param attributeResolvers The attributeResolvers to set.
     */
    public void setAttributeResolvers(Map attributeResolvers) {
    	this.attributeResolvers = attributeResolvers;
    }
    /**
     * @param attributeSourceChain The attributeSourceChain to set.
     */
    public void setAttributeSourceChain(String attributeSourceChain) {
    	this.attributeSourceChain = attributeSourceChain;
    }
    public Object getObject(String objectName, RequestContext context)  {
    	Object attributeResolverObj = attributeResolvers.get(objectName);
    	if (attributeResolverObj != null) {
    		AttributeResolver attributeResolver;
    		if (attributeResolverObj instanceof String) {
    			try {
    				attributeResolver = (AttributeResolver) Class.forName((String) attributeResolverObj).newInstance();
    			} catch (InstantiationException e) {
    				throw new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());
    			} catch (IllegalAccessException e) {
    				throw new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());
    			} catch (ClassNotFoundException e) {
    				throw new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());
    			}
    		} else {
    			attributeResolver = (AttributeResolver) attributeResolverObj;
    		}
    
    		return attributeResolver.getObject(objectName,context);
    	} else {
    		return DEFAULT_ATTRIBUTE_RESOLVER.getObject(objectName,context);
    	}
    	
    	
    }
    
    public void afterPropertiesSet() {
    	super.afterPropertiesSet();
    	if (attributeSourceChain != null) {
    		DEFAULT_ATTRIBUTE_RESOLVER.setAttributeSourceChain(attributeSourceChain);
    	}
    }

}

---

package org.springframework.web.flow.action;

import org.springframework.util.StringUtils;
import org.springframework.web.flow.RequestContext;
import org.springframework.web.flow.execution.portlet.PortletRequestEvent;
import org.springframework.web.portlet.context.support.PortletWebApplicationContextUtils;

public class DefaultAttributeResolverImpl implements AttributeResolver {
private String attributeSourceChain = \"parameter,request,flow,session,context\";
private static final Object SESSION = \"session\";
private static final Object REQUEST = \"request\";
private static final String FLOW = \"flow\";
private static final Object PARAMETER = \"parameter\";
private static final Object CONTEXT = \"context\";

    /**
     * @param attributeSourceChain The attributeSourceChain to set.
     */
    public void setAttributeSourceChain(String attributeSourceChain) {
    	this.attributeSourceChain = attributeSourceChain;
    }
    
    public Object getObject(String objectName, RequestContext context) {
    	String[] attributeSourceChainSet = StringUtils.commaDelimitedListToStringArray(attributeSourceChain);
    	PortletRequestEvent event = (PortletRequestEvent)context.getOriginatingEvent();
    	for (int i = 0; i < attributeSourceChainSet.length; i++) {
    		String attributeSource = attributeSourceChainSet[i];
    		
    		//TODO: should use typesafe enum from ScopeType
    		Object obj;
    		if (attributeSource.equals(SESSION)) {
    	        obj = event.getRequest().getPortletSession().getAttribute(objectName);
    		} else if (attributeSource.equals(REQUEST)) {
    			obj = context.getRequestScope().getAttribute(objectName);
    		} else if (attributeSource.equals(FLOW)) {
    			obj = context.getFlowScope().getAttribute(objectName);
    		} else if (attributeSource.equals(PARAMETER)) {
    			obj = event.getParameter(objectName);
    		} else if (attributeSource.equals(CONTEXT)) {
    			obj = PortletWebApplicationContextUtils.getWebApplicationContext(event.getRequest().getPortletSession().getPortletContext()).getBean(objectName);
    		} else {
    			throw new RuntimeException(\"TODO: What should happen if scope is invalid\");
    		}
    		if (obj != null) {
    			return obj;
    		}
    		
    	}
    	throw new RuntimeException(\"TODO: determine what happens if you don't find object in any scope\");
    	
    }

}

---

**Affects:** 2.0 M1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5654","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5654/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5654/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5654/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5654","id":398056816,"node_id":"MDU6SXNzdWUzOTgwNTY4MTY=","number":5654,"title":"Want to bind input in some attribute source to a backing form object? You want to be able to take into account multiple attribute sources in a configurable order? [SPR-934]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2005-05-10T04:08:51Z","updated_at":"2019-01-13T22:51:45Z","closed_at":"2012-06-19T09:37:31Z","author_association":"COLLABORATOR","body":"**[Keith Garry Boyce](https://jira.spring.io/secure/ViewProfile.jspa?name=garpinc)** opened **[SPR-934](https://jira.spring.io/browse/SPR-934?redirect=false)** and commented\n\nNow what I want is as follows.\nAttributes exist in session which apply to an object that may or may not exists in the scope I want. Similar to above I want the object of the specified class to be created if it doesn't exist in the scope I want and it's values to be populated from the session rather than request. Better yet rather than it explicitly being populated from request as above I would like something like:\n\nCode:\n\\<bean id=\"person.Search.criteria.formAction\"\nclass=\"org.springframework.web.flow.action.FormAction\">\n\\<property name=\"populateFrom\" value=\"request,session,flow\"/>\n\\<property name=\"formObjectName\" value=\"query\"/>\n\\<property name=\"formObjectClass\" value=\"org.springframework.samples.phonebook.domain.PhoneBookQuery\"/>\n\\<property name=\"formObjectScopeAsString\" value=\"flow\"/>\n\\<property name=\"validator\">\n\\<bean id=\"queryValidator\" class=\"org.springframework.samples.phonebook.domain.PhoneBookQueryValidator\"/>\n\\</property>\n\\</bean>\n\nSee \\<property name=\"populateFrom\" value=\"request,session\"/>\n\nThat way it will try to populate from request first and then from session or in what ever order I specify with the first in the order taking precedent if it exists in both places.\n\nThen\n\nCode:\nPhoneBookQuery query = (PhoneBookQuery)context.getFlowScope().getAttribute(\"query\");\n\n---\n\nHere is the code I have so far to do resolution. It should be pretty easy to use reflection to bind objects using getObject to resolve object (BTW: maybe it should be renamed to resolveObject). Hopefully it will get the ball rolling...\n\npackage org.springframework.web.flow.action;\n\nimport org.springframework.web.flow.RequestContext;\n\npublic interface AttributeResolver {\nObject getObject(String objectName, RequestContext context);\n\n    void setAttributeSourceChain(String attributeSourceChain);\n\n}\n\n---\n\npackage org.springframework.web.flow.action;\n\nimport java.util.HashMap;\nimport java.util.Map;\n\nimport org.springframework.beans.factory.InitializingBean;\nimport org.springframework.web.flow.RequestContext;\n\npublic class MultiActionExtended extends MultiAction implements InitializingBean {\nprivate String attributeSourceChain;\nprivate Map attributeResolvers = new HashMap();\nprivate AttributeResolver DEFAULT_ATTRIBUTE_RESOLVER = new DefaultAttributeResolverImpl();\n\n    /**\n     * @param attributeResolvers The attributeResolvers to set.\n     */\n    public void setAttributeResolvers(Map attributeResolvers) {\n    \tthis.attributeResolvers = attributeResolvers;\n    }\n    /**\n     * @param attributeSourceChain The attributeSourceChain to set.\n     */\n    public void setAttributeSourceChain(String attributeSourceChain) {\n    \tthis.attributeSourceChain = attributeSourceChain;\n    }\n    public Object getObject(String objectName, RequestContext context)  {\n    \tObject attributeResolverObj = attributeResolvers.get(objectName);\n    \tif (attributeResolverObj != null) {\n    \t\tAttributeResolver attributeResolver;\n    \t\tif (attributeResolverObj instanceof String) {\n    \t\t\ttry {\n    \t\t\t\tattributeResolver = (AttributeResolver) Class.forName((String) attributeResolverObj).newInstance();\n    \t\t\t} catch (InstantiationException e) {\n    \t\t\t\tthrow new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());\n    \t\t\t} catch (IllegalAccessException e) {\n    \t\t\t\tthrow new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());\n    \t\t\t} catch (ClassNotFoundException e) {\n    \t\t\t\tthrow new RuntimeException(\"TODO: decide how to handle this:\" + e.toString());\n    \t\t\t}\n    \t\t} else {\n    \t\t\tattributeResolver = (AttributeResolver) attributeResolverObj;\n    \t\t}\n    \n    \t\treturn attributeResolver.getObject(objectName,context);\n    \t} else {\n    \t\treturn DEFAULT_ATTRIBUTE_RESOLVER.getObject(objectName,context);\n    \t}\n    \t\n    \t\n    }\n    \n    public void afterPropertiesSet() {\n    \tsuper.afterPropertiesSet();\n    \tif (attributeSourceChain != null) {\n    \t\tDEFAULT_ATTRIBUTE_RESOLVER.setAttributeSourceChain(attributeSourceChain);\n    \t}\n    }\n\n}\n\n---\n\npackage org.springframework.web.flow.action;\n\nimport org.springframework.util.StringUtils;\nimport org.springframework.web.flow.RequestContext;\nimport org.springframework.web.flow.execution.portlet.PortletRequestEvent;\nimport org.springframework.web.portlet.context.support.PortletWebApplicationContextUtils;\n\npublic class DefaultAttributeResolverImpl implements AttributeResolver {\nprivate String attributeSourceChain = \"parameter,request,flow,session,context\";\nprivate static final Object SESSION = \"session\";\nprivate static final Object REQUEST = \"request\";\nprivate static final String FLOW = \"flow\";\nprivate static final Object PARAMETER = \"parameter\";\nprivate static final Object CONTEXT = \"context\";\n\n    /**\n     * @param attributeSourceChain The attributeSourceChain to set.\n     */\n    public void setAttributeSourceChain(String attributeSourceChain) {\n    \tthis.attributeSourceChain = attributeSourceChain;\n    }\n    \n    public Object getObject(String objectName, RequestContext context) {\n    \tString[] attributeSourceChainSet = StringUtils.commaDelimitedListToStringArray(attributeSourceChain);\n    \tPortletRequestEvent event = (PortletRequestEvent)context.getOriginatingEvent();\n    \tfor (int i = 0; i < attributeSourceChainSet.length; i++) {\n    \t\tString attributeSource = attributeSourceChainSet[i];\n    \t\t\n    \t\t//TODO: should use typesafe enum from ScopeType\n    \t\tObject obj;\n    \t\tif (attributeSource.equals(SESSION)) {\n    \t        obj = event.getRequest().getPortletSession().getAttribute(objectName);\n    \t\t} else if (attributeSource.equals(REQUEST)) {\n    \t\t\tobj = context.getRequestScope().getAttribute(objectName);\n    \t\t} else if (attributeSource.equals(FLOW)) {\n    \t\t\tobj = context.getFlowScope().getAttribute(objectName);\n    \t\t} else if (attributeSource.equals(PARAMETER)) {\n    \t\t\tobj = event.getParameter(objectName);\n    \t\t} else if (attributeSource.equals(CONTEXT)) {\n    \t\t\tobj = PortletWebApplicationContextUtils.getWebApplicationContext(event.getRequest().getPortletSession().getPortletContext()).getBean(objectName);\n    \t\t} else {\n    \t\t\tthrow new RuntimeException(\"TODO: What should happen if scope is invalid\");\n    \t\t}\n    \t\tif (obj != null) {\n    \t\t\treturn obj;\n    \t\t}\n    \t\t\n    \t}\n    \tthrow new RuntimeException(\"TODO: determine what happens if you don't find object in any scope\");\n    \t\n    }\n\n}\n\n---\n\n**Affects:** 2.0 M1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453294022","453294023"], "labels":["status: declined","type: enhancement"]}