{"id":"19575", "title":"@JsonIgnoreProperties(ignoreUnknown=false) is not working in Spring 4.2.8 and upper version [SPR-15008]", "body":"**[Masbha Uddin Ahmed](https://jira.spring.io/secure/ViewProfile.jspa?name=masbha)** opened **[SPR-15008](https://jira.spring.io/browse/SPR-15008?redirect=false)** and commented

Recently we have increased our Spring version from 4.0.4 to 4.2.8 and this we have done in order to support MongoDB upgrade to 3.2.x version. After upgrading to latest spring version we have noticed, in all Rest Api's if we pass some invalid inputs along with valid input fields, requests are still getting through and getting 200 response code even though it has invalid fields.

This was working fine in previous Spring version of 4.0.4, where it will throw malformed syntax with 400 response code.

Somehow with recent upgrades of Spring, this feature is not working anymore. We have tried different Spring version in 4.2.x and 4.3.x series, still it is not working as expected.

Please find the example below for understanding on this issue. We need a solution or workaround on this issue as we couldn't figure out what is causing the issue.

Also could you confirm is this a bug in Spring or are we missing anything with Spring upgrade.

`@JsonIgnoreProperties`(ignoreUnknown=false) is not working with spring 4.2.0 and upper version of spring. But it is working with 4.0.4 and 4.0.1  .

Spring and Jackson dependency we have used,

```java

              <dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.6.3</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>2.6.3</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>2.6.3</version>
		</dependency> 
               <dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>4.2.8.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>4.2.8.RELEASE</version>
		</dependency>

```

If I send json request with invalid fields then it is accepting as a valid request. But it should give the bad request response.

For example:
If I have class

public class Student {
private String id;
private String name;
}

If send valid corresponding json request it should be like

{
\"id\": \"123\",
\"name\": \"test\"
}

But even if I send json request with invalid fields like below it is still accepting.

{
\"id\": \"123\",
\"name\": \"test\",
\"anyinvalidkey\": \"test\"
}

---

**Affects:** 4.2.4, 4.2.8

**Issue Links:**
- #16510 Set Jackson FAIL_ON_UNKNOWN_PROPERTIES property to false by default

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19575","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19575/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19575/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19575/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19575","id":398202154,"node_id":"MDU6SXNzdWUzOTgyMDIxNTQ=","number":19575,"title":"@JsonIgnoreProperties(ignoreUnknown=false) is not working in Spring 4.2.8 and upper version [SPR-15008]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2016-12-12T21:53:44Z","updated_at":"2019-01-11T15:21:47Z","closed_at":"2018-04-03T08:28:21Z","author_association":"COLLABORATOR","body":"**[Masbha Uddin Ahmed](https://jira.spring.io/secure/ViewProfile.jspa?name=masbha)** opened **[SPR-15008](https://jira.spring.io/browse/SPR-15008?redirect=false)** and commented\n\nRecently we have increased our Spring version from 4.0.4 to 4.2.8 and this we have done in order to support MongoDB upgrade to 3.2.x version. After upgrading to latest spring version we have noticed, in all Rest Api's if we pass some invalid inputs along with valid input fields, requests are still getting through and getting 200 response code even though it has invalid fields.\n\nThis was working fine in previous Spring version of 4.0.4, where it will throw malformed syntax with 400 response code.\n\nSomehow with recent upgrades of Spring, this feature is not working anymore. We have tried different Spring version in 4.2.x and 4.3.x series, still it is not working as expected.\n\nPlease find the example below for understanding on this issue. We need a solution or workaround on this issue as we couldn't figure out what is causing the issue.\n\nAlso could you confirm is this a bug in Spring or are we missing anything with Spring upgrade.\n\n`@JsonIgnoreProperties`(ignoreUnknown=false) is not working with spring 4.2.0 and upper version of spring. But it is working with 4.0.4 and 4.0.1  .\n\nSpring and Jackson dependency we have used,\n\n```java\n\n              <dependency>\n\t\t\t<groupId>com.fasterxml.jackson.core</groupId>\n\t\t\t<artifactId>jackson-databind</artifactId>\n\t\t\t<version>2.6.3</version>\n\t\t</dependency>\n\t\t<dependency>\n\t\t\t<groupId>com.fasterxml.jackson.core</groupId>\n\t\t\t<artifactId>jackson-core</artifactId>\n\t\t\t<version>2.6.3</version>\n\t\t</dependency>\n\t\t<dependency>\n\t\t\t<groupId>com.fasterxml.jackson.core</groupId>\n\t\t\t<artifactId>jackson-annotations</artifactId>\n\t\t\t<version>2.6.3</version>\n\t\t</dependency> \n               <dependency>\n\t\t\t<groupId>org.springframework</groupId>\n\t\t\t<artifactId>spring-context</artifactId>\n\t\t\t<version>4.2.8.RELEASE</version>\n\t\t</dependency>\n\t\t<dependency>\n\t\t\t<groupId>org.springframework</groupId>\n\t\t\t<artifactId>spring-webmvc</artifactId>\n\t\t\t<version>4.2.8.RELEASE</version>\n\t\t</dependency>\n\n```\n\nIf I send json request with invalid fields then it is accepting as a valid request. But it should give the bad request response.\n\nFor example:\nIf I have class\n\npublic class Student {\nprivate String id;\nprivate String name;\n}\n\nIf send valid corresponding json request it should be like\n\n{\n\"id\": \"123\",\n\"name\": \"test\"\n}\n\nBut even if I send json request with invalid fields like below it is still accepting.\n\n{\n\"id\": \"123\",\n\"name\": \"test\",\n\"anyinvalidkey\": \"test\"\n}\n\n---\n\n**Affects:** 4.2.4, 4.2.8\n\n**Issue Links:**\n- #16510 Set Jackson FAIL_ON_UNKNOWN_PROPERTIES property to false by default\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453448861","453448863","453448867"], "labels":[]}