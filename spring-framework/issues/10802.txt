{"id":"10802", "title":"use RestUrlRewriteFilter to rewrite url  from \"/foo.gif\" to \"/static/foo.gif\" for visit static resource [SPR-6134]", "body":"**[badqiu](https://jira.spring.io/secure/ViewProfile.jspa?name=badqiu)** opened **[SPR-6134](https://jira.spring.io/browse/SPR-6134?redirect=false)** and commented

current petclinic sample to visit static resource must start with /static prefix.

However, we can use URL rewriting to avoid adding the prefix.

RestUrlRewriteFilter will forward like /foo.js => /static/foo.js
rewrite usecase:
/foo.js => /static/foo.js
/foo/demo.gif => /static/demo.gif

not rewrite by excludeExtentions, default value is  do,jsp,jspx:
/foo.jsp => /foo.jsp
/foo.jspx => /foo.jsp
/foo.do => /foo.do

web.xml config
\\<filter>
\\<filter-name>RestUrlRewriteFilter\\</filter-name>
\\<filter-class>cn.org.rapid_framework.web.filter.RestUrlRewriteFilter\\</filter-class>
\\<init-param>
\\<param-name>prefix\\</param-name>
\\<param-value>/static\\</param-value>
\\</init-param>
\\<init-param>
\\<param-name>excludeExtentions\\</param-name>
\\<param-value>jsp,jspx,do\\</param-value>
\\</init-param>
\\<init-param>
\\<param-name>debug\\</param-name>
\\<param-value>true\\</param-value>
\\</init-param>				
\\</filter>
\\<filter-mapping>
\\<filter-name>RestUrlRewriteFilter\\</filter-name>
\\<url-pattern>/*\\</url-pattern>
\\</filter-mapping>

this is my patch.

---

**Affects:** 3.0 M4

**Attachments:**
- [RestUrlRewriteFilter.java](https://jira.spring.io/secure/attachment/15740/RestUrlRewriteFilter.java) (_5.35 kB_)
- [RestUrlRewriteFilter.java](https://jira.spring.io/secure/attachment/15729/RestUrlRewriteFilter.java) (_4.22 kB_)
- [RestUrlRewriteFilterTest.java](https://jira.spring.io/secure/attachment/15741/RestUrlRewriteFilterTest.java) (_3.30 kB_)
- [RestUrlRewriteFilterTest.java](https://jira.spring.io/secure/attachment/15730/RestUrlRewriteFilterTest.java) (_2.68 kB_)

**Issue Links:**
- #15561 Enhance ResourceHttpRequestHandler with ResourceResolver strategy (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10802","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10802/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10802/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10802/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10802","id":398097621,"node_id":"MDU6SXNzdWUzOTgwOTc2MjE=","number":10802,"title":"use RestUrlRewriteFilter to rewrite url  from \"/foo.gif\" to \"/static/foo.gif\" for visit static resource [SPR-6134]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2009-09-19T21:03:37Z","updated_at":"2019-01-11T14:10:21Z","closed_at":"2015-04-02T02:40:37Z","author_association":"COLLABORATOR","body":"**[badqiu](https://jira.spring.io/secure/ViewProfile.jspa?name=badqiu)** opened **[SPR-6134](https://jira.spring.io/browse/SPR-6134?redirect=false)** and commented\n\ncurrent petclinic sample to visit static resource must start with /static prefix.\n\nHowever, we can use URL rewriting to avoid adding the prefix.\n\nRestUrlRewriteFilter will forward like /foo.js => /static/foo.js\nrewrite usecase:\n/foo.js => /static/foo.js\n/foo/demo.gif => /static/demo.gif\n\nnot rewrite by excludeExtentions, default value is  do,jsp,jspx:\n/foo.jsp => /foo.jsp\n/foo.jspx => /foo.jsp\n/foo.do => /foo.do\n\nweb.xml config\n\\<filter>\n\\<filter-name>RestUrlRewriteFilter\\</filter-name>\n\\<filter-class>cn.org.rapid_framework.web.filter.RestUrlRewriteFilter\\</filter-class>\n\\<init-param>\n\\<param-name>prefix\\</param-name>\n\\<param-value>/static\\</param-value>\n\\</init-param>\n\\<init-param>\n\\<param-name>excludeExtentions\\</param-name>\n\\<param-value>jsp,jspx,do\\</param-value>\n\\</init-param>\n\\<init-param>\n\\<param-name>debug\\</param-name>\n\\<param-value>true\\</param-value>\n\\</init-param>\t\t\t\t\n\\</filter>\n\\<filter-mapping>\n\\<filter-name>RestUrlRewriteFilter\\</filter-name>\n\\<url-pattern>/*\\</url-pattern>\n\\</filter-mapping>\n\nthis is my patch.\n\n---\n\n**Affects:** 3.0 M4\n\n**Attachments:**\n- [RestUrlRewriteFilter.java](https://jira.spring.io/secure/attachment/15740/RestUrlRewriteFilter.java) (_5.35 kB_)\n- [RestUrlRewriteFilter.java](https://jira.spring.io/secure/attachment/15729/RestUrlRewriteFilter.java) (_4.22 kB_)\n- [RestUrlRewriteFilterTest.java](https://jira.spring.io/secure/attachment/15741/RestUrlRewriteFilterTest.java) (_3.30 kB_)\n- [RestUrlRewriteFilterTest.java](https://jira.spring.io/secure/attachment/15730/RestUrlRewriteFilterTest.java) (_2.68 kB_)\n\n**Issue Links:**\n- #15561 Enhance ResourceHttpRequestHandler with ResourceResolver strategy (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453341982","453341985"], "labels":["in: web","status: duplicate","type: enhancement"]}