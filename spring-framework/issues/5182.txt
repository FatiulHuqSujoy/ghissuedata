{"id":"5182", "title":"Be able to replace the single session instance with a new instance after a database exception [SPR-453]", "body":"**[Stefaan Destoop](https://jira.spring.io/secure/ViewProfile.jspa?name=sdestoop)** opened **[SPR-453](https://jira.spring.io/browse/SPR-453?redirect=false)** and commented

The single sesion mode in OpenSessionInViewFilter has a big disadvantage:  (taken from the JavaDOC):
\"A single session per request allows for most efficient first-level caching, but can cause side effects, for example on saveOrUpdate or if continuing after a rolled-back transaction. ...\"
In order to counter (some) side effects, I want to close the session and replace it with a new one.  From what I can read from the Hibernate forums, this is recommended by Hibernate after an exception.

Proposed solution:
In the OpenSessionInViewFilter, we shouldn't use the local variable 'session' when closing the session.  Instead, we request it from the TransactionSynchronizationManager: (replace in the finally block in case of not participating and single session)
SessionHolder sessionHolder = (SessionHolder)TransactionSynchronizationManager.unbindResource(sessionFactory);
Session currentSession = sessionHolder.getSession();
logger.debug(\"Closing single Hibernate session in OpenSessionInViewFilter\");
closeSession(currentSession, sessionFactory);

In fact, the OpenSessionInViewInterceptor already does that as the create and close are in two seperate methods.

To call the replaceWithNewSingleSession, I have 4 posible solutions.  The problem is that we can't set a reference to the OpenSessionInViewFilter, because it isn't a singleton and it isn't set by spring but instead initialized from the web.xml.
1. Add a static method replaceWithNewSingleSession(SessionFactory) on the OpenSessionInViewFilter.
   /**

* Replace the single session with a new instance.
* This is extremely useful after a database error such as
* DataIntegrityViolationException.
* `@param` sessionFactory
* 

      The session factoy in use.

*/
public static void replaceWithNewSingleSession(SessionFactory sessionFactory) {
SessionHolder sessionHolder = (SessionHolder)TransactionSynchronizationManager.unbindResource(sessionFactory);
Session oldSession = sessionHolder.getSession();
if (oldSession != null) {
SessionFactoryUtils.closeSessionIfNecessary( oldSession, sessionFactory);
}
Session newSession = SessionFactoryUtils.getSession(sessionFactory, true);
TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(newSession));
newSession.setFlushMode(FlushMode.NEVER);
}
(The same code or better a direct call to this code could be set in the OpenSessionInViewInterceptor).

In the code of our controllers we need to have a new sessionFactory property set, so we can call:
try {
methodThatMightTrowException();
} catch(DataIntegrityViolationException dive) {
errors.reject(\"dataIntegrityViolation\", \"A data integrity exception occured\");
replaceWithNewSingleSession();
}
The replaceWithNewSingleSession and setSessionFactory could be put in the AbstractController, however, this would be confusing for people not using hibernate or not using the OpenSessionInViewFilter.
/**
* Switch from single session mode to deferred close (= \"multiple\" session mode).
  */
  protected void replaceWithNewSingleSession() {
  if (sessionFactory != null) {
  DynamicOpenSessionInViewFilter.replaceWithNewSingleSession(sessionFactory);
  } else {
  logger.error(\"Cannot replace with a new session unless sessionFactory is set for this spring bean (class: \"+this.getClass().toString()+\")\");
  }
  }

2. We could have the sessionFactory kept static in the OpenSessionInViewFilter after it has been looked up us that static instance inside the replaceWithNewSingleSession.  This is my current solution.
   private static SessionFactory sessionFactory;

/**
* Look up the SessionFactory that this filter should use.
* 

\\<p>The default implementation looks for a bean with the specified name
* in Spring's root application context.
* `@return` the SessionFactory to use
* `@see` #getSessionFactoryBeanName
  */
  protected SessionFactory lookupSessionFactory() {
  // replace super.lookupSessionFactory with current code in OpenSessionInViewFilter.
  sessionFactory = super.lookupSessionFactory();
  return sessionFactory;
  }

3. We could create a ThreadLocal object to keep the sessionFactory instance.

4. Only provide this functionality on the OpenSessionInViewInterceptor class as this one is referencable from the spring context.  The controllers then need to have a reference to the OpenSessionInViewInterceptor.



---

**Affects:** 1.1.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5182","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5182/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5182/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5182/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5182","id":398052630,"node_id":"MDU6SXNzdWUzOTgwNTI2MzA=","number":5182,"title":"Be able to replace the single session instance with a new instance after a database exception [SPR-453]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/12","html_url":"https://github.com/spring-projects/spring-framework/milestone/12","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/12/labels","id":3960782,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Mg==","number":12,"title":"1.1.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":62,"state":"closed","created_at":"2019-01-10T22:02:05Z","updated_at":"2019-01-10T22:58:37Z","due_on":null,"closed_at":"2019-01-10T22:02:05Z"},"comments":3,"created_at":"2004-11-11T21:16:44Z","updated_at":"2019-01-13T22:53:26Z","closed_at":"2004-11-13T19:46:29Z","author_association":"COLLABORATOR","body":"**[Stefaan Destoop](https://jira.spring.io/secure/ViewProfile.jspa?name=sdestoop)** opened **[SPR-453](https://jira.spring.io/browse/SPR-453?redirect=false)** and commented\n\nThe single sesion mode in OpenSessionInViewFilter has a big disadvantage:  (taken from the JavaDOC):\n\"A single session per request allows for most efficient first-level caching, but can cause side effects, for example on saveOrUpdate or if continuing after a rolled-back transaction. ...\"\nIn order to counter (some) side effects, I want to close the session and replace it with a new one.  From what I can read from the Hibernate forums, this is recommended by Hibernate after an exception.\n\nProposed solution:\nIn the OpenSessionInViewFilter, we shouldn't use the local variable 'session' when closing the session.  Instead, we request it from the TransactionSynchronizationManager: (replace in the finally block in case of not participating and single session)\nSessionHolder sessionHolder = (SessionHolder)TransactionSynchronizationManager.unbindResource(sessionFactory);\nSession currentSession = sessionHolder.getSession();\nlogger.debug(\"Closing single Hibernate session in OpenSessionInViewFilter\");\ncloseSession(currentSession, sessionFactory);\n\nIn fact, the OpenSessionInViewInterceptor already does that as the create and close are in two seperate methods.\n\nTo call the replaceWithNewSingleSession, I have 4 posible solutions.  The problem is that we can't set a reference to the OpenSessionInViewFilter, because it isn't a singleton and it isn't set by spring but instead initialized from the web.xml.\n1. Add a static method replaceWithNewSingleSession(SessionFactory) on the OpenSessionInViewFilter.\n   /**\n\n* Replace the single session with a new instance.\n* This is extremely useful after a database error such as\n* DataIntegrityViolationException.\n* `@param` sessionFactory\n* \n\n      The session factoy in use.\n\n*/\npublic static void replaceWithNewSingleSession(SessionFactory sessionFactory) {\nSessionHolder sessionHolder = (SessionHolder)TransactionSynchronizationManager.unbindResource(sessionFactory);\nSession oldSession = sessionHolder.getSession();\nif (oldSession != null) {\nSessionFactoryUtils.closeSessionIfNecessary( oldSession, sessionFactory);\n}\nSession newSession = SessionFactoryUtils.getSession(sessionFactory, true);\nTransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(newSession));\nnewSession.setFlushMode(FlushMode.NEVER);\n}\n(The same code or better a direct call to this code could be set in the OpenSessionInViewInterceptor).\n\nIn the code of our controllers we need to have a new sessionFactory property set, so we can call:\ntry {\nmethodThatMightTrowException();\n} catch(DataIntegrityViolationException dive) {\nerrors.reject(\"dataIntegrityViolation\", \"A data integrity exception occured\");\nreplaceWithNewSingleSession();\n}\nThe replaceWithNewSingleSession and setSessionFactory could be put in the AbstractController, however, this would be confusing for people not using hibernate or not using the OpenSessionInViewFilter.\n/**\n* Switch from single session mode to deferred close (= \"multiple\" session mode).\n  */\n  protected void replaceWithNewSingleSession() {\n  if (sessionFactory != null) {\n  DynamicOpenSessionInViewFilter.replaceWithNewSingleSession(sessionFactory);\n  } else {\n  logger.error(\"Cannot replace with a new session unless sessionFactory is set for this spring bean (class: \"+this.getClass().toString()+\")\");\n  }\n  }\n\n2. We could have the sessionFactory kept static in the OpenSessionInViewFilter after it has been looked up us that static instance inside the replaceWithNewSingleSession.  This is my current solution.\n   private static SessionFactory sessionFactory;\n\n/**\n* Look up the SessionFactory that this filter should use.\n* \n\n\\<p>The default implementation looks for a bean with the specified name\n* in Spring's root application context.\n* `@return` the SessionFactory to use\n* `@see` #getSessionFactoryBeanName\n  */\n  protected SessionFactory lookupSessionFactory() {\n  // replace super.lookupSessionFactory with current code in OpenSessionInViewFilter.\n  sessionFactory = super.lookupSessionFactory();\n  return sessionFactory;\n  }\n\n3. We could create a ThreadLocal object to keep the sessionFactory instance.\n\n4. Only provide this functionality on the OpenSessionInViewInterceptor class as this one is referencable from the spring context.  The controllers then need to have a reference to the OpenSessionInViewInterceptor.\n\n\n\n---\n\n**Affects:** 1.1.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453289253","453289254","453289257"], "labels":["in: data","type: enhancement"]}