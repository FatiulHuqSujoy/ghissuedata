{"id":"13901", "title":"Threading issue in InjectionMetadata.InjectedElement [SPR-9263]", "body":"**[Anders Kobberup](https://jira.spring.io/secure/ViewProfile.jspa?name=andersk)** opened **[SPR-9263](https://jira.spring.io/browse/SPR-9263?redirect=false)** and commented

We are having a rising amount of issues where the symptom is that random properties that have been autowired into our beans (via `@autowired` setter methods) turns our null, without any exceptions during start up.
This means that we have no way of knowing whether our production setup is actually functioning as expected, or if it will encounter nullpointers as the diferent parts of the platform is used. Also the only way to \"fix\" any such nullpointer is to restart the application, which obviously is not good.

I have traced the problem down to what i believe is a concurrency flaw in checkPropertySkipping in InjectionMetadata.InjectedElement.

```
172: protected boolean checkPropertySkipping(PropertyValues pvs) {
173: 	if (this.skip == null) {
174: 		if (pvs != null) {
175: 			synchronized (pvs) {
176: 				if (this.skip == null) {
177: 					if (this.pd != null) {
178: 						if (pvs.contains(this.pd.getName())) {
179: 							// Explicit value provided as part of the bean definition.
180: 							this.skip = true;
181: 							return true;
182: 						}
183: 						else if (pvs instanceof MutablePropertyValues) {
184: 							((MutablePropertyValues) pvs).registerProcessedProperty(this.pd.getName());
185: 						}
186: 					}
187: 				}
188: 			}
189: 		}
190: 		this.skip = false;
191: 	}
192: 	return this.skip;
193: }
```

Consider the scenario where at least two threads are creating an instance of a given bean, which have autowired properties. Both of these threads will enter the 'Inject(...)' method, and from here go to checkPropertySkipping (the argument for both threads are the same PropertyValues object).

Thread A will enter the synchronized block, while Thread B will be halted before this block.
Thread A will proceed to the inside of the synchronized block, and enter the 'else if' block, and register the propertyname in the PropertyValues object as processed (line 184).
As soon as Thread A exits the synchronized block, the java scheduler halts Thread A and resumes Thread B which will enter the synch block and, as Thread A has been halted and thus not having set 'this.skip = false' (line 190), the 'if(this.skip == null)' statement (line 176) will return true.
After this, thread A may continue running (the object that it is creating will be created correctly).

Thread B will evaluate the 'if (pvs.contains(this.pd.getName()))' statement, which will return true, as Thread A has registered the property name as processed, and as a result Thread B will set 'this.skip = true' (line 180) and return.

As 'skip' has been set to true on this InjectedElement, every later invocation of checkPropertySkipping after this will return true, and thus every later object that is instantiated will not have this dependency injected.

The obvious solution is to ensure that 'this.skip = false' is also set within the synchronized block (add a line containing 'this.skip = false;' after line 187), thus ensuring that any threads that are awaiting the monitor will evaluate the if statement at line  176, to false and thus not setting 'this.skip = true'.

In order to see if this is more than just a theoretical posibility that multiple threads will enter this code, i have created a small test class that can simulate this method being invoked by multiple threads. The test i have run, show that it will happen.

```
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class SynchTest {

	private final Object mutex = new Object();
	private Boolean finished;
	private AtomicInteger numberOfCompletions = new AtomicInteger( 0 );
	private int testNumber;

	public SynchTest( int TestNumber ) {
		this.testNumber = TestNumber;
	}

	public Runn getRunn() {
		return new Runn();
	}

	void gotThrough() {
		final int nCount = numberOfCompletions.incrementAndGet();
		if ( nCount > 1 ) {
			//woops - this is not good!!
			System.out.println( \"In test \" + testNumber + \", \" + nCount + \" got into the synch block!!\" );
		}
	}

	private class Runn implements Callable<Object> {

		@Override
		public Object call() throws Exception {
			if ( finished == null ) {
					synchronized ( mutex ) {
						if ( finished == null ) {
							gotThrough();
						}
					}
				finished = false;
			}
			return null;
		}
	}

	public static void main( String[] args ) throws InterruptedException {
		ExecutorService cThreadPool = Executors.newFixedThreadPool( 5 );
		ArrayList<Runn> cRunnables = new ArrayList<>();
		for ( int i = 0 ; i < 1000 ; i++ ) {
			SynchTest cTest = new SynchTest( i );
			for ( int j = 0 ; j < 5 ; j++ ) {
				cRunnables.add( cTest.getRunn() );
			}

		}
		System.out.println( \"Invoking \"+cRunnables.size()+\" tasks\" );
		cThreadPool.invokeAll( cRunnables );
		cThreadPool.shutdown();
		cThreadPool.awaitTermination( 5, TimeUnit.DAYS );
	}
}
```

Heres the result of just one run:

Invoking 5000 tasks
In test 695, 2 got into the synch block!!
In test 765, 2 got into the synch block!!
In test 824, 2 got into the synch block!!
In test 872, 2 got into the synch block!!
In test 981, 2 got into the synch block!!

---

**Affects:** 3.1.1

**Issue Links:**
- #14439 Autowired properties can remain unset during concurrent instantiation of prototype-beans (_**\"duplicates\"**_)
- #13894 Concurrent retrieval of prototype-scoped beans may result in null `@Autowired` fields

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13901","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13901/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13901/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13901/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13901","id":398118251,"node_id":"MDU6SXNzdWUzOTgxMTgyNTE=","number":13901,"title":"Threading issue in InjectionMetadata.InjectedElement [SPR-9263]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2012-03-23T01:25:15Z","updated_at":"2019-01-13T07:05:32Z","closed_at":"2013-01-16T04:53:42Z","author_association":"COLLABORATOR","body":"**[Anders Kobberup](https://jira.spring.io/secure/ViewProfile.jspa?name=andersk)** opened **[SPR-9263](https://jira.spring.io/browse/SPR-9263?redirect=false)** and commented\n\nWe are having a rising amount of issues where the symptom is that random properties that have been autowired into our beans (via `@autowired` setter methods) turns our null, without any exceptions during start up.\nThis means that we have no way of knowing whether our production setup is actually functioning as expected, or if it will encounter nullpointers as the diferent parts of the platform is used. Also the only way to \"fix\" any such nullpointer is to restart the application, which obviously is not good.\n\nI have traced the problem down to what i believe is a concurrency flaw in checkPropertySkipping in InjectionMetadata.InjectedElement.\n\n```\n172: protected boolean checkPropertySkipping(PropertyValues pvs) {\n173: \tif (this.skip == null) {\n174: \t\tif (pvs != null) {\n175: \t\t\tsynchronized (pvs) {\n176: \t\t\t\tif (this.skip == null) {\n177: \t\t\t\t\tif (this.pd != null) {\n178: \t\t\t\t\t\tif (pvs.contains(this.pd.getName())) {\n179: \t\t\t\t\t\t\t// Explicit value provided as part of the bean definition.\n180: \t\t\t\t\t\t\tthis.skip = true;\n181: \t\t\t\t\t\t\treturn true;\n182: \t\t\t\t\t\t}\n183: \t\t\t\t\t\telse if (pvs instanceof MutablePropertyValues) {\n184: \t\t\t\t\t\t\t((MutablePropertyValues) pvs).registerProcessedProperty(this.pd.getName());\n185: \t\t\t\t\t\t}\n186: \t\t\t\t\t}\n187: \t\t\t\t}\n188: \t\t\t}\n189: \t\t}\n190: \t\tthis.skip = false;\n191: \t}\n192: \treturn this.skip;\n193: }\n```\n\nConsider the scenario where at least two threads are creating an instance of a given bean, which have autowired properties. Both of these threads will enter the 'Inject(...)' method, and from here go to checkPropertySkipping (the argument for both threads are the same PropertyValues object).\n\nThread A will enter the synchronized block, while Thread B will be halted before this block.\nThread A will proceed to the inside of the synchronized block, and enter the 'else if' block, and register the propertyname in the PropertyValues object as processed (line 184).\nAs soon as Thread A exits the synchronized block, the java scheduler halts Thread A and resumes Thread B which will enter the synch block and, as Thread A has been halted and thus not having set 'this.skip = false' (line 190), the 'if(this.skip == null)' statement (line 176) will return true.\nAfter this, thread A may continue running (the object that it is creating will be created correctly).\n\nThread B will evaluate the 'if (pvs.contains(this.pd.getName()))' statement, which will return true, as Thread A has registered the property name as processed, and as a result Thread B will set 'this.skip = true' (line 180) and return.\n\nAs 'skip' has been set to true on this InjectedElement, every later invocation of checkPropertySkipping after this will return true, and thus every later object that is instantiated will not have this dependency injected.\n\nThe obvious solution is to ensure that 'this.skip = false' is also set within the synchronized block (add a line containing 'this.skip = false;' after line 187), thus ensuring that any threads that are awaiting the monitor will evaluate the if statement at line  176, to false and thus not setting 'this.skip = true'.\n\nIn order to see if this is more than just a theoretical posibility that multiple threads will enter this code, i have created a small test class that can simulate this method being invoked by multiple threads. The test i have run, show that it will happen.\n\n```\nimport java.util.ArrayList;\nimport java.util.concurrent.Callable;\nimport java.util.concurrent.ExecutorService;\nimport java.util.concurrent.Executors;\nimport java.util.concurrent.TimeUnit;\nimport java.util.concurrent.atomic.AtomicInteger;\n\npublic class SynchTest {\n\n\tprivate final Object mutex = new Object();\n\tprivate Boolean finished;\n\tprivate AtomicInteger numberOfCompletions = new AtomicInteger( 0 );\n\tprivate int testNumber;\n\n\tpublic SynchTest( int TestNumber ) {\n\t\tthis.testNumber = TestNumber;\n\t}\n\n\tpublic Runn getRunn() {\n\t\treturn new Runn();\n\t}\n\n\tvoid gotThrough() {\n\t\tfinal int nCount = numberOfCompletions.incrementAndGet();\n\t\tif ( nCount > 1 ) {\n\t\t\t//woops - this is not good!!\n\t\t\tSystem.out.println( \"In test \" + testNumber + \", \" + nCount + \" got into the synch block!!\" );\n\t\t}\n\t}\n\n\tprivate class Runn implements Callable<Object> {\n\n\t\t@Override\n\t\tpublic Object call() throws Exception {\n\t\t\tif ( finished == null ) {\n\t\t\t\t\tsynchronized ( mutex ) {\n\t\t\t\t\t\tif ( finished == null ) {\n\t\t\t\t\t\t\tgotThrough();\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\tfinished = false;\n\t\t\t}\n\t\t\treturn null;\n\t\t}\n\t}\n\n\tpublic static void main( String[] args ) throws InterruptedException {\n\t\tExecutorService cThreadPool = Executors.newFixedThreadPool( 5 );\n\t\tArrayList<Runn> cRunnables = new ArrayList<>();\n\t\tfor ( int i = 0 ; i < 1000 ; i++ ) {\n\t\t\tSynchTest cTest = new SynchTest( i );\n\t\t\tfor ( int j = 0 ; j < 5 ; j++ ) {\n\t\t\t\tcRunnables.add( cTest.getRunn() );\n\t\t\t}\n\n\t\t}\n\t\tSystem.out.println( \"Invoking \"+cRunnables.size()+\" tasks\" );\n\t\tcThreadPool.invokeAll( cRunnables );\n\t\tcThreadPool.shutdown();\n\t\tcThreadPool.awaitTermination( 5, TimeUnit.DAYS );\n\t}\n}\n```\n\nHeres the result of just one run:\n\nInvoking 5000 tasks\nIn test 695, 2 got into the synch block!!\nIn test 765, 2 got into the synch block!!\nIn test 824, 2 got into the synch block!!\nIn test 872, 2 got into the synch block!!\nIn test 981, 2 got into the synch block!!\n\n---\n\n**Affects:** 3.1.1\n\n**Issue Links:**\n- #14439 Autowired properties can remain unset during concurrent instantiation of prototype-beans (_**\"duplicates\"**_)\n- #13894 Concurrent retrieval of prototype-scoped beans may result in null `@Autowired` fields\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453366768"], "labels":["in: core","status: duplicate"]}