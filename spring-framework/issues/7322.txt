{"id":"7322", "title":"form:checkbox limitations [SPR-2633]", "body":"**[Daniel Hopper](https://jira.spring.io/secure/ViewProfile.jspa?name=split3)** opened **[SPR-2633](https://jira.spring.io/browse/SPR-2633?redirect=false)** and commented

public class Client {
private Long id;

    private Set<Product> products;
    
    ...get/set id methods...
    
    public Set<Product> getProducts() {
        return products;
    }
    
    public void setProducts(Set<Product> products) {
        this.products = products;
    }

}

public class Product {

    private Long id;
    
    private String name;
    
    ... get/set methods...

}

public class BasicDaoCollectionEditor extends CustomCollectionEditor{

    private BasicDao basicDao;
    
    public BasicDaoCollectionEditor(Class clazz, BasicDao basicDao) {
    	super(clazz);
    	this.basicDao = basicDao;
    }
    
    @Override
    protected Object convertElement(Object object) {
    	Long id = new Long((String)object);
    	return basicDao.load(id);
    }

}

In the controller the backing object is a Client object
Also in request scope is a collection of available products in scope products.
------------ jsp -------------

<form:form>
<c:forEach var=\"product\" items=\"${products}\">
<form:checkbox path=\"products\" value=\"${product.id}\"/>${product.name}
</c:forEach>
</form:form>
------------ jsp -------------

The collection products will contain the appropriate products on select and submit.
The problem is on update or a resubmit due to validation errors the form:checkbox is trying
to check the value Long == Product.  Would is make sense to have something like

<form:checkbox path=\"products\" checkField=\"id\" value=\"${product.id}\"/>${product.name}

So that way is would check againsted a particular field if desired, or is there another way around this?

Also the checkbox tag will select the appropriate values on validation error or update if the tag uses

--------- jsp ----------------
<form:checkbox path=\"products\" value=\"${product}\"/>${product.name}
--------- jsp ----------------

In which case the value is actually the toString of the object, but then supports won't work at all unless the toString is returning just the id value which isn't really an option.


---

**Affects:** 2.0 RC4

8 votes, 11 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7322","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7322/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7322/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7322/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7322","id":398071087,"node_id":"MDU6SXNzdWUzOTgwNzEwODc=","number":7322,"title":"form:checkbox limitations [SPR-2633]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false,"description":"Issues in web modules (web, webmvc, webflux, websocket)"},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false,"description":"A general enhancement"}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41","html_url":"https://github.com/spring-projects/spring-framework/milestone/41","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41/labels","id":3960813,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxMw==","number":41,"title":"2.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":76,"state":"closed","created_at":"2019-01-10T22:02:41Z","updated_at":"2019-01-11T01:26:07Z","due_on":"2007-04-08T07:00:00Z","closed_at":"2019-01-10T22:02:41Z"},"comments":17,"created_at":"2006-09-25T00:45:57Z","updated_at":"2012-06-19T03:50:06Z","closed_at":"2012-06-19T03:50:06Z","author_association":"COLLABORATOR","body":"**[Daniel Hopper](https://jira.spring.io/secure/ViewProfile.jspa?name=split3)** opened **[SPR-2633](https://jira.spring.io/browse/SPR-2633?redirect=false)** and commented\n\npublic class Client {\nprivate Long id;\n\n    private Set<Product> products;\n    \n    ...get/set id methods...\n    \n    public Set<Product> getProducts() {\n        return products;\n    }\n    \n    public void setProducts(Set<Product> products) {\n        this.products = products;\n    }\n\n}\n\npublic class Product {\n\n    private Long id;\n    \n    private String name;\n    \n    ... get/set methods...\n\n}\n\npublic class BasicDaoCollectionEditor extends CustomCollectionEditor{\n\n    private BasicDao basicDao;\n    \n    public BasicDaoCollectionEditor(Class clazz, BasicDao basicDao) {\n    \tsuper(clazz);\n    \tthis.basicDao = basicDao;\n    }\n    \n    @Override\n    protected Object convertElement(Object object) {\n    \tLong id = new Long((String)object);\n    \treturn basicDao.load(id);\n    }\n\n}\n\nIn the controller the backing object is a Client object\nAlso in request scope is a collection of available products in scope products.\n------------ jsp -------------\n\n<form:form>\n<c:forEach var=\"product\" items=\"${products}\">\n<form:checkbox path=\"products\" value=\"${product.id}\"/>${product.name}\n</c:forEach>\n</form:form>\n------------ jsp -------------\n\nThe collection products will contain the appropriate products on select and submit.\nThe problem is on update or a resubmit due to validation errors the form:checkbox is trying\nto check the value Long == Product.  Would is make sense to have something like\n\n<form:checkbox path=\"products\" checkField=\"id\" value=\"${product.id}\"/>${product.name}\n\nSo that way is would check againsted a particular field if desired, or is there another way around this?\n\nAlso the checkbox tag will select the appropriate values on validation error or update if the tag uses\n\n--------- jsp ----------------\n<form:checkbox path=\"products\" value=\"${product}\"/>${product.name}\n--------- jsp ----------------\n\nIn which case the value is actually the toString of the object, but then supports won't work at all unless the toString is returning just the id value which isn't really an option.\n\n\n---\n\n**Affects:** 2.0 RC4\n\n8 votes, 11 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453310399","453310401","453310402","453310403","453310405","453310407","453310409","453310410","453310411","453310413","453310415","453310416","453310417","453310419","453310420","453310421","453310422"], "labels":["in: web","type: enhancement"]}