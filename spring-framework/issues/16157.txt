{"id":"16157", "title":"File extension not removed when URI variable contains a dot [SPR-11532]", "body":"**[Jonathan Peffer](https://jira.spring.io/secure/ViewProfile.jspa?name=jpeffer)** opened **[SPR-11532](https://jira.spring.io/browse/SPR-11532?redirect=false)** and commented

While mapping a URI fragment into a nested object's attribute and using the ContentNegotiatingViewResolver, the content-type appended at the end of the URL is not stripped from the bound value.

Using the following URL:

```
http://localhost/rest/establishments/employees/976685.json
```

I receive the following JSON payload response (I've isolated the return to just the bound ModelAttribute to be more concise):

```
request-payload\":{\"areaOfResponsibility\":{\"owner\":{\"id\":\"976685.json\"}}}
```

Here is the annotated method signature in the associated Controller:

```
@RequestMapping(value = \"/establishments/employees/{areaOfResponsibility.owner.id}\",
                    method = RequestMethod.GET)
public String getEstablishmentsByEmployeeId(@Valid @ModelAttribute(\"request-payload\") EstablishmentDto establishment,
                                                ModelMap model)
```

If I however do not map to an attribute of a nested object and rather use a base object like String, Integer, etc. within the top level object, the content-type is removed properly from the mapped value.

Using the following URL:

```
http://localhost/rest/assets/establishments/123456789.json
```

I receive the following JSON payload response (I've isolated the return to just the bound ModelAttribute to be more concise):

```
request-payload\":{\"establishmentId\":\"123456789\"}
```

Here is the annotated method signature in the associated Controller:

```
@RequestMapping(value = \"/assets/establishments/{establishmentId}\",
                    method = RequestMethod.GET)
public String getAssetByEstablishmentId(@Valid @ModelAttribute(\"request-payload\") EstablishmentDto establishment,
                                            ModelMap model)
```

As you can see, this properly strips the content-type from the value of the mapped path variable.

I have only tested and confirmed this behavior is present within Spring 4.0.1-RELEASE, 4.0.2-RELEASE, and 4.0.3-BUILD-SNAPSHOT.

---

**Affects:** 4.0.1, 4.0.2

**Issue Links:**
- #16225 PatternsRequestCondition strips off extensions

0 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16157","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16157/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16157/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16157/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16157","id":398166335,"node_id":"MDU6SXNzdWUzOTgxNjYzMzU=","number":16157,"title":"File extension not removed when URI variable contains a dot [SPR-11532]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":10,"created_at":"2014-03-09T18:03:18Z","updated_at":"2019-01-12T16:35:18Z","closed_at":"2014-03-27T05:58:02Z","author_association":"COLLABORATOR","body":"**[Jonathan Peffer](https://jira.spring.io/secure/ViewProfile.jspa?name=jpeffer)** opened **[SPR-11532](https://jira.spring.io/browse/SPR-11532?redirect=false)** and commented\n\nWhile mapping a URI fragment into a nested object's attribute and using the ContentNegotiatingViewResolver, the content-type appended at the end of the URL is not stripped from the bound value.\n\nUsing the following URL:\n\n```\nhttp://localhost/rest/establishments/employees/976685.json\n```\n\nI receive the following JSON payload response (I've isolated the return to just the bound ModelAttribute to be more concise):\n\n```\nrequest-payload\":{\"areaOfResponsibility\":{\"owner\":{\"id\":\"976685.json\"}}}\n```\n\nHere is the annotated method signature in the associated Controller:\n\n```\n@RequestMapping(value = \"/establishments/employees/{areaOfResponsibility.owner.id}\",\n                    method = RequestMethod.GET)\npublic String getEstablishmentsByEmployeeId(@Valid @ModelAttribute(\"request-payload\") EstablishmentDto establishment,\n                                                ModelMap model)\n```\n\nIf I however do not map to an attribute of a nested object and rather use a base object like String, Integer, etc. within the top level object, the content-type is removed properly from the mapped value.\n\nUsing the following URL:\n\n```\nhttp://localhost/rest/assets/establishments/123456789.json\n```\n\nI receive the following JSON payload response (I've isolated the return to just the bound ModelAttribute to be more concise):\n\n```\nrequest-payload\":{\"establishmentId\":\"123456789\"}\n```\n\nHere is the annotated method signature in the associated Controller:\n\n```\n@RequestMapping(value = \"/assets/establishments/{establishmentId}\",\n                    method = RequestMethod.GET)\npublic String getAssetByEstablishmentId(@Valid @ModelAttribute(\"request-payload\") EstablishmentDto establishment,\n                                            ModelMap model)\n```\n\nAs you can see, this properly strips the content-type from the value of the mapped path variable.\n\nI have only tested and confirmed this behavior is present within Spring 4.0.1-RELEASE, 4.0.2-RELEASE, and 4.0.3-BUILD-SNAPSHOT.\n\n---\n\n**Affects:** 4.0.1, 4.0.2\n\n**Issue Links:**\n- #16225 PatternsRequestCondition strips off extensions\n\n0 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453410849","453410850","453410851","453410852","453410853","453410855","453410856","453410857","453410858","453410859"], "labels":["in: web","status: declined"]}