{"id":"20479", "title":"Reactive response is not shown in Postman [SPR-15925]", "body":"**[Orest](https://jira.spring.io/secure/ViewProfile.jspa?name=korest)** opened **[SPR-15925](https://jira.spring.io/browse/SPR-15925?redirect=false)** and commented

I've created sample project using start.spring.io with the latest Spring Boot version 2.0.0.M3 and Reactive Web dependency.

Added there simple controller with one endpoint:

```java
@RestController
public class DemoController {

    @GetMapping(value = \"/test\")
    public Mono<ServerResponse> serverResponseMono() {
        return ServerResponse.ok()
                .contentType(APPLICATION_JSON)
                .body(Mono.just(\"test\"), String.class);
    }

}

```

Then called it in Postman and I can't see the result. It has status 200 but body is

```java
data:
```

Also I can see headers:

```java
Content-Type →text/event-stream
transfer-encoding →chunked
```

Not sure if it a bug but I suppose it should show response in Postman.
Attached the sample project.


---

**Affects:** 5.0 RC3

**Attachments:**
- [demo_postman.7z](https://jira.spring.io/secure/attachment/25091/demo_postman.7z) (_68.03 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20479","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20479/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20479/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20479/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20479","id":398215011,"node_id":"MDU6SXNzdWUzOTgyMTUwMTE=","number":20479,"title":"Reactive response is not shown in Postman [SPR-15925]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2017-09-05T07:57:50Z","updated_at":"2019-01-12T05:20:19Z","closed_at":"2017-09-05T08:36:47Z","author_association":"COLLABORATOR","body":"**[Orest](https://jira.spring.io/secure/ViewProfile.jspa?name=korest)** opened **[SPR-15925](https://jira.spring.io/browse/SPR-15925?redirect=false)** and commented\n\nI've created sample project using start.spring.io with the latest Spring Boot version 2.0.0.M3 and Reactive Web dependency.\n\nAdded there simple controller with one endpoint:\n\n```java\n@RestController\npublic class DemoController {\n\n    @GetMapping(value = \"/test\")\n    public Mono<ServerResponse> serverResponseMono() {\n        return ServerResponse.ok()\n                .contentType(APPLICATION_JSON)\n                .body(Mono.just(\"test\"), String.class);\n    }\n\n}\n\n```\n\nThen called it in Postman and I can't see the result. It has status 200 but body is\n\n```java\ndata:\n```\n\nAlso I can see headers:\n\n```java\nContent-Type →text/event-stream\ntransfer-encoding →chunked\n```\n\nNot sure if it a bug but I suppose it should show response in Postman.\nAttached the sample project.\n\n\n---\n\n**Affects:** 5.0 RC3\n\n**Attachments:**\n- [demo_postman.7z](https://jira.spring.io/secure/attachment/25091/demo_postman.7z) (_68.03 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453460524"], "labels":["status: invalid"]}