{"id":"22121", "title":"attached jar cannot be scanned  [SPR-17589]", "body":"**[soner sivri](https://jira.spring.io/secure/ViewProfile.jspa?name=sonersivri)** opened **[SPR-17589](https://jira.spring.io/browse/SPR-17589?redirect=false)** and commented

Attached jars cannot be scanned in springboot

what is the problem 

if you attempt to autowire ConfigurationService, you can generate the bug

java classpath Class can be called with no error

attached sample project

 cl.getResources(path) not found jar path 

protected Set\\<Resource> doFindAllClassPathResources(String path) throws IOException {

Set\\<Resource> result = new LinkedHashSet<>(16);

ClassLoader cl = getClassLoader();

Enumeration\\<URL> resourceUrls = (cl != null ? cl.getResources(path) : ClassLoader.getSystemResources(path));

while (resourceUrls.hasMoreElements())

{ URL url = resourceUrls.nextElement(); result.add(convertClassLoaderURL(url)); }

if (\"\".equals(path))

{ // The above result is likely to be incomplete, i.e. only containing file system references. // We need to have pointers to each of the jar files on the classpath as well... addAllClassLoaderJarRoots(cl, result); }

returnresult;

}


---

**Attachments:**
- [boot.zip](https://jira.spring.io/secure/attachment/26150/boot.zip) (_6.03 kB_)
- [common-1.0.jar](https://jira.spring.io/secure/attachment/26147/common-1.0.jar) (_26.59 kB_)
- [configuration-1.0.jar](https://jira.spring.io/secure/attachment/26148/configuration-1.0.jar) (_8.01 kB_)
- [entity-1.0.jar](https://jira.spring.io/secure/attachment/26149/entity-1.0.jar) (_58.90 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22121","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22121/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22121/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22121/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22121","id":398238089,"node_id":"MDU6SXNzdWUzOTgyMzgwODk=","number":22121,"title":"attached jar cannot be scanned  [SPR-17589]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2018-12-11T11:18:44Z","updated_at":"2019-01-12T05:17:30Z","closed_at":"2018-12-12T10:32:35Z","author_association":"COLLABORATOR","body":"**[soner sivri](https://jira.spring.io/secure/ViewProfile.jspa?name=sonersivri)** opened **[SPR-17589](https://jira.spring.io/browse/SPR-17589?redirect=false)** and commented\n\nAttached jars cannot be scanned in springboot\n\nwhat is the problem \n\nif you attempt to autowire ConfigurationService, you can generate the bug\n\njava classpath Class can be called with no error\n\nattached sample project\n\n cl.getResources(path) not found jar path \n\nprotected Set\\<Resource> doFindAllClassPathResources(String path) throws IOException {\n\nSet\\<Resource> result = new LinkedHashSet<>(16);\n\nClassLoader cl = getClassLoader();\n\nEnumeration\\<URL> resourceUrls = (cl != null ? cl.getResources(path) : ClassLoader.getSystemResources(path));\n\nwhile (resourceUrls.hasMoreElements())\n\n{ URL url = resourceUrls.nextElement(); result.add(convertClassLoaderURL(url)); }\n\nif (\"\".equals(path))\n\n{ // The above result is likely to be incomplete, i.e. only containing file system references. // We need to have pointers to each of the jar files on the classpath as well... addAllClassLoaderJarRoots(cl, result); }\n\nreturnresult;\n\n}\n\n\n---\n\n**Attachments:**\n- [boot.zip](https://jira.spring.io/secure/attachment/26150/boot.zip) (_6.03 kB_)\n- [common-1.0.jar](https://jira.spring.io/secure/attachment/26147/common-1.0.jar) (_26.59 kB_)\n- [configuration-1.0.jar](https://jira.spring.io/secure/attachment/26148/configuration-1.0.jar) (_8.01 kB_)\n- [entity-1.0.jar](https://jira.spring.io/secure/attachment/26149/entity-1.0.jar) (_58.90 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453480375","453480376"], "labels":["status: invalid"]}