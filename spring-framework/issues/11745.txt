{"id":"11745", "title":"AnnotationMethodHandlerExceptionResolver incorrectly resolving exceptions [SPR-7085]", "body":"**[Jeff T](https://jira.spring.io/secure/ViewProfile.jspa?name=jeff_t)** opened **[SPR-7085](https://jira.spring.io/browse/SPR-7085?redirect=false)** and commented

I have a controller class with the following exception handler methods:

Code:

`@ExceptionHandler`({PropertyAccessException.class})
`@ResponseStatus`(HttpStatus.BAD_REQUEST)
public ModelAndView handlePropertyAccessException(PropertyAccessException ex) {
// create modelAndView and process ex
return modelAndView;
}

`@ExceptionHandler`({Exception.class})
`@ResponseStatus`(HttpStatus.INTERNAL_SERVER_ERROR)
public ModelAndView handleException(Exception ex) {
// create modelAndView and process ex
return modelAndView;
}

When a ConversionNotSupportedException (subclass of PropertyAccessException) is thrown, the handleException() method is called instead of the expected handlePropertyAccessException() method.

Digging into the AnnotationMethodHandlerExceptionResolver class I find the following private Comparator. This comparator returns 0 when o1 is Exception, o2 is PropertyAccessException, and handlerExceptionType is ConversionNotSupportedException. This is incorrect and the cause of the problem.

Code:

/** Comparator capable of sorting exceptions based on their depth from the thrown exception type. */
private static class DepthComparator implements Comparator<Class<? extends Throwable>> {
private final Class<? extends Throwable> handlerExceptionType;
private DepthComparator(Exception handlerException) {
this.handlerExceptionType = handlerException.getClass();
}
public int compare(Class<? extends Throwable> o1, Class<? extends Throwable> o2) {
int depth1 = getDepth(o1, 0);
int depth2 = getDepth(o2, 0);
return depth2 - depth1;
}
private int getDepth(Class exceptionType, int depth) {
if (exceptionType.equals(handlerExceptionType)) {
// Found it!
return depth;
}
// If we've gone as far as we can go and haven't found it...
if (Throwable.class.equals(exceptionType)) {
return -1;
}
return getDepth(exceptionType.getSuperclass(), depth + 1);
}
}

I modified the class as follows. With this modification, the comparator returns 4 when o1 is Exception, o2 is PropertyAccessException, and handlerExceptionType is ConversionNotSupportedException. This results in the correct behavior:

Code:

/** Comparator capable of sorting exceptions based on their depth from the thrown exception type. */
private static class DepthComparator implements Comparator<Class<? extends Throwable>> {
private final Class<? extends Throwable> handlerExceptionType;
private DepthComparator(Exception handlerException) {
this.handlerExceptionType = handlerException.getClass();
}
public int compare(Class<? extends Throwable> o1, Class<? extends Throwable> o2) {
int depth1 = getDepth(o1, 0, handlerExceptionType);
int depth2 = getDepth(o2, 0, handlerExceptionType);
return depth1 - depth2;
}
private int getDepth(Class exceptionType, int depth, Class handlerExceptionType) {
if (exceptionType.equals(handlerExceptionType)) {
// Found it!
return depth;
}
// If we've gone as far as we can go and haven't found it...
if (Throwable.class.equals(exceptionType)) {
return -1;
}
return getDepth(exceptionType, depth + 1, handlerExceptionType.getSuperclass());
}
}

With this modification, handlePropertyAccessException() is called when a ConversionNotSupportedException is thrown.


---

**Affects:** 3.0.1

**Reference URL:** http://forum.springsource.org/showthread.php?t=87590

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/33252495cfea895f61aef38c56b59a660ef5ae75
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11745","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11745/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11745/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11745/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11745","id":398104454,"node_id":"MDU6SXNzdWUzOTgxMDQ0NTQ=","number":11745,"title":"AnnotationMethodHandlerExceptionResolver incorrectly resolving exceptions [SPR-7085]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70","html_url":"https://github.com/spring-projects/spring-framework/milestone/70","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70/labels","id":3960843,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mw==","number":70,"title":"3.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":117,"state":"closed","created_at":"2019-01-10T22:03:16Z","updated_at":"2019-01-11T02:35:54Z","due_on":"2010-06-13T07:00:00Z","closed_at":"2019-01-10T22:03:16Z"},"comments":1,"created_at":"2010-04-09T08:25:00Z","updated_at":"2012-06-19T03:43:58Z","closed_at":"2012-06-19T03:43:58Z","author_association":"COLLABORATOR","body":"**[Jeff T](https://jira.spring.io/secure/ViewProfile.jspa?name=jeff_t)** opened **[SPR-7085](https://jira.spring.io/browse/SPR-7085?redirect=false)** and commented\n\nI have a controller class with the following exception handler methods:\n\nCode:\n\n`@ExceptionHandler`({PropertyAccessException.class})\n`@ResponseStatus`(HttpStatus.BAD_REQUEST)\npublic ModelAndView handlePropertyAccessException(PropertyAccessException ex) {\n// create modelAndView and process ex\nreturn modelAndView;\n}\n\n`@ExceptionHandler`({Exception.class})\n`@ResponseStatus`(HttpStatus.INTERNAL_SERVER_ERROR)\npublic ModelAndView handleException(Exception ex) {\n// create modelAndView and process ex\nreturn modelAndView;\n}\n\nWhen a ConversionNotSupportedException (subclass of PropertyAccessException) is thrown, the handleException() method is called instead of the expected handlePropertyAccessException() method.\n\nDigging into the AnnotationMethodHandlerExceptionResolver class I find the following private Comparator. This comparator returns 0 when o1 is Exception, o2 is PropertyAccessException, and handlerExceptionType is ConversionNotSupportedException. This is incorrect and the cause of the problem.\n\nCode:\n\n/** Comparator capable of sorting exceptions based on their depth from the thrown exception type. */\nprivate static class DepthComparator implements Comparator<Class<? extends Throwable>> {\nprivate final Class<? extends Throwable> handlerExceptionType;\nprivate DepthComparator(Exception handlerException) {\nthis.handlerExceptionType = handlerException.getClass();\n}\npublic int compare(Class<? extends Throwable> o1, Class<? extends Throwable> o2) {\nint depth1 = getDepth(o1, 0);\nint depth2 = getDepth(o2, 0);\nreturn depth2 - depth1;\n}\nprivate int getDepth(Class exceptionType, int depth) {\nif (exceptionType.equals(handlerExceptionType)) {\n// Found it!\nreturn depth;\n}\n// If we've gone as far as we can go and haven't found it...\nif (Throwable.class.equals(exceptionType)) {\nreturn -1;\n}\nreturn getDepth(exceptionType.getSuperclass(), depth + 1);\n}\n}\n\nI modified the class as follows. With this modification, the comparator returns 4 when o1 is Exception, o2 is PropertyAccessException, and handlerExceptionType is ConversionNotSupportedException. This results in the correct behavior:\n\nCode:\n\n/** Comparator capable of sorting exceptions based on their depth from the thrown exception type. */\nprivate static class DepthComparator implements Comparator<Class<? extends Throwable>> {\nprivate final Class<? extends Throwable> handlerExceptionType;\nprivate DepthComparator(Exception handlerException) {\nthis.handlerExceptionType = handlerException.getClass();\n}\npublic int compare(Class<? extends Throwable> o1, Class<? extends Throwable> o2) {\nint depth1 = getDepth(o1, 0, handlerExceptionType);\nint depth2 = getDepth(o2, 0, handlerExceptionType);\nreturn depth1 - depth2;\n}\nprivate int getDepth(Class exceptionType, int depth, Class handlerExceptionType) {\nif (exceptionType.equals(handlerExceptionType)) {\n// Found it!\nreturn depth;\n}\n// If we've gone as far as we can go and haven't found it...\nif (Throwable.class.equals(exceptionType)) {\nreturn -1;\n}\nreturn getDepth(exceptionType, depth + 1, handlerExceptionType.getSuperclass());\n}\n}\n\nWith this modification, handlePropertyAccessException() is called when a ConversionNotSupportedException is thrown.\n\n\n---\n\n**Affects:** 3.0.1\n\n**Reference URL:** http://forum.springsource.org/showthread.php?t=87590\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/33252495cfea895f61aef38c56b59a660ef5ae75\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453349999"], "labels":["in: web","type: bug"]}