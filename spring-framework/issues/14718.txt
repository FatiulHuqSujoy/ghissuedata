{"id":"14718", "title":"HTTP 405 (method not supported) returned instead of 406 (not acceptable) when one URL pattern supports 2 or more HTTP methods [SPR-10085]", "body":"**[Daniel Gredler](https://jira.spring.io/secure/ViewProfile.jspa?name=gredler2)** opened **[SPR-10085](https://jira.spring.io/browse/SPR-10085?redirect=false)** and commented

If I define an API that supports two HTTP methods on the same URL pattern, e.g. \"GET /cars/(id)\" (to retrieve a car) and \"DELETE /cars/(id)\" (to delete a car), then sending an unacceptable MIME type in a GET request's \"Accept\" header results in a HTTP 405 error (method not supported), instead of a HTTP 406 (not acceptable) error.

For example:

```
@Controller
@RequestMapping(value = \"/mailitems\")
public class MailItemsImpl implements MailItems {

    private static final String XML = \"application/xml\";
    private static final String JSON = \"application/json\";

    /** {@inheritDoc} */
    @Override
    @RequestMapping(method = POST, value = \"/\", consumes = { XML, JSON }, produces = { XML, JSON })
    public @ResponseBody MailItem createMailItem(@RequestBody MailItem mailItem) {
        return mailItem;
    }

    /** {@inheritDoc} */
    @Override
    @RequestMapping(method = GET, value = \"/{id}\", produces = { XML, JSON })
    public @ResponseBody MailItem getMailItem(@PathVariable long id) {
        return new MailItem();
    }

    /** {@inheritDoc} */
    @Override
    @RequestMapping(method = DELETE, value = \"/{id}\")
    public @ResponseBody void deleteMailItem(@PathVariable long id) {
        // Empty.
    }
}
```

Test code (using REST-assured):

```
@Test
public void testGetWithUnknownOutput() {
    given()
        .header(\"Accept\", \"application/foo\")
    .expect()
        .statusCode(406) // XXX: expected 406, but we get 405...
    .when()
        .get(\"/rest/mailitems/5\");
}
```

The test passes if you remove the DELETE mapping in `MailItemsImpl`.

The issue appears to be in `RequestMappingInfoHandlerMapping.handleNoMatch()`, which somehow determines that although the GET request could not be honored due to the Accept header, the HTTP error should indicate that GET is not supported, and only DELETE is supported.

This is similar to #12037, but I think it's a different issue.

---

**Affects:** 3.1.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14718","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14718/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14718/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14718/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14718","id":398155497,"node_id":"MDU6SXNzdWUzOTgxNTU0OTc=","number":14718,"title":"HTTP 405 (method not supported) returned instead of 406 (not acceptable) when one URL pattern supports 2 or more HTTP methods [SPR-10085]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2012-12-07T12:50:52Z","updated_at":"2019-01-12T16:47:21Z","closed_at":"2012-12-13T13:00:49Z","author_association":"COLLABORATOR","body":"**[Daniel Gredler](https://jira.spring.io/secure/ViewProfile.jspa?name=gredler2)** opened **[SPR-10085](https://jira.spring.io/browse/SPR-10085?redirect=false)** and commented\n\nIf I define an API that supports two HTTP methods on the same URL pattern, e.g. \"GET /cars/(id)\" (to retrieve a car) and \"DELETE /cars/(id)\" (to delete a car), then sending an unacceptable MIME type in a GET request's \"Accept\" header results in a HTTP 405 error (method not supported), instead of a HTTP 406 (not acceptable) error.\n\nFor example:\n\n```\n@Controller\n@RequestMapping(value = \"/mailitems\")\npublic class MailItemsImpl implements MailItems {\n\n    private static final String XML = \"application/xml\";\n    private static final String JSON = \"application/json\";\n\n    /** {@inheritDoc} */\n    @Override\n    @RequestMapping(method = POST, value = \"/\", consumes = { XML, JSON }, produces = { XML, JSON })\n    public @ResponseBody MailItem createMailItem(@RequestBody MailItem mailItem) {\n        return mailItem;\n    }\n\n    /** {@inheritDoc} */\n    @Override\n    @RequestMapping(method = GET, value = \"/{id}\", produces = { XML, JSON })\n    public @ResponseBody MailItem getMailItem(@PathVariable long id) {\n        return new MailItem();\n    }\n\n    /** {@inheritDoc} */\n    @Override\n    @RequestMapping(method = DELETE, value = \"/{id}\")\n    public @ResponseBody void deleteMailItem(@PathVariable long id) {\n        // Empty.\n    }\n}\n```\n\nTest code (using REST-assured):\n\n```\n@Test\npublic void testGetWithUnknownOutput() {\n    given()\n        .header(\"Accept\", \"application/foo\")\n    .expect()\n        .statusCode(406) // XXX: expected 406, but we get 405...\n    .when()\n        .get(\"/rest/mailitems/5\");\n}\n```\n\nThe test passes if you remove the DELETE mapping in `MailItemsImpl`.\n\nThe issue appears to be in `RequestMappingInfoHandlerMapping.handleNoMatch()`, which somehow determines that although the GET request could not be honored due to the Accept header, the HTTP error should indicate that GET is not supported, and only DELETE is supported.\n\nThis is similar to #12037, but I think it's a different issue.\n\n---\n\n**Affects:** 3.1.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453399324","453399326","453399327"], "labels":["in: web","status: duplicate"]}