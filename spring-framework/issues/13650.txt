{"id":"13650", "title":"Provide support for ApplicationContextInitializers in the TestContext framework [SPR-9011]", "body":"**[Rossen Stoyanchev](https://jira.spring.io/secure/ViewProfile.jspa?name=rstoya05-aop)** opened **[SPR-9011](https://jira.spring.io/browse/SPR-9011?redirect=false)** and commented

#### Status Quo

Starting with Spring 3.1 applications can specify `contextInitializerClasses` via `context-param` and `init-param` in `web.xml`.

---

#### Goals

For comprehensive testing it should be possible to re-use `ApplicationContextInitializer` instances in tests as well.

This could be done at the `@ContextConfiguration` level by allowing an array of ACI types to be specified, and the TCF would allow each to visit the `ApplicationContext` at the right time.

---

#### Deliverables

1. 

[x] Introduce a new `initializers` attribute in `@ContextConfiguration`.

---

```
     ```

```

Class<? extends ApplicationContextInitializer<? extends ConfigurableApplicationContext>>[] initializers() default {};

```
1. [x] Introduce a new `inheritInitializers` attribute in `@ContextConfiguration`.
   - ```
boolean inheritInitializers() default true;
```

1. [x] Allow a context to be loaded solely via a custom `ApplicationContextInitializer` (i.e., without locations or classes)

2. [x] Initializers must be included in `MergedContextConfiguration` for determining the context cache key.

3. [x] Invoke initializers within existing `SmartContextLoader` implementations.

   - for example, in `AbstractGenericContextLoader.loadContext(...)` methods

   - 

   [x] per the contract defined in the Javadoc for `ApplicationContextInitializer`: _`ApplicationContextInitializer` processors are encouraged to detect whether Spring's `Ordered` interface has been implemented or if the `@Order` annotation is present and to sort instances accordingly if so prior to invocation._
   ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   ```
          ```

   ```

Collections.sort(initializerInstances, new AnnotationAwareOrderComparator());

```
1. [x] Document in Javadoc
1. [x] Document in the reference manual

----
#### Pseudocode Examples

```java 
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    locations = \"/app-config.xml\",
    initializers = CustomInitializer.class)
public class ApplicationContextInitializerTests {}
```

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    locations = \"/app-config.xml\",
    initializers = {PropertySourceInitializer.class, ProfileInitializer.class})
public class ApplicationContextInitializerTests {}
```

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    classes = BaseConfig.class,
    initializers = BaseInitializer.class)
public class BaseTest {}

@ContextConfiguration(
    classes = ExtendedConfig.class,
    initializers = ExtendedInitializer.class)
public class ExtendedTest extends BaseTest {}
```

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    classes = BaseConfig.class,
    initializers = BaseInitializer.class)
public class BaseTest {}

@ContextConfiguration(
    classes = ExtendedConfig.class,
    initializers = ExtendedInitializer.class,
    inheritInitializers = false)
public class ExtendedTest extends BaseTest {}
```

```java
// In the following example, an exception would not be thrown even
// if no default XML file or @Configuration class is detected.
// In other words, the initializer would be responsible for 
// providing XML configuration files or annotated configuration
// classes to the provided context.
@ContextConfiguration(initializers = EntireAppInitializer.class)
public class InitializerWithoutConfigFilesOrClassesTest extends BaseTest  {}
```

---

**Affects:** 3.1 GA

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1f93777bbd9ec10ac1693a1362231a3f2f74aa8d, https://github.com/spring-projects/spring-framework/commit/58daeea1e2d7f3688057e0131cba5291a6f70fc2

11 votes, 15 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13650","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13650/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13650/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13650/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13650","id":398116566,"node_id":"MDU6SXNzdWUzOTgxMTY1NjY=","number":13650,"title":"Provide support for ApplicationContextInitializers in the TestContext framework [SPR-9011]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512024,"node_id":"MDU6TGFiZWwxMTg4NTEyMDI0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/has:%20votes-jira","name":"has: votes-jira","color":"dfdfdf","default":false},{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false},"assignees":[{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/85","html_url":"https://github.com/spring-projects/spring-framework/milestone/85","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/85/labels","id":3960858,"node_id":"MDk6TWlsZXN0b25lMzk2MDg1OA==","number":85,"title":"3.2 M2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":93,"state":"closed","created_at":"2019-01-10T22:03:33Z","updated_at":"2019-01-11T06:57:25Z","due_on":"2012-09-10T07:00:00Z","closed_at":"2019-01-10T22:03:33Z"},"comments":8,"created_at":"2012-01-09T06:17:55Z","updated_at":"2019-01-13T21:41:02Z","closed_at":"2012-08-20T06:38:58Z","author_association":"COLLABORATOR","body":"**[Rossen Stoyanchev](https://jira.spring.io/secure/ViewProfile.jspa?name=rstoya05-aop)** opened **[SPR-9011](https://jira.spring.io/browse/SPR-9011?redirect=false)** and commented\n\n#### Status Quo\n\nStarting with Spring 3.1 applications can specify `contextInitializerClasses` via `context-param` and `init-param` in `web.xml`.\n\n---\n\n#### Goals\n\nFor comprehensive testing it should be possible to re-use `ApplicationContextInitializer` instances in tests as well.\n\nThis could be done at the `@ContextConfiguration` level by allowing an array of ACI types to be specified, and the TCF would allow each to visit the `ApplicationContext` at the right time.\n\n---\n\n#### Deliverables\n\n1. \n\n[x] Introduce a new `initializers` attribute in `@ContextConfiguration`.\n\n---\n\n```\n     ```\n\n```\n\nClass<? extends ApplicationContextInitializer<? extends ConfigurableApplicationContext>>[] initializers() default {};\n\n```\n1. [x] Introduce a new `inheritInitializers` attribute in `@ContextConfiguration`.\n   - ```\nboolean inheritInitializers() default true;\n```\n\n1. [x] Allow a context to be loaded solely via a custom `ApplicationContextInitializer` (i.e., without locations or classes)\n\n2. [x] Initializers must be included in `MergedContextConfiguration` for determining the context cache key.\n\n3. [x] Invoke initializers within existing `SmartContextLoader` implementations.\n\n   - for example, in `AbstractGenericContextLoader.loadContext(...)` methods\n\n   - \n\n   [x] per the contract defined in the Javadoc for `ApplicationContextInitializer`: _`ApplicationContextInitializer` processors are encouraged to detect whether Spring's `Ordered` interface has been implemented or if the `@Order` annotation is present and to sort instances accordingly if so prior to invocation._\n   ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n   ```\n          ```\n\n   ```\n\nCollections.sort(initializerInstances, new AnnotationAwareOrderComparator());\n\n```\n1. [x] Document in Javadoc\n1. [x] Document in the reference manual\n\n----\n#### Pseudocode Examples\n\n```java \n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(\n    locations = \"/app-config.xml\",\n    initializers = CustomInitializer.class)\npublic class ApplicationContextInitializerTests {}\n```\n\n```java\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(\n    locations = \"/app-config.xml\",\n    initializers = {PropertySourceInitializer.class, ProfileInitializer.class})\npublic class ApplicationContextInitializerTests {}\n```\n\n```java\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(\n    classes = BaseConfig.class,\n    initializers = BaseInitializer.class)\npublic class BaseTest {}\n\n@ContextConfiguration(\n    classes = ExtendedConfig.class,\n    initializers = ExtendedInitializer.class)\npublic class ExtendedTest extends BaseTest {}\n```\n\n```java\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(\n    classes = BaseConfig.class,\n    initializers = BaseInitializer.class)\npublic class BaseTest {}\n\n@ContextConfiguration(\n    classes = ExtendedConfig.class,\n    initializers = ExtendedInitializer.class,\n    inheritInitializers = false)\npublic class ExtendedTest extends BaseTest {}\n```\n\n```java\n// In the following example, an exception would not be thrown even\n// if no default XML file or @Configuration class is detected.\n// In other words, the initializer would be responsible for \n// providing XML configuration files or annotated configuration\n// classes to the provided context.\n@ContextConfiguration(initializers = EntireAppInitializer.class)\npublic class InitializerWithoutConfigFilesOrClassesTest extends BaseTest  {}\n```\n\n---\n\n**Affects:** 3.1 GA\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1f93777bbd9ec10ac1693a1362231a3f2f74aa8d, https://github.com/spring-projects/spring-framework/commit/58daeea1e2d7f3688057e0131cba5291a6f70fc2\n\n11 votes, 15 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453364664","453364665","453364666","453364668","453364669","453364670","453364672","453364674"], "labels":["has: votes-jira","in: test","type: enhancement"]}