{"id":"5582", "title":"Make use of TransactionAwareDataSourceProxy transparent [SPR-856]", "body":"**[Lance Eason](https://jira.spring.io/secure/ViewProfile.jspa?name=leason)** opened **[SPR-856](https://jira.spring.io/browse/SPR-856?redirect=false)** and commented

Declarative transaction management appears to work when DataSourceTransactionManager is handed a datasource that's wrapped by a TransactionAwareDataSourceProxy but in reality all transactional semantics are lost.  I'd be equally satisfied with any of the following three improvements:
1) This was clearly documented
2) An exception, e.g. IllegalStateException, was thrown with this configuration
3) It worked

From the mailing list:

From: Matt Sgarlata [mailto:sgarlatm@users.sourceforge.net]
Sent: Tuesday, April 05, 2005 3:18 PM
To: springframework-user@lists.sourceforge.net
Subject: [Springframework-user] Re: limitation with TransactionAwareDataSourceProxy

Hi Lance,

TransactionManagers were written with the intent that they would be passed the raw underlying datasource.  However, I think you have a good
point: TransactionManagers should be able to work with either the TransactionalAwareDataSourceProxy or with the raw underlying DataSource.

The TransactionalAwareDataSourceProxy is a Spring class, after all.
Perhaps you can enter this as an enhancement request in JIRA?

Matt

Lance Eason wrote:

> Hi, newbie to Spring and trying to get declarative transactions
> working.
> I spent the morning debugging an issue related to
> TransactionAwareDataSourceProxy so I thought I'd call attention to it
> and hopefully save the next guy some time.
> 
> I've got legacy data access classes (converting over from EJB) that I
> don't want to have to re-write immediately to be Spring transaction
> friendly.  From the documentation I learned that Spring will support
> this, I can just wrap my DataSource with a
> TransactionalAwareDataSourceProxy and when my classes call
> DataSource.getConnection() they'll get the transaction's connection
> and
> be none the wiser.  So I took a stab at it and found I wasn't getting
> transactional behavior.  My mapping file looked like this:
> 
> <bean id=\"dataSource\"

class=\"org.springframework.jdbc.datasource.TransactionAwareDataSourcePro

> xy\">
> \\<property name=\"targetDataSource\">
> \\<bean class=\"org.apache.commons.dbcp.BasicDataSource\">
> \\<property name=\"driverClassName\">
> \\<value>${jdbc.driver.name}\\</value>
> \\</property>
> \\<property name=\"url\">
> \\<value>${jdbc.bmi.server.url}\\</value>
> \\</property>
> \\<property name=\"username\">
> \\<value>${jdbc.bmi.username}\\</value>
> \\</property>
> \\<property name=\"password\">
> \\<value>${jdbc.bmi.password}\\</value>
> \\</property>
> \\</bean>
> \\</property>
> \\</bean>
> 
> <bean id=\"transactionManager\"

class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\"

> \\<property name=\"dataSource\">
> 
>> \\<ref bean=\"dataSource\"/>
>> \\</property>

\\</bean>
\\<bean id=\"LegacyJDBC\" class=\"test.spring.LegacyJDBC\">

> \\<constructor-arg>
>    \\<ref bean=\"dataSource\"/>
> \\</constructor-arg>

\\</bean>

I have a DBCP BasicDataSoure wrapped by a
TransactionAwareDataSourceProxy and that datasource is being injected
into my legacy JDBC class as well as the transaction manager.  The
legacy JDBC class is called by a higher level class wrapped with a
TransactionProxyFactoryBean (omitted).  So theoretically my
transaction
manager will start a transaction when it hits my higher level class
and
the following line in my legacy JDBC class will find the connection
associated with the transaction via the magic of the proxy:

Connection connection = dataSource.getConnection();

As it turns out theory didn't hold, I wasn't getting transactional
behavior.  Adding debugging to my JDBC class revealed I was getting a
different connection where autocommit was set to true:

Connection connection = dataSource.getConnection();
System.err.println(\"using connection: \" + connection);
System.err.println(\"auto? \" + connection.getAutoCommit());

Connection connection2 = DataSourceUtils.getConnection(dataSource);
System.err.println(\"util connection: \" + connection2);
System.err.println(\"auto? \" + connection2.getAutoCommit());

with the output:

using connection: org.apache.commons.dbcp.PoolableConnection@fe1904
auto? true
util connection: org.apache.commons.dbcp.PoolableConnection@adb1d4
auto? false

Turns out the problem is with passing the
TransactionAwareDataSourceProxy to the transaction manager,
specifically
with this stacktrace:

at

org.springframework.jdbc.datasource.DataSourceUtils.doGetConnection(Data

> SourceUtils.java:177)
> at

org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy.getC

> onnection(TransactionAwareDataSourceProxy.java:79)
> at

org.springframework.jdbc.datasource.DataSourceUtils.doGetConnection(Data

> SourceUtils.java:177)
> at

org.springframework.jdbc.datasource.DataSourceUtils.getConnection(DataSo

> urceUtils.java:152)
> at

org.springframework.jdbc.datasource.DataSourceTransactionManager.doBegin

> (TestTransactionManager.java:101)
> 
> The transaction manager is starting a new transaction so it's calling
> DataSourceUtils.getConnection/doGetConnection to get a connection for
> the the transaction.  The datasource is really a proxy that turns
> around
> and calls DataSourceUtils.doGetConnection a second nested time.
> Something in the nesting screws things up.
> 
> Splitting up my bean definition for my datasource into two bean
> definitions, one for the raw datasource and one for the proxied data
> source, and passing the raw datasource to the transaction manager
> fixes
> the problem:
> 
> \\<bean id=\"dataSource\"
> class=\"org.apache.commons.dbcp.BasicDataSource\">
> \\<property name=\"driverClassName\">
> \\<value>${jdbc.driver.name}\\</value>
> \\</property>
> \\<property name=\"url\">
> \\<value>${jdbc.bmi.server.url}\\</value>
> \\</property>
> \\<property name=\"username\">
> \\<value>${jdbc.bmi.username}\\</value>
> \\</property>
> \\<property name=\"password\">
> \\<value>${jdbc.bmi.password}\\</value>
> \\</property>
> \\</bean>
> 
> <bean id=\"dataSourceTx\"

class=\"org.springframework.jdbc.datasource.TransactionAwareDataSourcePro

> xy\">
> \\<property name=\"targetDataSource\">
> \\<ref bean=\"dataSource\"/>
> \\</property>
> \\</bean>
> 
> <bean id=\"transactionManager\"

class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\"

> \\<property name=\"dataSource\">
> 
>> \\<ref bean=\"dataSource\"/>
>> \\</property>

\\</bean>
\\<bean id=\"LegacyJDBC\" class=\"test.spring.LegacyJDBC\">

> \\<constructor-arg>
>    \\<ref bean=\"dataSourceTx\"/>
> \\</constructor-arg>

\\</bean>

---

**Affects:** 1.1.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5582","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5582/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5582/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5582/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5582","id":398056222,"node_id":"MDU6SXNzdWUzOTgwNTYyMjI=","number":5582,"title":"Make use of TransactionAwareDataSourceProxy transparent [SPR-856]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/17","html_url":"https://github.com/spring-projects/spring-framework/milestone/17","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/17/labels","id":3960787,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Nw==","number":17,"title":"1.2 RC2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":43,"state":"closed","created_at":"2019-01-10T22:02:12Z","updated_at":"2019-01-10T23:14:14Z","due_on":null,"closed_at":"2019-01-10T22:02:12Z"},"comments":1,"created_at":"2005-04-05T18:44:07Z","updated_at":"2019-01-13T22:52:05Z","closed_at":"2005-08-03T21:35:11Z","author_association":"COLLABORATOR","body":"**[Lance Eason](https://jira.spring.io/secure/ViewProfile.jspa?name=leason)** opened **[SPR-856](https://jira.spring.io/browse/SPR-856?redirect=false)** and commented\n\nDeclarative transaction management appears to work when DataSourceTransactionManager is handed a datasource that's wrapped by a TransactionAwareDataSourceProxy but in reality all transactional semantics are lost.  I'd be equally satisfied with any of the following three improvements:\n1) This was clearly documented\n2) An exception, e.g. IllegalStateException, was thrown with this configuration\n3) It worked\n\nFrom the mailing list:\n\nFrom: Matt Sgarlata [mailto:sgarlatm@users.sourceforge.net]\nSent: Tuesday, April 05, 2005 3:18 PM\nTo: springframework-user@lists.sourceforge.net\nSubject: [Springframework-user] Re: limitation with TransactionAwareDataSourceProxy\n\nHi Lance,\n\nTransactionManagers were written with the intent that they would be passed the raw underlying datasource.  However, I think you have a good\npoint: TransactionManagers should be able to work with either the TransactionalAwareDataSourceProxy or with the raw underlying DataSource.\n\nThe TransactionalAwareDataSourceProxy is a Spring class, after all.\nPerhaps you can enter this as an enhancement request in JIRA?\n\nMatt\n\nLance Eason wrote:\n\n> Hi, newbie to Spring and trying to get declarative transactions\n> working.\n> I spent the morning debugging an issue related to\n> TransactionAwareDataSourceProxy so I thought I'd call attention to it\n> and hopefully save the next guy some time.\n> \n> I've got legacy data access classes (converting over from EJB) that I\n> don't want to have to re-write immediately to be Spring transaction\n> friendly.  From the documentation I learned that Spring will support\n> this, I can just wrap my DataSource with a\n> TransactionalAwareDataSourceProxy and when my classes call\n> DataSource.getConnection() they'll get the transaction's connection\n> and\n> be none the wiser.  So I took a stab at it and found I wasn't getting\n> transactional behavior.  My mapping file looked like this:\n> \n> <bean id=\"dataSource\"\n\nclass=\"org.springframework.jdbc.datasource.TransactionAwareDataSourcePro\n\n> xy\">\n> \\<property name=\"targetDataSource\">\n> \\<bean class=\"org.apache.commons.dbcp.BasicDataSource\">\n> \\<property name=\"driverClassName\">\n> \\<value>${jdbc.driver.name}\\</value>\n> \\</property>\n> \\<property name=\"url\">\n> \\<value>${jdbc.bmi.server.url}\\</value>\n> \\</property>\n> \\<property name=\"username\">\n> \\<value>${jdbc.bmi.username}\\</value>\n> \\</property>\n> \\<property name=\"password\">\n> \\<value>${jdbc.bmi.password}\\</value>\n> \\</property>\n> \\</bean>\n> \\</property>\n> \\</bean>\n> \n> <bean id=\"transactionManager\"\n\nclass=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\"\n\n> \\<property name=\"dataSource\">\n> \n>> \\<ref bean=\"dataSource\"/>\n>> \\</property>\n\n\\</bean>\n\\<bean id=\"LegacyJDBC\" class=\"test.spring.LegacyJDBC\">\n\n> \\<constructor-arg>\n>    \\<ref bean=\"dataSource\"/>\n> \\</constructor-arg>\n\n\\</bean>\n\nI have a DBCP BasicDataSoure wrapped by a\nTransactionAwareDataSourceProxy and that datasource is being injected\ninto my legacy JDBC class as well as the transaction manager.  The\nlegacy JDBC class is called by a higher level class wrapped with a\nTransactionProxyFactoryBean (omitted).  So theoretically my\ntransaction\nmanager will start a transaction when it hits my higher level class\nand\nthe following line in my legacy JDBC class will find the connection\nassociated with the transaction via the magic of the proxy:\n\nConnection connection = dataSource.getConnection();\n\nAs it turns out theory didn't hold, I wasn't getting transactional\nbehavior.  Adding debugging to my JDBC class revealed I was getting a\ndifferent connection where autocommit was set to true:\n\nConnection connection = dataSource.getConnection();\nSystem.err.println(\"using connection: \" + connection);\nSystem.err.println(\"auto? \" + connection.getAutoCommit());\n\nConnection connection2 = DataSourceUtils.getConnection(dataSource);\nSystem.err.println(\"util connection: \" + connection2);\nSystem.err.println(\"auto? \" + connection2.getAutoCommit());\n\nwith the output:\n\nusing connection: org.apache.commons.dbcp.PoolableConnection@fe1904\nauto? true\nutil connection: org.apache.commons.dbcp.PoolableConnection@adb1d4\nauto? false\n\nTurns out the problem is with passing the\nTransactionAwareDataSourceProxy to the transaction manager,\nspecifically\nwith this stacktrace:\n\nat\n\norg.springframework.jdbc.datasource.DataSourceUtils.doGetConnection(Data\n\n> SourceUtils.java:177)\n> at\n\norg.springframework.jdbc.datasource.TransactionAwareDataSourceProxy.getC\n\n> onnection(TransactionAwareDataSourceProxy.java:79)\n> at\n\norg.springframework.jdbc.datasource.DataSourceUtils.doGetConnection(Data\n\n> SourceUtils.java:177)\n> at\n\norg.springframework.jdbc.datasource.DataSourceUtils.getConnection(DataSo\n\n> urceUtils.java:152)\n> at\n\norg.springframework.jdbc.datasource.DataSourceTransactionManager.doBegin\n\n> (TestTransactionManager.java:101)\n> \n> The transaction manager is starting a new transaction so it's calling\n> DataSourceUtils.getConnection/doGetConnection to get a connection for\n> the the transaction.  The datasource is really a proxy that turns\n> around\n> and calls DataSourceUtils.doGetConnection a second nested time.\n> Something in the nesting screws things up.\n> \n> Splitting up my bean definition for my datasource into two bean\n> definitions, one for the raw datasource and one for the proxied data\n> source, and passing the raw datasource to the transaction manager\n> fixes\n> the problem:\n> \n> \\<bean id=\"dataSource\"\n> class=\"org.apache.commons.dbcp.BasicDataSource\">\n> \\<property name=\"driverClassName\">\n> \\<value>${jdbc.driver.name}\\</value>\n> \\</property>\n> \\<property name=\"url\">\n> \\<value>${jdbc.bmi.server.url}\\</value>\n> \\</property>\n> \\<property name=\"username\">\n> \\<value>${jdbc.bmi.username}\\</value>\n> \\</property>\n> \\<property name=\"password\">\n> \\<value>${jdbc.bmi.password}\\</value>\n> \\</property>\n> \\</bean>\n> \n> <bean id=\"dataSourceTx\"\n\nclass=\"org.springframework.jdbc.datasource.TransactionAwareDataSourcePro\n\n> xy\">\n> \\<property name=\"targetDataSource\">\n> \\<ref bean=\"dataSource\"/>\n> \\</property>\n> \\</bean>\n> \n> <bean id=\"transactionManager\"\n\nclass=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\"\n\n> \\<property name=\"dataSource\">\n> \n>> \\<ref bean=\"dataSource\"/>\n>> \\</property>\n\n\\</bean>\n\\<bean id=\"LegacyJDBC\" class=\"test.spring.LegacyJDBC\">\n\n> \\<constructor-arg>\n>    \\<ref bean=\"dataSourceTx\"/>\n> \\</constructor-arg>\n\n\\</bean>\n\n---\n\n**Affects:** 1.1.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453293269"], "labels":["in: data","type: enhancement"]}