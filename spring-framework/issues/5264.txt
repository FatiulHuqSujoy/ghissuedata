{"id":"5264", "title":"Improve documentation on scheduling support [SPR-535]", "body":"**[Tim Nolan](https://jira.spring.io/secure/ViewProfile.jspa?name=katentim)** opened **[SPR-535](https://jira.spring.io/browse/SPR-535?redirect=false)** and commented

RCS file: /cvsroot/springframework/spring/docs/reference/src/scheduling.xml,v
retrieving revision 1.5
diff -u -r1.5 scheduling.xml
--- scheduling.xml	22 Nov 2004 16:31:31 -0000	1.5
+++ scheduling.xml	9 Dec 2004 11:37:57 -0000
@@ -5,12 +5,12 @@

\\<title>Introduction\\</title>
\\<para>
Spring features integration classes for scheduling support. Currently, Spring
-------------------------------------------------------------------------------------------------------------------

    supports the Timer, built in in the JDK since 1.3 and the Quartz Scheduler

+ 

      supports the Timer, built in the JDK since 1.3 and the Quartz Scheduler
      (<ulink url=\"http://www.quartzscheduler.org\"/>). Both schedulers are set up

- 

      using a FactoryBean with optional references to Timers respectively Triggers.

+ 

      using a FactoryBean with optional references to Timers or Triggers.
      Furthermore, a convenience class for both the Quartz Scheduler and the Timer is

- 

      available that allows you to invoke a method an a target object you can specify

- 

      yourself (analogous to normal <literal>MethodInvokingFactoryBeans</literal>

+ 

      available that allows you to invoke a method and a target object that you can

+ 

      specify yourself (analogous to normal <literal>MethodInvokingFactoryBeans</literal>

  \\</para>
  \\</sect1>

@@ -29,8 +29,8 @@
\\<para>
\\<literal>JobDetail\\</literal> objects contain all information needed to
run a job. Spring provides a so-called \\<literal>JobDetailBean\\</literal>

---

    that makes the JobDetail more of an actual with sensible defaults.

- 

      Let's have a look at an example:

+ 

      that makes the JobDetail more of an actual Spring JavaBean with sensible

+ 

      defaults. Let's have a look at an example:
      <programlisting><![CDATA[

\\<bean name=\"exampleJob\" class=\"org.springframework.scheduling.quartz.JobDetailBean\">
\\<property name=\"jobClass\">
@@ -117,12 +117,12 @@
business object and wire up the detail object.
\\</para>
\\<para>
- 

      By default, Quartz Jobs are stateless, resulting in the possibility of jobs interfering

- 

      with eachother. If you specify two triggers for the same JobDetail, it might be possible

+ 

      By default, Quartz Jobs are stateless, resulting in the possibility of jobs interferring

+ 

      with each other. If you specify two triggers for the same JobDetail, it might be possible
      that before the first job has finished, the second one will start. If JobDetail objects
      implement the Stateful interface, this won't happen. The second job will not start

- 

      before the first one has finished. To make jobs resulting from the MethodInvokingJobDetailFactoryBEan

- 

      non-concurrent set the \\<literal>concurrent\\</literal> flag to \\<literal>false\\</literal>.

+ 

      before the first one has finished. To make jobs resulting from the MethodInvokingJobDetailFactoryBean

+ 

      non-concurrent, set the \\<literal>concurrent\\</literal> flag to \\<literal>false\\</literal>.
      \\<programlisting>\\<![CDATA[

\\<bean id=\"methodInvokingJobDetail\"
class=\"org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean\">
@@ -147,7 +147,7 @@
\\</para>
\\<para>
Triggers need to be scheduled. Spring offers a SchedulerFactoryBean exposing properties
- 

      to set te triggers. The SchedulerFactoryBean schedules the actual triggers.

+ 

           to set the triggers. The SchedulerFactoryBean schedules the actual triggers.
      \\</para>
      \\<para>
           A couple of examples:

@@ -192,7 +192,7 @@
]]>\\</programlisting>
More properties are available for the SchedulerFactoryBean for you to set, such as the
Calendars used by the job details, properties to customize Quartz with, etcetera. Have a look
- 

      at the JavaDOC (\\<ulink url=\"http://www.springframework.org/docs/api/org/springframework/scheduling/quartz/SchedulerFactoryBean.html\"/>) for more information.

+ 

           at the JavaDoc (\\<ulink url=\"http://www.springframework.org/docs/api/org/springframework/scheduling/quartz/SchedulerFactoryBean.html\"/>) for more information.
      \\</para>

  \\</sect2>
  \\</sect1>	
  @@ -260,7 +260,7 @@
  \\<sect2>
  \\<title>Using the MethodInvokingTimerTaskFactoryBean\\</title>
  \\<para>

- 

      Just as the Quartz support, the Timer support also features a component that

+ 

      Similar to the Quartz support, the Timer support also features a component that
      allows you to periodically invoke a method:
      \\<programlisting>\\<![CDATA[

\\<bean id=\"methodInvokingTask\"
@@ -283,13 +283,13 @@
]]>\\</programlisting>

    Changing the reference of the above example in which the ScheduledTimerTask is mentioned to the

- 

      <literal>methodInvokingTask</literal> will result in this task to be executed.

+ 

           <literal>methodInvokingTask</literal> will result in this task being executed.
      </para>

  \\</sect2>
  \\<sect2>
  \\<title>Wrapping up: setting up the tasks using the TimerFactoryBean\\</title>
  \\<para>

- 

      The TimerFactoryBean is similar to the QuartzSchedulerFactoryBean in that is serves the same

+ 

      The TimerFactoryBean is similar to the QuartzSchedulerFactoryBean in that it serves the same
      purpose: setting up the actual scheduling. The TimerFactoryBean sets up an actual Timer and
      schedules the tasks it has references to. You can specify whether or not daemon threads should
      be used.

---

No further details from [SPR-535](https://jira.spring.io/browse/SPR-535?redirect=false)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5264","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5264/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5264/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5264/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5264","id":398053366,"node_id":"MDU6SXNzdWUzOTgwNTMzNjY=","number":5264,"title":"Improve documentation on scheduling support [SPR-535]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/13","html_url":"https://github.com/spring-projects/spring-framework/milestone/13","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/13/labels","id":3960783,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Mw==","number":13,"title":"1.1.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":49,"state":"closed","created_at":"2019-01-10T22:02:06Z","updated_at":"2019-01-10T23:03:08Z","due_on":null,"closed_at":"2019-01-10T22:02:06Z"},"comments":1,"created_at":"2004-12-08T21:44:12Z","updated_at":"2019-01-13T22:53:15Z","closed_at":"2004-12-08T22:01:02Z","author_association":"COLLABORATOR","body":"**[Tim Nolan](https://jira.spring.io/secure/ViewProfile.jspa?name=katentim)** opened **[SPR-535](https://jira.spring.io/browse/SPR-535?redirect=false)** and commented\n\nRCS file: /cvsroot/springframework/spring/docs/reference/src/scheduling.xml,v\nretrieving revision 1.5\ndiff -u -r1.5 scheduling.xml\n--- scheduling.xml\t22 Nov 2004 16:31:31 -0000\t1.5\n+++ scheduling.xml\t9 Dec 2004 11:37:57 -0000\n@@ -5,12 +5,12 @@\n\n\\<title>Introduction\\</title>\n\\<para>\nSpring features integration classes for scheduling support. Currently, Spring\n-------------------------------------------------------------------------------------------------------------------\n\n    supports the Timer, built in in the JDK since 1.3 and the Quartz Scheduler\n\n+ \n\n      supports the Timer, built in the JDK since 1.3 and the Quartz Scheduler\n      (<ulink url=\"http://www.quartzscheduler.org\"/>). Both schedulers are set up\n\n- \n\n      using a FactoryBean with optional references to Timers respectively Triggers.\n\n+ \n\n      using a FactoryBean with optional references to Timers or Triggers.\n      Furthermore, a convenience class for both the Quartz Scheduler and the Timer is\n\n- \n\n      available that allows you to invoke a method an a target object you can specify\n\n- \n\n      yourself (analogous to normal <literal>MethodInvokingFactoryBeans</literal>\n\n+ \n\n      available that allows you to invoke a method and a target object that you can\n\n+ \n\n      specify yourself (analogous to normal <literal>MethodInvokingFactoryBeans</literal>\n\n  \\</para>\n  \\</sect1>\n\n@@ -29,8 +29,8 @@\n\\<para>\n\\<literal>JobDetail\\</literal> objects contain all information needed to\nrun a job. Spring provides a so-called \\<literal>JobDetailBean\\</literal>\n\n---\n\n    that makes the JobDetail more of an actual with sensible defaults.\n\n- \n\n      Let's have a look at an example:\n\n+ \n\n      that makes the JobDetail more of an actual Spring JavaBean with sensible\n\n+ \n\n      defaults. Let's have a look at an example:\n      <programlisting><![CDATA[\n\n\\<bean name=\"exampleJob\" class=\"org.springframework.scheduling.quartz.JobDetailBean\">\n\\<property name=\"jobClass\">\n@@ -117,12 +117,12 @@\nbusiness object and wire up the detail object.\n\\</para>\n\\<para>\n- \n\n      By default, Quartz Jobs are stateless, resulting in the possibility of jobs interfering\n\n- \n\n      with eachother. If you specify two triggers for the same JobDetail, it might be possible\n\n+ \n\n      By default, Quartz Jobs are stateless, resulting in the possibility of jobs interferring\n\n+ \n\n      with each other. If you specify two triggers for the same JobDetail, it might be possible\n      that before the first job has finished, the second one will start. If JobDetail objects\n      implement the Stateful interface, this won't happen. The second job will not start\n\n- \n\n      before the first one has finished. To make jobs resulting from the MethodInvokingJobDetailFactoryBEan\n\n- \n\n      non-concurrent set the \\<literal>concurrent\\</literal> flag to \\<literal>false\\</literal>.\n\n+ \n\n      before the first one has finished. To make jobs resulting from the MethodInvokingJobDetailFactoryBean\n\n+ \n\n      non-concurrent, set the \\<literal>concurrent\\</literal> flag to \\<literal>false\\</literal>.\n      \\<programlisting>\\<![CDATA[\n\n\\<bean id=\"methodInvokingJobDetail\"\nclass=\"org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean\">\n@@ -147,7 +147,7 @@\n\\</para>\n\\<para>\nTriggers need to be scheduled. Spring offers a SchedulerFactoryBean exposing properties\n- \n\n      to set te triggers. The SchedulerFactoryBean schedules the actual triggers.\n\n+ \n\n           to set the triggers. The SchedulerFactoryBean schedules the actual triggers.\n      \\</para>\n      \\<para>\n           A couple of examples:\n\n@@ -192,7 +192,7 @@\n]]>\\</programlisting>\nMore properties are available for the SchedulerFactoryBean for you to set, such as the\nCalendars used by the job details, properties to customize Quartz with, etcetera. Have a look\n- \n\n      at the JavaDOC (\\<ulink url=\"http://www.springframework.org/docs/api/org/springframework/scheduling/quartz/SchedulerFactoryBean.html\"/>) for more information.\n\n+ \n\n           at the JavaDoc (\\<ulink url=\"http://www.springframework.org/docs/api/org/springframework/scheduling/quartz/SchedulerFactoryBean.html\"/>) for more information.\n      \\</para>\n\n  \\</sect2>\n  \\</sect1>\t\n  @@ -260,7 +260,7 @@\n  \\<sect2>\n  \\<title>Using the MethodInvokingTimerTaskFactoryBean\\</title>\n  \\<para>\n\n- \n\n      Just as the Quartz support, the Timer support also features a component that\n\n+ \n\n      Similar to the Quartz support, the Timer support also features a component that\n      allows you to periodically invoke a method:\n      \\<programlisting>\\<![CDATA[\n\n\\<bean id=\"methodInvokingTask\"\n@@ -283,13 +283,13 @@\n]]>\\</programlisting>\n\n    Changing the reference of the above example in which the ScheduledTimerTask is mentioned to the\n\n- \n\n      <literal>methodInvokingTask</literal> will result in this task to be executed.\n\n+ \n\n           <literal>methodInvokingTask</literal> will result in this task being executed.\n      </para>\n\n  \\</sect2>\n  \\<sect2>\n  \\<title>Wrapping up: setting up the tasks using the TimerFactoryBean\\</title>\n  \\<para>\n\n- \n\n      The TimerFactoryBean is similar to the QuartzSchedulerFactoryBean in that is serves the same\n\n+ \n\n      The TimerFactoryBean is similar to the QuartzSchedulerFactoryBean in that it serves the same\n      purpose: setting up the actual scheduling. The TimerFactoryBean sets up an actual Timer and\n      schedules the tasks it has references to. You can specify whether or not daemon threads should\n      be used.\n\n---\n\nNo further details from [SPR-535](https://jira.spring.io/browse/SPR-535?redirect=false)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453290070"], "labels":["type: enhancement"]}