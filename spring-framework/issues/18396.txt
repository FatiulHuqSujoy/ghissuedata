{"id":"18396", "title":"Why Content type is null when produces=MediaType.TEXT_HTML_VALUE is used [SPR-13823]", "body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-13823](https://jira.spring.io/browse/SPR-13823?redirect=false)** and commented

Hello

This post mostly how a consult about internal work of Spring Framework about Spring MVC.

If I have a `@RequestMapping` method with produces for XML and/or JSON, thanks to Spring MVC Test with the `print()` method I can confirm the following:

```java
ModelAndView:
        View name = null
             View = null
            Model = null

FlashMap:
       Attributes = null

MockHttpServletResponse:
           Status = 200
    Error message = null
          Headers = {Content-Type=[application/xml]}
     Content type = application/xml
             Body = <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>
<persona>
    <id>100</id>
    <nombre>Something</nombre>
    <apellido>Something</apellido>
    <fecha>1977-12-08T00:00:00-05:00</fecha>
</persona>

    Forwarded URL = null
   Redirected URL = null
          Cookies = []
```

I can see `ModelAndView` empty and `MockHttpServletResponse` with `Content type = application/xml` and it is expected. Until here all is normal.

But if I have other `@RequestMapping` with `produces=MediaType.TEXT_HTML_VALUE` and of course returning a String (view-name) and Model. Again with `print()` method _now_ I can see the following:

```java
ModelAndView:
        View name = persona/findOne
             View = null
        Attribute = persona
            value = Persona [id=100, nombre=Jesús Você, apellido=Mão Nuñez, fecha=Thu Dec 08 00:00:00 PET 1977]
           errors = []

FlashMap:
       Attributes = null

MockHttpServletResponse:
           Status = 200
    Error message = null
          Headers = {}
     Content type = null
             Body = 
    Forwarded URL = /WEB-INF/view/jsp/persona/findOne.jsp
   Redirected URL = null
          Cookies = []
```

For me all _practically_ have sense, but I am with the doubt about why `Content type` is `null` and not text/html?. it mostly according with the `produces=MediaType.TEXT_HTML_VALUE` established.

Since I am returning a model I am ok that the `Body` is empty and `value` has the object _Persona_, but wondered in some way why `Content type` is `null`.

I did realise about this behaviour when in two different test methods (through Spock) I have:

* resultActions.andExpect(content().contentType(MediaType.APPLICATION_XML)) // OK
* resultActions.andExpect(content().contentType(MediaType.TEXT_HTML)) //fails.

(1) I ask for the explanation of this behaviour
(2) What could be the correct way to test or check if the content is text/html

**Note**: consider your answer to be included in Spring Reference documentation. (Of course, if has sense)

Thanks in advance.


---

**Affects:** 4.2 GA, 4.2.4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18396","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18396/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18396/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18396/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18396","id":398188158,"node_id":"MDU6SXNzdWUzOTgxODgxNTg=","number":18396,"title":"Why Content type is null when produces=MediaType.TEXT_HTML_VALUE is used [SPR-13823]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188511868,"node_id":"MDU6TGFiZWwxMTg4NTExODY4","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20documentation","name":"type: documentation","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false},"assignees":[{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2015-12-26T13:54:59Z","updated_at":"2015-12-29T01:11:00Z","closed_at":"2015-12-29T01:11:00Z","author_association":"COLLABORATOR","body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-13823](https://jira.spring.io/browse/SPR-13823?redirect=false)** and commented\n\nHello\n\nThis post mostly how a consult about internal work of Spring Framework about Spring MVC.\n\nIf I have a `@RequestMapping` method with produces for XML and/or JSON, thanks to Spring MVC Test with the `print()` method I can confirm the following:\n\n```java\nModelAndView:\n        View name = null\n             View = null\n            Model = null\n\nFlashMap:\n       Attributes = null\n\nMockHttpServletResponse:\n           Status = 200\n    Error message = null\n          Headers = {Content-Type=[application/xml]}\n     Content type = application/xml\n             Body = <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<persona>\n    <id>100</id>\n    <nombre>Something</nombre>\n    <apellido>Something</apellido>\n    <fecha>1977-12-08T00:00:00-05:00</fecha>\n</persona>\n\n    Forwarded URL = null\n   Redirected URL = null\n          Cookies = []\n```\n\nI can see `ModelAndView` empty and `MockHttpServletResponse` with `Content type = application/xml` and it is expected. Until here all is normal.\n\nBut if I have other `@RequestMapping` with `produces=MediaType.TEXT_HTML_VALUE` and of course returning a String (view-name) and Model. Again with `print()` method _now_ I can see the following:\n\n```java\nModelAndView:\n        View name = persona/findOne\n             View = null\n        Attribute = persona\n            value = Persona [id=100, nombre=Jesús Você, apellido=Mão Nuñez, fecha=Thu Dec 08 00:00:00 PET 1977]\n           errors = []\n\nFlashMap:\n       Attributes = null\n\nMockHttpServletResponse:\n           Status = 200\n    Error message = null\n          Headers = {}\n     Content type = null\n             Body = \n    Forwarded URL = /WEB-INF/view/jsp/persona/findOne.jsp\n   Redirected URL = null\n          Cookies = []\n```\n\nFor me all _practically_ have sense, but I am with the doubt about why `Content type` is `null` and not text/html?. it mostly according with the `produces=MediaType.TEXT_HTML_VALUE` established.\n\nSince I am returning a model I am ok that the `Body` is empty and `value` has the object _Persona_, but wondered in some way why `Content type` is `null`.\n\nI did realise about this behaviour when in two different test methods (through Spock) I have:\n\n* resultActions.andExpect(content().contentType(MediaType.APPLICATION_XML)) // OK\n* resultActions.andExpect(content().contentType(MediaType.TEXT_HTML)) //fails.\n\n(1) I ask for the explanation of this behaviour\n(2) What could be the correct way to test or check if the content is text/html\n\n**Note**: consider your answer to be included in Spring Reference documentation. (Of course, if has sense)\n\nThanks in advance.\n\n\n---\n\n**Affects:** 4.2 GA, 4.2.4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453434535","453434538"], "labels":["in: test","in: web","status: declined","type: documentation"]}