{"id":"22413", "title":"WebFlux with undertow throws exception when serving big static files.", "body":"<!--
!!! For Security Vulnerabilities, please go to https://pivotal.io/security !!!
-->
**Affects:** \\5.1.3, 5.0.*

---
<!--
Thanks for taking the time to create an issue. Please read the following:

- Questions should be asked on Stack Overflow.
- For bugs, specify affected versions and explain what you are trying to do.
- For enhancements, provide context and describe the problem.

Issue or Pull Request? Create only one, not both. GitHub treats them as the same.
If unsure, start with an issue, and if you submit a pull request later, the
issue will be closed as superseded.
-->
When serving big enough static resources (file), WebFlux + Undertow throws following exception:
`java.io.IOException: UT000094: Blocking await method called from IO thread.`

It is caused by `Channels.transferBlocking` call in `org.springframework.http.server.reactive.UndertowServerHttpResponse` class:

	@Override
	public Mono<Void> writeWith(Path file, long position, long count) {
		return doCommit(() ->
				Mono.defer(() -> {
					try (FileChannel source = FileChannel.open(file, StandardOpenOption.READ)) {
						StreamSinkChannel destination = this.exchange.getResponseChannel();
						Channels.transferBlocking(destination, source, position, count);
						return Mono.empty();
					}
					catch (IOException ex) {
						return Mono.error(ex);
					}
				}));
	}


Moving this call to IO threads seems to work, but migt be sub-optimal (see: `.subscribeOn(Schedulers.elastic())` near end):


	@Override
    public Mono<Void> writeWith(Path file, long position, long count) {
        return doCommit(() ->
                Mono.defer(() -> {
                    try (FileChannel source = FileChannel.open(file, StandardOpenOption.READ)) {
                        StreamSinkChannel destination = this.exchange.getResponseChannel();
                        Channels.transferBlocking(destination, source, position, count);
                        return Mono.empty();
                    }
                    catch (IOException ex) {
                        ex.printStackTrace();
                        return Mono.error(ex);
                    }
                }))
                // TO HANDLE BLOCKING!
                .subscribeOn(Schedulers.elastic())
                ;
    }


Stack trace:

	2019-02-14 14:19:29.329 ERROR 44484 --- [   XNIO-1 I/O-1] io.undertow                              : UT005085: Connection io.undertow.server.protocol.http.HttpServerConnection@13871769 for exchange HttpServerExchange{ GET /test.mp4 request {Accept=[*/*], Accept-Language=[pl-pl], Accept-Encoding=[identity], User-Agent=[Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15], Range=[bytes=0-1], Connection=[keep-alive], X-Playback-Session-Id=[B6657529-5085-4A47-AD60-089BDEC59DD5], Cookie=[Idea-e386df9=4ce8f3fb-c39f-415a-8ff1-c19f2999954d; Idea-e386df8=603d089f-ad6a-4efd-8e7b-186238fff603; _ga=GA1.1.524791181.1511035090], Referer=[http://localhost:8080/test.mp4], Host=[localhost:8080]} response {Connection=[keep-alive], Last-Modified=[Wed, 13 Feb 2019 19:58:38 GMT], Content-Range=[bytes 0-1/121486134], Content-Length=[2], Content-Type=[video/mp4], Accept-Ranges=[bytes], Date=[Thu, 14 Feb 2019 13:19:29 GMT]}} was not closed cleanly, forcibly closing connection
	java.io.IOException: UT000094: Blocking await method called from IO thread. Blocking IO must be dispatched to a worker thread or deadlocks will result.
	at io.undertow.server.HttpServerExchange$WriteDispatchChannel.awaitWritable(HttpServerExchange.java:2035)
	at org.xnio.channels.Channels.transferBlocking(Channels.java:514)
	at org.springframework.http.server.reactive.UndertowServerHttpResponse.lambda$null$0(UndertowServerHttpResponse.java:131)
	at reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:44)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at reactor.core.publisher.FluxConcatIterable$ConcatIterableSubscriber.onComplete(FluxConcatIterable.java:146)
	at reactor.core.publisher.FluxConcatIterable.subscribe(FluxConcatIterable.java:60)
	at reactor.core.publisher.MonoIgnoreElements.subscribe(MonoIgnoreElements.java:37)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at reactor.core.publisher.FluxFlatMap.trySubscribeScalarMap(FluxFlatMap.java:172)
	at reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:53)
	at reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:150)
	at reactor.core.publisher.FluxSwitchIfEmpty$SwitchIfEmptySubscriber.onNext(FluxSwitchIfEmpty.java:67)
	at reactor.core.publisher.Operators$MonoSubscriber.complete(Operators.java:1476)
	at reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:144)
	at reactor.core.publisher.MonoNext$NextSubscriber.onNext(MonoNext.java:76)
	at reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.drain(FluxConcatMap.java:422)
	at reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.onSubscribe(FluxConcatMap.java:212)
	at reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:139)
	at reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:63)
	at reactor.core.publisher.FluxConcatMap.subscribe(FluxConcatMap.java:121)
	at reactor.core.publisher.MonoNext.subscribe(MonoNext.java:40)
	at reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)
	at reactor.core.publisher.MonoSwitchIfEmpty.subscribe(MonoSwitchIfEmpty.java:44)
	at reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at reactor.core.publisher.MonoIgnoreThen$ThenIgnoreMain.drain(MonoIgnoreThen.java:172)
	at reactor.core.publisher.MonoIgnoreThen.subscribe(MonoIgnoreThen.java:56)
	at reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:150)
	at reactor.core.publisher.FluxSwitchIfEmpty$SwitchIfEmptySubscriber.onNext(FluxSwitchIfEmpty.java:67)
	at reactor.core.publisher.MonoNext$NextSubscriber.onNext(MonoNext.java:76)
	at reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.innerNext(FluxConcatMap.java:275)
	at reactor.core.publisher.FluxConcatMap$ConcatMapInner.onNext(FluxConcatMap.java:849)
	at reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.onNext(FluxMapFuseable.java:121)
	at reactor.core.publisher.Operators$ScalarSubscription.request(Operators.java:2041)
	at reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.request(FluxMapFuseable.java:162)
	at reactor.core.publisher.Operators$MultiSubscriptionSubscriber.set(Operators.java:1849)
	at reactor.core.publisher.Operators$MultiSubscriptionSubscriber.onSubscribe(Operators.java:1723)
	at reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.onSubscribe(FluxMapFuseable.java:90)
	at reactor.core.publisher.MonoJust.subscribe(MonoJust.java:54)
	at reactor.core.publisher.MonoMapFuseable.subscribe(MonoMapFuseable.java:59)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.drain(FluxConcatMap.java:442)
	at reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.onSubscribe(FluxConcatMap.java:212)
	at reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:139)
	at reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:63)
	at reactor.core.publisher.FluxConcatMap.subscribe(FluxConcatMap.java:121)
	at reactor.core.publisher.MonoNext.subscribe(MonoNext.java:40)
	at reactor.core.publisher.MonoSwitchIfEmpty.subscribe(MonoSwitchIfEmpty.java:44)
	at reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)
	at reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)
	at reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)
	at reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)
	at reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)
	at reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)
	at reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)
	at reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)
	at reactor.core.publisher.MonoPeekTerminal.subscribe(MonoPeekTerminal.java:61)
	at reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at reactor.core.publisher.MonoIgnoreThen$ThenIgnoreMain.drain(MonoIgnoreThen.java:172)
	at reactor.core.publisher.MonoIgnoreThen.subscribe(MonoIgnoreThen.java:56)
	at reactor.core.publisher.Mono.subscribe(Mono.java:3608)
	at org.springframework.http.server.reactive.UndertowHttpHandlerAdapter.handleRequest(UndertowHttpHandlerAdapter.java:87)
	at io.undertow.server.Connectors.executeRootHandler(Connectors.java:360)
	at io.undertow.server.protocol.http.HttpReadListener.handleEventWithNoRunningRequest(HttpReadListener.java:255)
	at io.undertow.server.protocol.http.HttpReadListener.handleEvent(HttpReadListener.java:136)
	at io.undertow.server.protocol.http.HttpReadListener.handleEvent(HttpReadListener.java:59)
	at org.xnio.ChannelListeners.invokeChannelListener(ChannelListeners.java:92)
	at org.xnio.conduits.ReadReadyHandler$ChannelListenerHandler.readReady(ReadReadyHandler.java:66)
	at org.xnio.nio.NioSocketConduit.handleReady(NioSocketConduit.java:88)
	at org.xnio.nio.WorkerThread.run(WorkerThread.java:561)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22413","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22413/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22413/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22413/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22413","id":410301418,"node_id":"MDU6SXNzdWU0MTAzMDE0MTg=","number":22413,"title":"WebFlux with undertow throws exception when serving big static files.","user":{"login":"lkolek","id":17950619,"node_id":"MDQ6VXNlcjE3OTUwNjE5","avatar_url":"https://avatars3.githubusercontent.com/u/17950619?v=4","gravatar_id":"","url":"https://api.github.com/users/lkolek","html_url":"https://github.com/lkolek","followers_url":"https://api.github.com/users/lkolek/followers","following_url":"https://api.github.com/users/lkolek/following{/other_user}","gists_url":"https://api.github.com/users/lkolek/gists{/gist_id}","starred_url":"https://api.github.com/users/lkolek/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lkolek/subscriptions","organizations_url":"https://api.github.com/users/lkolek/orgs","repos_url":"https://api.github.com/users/lkolek/repos","events_url":"https://api.github.com/users/lkolek/events{/privacy}","received_events_url":"https://api.github.com/users/lkolek/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/210","html_url":"https://github.com/spring-projects/spring-framework/milestone/210","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/210/labels","id":4008678,"node_id":"MDk6TWlsZXN0b25lNDAwODY3OA==","number":210,"title":"5.1.6","description":"","creator":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":48,"state":"closed","created_at":"2019-01-28T17:08:14Z","updated_at":"2019-04-09T15:07:07Z","due_on":"2019-03-31T07:00:00Z","closed_at":"2019-03-31T13:18:44Z"},"comments":5,"created_at":"2019-02-14T13:32:39Z","updated_at":"2019-03-19T10:32:40Z","closed_at":"2019-03-19T10:32:40Z","author_association":"NONE","body":"<!--\r\n!!! For Security Vulnerabilities, please go to https://pivotal.io/security !!!\r\n-->\r\n**Affects:** \\5.1.3, 5.0.*\r\n\r\n---\r\n<!--\r\nThanks for taking the time to create an issue. Please read the following:\r\n\r\n- Questions should be asked on Stack Overflow.\r\n- For bugs, specify affected versions and explain what you are trying to do.\r\n- For enhancements, provide context and describe the problem.\r\n\r\nIssue or Pull Request? Create only one, not both. GitHub treats them as the same.\r\nIf unsure, start with an issue, and if you submit a pull request later, the\r\nissue will be closed as superseded.\r\n-->\r\nWhen serving big enough static resources (file), WebFlux + Undertow throws following exception:\r\n`java.io.IOException: UT000094: Blocking await method called from IO thread.`\r\n\r\nIt is caused by `Channels.transferBlocking` call in `org.springframework.http.server.reactive.UndertowServerHttpResponse` class:\r\n\r\n\t@Override\r\n\tpublic Mono<Void> writeWith(Path file, long position, long count) {\r\n\t\treturn doCommit(() ->\r\n\t\t\t\tMono.defer(() -> {\r\n\t\t\t\t\ttry (FileChannel source = FileChannel.open(file, StandardOpenOption.READ)) {\r\n\t\t\t\t\t\tStreamSinkChannel destination = this.exchange.getResponseChannel();\r\n\t\t\t\t\t\tChannels.transferBlocking(destination, source, position, count);\r\n\t\t\t\t\t\treturn Mono.empty();\r\n\t\t\t\t\t}\r\n\t\t\t\t\tcatch (IOException ex) {\r\n\t\t\t\t\t\treturn Mono.error(ex);\r\n\t\t\t\t\t}\r\n\t\t\t\t}));\r\n\t}\r\n\r\n\r\nMoving this call to IO threads seems to work, but migt be sub-optimal (see: `.subscribeOn(Schedulers.elastic())` near end):\r\n\r\n\r\n\t@Override\r\n    public Mono<Void> writeWith(Path file, long position, long count) {\r\n        return doCommit(() ->\r\n                Mono.defer(() -> {\r\n                    try (FileChannel source = FileChannel.open(file, StandardOpenOption.READ)) {\r\n                        StreamSinkChannel destination = this.exchange.getResponseChannel();\r\n                        Channels.transferBlocking(destination, source, position, count);\r\n                        return Mono.empty();\r\n                    }\r\n                    catch (IOException ex) {\r\n                        ex.printStackTrace();\r\n                        return Mono.error(ex);\r\n                    }\r\n                }))\r\n                // TO HANDLE BLOCKING!\r\n                .subscribeOn(Schedulers.elastic())\r\n                ;\r\n    }\r\n\r\n\r\nStack trace:\r\n\r\n\t2019-02-14 14:19:29.329 ERROR 44484 --- [   XNIO-1 I/O-1] io.undertow                              : UT005085: Connection io.undertow.server.protocol.http.HttpServerConnection@13871769 for exchange HttpServerExchange{ GET /test.mp4 request {Accept=[*/*], Accept-Language=[pl-pl], Accept-Encoding=[identity], User-Agent=[Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15], Range=[bytes=0-1], Connection=[keep-alive], X-Playback-Session-Id=[B6657529-5085-4A47-AD60-089BDEC59DD5], Cookie=[Idea-e386df9=4ce8f3fb-c39f-415a-8ff1-c19f2999954d; Idea-e386df8=603d089f-ad6a-4efd-8e7b-186238fff603; _ga=GA1.1.524791181.1511035090], Referer=[http://localhost:8080/test.mp4], Host=[localhost:8080]} response {Connection=[keep-alive], Last-Modified=[Wed, 13 Feb 2019 19:58:38 GMT], Content-Range=[bytes 0-1/121486134], Content-Length=[2], Content-Type=[video/mp4], Accept-Ranges=[bytes], Date=[Thu, 14 Feb 2019 13:19:29 GMT]}} was not closed cleanly, forcibly closing connection\r\n\tjava.io.IOException: UT000094: Blocking await method called from IO thread. Blocking IO must be dispatched to a worker thread or deadlocks will result.\r\n\tat io.undertow.server.HttpServerExchange$WriteDispatchChannel.awaitWritable(HttpServerExchange.java:2035)\r\n\tat org.xnio.channels.Channels.transferBlocking(Channels.java:514)\r\n\tat org.springframework.http.server.reactive.UndertowServerHttpResponse.lambda$null$0(UndertowServerHttpResponse.java:131)\r\n\tat reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:44)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat reactor.core.publisher.FluxConcatIterable$ConcatIterableSubscriber.onComplete(FluxConcatIterable.java:146)\r\n\tat reactor.core.publisher.FluxConcatIterable.subscribe(FluxConcatIterable.java:60)\r\n\tat reactor.core.publisher.MonoIgnoreElements.subscribe(MonoIgnoreElements.java:37)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat reactor.core.publisher.FluxFlatMap.trySubscribeScalarMap(FluxFlatMap.java:172)\r\n\tat reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:53)\r\n\tat reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:150)\r\n\tat reactor.core.publisher.FluxSwitchIfEmpty$SwitchIfEmptySubscriber.onNext(FluxSwitchIfEmpty.java:67)\r\n\tat reactor.core.publisher.Operators$MonoSubscriber.complete(Operators.java:1476)\r\n\tat reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:144)\r\n\tat reactor.core.publisher.MonoNext$NextSubscriber.onNext(MonoNext.java:76)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.drain(FluxConcatMap.java:422)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.onSubscribe(FluxConcatMap.java:212)\r\n\tat reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:139)\r\n\tat reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:63)\r\n\tat reactor.core.publisher.FluxConcatMap.subscribe(FluxConcatMap.java:121)\r\n\tat reactor.core.publisher.MonoNext.subscribe(MonoNext.java:40)\r\n\tat reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)\r\n\tat reactor.core.publisher.MonoSwitchIfEmpty.subscribe(MonoSwitchIfEmpty.java:44)\r\n\tat reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat reactor.core.publisher.MonoIgnoreThen$ThenIgnoreMain.drain(MonoIgnoreThen.java:172)\r\n\tat reactor.core.publisher.MonoIgnoreThen.subscribe(MonoIgnoreThen.java:56)\r\n\tat reactor.core.publisher.MonoFlatMap$FlatMapMain.onNext(MonoFlatMap.java:150)\r\n\tat reactor.core.publisher.FluxSwitchIfEmpty$SwitchIfEmptySubscriber.onNext(FluxSwitchIfEmpty.java:67)\r\n\tat reactor.core.publisher.MonoNext$NextSubscriber.onNext(MonoNext.java:76)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.innerNext(FluxConcatMap.java:275)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapInner.onNext(FluxConcatMap.java:849)\r\n\tat reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.onNext(FluxMapFuseable.java:121)\r\n\tat reactor.core.publisher.Operators$ScalarSubscription.request(Operators.java:2041)\r\n\tat reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.request(FluxMapFuseable.java:162)\r\n\tat reactor.core.publisher.Operators$MultiSubscriptionSubscriber.set(Operators.java:1849)\r\n\tat reactor.core.publisher.Operators$MultiSubscriptionSubscriber.onSubscribe(Operators.java:1723)\r\n\tat reactor.core.publisher.FluxMapFuseable$MapFuseableSubscriber.onSubscribe(FluxMapFuseable.java:90)\r\n\tat reactor.core.publisher.MonoJust.subscribe(MonoJust.java:54)\r\n\tat reactor.core.publisher.MonoMapFuseable.subscribe(MonoMapFuseable.java:59)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.drain(FluxConcatMap.java:442)\r\n\tat reactor.core.publisher.FluxConcatMap$ConcatMapImmediate.onSubscribe(FluxConcatMap.java:212)\r\n\tat reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:139)\r\n\tat reactor.core.publisher.FluxIterable.subscribe(FluxIterable.java:63)\r\n\tat reactor.core.publisher.FluxConcatMap.subscribe(FluxConcatMap.java:121)\r\n\tat reactor.core.publisher.MonoNext.subscribe(MonoNext.java:40)\r\n\tat reactor.core.publisher.MonoSwitchIfEmpty.subscribe(MonoSwitchIfEmpty.java:44)\r\n\tat reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)\r\n\tat reactor.core.publisher.MonoFlatMap.subscribe(MonoFlatMap.java:60)\r\n\tat reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)\r\n\tat reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)\r\n\tat reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)\r\n\tat reactor.core.publisher.MonoDefer.subscribe(MonoDefer.java:52)\r\n\tat reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)\r\n\tat reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)\r\n\tat reactor.core.publisher.MonoPeekTerminal.subscribe(MonoPeekTerminal.java:61)\r\n\tat reactor.core.publisher.MonoOnErrorResume.subscribe(MonoOnErrorResume.java:44)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat reactor.core.publisher.MonoIgnoreThen$ThenIgnoreMain.drain(MonoIgnoreThen.java:172)\r\n\tat reactor.core.publisher.MonoIgnoreThen.subscribe(MonoIgnoreThen.java:56)\r\n\tat reactor.core.publisher.Mono.subscribe(Mono.java:3608)\r\n\tat org.springframework.http.server.reactive.UndertowHttpHandlerAdapter.handleRequest(UndertowHttpHandlerAdapter.java:87)\r\n\tat io.undertow.server.Connectors.executeRootHandler(Connectors.java:360)\r\n\tat io.undertow.server.protocol.http.HttpReadListener.handleEventWithNoRunningRequest(HttpReadListener.java:255)\r\n\tat io.undertow.server.protocol.http.HttpReadListener.handleEvent(HttpReadListener.java:136)\r\n\tat io.undertow.server.protocol.http.HttpReadListener.handleEvent(HttpReadListener.java:59)\r\n\tat org.xnio.ChannelListeners.invokeChannelListener(ChannelListeners.java:92)\r\n\tat org.xnio.conduits.ReadReadyHandler$ChannelListenerHandler.readReady(ReadReadyHandler.java:66)\r\n\tat org.xnio.nio.NioSocketConduit.handleReady(NioSocketConduit.java:88)\r\n\tat org.xnio.nio.WorkerThread.run(WorkerThread.java:561)\r\n","closed_by":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}}", "commentIds":["471573533","473271853","473275368","473390780","473916111"], "labels":["in: web"]}