{"id":"6050", "title":"LoadBalancing'Service'FactoryBean.. BeanFactory that adds load balancing/failover functionality to a set of (remoteservice)beans. [SPR-1350]", "body":"**[Peter Veentjer](https://jira.spring.io/secure/ViewProfile.jspa?name=alarmnummer)** opened **[SPR-1350](https://jira.spring.io/browse/SPR-1350?redirect=false)** and commented

At the moment it isn`t possible to add failover/loadbalancing to multiple beans. I think it can be done, without any changes in the code, it would just require a new FactoryBean.

example:
[code]
\\<bean id=\"paymentService1\"
class=\"org.springframework.remoting.rmi.RmiProxyFactoryBean\">
\\<property name=\"serviceUrl\">
\\<value>rmi://${paymenthost}/PayService\\</value>
\\</property>
\\<property name=\"serviceInterface\">
\\<value>com.springinaction.payment.PaymentService\\</value>
\\</property>
\\</bean>

\\<bean id=\"paymentService2\"
class=\"org.springframework.remoting.other.HessianProxyFactoryBean\">
\\<property name=\"serviceUrl\">
\\<value>hessian://${paymenthost2}/PayService\\</value>
\\</property>
\\<property name=\"serviceInterface\">
\\<value>com.springinaction.payment.PaymentService\\</value>
\\</property>
\\</bean>

\\<bean    id=\"paymentService\"
class=\"LoadBalancingFactoryBean\">

\\<property name=\"serviceList\">
\\<list>
\\<bean ref=\"paymentService1\"/>
\\<bean ref=\"paymentService2\"/>         
\\</list>
\\</property-arg>   
\\<property name=\"selectionStrategy\">
\\<bean class=\"RoundRobin\"/>
\\</property>
\\<property name=\"serviceInterface\">
\\<value>com.springinaction.payment.PaymentService\\</value>
\\</property>
\\</bean> 
[/code]

PaymentService1 is a RMI service. PaygmentService2 is a Hessian service. Those two services can be joined in a new bean: paymentService

The PaymentService creates a new objects that calls one of the other services maybe with round robin, or something else. And failover wouldn`t be a big problem to add (if one of the services fails you can try another one).


---

**Attachments:**
- [LoadBalancedBeanFactory.zip](https://jira.spring.io/secure/attachment/12381/LoadBalancedBeanFactory.zip) (_6.34 kB_)

14 votes, 13 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6050","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6050/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6050/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6050/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6050","id":398060425,"node_id":"MDU6SXNzdWUzOTgwNjA0MjU=","number":6050,"title":"LoadBalancing'Service'FactoryBean.. BeanFactory that adds load balancing/failover functionality to a set of (remoteservice)beans. [SPR-1350]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512024,"node_id":"MDU6TGFiZWwxMTg4NTEyMDI0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/has:%20votes-jira","name":"has: votes-jira","color":"dfdfdf","default":false,"description":"Issues migrated from JIRA with more than 10 votes at the time of import"},{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false,"description":"An outdated, unresolved issue that's closed in bulk as part of a cleaning process"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2005-10-04T21:23:30Z","updated_at":"2019-01-12T03:51:21Z","closed_at":"2019-01-12T00:30:45Z","author_association":"COLLABORATOR","body":"**[Peter Veentjer](https://jira.spring.io/secure/ViewProfile.jspa?name=alarmnummer)** opened **[SPR-1350](https://jira.spring.io/browse/SPR-1350?redirect=false)** and commented\n\nAt the moment it isn`t possible to add failover/loadbalancing to multiple beans. I think it can be done, without any changes in the code, it would just require a new FactoryBean.\n\nexample:\n[code]\n\\<bean id=\"paymentService1\"\nclass=\"org.springframework.remoting.rmi.RmiProxyFactoryBean\">\n\\<property name=\"serviceUrl\">\n\\<value>rmi://${paymenthost}/PayService\\</value>\n\\</property>\n\\<property name=\"serviceInterface\">\n\\<value>com.springinaction.payment.PaymentService\\</value>\n\\</property>\n\\</bean>\n\n\\<bean id=\"paymentService2\"\nclass=\"org.springframework.remoting.other.HessianProxyFactoryBean\">\n\\<property name=\"serviceUrl\">\n\\<value>hessian://${paymenthost2}/PayService\\</value>\n\\</property>\n\\<property name=\"serviceInterface\">\n\\<value>com.springinaction.payment.PaymentService\\</value>\n\\</property>\n\\</bean>\n\n\\<bean    id=\"paymentService\"\nclass=\"LoadBalancingFactoryBean\">\n\n\\<property name=\"serviceList\">\n\\<list>\n\\<bean ref=\"paymentService1\"/>\n\\<bean ref=\"paymentService2\"/>         \n\\</list>\n\\</property-arg>   \n\\<property name=\"selectionStrategy\">\n\\<bean class=\"RoundRobin\"/>\n\\</property>\n\\<property name=\"serviceInterface\">\n\\<value>com.springinaction.payment.PaymentService\\</value>\n\\</property>\n\\</bean> \n[/code]\n\nPaymentService1 is a RMI service. PaygmentService2 is a Hessian service. Those two services can be joined in a new bean: paymentService\n\nThe PaymentService creates a new objects that calls one of the other services maybe with round robin, or something else. And failover wouldn`t be a big problem to add (if one of the services fails you can try another one).\n\n\n---\n\n**Attachments:**\n- [LoadBalancedBeanFactory.zip](https://jira.spring.io/secure/attachment/12381/LoadBalancedBeanFactory.zip) (_6.34 kB_)\n\n14 votes, 13 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453298250","453716890"], "labels":["has: votes-jira","in: core","status: bulk-closed"]}