{"id":"15896", "title":"enable cache can use both @Cacheable and @CachePut  [SPR-11271]", "body":"**[zhangkaitao](https://jira.spring.io/secure/ViewProfile.jspa?name=zhangkaitao)** opened **[SPR-11271](https://jira.spring.io/browse/SPR-11271?redirect=false)** and commented

such as user module，exists：
id
username
email

when invoke findById method，I want add
id --> user 、 username --> user 、 email --> user

can modify Cache

```
// We only attempt to get a cached result if there are no put requests
if (cachePutRequests.isEmpty() && contexts.get(CachePutOperation.class).isEmpty()) {
     result = findCachedResult(contexts.get(CacheableOperation.class));
}
```

to

```

        Collection<CacheOperationContext> cacheOperationContexts = contexts.get(CacheableOperation.class);
        // We only attempt to get a cached result if there are has @Cachable
        if (!cacheOperationContexts.isEmpty()) {
            result = findCachedResult(cacheOperationContexts);
        }
```

enable。

now,i can use follow code support:

```
    @Caching(
            cacheable = {
                    @Cacheable(value = \"user\", key = \"#id\")
            },
            put = {
                    @CachePut(value = \"user\", key = \"#result.username\", condition = \"#result != null\"),
                    @CachePut(value = \"user\", key = \"#result.email\", condition = \"#result != null\")
            }
    )
    public User findById(final Long id) {
        System.out.println(\"cache miss, invoke find by id, id:\" + id);
        for (User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

```

https://github.com/zhangkaitao/spring4-showcase/blob/master/spring-cache/src/main/java/com/sishuok/spring/service/UserService.java
https://github.com/zhangkaitao/spring4-showcase/blob/master/spring-cache/src/test/java/org/springframework/cache/interceptor/CacheAspectSupport.java

but，best cache way is：
id --> user、 username --> id、email --> id

`@CachePut`(value=\"cacheName\", key=\"#user.username\", cacheValue=\"#user.username\")
public void save(User user) {}

`@Cacheable`(value=\"cacheName\", ley=\"#user.username\", cacheValue=\"#caches[0].get(#caches[0].get(#username).get())\")
public User findByUsername(String username);



---

**Affects:** 4.0 GA
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15896","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15896/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15896/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15896/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15896","id":398164183,"node_id":"MDU6SXNzdWUzOTgxNjQxODM=","number":15896,"title":"enable cache can use both @Cacheable and @CachePut  [SPR-11271]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"snicoll","id":490484,"node_id":"MDQ6VXNlcjQ5MDQ4NA==","avatar_url":"https://avatars0.githubusercontent.com/u/490484?v=4","gravatar_id":"","url":"https://api.github.com/users/snicoll","html_url":"https://github.com/snicoll","followers_url":"https://api.github.com/users/snicoll/followers","following_url":"https://api.github.com/users/snicoll/following{/other_user}","gists_url":"https://api.github.com/users/snicoll/gists{/gist_id}","starred_url":"https://api.github.com/users/snicoll/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snicoll/subscriptions","organizations_url":"https://api.github.com/users/snicoll/orgs","repos_url":"https://api.github.com/users/snicoll/repos","events_url":"https://api.github.com/users/snicoll/events{/privacy}","received_events_url":"https://api.github.com/users/snicoll/received_events","type":"User","site_admin":false},"assignees":[{"login":"snicoll","id":490484,"node_id":"MDQ6VXNlcjQ5MDQ4NA==","avatar_url":"https://avatars0.githubusercontent.com/u/490484?v=4","gravatar_id":"","url":"https://api.github.com/users/snicoll","html_url":"https://github.com/snicoll","followers_url":"https://api.github.com/users/snicoll/followers","following_url":"https://api.github.com/users/snicoll/following{/other_user}","gists_url":"https://api.github.com/users/snicoll/gists{/gist_id}","starred_url":"https://api.github.com/users/snicoll/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snicoll/subscriptions","organizations_url":"https://api.github.com/users/snicoll/orgs","repos_url":"https://api.github.com/users/snicoll/repos","events_url":"https://api.github.com/users/snicoll/events{/privacy}","received_events_url":"https://api.github.com/users/snicoll/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2014-01-01T05:08:49Z","updated_at":"2019-01-14T05:17:56Z","closed_at":"2015-12-22T13:21:54Z","author_association":"COLLABORATOR","body":"**[zhangkaitao](https://jira.spring.io/secure/ViewProfile.jspa?name=zhangkaitao)** opened **[SPR-11271](https://jira.spring.io/browse/SPR-11271?redirect=false)** and commented\n\nsuch as user module，exists：\nid\nusername\nemail\n\nwhen invoke findById method，I want add\nid --> user 、 username --> user 、 email --> user\n\ncan modify Cache\n\n```\n// We only attempt to get a cached result if there are no put requests\nif (cachePutRequests.isEmpty() && contexts.get(CachePutOperation.class).isEmpty()) {\n     result = findCachedResult(contexts.get(CacheableOperation.class));\n}\n```\n\nto\n\n```\n\n        Collection<CacheOperationContext> cacheOperationContexts = contexts.get(CacheableOperation.class);\n        // We only attempt to get a cached result if there are has @Cachable\n        if (!cacheOperationContexts.isEmpty()) {\n            result = findCachedResult(cacheOperationContexts);\n        }\n```\n\nenable。\n\nnow,i can use follow code support:\n\n```\n    @Caching(\n            cacheable = {\n                    @Cacheable(value = \"user\", key = \"#id\")\n            },\n            put = {\n                    @CachePut(value = \"user\", key = \"#result.username\", condition = \"#result != null\"),\n                    @CachePut(value = \"user\", key = \"#result.email\", condition = \"#result != null\")\n            }\n    )\n    public User findById(final Long id) {\n        System.out.println(\"cache miss, invoke find by id, id:\" + id);\n        for (User user : users) {\n            if (user.getId().equals(id)) {\n                return user;\n            }\n        }\n        return null;\n    }\n\n```\n\nhttps://github.com/zhangkaitao/spring4-showcase/blob/master/spring-cache/src/main/java/com/sishuok/spring/service/UserService.java\nhttps://github.com/zhangkaitao/spring4-showcase/blob/master/spring-cache/src/test/java/org/springframework/cache/interceptor/CacheAspectSupport.java\n\nbut，best cache way is：\nid --> user、 username --> id、email --> id\n\n`@CachePut`(value=\"cacheName\", key=\"#user.username\", cacheValue=\"#user.username\")\npublic void save(User user) {}\n\n`@Cacheable`(value=\"cacheName\", ley=\"#user.username\", cacheValue=\"#caches[0].get(#caches[0].get(#username).get())\")\npublic User findByUsername(String username);\n\n\n\n---\n\n**Affects:** 4.0 GA\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453408383","453408385","453408387"], "labels":["in: core","status: declined","type: enhancement"]}