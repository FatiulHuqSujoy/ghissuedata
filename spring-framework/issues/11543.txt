{"id":"11543", "title":"AnnotationMethodHandlerAdapter.handleResponseBody prioritizes messageConverter over MediaType [SPR-6877]", "body":"**[Ted Bergeron](https://jira.spring.io/secure/ViewProfile.jspa?name=tedberg)** opened **[SPR-6877](https://jira.spring.io/browse/SPR-6877?redirect=false)** and commented

The nested loop should be flip flopped.  The current logic iterates over the message converters, then looks at the media types.  For request header:

```
Accept	application/json, text/javascript, */*
```

Which is sent via jQuery, this logic picks the first converter than can handle ```
\\*/\\*

```It should start with application/json and scan all message converters looking for a match.  Only when a match isn't found should the logic ever consider the second or third arguments.

```

    	private void handleResponseBody(Object returnValue, ServletWebRequest webRequest)
    			throws ServletException, IOException {
    
    		HttpInputMessage inputMessage = new ServletServerHttpRequest(webRequest.getRequest());
    		List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();
    		if (acceptedMediaTypes.isEmpty()) {
    			acceptedMediaTypes = Collections.singletonList(MediaType.ALL);
    		}
    		MediaType.sortBySpecificity(acceptedMediaTypes);
    		HttpOutputMessage outputMessage = new ServletServerHttpResponse(webRequest.getResponse());
    		Class<?> returnValueType = returnValue.getClass();
    		List<MediaType> allSupportedMediaTypes = new ArrayList<MediaType>();
    		if (messageConverters != null) {
    			for (HttpMessageConverter messageConverter : messageConverters) {
    				allSupportedMediaTypes.addAll(messageConverter.getSupportedMediaTypes());
    				for (MediaType acceptedMediaType : acceptedMediaTypes) {
    					if (messageConverter.canWrite(returnValueType, acceptedMediaType)) {
    						messageConverter.write(returnValue, acceptedMediaType, outputMessage);
    						this.responseArgumentUsed = true;
    						return;
    					}
    				}
    			}
    		}
    		throw new HttpMediaTypeNotAcceptableException(allSupportedMediaTypes);
    	}

```

In my case, I've registered the converters in the order matching the docs: http://static.springsource.org/spring/docs/3.0.x/spring-framework-reference/html/remoting.html#rest-message-conversion

```

\\<property name=\"messageConverters\">
\\<list>
\\<bean class=\"org.springframework.http.converter.StringHttpMessageConverter\"/>
\\<bean class=\"org.springframework.http.converter.FormHttpMessageConverter\"/>
\\<bean class=\"org.springframework.http.converter.ByteArrayHttpMessageConverter\"/>
\\<!--\\<bean class=\"org.springframework.http.converter.xml.MarshallingHttpMessageConverter\"/>-->
\\<bean class=\"org.springframework.http.converter.json.MappingJacksonHttpMessageConverter\"/>
\\<bean class=\"org.springframework.http.converter.xml.SourceHttpMessageConverter\"/>
\\<!--\\<bean class=\"org.springframework.http.converter.BufferedImageHttpMessageConverter\"/>-->
\\</list>
\\</property>

```

If I enable the xml converter, it tries to process my request, despite the json accept header.  Nothing in the docs indicate that the ordering is very important, as I'd expect these to be mutually exclusive.  If there is a specific ordering that should be used, please add that to the docs.

The only ordering I can see is the default in the code of:

```

private HttpMessageConverter<?>[] messageConverters =
new HttpMessageConverter[]{new ByteArrayHttpMessageConverter(), new StringHttpMessageConverter(),
new FormHttpMessageConverter(), new SourceHttpMessageConverter()};

```



---

**Affects:** 3.0.1

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/3c8a47bd06023cf8eba681f56720c7dc5d532662, https://github.com/spring-projects/spring-framework/commit/62f9f477f5887c3c6237472d7d8db04510675ceb
```

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11543","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11543/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11543/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11543/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11543","id":398103174,"node_id":"MDU6SXNzdWUzOTgxMDMxNzQ=","number":11543,"title":"AnnotationMethodHandlerAdapter.handleResponseBody prioritizes messageConverter over MediaType [SPR-6877]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/69","html_url":"https://github.com/spring-projects/spring-framework/milestone/69","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/69/labels","id":3960842,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mg==","number":69,"title":"3.0.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":98,"state":"closed","created_at":"2019-01-10T22:03:15Z","updated_at":"2019-01-11T02:52:19Z","due_on":"2010-03-31T07:00:00Z","closed_at":"2019-01-10T22:03:15Z"},"comments":1,"created_at":"2010-02-19T07:26:48Z","updated_at":"2019-01-13T21:46:58Z","closed_at":"2012-06-19T03:43:22Z","author_association":"COLLABORATOR","body":"**[Ted Bergeron](https://jira.spring.io/secure/ViewProfile.jspa?name=tedberg)** opened **[SPR-6877](https://jira.spring.io/browse/SPR-6877?redirect=false)** and commented\n\nThe nested loop should be flip flopped.  The current logic iterates over the message converters, then looks at the media types.  For request header:\n\n```\nAccept\tapplication/json, text/javascript, */*\n```\n\nWhich is sent via jQuery, this logic picks the first converter than can handle ```\n\\*/\\*\n\n```It should start with application/json and scan all message converters looking for a match.  Only when a match isn't found should the logic ever consider the second or third arguments.\n\n```\n\n    \tprivate void handleResponseBody(Object returnValue, ServletWebRequest webRequest)\n    \t\t\tthrows ServletException, IOException {\n    \n    \t\tHttpInputMessage inputMessage = new ServletServerHttpRequest(webRequest.getRequest());\n    \t\tList<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();\n    \t\tif (acceptedMediaTypes.isEmpty()) {\n    \t\t\tacceptedMediaTypes = Collections.singletonList(MediaType.ALL);\n    \t\t}\n    \t\tMediaType.sortBySpecificity(acceptedMediaTypes);\n    \t\tHttpOutputMessage outputMessage = new ServletServerHttpResponse(webRequest.getResponse());\n    \t\tClass<?> returnValueType = returnValue.getClass();\n    \t\tList<MediaType> allSupportedMediaTypes = new ArrayList<MediaType>();\n    \t\tif (messageConverters != null) {\n    \t\t\tfor (HttpMessageConverter messageConverter : messageConverters) {\n    \t\t\t\tallSupportedMediaTypes.addAll(messageConverter.getSupportedMediaTypes());\n    \t\t\t\tfor (MediaType acceptedMediaType : acceptedMediaTypes) {\n    \t\t\t\t\tif (messageConverter.canWrite(returnValueType, acceptedMediaType)) {\n    \t\t\t\t\t\tmessageConverter.write(returnValue, acceptedMediaType, outputMessage);\n    \t\t\t\t\t\tthis.responseArgumentUsed = true;\n    \t\t\t\t\t\treturn;\n    \t\t\t\t\t}\n    \t\t\t\t}\n    \t\t\t}\n    \t\t}\n    \t\tthrow new HttpMediaTypeNotAcceptableException(allSupportedMediaTypes);\n    \t}\n\n```\n\nIn my case, I've registered the converters in the order matching the docs: http://static.springsource.org/spring/docs/3.0.x/spring-framework-reference/html/remoting.html#rest-message-conversion\n\n```\n\n\\<property name=\"messageConverters\">\n\\<list>\n\\<bean class=\"org.springframework.http.converter.StringHttpMessageConverter\"/>\n\\<bean class=\"org.springframework.http.converter.FormHttpMessageConverter\"/>\n\\<bean class=\"org.springframework.http.converter.ByteArrayHttpMessageConverter\"/>\n\\<!--\\<bean class=\"org.springframework.http.converter.xml.MarshallingHttpMessageConverter\"/>-->\n\\<bean class=\"org.springframework.http.converter.json.MappingJacksonHttpMessageConverter\"/>\n\\<bean class=\"org.springframework.http.converter.xml.SourceHttpMessageConverter\"/>\n\\<!--\\<bean class=\"org.springframework.http.converter.BufferedImageHttpMessageConverter\"/>-->\n\\</list>\n\\</property>\n\n```\n\nIf I enable the xml converter, it tries to process my request, despite the json accept header.  Nothing in the docs indicate that the ordering is very important, as I'd expect these to be mutually exclusive.  If there is a specific ordering that should be used, please add that to the docs.\n\nThe only ordering I can see is the default in the code of:\n\n```\n\nprivate HttpMessageConverter<?>[] messageConverters =\nnew HttpMessageConverter[]{new ByteArrayHttpMessageConverter(), new StringHttpMessageConverter(),\nnew FormHttpMessageConverter(), new SourceHttpMessageConverter()};\n\n```\n\n\n\n---\n\n**Affects:** 3.0.1\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/3c8a47bd06023cf8eba681f56720c7dc5d532662, https://github.com/spring-projects/spring-framework/commit/62f9f477f5887c3c6237472d7d8db04510675ceb\n```\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453348442"], "labels":["in: web","type: bug"]}