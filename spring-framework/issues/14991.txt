{"id":"14991", "title":"Provide annotation based alternative to AbstractAnnotationConfigDispatcherServletInitializer [SPR-10359]", "body":"**[Oliver Drotbohm](https://jira.spring.io/secure/ViewProfile.jspa?name=olivergierke)** opened **[SPR-10359](https://jira.spring.io/browse/SPR-10359?redirect=false)** and commented

Most of the `WebApplicationInititalizer` implementations based on `AbstractAnnotationConfigDispatcherServletInitializer` look like this:

```java
public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] { ApplicationConfig.class };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] { WebConfig.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { \"/\" };
  }
}
```

It would be cool if this could be boiled down to this:

```java
@InitWebApplication(
  rootConfig = ApplicationConfig.class,
  webConfig = WebConfig.class,
  mapping = \"/\")
public abstract class MyWebApplicationInitializer implements WebApplicationInitializer { }
```

Alternatively we could introduce annotations to mark configuration classes directly:

```java
public abstract class MyWebApplicationInitializer implements WebApplicationInitializer {

  @RootContext
  public static class ApplicationConfig { … }

  @WebContext
  public static class WebConfig { … }
}
```

The latter probably has the issue of ordering as looking up the static classes probably does not return them in a predefined order (in case there are multiple ones defined to get into the `@WebContext`. Also the servlet mapping definition does not fit into this model nicely. I just thought to document it as a slightly less usable alternative.


---
No further details from [SPR-10359](https://jira.spring.io/browse/SPR-10359?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14991","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14991/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14991/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14991/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14991","id":398157776,"node_id":"MDU6SXNzdWUzOTgxNTc3NzY=","number":14991,"title":"Provide annotation based alternative to AbstractAnnotationConfigDispatcherServletInitializer [SPR-10359]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-03-07T02:54:45Z","updated_at":"2019-01-12T00:25:09Z","closed_at":"2019-01-12T00:25:09Z","author_association":"COLLABORATOR","body":"**[Oliver Drotbohm](https://jira.spring.io/secure/ViewProfile.jspa?name=olivergierke)** opened **[SPR-10359](https://jira.spring.io/browse/SPR-10359?redirect=false)** and commented\n\nMost of the `WebApplicationInititalizer` implementations based on `AbstractAnnotationConfigDispatcherServletInitializer` look like this:\n\n```java\npublic class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {\n\n  @Override\n  protected Class<?>[] getRootConfigClasses() {\n    return new Class<?>[] { ApplicationConfig.class };\n  }\n\n  @Override\n  protected Class<?>[] getServletConfigClasses() {\n    return new Class<?>[] { WebConfig.class };\n  }\n\n  @Override\n  protected String[] getServletMappings() {\n    return new String[] { \"/\" };\n  }\n}\n```\n\nIt would be cool if this could be boiled down to this:\n\n```java\n@InitWebApplication(\n  rootConfig = ApplicationConfig.class,\n  webConfig = WebConfig.class,\n  mapping = \"/\")\npublic abstract class MyWebApplicationInitializer implements WebApplicationInitializer { }\n```\n\nAlternatively we could introduce annotations to mark configuration classes directly:\n\n```java\npublic abstract class MyWebApplicationInitializer implements WebApplicationInitializer {\n\n  @RootContext\n  public static class ApplicationConfig { … }\n\n  @WebContext\n  public static class WebConfig { … }\n}\n```\n\nThe latter probably has the issue of ordering as looking up the static classes probably does not return them in a predefined order (in case there are multiple ones defined to get into the `@WebContext`. Also the servlet mapping definition does not fit into this model nicely. I just thought to document it as a slightly less usable alternative.\n\n\n---\nNo further details from [SPR-10359](https://jira.spring.io/browse/SPR-10359?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453699973"], "labels":["in: web","status: bulk-closed"]}