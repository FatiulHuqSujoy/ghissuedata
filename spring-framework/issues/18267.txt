{"id":"18267", "title":"PropertyEditor not using getAsText when not using @InitBinder. [SPR-13692]", "body":"**[Franjo Stipanovic](https://jira.spring.io/secure/ViewProfile.jspa?name=fritzfs)** opened **[SPR-13692](https://jira.spring.io/browse/SPR-13692?redirect=false)** and commented

If I wish to use both setAsText and getAsText methods in my property editor I must use explicit registration using `@InitBinder` in controller. Without it - only setAsText is used.

AFAICS problems starts from here

```java
org.springframework.validation.AbstractBindingResult
@Override
public PropertyEditor findEditor(String field, Class<?> valueType) {
     ....
                                                                                                                                                                                                                                                     return editorRegistry.findCustomEditor(valueTypeToUse, fixedField(field));			
                                                            org.springframework.beans.PropertyEditorRegistrySupport	
@Override
public PropertyEditor findCustomEditor(Class<?> requiredType, String propertyPath) {
     ....
     return getCustomEditor(requiredTypeToUse);
        }		
private PropertyEditor getCustomEditor(Class<?> requiredType) {
     if (requiredType == null || this.customEditors == null) {
          return null;
     }
```

This last method throws null because customEditors is null. There's no connection to BeanUtils.findEditorByConvention() anywhere :-(

Documentation (http://docs.spring.io/spring/docs/current/spring-framework-reference/html/validation.html) says:

\"_Note also that the standard JavaBeans infrastructure will automatically discover PropertyEditor classes (without you having to register them explicitly) if they are in the same package as the class they handle, and have the same name as that class, with 'Editor' appended; for example, one could have the following class and package structure, which would be sufficient for the FooEditor class to be recognized and used as the PropertyEditor for Foo-typed properties._\"

com.chuck.Foo
com.chunk.FooEditor

```java
public class FooEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		Foo foo = new Foo();
		foo.setId(Integer.valueOf(text));
		setValue(foo);
	}

	@Override
	public String getAsText() {
		Foo foo = (Foo) this.getValue();
		return foo.getId().toString();
	}

}
```



---

**Affects:** 4.2.1, 4.2.3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18267","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18267/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18267/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18267/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18267","id":398186776,"node_id":"MDU6SXNzdWUzOTgxODY3NzY=","number":18267,"title":"PropertyEditor not using getAsText when not using @InitBinder. [SPR-13692]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-11-16T16:44:16Z","updated_at":"2019-01-12T02:44:59Z","closed_at":"2019-01-12T02:44:59Z","author_association":"COLLABORATOR","body":"**[Franjo Stipanovic](https://jira.spring.io/secure/ViewProfile.jspa?name=fritzfs)** opened **[SPR-13692](https://jira.spring.io/browse/SPR-13692?redirect=false)** and commented\n\nIf I wish to use both setAsText and getAsText methods in my property editor I must use explicit registration using `@InitBinder` in controller. Without it - only setAsText is used.\n\nAFAICS problems starts from here\n\n```java\norg.springframework.validation.AbstractBindingResult\n@Override\npublic PropertyEditor findEditor(String field, Class<?> valueType) {\n     ....\n                                                                                                                                                                                                                                                     return editorRegistry.findCustomEditor(valueTypeToUse, fixedField(field));\t\t\t\n                                                            org.springframework.beans.PropertyEditorRegistrySupport\t\n@Override\npublic PropertyEditor findCustomEditor(Class<?> requiredType, String propertyPath) {\n     ....\n     return getCustomEditor(requiredTypeToUse);\n        }\t\t\nprivate PropertyEditor getCustomEditor(Class<?> requiredType) {\n     if (requiredType == null || this.customEditors == null) {\n          return null;\n     }\n```\n\nThis last method throws null because customEditors is null. There's no connection to BeanUtils.findEditorByConvention() anywhere :-(\n\nDocumentation (http://docs.spring.io/spring/docs/current/spring-framework-reference/html/validation.html) says:\n\n\"_Note also that the standard JavaBeans infrastructure will automatically discover PropertyEditor classes (without you having to register them explicitly) if they are in the same package as the class they handle, and have the same name as that class, with 'Editor' appended; for example, one could have the following class and package structure, which would be sufficient for the FooEditor class to be recognized and used as the PropertyEditor for Foo-typed properties._\"\n\ncom.chuck.Foo\ncom.chunk.FooEditor\n\n```java\npublic class FooEditor extends PropertyEditorSupport {\n\n\t@Override\n\tpublic void setAsText(String text) {\n\t\tFoo foo = new Foo();\n\t\tfoo.setId(Integer.valueOf(text));\n\t\tsetValue(foo);\n\t}\n\n\t@Override\n\tpublic String getAsText() {\n\t\tFoo foo = (Foo) this.getValue();\n\t\treturn foo.getId().toString();\n\t}\n\n}\n```\n\n\n\n---\n\n**Affects:** 4.2.1, 4.2.3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453713229"], "labels":["in: core","status: bulk-closed"]}