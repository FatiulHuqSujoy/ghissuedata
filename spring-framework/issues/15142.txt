{"id":"15142", "title":"PropertyDescriptorUtils.java is missing from spring-beans-3.2.2.RELEASE-sources.jar [SPR-10511]", "body":"**[Archie Cobbs](https://jira.spring.io/secure/ViewProfile.jspa?name=archie172)** opened **[SPR-10511](https://jira.spring.io/browse/SPR-10511?redirect=false)** and commented

It appears at least one Java source file is missing from at least one Maven `-sources` artifact in the `3.2.2` release:

```
$ unzip -l ~/.ivy2/packager/cache/spring-beans-3.2.2.RELEASE.jar | grep PropertyDescriptorUtils
     5043  03-13-13 13:56   org/springframework/beans/PropertyDescriptorUtils.class
$ 
```

Yet:

```
$ unzip -l ~/.ivy2/packager/cache/spring-beans-3.2.2.RELEASE-sources.jar | grep PropertyDescriptorUtils
$ 
```

This seems very odd, seeing as all this stuff is supposed to be automated.

Is there some kind of build problem?


---

**Affects:** 3.2.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15142","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15142/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15142/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15142/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15142","id":398158740,"node_id":"MDU6SXNzdWUzOTgxNTg3NDA=","number":15142,"title":"PropertyDescriptorUtils.java is missing from spring-beans-3.2.2.RELEASE-sources.jar [SPR-10511]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-05-03T07:27:25Z","updated_at":"2019-01-12T05:26:59Z","closed_at":"2017-08-01T08:35:06Z","author_association":"COLLABORATOR","body":"**[Archie Cobbs](https://jira.spring.io/secure/ViewProfile.jspa?name=archie172)** opened **[SPR-10511](https://jira.spring.io/browse/SPR-10511?redirect=false)** and commented\n\nIt appears at least one Java source file is missing from at least one Maven `-sources` artifact in the `3.2.2` release:\n\n```\n$ unzip -l ~/.ivy2/packager/cache/spring-beans-3.2.2.RELEASE.jar | grep PropertyDescriptorUtils\n     5043  03-13-13 13:56   org/springframework/beans/PropertyDescriptorUtils.class\n$ \n```\n\nYet:\n\n```\n$ unzip -l ~/.ivy2/packager/cache/spring-beans-3.2.2.RELEASE-sources.jar | grep PropertyDescriptorUtils\n$ \n```\n\nThis seems very odd, seeing as all this stuff is supposed to be automated.\n\nIs there some kind of build problem?\n\n\n---\n\n**Affects:** 3.2.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453402671"], "labels":["status: invalid"]}