{"id":"16034", "title":"EhCacheCacheManager does not wrap runtime-registered caches with TransactionAwareCacheDecorator [SPR-11407]", "body":"**[Christoph Strobl](https://jira.spring.io/secure/ViewProfile.jspa?name=cstrobl)** opened **[SPR-11407](https://jira.spring.io/browse/SPR-11407?redirect=false)** and commented

`EhCacheManager` fails to return a decorated `Cache` when requesting a `Cache` that has been added at runtime, for the first time, from the backing `CacheManager`. This causes values to be added to the cache in case the transaction is rolled back. Any subsequent operations will work fine off the second request `EhCacheManager` returns the properly decorated instance.

The test listed below reproduces the scenario.

* `testValuesShouldNotBeAddedToCacheWhenCacheRequestedForFirstTimeAndTransactionIsRolledBack` fails as the values are added to the cache though they should not have been.
* `testValuesShouldNotBeAddedToCacheWhenCacheRequestedSeveralTimesAndTransactionIsRolledBack` succeeds as the cache was requested one time before using it within the transaction.

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@Transactional
@TransactionConfiguration(transactionManager = \"transactionManager\")
public class TransactionalEhCacheManagerUnitTests {

  // some transactional service where caching is used somewhere
  private @Autowired FooService transactionalService;

  // spring cacheManager
  private @Autowired CacheManager cacheManager;

  // backing EhCacheManager used to add Cache at runtime and get cache statistics
  private @Autowired net.sf.ehcache.CacheManager ehCacheManager;

  @Configuration
  @EnableCaching
  public static class Config {

    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {

      DataSourceTransactionManager txmgr = new DataSourceTransactionManager();
      txmgr.setDataSource(dataSource());
      txmgr.afterPropertiesSet();

      return txmgr;
    }

    @Bean
    public DataSource dataSource() throws SQLException {

      DataSource dataSourceMock = mock(DataSource.class);
      when(dataSourceMock.getConnection()).thenReturn(mock(Connection.class));

      return dataSourceMock;
    }

    @Bean
    public CacheManager cacheManager() {

      EhCacheCacheManager cacheManager = new EhCacheCacheManager();
      cacheManager.setCacheManager(ehCacheManager());
      cacheManager.setTransactionAware(true);

      return cacheManager;
    }

    @Bean
    public net.sf.ehcache.CacheManager ehCacheManager() {
      return net.sf.ehcache.CacheManager.newInstance();
    }

    @Bean
    public FooService fooService() {
      return new FooService();
    }

    @Bean
    public BarRepository barRepository() {
      return new BarRepository();
    }
  }

  @AfterTransaction
  public void after() {
    assertThat(ehCacheManager.getCache(\"bar\").getStatistics().getSize(), equalTo(0L));
  }

  /**
   * If the {@link Cache} is added at runtime the cache manager should use {@link TransactionAwareCacheDecorator} to
   * decorate cache and use the transaction aware one for caching operations.
   */
  @Rollback(true)
  @Test
  public void testValuesShouldNotBeAddedToCacheWhenCacheRequestedForFirstTimeAndTransactionIsRolledBack() {

    ehCacheManager.addCache(\"bar\");
    transactionalService.foo();
  }

  @Rollback(true)
  @Test
  public void testValuesShouldNotBeAddedToCacheWhenCacheRequestedSeveralTimesAndTransactionIsRolledBack() {

    ehCacheManager.addCache(\"bar\");
    // get the cache explicitly one time so that the next call will get the decorated cache instance
    cacheManager.getCache(\"bar\");

    transactionalService.foo();
  }

  static class FooService {

    private @Autowired BarRepository repo;

    @Transactional
    public String foo() {
      return \"foo\" + repo.bar();
    }
  }

  static class BarRepository {

    @Cacheable(\"bar\")
    public String bar() {
      return \"bar\";
    }

  }
}
```

The same issue should exist with `JCacheManager`.

---

**Affects:** 3.2.7, 4.0.1

**Issue Links:**
- #16143 Properly wrap runtime-registered caches with TransactionAwareCacheDecorator

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/ef1748f69444053a9bece0df76859ee10cc1550c, https://github.com/spring-projects/spring-framework/commit/42dec022037b1a942cd546fd6f4b9e3e3991849e

**Backported to:** [3.2.8](https://github.com/spring-projects/spring-framework/milestone/96?closed=1)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16034","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16034/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16034/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16034/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16034","id":398165401,"node_id":"MDU6SXNzdWUzOTgxNjU0MDE=","number":16034,"title":"EhCacheCacheManager does not wrap runtime-registered caches with TransactionAwareCacheDecorator [SPR-11407]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/114","html_url":"https://github.com/spring-projects/spring-framework/milestone/114","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/114/labels","id":3960887,"node_id":"MDk6TWlsZXN0b25lMzk2MDg4Nw==","number":114,"title":"4.0.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":44,"state":"closed","created_at":"2019-01-10T22:04:08Z","updated_at":"2019-01-11T07:39:59Z","due_on":"2014-02-17T08:00:00Z","closed_at":"2019-01-10T22:04:08Z"},"comments":4,"created_at":"2014-02-09T21:31:51Z","updated_at":"2019-01-11T21:27:54Z","closed_at":"2014-03-05T08:13:01Z","author_association":"COLLABORATOR","body":"**[Christoph Strobl](https://jira.spring.io/secure/ViewProfile.jspa?name=cstrobl)** opened **[SPR-11407](https://jira.spring.io/browse/SPR-11407?redirect=false)** and commented\n\n`EhCacheManager` fails to return a decorated `Cache` when requesting a `Cache` that has been added at runtime, for the first time, from the backing `CacheManager`. This causes values to be added to the cache in case the transaction is rolled back. Any subsequent operations will work fine off the second request `EhCacheManager` returns the properly decorated instance.\n\nThe test listed below reproduces the scenario.\n\n* `testValuesShouldNotBeAddedToCacheWhenCacheRequestedForFirstTimeAndTransactionIsRolledBack` fails as the values are added to the cache though they should not have been.\n* `testValuesShouldNotBeAddedToCacheWhenCacheRequestedSeveralTimesAndTransactionIsRolledBack` succeeds as the cache was requested one time before using it within the transaction.\n\n```java\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration\n@Transactional\n@TransactionConfiguration(transactionManager = \"transactionManager\")\npublic class TransactionalEhCacheManagerUnitTests {\n\n  // some transactional service where caching is used somewhere\n  private @Autowired FooService transactionalService;\n\n  // spring cacheManager\n  private @Autowired CacheManager cacheManager;\n\n  // backing EhCacheManager used to add Cache at runtime and get cache statistics\n  private @Autowired net.sf.ehcache.CacheManager ehCacheManager;\n\n  @Configuration\n  @EnableCaching\n  public static class Config {\n\n    @Bean\n    public PlatformTransactionManager transactionManager() throws SQLException {\n\n      DataSourceTransactionManager txmgr = new DataSourceTransactionManager();\n      txmgr.setDataSource(dataSource());\n      txmgr.afterPropertiesSet();\n\n      return txmgr;\n    }\n\n    @Bean\n    public DataSource dataSource() throws SQLException {\n\n      DataSource dataSourceMock = mock(DataSource.class);\n      when(dataSourceMock.getConnection()).thenReturn(mock(Connection.class));\n\n      return dataSourceMock;\n    }\n\n    @Bean\n    public CacheManager cacheManager() {\n\n      EhCacheCacheManager cacheManager = new EhCacheCacheManager();\n      cacheManager.setCacheManager(ehCacheManager());\n      cacheManager.setTransactionAware(true);\n\n      return cacheManager;\n    }\n\n    @Bean\n    public net.sf.ehcache.CacheManager ehCacheManager() {\n      return net.sf.ehcache.CacheManager.newInstance();\n    }\n\n    @Bean\n    public FooService fooService() {\n      return new FooService();\n    }\n\n    @Bean\n    public BarRepository barRepository() {\n      return new BarRepository();\n    }\n  }\n\n  @AfterTransaction\n  public void after() {\n    assertThat(ehCacheManager.getCache(\"bar\").getStatistics().getSize(), equalTo(0L));\n  }\n\n  /**\n   * If the {@link Cache} is added at runtime the cache manager should use {@link TransactionAwareCacheDecorator} to\n   * decorate cache and use the transaction aware one for caching operations.\n   */\n  @Rollback(true)\n  @Test\n  public void testValuesShouldNotBeAddedToCacheWhenCacheRequestedForFirstTimeAndTransactionIsRolledBack() {\n\n    ehCacheManager.addCache(\"bar\");\n    transactionalService.foo();\n  }\n\n  @Rollback(true)\n  @Test\n  public void testValuesShouldNotBeAddedToCacheWhenCacheRequestedSeveralTimesAndTransactionIsRolledBack() {\n\n    ehCacheManager.addCache(\"bar\");\n    // get the cache explicitly one time so that the next call will get the decorated cache instance\n    cacheManager.getCache(\"bar\");\n\n    transactionalService.foo();\n  }\n\n  static class FooService {\n\n    private @Autowired BarRepository repo;\n\n    @Transactional\n    public String foo() {\n      return \"foo\" + repo.bar();\n    }\n  }\n\n  static class BarRepository {\n\n    @Cacheable(\"bar\")\n    public String bar() {\n      return \"bar\";\n    }\n\n  }\n}\n```\n\nThe same issue should exist with `JCacheManager`.\n\n---\n\n**Affects:** 3.2.7, 4.0.1\n\n**Issue Links:**\n- #16143 Properly wrap runtime-registered caches with TransactionAwareCacheDecorator\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/ef1748f69444053a9bece0df76859ee10cc1550c, https://github.com/spring-projects/spring-framework/commit/42dec022037b1a942cd546fd6f4b9e3e3991849e\n\n**Backported to:** [3.2.8](https://github.com/spring-projects/spring-framework/milestone/96?closed=1)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453409742","453409743","453409745","453409747"], "labels":["in: core","status: backported","type: bug"]}