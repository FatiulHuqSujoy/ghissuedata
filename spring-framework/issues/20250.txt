{"id":"20250", "title":"Simplify applying a strategy to requests from WebTestClient and WebTestClient [SPR-15691]", "body":"**[Rob Winch](https://jira.spring.io/secure/ViewProfile.jspa?name=rwinch)** opened **[SPR-15691](https://jira.spring.io/browse/SPR-15691?redirect=false)** and commented

# Description

With the changes from #20216 modifying a request with a strategy (i.e. `ExchangeFilterFunction`) becomes 3 statements instead of a single one. For example, we use to have:

```
webClient
  .filter(basicAuthentication(\"user\",\"password))
  ...
```

now the code is

```
webClient
  .mutate()
  .filter(basicAuthentication(\"user\",\"password))
  .build()
  ...
```

It would be ideal to make this a single statement again.

# Solution Ideas

## Combining Methods

One idea would be to provide a single method that accepts an `ExchangeFilterFunction` and invokes all three methods.

```
webClient
   // shortcut for mutate, filter, build
  .with(basicAuthentication(\"user\",\"password))
  ...
```

## ClientRequest attributes

Another (improved) approach would be to allow `ClientRequest` to have some concept of attributes. Perhaps something like:

```
webClient
   .with( requestAttrs -> { .. })
   ...
```

This would allow the `ExchangeFilterFunction` to leverage the requestAttrs and thus remain stateless.

This approach improves usability in regards to if an `ExchangeFilterFunction` needed some values that were always reused. Currently (and in the past) we would need to do something like:

```
webClient
  .filter(oauth(someBeanThatIsConstant, a1, b1,...))
  ...

webClient
  .filter(oauth(someBeanThatIsConstant, a2, b2,...))
  ...
```

You can see that this is a burden on the user to have to provide `someBeanThatIsConstant` for every request. With the above approach the `someBeanThatIsConstant` can be injected into an `ExchangeFilterFunction` that is global and the requestAttrs would provide values that change.

```
webClient = ...
    .filter(oauth(aBeanThatIsConstant))
    ...

webClient
  .with( requestAttrs -> { ... } )
  ...

webClient
  // here oauth is just a static method provided by Spring Security that creates
  // a consumer that does the same thing as above
  .with( oauth(a1,a2) )
  ...
```

This approach also means we can make additional requests in the `ExchangeFilterFunction` using the `ExchangeFunction` without worrying about another `ExchangeFilterFunction` adding something that is isn't desirable to the request. For example, currently if a user makes an HTTP basic request and we want to do something inside another `ExhangeFunction` that makes a request, the `ExchangeFunction` passed in would automatically populate HTTP Basic on the new request which is not necessarily desirable.

With `ClientRequest` attributes the attributes would not be populated on this additional request so the `ExchangeFunction` would not add the basic auth headers.

---

**Affects:** 5.0 RC3

**Issue Links:**
- #20216 Ordering of WebClient.filter(s)

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/74b4c028819c67b90d4d06d352d3083d91d8a3f8
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20250","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20250/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20250/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20250/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20250","id":398211357,"node_id":"MDU6SXNzdWUzOTgyMTEzNTc=","number":20250,"title":"Simplify applying a strategy to requests from WebTestClient and WebTestClient [SPR-15691]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/179","html_url":"https://github.com/spring-projects/spring-framework/milestone/179","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/179/labels","id":3960952,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1Mg==","number":179,"title":"5.0 RC3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":70,"state":"closed","created_at":"2019-01-10T22:05:26Z","updated_at":"2019-01-11T09:55:20Z","due_on":"2017-07-23T07:00:00Z","closed_at":"2019-01-10T22:05:26Z"},"comments":2,"created_at":"2017-06-22T16:35:43Z","updated_at":"2019-01-11T16:01:53Z","closed_at":"2017-07-24T07:51:14Z","author_association":"COLLABORATOR","body":"**[Rob Winch](https://jira.spring.io/secure/ViewProfile.jspa?name=rwinch)** opened **[SPR-15691](https://jira.spring.io/browse/SPR-15691?redirect=false)** and commented\n\n# Description\n\nWith the changes from #20216 modifying a request with a strategy (i.e. `ExchangeFilterFunction`) becomes 3 statements instead of a single one. For example, we use to have:\n\n```\nwebClient\n  .filter(basicAuthentication(\"user\",\"password))\n  ...\n```\n\nnow the code is\n\n```\nwebClient\n  .mutate()\n  .filter(basicAuthentication(\"user\",\"password))\n  .build()\n  ...\n```\n\nIt would be ideal to make this a single statement again.\n\n# Solution Ideas\n\n## Combining Methods\n\nOne idea would be to provide a single method that accepts an `ExchangeFilterFunction` and invokes all three methods.\n\n```\nwebClient\n   // shortcut for mutate, filter, build\n  .with(basicAuthentication(\"user\",\"password))\n  ...\n```\n\n## ClientRequest attributes\n\nAnother (improved) approach would be to allow `ClientRequest` to have some concept of attributes. Perhaps something like:\n\n```\nwebClient\n   .with( requestAttrs -> { .. })\n   ...\n```\n\nThis would allow the `ExchangeFilterFunction` to leverage the requestAttrs and thus remain stateless.\n\nThis approach improves usability in regards to if an `ExchangeFilterFunction` needed some values that were always reused. Currently (and in the past) we would need to do something like:\n\n```\nwebClient\n  .filter(oauth(someBeanThatIsConstant, a1, b1,...))\n  ...\n\nwebClient\n  .filter(oauth(someBeanThatIsConstant, a2, b2,...))\n  ...\n```\n\nYou can see that this is a burden on the user to have to provide `someBeanThatIsConstant` for every request. With the above approach the `someBeanThatIsConstant` can be injected into an `ExchangeFilterFunction` that is global and the requestAttrs would provide values that change.\n\n```\nwebClient = ...\n    .filter(oauth(aBeanThatIsConstant))\n    ...\n\nwebClient\n  .with( requestAttrs -> { ... } )\n  ...\n\nwebClient\n  // here oauth is just a static method provided by Spring Security that creates\n  // a consumer that does the same thing as above\n  .with( oauth(a1,a2) )\n  ...\n```\n\nThis approach also means we can make additional requests in the `ExchangeFilterFunction` using the `ExchangeFunction` without worrying about another `ExchangeFilterFunction` adding something that is isn't desirable to the request. For example, currently if a user makes an HTTP basic request and we want to do something inside another `ExhangeFunction` that makes a request, the `ExchangeFunction` passed in would automatically populate HTTP Basic on the new request which is not necessarily desirable.\n\nWith `ClientRequest` attributes the attributes would not be populated on this additional request so the `ExchangeFunction` would not add the basic auth headers.\n\n---\n\n**Affects:** 5.0 RC3\n\n**Issue Links:**\n- #20216 Ordering of WebClient.filter(s)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/74b4c028819c67b90d4d06d352d3083d91d8a3f8\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453457267","453457273"], "labels":["in: test","in: web","type: enhancement"]}