{"id":"18659", "title":"Doc: Different behavior for @Named and @Component [SPR-14087]", "body":"**[MD Sayem Ahmed](https://jira.spring.io/secure/ViewProfile.jspa?name=sayembd)** opened **[SPR-14087](https://jira.spring.io/browse/SPR-14087?redirect=false)** and commented

I am trying to understand the differences between these two annotations and how they affect injection in Spring. Consider the following piece of code -

```java
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ExternalPropertiesHolder {}
```

When I mark a class with this annotation -

```java
@ExternalPropertiesHolder
public class SomeProperties {}
```

and then this dependency is injected using `@Inject`, it works perfectly -

```java
@Service
public class SomeService {
    private SomeProperties someProperties;

    @Inject
    public SomeService(SomeProperties someProperties) {
        this.someProperties = someProperties;
    }
}
```

However, when I replace `@Component` with `@Named` -

```java
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Named           // --> Here!
public @interface ExternalPropertiesHolder {}
```

Then the injection fails with the usual bean not found exception -

> Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type [com.hogehoge.SomeProperties] found for dependency: expected at least 1 bean which qualifies as autowire candidate for this dependency. Dependency annotations: {}

Shouldn't they behave the same way?

I am using Spring Boot 1.3.3.RELEASE version, and using JDK 8 to compile my application.


---

**Reference URL:** http://stackoverflow.com/questions/36203489/spring-differences-between-named-and-component

**Backported to:** [4.2.7](https://github.com/spring-projects/spring-framework/milestone/144?closed=1)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18659","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18659/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18659/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18659/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18659","id":398191291,"node_id":"MDU6SXNzdWUzOTgxOTEyOTE=","number":18659,"title":"Doc: Different behavior for @Named and @Component [SPR-14087]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511933,"node_id":"MDU6TGFiZWwxMTg4NTExOTMz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20task","name":"type: task","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/149","html_url":"https://github.com/spring-projects/spring-framework/milestone/149","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/149/labels","id":3960922,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyMg==","number":149,"title":"4.3 GA","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":63,"state":"closed","created_at":"2019-01-10T22:04:50Z","updated_at":"2019-01-11T09:14:25Z","due_on":"2016-06-09T07:00:00Z","closed_at":"2019-01-10T22:04:50Z"},"comments":3,"created_at":"2016-03-24T15:49:50Z","updated_at":"2016-06-10T09:19:12Z","closed_at":"2016-06-10T09:19:12Z","author_association":"COLLABORATOR","body":"**[MD Sayem Ahmed](https://jira.spring.io/secure/ViewProfile.jspa?name=sayembd)** opened **[SPR-14087](https://jira.spring.io/browse/SPR-14087?redirect=false)** and commented\n\nI am trying to understand the differences between these two annotations and how they affect injection in Spring. Consider the following piece of code -\n\n```java\n@Target({ElementType.TYPE})\n@Retention(RetentionPolicy.RUNTIME)\n@Documented\n@Component\npublic @interface ExternalPropertiesHolder {}\n```\n\nWhen I mark a class with this annotation -\n\n```java\n@ExternalPropertiesHolder\npublic class SomeProperties {}\n```\n\nand then this dependency is injected using `@Inject`, it works perfectly -\n\n```java\n@Service\npublic class SomeService {\n    private SomeProperties someProperties;\n\n    @Inject\n    public SomeService(SomeProperties someProperties) {\n        this.someProperties = someProperties;\n    }\n}\n```\n\nHowever, when I replace `@Component` with `@Named` -\n\n```java\n@Target({ElementType.TYPE})\n@Retention(RetentionPolicy.RUNTIME)\n@Documented\n@Named           // --> Here!\npublic @interface ExternalPropertiesHolder {}\n```\n\nThen the injection fails with the usual bean not found exception -\n\n> Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type [com.hogehoge.SomeProperties] found for dependency: expected at least 1 bean which qualifies as autowire candidate for this dependency. Dependency annotations: {}\n\nShouldn't they behave the same way?\n\nI am using Spring Boot 1.3.3.RELEASE version, and using JDK 8 to compile my application.\n\n\n---\n\n**Reference URL:** http://stackoverflow.com/questions/36203489/spring-differences-between-named-and-component\n\n**Backported to:** [4.2.7](https://github.com/spring-projects/spring-framework/milestone/144?closed=1)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453437839","453437842","453437844"], "labels":["in: core","status: backported","type: task"]}