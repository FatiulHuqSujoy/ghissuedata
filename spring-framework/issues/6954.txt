{"id":"6954", "title":"<aop:aspectj-autoproxy/> doesn't care for abstract beans [SPR-2265]", "body":"**[Corby Page](https://jira.spring.io/secure/ViewProfile.jspa?name=corby)** opened **[SPR-2265](https://jira.spring.io/browse/SPR-2265?redirect=false)** and commented

I have a set of Spring config files that work just fine, until I add:

<aop:aspectj-autoproxy/>

Note, this is without adding any pointcut or advisor definitions, I just added the autoproxy declaration.

It appears to object to the existence of an abstract bean that is defined in my config:

\\<bean id=\"baseSessionFactory\" class=\"org.springframework.orm.hibernate3.LocalSessionFactoryBean\" abstract=\"true\">
\\<property name=\"mappingResources\">
\\<list>
\\<value>bigdeal.hbm.xml\\</value>
\\<value>osworkflow.hbm.xml\\</value>
\\</list>
\\</property>
\\<property name=\"dataSource\" ref=\"dataSource\"/>
\\<property name=\"entityInterceptor\" ref=\"dependencyInjectionInterceptor\"/>
\\</bean>
\\<bean id=\"bigdealSessionFactory\" parent=\"baseSessionFactory\">
\\<property name=\"hibernateProperties\">
\\<props>
[Boring config excluded]
\\</props>
\\</property>
\\</bean>

The stacktrace I get on startup is:

org.springframework.beans.factory.BeanCreationExce ption: Error creating bean with name 'bigdealSessionFactory' defined in class path resource [serverComponents.xml]:
Cannot resolve reference to bean 'dataSource' while setting bean property'dataSource';
nested exception is org.springframework.beans.factory.BeanIsAbstractEx ception:
Error creating bean with name 'baseSessionFactory':
Bean definition is abstract

Caused by: org.springframework.beans.factory.BeanIsAbstractEx ception:
Error creating bean with name 'baseSessionFactory':
Bean definition is abstract

at org.springframework.beans.factory.support.Abstract BeanFactory.checkMergedBeanDefinition(AbstractBean Factory.java:804)
at org.springframework.beans.factory.support.Abstract BeanFactory.getBean(AbstractBeanFactory.java:233)
at org.springframework.beans.factory.support.Abstract BeanFactory.getBean(AbstractBeanFactory.java:153)
at org.springframework.beans.factory.support.Abstract BeanFactory.getType(AbstractBeanFactory.java:377)
at org.springframework.aop.aspectj.annotation.Annotat ionAwareAspectJAutoProxyCreator.createAspectJAdvis ors(AnnotationAwareAspectJAutoProxyCreator.java:16 7)
at org.springframework.aop.aspectj.annotation.Annotat ionAwareAspectJAutoProxyCreator.findCandidateAdvis ors(AnnotationAwareAspectJAutoProxyCreator.java:13 7)
at org.springframework.aop.framework.autoproxy.Abstra ctAdvisorAutoProxyCreator.findEligibleAdvisors(Abs tractAdvisorAutoProxyCreator.java:67)
at org.springframework.aop.framework.autoproxy.Abstra ctAdvisorAutoProxyCreator.getAdvicesAndAdvisorsFor Bean(AbstractAdvisorAutoProxyCreator.java:53)



---

**Affects:** 2.0 RC2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6954","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6954/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6954/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6954/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6954","id":398068082,"node_id":"MDU6SXNzdWUzOTgwNjgwODI=","number":6954,"title":"<aop:aspectj-autoproxy/> doesn't care for abstract beans [SPR-2265]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false,"description":"A general bug"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/34","html_url":"https://github.com/spring-projects/spring-framework/milestone/34","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/34/labels","id":3960805,"node_id":"MDk6TWlsZXN0b25lMzk2MDgwNQ==","number":34,"title":"2.0 RC3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":87,"state":"closed","created_at":"2019-01-10T22:02:32Z","updated_at":"2019-01-10T23:56:15Z","due_on":null,"closed_at":"2019-01-10T22:02:32Z"},"comments":0,"created_at":"2006-07-09T07:02:27Z","updated_at":"2012-06-19T03:52:38Z","closed_at":"2012-06-19T03:52:38Z","author_association":"COLLABORATOR","body":"**[Corby Page](https://jira.spring.io/secure/ViewProfile.jspa?name=corby)** opened **[SPR-2265](https://jira.spring.io/browse/SPR-2265?redirect=false)** and commented\n\nI have a set of Spring config files that work just fine, until I add:\n\n<aop:aspectj-autoproxy/>\n\nNote, this is without adding any pointcut or advisor definitions, I just added the autoproxy declaration.\n\nIt appears to object to the existence of an abstract bean that is defined in my config:\n\n\\<bean id=\"baseSessionFactory\" class=\"org.springframework.orm.hibernate3.LocalSessionFactoryBean\" abstract=\"true\">\n\\<property name=\"mappingResources\">\n\\<list>\n\\<value>bigdeal.hbm.xml\\</value>\n\\<value>osworkflow.hbm.xml\\</value>\n\\</list>\n\\</property>\n\\<property name=\"dataSource\" ref=\"dataSource\"/>\n\\<property name=\"entityInterceptor\" ref=\"dependencyInjectionInterceptor\"/>\n\\</bean>\n\\<bean id=\"bigdealSessionFactory\" parent=\"baseSessionFactory\">\n\\<property name=\"hibernateProperties\">\n\\<props>\n[Boring config excluded]\n\\</props>\n\\</property>\n\\</bean>\n\nThe stacktrace I get on startup is:\n\norg.springframework.beans.factory.BeanCreationExce ption: Error creating bean with name 'bigdealSessionFactory' defined in class path resource [serverComponents.xml]:\nCannot resolve reference to bean 'dataSource' while setting bean property'dataSource';\nnested exception is org.springframework.beans.factory.BeanIsAbstractEx ception:\nError creating bean with name 'baseSessionFactory':\nBean definition is abstract\n\nCaused by: org.springframework.beans.factory.BeanIsAbstractEx ception:\nError creating bean with name 'baseSessionFactory':\nBean definition is abstract\n\nat org.springframework.beans.factory.support.Abstract BeanFactory.checkMergedBeanDefinition(AbstractBean Factory.java:804)\nat org.springframework.beans.factory.support.Abstract BeanFactory.getBean(AbstractBeanFactory.java:233)\nat org.springframework.beans.factory.support.Abstract BeanFactory.getBean(AbstractBeanFactory.java:153)\nat org.springframework.beans.factory.support.Abstract BeanFactory.getType(AbstractBeanFactory.java:377)\nat org.springframework.aop.aspectj.annotation.Annotat ionAwareAspectJAutoProxyCreator.createAspectJAdvis ors(AnnotationAwareAspectJAutoProxyCreator.java:16 7)\nat org.springframework.aop.aspectj.annotation.Annotat ionAwareAspectJAutoProxyCreator.findCandidateAdvis ors(AnnotationAwareAspectJAutoProxyCreator.java:13 7)\nat org.springframework.aop.framework.autoproxy.Abstra ctAdvisorAutoProxyCreator.findEligibleAdvisors(Abs tractAdvisorAutoProxyCreator.java:67)\nat org.springframework.aop.framework.autoproxy.Abstra ctAdvisorAutoProxyCreator.getAdvicesAndAdvisorsFor Bean(AbstractAdvisorAutoProxyCreator.java:53)\n\n\n\n---\n\n**Affects:** 2.0 RC2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["in: core","type: bug"]}