{"id":"5817", "title":"CustomEditor getAsText() not called in MultiActionController [SPR-1113]", "body":"**[Choon Whee](https://jira.spring.io/secure/ViewProfile.jspa?name=chunhui)** opened **[SPR-1113](https://jira.spring.io/browse/SPR-1113?redirect=false)** and commented

I have a problem in which when I register a CustomDateEditor in initBinder() method in MultiActionController (Works perfectly in SimpleFormController), when the Date is not converted to the specified String format.

my code is as follows briefly:

protected void initBinder(ServletRequest servletRequest, ServletRequestDataBinder binder) {
CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat
('dd/MM/yyyy'), true);
binder.registerCustomEditor(java.util.Date.class, cde);
}

and my jsp binding code:

<spring:bind path=\"updateMember.dob\">
<c:out value=\"status.value\">
</spring:bind>

I later tried to test this by writting a method that extends CustomDateEditor and overwrites getAsText() and setAsText() as such:

... String getAsText(String s){
text = super.getAsText(s);
logger.info(\"getAsText():\" + text);
return text;
}

when I tried it out, I found that setAsText() was called when I do a submission. but when I load the page, getAsText() is never called.

To make sure I did not write my code and configuration wrongly, I converted my MultiActionController to SimpleFormController with the essential code intact and it worked perfectly as expected.

another issue thats connected... in the MultiActionController, when I submit a wrongly formatted date, I get shown the exception page with the BindException.

I have this in my message.properties:
typeMismatch=Invalid data.
typeMismatch.java.util.Date=Invalid date format.

Again, I tried out in SimpleFormController and it works perfectly. I tried out the codes in Spring 1.2.1 and later 1.2.2. Same results.

I looked through the forums and realised a few ppl have the same problems. but nobody did solve their problems. All their final verdict was it should work and nobody really was able to help further and no one solved their problem.

They didn't mention the controllers they are using, so I suppose those who reply should work is using FormControllers while those who encountered problem are using MultiActionController? So I think this may fit the puzzle why some ppl can work and some can't.


---

**Affects:** 1.2.1, 1.2.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5817","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5817/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5817/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5817/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5817","id":398058349,"node_id":"MDU6SXNzdWUzOTgwNTgzNDk=","number":5817,"title":"CustomEditor getAsText() not called in MultiActionController [SPR-1113]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2005-07-07T21:13:31Z","updated_at":"2019-01-12T16:41:05Z","closed_at":"2012-06-19T03:18:55Z","author_association":"COLLABORATOR","body":"**[Choon Whee](https://jira.spring.io/secure/ViewProfile.jspa?name=chunhui)** opened **[SPR-1113](https://jira.spring.io/browse/SPR-1113?redirect=false)** and commented\n\nI have a problem in which when I register a CustomDateEditor in initBinder() method in MultiActionController (Works perfectly in SimpleFormController), when the Date is not converted to the specified String format.\n\nmy code is as follows briefly:\n\nprotected void initBinder(ServletRequest servletRequest, ServletRequestDataBinder binder) {\nCustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat\n('dd/MM/yyyy'), true);\nbinder.registerCustomEditor(java.util.Date.class, cde);\n}\n\nand my jsp binding code:\n\n<spring:bind path=\"updateMember.dob\">\n<c:out value=\"status.value\">\n</spring:bind>\n\nI later tried to test this by writting a method that extends CustomDateEditor and overwrites getAsText() and setAsText() as such:\n\n... String getAsText(String s){\ntext = super.getAsText(s);\nlogger.info(\"getAsText():\" + text);\nreturn text;\n}\n\nwhen I tried it out, I found that setAsText() was called when I do a submission. but when I load the page, getAsText() is never called.\n\nTo make sure I did not write my code and configuration wrongly, I converted my MultiActionController to SimpleFormController with the essential code intact and it worked perfectly as expected.\n\nanother issue thats connected... in the MultiActionController, when I submit a wrongly formatted date, I get shown the exception page with the BindException.\n\nI have this in my message.properties:\ntypeMismatch=Invalid data.\ntypeMismatch.java.util.Date=Invalid date format.\n\nAgain, I tried out in SimpleFormController and it works perfectly. I tried out the codes in Spring 1.2.1 and later 1.2.2. Same results.\n\nI looked through the forums and realised a few ppl have the same problems. but nobody did solve their problems. All their final verdict was it should work and nobody really was able to help further and no one solved their problem.\n\nThey didn't mention the controllers they are using, so I suppose those who reply should work is using FormControllers while those who encountered problem are using MultiActionController? So I think this may fit the puzzle why some ppl can work and some can't.\n\n\n---\n\n**Affects:** 1.2.1, 1.2.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453295725","453295727","453295731","453295733","453295734"], "labels":["in: web","status: declined"]}