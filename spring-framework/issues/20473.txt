{"id":"20473", "title":"only one MultipartFile object populated when using an java.util.Optional MutipartFile array or list @RequestParam [SPR-15919]", "body":"**[Brice Roncace](https://jira.spring.io/secure/ViewProfile.jspa?name=broncace)** opened **[SPR-15919](https://jira.spring.io/browse/SPR-15919?redirect=false)** and commented

Having the `multipart/form-data` form:

```
<form action=\"<c:url value=\"/optionalPost\"/>\" method=\"post\" enctype=\"multipart/form-data\">
  <input type=\"hidden\" name=\"${_csrf.parameterName}\" value=\"${_csrf.token}\"/>
  <input type=\"file\" name=\"files\" multiple/>
  <input type=\"submit\"/>
</form>
```

When selecting multiple files for the file input, only one file is actually received by the controller when handling this request:

```
 // Does NOT work for multiple files (only one comes in)
@PostMapping(\"/optionalPost\")
public String postTest2(@RequestParam Optional<List<MultipartFile>> files) {
  if (files != null && files.isPresent()) {
    System.out.println(files.get().size());
    files.get().forEach(file -> System.out.println(file.getOriginalFilename()));
  }
  return \"index\";
}
```

This fails in the same way:

```
// Does NOT work for multiple files (only one comes in)
@PostMapping(\"/optionalPost\")
public String postTest3(@RequestParam Optional<MultipartFile[]> files) {
  if (files != null && files.isPresent()) {
    System.out.println(files.get().length);
    Stream.of(files.get()).forEach(file -> System.out.println(file.getOriginalFilename()));
  }
  return \"index\";
}
```

The workaround is to avoid the use of `Optional`:

```
// Works for multiple files
@PostMapping(\"/optionalPost\")
public String postTest4(@RequestParam MultipartFile[] files) {
  System.out.println(files.length);
  Stream.of(files).forEach(file -> System.out.println(file.getOriginalFilename()));
  return \"index\";
}
```

```
// Works for multiple files
@PostMapping(\"/optionalPost\")
public String postTest5(@RequestParam List<MultipartFile> files) {
  System.out.println(files.size());
  files.forEach(file -> System.out.println(file.getOriginalFilename()));
  return \"index\";
}
```

---

**Affects:** 4.3.10

**Issue Links:**
- #20235 Request params Optional<List<String> and List<String> are inconsistent
- #20472 java.util.Optional MultipartFile[] `@RequestParam` argument is null in multipart/form-data POST

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/53a9697ff1437896ae323b0108c8b82a2ba1c551, https://github.com/spring-projects/spring-framework/commit/15c82afc1cdedc95b453e8f4ca61fd72f4a3ae82

**Backported to:** [4.3.12](https://github.com/spring-projects/spring-framework/milestone/161?closed=1)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20473","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20473/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20473/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20473/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20473","id":398214934,"node_id":"MDU6SXNzdWUzOTgyMTQ5MzQ=","number":20473,"title":"only one MultipartFile object populated when using an java.util.Optional MutipartFile array or list @RequestParam [SPR-15919]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/181","html_url":"https://github.com/spring-projects/spring-framework/milestone/181","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/181/labels","id":3960954,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1NA==","number":181,"title":"5.0 GA","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":54,"state":"closed","created_at":"2019-01-10T22:05:29Z","updated_at":"2019-01-11T11:16:38Z","due_on":"2017-09-27T07:00:00Z","closed_at":"2019-01-10T22:05:29Z"},"comments":1,"created_at":"2017-08-31T14:06:39Z","updated_at":"2019-01-13T05:05:11Z","closed_at":"2017-09-28T12:14:38Z","author_association":"COLLABORATOR","body":"**[Brice Roncace](https://jira.spring.io/secure/ViewProfile.jspa?name=broncace)** opened **[SPR-15919](https://jira.spring.io/browse/SPR-15919?redirect=false)** and commented\n\nHaving the `multipart/form-data` form:\n\n```\n<form action=\"<c:url value=\"/optionalPost\"/>\" method=\"post\" enctype=\"multipart/form-data\">\n  <input type=\"hidden\" name=\"${_csrf.parameterName}\" value=\"${_csrf.token}\"/>\n  <input type=\"file\" name=\"files\" multiple/>\n  <input type=\"submit\"/>\n</form>\n```\n\nWhen selecting multiple files for the file input, only one file is actually received by the controller when handling this request:\n\n```\n // Does NOT work for multiple files (only one comes in)\n@PostMapping(\"/optionalPost\")\npublic String postTest2(@RequestParam Optional<List<MultipartFile>> files) {\n  if (files != null && files.isPresent()) {\n    System.out.println(files.get().size());\n    files.get().forEach(file -> System.out.println(file.getOriginalFilename()));\n  }\n  return \"index\";\n}\n```\n\nThis fails in the same way:\n\n```\n// Does NOT work for multiple files (only one comes in)\n@PostMapping(\"/optionalPost\")\npublic String postTest3(@RequestParam Optional<MultipartFile[]> files) {\n  if (files != null && files.isPresent()) {\n    System.out.println(files.get().length);\n    Stream.of(files.get()).forEach(file -> System.out.println(file.getOriginalFilename()));\n  }\n  return \"index\";\n}\n```\n\nThe workaround is to avoid the use of `Optional`:\n\n```\n// Works for multiple files\n@PostMapping(\"/optionalPost\")\npublic String postTest4(@RequestParam MultipartFile[] files) {\n  System.out.println(files.length);\n  Stream.of(files).forEach(file -> System.out.println(file.getOriginalFilename()));\n  return \"index\";\n}\n```\n\n```\n// Works for multiple files\n@PostMapping(\"/optionalPost\")\npublic String postTest5(@RequestParam List<MultipartFile> files) {\n  System.out.println(files.size());\n  files.forEach(file -> System.out.println(file.getOriginalFilename()));\n  return \"index\";\n}\n```\n\n---\n\n**Affects:** 4.3.10\n\n**Issue Links:**\n- #20235 Request params Optional<List<String> and List<String> are inconsistent\n- #20472 java.util.Optional MultipartFile[] `@RequestParam` argument is null in multipart/form-data POST\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/53a9697ff1437896ae323b0108c8b82a2ba1c551, https://github.com/spring-projects/spring-framework/commit/15c82afc1cdedc95b453e8f4ca61fd72f4a3ae82\n\n**Backported to:** [4.3.12](https://github.com/spring-projects/spring-framework/milestone/161?closed=1)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453460464"], "labels":["in: web","status: backported","type: bug"]}