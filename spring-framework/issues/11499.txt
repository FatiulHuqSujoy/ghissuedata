{"id":"11499", "title":"@RequestBody Does not support inheritance [SPR-6833]", "body":"**[Bruce Lowe](https://jira.spring.io/secure/ViewProfile.jspa?name=brucelowe)** opened **[SPR-6833](https://jira.spring.io/browse/SPR-6833?redirect=false)** and commented

I have a controller with a methods defined as such

`@RequestMapping`(value = \"/services/{serviceName}/scenes/\", method = RequestMethod.POST)
public void addScene(`@PathVariable`(\"serviceName\") String serviceName,
`@RequestBody` SceneTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)
{
...
}

It is designed to be able to POST \"SceneTO\"s to.   I have made a smart custom message converters so people can submit VideoScenes, AudioScenes, TextScenes etc.  The convertor will create the appropriate SceneTO inheriting object.

However, i get this exception:

java.lang.IllegalArgumentException: argument type mismatch
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
at java.lang.reflect.Method.invoke(Method.java:597)
at org.springframework.web.bind.annotation.support.HandlerMethodInvoker.doInvokeMethod(HandlerMethodInvoker.java:710)
at org.springframework.web.bind.annotation.support.HandlerMethodInvoker.invokeHandlerMethod(HandlerMethodInvoker.java:167)
at org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter.invokeHandlerMethod(AnnotationMethodHandlerAdapter.java:414)
at org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter.handle(AnnotationMethodHandlerAdapter.java:402)
at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:771)
at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:716)
at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:647)
at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:563)
at javax.servlet.http.HttpServlet.service(HttpServlet.java:727)
at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)
at org.mortbay.jetty.servlet.ServletHolder.handle(ServletHolder.java:487)
at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1093)
at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:71)
at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:76)
at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1084)
at org.mortbay.jetty.servlet.ServletHandler.handle(ServletHandler.java:360)
at org.mortbay.jetty.security.SecurityHandler.handle(SecurityHandler.java:216)
at org.mortbay.jetty.servlet.SessionHandler.handle(SessionHandler.java:181)
at org.mortbay.jetty.handler.ContextHandler.handle(ContextHandler.java:726)
at org.mortbay.jetty.webapp.WebAppContext.handle(WebAppContext.java:405)
at org.mortbay.jetty.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:206)
at org.mortbay.jetty.handler.HandlerCollection.handle(HandlerCollection.java:114)
at org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)
at org.mortbay.jetty.Server.handle(Server.java:324)
at org.mortbay.jetty.HttpConnection.handleRequest(HttpConnection.java:505)
at org.mortbay.jetty.HttpConnection$RequestHandler.content(HttpConnection.java:842)
at org.mortbay.jetty.HttpParser.parseNext(HttpParser.java:648)
at org.mortbay.jetty.HttpParser.parseAvailable(HttpParser.java:211)
at org.mortbay.jetty.HttpConnection.handle(HttpConnection.java:380)
at org.mortbay.io.nio.SelectChannelEndPoint.run(SelectChannelEndPoint.java:395)
at org.mortbay.thread.BoundedThreadPool$PoolThread.run(BoundedThreadPool.java:450)

If I change my convertor to ONLY create SceneTO objects (instead of an inheriting one), it works fine. The method clearly does not support inheritance

We can currently work around the problem by doing this

`@RequestMapping`(value = \"/services/{serviceName}/scenes/video\", method = RequestMethod.POST)
public void addScene(`@PathVariable`(\"serviceName\") String serviceName,
`@RequestBody` VideoTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)
and
`@RequestMapping`(value = \"/services/{serviceName}/scenes/audio\", method = RequestMethod.POST)
public void addScene(`@PathVariable`(\"serviceName\") String serviceName,
`@RequestBody` AudioTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)

But it does seem less clean and results in much duplicated code.  Allowing inheritance would seem much more elegant.  (I imagine some other people may want interfaces as well)


---
No further details from [SPR-6833](https://jira.spring.io/browse/SPR-6833?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11499","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11499/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11499/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11499/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11499","id":398102890,"node_id":"MDU6SXNzdWUzOTgxMDI4OTA=","number":11499,"title":"@RequestBody Does not support inheritance [SPR-6833]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2010-02-12T03:15:51Z","updated_at":"2019-01-12T05:32:32Z","closed_at":"2013-11-14T08:32:42Z","author_association":"COLLABORATOR","body":"**[Bruce Lowe](https://jira.spring.io/secure/ViewProfile.jspa?name=brucelowe)** opened **[SPR-6833](https://jira.spring.io/browse/SPR-6833?redirect=false)** and commented\n\nI have a controller with a methods defined as such\n\n`@RequestMapping`(value = \"/services/{serviceName}/scenes/\", method = RequestMethod.POST)\npublic void addScene(`@PathVariable`(\"serviceName\") String serviceName,\n`@RequestBody` SceneTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)\n{\n...\n}\n\nIt is designed to be able to POST \"SceneTO\"s to.   I have made a smart custom message converters so people can submit VideoScenes, AudioScenes, TextScenes etc.  The convertor will create the appropriate SceneTO inheriting object.\n\nHowever, i get this exception:\n\njava.lang.IllegalArgumentException: argument type mismatch\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\nat java.lang.reflect.Method.invoke(Method.java:597)\nat org.springframework.web.bind.annotation.support.HandlerMethodInvoker.doInvokeMethod(HandlerMethodInvoker.java:710)\nat org.springframework.web.bind.annotation.support.HandlerMethodInvoker.invokeHandlerMethod(HandlerMethodInvoker.java:167)\nat org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter.invokeHandlerMethod(AnnotationMethodHandlerAdapter.java:414)\nat org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter.handle(AnnotationMethodHandlerAdapter.java:402)\nat org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:771)\nat org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:716)\nat org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:647)\nat org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:563)\nat javax.servlet.http.HttpServlet.service(HttpServlet.java:727)\nat javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\nat org.mortbay.jetty.servlet.ServletHolder.handle(ServletHolder.java:487)\nat org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1093)\nat org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:71)\nat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:76)\nat org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1084)\nat org.mortbay.jetty.servlet.ServletHandler.handle(ServletHandler.java:360)\nat org.mortbay.jetty.security.SecurityHandler.handle(SecurityHandler.java:216)\nat org.mortbay.jetty.servlet.SessionHandler.handle(SessionHandler.java:181)\nat org.mortbay.jetty.handler.ContextHandler.handle(ContextHandler.java:726)\nat org.mortbay.jetty.webapp.WebAppContext.handle(WebAppContext.java:405)\nat org.mortbay.jetty.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:206)\nat org.mortbay.jetty.handler.HandlerCollection.handle(HandlerCollection.java:114)\nat org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)\nat org.mortbay.jetty.Server.handle(Server.java:324)\nat org.mortbay.jetty.HttpConnection.handleRequest(HttpConnection.java:505)\nat org.mortbay.jetty.HttpConnection$RequestHandler.content(HttpConnection.java:842)\nat org.mortbay.jetty.HttpParser.parseNext(HttpParser.java:648)\nat org.mortbay.jetty.HttpParser.parseAvailable(HttpParser.java:211)\nat org.mortbay.jetty.HttpConnection.handle(HttpConnection.java:380)\nat org.mortbay.io.nio.SelectChannelEndPoint.run(SelectChannelEndPoint.java:395)\nat org.mortbay.thread.BoundedThreadPool$PoolThread.run(BoundedThreadPool.java:450)\n\nIf I change my convertor to ONLY create SceneTO objects (instead of an inheriting one), it works fine. The method clearly does not support inheritance\n\nWe can currently work around the problem by doing this\n\n`@RequestMapping`(value = \"/services/{serviceName}/scenes/video\", method = RequestMethod.POST)\npublic void addScene(`@PathVariable`(\"serviceName\") String serviceName,\n`@RequestBody` VideoTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)\nand\n`@RequestMapping`(value = \"/services/{serviceName}/scenes/audio\", method = RequestMethod.POST)\npublic void addScene(`@PathVariable`(\"serviceName\") String serviceName,\n`@RequestBody` AudioTO receivedSceneTO, HttpServletRequest request, HttpServletResponse response)\n\nBut it does seem less clean and results in much duplicated code.  Allowing inheritance would seem much more elegant.  (I imagine some other people may want interfaces as well)\n\n\n---\nNo further details from [SPR-6833](https://jira.spring.io/browse/SPR-6833?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453348094","453348096"], "labels":["in: web","status: invalid"]}