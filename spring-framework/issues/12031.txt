{"id":"12031", "title":"WeakReferenceMonitor has race condition [SPR-7373]", "body":"**[Mike Quilleash](https://jira.spring.io/secure/ViewProfile.jspa?name=quilleashm)** opened **[SPR-7373](https://jira.spring.io/browse/SPR-7373?redirect=false)** and commented

WeakReferenceMonitor appears to have a subtle threading issue where an item can get added but ReleaseListener never gets called.

Sequence of events

- monitor(item1, handler)
- Item1 added to trackedEntries
- isMonitoringThreadRunning() returns false
- New monitoring thread started

Some time later

Monitor thread

- Item1 becomes weakly reachable
- Monitor thread detects reference in handleQueue
- removeEntry(item1) and released
- trackedEntries.isEmpty() returns true
- while loop exits

Monitor thread pre-empted

Another thread

- monitor(item2, handler)
- Item1 added to trackedEntries
- sync
- isMonitoringThreadRunning() returns true
- release
- done

Monitor thread rescheduled

- sync
- Monitor thread sets monitoringThread to null
- release
- monitor thread quits

In this state item2 release handler will never be called when item2 becomes weakly reachable.  If another item is added then the monitor thread will be restarted and item2 will be released properly.

Subtle and unlikely to occur and less likely to persist if monitor() is called often.



---

**Affects:** 3.0 GA

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/f90125f9844146d4a36e4b6c594d832e5f21dda0
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12031","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12031/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12031/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12031/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12031","id":398106208,"node_id":"MDU6SXNzdWUzOTgxMDYyMDg=","number":12031,"title":"WeakReferenceMonitor has race condition [SPR-7373]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/71","html_url":"https://github.com/spring-projects/spring-framework/milestone/71","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/71/labels","id":3960844,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0NA==","number":71,"title":"3.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":74,"state":"closed","created_at":"2019-01-10T22:03:17Z","updated_at":"2019-01-11T02:52:15Z","due_on":"2010-08-17T07:00:00Z","closed_at":"2019-01-10T22:03:17Z"},"comments":1,"created_at":"2010-07-14T23:51:49Z","updated_at":"2012-06-19T03:44:00Z","closed_at":"2012-06-19T03:44:00Z","author_association":"COLLABORATOR","body":"**[Mike Quilleash](https://jira.spring.io/secure/ViewProfile.jspa?name=quilleashm)** opened **[SPR-7373](https://jira.spring.io/browse/SPR-7373?redirect=false)** and commented\n\nWeakReferenceMonitor appears to have a subtle threading issue where an item can get added but ReleaseListener never gets called.\n\nSequence of events\n\n- monitor(item1, handler)\n- Item1 added to trackedEntries\n- isMonitoringThreadRunning() returns false\n- New monitoring thread started\n\nSome time later\n\nMonitor thread\n\n- Item1 becomes weakly reachable\n- Monitor thread detects reference in handleQueue\n- removeEntry(item1) and released\n- trackedEntries.isEmpty() returns true\n- while loop exits\n\nMonitor thread pre-empted\n\nAnother thread\n\n- monitor(item2, handler)\n- Item1 added to trackedEntries\n- sync\n- isMonitoringThreadRunning() returns true\n- release\n- done\n\nMonitor thread rescheduled\n\n- sync\n- Monitor thread sets monitoringThread to null\n- release\n- monitor thread quits\n\nIn this state item2 release handler will never be called when item2 becomes weakly reachable.  If another item is added then the monitor thread will be restarted and item2 will be released properly.\n\nSubtle and unlikely to occur and less likely to persist if monitor() is called often.\n\n\n\n---\n\n**Affects:** 3.0 GA\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/f90125f9844146d4a36e4b6c594d832e5f21dda0\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453352228"], "labels":["in: core","type: bug"]}