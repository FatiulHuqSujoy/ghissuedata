{"id":"17516", "title":"Random NegativeArraySizeExceptions from AOP-driven getMethod call on JDK 8 [SPR-12917]", "body":"**[Greg Adams](https://jira.spring.io/secure/ViewProfile.jspa?name=gadams00)** opened **[SPR-12917](https://jira.spring.io/browse/SPR-12917?redirect=false)** and commented

We're seeing random NegativeArraySizeException from Spring AOP when running with jdk 1.8 on linux (centos).

For us these are all in stacktraces including CacheOperationSourcePointcut from the spring caching abstraction support, but I've seen a similar report on stackoverflow regarding the `@Transactional` interceptor.

Occasionally, these exceptions result in a jvm crash with core dump. Oracle support opened an openjdk bug about this here:

https://bugs.openjdk.java.net/browse/JDK-8076521

but I think it's possible this may be a Spring AOP bug instead of a JDK bug. Downgrading to JDK 1.7 makes this issue go away.  Also note that we are unable to reproduce this exception at will, since the exact same code works the vast majority of the time. We can only find these via production monitoring, where I believe it happens just due to the sheer number of requests served over a long period of time.

stacktrace

```
java.lang.Class.getDeclaredMethods0 (Native Method)

              java.lang.Class.privateGetDeclaredMethods (Class.java:2701)

              java.lang.Class.privateGetMethodRecursive (Class.java:3048)

                             java.lang.Class.getMethod0 (Class.java:3018)

                              java.lang.Class.getMethod (Class.java:1784)

….springframework.util.ClassUtils.getMostSpecificMethod (ClassUtils.java:768)

…actFallbackCacheOperationSource.computeCacheOperations (AbstractFallbackCacheOperationSource.java:135)

…bstractFallbackCacheOperationSource.getCacheOperations (AbstractFallbackCacheOperationSource.java:100)

…cache.interceptor.CacheOperationSourcePointcut.matches (CacheOperationSourcePointcut.java:39)

 org.springframework.aop.support.MethodMatchers.matches (MethodMatchers.java:94)

…ainFactory.getInterceptorsAndDynamicInterceptionAdvice (DefaultAdvisorChainFactory.java:67)

…sedSupport.getInterceptorsAndDynamicInterceptionAdvice (AdvisedSupport.java:489)

…pringframework.aop.framework.JdkDynamicAopProxy.invoke (JdkDynamicAopProxy.java:193)

        com.sun.proxy.$Proxy133.getCartShippingEstimate (Unknown Source)

 com.build.service.cart.CartServiceImpl.getLeadTimeText (CartServiceImpl.java:1926)

…m.build.service.order.OrderServiceImpl.getOrderReceipt (OrderServiceImpl.java:2157)

   com.build.service.order.OrderServiceImpl.cancelOrder (OrderServiceImpl.java:2341)
```

The DAO method being called by CartServiceImpl.getLeadTimeText actually does not have `@Cacheable` or any other caching annotations. However, one of our `@Configuration` classes has `@EnableCaching`, which I believe applies the caching interceptor to all proxies.

in case it's helpful, a core dump from one of the jvm crashes is here:
https://www.dropbox.com/s/ixnm3d7g7rneau4/core_1421.tgz?dl=0


---

**Affects:** 4.1.5

**Reference URL:** http://stackoverflow.com/questions/29311288/java-lang-negativearraysizeexception-from-spring-cache
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17516","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17516/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17516/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17516/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17516","id":398178668,"node_id":"MDU6SXNzdWUzOTgxNzg2Njg=","number":17516,"title":"Random NegativeArraySizeExceptions from AOP-driven getMethod call on JDK 8 [SPR-12917]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2015-04-15T10:18:24Z","updated_at":"2019-01-12T05:24:30Z","closed_at":"2015-05-26T09:09:36Z","author_association":"COLLABORATOR","body":"**[Greg Adams](https://jira.spring.io/secure/ViewProfile.jspa?name=gadams00)** opened **[SPR-12917](https://jira.spring.io/browse/SPR-12917?redirect=false)** and commented\n\nWe're seeing random NegativeArraySizeException from Spring AOP when running with jdk 1.8 on linux (centos).\n\nFor us these are all in stacktraces including CacheOperationSourcePointcut from the spring caching abstraction support, but I've seen a similar report on stackoverflow regarding the `@Transactional` interceptor.\n\nOccasionally, these exceptions result in a jvm crash with core dump. Oracle support opened an openjdk bug about this here:\n\nhttps://bugs.openjdk.java.net/browse/JDK-8076521\n\nbut I think it's possible this may be a Spring AOP bug instead of a JDK bug. Downgrading to JDK 1.7 makes this issue go away.  Also note that we are unable to reproduce this exception at will, since the exact same code works the vast majority of the time. We can only find these via production monitoring, where I believe it happens just due to the sheer number of requests served over a long period of time.\n\nstacktrace\n\n```\njava.lang.Class.getDeclaredMethods0 (Native Method)\n\n              java.lang.Class.privateGetDeclaredMethods (Class.java:2701)\n\n              java.lang.Class.privateGetMethodRecursive (Class.java:3048)\n\n                             java.lang.Class.getMethod0 (Class.java:3018)\n\n                              java.lang.Class.getMethod (Class.java:1784)\n\n….springframework.util.ClassUtils.getMostSpecificMethod (ClassUtils.java:768)\n\n…actFallbackCacheOperationSource.computeCacheOperations (AbstractFallbackCacheOperationSource.java:135)\n\n…bstractFallbackCacheOperationSource.getCacheOperations (AbstractFallbackCacheOperationSource.java:100)\n\n…cache.interceptor.CacheOperationSourcePointcut.matches (CacheOperationSourcePointcut.java:39)\n\n org.springframework.aop.support.MethodMatchers.matches (MethodMatchers.java:94)\n\n…ainFactory.getInterceptorsAndDynamicInterceptionAdvice (DefaultAdvisorChainFactory.java:67)\n\n…sedSupport.getInterceptorsAndDynamicInterceptionAdvice (AdvisedSupport.java:489)\n\n…pringframework.aop.framework.JdkDynamicAopProxy.invoke (JdkDynamicAopProxy.java:193)\n\n        com.sun.proxy.$Proxy133.getCartShippingEstimate (Unknown Source)\n\n com.build.service.cart.CartServiceImpl.getLeadTimeText (CartServiceImpl.java:1926)\n\n…m.build.service.order.OrderServiceImpl.getOrderReceipt (OrderServiceImpl.java:2157)\n\n   com.build.service.order.OrderServiceImpl.cancelOrder (OrderServiceImpl.java:2341)\n```\n\nThe DAO method being called by CartServiceImpl.getLeadTimeText actually does not have `@Cacheable` or any other caching annotations. However, one of our `@Configuration` classes has `@EnableCaching`, which I believe applies the caching interceptor to all proxies.\n\nin case it's helpful, a core dump from one of the jvm crashes is here:\nhttps://www.dropbox.com/s/ixnm3d7g7rneau4/core_1421.tgz?dl=0\n\n\n---\n\n**Affects:** 4.1.5\n\n**Reference URL:** http://stackoverflow.com/questions/29311288/java-lang-negativearraysizeexception-from-spring-cache\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453424683","453424684","453424685","453424687","453424688"], "labels":["in: core","status: invalid"]}