{"id":"22774", "title":"PrematureCloseException: Connection prematurely closed BEFORE when running UnitTests", "body":"Using Spring Boot 2.1.3-RELEASE and 0.8.6.RELEASE of reactor-netty i'm using webClient and using StepVerifier for testing.
All the tests are ok, but when im using DynamicTest it returns the exception.

```java
public Mono<Optional<JsonNode>> getObject(String name) {
    return webClient.get().uri(buildUri(name))
        .exchange()
        .flatMap(clientResponse -> `handleClientResponse(clientResponse));
}

private Mono<Optional<JsonNode>> handleClientResponse(ClientResponse clientResponse, String name) {
    if (clientResponse.statusCode().isError()) {
      if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
        return Mono.just(Optional.empty());
      } else {
        throw new BadGatewayException(
            format(\"Unable to retrieve external object with name: '{%s}'\",
                name));
      }
    }
    return clientResponse.bodyToMono(new ParameterizedTypeReference<Response<JsonNode>>() {
    })
        .map(r -> Optional.ofNullable(r.getData().getItem()));
  }
```

Test

```java
@TestFactory
@DisplayName(\"Get Object With Random HTTP Responses\")
public Collection<DynamicTest> getObjectWithInvalidHttpResponseTest() {

      return Stream.of(RANDOM_DATA_THEN_CLOSE, CONNECTION_RESET_BY_PEER, EMPTY_RESPONSE, MALFORMED_RESPONSE_CHUNK)
          .map(fault -> DynamicTest.dynamicTest(fault.toString(), () -> {
            stubFor(get(urlEqualTo(OBJECT_URL))
                .willReturn(aResponse()
                    .withFault(fault)
                    .withStatus(HttpStatus.OK.value())));

            StepVerifier.create(objectGateway.getObject(OBJECT_NAME))
                .expectErrorMatches(throwable -> throwable instanceof BadGatewayException &&
                    throwable.getMessage()
                        .equals(\"Unexpected response retrieving Object with name [xxxxx]\")
                ).verify();

          })).collect(Collectors.toList());
  }
```
Stack tracer

```
2019-04-09 10:46:51.121  WARN   --- [ctor-http-nio-4] r.netty.http.client.HttpClientConnect    : [id: 0xeea96051, L:0.0.0.0/0.0.0.0:63355] The connection observed an error

reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response


java.lang.AssertionError: expectation \"expectErrorMatches\" failed (predicate failed on exception: reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response)

	at reactor.test.ErrorFormatter.assertionError(ErrorFormatter.java:105)
	at reactor.test.ErrorFormatter.failPrefix(ErrorFormatter.java:94)
	at reactor.test.ErrorFormatter.fail(ErrorFormatter.java:64)
	at reactor.test.ErrorFormatter.failOptional(ErrorFormatter.java:79)
	at reactor.test.DefaultStepVerifierBuilder.lambda$expectErrorMatches$8(DefaultStepVerifierBuilder.java:399)
	at reactor.test.DefaultStepVerifierBuilder$SignalEvent.test(DefaultStepVerifierBuilder.java:2112)
	at reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onSignal(DefaultStepVerifierBuilder.java:1408)
	at reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onExpectation(DefaultStepVerifierBuilder.java:1356)
	at reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onError(DefaultStepVerifierBuilder.java:1030)
	at reactor.core.publisher.MonoFlatMap$FlatMapMain.onError(MonoFlatMap.java:165)
	at reactor.core.publisher.Operators$MultiSubscriptionSubscriber.onError(Operators.java:1747)
	at org.springframework.cloud.sleuth.instrument.web.client.TraceExchangeFilterFunction$MonoWebClientTrace$WebClientTracerSubscriber.onError(TraceWebClientBeanPostProcessor.java:276)
	at reactor.core.publisher.FluxMap$MapSubscriber.onError(FluxMap.java:126)
	at reactor.core.publisher.FluxPeek$PeekSubscriber.onError(FluxPeek.java:214)
	at reactor.core.publisher.FluxPeek$PeekSubscriber.onError(FluxPeek.java:214)
	at reactor.core.publisher.MonoNext$NextSubscriber.onError(MonoNext.java:87)
	at reactor.core.publisher.MonoFlatMapMany$FlatMapManyMain.onError(MonoFlatMapMany.java:193)
	at reactor.core.publisher.FluxRetryPredicate$RetryPredicateSubscriber.onError(FluxRetryPredicate.java:100)
	at org.springframework.cloud.sleuth.instrument.reactor.ScopePassingSpanSubscriber.onError(ScopePassingSpanSubscriber.java:105)
	at reactor.core.publisher.MonoCreate$DefaultMonoSink.error(MonoCreate.java:167)
	at reactor.netty.http.client.HttpClientConnect$HttpObserver.onUncaughtException(HttpClientConnect.java:404)
	at reactor.netty.ReactorNetty$CompositeConnectionObserver.onUncaughtException(ReactorNetty.java:351)
	at reactor.netty.resources.PooledConnectionProvider$DisposableAcquire.onUncaughtException(PooledConnectionProvider.java:503)
	at reactor.netty.resources.PooledConnectionProvider$PooledConnection.onUncaughtException(PooledConnectionProvider.java:412)
	at reactor.netty.http.client.HttpClientOperations.onInboundClose(HttpClientOperations.java:254)
	at reactor.netty.channel.ChannelOperationsHandler.channelInactive(ChannelOperationsHandler.java:121)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)
	at io.netty.channel.ChannelInboundHandlerAdapter.channelInactive(ChannelInboundHandlerAdapter.java:75)
	at io.netty.handler.timeout.IdleStateHandler.channelInactive(IdleStateHandler.java:277)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)
	at io.netty.channel.ChannelInboundHandlerAdapter.channelInactive(ChannelInboundHandlerAdapter.java:75)
	at io.netty.handler.codec.http.HttpContentDecoder.channelInactive(HttpContentDecoder.java:206)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)
	at io.netty.channel.CombinedChannelDuplexHandler$DelegatingChannelHandlerContext.fireChannelInactive(CombinedChannelDuplexHandler.java:420)
	at io.netty.handler.codec.ByteToMessageDecoder.channelInputClosed(ByteToMessageDecoder.java:390)
	at io.netty.handler.codec.ByteToMessageDecoder.channelInactive(ByteToMessageDecoder.java:355)
	at io.netty.handler.codec.http.HttpClientCodec$Decoder.channelInactive(HttpClientCodec.java:282)
	at io.netty.channel.CombinedChannelDuplexHandler.channelInactive(CombinedChannelDuplexHandler.java:223)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)
	at io.netty.channel.DefaultChannelPipeline$HeadContext.channelInactive(DefaultChannelPipeline.java:1403)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)
	at io.netty.channel.DefaultChannelPipeline.fireChannelInactive(DefaultChannelPipeline.java:912)
	at io.netty.channel.AbstractChannel$AbstractUnsafe$8.run(AbstractChannel.java:826)
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java:163)
	at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404)
	at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:495)
	at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:905)
	at java.base/java.lang.Thread.run(Thread.java:834)
	Suppressed: reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response
```

Thanks in advance!
---
<!--
Thanks for taking the time to create an issue. Please read the following:

- Questions should be asked on Stack Overflow.
- For bugs, specify affected versions and explain what you are trying to do.
- For enhancements, provide context and describe the problem.

Issue or Pull Request? Create only one, not both. GitHub treats them as the same.
If unsure, start with an issue, and if you submit a pull request later, the
issue will be closed as superseded.
-->", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22774","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22774/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22774/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22774/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22774","id":430872667,"node_id":"MDU6SXNzdWU0MzA4NzI2Njc=","number":22774,"title":"PrematureCloseException: Connection prematurely closed BEFORE when running UnitTests","user":{"login":"xDouglasx","id":11204794,"node_id":"MDQ6VXNlcjExMjA0Nzk0","avatar_url":"https://avatars2.githubusercontent.com/u/11204794?v=4","gravatar_id":"","url":"https://api.github.com/users/xDouglasx","html_url":"https://github.com/xDouglasx","followers_url":"https://api.github.com/users/xDouglasx/followers","following_url":"https://api.github.com/users/xDouglasx/following{/other_user}","gists_url":"https://api.github.com/users/xDouglasx/gists{/gist_id}","starred_url":"https://api.github.com/users/xDouglasx/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/xDouglasx/subscriptions","organizations_url":"https://api.github.com/users/xDouglasx/orgs","repos_url":"https://api.github.com/users/xDouglasx/repos","events_url":"https://api.github.com/users/xDouglasx/events{/privacy}","received_events_url":"https://api.github.com/users/xDouglasx/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-04-09T09:48:24Z","updated_at":"2019-05-08T13:09:10Z","closed_at":"2019-05-08T13:08:55Z","author_association":"NONE","body":"Using Spring Boot 2.1.3-RELEASE and 0.8.6.RELEASE of reactor-netty i'm using webClient and using StepVerifier for testing.\r\nAll the tests are ok, but when im using DynamicTest it returns the exception.\r\n\r\n```java\r\npublic Mono<Optional<JsonNode>> getObject(String name) {\r\n    return webClient.get().uri(buildUri(name))\r\n        .exchange()\r\n        .flatMap(clientResponse -> `handleClientResponse(clientResponse));\r\n}\r\n\r\nprivate Mono<Optional<JsonNode>> handleClientResponse(ClientResponse clientResponse, String name) {\r\n    if (clientResponse.statusCode().isError()) {\r\n      if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {\r\n        return Mono.just(Optional.empty());\r\n      } else {\r\n        throw new BadGatewayException(\r\n            format(\"Unable to retrieve external object with name: '{%s}'\",\r\n                name));\r\n      }\r\n    }\r\n    return clientResponse.bodyToMono(new ParameterizedTypeReference<Response<JsonNode>>() {\r\n    })\r\n        .map(r -> Optional.ofNullable(r.getData().getItem()));\r\n  }\r\n```\r\n\r\nTest\r\n\r\n```java\r\n@TestFactory\r\n@DisplayName(\"Get Object With Random HTTP Responses\")\r\npublic Collection<DynamicTest> getObjectWithInvalidHttpResponseTest() {\r\n\r\n      return Stream.of(RANDOM_DATA_THEN_CLOSE, CONNECTION_RESET_BY_PEER, EMPTY_RESPONSE, MALFORMED_RESPONSE_CHUNK)\r\n          .map(fault -> DynamicTest.dynamicTest(fault.toString(), () -> {\r\n            stubFor(get(urlEqualTo(OBJECT_URL))\r\n                .willReturn(aResponse()\r\n                    .withFault(fault)\r\n                    .withStatus(HttpStatus.OK.value())));\r\n\r\n            StepVerifier.create(objectGateway.getObject(OBJECT_NAME))\r\n                .expectErrorMatches(throwable -> throwable instanceof BadGatewayException &&\r\n                    throwable.getMessage()\r\n                        .equals(\"Unexpected response retrieving Object with name [xxxxx]\")\r\n                ).verify();\r\n\r\n          })).collect(Collectors.toList());\r\n  }\r\n```\r\nStack tracer\r\n\r\n```\r\n2019-04-09 10:46:51.121  WARN   --- [ctor-http-nio-4] r.netty.http.client.HttpClientConnect    : [id: 0xeea96051, L:0.0.0.0/0.0.0.0:63355] The connection observed an error\r\n\r\nreactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response\r\n\r\n\r\njava.lang.AssertionError: expectation \"expectErrorMatches\" failed (predicate failed on exception: reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response)\r\n\r\n\tat reactor.test.ErrorFormatter.assertionError(ErrorFormatter.java:105)\r\n\tat reactor.test.ErrorFormatter.failPrefix(ErrorFormatter.java:94)\r\n\tat reactor.test.ErrorFormatter.fail(ErrorFormatter.java:64)\r\n\tat reactor.test.ErrorFormatter.failOptional(ErrorFormatter.java:79)\r\n\tat reactor.test.DefaultStepVerifierBuilder.lambda$expectErrorMatches$8(DefaultStepVerifierBuilder.java:399)\r\n\tat reactor.test.DefaultStepVerifierBuilder$SignalEvent.test(DefaultStepVerifierBuilder.java:2112)\r\n\tat reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onSignal(DefaultStepVerifierBuilder.java:1408)\r\n\tat reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onExpectation(DefaultStepVerifierBuilder.java:1356)\r\n\tat reactor.test.DefaultStepVerifierBuilder$DefaultVerifySubscriber.onError(DefaultStepVerifierBuilder.java:1030)\r\n\tat reactor.core.publisher.MonoFlatMap$FlatMapMain.onError(MonoFlatMap.java:165)\r\n\tat reactor.core.publisher.Operators$MultiSubscriptionSubscriber.onError(Operators.java:1747)\r\n\tat org.springframework.cloud.sleuth.instrument.web.client.TraceExchangeFilterFunction$MonoWebClientTrace$WebClientTracerSubscriber.onError(TraceWebClientBeanPostProcessor.java:276)\r\n\tat reactor.core.publisher.FluxMap$MapSubscriber.onError(FluxMap.java:126)\r\n\tat reactor.core.publisher.FluxPeek$PeekSubscriber.onError(FluxPeek.java:214)\r\n\tat reactor.core.publisher.FluxPeek$PeekSubscriber.onError(FluxPeek.java:214)\r\n\tat reactor.core.publisher.MonoNext$NextSubscriber.onError(MonoNext.java:87)\r\n\tat reactor.core.publisher.MonoFlatMapMany$FlatMapManyMain.onError(MonoFlatMapMany.java:193)\r\n\tat reactor.core.publisher.FluxRetryPredicate$RetryPredicateSubscriber.onError(FluxRetryPredicate.java:100)\r\n\tat org.springframework.cloud.sleuth.instrument.reactor.ScopePassingSpanSubscriber.onError(ScopePassingSpanSubscriber.java:105)\r\n\tat reactor.core.publisher.MonoCreate$DefaultMonoSink.error(MonoCreate.java:167)\r\n\tat reactor.netty.http.client.HttpClientConnect$HttpObserver.onUncaughtException(HttpClientConnect.java:404)\r\n\tat reactor.netty.ReactorNetty$CompositeConnectionObserver.onUncaughtException(ReactorNetty.java:351)\r\n\tat reactor.netty.resources.PooledConnectionProvider$DisposableAcquire.onUncaughtException(PooledConnectionProvider.java:503)\r\n\tat reactor.netty.resources.PooledConnectionProvider$PooledConnection.onUncaughtException(PooledConnectionProvider.java:412)\r\n\tat reactor.netty.http.client.HttpClientOperations.onInboundClose(HttpClientOperations.java:254)\r\n\tat reactor.netty.channel.ChannelOperationsHandler.channelInactive(ChannelOperationsHandler.java:121)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)\r\n\tat io.netty.channel.ChannelInboundHandlerAdapter.channelInactive(ChannelInboundHandlerAdapter.java:75)\r\n\tat io.netty.handler.timeout.IdleStateHandler.channelInactive(IdleStateHandler.java:277)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)\r\n\tat io.netty.channel.ChannelInboundHandlerAdapter.channelInactive(ChannelInboundHandlerAdapter.java:75)\r\n\tat io.netty.handler.codec.http.HttpContentDecoder.channelInactive(HttpContentDecoder.java:206)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)\r\n\tat io.netty.channel.CombinedChannelDuplexHandler$DelegatingChannelHandlerContext.fireChannelInactive(CombinedChannelDuplexHandler.java:420)\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.channelInputClosed(ByteToMessageDecoder.java:390)\r\n\tat io.netty.handler.codec.ByteToMessageDecoder.channelInactive(ByteToMessageDecoder.java:355)\r\n\tat io.netty.handler.codec.http.HttpClientCodec$Decoder.channelInactive(HttpClientCodec.java:282)\r\n\tat io.netty.channel.CombinedChannelDuplexHandler.channelInactive(CombinedChannelDuplexHandler.java:223)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:224)\r\n\tat io.netty.channel.DefaultChannelPipeline$HeadContext.channelInactive(DefaultChannelPipeline.java:1403)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:245)\r\n\tat io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:231)\r\n\tat io.netty.channel.DefaultChannelPipeline.fireChannelInactive(DefaultChannelPipeline.java:912)\r\n\tat io.netty.channel.AbstractChannel$AbstractUnsafe$8.run(AbstractChannel.java:826)\r\n\tat io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java:163)\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:404)\r\n\tat io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:495)\r\n\tat io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:905)\r\n\tat java.base/java.lang.Thread.run(Thread.java:834)\r\n\tSuppressed: reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response\r\n```\r\n\r\nThanks in advance!\r\n---\r\n<!--\r\nThanks for taking the time to create an issue. Please read the following:\r\n\r\n- Questions should be asked on Stack Overflow.\r\n- For bugs, specify affected versions and explain what you are trying to do.\r\n- For enhancements, provide context and describe the problem.\r\n\r\nIssue or Pull Request? Create only one, not both. GitHub treats them as the same.\r\nIf unsure, start with an issue, and if you submit a pull request later, the\r\nissue will be closed as superseded.\r\n-->","closed_by":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}}", "commentIds":["481194546","490478898"], "labels":["status: invalid"]}