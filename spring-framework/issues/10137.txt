{"id":"10137", "title":"throws clause ignored by pointcut parser [SPR-5464]", "body":"**[Robb Samuell](https://jira.spring.io/secure/ViewProfile.jspa?name=rsamuell)** opened **[SPR-5464](https://jira.spring.io/browse/SPR-5464?redirect=false)** and commented

I have a AxisFaultRetryInteceptor that is designed to interecept methods that throw RemoteExceptions.  If a thrown RemoteException is an AxisFault, a retry is attempted up to 2 times.

My problem is that the <aop:pointcut> expression is not weaving the advice into joinpoints that have throws clause eventhough a throws clause is specified in the pointcut.

If I remove the throws clause from the pointcut expression, it successfully weaves into method signatures without throws clauses, but I need support for the throws clause in order to make this AxisFault interceptor work.

I have included the Java source code and applicationContext.xml fragment below.

****AxisFaultRetryInterceptor.java****

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.axis2.AxisFault;

public class AxisFaultRetryInterceptor implements MethodInterceptor {
private static final int DEFAULT_MAX_RETRIES = 2;

       private int maxRetries = DEFAULT_MAX_RETRIES;
    
       public void setMaxRetries(int maxRetries) {
          this.maxRetries = maxRetries;
       }
       
    	/**
    	 * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
    	 */
       public Object invoke(MethodInvocation invocation) throws Throwable {
    	      int numAttempts = 0;
    	      AxisFault af;
    	      do {
    	         numAttempts++;
    	         try { 
    	            return invocation.proceed();
    	         }
    	         catch(AxisFault ex) {
    	            af = ex;
    	         }
    	      }
    	      while(numAttempts <= this.maxRetries);
    	      throw af;
       }

}

****applicationContext.xml****

    <!-- AOP Interceptors -->
    <bean id=\"axisFaultRetryAdvice\" class=\"com.mycompany.interceptors.AxisFaultRetryInterceptor\"/>
    
    <!-- AOP Config -->
    <aop:config>
        <aop:pointcut id=\"axisFaultRetryPointcut\" expression=\"execution(* *..SomeAxisStub.*(..) throws *..*RemoteException*)\"/>
        <aop:advisor id=\"axisFaultRetryAdvisor\" advice-ref=\"axisFaultRetryAdvice\" pointcut-ref=\"axisFaultRetryPointcut\"/>
    </aop:config>



---

**Affects:** 2.5.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10137","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10137/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10137/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10137/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10137","id":398093101,"node_id":"MDU6SXNzdWUzOTgwOTMxMDE=","number":10137,"title":"throws clause ignored by pointcut parser [SPR-5464]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2009-02-04T23:44:50Z","updated_at":"2019-01-12T16:27:20Z","closed_at":"2018-12-26T17:28:16Z","author_association":"COLLABORATOR","body":"**[Robb Samuell](https://jira.spring.io/secure/ViewProfile.jspa?name=rsamuell)** opened **[SPR-5464](https://jira.spring.io/browse/SPR-5464?redirect=false)** and commented\n\nI have a AxisFaultRetryInteceptor that is designed to interecept methods that throw RemoteExceptions.  If a thrown RemoteException is an AxisFault, a retry is attempted up to 2 times.\n\nMy problem is that the <aop:pointcut> expression is not weaving the advice into joinpoints that have throws clause eventhough a throws clause is specified in the pointcut.\n\nIf I remove the throws clause from the pointcut expression, it successfully weaves into method signatures without throws clauses, but I need support for the throws clause in order to make this AxisFault interceptor work.\n\nI have included the Java source code and applicationContext.xml fragment below.\n\n****AxisFaultRetryInterceptor.java****\n\nimport org.aopalliance.intercept.MethodInterceptor;\nimport org.aopalliance.intercept.MethodInvocation;\nimport org.apache.axis2.AxisFault;\n\npublic class AxisFaultRetryInterceptor implements MethodInterceptor {\nprivate static final int DEFAULT_MAX_RETRIES = 2;\n\n       private int maxRetries = DEFAULT_MAX_RETRIES;\n    \n       public void setMaxRetries(int maxRetries) {\n          this.maxRetries = maxRetries;\n       }\n       \n    \t/**\n    \t * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)\n    \t */\n       public Object invoke(MethodInvocation invocation) throws Throwable {\n    \t      int numAttempts = 0;\n    \t      AxisFault af;\n    \t      do {\n    \t         numAttempts++;\n    \t         try { \n    \t            return invocation.proceed();\n    \t         }\n    \t         catch(AxisFault ex) {\n    \t            af = ex;\n    \t         }\n    \t      }\n    \t      while(numAttempts <= this.maxRetries);\n    \t      throw af;\n       }\n\n}\n\n****applicationContext.xml****\n\n    <!-- AOP Interceptors -->\n    <bean id=\"axisFaultRetryAdvice\" class=\"com.mycompany.interceptors.AxisFaultRetryInterceptor\"/>\n    \n    <!-- AOP Config -->\n    <aop:config>\n        <aop:pointcut id=\"axisFaultRetryPointcut\" expression=\"execution(* *..SomeAxisStub.*(..) throws *..*RemoteException*)\"/>\n        <aop:advisor id=\"axisFaultRetryAdvisor\" advice-ref=\"axisFaultRetryAdvice\" pointcut-ref=\"axisFaultRetryPointcut\"/>\n    </aop:config>\n\n\n\n---\n\n**Affects:** 2.5.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453336392"], "labels":["in: core","status: declined"]}