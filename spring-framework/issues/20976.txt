{"id":"20976", "title":"MockMvc error handling with CompletableFuture regression in 5.0.1+ [SPR-16430]", "body":"**[Matt Newman](https://jira.spring.io/secure/ViewProfile.jspa?name=mdjnewman)** opened **[SPR-16430](https://jira.spring.io/browse/SPR-16430?redirect=false)** and commented

I originally reported this on the Spring Boot project: https://github.com/spring-projects/spring-boot/issues/11345

---

With 5.0.0.RELEASE, the following controller:

```java
@SpringBootApplication
@RestController
class DemoApplication {
    @RequestMapping(\"/sad\", method = arrayOf(RequestMethod.POST))
    fun getErroringCompletableFuture(): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            throw RuntimeException()
        }
    }
}
```

coupled with the following advice:

```java
@ControllerAdvice
class ErrorHandlingAdvice {
    @ExceptionHandler(Exception::class)
    fun handleException(exception: Exception): ResponseEntity<*> {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(\"Handled by ${this.javaClass.simpleName}\")
    }
}
```

passes the following test:

```java
@Test
fun errorIsHandled() {
    val mvcResult = mockMvc
        .perform(post(\"/sad\"))
        .andExpect(request().asyncStarted())
        .andReturn()

    mockMvc
        .perform(asyncDispatch(mvcResult))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(content().string(\"Handled by ErrorHandlingAdvice\"))
        .andExpect(status().is5xxServerError)
}
```

In 5.0.1 & above, the status is set to 200 rather than 500.

A project reproducing this issue can be found at https://github.com/mdjnewman/studious-funicular. The test passes in the master branch (using Boot 2.0.0.M5 & Spring Framework 5.0.0). There is a `spring-5.0.1.RELEASE` branch which fails, the only change being the addition of

```java
ext['spring.version'] = '5.0.1.RELEASE'
```

to the build.gradle.

---

**Affects:** 5.0.1, 5.0.2, 5.0.3

**Issue Links:**
- #20616 MockMvc async does not re-use response instance on async dispatch

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/6d909b013e17e18da13f00213254a784c19408e4

0 votes, 6 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20976","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20976/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20976/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20976/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20976","id":398221980,"node_id":"MDU6SXNzdWUzOTgyMjE5ODA=","number":20976,"title":"MockMvc error handling with CompletableFuture regression in 5.0.1+ [SPR-16430]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/185","html_url":"https://github.com/spring-projects/spring-framework/milestone/185","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/185/labels","id":3960958,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1OA==","number":185,"title":"5.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":58,"state":"closed","created_at":"2019-01-10T22:05:33Z","updated_at":"2019-01-11T10:19:35Z","due_on":"2018-02-18T08:00:00Z","closed_at":"2019-01-10T22:05:33Z"},"comments":3,"created_at":"2018-01-29T12:29:25Z","updated_at":"2019-01-13T04:55:18Z","closed_at":"2018-02-19T12:19:21Z","author_association":"COLLABORATOR","body":"**[Matt Newman](https://jira.spring.io/secure/ViewProfile.jspa?name=mdjnewman)** opened **[SPR-16430](https://jira.spring.io/browse/SPR-16430?redirect=false)** and commented\n\nI originally reported this on the Spring Boot project: https://github.com/spring-projects/spring-boot/issues/11345\n\n---\n\nWith 5.0.0.RELEASE, the following controller:\n\n```java\n@SpringBootApplication\n@RestController\nclass DemoApplication {\n    @RequestMapping(\"/sad\", method = arrayOf(RequestMethod.POST))\n    fun getErroringCompletableFuture(): CompletableFuture<String> {\n        return CompletableFuture.supplyAsync {\n            throw RuntimeException()\n        }\n    }\n}\n```\n\ncoupled with the following advice:\n\n```java\n@ControllerAdvice\nclass ErrorHandlingAdvice {\n    @ExceptionHandler(Exception::class)\n    fun handleException(exception: Exception): ResponseEntity<*> {\n        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(\"Handled by ${this.javaClass.simpleName}\")\n    }\n}\n```\n\npasses the following test:\n\n```java\n@Test\nfun errorIsHandled() {\n    val mvcResult = mockMvc\n        .perform(post(\"/sad\"))\n        .andExpect(request().asyncStarted())\n        .andReturn()\n\n    mockMvc\n        .perform(asyncDispatch(mvcResult))\n        .andDo(MockMvcResultHandlers.print())\n        .andExpect(content().string(\"Handled by ErrorHandlingAdvice\"))\n        .andExpect(status().is5xxServerError)\n}\n```\n\nIn 5.0.1 & above, the status is set to 200 rather than 500.\n\nA project reproducing this issue can be found at https://github.com/mdjnewman/studious-funicular. The test passes in the master branch (using Boot 2.0.0.M5 & Spring Framework 5.0.0). There is a `spring-5.0.1.RELEASE` branch which fails, the only change being the addition of\n\n```java\next['spring.version'] = '5.0.1.RELEASE'\n```\n\nto the build.gradle.\n\n---\n\n**Affects:** 5.0.1, 5.0.2, 5.0.3\n\n**Issue Links:**\n- #20616 MockMvc async does not re-use response instance on async dispatch\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/6d909b013e17e18da13f00213254a784c19408e4\n\n0 votes, 6 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453466684","453466686","453466688"], "labels":["in: test","in: web","type: enhancement"]}