{"id":"20619", "title":"DataBufferUtils.read(ReadableByteChannel,...) may corrupt data when used with NettyDataBuffers [SPR-16070]", "body":"**[Oleg Alexeyev](https://jira.spring.io/secure/ViewProfile.jspa?name=blacklion)** opened **[SPR-16070](https://jira.spring.io/browse/SPR-16070?redirect=false)** and commented

If published buffer is not immediately consumed, `read` may corrupt the data because the same `ByteBuffer` is reused and `NettyDataBuffer.write(ByteBuffer)` just wraps it into a `ByteBuf` and adds to a `CompositeByteBuf`, effectively reusing the same byte array.

Here is a test reproducing the problem\"

```
public class DataBufferUtilsTest extends FTestBase {

  private final ByteBufAllocator allocator = DEFAULT;
  private final DataBuffer dataBuffer = bufferFactory.allocateBuffer();
  private final DataBufferFactory bufferFactory = new NettyDataBufferFactory(allocator);
  @Mock
  private ReadableByteChannel channel;

  @Test
  public void read_shouldPublishData() throws Exception {
    when(channel.read(any()))
        .thenAnswer(putByte(1))
        .thenAnswer(putByte(2))
        .thenAnswer(putByte(3))
        .thenReturn(-1);

    Flux<DataBuffer> read = read(channel, bufferFactory, 1);

    StepVerifier.create(
        read.reduce(DataBuffer::write)
            .map(this::dataBufferToBytes)
            .map(this::encodeHexString)
    )
        .expectNext(\"010203\")
        .verifyComplete();
  }

  private Answer<Integer> putByte(int b) {
    return invocation -> {
      invocation.getArgumentAt(0, ByteBuffer.class).put((byte) b);
      return 1;
    };
  }

  private byte[] dataBufferToBytes(DataBuffer buffer) {
    try {
      int byteCount = buffer.readableByteCount();
      byte[] bytes = new byte[byteCount];
      buffer.read(bytes);
      return bytes;
    } finally {
      release(buffer);
    }
  }

  private String encodeHexString(byte[] data) {
    StringBuilder builder = new StringBuilder();
    for (byte b : data) {
      builder.append((0xF0 & b) >>> 4);
      builder.append(0x0F & b);
    }
    return builder.toString();
  }
```

Output:

```
java.lang.AssertionError: expectation \"expectNext(010203)\" failed (expected value: 010203; actual value: 030303)
```

Another problem with this method is that reusing the same `ByteBuffer` for reads means unnecessary copy of data is required (it does happen with `DefaultDataBuffer` and doesn't with `NettyDataBuffer` - hence, the corruption), what seems to be contrary to the idea of more efficient hardware usage in reactive programs.

For now we're interested in Netty only, so we fixed it in the following way:

```
public static Flux<DataBuffer> read(ReadableByteChannel channel) {
         return Flux.generate(
      () -> channel,
      (ch, sink) -> {
        boolean release = true;
        NettyDataBuffer dataBuffer = DEFAULT_DATA_BUFFER_FACTORY.allocateBuffer(BUFFER_SIZE);
        try {
          int read;
          ByteBuf byteBuf = dataBuffer.getNativeBuffer();
          // Cannot use asByteBuffer() as it returns one with zero capacity: #20617
          ByteBuffer byteBuffer = byteBuf.nioBuffer(0, byteBuf.capacity());
          if ((read = ch.read(byteBuffer)) >= 0) {
            byteBuf.writerIndex(read);
            release = false;
            sink.next(dataBuffer);
          } else {
            sink.complete();
          }
        } catch (Throwable ex) {
          sink.error(ex);
        } finally {
          if (release) {
            release(dataBuffer);
          }
        }
        return channel;
      },
      IOUtils::closeQuietly
  );
```

This way to fix both the corruption and get rid of unnecessary copy.

Unfortunately, the idiom doesn't work well for `DefaultDataBuffer`. For it, with current API, perhaps `ByteArray` could be allocated, then data is read into it and then `DefaultDataBufferFactory.wrap(ByteBuffer)` would properly set `read/writePosition`.

---

**Affects:** 5.0 GA

**Issue Links:**
- #20617 Empty NettyByteBuffer.asByteBuffer() returns ByteBuffer with zero capacity

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/c7a15260d631371481519098432795e8808ec9bc

1 votes, 3 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20619","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20619/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20619/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20619/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20619","id":398216794,"node_id":"MDU6SXNzdWUzOTgyMTY3OTQ=","number":20619,"title":"DataBufferUtils.read(ReadableByteChannel,...) may corrupt data when used with NettyDataBuffers [SPR-16070]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/182","html_url":"https://github.com/spring-projects/spring-framework/milestone/182","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/182/labels","id":3960955,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1NQ==","number":182,"title":"5.0.1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":49,"state":"closed","created_at":"2019-01-10T22:05:30Z","updated_at":"2019-01-11T10:05:18Z","due_on":"2017-10-23T07:00:00Z","closed_at":"2019-01-10T22:05:30Z"},"comments":1,"created_at":"2017-10-13T17:09:55Z","updated_at":"2019-01-14T04:33:44Z","closed_at":"2017-10-24T16:24:06Z","author_association":"COLLABORATOR","body":"**[Oleg Alexeyev](https://jira.spring.io/secure/ViewProfile.jspa?name=blacklion)** opened **[SPR-16070](https://jira.spring.io/browse/SPR-16070?redirect=false)** and commented\n\nIf published buffer is not immediately consumed, `read` may corrupt the data because the same `ByteBuffer` is reused and `NettyDataBuffer.write(ByteBuffer)` just wraps it into a `ByteBuf` and adds to a `CompositeByteBuf`, effectively reusing the same byte array.\n\nHere is a test reproducing the problem\"\n\n```\npublic class DataBufferUtilsTest extends FTestBase {\n\n  private final ByteBufAllocator allocator = DEFAULT;\n  private final DataBuffer dataBuffer = bufferFactory.allocateBuffer();\n  private final DataBufferFactory bufferFactory = new NettyDataBufferFactory(allocator);\n  @Mock\n  private ReadableByteChannel channel;\n\n  @Test\n  public void read_shouldPublishData() throws Exception {\n    when(channel.read(any()))\n        .thenAnswer(putByte(1))\n        .thenAnswer(putByte(2))\n        .thenAnswer(putByte(3))\n        .thenReturn(-1);\n\n    Flux<DataBuffer> read = read(channel, bufferFactory, 1);\n\n    StepVerifier.create(\n        read.reduce(DataBuffer::write)\n            .map(this::dataBufferToBytes)\n            .map(this::encodeHexString)\n    )\n        .expectNext(\"010203\")\n        .verifyComplete();\n  }\n\n  private Answer<Integer> putByte(int b) {\n    return invocation -> {\n      invocation.getArgumentAt(0, ByteBuffer.class).put((byte) b);\n      return 1;\n    };\n  }\n\n  private byte[] dataBufferToBytes(DataBuffer buffer) {\n    try {\n      int byteCount = buffer.readableByteCount();\n      byte[] bytes = new byte[byteCount];\n      buffer.read(bytes);\n      return bytes;\n    } finally {\n      release(buffer);\n    }\n  }\n\n  private String encodeHexString(byte[] data) {\n    StringBuilder builder = new StringBuilder();\n    for (byte b : data) {\n      builder.append((0xF0 & b) >>> 4);\n      builder.append(0x0F & b);\n    }\n    return builder.toString();\n  }\n```\n\nOutput:\n\n```\njava.lang.AssertionError: expectation \"expectNext(010203)\" failed (expected value: 010203; actual value: 030303)\n```\n\nAnother problem with this method is that reusing the same `ByteBuffer` for reads means unnecessary copy of data is required (it does happen with `DefaultDataBuffer` and doesn't with `NettyDataBuffer` - hence, the corruption), what seems to be contrary to the idea of more efficient hardware usage in reactive programs.\n\nFor now we're interested in Netty only, so we fixed it in the following way:\n\n```\npublic static Flux<DataBuffer> read(ReadableByteChannel channel) {\n         return Flux.generate(\n      () -> channel,\n      (ch, sink) -> {\n        boolean release = true;\n        NettyDataBuffer dataBuffer = DEFAULT_DATA_BUFFER_FACTORY.allocateBuffer(BUFFER_SIZE);\n        try {\n          int read;\n          ByteBuf byteBuf = dataBuffer.getNativeBuffer();\n          // Cannot use asByteBuffer() as it returns one with zero capacity: #20617\n          ByteBuffer byteBuffer = byteBuf.nioBuffer(0, byteBuf.capacity());\n          if ((read = ch.read(byteBuffer)) >= 0) {\n            byteBuf.writerIndex(read);\n            release = false;\n            sink.next(dataBuffer);\n          } else {\n            sink.complete();\n          }\n        } catch (Throwable ex) {\n          sink.error(ex);\n        } finally {\n          if (release) {\n            release(dataBuffer);\n          }\n        }\n        return channel;\n      },\n      IOUtils::closeQuietly\n  );\n```\n\nThis way to fix both the corruption and get rid of unnecessary copy.\n\nUnfortunately, the idiom doesn't work well for `DefaultDataBuffer`. For it, with current API, perhaps `ByteArray` could be allocated, then data is read into it and then `DefaultDataBufferFactory.wrap(ByteBuffer)` would properly set `read/writePosition`.\n\n---\n\n**Affects:** 5.0 GA\n\n**Issue Links:**\n- #20617 Empty NettyByteBuffer.asByteBuffer() returns ByteBuffer with zero capacity\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/c7a15260d631371481519098432795e8808ec9bc\n\n1 votes, 3 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453462134"], "labels":["in: core","type: bug"]}