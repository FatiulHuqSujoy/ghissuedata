{"id":"8334", "title":"Create annotation to group tests [SPR-3653]", "body":"**[Magnus Heino](https://jira.spring.io/secure/ViewProfile.jspa?name=magnus)** opened **[SPR-3653](https://jira.spring.io/browse/SPR-3653?redirect=false)** and commented

The only thing I miss comparing in JUnit comparing with TestNG is the ability to group tests using annotations.

org.springframework.test.ConditionalTestCase makes it possible for subclasses to override  isDisabledInThisEnvironment(String testMethodName) and in that method, looking at the test method name, decide if the test should run or not.

Add a annotation like this:

```
package org.springframework.test;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value=ElementType.METHOD)
@Retention(value=RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Environment {
	
	String[] value();
}
```

Add this method to ConditionalTestCase

```
/**
 * Should this test run?
 * @param environment name of the test method environment
 * @return whether the test should execute in the provided envionment
 */
protected boolean isDisabledInEnvironment(String environment) {
	return false;
}
```

and change ConditionalTestCase.runBase to this:

```
public void runBare() throws Throwable {
	Method runMethod = getClass().getMethod(getName(), (Class[])null); 
	
	boolean disabledInEnvironment = false;
	
	if(runMethod.isAnnotationPresent(Environment.class)) {
		String[] environments = runMethod.getAnnotation(Environment.class).value();
		
		for (int i = 0; i < environments.length; i++) {
			if(isDisabledInEnvironment(environments[i])) {
				disabledInEnvironment = true;
			}
		}
	}
	
	// getName will return the name of the method being run
	if (disabledInEnvironment || isDisabledInThisEnvironment(getName())) {
		recordDisabled();
		logger.info(\"**** \" + getClass().getName() + \".\" + getName() + \" disabled in this environment: \" +
				\"Total disabled tests=\" + getDisabledTestCount());
		return;
	}
	
	// Let JUnit handle execution
	super.runBare();
}
```

Then it's possible to annotate each test method with what environment it should run in. Applications can create own baseclasses of the spring Abstract* test classes and override isDisabledInEnvironment(String environment) . In there, a decision is made based on the environment that the method is annotated with.

Example:

```
public class AppTest extends ConditionalTestCase {

	private final Log log = LogFactory.getLog(this.getClass());
	
	@Environment({\"database\", \"slow\"})
	public void testDatabaseSlow() {
		log.debug(\"NO\");
	}

	@Environment({\"slow\"})
	public void testSlow() {
		log.debug(\"YES\");
	}
	
	@Environment({\"database\"})
	public void testDatabase() {
		log.debug(\"NO\");
	}
	
	public void testNoAnnotation() {
		log.debug(\"YES\");
	}
	
	@Environment({})
	public void testEmptyArray() {
		log.debug(\"YES\");
	}
		
	@Override
	protected boolean isDisabledInEnvironment(String environment) {
		if(environment.equalsIgnoreCase(\"database\")) {
			return true;
		}
		
		return false;		
	}	
}
```

In a real scenario the isDisabledInEnvironment is moved to a application specific testcasebaseclass that inherits ConditionalTestCase, and this example class inherits from that new testcasebaseclass instead.

/Magnus

---

**Issue Links:**
- #9538 Introduce strategy for determining if a profile value is enabled for a particular test environment
- #10572 Allow multiple values be specified in the runtime for tests filtering by `@IfProfileValue`

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8334","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8334/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8334/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8334/events","html_url":"https://github.com/spring-projects/spring-framework/issues/8334","id":398079172,"node_id":"MDU6SXNzdWUzOTgwNzkxNzI=","number":8334,"title":"Create annotation to group tests [SPR-3653]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false},"assignees":[{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/51","html_url":"https://github.com/spring-projects/spring-framework/milestone/51","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/51/labels","id":3960823,"node_id":"MDk6TWlsZXN0b25lMzk2MDgyMw==","number":51,"title":"2.5 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":74,"state":"closed","created_at":"2019-01-10T22:02:53Z","updated_at":"2019-01-11T01:16:08Z","due_on":"2007-10-21T07:00:00Z","closed_at":"2019-01-10T22:02:53Z"},"comments":8,"created_at":"2007-07-04T06:01:38Z","updated_at":"2019-01-13T08:08:00Z","closed_at":"2012-06-19T03:50:31Z","author_association":"COLLABORATOR","body":"**[Magnus Heino](https://jira.spring.io/secure/ViewProfile.jspa?name=magnus)** opened **[SPR-3653](https://jira.spring.io/browse/SPR-3653?redirect=false)** and commented\n\nThe only thing I miss comparing in JUnit comparing with TestNG is the ability to group tests using annotations.\n\norg.springframework.test.ConditionalTestCase makes it possible for subclasses to override  isDisabledInThisEnvironment(String testMethodName) and in that method, looking at the test method name, decide if the test should run or not.\n\nAdd a annotation like this:\n\n```\npackage org.springframework.test;\n\nimport java.lang.annotation.Documented;\nimport java.lang.annotation.ElementType;\nimport java.lang.annotation.Inherited;\nimport java.lang.annotation.Retention;\nimport java.lang.annotation.RetentionPolicy;\nimport java.lang.annotation.Target;\n\n@Target(value=ElementType.METHOD)\n@Retention(value=RetentionPolicy.RUNTIME)\n@Inherited\n@Documented\npublic @interface Environment {\n\t\n\tString[] value();\n}\n```\n\nAdd this method to ConditionalTestCase\n\n```\n/**\n * Should this test run?\n * @param environment name of the test method environment\n * @return whether the test should execute in the provided envionment\n */\nprotected boolean isDisabledInEnvironment(String environment) {\n\treturn false;\n}\n```\n\nand change ConditionalTestCase.runBase to this:\n\n```\npublic void runBare() throws Throwable {\n\tMethod runMethod = getClass().getMethod(getName(), (Class[])null); \n\t\n\tboolean disabledInEnvironment = false;\n\t\n\tif(runMethod.isAnnotationPresent(Environment.class)) {\n\t\tString[] environments = runMethod.getAnnotation(Environment.class).value();\n\t\t\n\t\tfor (int i = 0; i < environments.length; i++) {\n\t\t\tif(isDisabledInEnvironment(environments[i])) {\n\t\t\t\tdisabledInEnvironment = true;\n\t\t\t}\n\t\t}\n\t}\n\t\n\t// getName will return the name of the method being run\n\tif (disabledInEnvironment || isDisabledInThisEnvironment(getName())) {\n\t\trecordDisabled();\n\t\tlogger.info(\"**** \" + getClass().getName() + \".\" + getName() + \" disabled in this environment: \" +\n\t\t\t\t\"Total disabled tests=\" + getDisabledTestCount());\n\t\treturn;\n\t}\n\t\n\t// Let JUnit handle execution\n\tsuper.runBare();\n}\n```\n\nThen it's possible to annotate each test method with what environment it should run in. Applications can create own baseclasses of the spring Abstract* test classes and override isDisabledInEnvironment(String environment) . In there, a decision is made based on the environment that the method is annotated with.\n\nExample:\n\n```\npublic class AppTest extends ConditionalTestCase {\n\n\tprivate final Log log = LogFactory.getLog(this.getClass());\n\t\n\t@Environment({\"database\", \"slow\"})\n\tpublic void testDatabaseSlow() {\n\t\tlog.debug(\"NO\");\n\t}\n\n\t@Environment({\"slow\"})\n\tpublic void testSlow() {\n\t\tlog.debug(\"YES\");\n\t}\n\t\n\t@Environment({\"database\"})\n\tpublic void testDatabase() {\n\t\tlog.debug(\"NO\");\n\t}\n\t\n\tpublic void testNoAnnotation() {\n\t\tlog.debug(\"YES\");\n\t}\n\t\n\t@Environment({})\n\tpublic void testEmptyArray() {\n\t\tlog.debug(\"YES\");\n\t}\n\t\t\n\t@Override\n\tprotected boolean isDisabledInEnvironment(String environment) {\n\t\tif(environment.equalsIgnoreCase(\"database\")) {\n\t\t\treturn true;\n\t\t}\n\t\t\n\t\treturn false;\t\t\n\t}\t\n}\n```\n\nIn a real scenario the isDisabledInEnvironment is moved to a application specific testcasebaseclass that inherits ConditionalTestCase, and this example class inherits from that new testcasebaseclass instead.\n\n/Magnus\n\n---\n\n**Issue Links:**\n- #9538 Introduce strategy for determining if a profile value is enabled for a particular test environment\n- #10572 Allow multiple values be specified in the runtime for tests filtering by `@IfProfileValue`\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453319669","453319672","453319673","453319674","453319675","453319676","453319677","453319678"], "labels":["in: test","type: enhancement"]}