{"id":"11657", "title":"Cglib make it impossible to autowire a sub-class [SPR-6992]", "body":"**[Michel Zanini](https://jira.spring.io/secure/ViewProfile.jspa?name=michelz)** opened **[SPR-6992](https://jira.spring.io/browse/SPR-6992?redirect=false)** and commented

Let`s say you have a class A and this class uses the ``@Async`` annotation.
Then class B extends class A.

When you try to autowire class B like this:

public class UnitTest {

`@Autowired`
private B b;

}

It does not work. I wonder if this is expected behaviour or not.

---

**Affects:** 3.0.1

**Attachments:**
- [test.zip](https://jira.spring.io/secure/attachment/16300/test.zip) (_7.87 kB_)

**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/7477645992f05da42539140db3b149025af73ec2

1 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11657","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11657/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11657/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11657/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11657","id":398103860,"node_id":"MDU6SXNzdWUzOTgxMDM4NjA=","number":11657,"title":"Cglib make it impossible to autowire a sub-class [SPR-6992]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2010-03-16T00:58:03Z","updated_at":"2019-01-13T07:54:49Z","closed_at":"2018-12-28T09:57:39Z","author_association":"COLLABORATOR","body":"**[Michel Zanini](https://jira.spring.io/secure/ViewProfile.jspa?name=michelz)** opened **[SPR-6992](https://jira.spring.io/browse/SPR-6992?redirect=false)** and commented\n\nLet`s say you have a class A and this class uses the ``@Async`` annotation.\nThen class B extends class A.\n\nWhen you try to autowire class B like this:\n\npublic class UnitTest {\n\n`@Autowired`\nprivate B b;\n\n}\n\nIt does not work. I wonder if this is expected behaviour or not.\n\n---\n\n**Affects:** 3.0.1\n\n**Attachments:**\n- [test.zip](https://jira.spring.io/secure/attachment/16300/test.zip) (_7.87 kB_)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/7477645992f05da42539140db3b149025af73ec2\n\n1 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453349294","453349295","453349296"], "labels":["in: core"]}