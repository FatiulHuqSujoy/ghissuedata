{"id":"18221", "title":"nested exception is org.hibernate.HibernateException: Unable to locate current JTA transaction [SPR-13644]", "body":"**[Sander Theetaert](https://jira.spring.io/secure/ViewProfile.jspa?name=bamboomy@gmail.com)** opened **[SPR-13644](https://jira.spring.io/browse/SPR-13644?redirect=false)** and commented

I try to upgrade from 3.1.2.RELEASE to 4.2.2.RELEASE and from hibernate 3.0 to hibernate 4 (previously 5 but I already gave up on that)

For that I changed org.springframework.orm.hibernate3.HibernateTransactionManager to
org.springframework.orm.hibernate4.HibernateTransactionManager and org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean to org.springframework.orm.hibernate4.LocalSessionFactoryBean

relevant part of the dao-context.xml is this:

    <bean id=\"transactionManager\" name=\"transactionManager\"
          class=\"org.springframework.orm.hibernate4.HibernateTransactionManager\">
        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>
    </bean>
    
    <!-- Enable the configuration of transactional behavior based on annotations -->
    <tx:annotation-driven transaction-manager=\"transactionManager\"/>
    
    <bean id=\"sessionFactory\" class=\"org.springframework.orm.hibernate4.LocalSessionFactoryBean\">
        <property name=\"dataSource\" ref=\"datasource\"/>
        <property name=\"packagesToScan\" value=\"be.fgov.just.cjr.model\"/>
        <property name=\"hibernateProperties\">
            <props>
                <prop key=\"hibernate.bytecode.use_reflection_optimizer\">true</prop>
                <prop key=\"hibernate.dialect\">be.fgov.just.cjr.oracle.OracleDialectWithXmlTypeSupport</prop>
                <prop key=\"hibernate.search.autoregister_listeners\">false</prop>
                <prop key=\"hibernate.transaction.factory_class\">org.hibernate.engine.transaction.internal.jta.JtaTransactionFactory</prop>
                <prop key=\"hibernate.use_sql_comments\">true</prop>
                <prop key=\"hibernate.generate_statistics\">true</prop>
                <prop key=\"hibernate.cache.use_structured_entries\">true</prop>
                <prop key=\"hibernate.cache.region.factory_class\">org.hibernate.cache.ehcache.EhCacheRegionFactory</prop>
                <prop key=\"hibernate.cache.use_query_cache\">true</prop>
                <prop key=\"hibernate.cache.use_second_level_cache\">true</prop>
                <prop key=\"hibernate.show_sql\">false</prop>
                <prop key=\"hibernate.current_session_context_class\">jta</prop>
                <prop key=\"hibernate.transaction.jta.platform\">org.hibernate.service.jta.platform.internal.WeblogicJtaPlatform</prop>
            </props>
        </property>
    </bean>

I also encoutered this issue: #15230 which advises to change CMTTransactionFactory to JtaTransactionFactory;

in fact that rises an other issue that I'm trying to counter for a couple of days now...

I also have a stack overflow thread on this issue:

(with a greater portion of the stacktrace + some additional info I don't know whether to copy past here also)

http://stackoverflow.com/questions/33522102/migration-to-hibernate-4-spring-4-2-2-org-hibernate-hibernateexception-could

---

**Affects:** 4.2.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18221","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18221/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18221/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18221/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18221","id":398186194,"node_id":"MDU6SXNzdWUzOTgxODYxOTQ=","number":18221,"title":"nested exception is org.hibernate.HibernateException: Unable to locate current JTA transaction [SPR-13644]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-11-05T16:38:47Z","updated_at":"2019-01-12T05:23:30Z","closed_at":"2015-11-05T16:52:57Z","author_association":"COLLABORATOR","body":"**[Sander Theetaert](https://jira.spring.io/secure/ViewProfile.jspa?name=bamboomy@gmail.com)** opened **[SPR-13644](https://jira.spring.io/browse/SPR-13644?redirect=false)** and commented\n\nI try to upgrade from 3.1.2.RELEASE to 4.2.2.RELEASE and from hibernate 3.0 to hibernate 4 (previously 5 but I already gave up on that)\n\nFor that I changed org.springframework.orm.hibernate3.HibernateTransactionManager to\norg.springframework.orm.hibernate4.HibernateTransactionManager and org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean to org.springframework.orm.hibernate4.LocalSessionFactoryBean\n\nrelevant part of the dao-context.xml is this:\n\n    <bean id=\"transactionManager\" name=\"transactionManager\"\n          class=\"org.springframework.orm.hibernate4.HibernateTransactionManager\">\n        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>\n    </bean>\n    \n    <!-- Enable the configuration of transactional behavior based on annotations -->\n    <tx:annotation-driven transaction-manager=\"transactionManager\"/>\n    \n    <bean id=\"sessionFactory\" class=\"org.springframework.orm.hibernate4.LocalSessionFactoryBean\">\n        <property name=\"dataSource\" ref=\"datasource\"/>\n        <property name=\"packagesToScan\" value=\"be.fgov.just.cjr.model\"/>\n        <property name=\"hibernateProperties\">\n            <props>\n                <prop key=\"hibernate.bytecode.use_reflection_optimizer\">true</prop>\n                <prop key=\"hibernate.dialect\">be.fgov.just.cjr.oracle.OracleDialectWithXmlTypeSupport</prop>\n                <prop key=\"hibernate.search.autoregister_listeners\">false</prop>\n                <prop key=\"hibernate.transaction.factory_class\">org.hibernate.engine.transaction.internal.jta.JtaTransactionFactory</prop>\n                <prop key=\"hibernate.use_sql_comments\">true</prop>\n                <prop key=\"hibernate.generate_statistics\">true</prop>\n                <prop key=\"hibernate.cache.use_structured_entries\">true</prop>\n                <prop key=\"hibernate.cache.region.factory_class\">org.hibernate.cache.ehcache.EhCacheRegionFactory</prop>\n                <prop key=\"hibernate.cache.use_query_cache\">true</prop>\n                <prop key=\"hibernate.cache.use_second_level_cache\">true</prop>\n                <prop key=\"hibernate.show_sql\">false</prop>\n                <prop key=\"hibernate.current_session_context_class\">jta</prop>\n                <prop key=\"hibernate.transaction.jta.platform\">org.hibernate.service.jta.platform.internal.WeblogicJtaPlatform</prop>\n            </props>\n        </property>\n    </bean>\n\nI also encoutered this issue: #15230 which advises to change CMTTransactionFactory to JtaTransactionFactory;\n\nin fact that rises an other issue that I'm trying to counter for a couple of days now...\n\nI also have a stack overflow thread on this issue:\n\n(with a greater portion of the stacktrace + some additional info I don't know whether to copy past here also)\n\nhttp://stackoverflow.com/questions/33522102/migration-to-hibernate-4-spring-4-2-2-org-hibernate-hibernateexception-could\n\n---\n\n**Affects:** 4.2.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453432366"], "labels":["in: data","status: invalid"]}