{"id":"10660", "title":"referencing a pointcut from an external class [SPR-5992]", "body":"**[Michael Isvy](https://jira.spring.io/secure/ViewProfile.jspa?name=michael.isvy)** opened **[SPR-5992](https://jira.spring.io/browse/SPR-5992?redirect=false)** and commented

In many cases, it is considered as a best practice that pointcuts should be externalized into a dedicated class. This is not so easy to do because we always need to specify the package path.

Please consider the following example.

```java
@Before(\"com.springsource.aspects.example.PoincutHolder.serviceMethod() || com.springsource.aspects.example.PoincutHolder.repositoryMethod()\" )
    public void monitor() {
       //...
    }
}
```

It would be much easier if we could reference PointcutHolder inside the configuration so we only need to specify the name of the pointcut itself. I was thinking of something like this:
Xml config:

```xml
<aop:config>
        <aop:pointcut-holder ref=\"pointcutHolder\"/>
    </aop:config>
```

And then it could be called in that way:

```java
@Before(\"serviceMethod() || repositoryMethod()\" )
    public void monitor() {
       //...
    }
}
```

This would be much easier to understand.
ps: of course, if 2 PoincutHolders hold some poincuts with the same names, we would just throw an exception at startup.


---

**Affects:** 3.0 M3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10660","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10660/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10660/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10660/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10660","id":398096731,"node_id":"MDU6SXNzdWUzOTgwOTY3MzE=","number":10660,"title":"referencing a pointcut from an external class [SPR-5992]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2009-08-09T20:02:39Z","updated_at":"2015-09-22T16:01:30Z","closed_at":"2015-09-22T16:01:30Z","author_association":"COLLABORATOR","body":"**[Michael Isvy](https://jira.spring.io/secure/ViewProfile.jspa?name=michael.isvy)** opened **[SPR-5992](https://jira.spring.io/browse/SPR-5992?redirect=false)** and commented\n\nIn many cases, it is considered as a best practice that pointcuts should be externalized into a dedicated class. This is not so easy to do because we always need to specify the package path.\n\nPlease consider the following example.\n\n```java\n@Before(\"com.springsource.aspects.example.PoincutHolder.serviceMethod() || com.springsource.aspects.example.PoincutHolder.repositoryMethod()\" )\n    public void monitor() {\n       //...\n    }\n}\n```\n\nIt would be much easier if we could reference PointcutHolder inside the configuration so we only need to specify the name of the pointcut itself. I was thinking of something like this:\nXml config:\n\n```xml\n<aop:config>\n        <aop:pointcut-holder ref=\"pointcutHolder\"/>\n    </aop:config>\n```\n\nAnd then it could be called in that way:\n\n```java\n@Before(\"serviceMethod() || repositoryMethod()\" )\n    public void monitor() {\n       //...\n    }\n}\n```\n\nThis would be much easier to understand.\nps: of course, if 2 PoincutHolders hold some poincuts with the same names, we would just throw an exception at startup.\n\n\n---\n\n**Affects:** 3.0 M3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453340828"], "labels":["in: core","status: declined","type: enhancement"]}