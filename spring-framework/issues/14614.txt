{"id":"14614", "title":"Make @RequestMapping inject the negotiated MediaType [SPR-9980]", "body":"**[Michael Osipov](https://jira.spring.io/secure/ViewProfile.jspa?name=michael-o)** opened **[SPR-9980](https://jira.spring.io/browse/SPR-9980?redirect=false)** and commented

I have a use caes where I do return a suitable object according for the negotiated content type.
In my case I accept JSON and XML. When JSON is returned to the client I produce a Map, when XML is returned I produce a custom object.

There is no way to determine easily which content type will be written out to the client. With examining the Accept header and iterating over registered message converters.

I have a work around which looks like this:

```
@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, 
  produces = { MediaType.APPLICATION_XML_VALUE })
  public ResponseEntity<Object> lookupProjectAsXml(@PathVariable String project,
      @RequestParam(value = \"fields\", required = false) String fields) {

    String[] fieldsArray = StringUtils.split(fields, ',');

    return lookup(project, fieldsArray, false, MediaType.APPLICATION_XML);
  }
```

and

```

@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, 
  produces = { MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<Object> lookupProjectAsJson(@PathVariable String project,
      @RequestParam(value = \"fields\", required = false) String fields,
      @RequestParam(value = \"asList\", required = false, defaultValue = \"false\") boolean asList) {

    String[] fieldsArray = StringUtils.split(fields, ',');

    return lookup(project, fieldsArray, asList, MediaType.APPLICATION_JSON);
  }
```

I would rather have:

```
@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, 
  produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
  public ResponseEntity<Object> lookupProject(@PathVariable String project,
      @RequestParam(value = \"fields\", required = false) String fields,
      @RequestParam(value = \"asList\", required = false, defaultValue = \"false\") boolean asList,
      MediaType mediaType) {

    // Process request...
    
    Object body;
    if (mediaType.equals(MediaType.APPLICATION_JSON)) {
      body = projectValues;
    } else if (mediaType.equals(MediaType.APPLICATION_XML)) {
      body = new Project(projectValues);
    } else {
      throw new NotImplementedException(\"Project lookup is not implemented for media type '\" + mediaType + \"'\");
    }
  
    return new ResponseEntity<Object>(body, HttpStatus.OK);
  }
```

---

**Affects:** 3.1.3

**Reference URL:** http://stackoverflow.com/q/13272443/696632

**Issue Links:**
- #13864 Response is committed before Interceptor postHandle invoked

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14614","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14614/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14614/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14614/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14614","id":398154729,"node_id":"MDU6SXNzdWUzOTgxNTQ3Mjk=","number":14614,"title":"Make @RequestMapping inject the negotiated MediaType [SPR-9980]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":9,"created_at":"2012-11-09T07:25:28Z","updated_at":"2019-01-11T14:20:38Z","closed_at":"2013-07-30T07:15:22Z","author_association":"COLLABORATOR","body":"**[Michael Osipov](https://jira.spring.io/secure/ViewProfile.jspa?name=michael-o)** opened **[SPR-9980](https://jira.spring.io/browse/SPR-9980?redirect=false)** and commented\n\nI have a use caes where I do return a suitable object according for the negotiated content type.\nIn my case I accept JSON and XML. When JSON is returned to the client I produce a Map, when XML is returned I produce a custom object.\n\nThere is no way to determine easily which content type will be written out to the client. With examining the Accept header and iterating over registered message converters.\n\nI have a work around which looks like this:\n\n```\n@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, \n  produces = { MediaType.APPLICATION_XML_VALUE })\n  public ResponseEntity<Object> lookupProjectAsXml(@PathVariable String project,\n      @RequestParam(value = \"fields\", required = false) String fields) {\n\n    String[] fieldsArray = StringUtils.split(fields, ',');\n\n    return lookup(project, fieldsArray, false, MediaType.APPLICATION_XML);\n  }\n```\n\nand\n\n```\n\n@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, \n  produces = { MediaType.APPLICATION_JSON_VALUE })\n  public ResponseEntity<Object> lookupProjectAsJson(@PathVariable String project,\n      @RequestParam(value = \"fields\", required = false) String fields,\n      @RequestParam(value = \"asList\", required = false, defaultValue = \"false\") boolean asList) {\n\n    String[] fieldsArray = StringUtils.split(fields, ',');\n\n    return lookup(project, fieldsArray, asList, MediaType.APPLICATION_JSON);\n  }\n```\n\nI would rather have:\n\n```\n@RequestMapping(value = \"/{project:[A-Z0-9_+\\\\.\\\\(\\\\)=\\\\-]+}\", method = RequestMethod.GET, \n  produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })\n  public ResponseEntity<Object> lookupProject(@PathVariable String project,\n      @RequestParam(value = \"fields\", required = false) String fields,\n      @RequestParam(value = \"asList\", required = false, defaultValue = \"false\") boolean asList,\n      MediaType mediaType) {\n\n    // Process request...\n    \n    Object body;\n    if (mediaType.equals(MediaType.APPLICATION_JSON)) {\n      body = projectValues;\n    } else if (mediaType.equals(MediaType.APPLICATION_XML)) {\n      body = new Project(projectValues);\n    } else {\n      throw new NotImplementedException(\"Project lookup is not implemented for media type '\" + mediaType + \"'\");\n    }\n  \n    return new ResponseEntity<Object>(body, HttpStatus.OK);\n  }\n```\n\n---\n\n**Affects:** 3.1.3\n\n**Reference URL:** http://stackoverflow.com/q/13272443/696632\n\n**Issue Links:**\n- #13864 Response is committed before Interceptor postHandle invoked\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453398557","453398560","453398561","453398563","453398564","453398568","453398571","453398573","453398574"], "labels":["in: web","status: declined","type: enhancement"]}