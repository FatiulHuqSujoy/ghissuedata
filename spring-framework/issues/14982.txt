{"id":"14982", "title":"Can I use Spring dependency injection to populate a read only bean for thread safety purpose? [SPR-10348]", "body":"**[Venkat Ganesh](https://jira.spring.io/secure/ViewProfile.jspa?name=vganesh)** opened **[SPR-10348](https://jira.spring.io/browse/SPR-10348?redirect=false)** and commented

Can I use Spring dependency injection to populate a read only bean for thread safety purpose?

After extracting the attached zip file to a folder, I can run the following command successfully and greeted by Pookey.
mvn clean compile exec:java

...

```
[INFO] --- exec-maven-plugin:1.2.1:java (default-cli) @ spring-readonly ---

Hello World!

Mar 02, 2013 1:32:00 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [application-context.xml]

My name is Pookey

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
...
```

My question is simple. I need to comment out setName method implementation in Person.java so that it will be a read only bean. I want spring
DI to create this bean using the data values I specify in an xml file and after it is created the thread safe bean will be used by the rest of the application.

The intent is the application programmer will create an xml file using the needed data values at run-time, when required and then ask Spring DI to create that bean. Since there are no set methods in the bean, it is thread safe from application point of view.

If I comment out the following three lines in Person.java,

```java
/*
	public void setName(String name) {
		this.name = name;
	}
	*/
```

and rerun it, I see this error. Is it not true using Java reflection the private data member can also be accessed? Can Spring set a private data member value when the setXXX() method is not available in the source code?

Or do we have to revert back to some aspectJ/groovy technique to make this happen?

Thanks and regards,
Ganesh

```
mvn clean compile exec:java 
...
[INFO] --- exec-maven-plugin:1.2.1:java (default-cli) @ spring-readonly ---
Hello World!
Mar 02, 2013 1:13:42 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [application-context.xml]

[WARNING]

java.lang.reflect.InvocationTargetException
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:601)
        at org.codehaus.mojo.exec.ExecJavaMojo$1.run(ExecJavaMojo.java:297)
        at java.lang.Thread.run(Thread.java:722)

Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'hello' defined in class path resource [application-context.xml]: Error setting property values; nested exception is org.springframework.beans.NotWritablePropertyException: Invalid property 'name' of bean class [read.only.
beans.spring.Person]: Bean property 'name' is not writable or has an invalid setter method. Does the parameter type of the setter match the return type of the getter?

        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1427)
        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:1132)
        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:522)
        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:461)
        at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:295)
        at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:223)
        at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:292)
        at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:194)
        at read.only.beans.spring.App.main(App.java:18)
        ... 6 more

Caused by: org.springframework.beans.NotWritablePropertyException: Invalid property 'name' of bean class [read.only.beans.spring.Person]: Bean property 'name' is not writable or has an invalid setter method. Does the parameter type of the setter match the return type of the getter?
        at org.springframework.beans.BeanWrapperImpl.setPropertyValue(BeanWrapperImpl.java:1042)
        at org.springframework.beans.BeanWrapperImpl.setPropertyValue(BeanWrapperImpl.java:902)
        at org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:75)
        at org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:57)
        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1424)
        ... 14 more

[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.881s
```



---

**Affects:** 3.2.1

**Attachments:**
- [spring-readonly.zip](https://jira.spring.io/secure/attachment/21002/spring-readonly.zip) (_4.41 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14982","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14982/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14982/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14982/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14982","id":398157726,"node_id":"MDU6SXNzdWUzOTgxNTc3MjY=","number":14982,"title":"Can I use Spring dependency injection to populate a read only bean for thread safety purpose? [SPR-10348]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2013-03-02T11:07:35Z","updated_at":"2019-01-12T05:27:17Z","closed_at":"2013-03-04T06:56:44Z","author_association":"COLLABORATOR","body":"**[Venkat Ganesh](https://jira.spring.io/secure/ViewProfile.jspa?name=vganesh)** opened **[SPR-10348](https://jira.spring.io/browse/SPR-10348?redirect=false)** and commented\n\nCan I use Spring dependency injection to populate a read only bean for thread safety purpose?\n\nAfter extracting the attached zip file to a folder, I can run the following command successfully and greeted by Pookey.\nmvn clean compile exec:java\n\n...\n\n```\n[INFO] --- exec-maven-plugin:1.2.1:java (default-cli) @ spring-readonly ---\n\nHello World!\n\nMar 02, 2013 1:32:00 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions\nINFO: Loading XML bean definitions from class path resource [application-context.xml]\n\nMy name is Pookey\n\n[INFO] ------------------------------------------------------------------------\n[INFO] BUILD SUCCESS\n...\n```\n\nMy question is simple. I need to comment out setName method implementation in Person.java so that it will be a read only bean. I want spring\nDI to create this bean using the data values I specify in an xml file and after it is created the thread safe bean will be used by the rest of the application.\n\nThe intent is the application programmer will create an xml file using the needed data values at run-time, when required and then ask Spring DI to create that bean. Since there are no set methods in the bean, it is thread safe from application point of view.\n\nIf I comment out the following three lines in Person.java,\n\n```java\n/*\n\tpublic void setName(String name) {\n\t\tthis.name = name;\n\t}\n\t*/\n```\n\nand rerun it, I see this error. Is it not true using Java reflection the private data member can also be accessed? Can Spring set a private data member value when the setXXX() method is not available in the source code?\n\nOr do we have to revert back to some aspectJ/groovy technique to make this happen?\n\nThanks and regards,\nGanesh\n\n```\nmvn clean compile exec:java \n...\n[INFO] --- exec-maven-plugin:1.2.1:java (default-cli) @ spring-readonly ---\nHello World!\nMar 02, 2013 1:13:42 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions\nINFO: Loading XML bean definitions from class path resource [application-context.xml]\n\n[WARNING]\n\njava.lang.reflect.InvocationTargetException\n        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)\n        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n        at java.lang.reflect.Method.invoke(Method.java:601)\n        at org.codehaus.mojo.exec.ExecJavaMojo$1.run(ExecJavaMojo.java:297)\n        at java.lang.Thread.run(Thread.java:722)\n\nCaused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'hello' defined in class path resource [application-context.xml]: Error setting property values; nested exception is org.springframework.beans.NotWritablePropertyException: Invalid property 'name' of bean class [read.only.\nbeans.spring.Person]: Bean property 'name' is not writable or has an invalid setter method. Does the parameter type of the setter match the return type of the getter?\n\n        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1427)\n        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:1132)\n        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:522)\n        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:461)\n        at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:295)\n        at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:223)\n        at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:292)\n        at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:194)\n        at read.only.beans.spring.App.main(App.java:18)\n        ... 6 more\n\nCaused by: org.springframework.beans.NotWritablePropertyException: Invalid property 'name' of bean class [read.only.beans.spring.Person]: Bean property 'name' is not writable or has an invalid setter method. Does the parameter type of the setter match the return type of the getter?\n        at org.springframework.beans.BeanWrapperImpl.setPropertyValue(BeanWrapperImpl.java:1042)\n        at org.springframework.beans.BeanWrapperImpl.setPropertyValue(BeanWrapperImpl.java:902)\n        at org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:75)\n        at org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:57)\n        at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1424)\n        ... 14 more\n\n[INFO] ------------------------------------------------------------------------\n[INFO] BUILD FAILURE\n[INFO] ------------------------------------------------------------------------\n[INFO] Total time: 5.881s\n```\n\n\n\n---\n\n**Affects:** 3.2.1\n\n**Attachments:**\n- [spring-readonly.zip](https://jira.spring.io/secure/attachment/21002/spring-readonly.zip) (_4.41 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453401544","453401545"], "labels":["in: core","status: invalid"]}