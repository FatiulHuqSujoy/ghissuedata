{"id":"6142", "title":"Nullpointerexception in org.springframework.aop.target.ThreadLocalTargetSource [SPR-1442]", "body":"**[Lars Rosenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=slapfivehigh)** opened **[SPR-1442](https://jira.spring.io/browse/SPR-1442?redirect=false)** and commented

While testing some frustrating aspects of \"prototypes versus Spring AOP\" I got the described NullPointerException. Lars

The exception:

java.lang.NullPointerException
at org.springframework.aop.target.ThreadLocalTargetSource.getTarget(ThreadLocalTargetSource.java:72)
at com.foo.tests.spring.proxytest.PrototypeProxyTestCase.createThreadLocalBusinessInterface(PrototypeProxyTestCase.java:133)
at com.foo.tests.spring.proxytest.PrototypeProxyTestCase.testClearNotASingletonDestroy(PrototypeProxyTestCase.java:84)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
at com.intellij.rt.execution.junit2.JUnitStarter.main(JUnitStarter.java:31)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
at com.intellij.rt.execution.application.AppMain.main(AppMain.java:86)

The test case:

/**
* Tests that I do not get a singleton using a {`@link` org.springframework.aop.framework.ProxyFactoryBean} when

* the business interface is marked as a prototype. Will destroy the target.
  */
  public void testClearNotASingletonDestroy() {
  BusinessImpl business = createThreadLocalBusinessInterface(true);
  BusinessImpl business2 = createThreadLocalBusinessInterface(true);

  Assert.assertNotSame(\"The two proxied business interfaces are the same (singletons). \", business, business2);

}

/**
* Creates a business implementation. Uses a singleton target source.
* `@param` destroy if \\<code>true\\</code> then the target referenced by the target source is destroyed.
* `@return` a business implementation.
  */
  private BusinessImpl createThreadLocalBusinessInterface(boolean destroy) {
  ProxyFactoryBean proxy = (ProxyFactoryBean) applicationContext.getBean(\"&thread-local-business\");
  ThreadLocalTargetSource targetSource = (ThreadLocalTargetSource) proxy.getTargetSource();
  BusinessImpl business = (BusinessImpl) targetSource.getTarget();  // line 133 that generates the nullpointerexception.
  Assert.assertTrue(\"Target is static\", !targetSource.isStatic());
  if (destroy) {
  targetSource.destroy();
  }
  return business;
  }

The configuration:

\\<bean id=\"thread-local-business\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">
\\<property name=\"targetSource\" ref=\"thread-local-business-target-source\"/>
\\<property name=\"proxyInterfaces\">
\\<list>
\\<value>com.foo.tests.spring.proxytest.Business\\</value>
\\</list>
\\</property>
\\</bean>
\\<bean id=\"thread-local-business-target-source\" class=\"org.springframework.aop.target.ThreadLocalTargetSource\">
\\<property name=\"targetBeanName\">\\<idref local=\"business-target\"/>\\</property>
\\</bean>
\\<bean id=\"prototype-thread-local-business\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">
\\<property name=\"targetSource\" ref=\"prototype-thread-local-business-target-source\"/>
\\<property name=\"proxyInterfaces\">
\\<list>
\\<value>com.foo.tests.spring.proxytest.Business\\</value>
\\</list>
\\</property>
\\</bean>
\\<bean id=\"prototype-thread-local-business-target-source\" class=\"org.springframework.aop.target.ThreadLocalTargetSource\" singleton=\"false\">
\\<property name=\"targetBeanName\">\\<idref local=\"business-target\"/>\\</property>
\\</bean>
\\<bean id=\"business-target\" class=\"com.foo.tests.spring.proxytest.impl.BusinessImpl\" singleton=\"false\"/>


---

**Affects:** 1.2.5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6142","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6142/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6142/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6142/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6142","id":398061247,"node_id":"MDU6SXNzdWUzOTgwNjEyNDc=","number":6142,"title":"Nullpointerexception in org.springframework.aop.target.ThreadLocalTargetSource [SPR-1442]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false,"description":"A general bug"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/24","html_url":"https://github.com/spring-projects/spring-framework/milestone/24","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/24/labels","id":3960794,"node_id":"MDk6TWlsZXN0b25lMzk2MDc5NA==","number":24,"title":"1.2.6","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":71,"state":"closed","created_at":"2019-01-10T22:02:21Z","updated_at":"2019-01-10T23:49:07Z","due_on":null,"closed_at":"2019-01-10T22:02:21Z"},"comments":1,"created_at":"2005-11-05T01:21:22Z","updated_at":"2012-06-19T03:54:20Z","closed_at":"2012-06-19T03:54:20Z","author_association":"COLLABORATOR","body":"**[Lars Rosenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=slapfivehigh)** opened **[SPR-1442](https://jira.spring.io/browse/SPR-1442?redirect=false)** and commented\n\nWhile testing some frustrating aspects of \"prototypes versus Spring AOP\" I got the described NullPointerException. Lars\n\nThe exception:\n\njava.lang.NullPointerException\nat org.springframework.aop.target.ThreadLocalTargetSource.getTarget(ThreadLocalTargetSource.java:72)\nat com.foo.tests.spring.proxytest.PrototypeProxyTestCase.createThreadLocalBusinessInterface(PrototypeProxyTestCase.java:133)\nat com.foo.tests.spring.proxytest.PrototypeProxyTestCase.testClearNotASingletonDestroy(PrototypeProxyTestCase.java:84)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\nat com.intellij.rt.execution.junit2.JUnitStarter.main(JUnitStarter.java:31)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\nat com.intellij.rt.execution.application.AppMain.main(AppMain.java:86)\n\nThe test case:\n\n/**\n* Tests that I do not get a singleton using a {`@link` org.springframework.aop.framework.ProxyFactoryBean} when\n\n* the business interface is marked as a prototype. Will destroy the target.\n  */\n  public void testClearNotASingletonDestroy() {\n  BusinessImpl business = createThreadLocalBusinessInterface(true);\n  BusinessImpl business2 = createThreadLocalBusinessInterface(true);\n\n  Assert.assertNotSame(\"The two proxied business interfaces are the same (singletons). \", business, business2);\n\n}\n\n/**\n* Creates a business implementation. Uses a singleton target source.\n* `@param` destroy if \\<code>true\\</code> then the target referenced by the target source is destroyed.\n* `@return` a business implementation.\n  */\n  private BusinessImpl createThreadLocalBusinessInterface(boolean destroy) {\n  ProxyFactoryBean proxy = (ProxyFactoryBean) applicationContext.getBean(\"&thread-local-business\");\n  ThreadLocalTargetSource targetSource = (ThreadLocalTargetSource) proxy.getTargetSource();\n  BusinessImpl business = (BusinessImpl) targetSource.getTarget();  // line 133 that generates the nullpointerexception.\n  Assert.assertTrue(\"Target is static\", !targetSource.isStatic());\n  if (destroy) {\n  targetSource.destroy();\n  }\n  return business;\n  }\n\nThe configuration:\n\n\\<bean id=\"thread-local-business\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">\n\\<property name=\"targetSource\" ref=\"thread-local-business-target-source\"/>\n\\<property name=\"proxyInterfaces\">\n\\<list>\n\\<value>com.foo.tests.spring.proxytest.Business\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\\<bean id=\"thread-local-business-target-source\" class=\"org.springframework.aop.target.ThreadLocalTargetSource\">\n\\<property name=\"targetBeanName\">\\<idref local=\"business-target\"/>\\</property>\n\\</bean>\n\\<bean id=\"prototype-thread-local-business\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">\n\\<property name=\"targetSource\" ref=\"prototype-thread-local-business-target-source\"/>\n\\<property name=\"proxyInterfaces\">\n\\<list>\n\\<value>com.foo.tests.spring.proxytest.Business\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\\<bean id=\"prototype-thread-local-business-target-source\" class=\"org.springframework.aop.target.ThreadLocalTargetSource\" singleton=\"false\">\n\\<property name=\"targetBeanName\">\\<idref local=\"business-target\"/>\\</property>\n\\</bean>\n\\<bean id=\"business-target\" class=\"com.foo.tests.spring.proxytest.impl.BusinessImpl\" singleton=\"false\"/>\n\n\n---\n\n**Affects:** 1.2.5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453299266"], "labels":["in: core","type: bug"]}