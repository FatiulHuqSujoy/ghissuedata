{"id":"12282", "title":"Auto proxying does not seem to work for interfaces with \"generices\" parameter types [SPR-7626]", "body":"**[Kyrill Alyoshin](https://jira.spring.io/secure/ViewProfile.jspa?name=kyrill007)** opened **[SPR-7626](https://jira.spring.io/browse/SPR-7626?redirect=false)** and commented

So, let's say we have an interface:

```
public CrudOperations<T> {
 void create(T entity);
}
```

and an inheriting typed interface

```
public VendorService extends CrudOperation<Vendor> {
}
```

and an implementation Hibernate service

```
public VendorServiceImpl implements VendorService {
 public void create(Vendor vendor) {
   //...get Hibernate session
   session.save(vendor);
 }
}
```

'create' method in VSI will not be transactionally wired using standard Spring configuration mechanisms. (I suspect that the compile-generated bridge method is getting wired (i.e. public void create(Object vendor)).) I do get Session not thread-bound exception in my case.

The workaround is, of course, to manually start/join the transaction using TransactionTemplate inside the 'create' method. But it is nonetheless a curious bug.


---

**Affects:** 3.0.4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12282","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12282/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12282/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12282/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12282","id":398107947,"node_id":"MDU6SXNzdWUzOTgxMDc5NDc=","number":12282,"title":"Auto proxying does not seem to work for interfaces with \"generices\" parameter types [SPR-7626]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2010-10-07T12:23:06Z","updated_at":"2019-01-11T14:32:46Z","closed_at":"2018-12-28T11:30:28Z","author_association":"COLLABORATOR","body":"**[Kyrill Alyoshin](https://jira.spring.io/secure/ViewProfile.jspa?name=kyrill007)** opened **[SPR-7626](https://jira.spring.io/browse/SPR-7626?redirect=false)** and commented\n\nSo, let's say we have an interface:\n\n```\npublic CrudOperations<T> {\n void create(T entity);\n}\n```\n\nand an inheriting typed interface\n\n```\npublic VendorService extends CrudOperation<Vendor> {\n}\n```\n\nand an implementation Hibernate service\n\n```\npublic VendorServiceImpl implements VendorService {\n public void create(Vendor vendor) {\n   //...get Hibernate session\n   session.save(vendor);\n }\n}\n```\n\n'create' method in VSI will not be transactionally wired using standard Spring configuration mechanisms. (I suspect that the compile-generated bridge method is getting wired (i.e. public void create(Object vendor)).) I do get Session not thread-bound exception in my case.\n\nThe workaround is, of course, to manually start/join the transaction using TransactionTemplate inside the 'create' method. But it is nonetheless a curious bug.\n\n\n---\n\n**Affects:** 3.0.4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453354397","453354398"], "labels":["in: core"]}