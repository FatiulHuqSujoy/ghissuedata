{"id":"20838", "title":"Replace explicit null-checks by a declarative approach [SPR-16291]", "body":"**[Denis Zhdanov](https://jira.spring.io/secure/ViewProfile.jspa?name=denis.zhdanov)** opened **[SPR-16291](https://jira.spring.io/browse/SPR-16291?redirect=false)** and commented

The idea is to replace explicit _null_ checks by _NonNull_ annotation on target method arguments and let the actual checks be inserted into resulting byte code during compilation based on that annotations.

[This PR](https://github.com/spring-projects/spring-framework/pull/1621) illustrates the concept on the _AbstractAliasAwareAnnotationAttributeExtractor_ class:

**Before**

```java
AbstractAliasAwareAnnotationAttributeExtractor(
        Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, S source) {
    Assert.notNull(annotationType, \"annotationType must not be null\");
    Assert.notNull(source, \"source must not be null\");
    this.annotationType = annotationType;
```

**After**

```java
AbstractAliasAwareAnnotationAttributeExtractor(
        @Nonnull Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, @Nonnull S source) {
    this.annotationType = annotationType;
```

The actual null check is added by the [Traute](http://traute.oss.harmonysoft.tech/) _javac_ plugin - it's configured to generate them for all method parameters marked by the _org.springframework.lang.NonNull_ annotation - [link](https://github.com/spring-projects/spring-framework/pull/1621/files#diff-c197962302397baf3a4cc36463dce5ea).

Result looks as if it's compiled from the source below:

```java
AbstractAliasAwareAnnotationAttributeExtractor(@NonNull Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, @NonNull S source) {
    if (annotationType == null) {
      throw new NullPointerException(\"annotationType must not be null\");
    }
    if (source == null) {
      throw new NullPointerException(\"source must not be null\");
    }
    this.annotationType = annotationType;
```

Actual result:

```
javap -c ./spring-core/build/classes/java/main/org/springframework/core/annotation/AbstractAliasAwareAnnotationAttributeExtractor.class
...
  org.springframework.core.annotation.AbstractAliasAwareAnnotationAttributeExtractor(java.lang.Class<? extends java.lang.annotation.Annotation>, java.lang.Object, S);
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object.\"<init>\":()V
       4: aload_1
       5: ifnonnull     18
       8: new           #2                  // class java/lang/NullPointerException
      11: dup
      12: ldc           #3                  // String annotationType must not be null
      14: invokespecial #4                  // Method java/lang/NullPointerException.\"<init>\":(Ljava/lang/String;)V
      17: athrow
      18: aload_3
      19: ifnonnull     32
      22: new           #2                  // class java/lang/NullPointerException
      25: dup
      26: ldc           #5                  // String source must not be null
      28: invokespecial #4                  // Method java/lang/NullPointerException.\"<init>\":(Ljava/lang/String;)V
      31: athrow
...
```

An additional benefit is that the annotation is present in javadocs, i.e. the contract is defined in more clear way. Another good thing is that IDEs highlight potential _NPE_ when a _might-be-null_ is used as a _NonNull_-parameter.

I'm the _Traute_'s author and I'm keen on applying it into beloved _Spring Framework_, so, if the team likes the change, I'm fine with creating a _PR_ which replaces all existing _null_-checks by the _NonNull_ annotation.


---

**Affects:** 5.0.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20838","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20838/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20838/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20838/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20838","id":398220017,"node_id":"MDU6SXNzdWUzOTgyMjAwMTc=","number":20838,"title":"Replace explicit null-checks by a declarative approach [SPR-16291]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":6,"created_at":"2017-12-12T08:31:58Z","updated_at":"2019-01-13T04:58:51Z","closed_at":"2017-12-19T15:27:42Z","author_association":"COLLABORATOR","body":"**[Denis Zhdanov](https://jira.spring.io/secure/ViewProfile.jspa?name=denis.zhdanov)** opened **[SPR-16291](https://jira.spring.io/browse/SPR-16291?redirect=false)** and commented\n\nThe idea is to replace explicit _null_ checks by _NonNull_ annotation on target method arguments and let the actual checks be inserted into resulting byte code during compilation based on that annotations.\n\n[This PR](https://github.com/spring-projects/spring-framework/pull/1621) illustrates the concept on the _AbstractAliasAwareAnnotationAttributeExtractor_ class:\n\n**Before**\n\n```java\nAbstractAliasAwareAnnotationAttributeExtractor(\n        Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, S source) {\n    Assert.notNull(annotationType, \"annotationType must not be null\");\n    Assert.notNull(source, \"source must not be null\");\n    this.annotationType = annotationType;\n```\n\n**After**\n\n```java\nAbstractAliasAwareAnnotationAttributeExtractor(\n        @Nonnull Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, @Nonnull S source) {\n    this.annotationType = annotationType;\n```\n\nThe actual null check is added by the [Traute](http://traute.oss.harmonysoft.tech/) _javac_ plugin - it's configured to generate them for all method parameters marked by the _org.springframework.lang.NonNull_ annotation - [link](https://github.com/spring-projects/spring-framework/pull/1621/files#diff-c197962302397baf3a4cc36463dce5ea).\n\nResult looks as if it's compiled from the source below:\n\n```java\nAbstractAliasAwareAnnotationAttributeExtractor(@NonNull Class<? extends Annotation> annotationType, @Nullable Object annotatedElement, @NonNull S source) {\n    if (annotationType == null) {\n      throw new NullPointerException(\"annotationType must not be null\");\n    }\n    if (source == null) {\n      throw new NullPointerException(\"source must not be null\");\n    }\n    this.annotationType = annotationType;\n```\n\nActual result:\n\n```\njavap -c ./spring-core/build/classes/java/main/org/springframework/core/annotation/AbstractAliasAwareAnnotationAttributeExtractor.class\n...\n  org.springframework.core.annotation.AbstractAliasAwareAnnotationAttributeExtractor(java.lang.Class<? extends java.lang.annotation.Annotation>, java.lang.Object, S);\n    Code:\n       0: aload_0\n       1: invokespecial #1                  // Method java/lang/Object.\"<init>\":()V\n       4: aload_1\n       5: ifnonnull     18\n       8: new           #2                  // class java/lang/NullPointerException\n      11: dup\n      12: ldc           #3                  // String annotationType must not be null\n      14: invokespecial #4                  // Method java/lang/NullPointerException.\"<init>\":(Ljava/lang/String;)V\n      17: athrow\n      18: aload_3\n      19: ifnonnull     32\n      22: new           #2                  // class java/lang/NullPointerException\n      25: dup\n      26: ldc           #5                  // String source must not be null\n      28: invokespecial #4                  // Method java/lang/NullPointerException.\"<init>\":(Ljava/lang/String;)V\n      31: athrow\n...\n```\n\nAn additional benefit is that the annotation is present in javadocs, i.e. the contract is defined in more clear way. Another good thing is that IDEs highlight potential _NPE_ when a _might-be-null_ is used as a _NonNull_-parameter.\n\nI'm the _Traute_'s author and I'm keen on applying it into beloved _Spring Framework_, so, if the team likes the change, I'm fine with creating a _PR_ which replaces all existing _null_-checks by the _NonNull_ annotation.\n\n\n---\n\n**Affects:** 5.0.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453464933","453464936","453464938","453464940","453464942","453464944"], "labels":["in: core","status: declined","type: enhancement"]}