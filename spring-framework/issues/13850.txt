{"id":"13850", "title":"Add <form:legend ..> tag to form taglib [SPR-9212]", "body":"**[Marcel Overdijk](https://jira.spring.io/secure/ViewProfile.jspa?name=marceloverdijk)** opened **[SPR-9212](https://jira.spring.io/browse/SPR-9212?redirect=false)** and commented

Just ass something like

```
<form:label path=\"description\" cssErrorClass=\"fielderror\">Product Group:</form:label>
```

a equivalent

```
<form:legend path=\"description\" cssErrorClass=\"fielderror\">Product Groep:</form:legend>
```

would be very useful.

Let me explain by the below example:

```
<fieldset data-role=\"controlgroup\">
	<legend>Gender:</legend>
     	<input type=\"radio\" name=\"gender\" id=\"gender-male\" value=\"Male\" checked=\"checked\" />
     	<label for=\"gender-male\">Male</label>
     	<input type=\"radio\" name=\"gender\" id=\"gender-female\" value=\"Female\"  />
     	<label for=\"gender-female\">Female</label>
</fieldset>
```

Note the use of the legend element to semantically show the 'label'/legend for the group of radiobuttons.
A ordinary label (instead of the legend) is semantically incorrect as both inputs basically form the element and already have their own labels.

The above example can be translated to Spring form tags easily except the legend tag.

Summarizing, something like:

```
<form:legend path=\"gender\" cssErrorClass=\"fielderror\">Gender:</form:legend> should do the trick.
```

(Note that above code snippet is based on jQuery Mobile's code exmaples)


---

**Reference URL:** http://forum.springsource.org/showthread.php?124005-How-to-check-if-particular-has-an-error-(in-jsp)&p=404712#post404712
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13850","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13850/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13850/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13850/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13850","id":398117886,"node_id":"MDU6SXNzdWUzOTgxMTc4ODY=","number":13850,"title":"Add <form:legend ..> tag to form taglib [SPR-9212]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2012-03-09T05:26:52Z","updated_at":"2019-01-12T03:48:57Z","closed_at":"2019-01-12T00:27:25Z","author_association":"COLLABORATOR","body":"**[Marcel Overdijk](https://jira.spring.io/secure/ViewProfile.jspa?name=marceloverdijk)** opened **[SPR-9212](https://jira.spring.io/browse/SPR-9212?redirect=false)** and commented\n\nJust ass something like\n\n```\n<form:label path=\"description\" cssErrorClass=\"fielderror\">Product Group:</form:label>\n```\n\na equivalent\n\n```\n<form:legend path=\"description\" cssErrorClass=\"fielderror\">Product Groep:</form:legend>\n```\n\nwould be very useful.\n\nLet me explain by the below example:\n\n```\n<fieldset data-role=\"controlgroup\">\n\t<legend>Gender:</legend>\n     \t<input type=\"radio\" name=\"gender\" id=\"gender-male\" value=\"Male\" checked=\"checked\" />\n     \t<label for=\"gender-male\">Male</label>\n     \t<input type=\"radio\" name=\"gender\" id=\"gender-female\" value=\"Female\"  />\n     \t<label for=\"gender-female\">Female</label>\n</fieldset>\n```\n\nNote the use of the legend element to semantically show the 'label'/legend for the group of radiobuttons.\nA ordinary label (instead of the legend) is semantically incorrect as both inputs basically form the element and already have their own labels.\n\nThe above example can be translated to Spring form tags easily except the legend tag.\n\nSummarizing, something like:\n\n```\n<form:legend path=\"gender\" cssErrorClass=\"fielderror\">Gender:</form:legend> should do the trick.\n```\n\n(Note that above code snippet is based on jQuery Mobile's code exmaples)\n\n\n---\n\n**Reference URL:** http://forum.springsource.org/showthread.php?124005-How-to-check-if-particular-has-an-error-(in-jsp)&p=404712#post404712\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453716687"], "labels":["in: web","status: bulk-closed"]}