{"id":"12118", "title":"Need additional check for edge case for Rhino calls [SPR-7460]", "body":"**[James Cook](https://jira.spring.io/secure/ViewProfile.jspa?name=oravecz)** opened **[SPR-7460](https://jira.spring.io/browse/SPR-7460?redirect=false)** and commented

We use a lot of server-side Javascript running on Rhino. When I issue a call from Rhino for a java function that can accept open-ended parameters, it can cause some confusion.

For example:
From JavaScript --
var bean = appContext.getBean('myBean');

In Java, there are several getBean(...) functions with different parameter sets.

Object getBean(String name)
\\<T> T getBean(String name, Class\\<T> requiredType)
Object getBean(String name, Object... args)
\\<T> T getBean(String name, Class\\<T> requiredType, Object... args)

It just so happens that my call ends up routed to:
Object getBean(String name, Object... args)

The name is 'myBean' as expected, but the args array is String[0].

This wouldn't be so bad, except later in the process, this check occurs in checkMergedBeanDefinition():

if (args != null && !mbd.isPrototype()) {
throw new BeanDefinitionStoreException(...);
}

So, if this is rewritten slightly, it will solve my dilemma.

if (args != null && args.length > 0 && !mbd.isPrototype()) {
throw new BeanDefinitionStoreException(...);
}

I suppose one could debate the merits of whether Rhino should be calling this method versus the single parameter method, but I assume they had a good reason for doing this. At the very least, it is probably an easier change to integrate in Spring than in Rhino. :)



---

**Affects:** 3.0.3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12118","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12118/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12118/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12118/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12118","id":398106818,"node_id":"MDU6SXNzdWUzOTgxMDY4MTg=","number":12118,"title":"Need additional check for edge case for Rhino calls [SPR-7460]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2010-08-12T11:56:09Z","updated_at":"2018-12-28T10:26:30Z","closed_at":"2018-12-28T10:26:30Z","author_association":"COLLABORATOR","body":"**[James Cook](https://jira.spring.io/secure/ViewProfile.jspa?name=oravecz)** opened **[SPR-7460](https://jira.spring.io/browse/SPR-7460?redirect=false)** and commented\n\nWe use a lot of server-side Javascript running on Rhino. When I issue a call from Rhino for a java function that can accept open-ended parameters, it can cause some confusion.\n\nFor example:\nFrom JavaScript --\nvar bean = appContext.getBean('myBean');\n\nIn Java, there are several getBean(...) functions with different parameter sets.\n\nObject getBean(String name)\n\\<T> T getBean(String name, Class\\<T> requiredType)\nObject getBean(String name, Object... args)\n\\<T> T getBean(String name, Class\\<T> requiredType, Object... args)\n\nIt just so happens that my call ends up routed to:\nObject getBean(String name, Object... args)\n\nThe name is 'myBean' as expected, but the args array is String[0].\n\nThis wouldn't be so bad, except later in the process, this check occurs in checkMergedBeanDefinition():\n\nif (args != null && !mbd.isPrototype()) {\nthrow new BeanDefinitionStoreException(...);\n}\n\nSo, if this is rewritten slightly, it will solve my dilemma.\n\nif (args != null && args.length > 0 && !mbd.isPrototype()) {\nthrow new BeanDefinitionStoreException(...);\n}\n\nI suppose one could debate the merits of whether Rhino should be calling this method versus the single parameter method, but I assume they had a good reason for doing this. At the very least, it is probably an easier change to integrate in Spring than in Rhino. :)\n\n\n\n---\n\n**Affects:** 3.0.3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453352994"], "labels":["in: core","status: declined","type: enhancement"]}