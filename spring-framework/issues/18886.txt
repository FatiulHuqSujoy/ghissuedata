{"id":"18886", "title":"ConcurrentMapCacheManager's storeByValue does not pick up ClassLoader [SPR-14314]", "body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-14314](https://jira.spring.io/browse/SPR-14314?redirect=false)** and commented

I've tried a new feature of cacheing supported at Spring 4.3.
I've used the `storeByValue` property of `ConcurrentMapCacheManager` as follow:

```
@EnableCaching
@Configuration
public class CacheConfig {
    @Bean
    CacheManager cacheManager() {
        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager(\"configs\");
        cacheManager.setStoreByValue(true);
        return cacheManager;
    }
}
```

However, it does not worked serialization and deserialization a cache value :(
Is my configuration wrong ?

I've tried as follow, it work fine.  I want to know best configuration for this case.

```
@EnableCaching
@Configuration
public class CacheConfig implements BeanClassLoaderAware { // Add

    ClassLoader classLoader;

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Bean
    CacheManager cacheManager() {
        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager(\"configs\");
        cacheManager.setBeanClassLoader(classLoader); // Add
        cacheManager.setStoreByValue(true);
        return cacheManager;
    }

}
```

---

**Affects:** 4.3 RC2

**Issue Links:**
- #18331 Support store-by-value in ConcurrentMapCacheManager

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18886","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18886/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18886/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18886/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18886","id":398193716,"node_id":"MDU6SXNzdWUzOTgxOTM3MTY=","number":18886,"title":"ConcurrentMapCacheManager's storeByValue does not pick up ClassLoader [SPR-14314]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/149","html_url":"https://github.com/spring-projects/spring-framework/milestone/149","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/149/labels","id":3960922,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyMg==","number":149,"title":"4.3 GA","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":63,"state":"closed","created_at":"2019-01-10T22:04:50Z","updated_at":"2019-01-11T09:14:25Z","due_on":"2016-06-09T07:00:00Z","closed_at":"2019-01-10T22:04:50Z"},"comments":2,"created_at":"2016-05-28T12:18:00Z","updated_at":"2019-01-11T16:09:09Z","closed_at":"2016-06-10T09:19:12Z","author_association":"COLLABORATOR","body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-14314](https://jira.spring.io/browse/SPR-14314?redirect=false)** and commented\n\nI've tried a new feature of cacheing supported at Spring 4.3.\nI've used the `storeByValue` property of `ConcurrentMapCacheManager` as follow:\n\n```\n@EnableCaching\n@Configuration\npublic class CacheConfig {\n    @Bean\n    CacheManager cacheManager() {\n        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager(\"configs\");\n        cacheManager.setStoreByValue(true);\n        return cacheManager;\n    }\n}\n```\n\nHowever, it does not worked serialization and deserialization a cache value :(\nIs my configuration wrong ?\n\nI've tried as follow, it work fine.  I want to know best configuration for this case.\n\n```\n@EnableCaching\n@Configuration\npublic class CacheConfig implements BeanClassLoaderAware { // Add\n\n    ClassLoader classLoader;\n\n    @Override\n    public void setBeanClassLoader(ClassLoader classLoader) {\n        this.classLoader = classLoader;\n    }\n\n    @Bean\n    CacheManager cacheManager() {\n        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager(\"configs\");\n        cacheManager.setBeanClassLoader(classLoader); // Add\n        cacheManager.setStoreByValue(true);\n        return cacheManager;\n    }\n\n}\n```\n\n---\n\n**Affects:** 4.3 RC2\n\n**Issue Links:**\n- #18331 Support store-by-value in ConcurrentMapCacheManager\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453440433","453440434"], "labels":["in: core","type: bug"]}