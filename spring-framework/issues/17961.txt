{"id":"17961", "title":"Cannot set a Content-Type using ScriptTemplateView [SPR-13379]", "body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-13379](https://jira.spring.io/browse/SPR-13379?redirect=false)** and commented

If `ScriptTemplateView` is used as `View`, response data display as plain text in Web Browser because it cannot set a Content-Type header(such as `\"text/html\"`).

I tried it using following implementations and configuration.

```java
@Controller
@RequestMapping(\"script-template\")
public class ScriptTemplateController {

    private static final Logger logger = LoggerFactory
            .getLogger(ScriptTemplateController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String home(Locale locale, Model model,HttpServletResponse response) {
        logger.info(\"Welcome home! The client locale is {}.\", locale);

        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
                DateFormat.LONG, locale);

        model.addAttribute(\"title\", \"Sample title\")
                .addAttribute(\"body\", \"Sample body\")
                .addAttribute(\"serverTime\", dateFormat.format(new Date()));

        return \"script-template/home\";
    }

}
```

```html
<html>
<head>
    <title>`title`</title>
</head>
<body>
<p>`body`</p>

<p>The time on the server is `serverTime`.</p>
</body>
</html>
```

```xml
<mvc:view-resolvers>
    <mvc:script-template prefix=\"META-INF/views/\" suffix=\".html\" />
</mvc:view-resolvers>

<mvc:script-template-configurer engine-name=\"nashorn\" render-object=\"Mustache\" render-function=\"render\">
    <mvc:script location=\"classpath:META-INF/resources/vendor/mustache/mustache.js\"/>
</mvc:script-template-configurer>
```

I access to `\"http://localhost:8080/script-template/\"`. However response data does not render as HTML. it render as plain text.

```html
<html>
<head>
    <title>Sample title</title>
</head>
<body>
<p>Sample body</p>

<p>The time on the server is August 23, 2015 4:58:33 AM JST.</p>
</body>
</html>
```

I expect to display as follows in Web Browser.

```
Sample body

The time on the server is August 23, 2015 5:20:12 AM JST.
```

I tried following other solution. However, it cannot resolved.

```xml
<mvc:view-resolvers>
    <mvc:bean-name />
</mvc:view-resolvers>

<bean id=\"script-template/home\" class=\"org.springframework.web.servlet.view.script.ScriptTemplateView\">
    <property name=\"url\" value=\"META-INF/views/script-template/home.html\"/>
    <property name=\"contentType\" value=\"text/html\"/>
</bean>
```

The `contentType` property is not used  in `ScriptTemplateView`.

I think it should be fix as can be specify a content-type.
How do think ?


---

**Affects:** 4.2 GA

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/04cff89eb700e82765615ff47b78aa47a9daf3fa
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17961","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17961/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17961/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17961/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17961","id":398183215,"node_id":"MDU6SXNzdWUzOTgxODMyMTU=","number":17961,"title":"Cannot set a Content-Type using ScriptTemplateView [SPR-13379]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/138","html_url":"https://github.com/spring-projects/spring-framework/milestone/138","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/138/labels","id":3960911,"node_id":"MDk6TWlsZXN0b25lMzk2MDkxMQ==","number":138,"title":"4.2.1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":60,"state":"closed","created_at":"2019-01-10T22:04:37Z","updated_at":"2019-01-11T11:16:15Z","due_on":"2015-08-31T07:00:00Z","closed_at":"2019-01-10T22:04:37Z"},"comments":4,"created_at":"2015-08-22T13:44:58Z","updated_at":"2015-09-01T11:38:22Z","closed_at":"2015-09-01T11:38:22Z","author_association":"COLLABORATOR","body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-13379](https://jira.spring.io/browse/SPR-13379?redirect=false)** and commented\n\nIf `ScriptTemplateView` is used as `View`, response data display as plain text in Web Browser because it cannot set a Content-Type header(such as `\"text/html\"`).\n\nI tried it using following implementations and configuration.\n\n```java\n@Controller\n@RequestMapping(\"script-template\")\npublic class ScriptTemplateController {\n\n    private static final Logger logger = LoggerFactory\n            .getLogger(ScriptTemplateController.class);\n\n    @RequestMapping(method = RequestMethod.GET)\n    public String home(Locale locale, Model model,HttpServletResponse response) {\n        logger.info(\"Welcome home! The client locale is {}.\", locale);\n\n        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,\n                DateFormat.LONG, locale);\n\n        model.addAttribute(\"title\", \"Sample title\")\n                .addAttribute(\"body\", \"Sample body\")\n                .addAttribute(\"serverTime\", dateFormat.format(new Date()));\n\n        return \"script-template/home\";\n    }\n\n}\n```\n\n```html\n<html>\n<head>\n    <title>`title`</title>\n</head>\n<body>\n<p>`body`</p>\n\n<p>The time on the server is `serverTime`.</p>\n</body>\n</html>\n```\n\n```xml\n<mvc:view-resolvers>\n    <mvc:script-template prefix=\"META-INF/views/\" suffix=\".html\" />\n</mvc:view-resolvers>\n\n<mvc:script-template-configurer engine-name=\"nashorn\" render-object=\"Mustache\" render-function=\"render\">\n    <mvc:script location=\"classpath:META-INF/resources/vendor/mustache/mustache.js\"/>\n</mvc:script-template-configurer>\n```\n\nI access to `\"http://localhost:8080/script-template/\"`. However response data does not render as HTML. it render as plain text.\n\n```html\n<html>\n<head>\n    <title>Sample title</title>\n</head>\n<body>\n<p>Sample body</p>\n\n<p>The time on the server is August 23, 2015 4:58:33 AM JST.</p>\n</body>\n</html>\n```\n\nI expect to display as follows in Web Browser.\n\n```\nSample body\n\nThe time on the server is August 23, 2015 5:20:12 AM JST.\n```\n\nI tried following other solution. However, it cannot resolved.\n\n```xml\n<mvc:view-resolvers>\n    <mvc:bean-name />\n</mvc:view-resolvers>\n\n<bean id=\"script-template/home\" class=\"org.springframework.web.servlet.view.script.ScriptTemplateView\">\n    <property name=\"url\" value=\"META-INF/views/script-template/home.html\"/>\n    <property name=\"contentType\" value=\"text/html\"/>\n</bean>\n```\n\nThe `contentType` property is not used  in `ScriptTemplateView`.\n\nI think it should be fix as can be specify a content-type.\nHow do think ?\n\n\n---\n\n**Affects:** 4.2 GA\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/04cff89eb700e82765615ff47b78aa47a9daf3fa\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453429370","453429371","453429372","453429374"], "labels":["in: web","type: bug"]}