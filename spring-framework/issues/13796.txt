{"id":"13796", "title":"MethodInterceptor causes integration test to commit changes to database [SPR-9158]", "body":"**[Guillermo De Luca](https://jira.spring.io/secure/ViewProfile.jspa?name=grd22001)** opened **[SPR-9158](https://jira.spring.io/browse/SPR-9158?redirect=false)** and commented

#### Overview

The following Spring AOP method interceptor (`MethodLoggingInterceptor`) causes a Hibernate-based DAO test case to commit changes to the database even though the transaction manged by the Spring TestContext framework is rolled back.

---

#### Code

Regarding the `testCreateRemoveHunter()` method in `SampleDaoImplTest`:

- The test works if the method interceptor is not enabled.
- The test works if only one operation is performed per test method.
- The logs confirm that the Spring TestContext Framework properly rolls back the transaction after the test method.

---

```
public class MethodLoggingInterceptor implements MethodInterceptor {

    private final Logger logger = Logger.getLogger(this.getClass().toString());

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Object result = methodInvocation.proceed();

        stopWatch.stop();

        StringBuilder logMessage = new StringBuilder();
        // build logMessage ...
	logger.info(logMessage.toString());

        return result;
    }
}
```

```xml
<bean name=\"methodLoggingInterceptor\" class=\"com.kohls.listing.das.interceptor.MethodLoggingInterceptor\"/>

<bean name=\"proxyCreator\" class=\"org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator\">
    <property name=\"beanNames\">
        <list>
            <value>*</value>
        </list>
     </property>
     <property name=\"interceptorNames\">
        <list>
            <value>methodLoggingInterceptor</value>
        </list>
     </property>
</bean>

<bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">
        <property name=\"driverClassName\" value=\"${database.driver}\"/>
        <property name=\"url\" value=\"${database.url}\"/>
        <property name=\"username\" value=\"${database.username}\"/>
        <property name=\"password\" value=\"${database.password}\"/>
        <property name=\"defaultCatalog\" value=\"${hibernate.default_schema}\"/>
</bean>

<bean id=\"transactionManager\" class=\"org.springframework.orm.hibernate3.HibernateTransactionManager\">
        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>
</bean>

<bean id=\"sessionFactory\" class=\"org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean\">
        <property name=\"dataSource\" ref=\"dataSource\"/>
        <property name=\"hibernateProperties\">
            <props>
                <prop key=\"hibernate.jdbc.batch_size\">100</prop>
                <prop key=\"hibernate.show_sql\">true</prop>
                <prop key=\"hibernate.connection.autocommit\">false</prop>
                <prop key=\"hibernate.dialect\">${database.dialect}</prop>
            </props>
        </property>
        <property name=\"configLocation\" value=\"hibernate.cfg.xml\"/>
</bean>
```

```
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(\"classpath*:applicationContext.xml\")
public class SampleDaoImplTest {

    @Autowired
    HunterDao hunterDao;

    @Test
    @Transactional
    public void testCreateRemoveHunter() {
        Hunter hunter = new Hunter();
        hunterDao.createHunter(hunter);
        hunterDao.deleteHunter(hunter);
    }

    @AfterTransaction
    public void afterTransaction() {
        // if you query the state of the database, the hunter table
        // contains the Hunter entity created in testCreateRemoveHunter(),
        // but it shouldn't since the transaction for the test was rolled
        // back by the Spring TestContext Framework.
    }
}
```

---

#### Analysis

The basic cause of the problem is that the `MethodInterceptor` is applied to **every** bean in the context by specifying \"*\" for the `beanNames` property of `BeanNameAutoProxyCreator`. This results in the interceptor being applied not only to application beans but also to the Hibernate `SessionFactory`.

If the `beanNames` property of `BeanNameAutoProxyCreator` is changed to something less _greedy_ than \"*\" -- for example, \"*Dao\" -- the `sessionFactory` will not be advised by the method interceptor, and the code will work as expected.

As for why the `SessionFactory` fails to operate as expected when advised by a `MethodInterceptor`, that remains to be determined, but it is assumed that Spring's Hibernate support fails to properly manage the `Session` for the current thread if the `SessionFactory` is hidden behind a dynamic proxy, which is the case when a `MethodInterceptor` is applied to every bean in the application context.

---

**Affects:** 3.0.5, 3.1.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13796","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13796/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13796/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13796/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13796","id":398117517,"node_id":"MDU6SXNzdWUzOTgxMTc1MTc=","number":13796,"title":"MethodInterceptor causes integration test to commit changes to database [SPR-9158]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false},"assignees":[{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}],"milestone":null,"comments":8,"created_at":"2012-02-23T12:10:50Z","updated_at":"2019-01-13T07:06:07Z","closed_at":"2012-07-31T07:10:10Z","author_association":"COLLABORATOR","body":"**[Guillermo De Luca](https://jira.spring.io/secure/ViewProfile.jspa?name=grd22001)** opened **[SPR-9158](https://jira.spring.io/browse/SPR-9158?redirect=false)** and commented\n\n#### Overview\n\nThe following Spring AOP method interceptor (`MethodLoggingInterceptor`) causes a Hibernate-based DAO test case to commit changes to the database even though the transaction manged by the Spring TestContext framework is rolled back.\n\n---\n\n#### Code\n\nRegarding the `testCreateRemoveHunter()` method in `SampleDaoImplTest`:\n\n- The test works if the method interceptor is not enabled.\n- The test works if only one operation is performed per test method.\n- The logs confirm that the Spring TestContext Framework properly rolls back the transaction after the test method.\n\n---\n\n```\npublic class MethodLoggingInterceptor implements MethodInterceptor {\n\n    private final Logger logger = Logger.getLogger(this.getClass().toString());\n\n    @Override\n    public Object invoke(MethodInvocation methodInvocation) throws Throwable {\n\n        StopWatch stopWatch = new StopWatch();\n        stopWatch.start();\n\n        Object result = methodInvocation.proceed();\n\n        stopWatch.stop();\n\n        StringBuilder logMessage = new StringBuilder();\n        // build logMessage ...\n\tlogger.info(logMessage.toString());\n\n        return result;\n    }\n}\n```\n\n```xml\n<bean name=\"methodLoggingInterceptor\" class=\"com.kohls.listing.das.interceptor.MethodLoggingInterceptor\"/>\n\n<bean name=\"proxyCreator\" class=\"org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator\">\n    <property name=\"beanNames\">\n        <list>\n            <value>*</value>\n        </list>\n     </property>\n     <property name=\"interceptorNames\">\n        <list>\n            <value>methodLoggingInterceptor</value>\n        </list>\n     </property>\n</bean>\n\n<bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">\n        <property name=\"driverClassName\" value=\"${database.driver}\"/>\n        <property name=\"url\" value=\"${database.url}\"/>\n        <property name=\"username\" value=\"${database.username}\"/>\n        <property name=\"password\" value=\"${database.password}\"/>\n        <property name=\"defaultCatalog\" value=\"${hibernate.default_schema}\"/>\n</bean>\n\n<bean id=\"transactionManager\" class=\"org.springframework.orm.hibernate3.HibernateTransactionManager\">\n        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>\n</bean>\n\n<bean id=\"sessionFactory\" class=\"org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean\">\n        <property name=\"dataSource\" ref=\"dataSource\"/>\n        <property name=\"hibernateProperties\">\n            <props>\n                <prop key=\"hibernate.jdbc.batch_size\">100</prop>\n                <prop key=\"hibernate.show_sql\">true</prop>\n                <prop key=\"hibernate.connection.autocommit\">false</prop>\n                <prop key=\"hibernate.dialect\">${database.dialect}</prop>\n            </props>\n        </property>\n        <property name=\"configLocation\" value=\"hibernate.cfg.xml\"/>\n</bean>\n```\n\n```\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(\"classpath*:applicationContext.xml\")\npublic class SampleDaoImplTest {\n\n    @Autowired\n    HunterDao hunterDao;\n\n    @Test\n    @Transactional\n    public void testCreateRemoveHunter() {\n        Hunter hunter = new Hunter();\n        hunterDao.createHunter(hunter);\n        hunterDao.deleteHunter(hunter);\n    }\n\n    @AfterTransaction\n    public void afterTransaction() {\n        // if you query the state of the database, the hunter table\n        // contains the Hunter entity created in testCreateRemoveHunter(),\n        // but it shouldn't since the transaction for the test was rolled\n        // back by the Spring TestContext Framework.\n    }\n}\n```\n\n---\n\n#### Analysis\n\nThe basic cause of the problem is that the `MethodInterceptor` is applied to **every** bean in the context by specifying \"*\" for the `beanNames` property of `BeanNameAutoProxyCreator`. This results in the interceptor being applied not only to application beans but also to the Hibernate `SessionFactory`.\n\nIf the `beanNames` property of `BeanNameAutoProxyCreator` is changed to something less _greedy_ than \"*\" -- for example, \"*Dao\" -- the `sessionFactory` will not be advised by the method interceptor, and the code will work as expected.\n\nAs for why the `SessionFactory` fails to operate as expected when advised by a `MethodInterceptor`, that remains to be determined, but it is assumed that Spring's Hibernate support fails to properly manage the `Session` for the current thread if the `SessionFactory` is hidden behind a dynamic proxy, which is the case when a `MethodInterceptor` is applied to every bean in the application context.\n\n---\n\n**Affects:** 3.0.5, 3.1.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453365883","453365884","453365885","453365887","453365889","453365890","453365891","453365892"], "labels":["in: data","in: test","status: declined"]}