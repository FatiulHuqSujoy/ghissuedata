{"id":"21097", "title":"Using @ControllerAdvice with WebFlux  [SPR-16554]", "body":"**[Enzo Bonggio](https://jira.spring.io/secure/ViewProfile.jspa?name=enzo.bonggio)** opened **[SPR-16554](https://jira.spring.io/browse/SPR-16554?redirect=false)** and commented

I have a project that I want to migrate from Tomcat to Netty but I found that one particular error is not being catch by my ControllerAdvice class.

To reproduce the problem:
1. create one project with webflux dependency
2. create one class that have ControllerAdvice annotation
3. add this code to the class:

```java
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    Complex handleAll(Throwable ex) {
        return new Complex();
    }

    @Data
    public static class Complex {
        String something;
    }
```

4. on Application.java add :

```java
@GetMapping(\"/something\")
Mono<Void> something() {
     return Mono.error(new Throwable(\"mensaje\"));
}
```

5. bootRun the app
6. make a POST call to localhost:8080/something

At this point I expect that the ExceptionHandler catch Method Not Allowed exception and return my Complex object but is not happening. I don't understand also why this is working as expected if I add compile('org.springframework.boot:spring-boot-starter-web') to the project.

---

**Reference URL:** https://gist.github.com/enzobonggio/8807cfc7e63c73c38fad9018a5c76702

**Issue Links:**
- #20940 [docs] Add WebFlux content on exception handling
- #21109 Support `@ResponseStatus-annotated` exceptions on WebFlux

0 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21097","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21097/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21097/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21097/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21097","id":398223736,"node_id":"MDU6SXNzdWUzOTgyMjM3MzY=","number":21097,"title":"Using @ControllerAdvice with WebFlux  [SPR-16554]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":9,"created_at":"2018-03-06T02:36:24Z","updated_at":"2019-01-14T04:31:59Z","closed_at":"2018-03-08T13:45:00Z","author_association":"COLLABORATOR","body":"**[Enzo Bonggio](https://jira.spring.io/secure/ViewProfile.jspa?name=enzo.bonggio)** opened **[SPR-16554](https://jira.spring.io/browse/SPR-16554?redirect=false)** and commented\n\nI have a project that I want to migrate from Tomcat to Netty but I found that one particular error is not being catch by my ControllerAdvice class.\n\nTo reproduce the problem:\n1. create one project with webflux dependency\n2. create one class that have ControllerAdvice annotation\n3. add this code to the class:\n\n```java\n    @ExceptionHandler(Throwable.class)\n    @ResponseBody\n    Complex handleAll(Throwable ex) {\n        return new Complex();\n    }\n\n    @Data\n    public static class Complex {\n        String something;\n    }\n```\n\n4. on Application.java add :\n\n```java\n@GetMapping(\"/something\")\nMono<Void> something() {\n     return Mono.error(new Throwable(\"mensaje\"));\n}\n```\n\n5. bootRun the app\n6. make a POST call to localhost:8080/something\n\nAt this point I expect that the ExceptionHandler catch Method Not Allowed exception and return my Complex object but is not happening. I don't understand also why this is working as expected if I add compile('org.springframework.boot:spring-boot-starter-web') to the project.\n\n---\n\n**Reference URL:** https://gist.github.com/enzobonggio/8807cfc7e63c73c38fad9018a5c76702\n\n**Issue Links:**\n- #20940 [docs] Add WebFlux content on exception handling\n- #21109 Support `@ResponseStatus-annotated` exceptions on WebFlux\n\n0 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453468291","453468293","453468295","453468296","453468297","453468298","453468300","453468302","453468303"], "labels":["in: web","status: declined"]}