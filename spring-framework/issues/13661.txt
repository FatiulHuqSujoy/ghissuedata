{"id":"13661", "title":".hibernate4.LocalSessionFac toryBean doen't have \"p:lobHandler-ref\" property [SPR-9022]", "body":"**[Maxim Kuzmik](https://jira.spring.io/secure/ViewProfile.jspa?name=forn)** opened **[SPR-9022](https://jira.spring.io/browse/SPR-9022?redirect=false)** and commented

Hello.

After merge for spring 3.1 and Hibernate 4. I can't do the work with Clob.

I have hbm file:

```
...
<property generated=\"never\" lazy=\"false\" name=\"hbmProperties\" type=\"org.springframework.orm.hibernate3.support.ClobStringType\">
...
```

In the package hibernate4.support.ClobStringType is absent.

And second one in the configuration i was have:
Code:

```
<bean id=\"sessionFactory\"
        class=\"org.springframework.orm.hibernate3.LocalSessionFactoryBean\"
        p:dataSource-ref=\"asgePortalDataSource\" p:lobHandler-ref=\"oracleLobHandler\">
...
</bean>
<bean id=\"oracleLobHandler\" class=\"org.springframework.jdbc.support.lob.OracleLobHandler\"
        lazy-init=\"true\">
        <property name=\"nativeJdbcExtractor\">
            <ref local=\"nativeJdbcExtractor\" />
        </property>
    </bean>
```

Now the org.springframework.orm.hibernate4.LocalSessionFac toryBean doen't have \"p:lobHandler-ref\" property.

What is the path to fix this?


---

**Affects:** 3.1 GA

**Reference URL:** http://forum.springsource.org/showthread.php?121101-Hibernate-4-logHandler-ClobStringType
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13661","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13661/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13661/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13661/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13661","id":398116667,"node_id":"MDU6SXNzdWUzOTgxMTY2Njc=","number":13661,"title":".hibernate4.LocalSessionFac toryBean doen't have \"p:lobHandler-ref\" property [SPR-9022]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2012-01-12T22:03:59Z","updated_at":"2012-01-13T06:14:26Z","closed_at":"2012-01-13T06:14:26Z","author_association":"COLLABORATOR","body":"**[Maxim Kuzmik](https://jira.spring.io/secure/ViewProfile.jspa?name=forn)** opened **[SPR-9022](https://jira.spring.io/browse/SPR-9022?redirect=false)** and commented\n\nHello.\n\nAfter merge for spring 3.1 and Hibernate 4. I can't do the work with Clob.\n\nI have hbm file:\n\n```\n...\n<property generated=\"never\" lazy=\"false\" name=\"hbmProperties\" type=\"org.springframework.orm.hibernate3.support.ClobStringType\">\n...\n```\n\nIn the package hibernate4.support.ClobStringType is absent.\n\nAnd second one in the configuration i was have:\nCode:\n\n```\n<bean id=\"sessionFactory\"\n        class=\"org.springframework.orm.hibernate3.LocalSessionFactoryBean\"\n        p:dataSource-ref=\"asgePortalDataSource\" p:lobHandler-ref=\"oracleLobHandler\">\n...\n</bean>\n<bean id=\"oracleLobHandler\" class=\"org.springframework.jdbc.support.lob.OracleLobHandler\"\n        lazy-init=\"true\">\n        <property name=\"nativeJdbcExtractor\">\n            <ref local=\"nativeJdbcExtractor\" />\n        </property>\n    </bean>\n```\n\nNow the org.springframework.orm.hibernate4.LocalSessionFac toryBean doen't have \"p:lobHandler-ref\" property.\n\nWhat is the path to fix this?\n\n\n---\n\n**Affects:** 3.1 GA\n\n**Reference URL:** http://forum.springsource.org/showthread.php?121101-Hibernate-4-logHandler-ClobStringType\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453364819"], "labels":["in: core","status: declined","type: enhancement"]}