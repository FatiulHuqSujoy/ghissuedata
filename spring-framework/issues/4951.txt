{"id":"4951", "title":"New spring:nestedPath Tag [SPR-220]", "body":"**[Seth Ladd](https://jira.spring.io/secure/ViewProfile.jspa?name=sethladd)** opened **[SPR-220](https://jira.spring.io/browse/SPR-220?redirect=false)** and commented

For the form simplification macros, a new tag will be needed.  This new tag, <spring:nestedPath>, allows for simplification of creating JSTL pages.  It will allow for easy including of common JSP Fragments without knowledge of the actual bean, or child bean, that the properties are from.

For example, common elements such as Address fields, may be isolated inside a JSP Fragment.  This allows for any page to include the file and to display address fields.

The including page would wrap the include with a <spring:nestedPath> tag to declare what bean, or child bean, the include's properties are from.

The <spring:bind> tag will need to be updated to optionally understand if it's being used inside the <spring:nestedPath> tag to append the nestedPath variable to the field being bound.

The example below details a full JSP 2.0 Taglib implementation of <spring:nestedPath> and the edited <spring:bind>, plus a form simplication macro.  It's assumed that the final version of <spring:nestedPath> would be a Java class, and the <spring:bind> modifications would be made to the Java class.

Example:

---

address_include.jspf

\\<div>
\\<springx:fieldLabel name=\"street1\"/>
\\<f:text name=\"street1\" />
\\</div>
\\<div>
\\<springx:fieldLabel name=\"street2\"/>
\\<f:text name=\"street2\" />
\\</div>

---

---

new_account.jsp

\\<div>
\\<springx:fieldLabel name=\"username\"/>
\\<f:text name=\"username\" />
\\</div>
\\<div>
\\<spring:nestedPath path=\"mailingAddress\">
\\<%@ include file=\"/WEB-INF/jspf/address_include.jspf\" %>
\\</spring:nestedPath>
\\</div>

---

---

text.tag (referenced from address_include.jspf)

<%@ tag body-content=\"empty\" dynamic-attributes=\"dynattrs\" %>
<%@ attribute name=\"name\" required=\"true\" %>
<%@ include file=\"/WEB-INF/jspf/taglibs.jspf\" %>
<springx:bind path=\"${name}\">
<input type=\"text\" value=\"${currentStatus.value}\" name=\"${currentStatus.expression}\" <c:forEach items=\"${dynattrs}\"var=\"a\">${a.key}=\"${a.value}\" </c:forEach> />
</springx:bind>

---

---

springx:bind.tag  (understands the nestedPath if it exists)

<%@ tag body-content=\"scriptless\" %>
<%@ attribute name=\"path\" required=\"true\" %>
<%@ include file=\"/WEB-INF/jspf/taglibs.jspf\" %>

<c:choose>
<c:when test=\"${empty nestedPath}\">
<c:set value=\"${path}\" var=\"nestedPath\" scope=\"request\" />
</c:when>
<c:otherwise>
<c:set value=\"${nestedPath}\" var=\"savedNestedPath\" scope=\"page\" />
<c:set value=\"${nestedPath}.${path}\" var=\"nestedPath\" scope=\"request\" />
</c:otherwise>
</c:choose>
<jsp:doBody/>
<c:set value=\"${savedNestedPath}\" var=\"nestedPath\" scope=\"request\"/>

---

---

**Affects:** 1.1 RC2

**Attachments:**
- [nested_tag_tests.patch](https://jira.spring.io/secure/attachment/10151/nested_tag_tests.patch) (_6.43 kB_)
- [nested_tag.patch](https://jira.spring.io/secure/attachment/10150/nested_tag.patch) (_6.86 kB_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4951","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4951/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4951/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4951/events","html_url":"https://github.com/spring-projects/spring-framework/issues/4951","id":398050088,"node_id":"MDU6SXNzdWUzOTgwNTAwODg=","number":4951,"title":"New spring:nestedPath Tag [SPR-220]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/8","html_url":"https://github.com/spring-projects/spring-framework/milestone/8","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/8/labels","id":3960777,"node_id":"MDk6TWlsZXN0b25lMzk2MDc3Nw==","number":8,"title":"1.1 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":65,"state":"closed","created_at":"2019-01-10T22:02:00Z","updated_at":"2019-01-10T22:57:01Z","due_on":null,"closed_at":"2019-01-10T22:02:00Z"},"comments":4,"created_at":"2004-07-15T06:27:36Z","updated_at":"2019-01-13T22:54:38Z","closed_at":"2004-07-27T18:58:13Z","author_association":"COLLABORATOR","body":"**[Seth Ladd](https://jira.spring.io/secure/ViewProfile.jspa?name=sethladd)** opened **[SPR-220](https://jira.spring.io/browse/SPR-220?redirect=false)** and commented\n\nFor the form simplification macros, a new tag will be needed.  This new tag, <spring:nestedPath>, allows for simplification of creating JSTL pages.  It will allow for easy including of common JSP Fragments without knowledge of the actual bean, or child bean, that the properties are from.\n\nFor example, common elements such as Address fields, may be isolated inside a JSP Fragment.  This allows for any page to include the file and to display address fields.\n\nThe including page would wrap the include with a <spring:nestedPath> tag to declare what bean, or child bean, the include's properties are from.\n\nThe <spring:bind> tag will need to be updated to optionally understand if it's being used inside the <spring:nestedPath> tag to append the nestedPath variable to the field being bound.\n\nThe example below details a full JSP 2.0 Taglib implementation of <spring:nestedPath> and the edited <spring:bind>, plus a form simplication macro.  It's assumed that the final version of <spring:nestedPath> would be a Java class, and the <spring:bind> modifications would be made to the Java class.\n\nExample:\n\n---\n\naddress_include.jspf\n\n\\<div>\n\\<springx:fieldLabel name=\"street1\"/>\n\\<f:text name=\"street1\" />\n\\</div>\n\\<div>\n\\<springx:fieldLabel name=\"street2\"/>\n\\<f:text name=\"street2\" />\n\\</div>\n\n---\n\n---\n\nnew_account.jsp\n\n\\<div>\n\\<springx:fieldLabel name=\"username\"/>\n\\<f:text name=\"username\" />\n\\</div>\n\\<div>\n\\<spring:nestedPath path=\"mailingAddress\">\n\\<%@ include file=\"/WEB-INF/jspf/address_include.jspf\" %>\n\\</spring:nestedPath>\n\\</div>\n\n---\n\n---\n\ntext.tag (referenced from address_include.jspf)\n\n<%@ tag body-content=\"empty\" dynamic-attributes=\"dynattrs\" %>\n<%@ attribute name=\"name\" required=\"true\" %>\n<%@ include file=\"/WEB-INF/jspf/taglibs.jspf\" %>\n<springx:bind path=\"${name}\">\n<input type=\"text\" value=\"${currentStatus.value}\" name=\"${currentStatus.expression}\" <c:forEach items=\"${dynattrs}\"var=\"a\">${a.key}=\"${a.value}\" </c:forEach> />\n</springx:bind>\n\n---\n\n---\n\nspringx:bind.tag  (understands the nestedPath if it exists)\n\n<%@ tag body-content=\"scriptless\" %>\n<%@ attribute name=\"path\" required=\"true\" %>\n<%@ include file=\"/WEB-INF/jspf/taglibs.jspf\" %>\n\n<c:choose>\n<c:when test=\"${empty nestedPath}\">\n<c:set value=\"${path}\" var=\"nestedPath\" scope=\"request\" />\n</c:when>\n<c:otherwise>\n<c:set value=\"${nestedPath}\" var=\"savedNestedPath\" scope=\"page\" />\n<c:set value=\"${nestedPath}.${path}\" var=\"nestedPath\" scope=\"request\" />\n</c:otherwise>\n</c:choose>\n<jsp:doBody/>\n<c:set value=\"${savedNestedPath}\" var=\"nestedPath\" scope=\"request\"/>\n\n---\n\n---\n\n**Affects:** 1.1 RC2\n\n**Attachments:**\n- [nested_tag_tests.patch](https://jira.spring.io/secure/attachment/10151/nested_tag_tests.patch) (_6.43 kB_)\n- [nested_tag.patch](https://jira.spring.io/secure/attachment/10150/nested_tag.patch) (_6.86 kB_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453286249","453286252","453286253","453286254"], "labels":["in: web","type: enhancement"]}