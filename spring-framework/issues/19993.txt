{"id":"19993", "title":"[doc] Update @ControllerAdvice Javadoc to discuss ordering [SPR-15432]", "body":"**[Andrzej Martynowicz](https://jira.spring.io/secure/ViewProfile.jspa?name=walkeros)** opened **[SPR-15432](https://jira.spring.io/browse/SPR-15432?redirect=false)** and commented

When there are multiple `@ControllerAdvice` annotated classes it seems that they are selected basing on alphabetical order rather than trying to do \"best match\".

Consider following two controllers. First is ordinary spring webmvc controller (annotated with `@Controller`) , the other is rest controlller (annotated with `@RestController`):

```java
// this one is annotated with @Controller
@Controller
public class TestController {
  
  @ResponseBody
  @RequestMapping(method = GET, value = \"/test\")
  public String testMethod() {
    if (true) {
      throw new RuntimeException(\"asdasdsadsad\");
    }
    return \"asdsadsad\";
  }
}

@RestController
@RequestMapping(value = \"/soume-url\", produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
public class SomeController {
  
  @RequestMapping(method = GET)
  public ResponseEntity<? extends Something> someMethod() {
    if (true) {
        throw new RuntimeException(\"ewewe\");
    }
     return new Something();
    }
  }
}
```

Now let's create two exception handler classes annotated with `@ControllerAdvice`. The second one is annotated with `@ControllerAdvice`(annotations = RestController.class) with the intention to be executed only for `@RestController` annotated classes.

```java
@ControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handleUnexpectedException(Exception e) {
    return new ResponseEntity<String>(\"some message\", HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

@ControllerAdvice(annotations = RestController.class)
public class RestControllerExceptionHandler {

  @ExceptionHandler(Exception.class)
  @ResponseBody // not needed(?), but addded just in cases
  public ResponseEntity<ErrorResponse> handleUnexpectedException(Exception e) {

    ErrorResponse errorResponse = new ErrorResponse(\"my error response\");
    return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
```

My expectation would be that exceptions thrown by `@RestController` classes are handled by RestControllerExceptionHandler because this one is more specialized (annotated with `@ControllerAdvice`(annotations = RestController.class)). However this does not work like this - the request is handled always by ControllerExceptionHandler.

Now the interesting thing is when you rename the RestControllerExceptionHandler to ARestControllerExceptionHandler  (so that it alphabetically precedes ControllerExceptionHandler) all works as exepected - ARestControllerExceptionHandler  handles exceptions from classes annotated with `@RestController` and `@ControllerExceptionHandler` handles all the other.

See also: http://stackoverflow.com/questions/43325685/spring-different-exception-handler-for-restcontroller-and-controller where I tried to ask for assistance and finally ended up with this bug,


---

**Affects:** 4.3.7

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/fce8ed62cef7df6aaffe5699dc64bf1a0c4faf3a, https://github.com/spring-projects/spring-framework/commit/546d7ddd07b75a3d17d46125510c11d83268bfd7
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19993","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19993/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19993/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19993/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19993","id":398207806,"node_id":"MDU6SXNzdWUzOTgyMDc4MDY=","number":19993,"title":"[doc] Update @ControllerAdvice Javadoc to discuss ordering [SPR-15432]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511868,"node_id":"MDU6TGFiZWwxMTg4NTExODY4","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20documentation","name":"type: documentation","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/157","html_url":"https://github.com/spring-projects/spring-framework/milestone/157","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/157/labels","id":3960930,"node_id":"MDk6TWlsZXN0b25lMzk2MDkzMA==","number":157,"title":"4.3.8","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":39,"state":"closed","created_at":"2019-01-10T22:05:00Z","updated_at":"2019-01-11T09:42:20Z","due_on":"2017-04-17T07:00:00Z","closed_at":"2019-01-10T22:05:00Z"},"comments":3,"created_at":"2017-04-11T08:23:04Z","updated_at":"2017-04-18T15:43:21Z","closed_at":"2017-04-18T15:43:21Z","author_association":"COLLABORATOR","body":"**[Andrzej Martynowicz](https://jira.spring.io/secure/ViewProfile.jspa?name=walkeros)** opened **[SPR-15432](https://jira.spring.io/browse/SPR-15432?redirect=false)** and commented\n\nWhen there are multiple `@ControllerAdvice` annotated classes it seems that they are selected basing on alphabetical order rather than trying to do \"best match\".\n\nConsider following two controllers. First is ordinary spring webmvc controller (annotated with `@Controller`) , the other is rest controlller (annotated with `@RestController`):\n\n```java\n// this one is annotated with @Controller\n@Controller\npublic class TestController {\n  \n  @ResponseBody\n  @RequestMapping(method = GET, value = \"/test\")\n  public String testMethod() {\n    if (true) {\n      throw new RuntimeException(\"asdasdsadsad\");\n    }\n    return \"asdsadsad\";\n  }\n}\n\n@RestController\n@RequestMapping(value = \"/soume-url\", produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)\npublic class SomeController {\n  \n  @RequestMapping(method = GET)\n  public ResponseEntity<? extends Something> someMethod() {\n    if (true) {\n        throw new RuntimeException(\"ewewe\");\n    }\n     return new Something();\n    }\n  }\n}\n```\n\nNow let's create two exception handler classes annotated with `@ControllerAdvice`. The second one is annotated with `@ControllerAdvice`(annotations = RestController.class) with the intention to be executed only for `@RestController` annotated classes.\n\n```java\n@ControllerAdvice\npublic class ControllerExceptionHandler {\n\n  @ExceptionHandler(Exception.class)\n  public ResponseEntity<String> handleUnexpectedException(Exception e) {\n    return new ResponseEntity<String>(\"some message\", HttpStatus.INTERNAL_SERVER_ERROR);\n  }\n}\n\n@ControllerAdvice(annotations = RestController.class)\npublic class RestControllerExceptionHandler {\n\n  @ExceptionHandler(Exception.class)\n  @ResponseBody // not needed(?), but addded just in cases\n  public ResponseEntity<ErrorResponse> handleUnexpectedException(Exception e) {\n\n    ErrorResponse errorResponse = new ErrorResponse(\"my error response\");\n    return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);\n  }\n}\n```\n\nMy expectation would be that exceptions thrown by `@RestController` classes are handled by RestControllerExceptionHandler because this one is more specialized (annotated with `@ControllerAdvice`(annotations = RestController.class)). However this does not work like this - the request is handled always by ControllerExceptionHandler.\n\nNow the interesting thing is when you rename the RestControllerExceptionHandler to ARestControllerExceptionHandler  (so that it alphabetically precedes ControllerExceptionHandler) all works as exepected - ARestControllerExceptionHandler  handles exceptions from classes annotated with `@RestController` and `@ControllerExceptionHandler` handles all the other.\n\nSee also: http://stackoverflow.com/questions/43325685/spring-different-exception-handler-for-restcontroller-and-controller where I tried to ask for assistance and finally ended up with this bug,\n\n\n---\n\n**Affects:** 4.3.7\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/fce8ed62cef7df6aaffe5699dc64bf1a0c4faf3a, https://github.com/spring-projects/spring-framework/commit/546d7ddd07b75a3d17d46125510c11d83268bfd7\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453453998","453453999","453454004"], "labels":["in: web","type: documentation"]}