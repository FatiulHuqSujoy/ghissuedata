{"id":"6853", "title":"Exception when resolving circular references among nested bean definitions [SPR-2162]", "body":"**[k d x](https://jira.spring.io/secure/ViewProfile.jspa?name=kdx)** opened **[SPR-2162](https://jira.spring.io/browse/SPR-2162?redirect=false)** and commented

Given the following Java sources:

p/Main.java:

---

package p;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.InputStreamResource;

public class Main
{

    public static void main(String [] args) throws FileNotFoundException
    {
        InputStream is1 = new FileInputStream(args[0]);
        XmlBeanFactory bf = new XmlBeanFactory(new InputStreamResource(is1));
        O o = (O)bf.getBeansOfType(O.class).values().iterator().next();
        System.out.println(\"o=\"+o);
    }

}

---

p/O.java:

---

package p;

public class O
{

    private I i;
    
    public I getI()
    {
    return this.i;
    }
    
    public void setI(I i)
    {
    this.i = i;
    }

}

---

p/I.java

---

package p;

public class I
{

    private I i;
    
    public I getI()
    {
    return this.i;
    }
    
    public void setI(I i)
    {
    this.i = i;
    }

}

---

and the following bean definition (spring-refcycle.xml):

---

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>
\\<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\"
\"http://www.springframework.org/dtd/spring-beans.dtd\">
\\<beans>
\\<bean id=\"bean-0\" class=\"p.O\">
\\<property name=\"i\">
\\<bean id=\"bean-1\" class=\"p.I\">
\\<property name=\"i\">
\\<bean id=\"bean-2\" class=\"p.I\">
\\<property name=\"i\">\\<ref bean=\"bean-1\" />\\</property>
\\</bean>
\\</property>
\\</bean>
\\</property>
\\</bean>
\\</beans>

---

running \"Main spring-refcycle.xml\" results in an exception:

---

16:50:38,287 INFO  [CollectionFactory] JDK 1.4+ collections available
16:50:38,287 DEBUG [CollectionFactory] Creating [java.util.LinkedHashMap]
16:50:38,334 INFO  [XmlBeanDefinitionReader] Loading XML bean definitions from resource loaded through InputStream
16:50:38,334 DEBUG [XmlBeanDefinitionReader] Using JAXP implementation [com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl@aeea66]
16:50:38,334 DEBUG [ResourceEntityResolver] Trying to resolve XML entity with public ID [-//SPRING//DTD BEAN//EN] and system ID http://www.springframework.org/dtd/spring-beans.dtd
16:50:38,334 DEBUG [ResourceEntityResolver] Trying to locate [spring-beans.dtd] in Spring jar
16:50:38,365 DEBUG [ResourceEntityResolver] Found beans DTD http://www.springframework.org/dtd/spring-beans.dtd in classpath
16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Loading bean definitions
16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default lazy init 'false'
16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default autowire 'no'
16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default dependency check 'none'
16:50:38,412 DEBUG [DefaultXmlBeanDefinitionParser] Found 1 \\<bean> elements in resource loaded through InputStream
16:50:38,412 DEBUG [CollectionFactory] Creating [java.util.LinkedHashMap]
16:51:05,850 DEBUG [XmlBeanFactory] Creating shared instance of singleton bean 'bean-0'
16:51:41,209 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-0' with merged definition [Root bean: class [p.O]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]
16:51:45,225 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-0'
16:52:07,365 DEBUG [CachedIntrospectionResults] Getting BeanInfo for class [p.O]
16:52:07,365 DEBUG [CachedIntrospectionResults] Caching PropertyDescriptors for class [p.O]
16:52:07,381 DEBUG [CachedIntrospectionResults] Found property 'class' of type [java.lang.Class]
16:52:07,381 DEBUG [CachedIntrospectionResults] Found property 'i' of type [p.I]
16:52:07,381 DEBUG [CachedIntrospectionResults] Class [p.O] is cache-safe
16:52:53,865 DEBUG [XmlBeanFactory] Eagerly caching bean with name 'bean-0' to allow for resolving potential circular references
16:54:07,553 DEBUG [BeanDefinitionValueResolver] Resolving inner bean definition 'bean-1' of bean 'bean-0'
17:10:41,922 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-1' with merged definition [Root bean: class [p.I]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]
17:10:41,922 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-1'
17:10:41,922 DEBUG [CachedIntrospectionResults] Getting BeanInfo for class [p.I]
17:10:41,922 DEBUG [CachedIntrospectionResults] Caching PropertyDescriptors for class [p.I]
17:10:41,922 DEBUG [CachedIntrospectionResults] Found property 'class' of type [java.lang.Class]
17:10:41,922 DEBUG [CachedIntrospectionResults] Found property 'i' of type [p.I]
17:10:41,922 DEBUG [CachedIntrospectionResults] Class [p.I] is cache-safe
17:10:45,328 DEBUG [BeanDefinitionValueResolver] Resolving inner bean definition 'bean-2' of bean 'bean-1'
17:10:47,141 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-2' with merged definition [Root bean: class [p.I]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]
17:10:47,141 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-2'
17:10:47,141 DEBUG [CachedIntrospectionResults] Using cached introspection results for class [p.I]
17:10:49,719 DEBUG [BeanDefinitionValueResolver] Resolving reference from property 'bean property 'i'' in bean 'bean-2' to bean 'bean-1'
17:10:49,735 DEBUG [CollectionFactory] Creating [java.util.LinkedHashSet]
17:10:51,016 DEBUG [XmlBeanFactory] No bean named 'bean-1' found in org.springframework.beans.factory.xml.XmlBeanFactory defining beans [bean-0]; root of BeanFactory hierarchy
Exception in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-0' defined in resource loaded through InputStream: Cannot create inner bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-1' defined in resource loaded through InputStream: Cannot create inner bean 'bean-2' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined
org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-1' defined in resource loaded through InputStream: Cannot create inner bean 'bean-2' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined
org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined
org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined
at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:360)
at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedBeanDefinition(AbstractBeanFactory.java:686)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:219)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:145)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveReference(BeanDefinitionValueResolver.java:186)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:106)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveInnerBeanDefinition(BeanDefinitionValueResolver.java:151)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:97)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveInnerBeanDefinition(BeanDefinitionValueResolver.java:151)
at org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:97)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:233)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:145)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeansOfType(DefaultListableBeanFactory.java:211)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeansOfType(DefaultListableBeanFactory.java:200)
at p.Main.main(Main.java:17)

---

Is this supposed to happen?

---

**Affects:** 1.2.8
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6853","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6853/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6853/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6853/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6853","id":398067263,"node_id":"MDU6SXNzdWUzOTgwNjcyNjM=","number":6853,"title":"Exception when resolving circular references among nested bean definitions [SPR-2162]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false,"description":"A suggestion or change that we don't feel we should currently apply"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2006-06-20T02:13:40Z","updated_at":"2019-01-13T22:47:34Z","closed_at":"2006-07-05T08:18:46Z","author_association":"COLLABORATOR","body":"**[k d x](https://jira.spring.io/secure/ViewProfile.jspa?name=kdx)** opened **[SPR-2162](https://jira.spring.io/browse/SPR-2162?redirect=false)** and commented\n\nGiven the following Java sources:\n\np/Main.java:\n\n---\n\npackage p;\n\nimport java.io.FileInputStream;\nimport java.io.FileNotFoundException;\nimport java.io.InputStream;\n\nimport org.springframework.beans.factory.xml.XmlBeanFactory;\nimport org.springframework.core.io.InputStreamResource;\n\npublic class Main\n{\n\n    public static void main(String [] args) throws FileNotFoundException\n    {\n        InputStream is1 = new FileInputStream(args[0]);\n        XmlBeanFactory bf = new XmlBeanFactory(new InputStreamResource(is1));\n        O o = (O)bf.getBeansOfType(O.class).values().iterator().next();\n        System.out.println(\"o=\"+o);\n    }\n\n}\n\n---\n\np/O.java:\n\n---\n\npackage p;\n\npublic class O\n{\n\n    private I i;\n    \n    public I getI()\n    {\n    return this.i;\n    }\n    \n    public void setI(I i)\n    {\n    this.i = i;\n    }\n\n}\n\n---\n\np/I.java\n\n---\n\npackage p;\n\npublic class I\n{\n\n    private I i;\n    \n    public I getI()\n    {\n    return this.i;\n    }\n    \n    public void setI(I i)\n    {\n    this.i = i;\n    }\n\n}\n\n---\n\nand the following bean definition (spring-refcycle.xml):\n\n---\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\\<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\"\n\"http://www.springframework.org/dtd/spring-beans.dtd\">\n\\<beans>\n\\<bean id=\"bean-0\" class=\"p.O\">\n\\<property name=\"i\">\n\\<bean id=\"bean-1\" class=\"p.I\">\n\\<property name=\"i\">\n\\<bean id=\"bean-2\" class=\"p.I\">\n\\<property name=\"i\">\\<ref bean=\"bean-1\" />\\</property>\n\\</bean>\n\\</property>\n\\</bean>\n\\</property>\n\\</bean>\n\\</beans>\n\n---\n\nrunning \"Main spring-refcycle.xml\" results in an exception:\n\n---\n\n16:50:38,287 INFO  [CollectionFactory] JDK 1.4+ collections available\n16:50:38,287 DEBUG [CollectionFactory] Creating [java.util.LinkedHashMap]\n16:50:38,334 INFO  [XmlBeanDefinitionReader] Loading XML bean definitions from resource loaded through InputStream\n16:50:38,334 DEBUG [XmlBeanDefinitionReader] Using JAXP implementation [com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl@aeea66]\n16:50:38,334 DEBUG [ResourceEntityResolver] Trying to resolve XML entity with public ID [-//SPRING//DTD BEAN//EN] and system ID http://www.springframework.org/dtd/spring-beans.dtd\n16:50:38,334 DEBUG [ResourceEntityResolver] Trying to locate [spring-beans.dtd] in Spring jar\n16:50:38,365 DEBUG [ResourceEntityResolver] Found beans DTD http://www.springframework.org/dtd/spring-beans.dtd in classpath\n16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Loading bean definitions\n16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default lazy init 'false'\n16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default autowire 'no'\n16:50:38,396 DEBUG [DefaultXmlBeanDefinitionParser] Default dependency check 'none'\n16:50:38,412 DEBUG [DefaultXmlBeanDefinitionParser] Found 1 \\<bean> elements in resource loaded through InputStream\n16:50:38,412 DEBUG [CollectionFactory] Creating [java.util.LinkedHashMap]\n16:51:05,850 DEBUG [XmlBeanFactory] Creating shared instance of singleton bean 'bean-0'\n16:51:41,209 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-0' with merged definition [Root bean: class [p.O]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]\n16:51:45,225 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-0'\n16:52:07,365 DEBUG [CachedIntrospectionResults] Getting BeanInfo for class [p.O]\n16:52:07,365 DEBUG [CachedIntrospectionResults] Caching PropertyDescriptors for class [p.O]\n16:52:07,381 DEBUG [CachedIntrospectionResults] Found property 'class' of type [java.lang.Class]\n16:52:07,381 DEBUG [CachedIntrospectionResults] Found property 'i' of type [p.I]\n16:52:07,381 DEBUG [CachedIntrospectionResults] Class [p.O] is cache-safe\n16:52:53,865 DEBUG [XmlBeanFactory] Eagerly caching bean with name 'bean-0' to allow for resolving potential circular references\n16:54:07,553 DEBUG [BeanDefinitionValueResolver] Resolving inner bean definition 'bean-1' of bean 'bean-0'\n17:10:41,922 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-1' with merged definition [Root bean: class [p.I]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]\n17:10:41,922 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-1'\n17:10:41,922 DEBUG [CachedIntrospectionResults] Getting BeanInfo for class [p.I]\n17:10:41,922 DEBUG [CachedIntrospectionResults] Caching PropertyDescriptors for class [p.I]\n17:10:41,922 DEBUG [CachedIntrospectionResults] Found property 'class' of type [java.lang.Class]\n17:10:41,922 DEBUG [CachedIntrospectionResults] Found property 'i' of type [p.I]\n17:10:41,922 DEBUG [CachedIntrospectionResults] Class [p.I] is cache-safe\n17:10:45,328 DEBUG [BeanDefinitionValueResolver] Resolving inner bean definition 'bean-2' of bean 'bean-1'\n17:10:47,141 DEBUG [XmlBeanFactory] Creating instance of bean 'bean-2' with merged definition [Root bean: class [p.I]; abstract=false; singleton=true; lazyInit=false; autowire=0; dependencyCheck=0; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in resource loaded through InputStream]\n17:10:47,141 DEBUG [XmlBeanFactory] Invoking BeanPostProcessors before instantiation of bean 'bean-2'\n17:10:47,141 DEBUG [CachedIntrospectionResults] Using cached introspection results for class [p.I]\n17:10:49,719 DEBUG [BeanDefinitionValueResolver] Resolving reference from property 'bean property 'i'' in bean 'bean-2' to bean 'bean-1'\n17:10:49,735 DEBUG [CollectionFactory] Creating [java.util.LinkedHashSet]\n17:10:51,016 DEBUG [XmlBeanFactory] No bean named 'bean-1' found in org.springframework.beans.factory.xml.XmlBeanFactory defining beans [bean-0]; root of BeanFactory hierarchy\nException in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-0' defined in resource loaded through InputStream: Cannot create inner bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-1' defined in resource loaded through InputStream: Cannot create inner bean 'bean-2' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined\norg.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-1' defined in resource loaded through InputStream: Cannot create inner bean 'bean-2' while setting bean property 'i'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined\norg.springframework.beans.factory.BeanCreationException: Error creating bean with name 'bean-2' defined in resource loaded through InputStream: Cannot resolve reference to bean 'bean-1' while setting bean property 'i'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined\norg.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'bean-1' is defined\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:360)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getMergedBeanDefinition(AbstractBeanFactory.java:686)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:219)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:145)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveReference(BeanDefinitionValueResolver.java:186)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:106)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveInnerBeanDefinition(BeanDefinitionValueResolver.java:151)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:97)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveInnerBeanDefinition(BeanDefinitionValueResolver.java:151)\nat org.springframework.beans.factory.support.BeanDefinitionValueResolver.resolveValueIfNecessary(BeanDefinitionValueResolver.java:97)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1046)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:857)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:378)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:233)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:145)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeansOfType(DefaultListableBeanFactory.java:211)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeansOfType(DefaultListableBeanFactory.java:200)\nat p.Main.main(Main.java:17)\n\n---\n\nIs this supposed to happen?\n\n---\n\n**Affects:** 1.2.8\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453305902","453305903","453305904","453305905","453305907"], "labels":["in: core","status: declined"]}