{"id":"11743", "title":"Unable to inject a URI with a fragment as a bean property value [SPR-7083]", "body":"**[Olav Reinert](https://jira.spring.io/secure/ViewProfile.jspa?name=oreinert)** opened **[SPR-7083](https://jira.spring.io/browse/SPR-7083?redirect=false)** and commented

If I create a bean with a property of type `java.net.URI`, and try to initialize it in an XML bean definition file, I'm unable to define a URI instance with a fragment in it.

For example, consider the following bean definition

```xml
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<beans xmlns=\"http://www.springframework.org/schema/beans\"
	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
	xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\">

	<bean id=\"bean\" class=\"TestInitURI\">
		<property name=\"uriSpec\" value=\"env://server#3\" />
		<property name=\"uri\" value=\"env://server#3\" />
	</bean>
</beans>
```

I apply this bean definition using the following test program:

```
import java.net.URI;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class TestInitURI {

    private String uriSpec;
    private URI uri;

    public void setUri(URI uri) { this.uri = uri; }
    public void setUriSpec(String uriSpec) { this.uriSpec = uriSpec; }

    public static void main(String[] args) {
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource(\"beans.xml\"));
        TestInitURI bean = beanFactory.getBean(TestInitURI.class);
        show(\"Bean property\", bean.uri);
        show(\"Expected \", URI.create(bean.uriSpec));
    }

    private static void show(String prefix, URI uri) {
        System.out.println(prefix + \" URI \" + uri + \" (host=\" + uri.getHost() + \", authority=\" + uri.getAuthority() + \", path=\"
                + uri.getPath() + \", fragment=\" + uri.getFragment() + \")\");
    }
}
```

The output is the following:

```
Bean property URI env://server%233 (host=null, authority=server#3, path=, fragment=null)
Expected  URI env://server#3 (host=server, authority=server, path=, fragment=3)
```

In other words, I would expect that defining a URI property in an XML bean definition file with a string constant is equivalent to initializing that property with a URI created by invoking `URI.create(String)` with the same string constant.

---

**Affects:** 3.0.2

**Issue Links:**
- #21123 URIEditor should not double escape classpath: URIs

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/03120b70d0c2f953e21fa3ae1b208a9b0f32f646
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11743","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11743/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11743/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11743/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11743","id":398104447,"node_id":"MDU6SXNzdWUzOTgxMDQ0NDc=","number":11743,"title":"Unable to inject a URI with a fragment as a bean property value [SPR-7083]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70","html_url":"https://github.com/spring-projects/spring-framework/milestone/70","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70/labels","id":3960843,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mw==","number":70,"title":"3.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":117,"state":"closed","created_at":"2019-01-10T22:03:16Z","updated_at":"2019-01-11T02:35:54Z","due_on":"2010-06-13T07:00:00Z","closed_at":"2019-01-10T22:03:16Z"},"comments":1,"created_at":"2010-04-08T23:30:37Z","updated_at":"2019-01-11T13:25:10Z","closed_at":"2018-03-12T20:24:20Z","author_association":"COLLABORATOR","body":"**[Olav Reinert](https://jira.spring.io/secure/ViewProfile.jspa?name=oreinert)** opened **[SPR-7083](https://jira.spring.io/browse/SPR-7083?redirect=false)** and commented\n\nIf I create a bean with a property of type `java.net.URI`, and try to initialize it in an XML bean definition file, I'm unable to define a URI instance with a fragment in it.\n\nFor example, consider the following bean definition\n\n```xml\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<beans xmlns=\"http://www.springframework.org/schema/beans\"\n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\txsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\">\n\n\t<bean id=\"bean\" class=\"TestInitURI\">\n\t\t<property name=\"uriSpec\" value=\"env://server#3\" />\n\t\t<property name=\"uri\" value=\"env://server#3\" />\n\t</bean>\n</beans>\n```\n\nI apply this bean definition using the following test program:\n\n```\nimport java.net.URI;\n\nimport org.springframework.beans.factory.BeanFactory;\nimport org.springframework.beans.factory.xml.XmlBeanFactory;\nimport org.springframework.core.io.ClassPathResource;\n\npublic class TestInitURI {\n\n    private String uriSpec;\n    private URI uri;\n\n    public void setUri(URI uri) { this.uri = uri; }\n    public void setUriSpec(String uriSpec) { this.uriSpec = uriSpec; }\n\n    public static void main(String[] args) {\n        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource(\"beans.xml\"));\n        TestInitURI bean = beanFactory.getBean(TestInitURI.class);\n        show(\"Bean property\", bean.uri);\n        show(\"Expected \", URI.create(bean.uriSpec));\n    }\n\n    private static void show(String prefix, URI uri) {\n        System.out.println(prefix + \" URI \" + uri + \" (host=\" + uri.getHost() + \", authority=\" + uri.getAuthority() + \", path=\"\n                + uri.getPath() + \", fragment=\" + uri.getFragment() + \")\");\n    }\n}\n```\n\nThe output is the following:\n\n```\nBean property URI env://server%233 (host=null, authority=server#3, path=, fragment=null)\nExpected  URI env://server#3 (host=server, authority=server, path=, fragment=3)\n```\n\nIn other words, I would expect that defining a URI property in an XML bean definition file with a string constant is equivalent to initializing that property with a URI created by invoking `URI.create(String)` with the same string constant.\n\n---\n\n**Affects:** 3.0.2\n\n**Issue Links:**\n- #21123 URIEditor should not double escape classpath: URIs\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/03120b70d0c2f953e21fa3ae1b208a9b0f32f646\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453349992"], "labels":["in: core","type: bug"]}