{"id":"19662", "title":"Possible performance issue in the generation of JSON in Spring Web Reactive [SPR-15095]", "body":"**[Daniel Fernández](https://jira.spring.io/secure/ViewProfile.jspa?name=dfernandez)** opened **[SPR-15095](https://jira.spring.io/browse/SPR-15095?redirect=false)** and commented

During my tests with the sandbox applications I developed for testing the integration between Thymeleaf and Spring 5 Web Reactive, I found some strange results that might be the symptom of some kind of performance issue in the Spring Web Reactive side, specifically when returning large amounts of JSON.

### Scenario

A web application can return a large amount of entities stored on a JDBC database, both as JSON or as an HTML `<table>`. These _entities_ are quite simple objects (5 properties: 4 strings and 1 integer).

### Implementation

The [thymeleafsandbox-biglist-mvc](https://github.com/thymeleaf/thymeleafsandbox-biglist-mvc/tree/spr15095) and [thymeleafsandbox-biglist-reactive](https://github.com/thymeleaf/thymeleafsandbox-biglist-reactive/tree/spr15095) (both containing a tag named `spr15095`) implement this scenario:

* Database is an in-memory SQLite, accessed through JDBC. The executed query returns 8,715 entries, which are repeated 300 times. Total: 2.6 million entries.
* `thymeleafsandbox-biglist-mvc` implements this scenario using Spring Boot 1.4.3, Spring 4 MVC and Thymeleaf 3.0.
* `thymeleafsandbox-biglist-reactive` implements this scenario using Spring Boot 2.0.0, Spring 5 Reactive and Thymeleaf 3.0.

The MVC application uses Apache Tomcat, the Reactive application uses Netty.

The MVC application returns its data as an `Iterator<PlaylistEntry>`, whereas the Reactive application returns a `Flux<PlaylistEntry>`.

Thymeleaf is configured in the Reactive application to use a maximum _output chunk size_ (i.e. size of the returned `DataBuffer` objects) of 8 KBytes. No explicit configuration of any kind is performed for the output chunk size of the JSON requests.

None of the applications have the Spring Boot _devtools_ enabled. Or should not have (it is not included as a dependency).

Both applications can be easily started with `mvn -U clean compile spring-boot:run`

### Observed JSON results

When the JSON data is requested using `curl`, this is the result obtained for **MVC**:

```
$ curl http://localhost:8080/biglist.json > /dev/null
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  343M    0  343M    0     0   224M      0 --:--:--  0:00:01 --:--:--  224M
```

So 343 Mbytes of JSON in little more than a second, which looks pretty good. But when sending the same request to the **Reactive** application:

```
$ curl http://localhost:8080/biglist.json > /dev/null
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  343M    0  343M    0     0  21.5M      0 --:--:--  0:00:15 --:--:-- 21.3M
```

Same 343 Mbytes of JSON, but the _average download rate_ goes down from 224MB/sec to **less than one tenth** of this, 21.5MB/sec!

Both JSON outputs have been checked to be exactly the same.

### Observed HTML results

These applications allow us to check the same figures for HTML output using Thymeleaf. This should give us an idea of whether the difference observed at the JSON side is entirely to be blamed on the _reactiveness_ of the Netty setup.

In this case Thymeleaf is used to generate HTML output for the same 2.6 Million database entries, using a complete HTML template with a `<table>` containing the data.

For the MVC application:

```
$ curl http://localhost:8080/biglist.thymeleaf > /dev/null
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  460M    0  460M    0     0  33.7M      0 --:--:--  0:00:13 --:--:-- 33.7M
```

Whereas for the Reactive application, using the Thymeleaf _data-driven_ operation mode (Thymeleaf subscribes to the `Flux<PlaylistEntry>` itself):

```
$ curl http://localhost:8080/biglist-datadriven.thymeleaf > /dev/null
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  460M    0  460M    0     0  24.4M      0 --:--:--  0:00:18 --:--:-- 24.5M
```

So note how this time 460 MBytes of HTML are being returned (exactly the same output in both cases), but the difference between MVC and Reactive is much less in the case of Thymeleaf generating HTML: from 33.7 MBytes/sec to 24.4 Mbytes/sec.

So the difference observed in MVC vs Reactive at the _HTML side_ is much, much smaller than what could be observed at JSON.

### Conclusions

If I'm not missing anything important, there **might** be some performance issues affecting the production of JSON in Spring Web Reactive.

These issues might be specific to Netty, but Tomcat has also been tested and, though the results improve, they don't improve _a lot_... so there might be something else.

---

**Affects:** 5.0 M4

**Reference URL:** https://github.com/thymeleaf/thymeleafsandbox-biglist-reactive/tree/spr15095

**Issue Links:**
- #19671 Add support for JSON Streaming
- #19670 Provide a way to enable streaming mode via annotations (and eventually app config)

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/6b9b0230c4de394348c42b6f5964fc66eb659dd3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19662","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19662/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19662/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19662/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19662","id":398203442,"node_id":"MDU6SXNzdWUzOTgyMDM0NDI=","number":19662,"title":"Possible performance issue in the generation of JSON in Spring Web Reactive [SPR-15095]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/176","html_url":"https://github.com/spring-projects/spring-framework/milestone/176","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/176/labels","id":3960949,"node_id":"MDk6TWlsZXN0b25lMzk2MDk0OQ==","number":176,"title":"5.0 M5","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":69,"state":"closed","created_at":"2019-01-10T22:05:23Z","updated_at":"2019-01-11T11:16:24Z","due_on":"2017-02-22T08:00:00Z","closed_at":"2019-01-10T22:05:23Z"},"comments":7,"created_at":"2017-01-04T23:44:47Z","updated_at":"2019-01-11T15:27:26Z","closed_at":"2017-02-23T09:29:12Z","author_association":"COLLABORATOR","body":"**[Daniel Fernández](https://jira.spring.io/secure/ViewProfile.jspa?name=dfernandez)** opened **[SPR-15095](https://jira.spring.io/browse/SPR-15095?redirect=false)** and commented\n\nDuring my tests with the sandbox applications I developed for testing the integration between Thymeleaf and Spring 5 Web Reactive, I found some strange results that might be the symptom of some kind of performance issue in the Spring Web Reactive side, specifically when returning large amounts of JSON.\n\n### Scenario\n\nA web application can return a large amount of entities stored on a JDBC database, both as JSON or as an HTML `<table>`. These _entities_ are quite simple objects (5 properties: 4 strings and 1 integer).\n\n### Implementation\n\nThe [thymeleafsandbox-biglist-mvc](https://github.com/thymeleaf/thymeleafsandbox-biglist-mvc/tree/spr15095) and [thymeleafsandbox-biglist-reactive](https://github.com/thymeleaf/thymeleafsandbox-biglist-reactive/tree/spr15095) (both containing a tag named `spr15095`) implement this scenario:\n\n* Database is an in-memory SQLite, accessed through JDBC. The executed query returns 8,715 entries, which are repeated 300 times. Total: 2.6 million entries.\n* `thymeleafsandbox-biglist-mvc` implements this scenario using Spring Boot 1.4.3, Spring 4 MVC and Thymeleaf 3.0.\n* `thymeleafsandbox-biglist-reactive` implements this scenario using Spring Boot 2.0.0, Spring 5 Reactive and Thymeleaf 3.0.\n\nThe MVC application uses Apache Tomcat, the Reactive application uses Netty.\n\nThe MVC application returns its data as an `Iterator<PlaylistEntry>`, whereas the Reactive application returns a `Flux<PlaylistEntry>`.\n\nThymeleaf is configured in the Reactive application to use a maximum _output chunk size_ (i.e. size of the returned `DataBuffer` objects) of 8 KBytes. No explicit configuration of any kind is performed for the output chunk size of the JSON requests.\n\nNone of the applications have the Spring Boot _devtools_ enabled. Or should not have (it is not included as a dependency).\n\nBoth applications can be easily started with `mvn -U clean compile spring-boot:run`\n\n### Observed JSON results\n\nWhen the JSON data is requested using `curl`, this is the result obtained for **MVC**:\n\n```\n$ curl http://localhost:8080/biglist.json > /dev/null\n  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n100  343M    0  343M    0     0   224M      0 --:--:--  0:00:01 --:--:--  224M\n```\n\nSo 343 Mbytes of JSON in little more than a second, which looks pretty good. But when sending the same request to the **Reactive** application:\n\n```\n$ curl http://localhost:8080/biglist.json > /dev/null\n  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n100  343M    0  343M    0     0  21.5M      0 --:--:--  0:00:15 --:--:-- 21.3M\n```\n\nSame 343 Mbytes of JSON, but the _average download rate_ goes down from 224MB/sec to **less than one tenth** of this, 21.5MB/sec!\n\nBoth JSON outputs have been checked to be exactly the same.\n\n### Observed HTML results\n\nThese applications allow us to check the same figures for HTML output using Thymeleaf. This should give us an idea of whether the difference observed at the JSON side is entirely to be blamed on the _reactiveness_ of the Netty setup.\n\nIn this case Thymeleaf is used to generate HTML output for the same 2.6 Million database entries, using a complete HTML template with a `<table>` containing the data.\n\nFor the MVC application:\n\n```\n$ curl http://localhost:8080/biglist.thymeleaf > /dev/null\n  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n100  460M    0  460M    0     0  33.7M      0 --:--:--  0:00:13 --:--:-- 33.7M\n```\n\nWhereas for the Reactive application, using the Thymeleaf _data-driven_ operation mode (Thymeleaf subscribes to the `Flux<PlaylistEntry>` itself):\n\n```\n$ curl http://localhost:8080/biglist-datadriven.thymeleaf > /dev/null\n  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n100  460M    0  460M    0     0  24.4M      0 --:--:--  0:00:18 --:--:-- 24.5M\n```\n\nSo note how this time 460 MBytes of HTML are being returned (exactly the same output in both cases), but the difference between MVC and Reactive is much less in the case of Thymeleaf generating HTML: from 33.7 MBytes/sec to 24.4 Mbytes/sec.\n\nSo the difference observed in MVC vs Reactive at the _HTML side_ is much, much smaller than what could be observed at JSON.\n\n### Conclusions\n\nIf I'm not missing anything important, there **might** be some performance issues affecting the production of JSON in Spring Web Reactive.\n\nThese issues might be specific to Netty, but Tomcat has also been tested and, though the results improve, they don't improve _a lot_... so there might be something else.\n\n---\n\n**Affects:** 5.0 M4\n\n**Reference URL:** https://github.com/thymeleaf/thymeleafsandbox-biglist-reactive/tree/spr15095\n\n**Issue Links:**\n- #19671 Add support for JSON Streaming\n- #19670 Provide a way to enable streaming mode via annotations (and eventually app config)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/6b9b0230c4de394348c42b6f5964fc66eb659dd3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453450110","453450112","453450114","453450116","453450117","453450119","453450120"], "labels":["in: web","type: enhancement"]}