{"id":"7185", "title":"Hibernate unique constraint : exception badly translated with HSQLDB [SPR-2496]", "body":"**[christophe blin](https://jira.spring.io/secure/ViewProfile.jspa?name=cblin)** opened **[SPR-2496](https://jira.spring.io/browse/SPR-2496?redirect=false)** and commented

Create a business object Node with a unique constraint on the property name.
The following code demonstrate the bug under HSQLDB

NodeServices ns = (NodeServices) this.applicationContext.getBean(\"nodeServices\");

      assertNotNull(ns);
    
    
    
      try {
    
         Node n = new Node();
    
         n.setName(\"first node\");
    
         ns.saveNode(n);
    
         ns.getHibernateTemplate().flush();
        Node n = new Node();
         n.setName(\"first node\");
         ns.saveNode(n);
         ns.getHibernateTemplate().flush();
    
         fail();
    
      } catch (DataIntegrityViolationException e) {
    
         //it is ok, we can not have 2 nodes named \"first node\"
    
      } catch (Exception e) {
    
         //TODO:we should not be here
    
         //NOT WORKING UNDER HSQLDB BUT WORKS WITH MYSQL
    
         //http://forum.springframework.org/showthread.php?t=28462
    
      }



---

**Affects:** 2.0 RC2, 2.0 RC3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7185","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7185/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7185/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7185/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7185","id":398069965,"node_id":"MDU6SXNzdWUzOTgwNjk5NjU=","number":7185,"title":"Hibernate unique constraint : exception badly translated with HSQLDB [SPR-2496]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false,"description":"Issues in data modules (jdbc, orm, oxm, tx)"},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false,"description":"A suggestion or change that we don't feel we should currently apply"}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2006-08-28T02:36:50Z","updated_at":"2019-01-12T16:28:55Z","closed_at":"2006-09-06T09:28:58Z","author_association":"COLLABORATOR","body":"**[christophe blin](https://jira.spring.io/secure/ViewProfile.jspa?name=cblin)** opened **[SPR-2496](https://jira.spring.io/browse/SPR-2496?redirect=false)** and commented\n\nCreate a business object Node with a unique constraint on the property name.\nThe following code demonstrate the bug under HSQLDB\n\nNodeServices ns = (NodeServices) this.applicationContext.getBean(\"nodeServices\");\n\n      assertNotNull(ns);\n    \n    \n    \n      try {\n    \n         Node n = new Node();\n    \n         n.setName(\"first node\");\n    \n         ns.saveNode(n);\n    \n         ns.getHibernateTemplate().flush();\n        Node n = new Node();\n         n.setName(\"first node\");\n         ns.saveNode(n);\n         ns.getHibernateTemplate().flush();\n    \n         fail();\n    \n      } catch (DataIntegrityViolationException e) {\n    \n         //it is ok, we can not have 2 nodes named \"first node\"\n    \n      } catch (Exception e) {\n    \n         //TODO:we should not be here\n    \n         //NOT WORKING UNDER HSQLDB BUT WORKS WITH MYSQL\n    \n         //http://forum.springframework.org/showthread.php?t=28462\n    \n      }\n\n\n\n---\n\n**Affects:** 2.0 RC2, 2.0 RC3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453309069","453309072","453309073","453309075","453309077"], "labels":["in: data","status: declined"]}