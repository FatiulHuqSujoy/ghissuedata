{"id":"8538", "title":"Support of ejb annotations @Entity(name=\"\") @Stateless @Stateful and  @TransactionAttributes out of the box [SPR-3858]", "body":"**[Andrew Bailey](https://jira.spring.io/secure/ViewProfile.jspa?name=hazlorealidad)** opened **[SPR-3858](https://jira.spring.io/browse/SPR-3858?redirect=false)** and commented

It would be great that an EJB aplication could be deployed to a spring container without change and reusing the ejb3 annotations where possible.

Would it be possible to support the ejb3 annotation `@TransactionAttribute` as well as `@Transactional` out of the box
I think its a case of creating a class Ejb3TransactionAttributeSource that extends AnnotationTransactionAttributeSource
and doing some aop but Im not sure how to create the interceptor for it,

also it would be good that `@Entity` , `@Stateless`  and `@Stateful` with and without the name attribute (name=\"...\")
have the same effect as `@Component` (or should that be `@Repository`) also out of the box (see example code below)

A new feature for RC1??

In seam they have another feature that I like, its the use of the same bean in multiple scopes:
http://www.redhat.com/docs/manuals/jboss/jboss-eap-4.2/doc/seam/Seam_Reference_Guide/Seam_annotations-Annotations_for_component_definition.html
`@Role`(name=\"roleName\", scope=ScopeType.SESSION)

`@Roles`({
`@Role`(name=\"user\", scope=ScopeType.CONVERSATION),
`@Role`(name=\"currentUser\", scope=ScopeType.SESSION)
})

would that be practical to implement?

Here is my example that you are welcome to use either as code or in the documentation:

<context:component-scan base-package=\"com.hazlorealidad\"
name-generator=\"com.hazlorealidad.spring.Ejb3NameGenerator\">

\\<!-- \\<context:include-filter type=\"annotation\" expression=\"org.springframework.stereotype.Repository\"/> -->

<context:include-filter type=\"annotation\" expression=\"javax.persistence.Entity\"/>
<context:include-filter type=\"annotation\" expression=\"javax.ejb.Stateful\"/>
<context:include-filter type=\"annotation\" expression=\"javax.ejb.Stateless\"/>
</context:component-scan>

package com.hazlorealidad.spring;
/**
* `@author` Andy Bailey http://www.hazlorealidad.com
  */
  import java.util.HashSet;
  import java.util.Map;
  import java.util.Set;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.core.type.AnnotationMetadata;

public class Ejb3NameGenerator extends AnnotationBeanNameGenerator
{
private static final boolean debug=false;
private static final Set\\<String> annotations;

    static 
    {
    	annotations=new HashSet<String>();
    	annotations.add(\"javax.persistence.Entity\");
    	annotations.add(\"javax.ejb.Stateful\");
    	annotations.add(\"javax.ejb.Stateless\");
    }	
    @Override protected String determineBeanNameFromAnnotation(
    		AnnotatedBeanDefinition definition)
    {
    	String name=super.determineBeanNameFromAnnotation(definition);
    	AnnotationMetadata meta = definition.getMetadata();
    	Set<String> types = meta.getAnnotationTypes();
    	
    	if (debug) System.out.println(\"+++ Annotation Types \"+types);
    	
    	if (name==null) //maintain backward compatibility
    	{
    		for (String annotation:annotations)
    		{
    			//really a map of <String,String> however it could change				
    			Map<String, Object> attrs = meta.getAnnotationAttributes(annotation);
    			if (debug) System.out.println(\"+++ Attrs \"+attrs);
    			if (attrs!=null)
    			{
    				Object object=attrs.get(\"name\");
    				if (object!=null)
    				{
    					name=object.toString();
    					//there never should be multiple annotations so exit loop on first
    					break;
    				}
    			}
    		}
    	}
    	if (debug) System.out.println(\"** determineBeanNameFromAnnotation \"+definition+\" returning \"+name);
    	return name;
    }
    
    
    //Not sure if this is necessary
    @Override protected boolean isStereotypeWithNameValue(String annotationType)
    {
    	boolean result=super.isStereotypeWithNameValue(annotationType);
    	if (!result)
    	{
    		result=annotations.contains(annotationType);
    	}
    	if (debug) System.out.println(\"** isStereotypeWithNameValue \"+annotationType+\" returning \"+result);
    	return result;		
    }	

}


---

**Affects:** 2.1 M3

4 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8538","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8538/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8538/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8538/events","html_url":"https://github.com/spring-projects/spring-framework/issues/8538","id":398080737,"node_id":"MDU6SXNzdWUzOTgwODA3Mzc=","number":8538,"title":"Support of ejb annotations @Entity(name=\"\") @Stateless @Stateful and  @TransactionAttributes out of the box [SPR-3858]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2007-09-10T07:59:58Z","updated_at":"2018-12-11T15:37:08Z","closed_at":"2018-12-11T15:37:08Z","author_association":"COLLABORATOR","body":"**[Andrew Bailey](https://jira.spring.io/secure/ViewProfile.jspa?name=hazlorealidad)** opened **[SPR-3858](https://jira.spring.io/browse/SPR-3858?redirect=false)** and commented\n\nIt would be great that an EJB aplication could be deployed to a spring container without change and reusing the ejb3 annotations where possible.\n\nWould it be possible to support the ejb3 annotation `@TransactionAttribute` as well as `@Transactional` out of the box\r\nI think its a case of creating a class Ejb3TransactionAttributeSource that extends AnnotationTransactionAttributeSource\r\nand doing some aop but Im not sure how to create the interceptor for it,\n\nalso it would be good that `@Entity` , `@Stateless`  and `@Stateful` with and without the name attribute (name=\"...\")\r\nhave the same effect as `@Component` (or should that be `@Repository`) also out of the box (see example code below)\n\nA new feature for RC1??\n\nIn seam they have another feature that I like, its the use of the same bean in multiple scopes:\r\nhttp://www.redhat.com/docs/manuals/jboss/jboss-eap-4.2/doc/seam/Seam_Reference_Guide/Seam_annotations-Annotations_for_component_definition.html\r\n`@Role`(name=\"roleName\", scope=ScopeType.SESSION)\n\n`@Roles`({\r\n`@Role`(name=\"user\", scope=ScopeType.CONVERSATION),\r\n`@Role`(name=\"currentUser\", scope=ScopeType.SESSION)\r\n})\n\nwould that be practical to implement?\n\nHere is my example that you are welcome to use either as code or in the documentation:\n\n<context:component-scan base-package=\"com.hazlorealidad\"\r\nname-generator=\"com.hazlorealidad.spring.Ejb3NameGenerator\">\n\n\\<!-- \\<context:include-filter type=\"annotation\" expression=\"org.springframework.stereotype.Repository\"/> -->\r\n\n<context:include-filter type=\"annotation\" expression=\"javax.persistence.Entity\"/>\r\n<context:include-filter type=\"annotation\" expression=\"javax.ejb.Stateful\"/>\r\n<context:include-filter type=\"annotation\" expression=\"javax.ejb.Stateless\"/>\r\n</context:component-scan>\n\npackage com.hazlorealidad.spring;\r\n/**\n* `@author` Andy Bailey http://www.hazlorealidad.com\r\n  */\r\n  import java.util.HashSet;\r\n  import java.util.Map;\r\n  import java.util.Set;\n\nimport org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;\r\nimport org.springframework.context.annotation.AnnotationBeanNameGenerator;\r\nimport org.springframework.core.type.AnnotationMetadata;\n\npublic class Ejb3NameGenerator extends AnnotationBeanNameGenerator\r\n{\r\nprivate static final boolean debug=false;\r\nprivate static final Set\\<String> annotations;\n\n    static \r\n    {\r\n    \tannotations=new HashSet<String>();\r\n    \tannotations.add(\"javax.persistence.Entity\");\r\n    \tannotations.add(\"javax.ejb.Stateful\");\r\n    \tannotations.add(\"javax.ejb.Stateless\");\r\n    }\t\r\n    @Override protected String determineBeanNameFromAnnotation(\r\n    \t\tAnnotatedBeanDefinition definition)\r\n    {\r\n    \tString name=super.determineBeanNameFromAnnotation(definition);\r\n    \tAnnotationMetadata meta = definition.getMetadata();\r\n    \tSet<String> types = meta.getAnnotationTypes();\r\n    \t\r\n    \tif (debug) System.out.println(\"+++ Annotation Types \"+types);\r\n    \t\r\n    \tif (name==null) //maintain backward compatibility\r\n    \t{\r\n    \t\tfor (String annotation:annotations)\r\n    \t\t{\r\n    \t\t\t//really a map of <String,String> however it could change\t\t\t\t\r\n    \t\t\tMap<String, Object> attrs = meta.getAnnotationAttributes(annotation);\r\n    \t\t\tif (debug) System.out.println(\"+++ Attrs \"+attrs);\r\n    \t\t\tif (attrs!=null)\r\n    \t\t\t{\r\n    \t\t\t\tObject object=attrs.get(\"name\");\r\n    \t\t\t\tif (object!=null)\r\n    \t\t\t\t{\r\n    \t\t\t\t\tname=object.toString();\r\n    \t\t\t\t\t//there never should be multiple annotations so exit loop on first\r\n    \t\t\t\t\tbreak;\r\n    \t\t\t\t}\r\n    \t\t\t}\r\n    \t\t}\r\n    \t}\r\n    \tif (debug) System.out.println(\"** determineBeanNameFromAnnotation \"+definition+\" returning \"+name);\r\n    \treturn name;\r\n    }\r\n    \r\n    \r\n    //Not sure if this is necessary\r\n    @Override protected boolean isStereotypeWithNameValue(String annotationType)\r\n    {\r\n    \tboolean result=super.isStereotypeWithNameValue(annotationType);\r\n    \tif (!result)\r\n    \t{\r\n    \t\tresult=annotations.contains(annotationType);\r\n    \t}\r\n    \tif (debug) System.out.println(\"** isStereotypeWithNameValue \"+annotationType+\" returning \"+result);\r\n    \treturn result;\t\t\r\n    }\t\r\n\n}\n\n\n---\n\n**Affects:** 2.1 M3\n\n4 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453321445","453321447","453321448"], "labels":["in: core","status: declined","type: enhancement"]}