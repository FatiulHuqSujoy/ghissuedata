{"id":"11227", "title":"Double-to-Integer number coercion in ApplicationContext fails on Windows XP [SPR-6561]", "body":"**[Sam Brannen](https://jira.spring.io/secure/ViewProfile.jspa?name=sbrannen)** opened **[SPR-6561](https://jira.spring.io/browse/SPR-6561?redirect=false)** and commented

The following code passes both on the Mac and Windows XP:

```
public static void main(String[] args) {
    ExpressionParser parser = new SpelExpressionParser();
    Expression exp = parser.parseExpression(\"T(java.lang.Math).random() * 10.0\");
    Integer randomNumber = exp.getValue(Integer.class);
    System.out.println(\"Random number = \" + randomNumber);
    if (randomNumber < 0 || randomNumber > 10) {
        System.err.println(\"Random number is out of bounds: \" + randomNumber);
    }
}
```

When similar SpEL related code is introduced into the ApplicationContext, however, there is a discrepancy between the behavior on Mac and Windows. For example, given the following configuration and test case, the test case passes on the Mac but fails on Windows with a NumberFormatException (e.g., NumberFormatException: For input string: \"1.23456789\").

```
<bean id=\"randomNumber\" class=\"java.lang.Integer\">
    <constructor-arg value=\"#{ T(java.lang.Math).random() * 10.0 }\" />
</bean>
```

```
@Test
public void spelInXmlBeanDefinition() {
    ApplicationContext appCtx = new ClassPathXmlApplicationContext(\"context.xml\");
    Integer randomNumber = appCtx.getBean(\"randomNumber\", Integer.class);
    assertNotNull(randomNumber);
    assertTrue(randomNumber >= 0 && randomNumber <= 10);
}
```

Thus, the type coercion from Double to Integer does not occur on Windows XP.

But... if you specify type=\"int\" for the constructor argument as in the following snippet, the code works as expected on both Mac and Windows XP.

```
<bean id=\"randomNumber\" class=\"java.lang.Integer\">
    <constructor-arg value=\"#{ T(java.lang.Math).random() * 10.0 }\" type=\"int\" />
</bean>
```

Is the automatic type coercion experienced on the Mac to be expected, or is the type=\"int\" declaration required on all platforms?


---

**Affects:** 3.0 RC3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11227","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11227/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11227/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11227/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11227","id":398100877,"node_id":"MDU6SXNzdWUzOTgxMDA4Nzc=","number":11227,"title":"Double-to-Integer number coercion in ApplicationContext fails on Windows XP [SPR-6561]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2009-12-13T21:38:28Z","updated_at":"2015-09-22T17:34:36Z","closed_at":"2015-09-22T17:34:36Z","author_association":"COLLABORATOR","body":"**[Sam Brannen](https://jira.spring.io/secure/ViewProfile.jspa?name=sbrannen)** opened **[SPR-6561](https://jira.spring.io/browse/SPR-6561?redirect=false)** and commented\n\nThe following code passes both on the Mac and Windows XP:\n\n```\npublic static void main(String[] args) {\n    ExpressionParser parser = new SpelExpressionParser();\n    Expression exp = parser.parseExpression(\"T(java.lang.Math).random() * 10.0\");\n    Integer randomNumber = exp.getValue(Integer.class);\n    System.out.println(\"Random number = \" + randomNumber);\n    if (randomNumber < 0 || randomNumber > 10) {\n        System.err.println(\"Random number is out of bounds: \" + randomNumber);\n    }\n}\n```\n\nWhen similar SpEL related code is introduced into the ApplicationContext, however, there is a discrepancy between the behavior on Mac and Windows. For example, given the following configuration and test case, the test case passes on the Mac but fails on Windows with a NumberFormatException (e.g., NumberFormatException: For input string: \"1.23456789\").\n\n```\n<bean id=\"randomNumber\" class=\"java.lang.Integer\">\n    <constructor-arg value=\"#{ T(java.lang.Math).random() * 10.0 }\" />\n</bean>\n```\n\n```\n@Test\npublic void spelInXmlBeanDefinition() {\n    ApplicationContext appCtx = new ClassPathXmlApplicationContext(\"context.xml\");\n    Integer randomNumber = appCtx.getBean(\"randomNumber\", Integer.class);\n    assertNotNull(randomNumber);\n    assertTrue(randomNumber >= 0 && randomNumber <= 10);\n}\n```\n\nThus, the type coercion from Double to Integer does not occur on Windows XP.\n\nBut... if you specify type=\"int\" for the constructor argument as in the following snippet, the code works as expected on both Mac and Windows XP.\n\n```\n<bean id=\"randomNumber\" class=\"java.lang.Integer\">\n    <constructor-arg value=\"#{ T(java.lang.Math).random() * 10.0 }\" type=\"int\" />\n</bean>\n```\n\nIs the automatic type coercion experienced on the Mac to be expected, or is the type=\"int\" declaration required on all platforms?\n\n\n---\n\n**Affects:** 3.0 RC3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453345601","453345602","453345603","453345604"], "labels":["in: core","status: declined","type: enhancement"]}