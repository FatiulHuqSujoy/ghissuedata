{"id":"14281", "title":"Forwarding sets locale and character encoding. [SPR-9647]", "body":"**[Martin Becker](https://jira.spring.io/secure/ViewProfile.jspa?name=mgbckr)** opened **[SPR-9647](https://jira.spring.io/browse/SPR-9647?redirect=false)** and commented

Hi everyone,

so I was having trouble with the character encoding of a JSON object returned by a REST interface implemented in Spring MVC.

Accessing the interface using a forwarded URL (the forwarding was also done via Spring's forwarding capabilities, i.e. return \"forward:url\") would always set a wrong character encoding, i.e., ISO-8859-1 (VS setting no character encoding when accessed directly).

Example:
**/some/forwarded/url** would return this header

```
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=57F7A6ECFDDEA925D2565E67A4B718CD; Path=/servlet-root/; HttpOnly
Pragma: no-cache
Cache-Control: no-cache,no-store
Expires: Wed, 31 Dec 1969 23:59:59 GMT
Last-Modified: Thu, 01 Jan 1970 00:00:00 GMT
Content-Type: application/json;charset=ISO-8859-1
Content-Language: en-US
Transfer-Encoding: chunked
Date: Wed, 25 Jul 2012 13:58:45 GMT
```

while **/the/original/url** would return this header

```
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=FC01F1AABA8DD80A420BE0C70E2B422C; Path=/ubicon-webapp/; HttpOnly
Pragma: no-cache
Cache-Control: no-cache,no-store
Expires: Wed, 31 Dec 1969 23:59:59 GMT
Last-Modified: Thu, 01 Jan 1970 00:00:00 GMT
Content-Type: application/json
Transfer-Encoding: chunked
Date: Wed, 25 Jul 2012 14:18:47 GMT
```

The redirect happens in a controller like this:

```
@RequestMapping(\"/some/forwarded/url\")
public String handle() {
  return \"forward:/the/original/url\";
}
```

The \"problem\" lies with in the forwarding handler returning a view to render. Rendering the DispatcherServlet will assign a locale which on the other hand assigns a character encoding. For details and workarounds, please see http://fstyle.de/hp_fstyle/wordpress/2012/07/22/spring-3-0-5-responsebody-and-json/

I would consider this unexpected behaviour. Please correct me if I am wrong.


---

**Affects:** 3.0.5

**Reference URL:** http://fstyle.de/hp_fstyle/wordpress/2012/07/22/spring-3-0-5-responsebody-and-json/
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14281","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14281/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14281/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14281/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14281","id":398152179,"node_id":"MDU6SXNzdWUzOTgxNTIxNzk=","number":14281,"title":"Forwarding sets locale and character encoding. [SPR-9647]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2012-07-27T02:49:23Z","updated_at":"2019-01-12T16:36:09Z","closed_at":"2012-07-30T06:25:22Z","author_association":"COLLABORATOR","body":"**[Martin Becker](https://jira.spring.io/secure/ViewProfile.jspa?name=mgbckr)** opened **[SPR-9647](https://jira.spring.io/browse/SPR-9647?redirect=false)** and commented\n\nHi everyone,\n\nso I was having trouble with the character encoding of a JSON object returned by a REST interface implemented in Spring MVC.\n\nAccessing the interface using a forwarded URL (the forwarding was also done via Spring's forwarding capabilities, i.e. return \"forward:url\") would always set a wrong character encoding, i.e., ISO-8859-1 (VS setting no character encoding when accessed directly).\n\nExample:\n**/some/forwarded/url** would return this header\n\n```\nHTTP/1.1 200 OK\nServer: Apache-Coyote/1.1\nSet-Cookie: JSESSIONID=57F7A6ECFDDEA925D2565E67A4B718CD; Path=/servlet-root/; HttpOnly\nPragma: no-cache\nCache-Control: no-cache,no-store\nExpires: Wed, 31 Dec 1969 23:59:59 GMT\nLast-Modified: Thu, 01 Jan 1970 00:00:00 GMT\nContent-Type: application/json;charset=ISO-8859-1\nContent-Language: en-US\nTransfer-Encoding: chunked\nDate: Wed, 25 Jul 2012 13:58:45 GMT\n```\n\nwhile **/the/original/url** would return this header\n\n```\nHTTP/1.1 200 OK\nServer: Apache-Coyote/1.1\nSet-Cookie: JSESSIONID=FC01F1AABA8DD80A420BE0C70E2B422C; Path=/ubicon-webapp/; HttpOnly\nPragma: no-cache\nCache-Control: no-cache,no-store\nExpires: Wed, 31 Dec 1969 23:59:59 GMT\nLast-Modified: Thu, 01 Jan 1970 00:00:00 GMT\nContent-Type: application/json\nTransfer-Encoding: chunked\nDate: Wed, 25 Jul 2012 14:18:47 GMT\n```\n\nThe redirect happens in a controller like this:\n\n```\n@RequestMapping(\"/some/forwarded/url\")\npublic String handle() {\n  return \"forward:/the/original/url\";\n}\n```\n\nThe \"problem\" lies with in the forwarding handler returning a view to render. Rendering the DispatcherServlet will assign a locale which on the other hand assigns a character encoding. For details and workarounds, please see http://fstyle.de/hp_fstyle/wordpress/2012/07/22/spring-3-0-5-responsebody-and-json/\n\nI would consider this unexpected behaviour. Please correct me if I am wrong.\n\n\n---\n\n**Affects:** 3.0.5\n\n**Reference URL:** http://fstyle.de/hp_fstyle/wordpress/2012/07/22/spring-3-0-5-responsebody-and-json/\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453395906","453395907","453395908"], "labels":["in: web","status: declined"]}