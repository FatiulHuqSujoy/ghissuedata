{"id":"5355", "title":"Missing dependencies in project.xml and project.properties [SPR-627]", "body":"**[Magnus Heino](https://jira.spring.io/secure/ViewProfile.jspa?name=magnus)** opened **[SPR-627](https://jira.spring.io/browse/SPR-627?redirect=false)** and commented

Index: project.properties

---

RCS file: /cvsroot/springframework/spring/project.properties,v
retrieving revision 1.65
diff -u -r1.65 project.properties
--- project.properties	20 Dec 2004 12:06:59 -0000	1.65
+++ project.properties	15 Jan 2005 12:23:57 -0000
@@ -172,11 +172,10 @@
maven.jar.concurrent = ${basedir}/lib/concurrent/concurrent-1.3.4.jar
maven.jar.cos = ${basedir}/lib/cos/cos.jar
maven.jar.freemarker = ${basedir}/lib/freemarker/freemarker.jar
-maven.jar.ibatis-common = ${basedir}/lib/ibatis/ibatis-common.jar
maven.jar.ibatis-sqlmap = ${basedir}/lib/ibatis/ibatis-sqlmap.jar
maven.jar.ibatis-sqlmap-2 = ${basedir}/lib/ibatis/ibatis-sqlmap-2.jar
-maven.jar.itext = ${basedir}/lib/itext/itext-1.02b.jar
-maven.jar.jasperreports = ${basedir}/lib/jasperreports/jasperreports-0.6.2.jar
+maven.jar.itext = ${basedir}/lib/itext/itext-1.1.4.jar
+maven.jar.jasperreports = ${basedir}/lib/jasperreports/jasperreports-0.6.3.jar
maven.jar.jdo = ${basedir}/lib/jdo/jdo.jar
maven.jar.jms = ${basedir}/lib/j2ee/jms.jar
maven.jar.ehcache = ${basedir}/lib/ehcache/ehcache-1.0.jar
@@ -187,7 +186,7 @@
maven.jar.j2ee-management = ${basedir}/lib/j2ee/j2ee-management.jar
maven.jar.jsf = ${basedir}/lib/jsf/jsf-api.jar
maven.jar.jotm = ${basedir}/lib/jotm/jotm.jar
-maven.jar.db-ojb = ${basedir}/lib/ojb/db-ojb-1.0.0.jar
+maven.jar.db-ojb = ${basedir}/lib/ojb/db-ojb-1.0.1.jar
maven.jar.quartz = ${basedir}/lib/quartz/quartz.jar
maven.jar.xapool = ${basedir}/lib/jotm/xapool.jar
maven.jar.jaxrpc = ${basedir}/lib/j2ee/jaxrpc.jar
Index: project.xml

---

RCS file: /cvsroot/springframework/spring/project.xml,v
retrieving revision 1.57
diff -u -r1.57 project.xml
--- project.xml	19 Dec 2004 21:11:06 -0000	1.57
+++ project.xml	15 Jan 2005 12:23:57 -0000
@@ -341,11 +341,6 @@
\\</dependency>
\\<dependency>
\\<groupId>ibatis\\</groupId>

---

    <artifactId>ibatis-common</artifactId>

- 

      <version>1.3.1</version>

- 

      </dependency>

- 

      <dependency>

- 

           <groupId>ibatis</groupId>
           <artifactId>ibatis-sqlmap</artifactId>
           <version>1.3.1</version>
      </dependency>

@@ -356,7 +351,7 @@
\\</dependency>
\\<dependency>
\\<id>itext\\</id>

---

    <version>1.02b</version>

+ 

           <version>1.1.4</version>
      </dependency>
      <dependency>
           <groupId>jamon</groupId>

@@ -366,7 +361,7 @@
\\<dependency>
\\<groupId>jasperreports\\</groupId>
\\<artifactId>jasperreports\\</artifactId>

---

    <version>0.6.2</version>

+ 

           <version>0.6.3</version>
      </dependency>
      <dependency>
           <id>jdom</id>

---

**Affects:** 1.1.4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5355","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5355/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5355/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5355/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5355","id":398054273,"node_id":"MDU6SXNzdWUzOTgwNTQyNzM=","number":5355,"title":"Missing dependencies in project.xml and project.properties [SPR-627]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/14","html_url":"https://github.com/spring-projects/spring-framework/milestone/14","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/14/labels","id":3960784,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4NA==","number":14,"title":"1.1.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":73,"state":"closed","created_at":"2019-01-10T22:02:08Z","updated_at":"2019-01-10T23:06:29Z","due_on":null,"closed_at":"2019-01-10T22:02:08Z"},"comments":1,"created_at":"2005-01-14T22:30:08Z","updated_at":"2019-01-13T22:52:54Z","closed_at":"2005-01-15T02:16:49Z","author_association":"COLLABORATOR","body":"**[Magnus Heino](https://jira.spring.io/secure/ViewProfile.jspa?name=magnus)** opened **[SPR-627](https://jira.spring.io/browse/SPR-627?redirect=false)** and commented\n\nIndex: project.properties\n\n---\n\nRCS file: /cvsroot/springframework/spring/project.properties,v\nretrieving revision 1.65\ndiff -u -r1.65 project.properties\n--- project.properties\t20 Dec 2004 12:06:59 -0000\t1.65\n+++ project.properties\t15 Jan 2005 12:23:57 -0000\n@@ -172,11 +172,10 @@\nmaven.jar.concurrent = ${basedir}/lib/concurrent/concurrent-1.3.4.jar\nmaven.jar.cos = ${basedir}/lib/cos/cos.jar\nmaven.jar.freemarker = ${basedir}/lib/freemarker/freemarker.jar\n-maven.jar.ibatis-common = ${basedir}/lib/ibatis/ibatis-common.jar\nmaven.jar.ibatis-sqlmap = ${basedir}/lib/ibatis/ibatis-sqlmap.jar\nmaven.jar.ibatis-sqlmap-2 = ${basedir}/lib/ibatis/ibatis-sqlmap-2.jar\n-maven.jar.itext = ${basedir}/lib/itext/itext-1.02b.jar\n-maven.jar.jasperreports = ${basedir}/lib/jasperreports/jasperreports-0.6.2.jar\n+maven.jar.itext = ${basedir}/lib/itext/itext-1.1.4.jar\n+maven.jar.jasperreports = ${basedir}/lib/jasperreports/jasperreports-0.6.3.jar\nmaven.jar.jdo = ${basedir}/lib/jdo/jdo.jar\nmaven.jar.jms = ${basedir}/lib/j2ee/jms.jar\nmaven.jar.ehcache = ${basedir}/lib/ehcache/ehcache-1.0.jar\n@@ -187,7 +186,7 @@\nmaven.jar.j2ee-management = ${basedir}/lib/j2ee/j2ee-management.jar\nmaven.jar.jsf = ${basedir}/lib/jsf/jsf-api.jar\nmaven.jar.jotm = ${basedir}/lib/jotm/jotm.jar\n-maven.jar.db-ojb = ${basedir}/lib/ojb/db-ojb-1.0.0.jar\n+maven.jar.db-ojb = ${basedir}/lib/ojb/db-ojb-1.0.1.jar\nmaven.jar.quartz = ${basedir}/lib/quartz/quartz.jar\nmaven.jar.xapool = ${basedir}/lib/jotm/xapool.jar\nmaven.jar.jaxrpc = ${basedir}/lib/j2ee/jaxrpc.jar\nIndex: project.xml\n\n---\n\nRCS file: /cvsroot/springframework/spring/project.xml,v\nretrieving revision 1.57\ndiff -u -r1.57 project.xml\n--- project.xml\t19 Dec 2004 21:11:06 -0000\t1.57\n+++ project.xml\t15 Jan 2005 12:23:57 -0000\n@@ -341,11 +341,6 @@\n\\</dependency>\n\\<dependency>\n\\<groupId>ibatis\\</groupId>\n\n---\n\n    <artifactId>ibatis-common</artifactId>\n\n- \n\n      <version>1.3.1</version>\n\n- \n\n      </dependency>\n\n- \n\n      <dependency>\n\n- \n\n           <groupId>ibatis</groupId>\n           <artifactId>ibatis-sqlmap</artifactId>\n           <version>1.3.1</version>\n      </dependency>\n\n@@ -356,7 +351,7 @@\n\\</dependency>\n\\<dependency>\n\\<id>itext\\</id>\n\n---\n\n    <version>1.02b</version>\n\n+ \n\n           <version>1.1.4</version>\n      </dependency>\n      <dependency>\n           <groupId>jamon</groupId>\n\n@@ -366,7 +361,7 @@\n\\<dependency>\n\\<groupId>jasperreports\\</groupId>\n\\<artifactId>jasperreports\\</artifactId>\n\n---\n\n    <version>0.6.2</version>\n\n+ \n\n           <version>0.6.3</version>\n      </dependency>\n      <dependency>\n           <id>jdom</id>\n\n---\n\n**Affects:** 1.1.4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453290988"], "labels":["type: bug"]}