{"id":"23907", "title":"useSuffixPatternMatch ignored if pathPrefix is also used", "body":"Affected version: 5.1.8

I am trying to disable suffix pattern matching so that PathVariables containing dots will not be truncated. The [documented solution](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-ann-requestmapping-suffix-pattern-match) for this problem

`webMvcConfigurer.setUseSuffixPatternMatch(false)`

works fine, but it breaks if I also try to add a path prefix

`webMvcConfigurer.addPathPrefix(\"/api\", HandlerTypePredicate.forAnnotation(RestController.class))`

The problem is in PatternsRequestCondition.combine(PatternRequestCondition other) at line 179. Here the useSuffixPatternMatch on my url is ignored in favor of the value on the '/api' Condition create at RequestMappingHandlerMapping.228

RequestMappingHandlerMapping.java
```java
@Override
@Nullable
protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
		RequestMappingInfo info = createRequestMappingInfo(method);
	if (info != null) {
		RequestMappingInfo typeInfo = createRequestMappingInfo(handlerType);
		if (typeInfo != null) {
			info = typeInfo.combine(info);
		}
		String prefix = getPathPrefix(handlerType);
		if (prefix != null) {
                        // PROBLEM HERE
			info = RequestMappingInfo.paths(prefix).build().combine(info);**
		}
	}
	return info;
}
```
PatternsRequestCondition.java
```java
@Override
public PatternsRequestCondition combine(PatternsRequestCondition other) {
	Set<String> result = new LinkedHashSet<>();
	if (!this.patterns.isEmpty() && !other.patterns.isEmpty()) {
		for (String pattern1 : this.patterns) {
			for (String pattern2 : other.patterns) {
				result.add(this.pathMatcher.combine(pattern1, pattern2));
			}
		}
	}
	else if (!this.patterns.isEmpty()) {
		result.addAll(this.patterns);
	}
	else if (!other.patterns.isEmpty()) {
		result.addAll(other.patterns);
	}
	else {
		result.add(\"\");
	}
        // PROBLEM HERE
	return new PatternsRequestCondition(result, this.pathHelper, this.pathMatcher,
			this.useSuffixPatternMatch, this.useTrailingSlashMatch, this.fileExtensions);
}
`", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23907","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23907/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23907/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23907/events","html_url":"https://github.com/spring-projects/spring-framework/issues/23907","id":516082388,"node_id":"MDU6SXNzdWU1MTYwODIzODg=","number":23907,"title":"useSuffixPatternMatch ignored if pathPrefix is also used","user":{"login":"tpischke-bedag","id":42371268,"node_id":"MDQ6VXNlcjQyMzcxMjY4","avatar_url":"https://avatars3.githubusercontent.com/u/42371268?v=4","gravatar_id":"","url":"https://api.github.com/users/tpischke-bedag","html_url":"https://github.com/tpischke-bedag","followers_url":"https://api.github.com/users/tpischke-bedag/followers","following_url":"https://api.github.com/users/tpischke-bedag/following{/other_user}","gists_url":"https://api.github.com/users/tpischke-bedag/gists{/gist_id}","starred_url":"https://api.github.com/users/tpischke-bedag/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tpischke-bedag/subscriptions","organizations_url":"https://api.github.com/users/tpischke-bedag/orgs","repos_url":"https://api.github.com/users/tpischke-bedag/repos","events_url":"https://api.github.com/users/tpischke-bedag/events{/privacy}","received_events_url":"https://api.github.com/users/tpischke-bedag/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/227","html_url":"https://github.com/spring-projects/spring-framework/milestone/227","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/227/labels","id":4791493,"node_id":"MDk6TWlsZXN0b25lNDc5MTQ5Mw==","number":227,"title":"5.2.2","description":"","creator":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"open_issues":21,"closed_issues":23,"state":"open","created_at":"2019-10-28T15:59:42Z","updated_at":"2019-11-11T11:29:51Z","due_on":"2019-11-28T08:00:00Z","closed_at":null},"comments":1,"created_at":"2019-11-01T12:28:20Z","updated_at":"2019-11-06T21:06:36Z","closed_at":"2019-11-06T17:54:52Z","author_association":"NONE","body":"Affected version: 5.1.8\r\n\r\nI am trying to disable suffix pattern matching so that PathVariables containing dots will not be truncated. The [documented solution](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-ann-requestmapping-suffix-pattern-match) for this problem\r\n\r\n`webMvcConfigurer.setUseSuffixPatternMatch(false)`\r\n\r\nworks fine, but it breaks if I also try to add a path prefix\r\n\r\n`webMvcConfigurer.addPathPrefix(\"/api\", HandlerTypePredicate.forAnnotation(RestController.class))`\r\n\r\nThe problem is in PatternsRequestCondition.combine(PatternRequestCondition other) at line 179. Here the useSuffixPatternMatch on my url is ignored in favor of the value on the '/api' Condition create at RequestMappingHandlerMapping.228\r\n\r\nRequestMappingHandlerMapping.java\r\n```java\r\n@Override\r\n@Nullable\r\nprotected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {\r\n\t\tRequestMappingInfo info = createRequestMappingInfo(method);\r\n\tif (info != null) {\r\n\t\tRequestMappingInfo typeInfo = createRequestMappingInfo(handlerType);\r\n\t\tif (typeInfo != null) {\r\n\t\t\tinfo = typeInfo.combine(info);\r\n\t\t}\r\n\t\tString prefix = getPathPrefix(handlerType);\r\n\t\tif (prefix != null) {\r\n                        // PROBLEM HERE\r\n\t\t\tinfo = RequestMappingInfo.paths(prefix).build().combine(info);**\r\n\t\t}\r\n\t}\r\n\treturn info;\r\n}\r\n```\r\nPatternsRequestCondition.java\r\n```java\r\n@Override\r\npublic PatternsRequestCondition combine(PatternsRequestCondition other) {\r\n\tSet<String> result = new LinkedHashSet<>();\r\n\tif (!this.patterns.isEmpty() && !other.patterns.isEmpty()) {\r\n\t\tfor (String pattern1 : this.patterns) {\r\n\t\t\tfor (String pattern2 : other.patterns) {\r\n\t\t\t\tresult.add(this.pathMatcher.combine(pattern1, pattern2));\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n\telse if (!this.patterns.isEmpty()) {\r\n\t\tresult.addAll(this.patterns);\r\n\t}\r\n\telse if (!other.patterns.isEmpty()) {\r\n\t\tresult.addAll(other.patterns);\r\n\t}\r\n\telse {\r\n\t\tresult.add(\"\");\r\n\t}\r\n        // PROBLEM HERE\r\n\treturn new PatternsRequestCondition(result, this.pathHelper, this.pathMatcher,\r\n\t\t\tthis.useSuffixPatternMatch, this.useTrailingSlashMatch, this.fileExtensions);\r\n}\r\n`","closed_by":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}}", "commentIds":["549308983"], "labels":["in: web","status: backported","type: bug"]}