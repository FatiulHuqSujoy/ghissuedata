{"id":"12552", "title":"Calling a @RequestMapping annotated method with a URI template with three variables does not work [SPR-7896]", "body":"**[Florian Rampp](https://jira.spring.io/secure/ViewProfile.jspa?name=jack_kerouac)** opened **[SPR-7896](https://jira.spring.io/browse/SPR-7896?redirect=false)** and commented

Assume the following method definitions in an `@Controller` annotated class:

```
@RequestMapping(value = \"/x/**\")
@ResponseBody
public String doSomething0() {
	return \"wild card\";
}

@RequestMapping(value = \"/x/{b}/{c}/{d}\")
@ResponseBody
public String doSomething1(@PathVariable(\"b\") String b, @PathVariable(\"c\") String c, @PathVariable(\"d\") String d) {
	return \"three params\";
}

@RequestMapping(value = \"/x/{b}/{c}\")
@ResponseBody
public String doSomething2(@PathVariable(\"b\") String b, @PathVariable(\"c\") String c) {
	return \"two params\";
}

@RequestMapping(value = \"/x/{b}\")
@ResponseBody
public String doSomething3(@PathVariable(\"b\") String b) {
	return \"one param\";
}
{/code}

If I issue a HTTP GET to http://localhost:8080/x, I receive \"wild card\".
If I issue a HTTP GET to http://localhost:8080/x/1, I receive \"one param\".
If I issue a HTTP GET to http://localhost:8080/x/1/2, I receive \"two params\".
*If I issue a HTTP GET to http://localhost:8080/x/1/2/3, I receive \"wild card\"!*
But I expect the method doSomething3 to be invoked and thus to receive \"three params\".

I think this is a bug in the method handler resolution. The most specific definition should be taken, which works for the cases with one and two URI template variables, but not for the case with three variables. At least it is inconsistent behavior.

If I omit the method doSomething0 with the wildcard URI definition, a HTTP GET to http://localhost:8080/x/1/2/3 returns \"three params\".
```

---

**Affects:** 3.0.5

**Issue Links:**
- #11407 RequestMapping with 3 PathVariable doesn't work (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12552","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12552/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12552/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12552/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12552","id":398109757,"node_id":"MDU6SXNzdWUzOTgxMDk3NTc=","number":12552,"title":"Calling a @RequestMapping annotated method with a URI template with three variables does not work [SPR-7896]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":0,"created_at":"2011-01-19T05:15:54Z","updated_at":"2019-01-12T16:49:17Z","closed_at":"2014-01-31T13:30:39Z","author_association":"COLLABORATOR","body":"**[Florian Rampp](https://jira.spring.io/secure/ViewProfile.jspa?name=jack_kerouac)** opened **[SPR-7896](https://jira.spring.io/browse/SPR-7896?redirect=false)** and commented\n\nAssume the following method definitions in an `@Controller` annotated class:\n\n```\n@RequestMapping(value = \"/x/**\")\n@ResponseBody\npublic String doSomething0() {\n\treturn \"wild card\";\n}\n\n@RequestMapping(value = \"/x/{b}/{c}/{d}\")\n@ResponseBody\npublic String doSomething1(@PathVariable(\"b\") String b, @PathVariable(\"c\") String c, @PathVariable(\"d\") String d) {\n\treturn \"three params\";\n}\n\n@RequestMapping(value = \"/x/{b}/{c}\")\n@ResponseBody\npublic String doSomething2(@PathVariable(\"b\") String b, @PathVariable(\"c\") String c) {\n\treturn \"two params\";\n}\n\n@RequestMapping(value = \"/x/{b}\")\n@ResponseBody\npublic String doSomething3(@PathVariable(\"b\") String b) {\n\treturn \"one param\";\n}\n{/code}\n\nIf I issue a HTTP GET to http://localhost:8080/x, I receive \"wild card\".\nIf I issue a HTTP GET to http://localhost:8080/x/1, I receive \"one param\".\nIf I issue a HTTP GET to http://localhost:8080/x/1/2, I receive \"two params\".\n*If I issue a HTTP GET to http://localhost:8080/x/1/2/3, I receive \"wild card\"!*\nBut I expect the method doSomething3 to be invoked and thus to receive \"three params\".\n\nI think this is a bug in the method handler resolution. The most specific definition should be taken, which works for the cases with one and two URI template variables, but not for the case with three variables. At least it is inconsistent behavior.\n\nIf I omit the method doSomething0 with the wildcard URI definition, a HTTP GET to http://localhost:8080/x/1/2/3 returns \"three params\".\n```\n\n---\n\n**Affects:** 3.0.5\n\n**Issue Links:**\n- #11407 RequestMapping with 3 PathVariable doesn't work (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["in: web","status: duplicate"]}