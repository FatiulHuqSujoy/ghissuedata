{"id":"16213", "title":"@RequestMapping detection breaks when introducing interface [SPR-11589]", "body":"**[Thomas Scheinecker](https://jira.spring.io/secure/ViewProfile.jspa?name=tscheinecker)** opened **[SPR-11589](https://jira.spring.io/browse/SPR-11589?redirect=false)** and commented

In my current application I facilitate the `@RequestMapping` and `@RestController` annotations for creating RESTful controllers.

To maintain a clean structure I decided to create a interface so all CRUDL methods have the same name.

But the problem is that as soon as my controller implements an interface the request mapping isn't applied anymore. It doesn't even matter which interface, and even marker interfaces like the one in the referenced gist break the mapping.

I tracked the problem down to the following steps:

During the mapping process in

```
...

protected void initHandlerMethods() {
	if (logger.isDebugEnabled()) {
		logger.debug(\"Looking for request mappings in application context: \" + getApplicationContext());
	}

	String[] beanNames = (this.detectHandlerMethodsInAncestorContexts ?
				BeanFactoryUtils.beanNamesForTypeIncludingAncestors(getApplicationContext(), Object.class) :
				getApplicationContext().getBeanNamesForType(Object.class));

	for (String beanName : beanNames) {
		if (isHandler(getApplicationContext().getType(beanName))){
			detectHandlerMethods(beanName);
		}
	}
	handlerMethodsInitialized(getHandlerMethods());
}
...
```

the type is retreived in

```
...
@Override
public Class<?> getType(String name) throws NoSuchBeanDefinitionException {
	String beanName = transformedBeanName(name);

	// Check manually registered singletons.
	Object beanInstance = getSingleton(beanName, false);
	if (beanInstance != null) {
		if (beanInstance instanceof FactoryBean && !BeanFactoryUtils.isFactoryDereference(name)) {
			return getTypeForFactoryBean((FactoryBean<?>) beanInstance);
		}
		else {
			return beanInstance.getClass();
		}
	}
...
}

...
```

in this particular code block `code`return beanInstance.getClass();`code` (line 574) is executed, which resolves to com.sun.proxy.$Proxy82@XXXX
which in turn leads to the problem that

```
...
	
@Override
protected boolean isHandler(Class<?> beanType) {
	return ((AnnotationUtils.findAnnotation(beanType, Controller.class) != null) || (AnnotationUtils.findAnnotation(beanType, RequestMapping.class) != null));
}

...
```

evaluates to false as the proxy class of course doesn't define any annotations. Therefore the request mapping isn't registered.

Everything works fine if I use an abstract class instead, but breaks as soon as I add an interface to that particular abstract class.

As additional info:
i use spring-boot to start the application, and run java 1.7.0_51


---

**Affects:** 4.0.1, 4.0.2

**Reference URL:** https://gist.github.com/tscheinecker/27cb4e683892b078cd9c

1 votes, 3 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16213","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16213/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16213/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16213/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16213","id":398166831,"node_id":"MDU6SXNzdWUzOTgxNjY4MzE=","number":16213,"title":"@RequestMapping detection breaks when introducing interface [SPR-11589]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2014-03-24T01:33:14Z","updated_at":"2019-01-11T21:33:08Z","closed_at":"2018-12-12T16:24:50Z","author_association":"COLLABORATOR","body":"**[Thomas Scheinecker](https://jira.spring.io/secure/ViewProfile.jspa?name=tscheinecker)** opened **[SPR-11589](https://jira.spring.io/browse/SPR-11589?redirect=false)** and commented\n\nIn my current application I facilitate the `@RequestMapping` and `@RestController` annotations for creating RESTful controllers.\n\nTo maintain a clean structure I decided to create a interface so all CRUDL methods have the same name.\n\nBut the problem is that as soon as my controller implements an interface the request mapping isn't applied anymore. It doesn't even matter which interface, and even marker interfaces like the one in the referenced gist break the mapping.\n\nI tracked the problem down to the following steps:\n\nDuring the mapping process in\n\n```\n...\n\nprotected void initHandlerMethods() {\n\tif (logger.isDebugEnabled()) {\n\t\tlogger.debug(\"Looking for request mappings in application context: \" + getApplicationContext());\n\t}\n\n\tString[] beanNames = (this.detectHandlerMethodsInAncestorContexts ?\n\t\t\t\tBeanFactoryUtils.beanNamesForTypeIncludingAncestors(getApplicationContext(), Object.class) :\n\t\t\t\tgetApplicationContext().getBeanNamesForType(Object.class));\n\n\tfor (String beanName : beanNames) {\n\t\tif (isHandler(getApplicationContext().getType(beanName))){\n\t\t\tdetectHandlerMethods(beanName);\n\t\t}\n\t}\n\thandlerMethodsInitialized(getHandlerMethods());\n}\n...\n```\n\nthe type is retreived in\n\n```\n...\n@Override\npublic Class<?> getType(String name) throws NoSuchBeanDefinitionException {\n\tString beanName = transformedBeanName(name);\n\n\t// Check manually registered singletons.\n\tObject beanInstance = getSingleton(beanName, false);\n\tif (beanInstance != null) {\n\t\tif (beanInstance instanceof FactoryBean && !BeanFactoryUtils.isFactoryDereference(name)) {\n\t\t\treturn getTypeForFactoryBean((FactoryBean<?>) beanInstance);\n\t\t}\n\t\telse {\n\t\t\treturn beanInstance.getClass();\n\t\t}\n\t}\n...\n}\n\n...\n```\n\nin this particular code block `code`return beanInstance.getClass();`code` (line 574) is executed, which resolves to com.sun.proxy.$Proxy82@XXXX\nwhich in turn leads to the problem that\n\n```\n...\n\t\n@Override\nprotected boolean isHandler(Class<?> beanType) {\n\treturn ((AnnotationUtils.findAnnotation(beanType, Controller.class) != null) || (AnnotationUtils.findAnnotation(beanType, RequestMapping.class) != null));\n}\n\n...\n```\n\nevaluates to false as the proxy class of course doesn't define any annotations. Therefore the request mapping isn't registered.\n\nEverything works fine if I use an abstract class instead, but breaks as soon as I add an interface to that particular abstract class.\n\nAs additional info:\ni use spring-boot to start the application, and run java 1.7.0_51\n\n\n---\n\n**Affects:** 4.0.1, 4.0.2\n\n**Reference URL:** https://gist.github.com/tscheinecker/27cb4e683892b078cd9c\n\n1 votes, 3 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453411444","453411445"], "labels":["in: web","status: declined","type: enhancement"]}