{"id":"4848", "title":"readonly transaction attribute doesn't seem to work. [SPR-114]", "body":"**[Mike Youngstrom](https://jira.spring.io/secure/ViewProfile.jspa?name=youngm)** opened **[SPR-114](https://jira.spring.io/browse/SPR-114?redirect=false)** and commented

If I wrap a simple spring bean into a TransactionInterceptor and aHibernateInterceptor and set all methods to \"readonly\" and attempt to delete an object using hibernate and then query hibernate again the object is perminately deleted.  Conversely if I delete the item and then do not attempt to query hibernate again then the item is correctly not deleted.

TestBean.java

```
public class TestBean implements Executeable {
    public HibernateTemplate hibernateTemplate;

    public String execute() {
        hibernateTemplate.execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {
                List accounts = session.find(\"from KeyValue\");
                System.out.println(accounts.size());
                session.delete(accounts.get(0));
//If you comment out the line below this then everything works as expected.
                accounts = session.find(\"from KeyValue\");
                System.out.println(accounts.size());
                return null;
            }
        });
        return null;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
}
```

Executeable.java

```
public interface Executeable {

    public String execute();
}
```

pertinate applicationContext.xml entries.

```xml
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">
<beans>
	<bean id=\"mySessionFactory\" class=\"org.springframework.jndi.JndiObjectFactoryBean\">
		<property name=\"jndiName\">
			<value>java:/hibernate/HibernateFactory</value>
		</property>
	</bean>
    <bean id=\"myHibernateInterceptor\" 
        class=\"org.springframework.orm.hibernate.HibernateInterceptor\">
        <property name=\"sessionFactory\">
            <ref bean=\"mySessionFactory\"/>
        </property>
    </bean>
	<bean id=\"matchAllWithPropReq\" class=\"org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource\">
		<property name=\"properties\">
		   <props>
				<prop key=\"*\">PROPAGATION_REQUIRED,readOnly</prop>
			</props>
		</property>
	</bean>
	<bean id=\"transactionManager\" class=\"org.springframework.transaction.jta.JtaTransactionManager\">
		<property name=\"transactionManagerName\">
			<value>java:/TransactionManager</value>
		</property>
	</bean>
    <bean id=\"hibernateTemplate\" class=\"org.springframework.orm.hibernate.HibernateTemplate\">
    	<property name=\"sessionFactory\">
    		<ref local=\"mySessionFactory\"/>
    	</property>
    </bean>
    <bean id=\"myTransactionInterceptor\" 
        class=\"org.springframework.transaction.interceptor.TransactionInterceptor\">
        <property name=\"transactionManager\">
            <ref bean=\"transactionManager\"/>
        </property>
        <property name=\"transactionAttributeSource\">
        	<ref local=\"matchAllWithPropReq\"/>
        </property>
    </bean>
	<bean id=\"Bean1\" class=\"TestBean\" singleton=\"false\">
		<property name=\"hibernateTemplate\">
			<ref local=\"hibernateTemplate\"/>
		</property>
	</bean>
    <bean id=\"myBean1\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">
        <property name=\"proxyInterfaces\">
            <value>Executeable</value>
        </property>
        <property name=\"interceptorNames\">
            <list>
                <value>myTransactionInterceptor</value>
                <value>myHibernateInterceptor</value>
            </list>
        </property>
        <property name=\"target\">
        	<ref local=\"Bean1\"/>
        </property>
    </bean>
</beans>
```



---

**Affects:** 1.0.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4848","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4848/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4848/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4848/events","html_url":"https://github.com/spring-projects/spring-framework/issues/4848","id":398049075,"node_id":"MDU6SXNzdWUzOTgwNDkwNzU=","number":4848,"title":"readonly transaction attribute doesn't seem to work. [SPR-114]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2004-05-05T05:10:51Z","updated_at":"2019-01-12T16:30:21Z","closed_at":"2004-05-17T19:21:30Z","author_association":"COLLABORATOR","body":"**[Mike Youngstrom](https://jira.spring.io/secure/ViewProfile.jspa?name=youngm)** opened **[SPR-114](https://jira.spring.io/browse/SPR-114?redirect=false)** and commented\n\nIf I wrap a simple spring bean into a TransactionInterceptor and aHibernateInterceptor and set all methods to \"readonly\" and attempt to delete an object using hibernate and then query hibernate again the object is perminately deleted.  Conversely if I delete the item and then do not attempt to query hibernate again then the item is correctly not deleted.\n\nTestBean.java\n\n```\npublic class TestBean implements Executeable {\n    public HibernateTemplate hibernateTemplate;\n\n    public String execute() {\n        hibernateTemplate.execute(new HibernateCallback() {\n            public Object doInHibernate(Session session) throws HibernateException {\n                List accounts = session.find(\"from KeyValue\");\n                System.out.println(accounts.size());\n                session.delete(accounts.get(0));\n//If you comment out the line below this then everything works as expected.\n                accounts = session.find(\"from KeyValue\");\n                System.out.println(accounts.size());\n                return null;\n            }\n        });\n        return null;\n    }\n\n    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {\n        this.hibernateTemplate = hibernateTemplate;\n    }\n}\n```\n\nExecuteable.java\n\n```\npublic interface Executeable {\n\n    public String execute();\n}\n```\n\npertinate applicationContext.xml entries.\n\n```xml\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">\n<beans>\n\t<bean id=\"mySessionFactory\" class=\"org.springframework.jndi.JndiObjectFactoryBean\">\n\t\t<property name=\"jndiName\">\n\t\t\t<value>java:/hibernate/HibernateFactory</value>\n\t\t</property>\n\t</bean>\n    <bean id=\"myHibernateInterceptor\" \n        class=\"org.springframework.orm.hibernate.HibernateInterceptor\">\n        <property name=\"sessionFactory\">\n            <ref bean=\"mySessionFactory\"/>\n        </property>\n    </bean>\n\t<bean id=\"matchAllWithPropReq\" class=\"org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource\">\n\t\t<property name=\"properties\">\n\t\t   <props>\n\t\t\t\t<prop key=\"*\">PROPAGATION_REQUIRED,readOnly</prop>\n\t\t\t</props>\n\t\t</property>\n\t</bean>\n\t<bean id=\"transactionManager\" class=\"org.springframework.transaction.jta.JtaTransactionManager\">\n\t\t<property name=\"transactionManagerName\">\n\t\t\t<value>java:/TransactionManager</value>\n\t\t</property>\n\t</bean>\n    <bean id=\"hibernateTemplate\" class=\"org.springframework.orm.hibernate.HibernateTemplate\">\n    \t<property name=\"sessionFactory\">\n    \t\t<ref local=\"mySessionFactory\"/>\n    \t</property>\n    </bean>\n    <bean id=\"myTransactionInterceptor\" \n        class=\"org.springframework.transaction.interceptor.TransactionInterceptor\">\n        <property name=\"transactionManager\">\n            <ref bean=\"transactionManager\"/>\n        </property>\n        <property name=\"transactionAttributeSource\">\n        \t<ref local=\"matchAllWithPropReq\"/>\n        </property>\n    </bean>\n\t<bean id=\"Bean1\" class=\"TestBean\" singleton=\"false\">\n\t\t<property name=\"hibernateTemplate\">\n\t\t\t<ref local=\"hibernateTemplate\"/>\n\t\t</property>\n\t</bean>\n    <bean id=\"myBean1\" class=\"org.springframework.aop.framework.ProxyFactoryBean\">\n        <property name=\"proxyInterfaces\">\n            <value>Executeable</value>\n        </property>\n        <property name=\"interceptorNames\">\n            <list>\n                <value>myTransactionInterceptor</value>\n                <value>myHibernateInterceptor</value>\n            </list>\n        </property>\n        <property name=\"target\">\n        \t<ref local=\"Bean1\"/>\n        </property>\n    </bean>\n</beans>\n```\n\n\n\n---\n\n**Affects:** 1.0.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453285157","453285160","453285162"], "labels":["in: data","status: declined"]}