{"id":"11336", "title":"@Scheduled annotation should support property placeholder values [SPR-6670]", "body":"**[Mark Fisher](https://jira.spring.io/secure/ViewProfile.jspa?name=mark.fisher)** opened **[SPR-6670](https://jira.spring.io/browse/SPR-6670?redirect=false)** and commented

This would promote externalization of the actual scheduling configuration.

Rather than:

```
@Scheduled(cron = \"0 0 9-17 * * MON-FRI\")
public void doSomething() {...}
```

The cron expression could be in a properties file, so the annotation no longer contains a hard-coded value:

```
@Scheduled(\"${schedules.businessHours}\")
public void doSomething() {...}
```

This should also work with meta-annotations, e.g.:

```
@Scheduled(cron = \"${schedules.businessHours}\")
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)	
public @interface BusinessHours {}
```

...would apply to:

```
@BusinessHours
public void doSomething() {...}
```

---

**Affects:** 3.0 GA

**Issue Links:**
- #12722 `@Scheduled`'s int attributes do not allow for placeholder values

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/410dd0aa9fa5a0970aa60849cfcac94dc540ddb4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11336","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11336/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11336/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11336/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11336","id":398101728,"node_id":"MDU6SXNzdWUzOTgxMDE3Mjg=","number":11336,"title":"@Scheduled annotation should support property placeholder values [SPR-6670]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/68","html_url":"https://github.com/spring-projects/spring-framework/milestone/68","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/68/labels","id":3960841,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0MQ==","number":68,"title":"3.0.1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":157,"state":"closed","created_at":"2019-01-10T22:03:13Z","updated_at":"2019-01-11T11:16:55Z","due_on":"2010-02-16T08:00:00Z","closed_at":"2019-01-10T22:03:13Z"},"comments":2,"created_at":"2010-01-11T05:45:09Z","updated_at":"2019-01-13T07:56:05Z","closed_at":"2013-01-22T12:25:06Z","author_association":"COLLABORATOR","body":"**[Mark Fisher](https://jira.spring.io/secure/ViewProfile.jspa?name=mark.fisher)** opened **[SPR-6670](https://jira.spring.io/browse/SPR-6670?redirect=false)** and commented\n\nThis would promote externalization of the actual scheduling configuration.\n\nRather than:\n\n```\n@Scheduled(cron = \"0 0 9-17 * * MON-FRI\")\npublic void doSomething() {...}\n```\n\nThe cron expression could be in a properties file, so the annotation no longer contains a hard-coded value:\n\n```\n@Scheduled(\"${schedules.businessHours}\")\npublic void doSomething() {...}\n```\n\nThis should also work with meta-annotations, e.g.:\n\n```\n@Scheduled(cron = \"${schedules.businessHours}\")\n@Target(ElementType.METHOD)\n@Retention(RetentionPolicy.RUNTIME)\t\npublic @interface BusinessHours {}\n```\n\n...would apply to:\n\n```\n@BusinessHours\npublic void doSomething() {...}\n```\n\n---\n\n**Affects:** 3.0 GA\n\n**Issue Links:**\n- #12722 `@Scheduled`'s int attributes do not allow for placeholder values\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/410dd0aa9fa5a0970aa60849cfcac94dc540ddb4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453346537","453346538"], "labels":["in: core","type: enhancement"]}