{"id":"7388", "title":"[url] handler mapping interceptor not being applied after moving to spring 2.0 [SPR-2700]", "body":"**[Harold Neiper](https://jira.spring.io/secure/ViewProfile.jspa?name=hneiper)** opened **[SPR-2700](https://jira.spring.io/browse/SPR-2700?redirect=false)** and commented

Switching from 1.2.7 to 2.0Final.  I have a url handler mapping that extends AbstractUrlHandlerMapping. And in my bean configuration I have an interceptor mapped to the \"interceptors\" property of that bean.  The interceptor extends HandlerInterceptorAdapter and implements the preHandle method returning either true or throws ModelAndViewDefiningException with a new **error** view specified if the users session or token is null.  Anyhow when using the 2.0 release the interceptor never seems to get applied.  I have stepped thru with debugging and threw in some outputs to the console to see if the preHandle was getting invoked but it was not.

The url handler mapping bean:

---

    <bean id=\"Rio.secureHandlerMapping\" 
    	class=\"com.ubs.ttt.arch.midtier.component.web.security.SecurityUrlHandlerMapping\">
        <property name=\"interceptors\">
            <list>
                <ref bean=\"Rio.securityInterceptor\"/>
            </list>
        </property>
        <property name=\"urlMap\">
            <map>
                <entry key=\"/getManifest.do\" value-ref=\"Rio.GetManifestController\"/>
            </map>
        </property>
    </bean>

The interceptor:

---

public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private static final String AUTHORIZED_TOKEN_KEY = \"authorizedUserToken\";
    
    public SecurityInterceptor() {}
    
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception {
        Authentication token = this.getAuthenticatedUser(request);
    
        if ((token == null || !token.isAuthenticated()) 
                || (token != null && !(token.getSubject() instanceof Subject))) {
            ModelAndView modelAndView = new ModelAndView(\"SecurityNotLoggedInError\");
            throw new ModelAndViewDefiningException(modelAndView);
        } else {
            return true;
        }
    }
    
    public Authentication getAuthenticatedUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {return null;}
        return (Authentication)session.getAttribute(AUTHORIZED_TOKEN_KEY);
    }       

}

The url handler mapping:

---

final public class SecurityUrlHandlerMapping extends AbstractUrlHandlerMapping {

    private final Map urlMap = new HashMap();
    
    /**
     * Set a Map with URL paths as keys and handler beans as values.
     * Convenient for population with bean references.
     * <p>Supports direct URL matches and Ant-style pattern matches.
     * For syntax details, see the PathMatcher class.
     * @param urlMap - map with URLs as keys and beans as values
     * @see org.springframework.util.PathMatcher
     */
    public final void setUrlMap(Map urlMap) {
        this.urlMap.putAll(urlMap);
    }
    
    /**
     * Map URL paths to handler bean names.
     * This the typical way of configuring this HandlerMapping.
     * <p>Supports direct URL matches and Ant-style pattern matches.
     * For syntax details, see the PathMatcher class.
     * @param mappings - properties with URLs as keys and bean names as values
     * @see org.springframework.util.PathMatcher
     */
    public final void setMappings(Properties mappings) {
        this.urlMap.putAll(mappings);
    }
    
    public void initApplicationContext() throws BeansException {
        if (this.urlMap.isEmpty()) {
            logger.info(\"Neither 'urlMap' nor 'mappings' set on SimpleUrlHandlerMapping\");
        } else {
            Iterator itr = this.urlMap.keySet().iterator();
            while (itr.hasNext()) {
                String url = (String) itr.next();
                Object handler = this.urlMap.get(url);
                // prepend with slash if it's not present
                if (!url.startsWith(\"/\")) {
                    url = \"/\" + url;
                }
                registerHandler(url, handler);
            }
        }
    }
    
    /**
     * Return the object this url is referring to allow access 
     * to a bean keyed by the url.
     * @param beanName - the bean you are looking to return
     * @return a <code>Object</code> to be cast to a bean of known type.
     */
    public Object getObjectMappedForBean(String beanName) {
        Iterator iter = this.urlMap.keySet().iterator();
        while (iter.hasNext()) {
            String url = (String) iter.next();
            if (url.equals(beanName)) {
                return this.urlMap.get(url);
            }
        }
        return null;
    }   

}

And finally the testcase:

---

public class DispatcherServletTestCase extends TestCase {

    private DispatcherServlet rioDispatcherServlet;
    private MockServletConfig servletConfig; 
    
    protected void setUp() throws Exception {
        servletConfig = new MockServletConfig(new MockServletContext(), \"rio\");
        rioDispatcherServlet = new DispatcherServlet();
        rioDispatcherServlet.setContextClass(TestWebApplicationContext.class);
        rioDispatcherServlet.init(servletConfig);
    }
    
    private ServletContext getServletContext() {
        return servletConfig.getServletContext();
    }    
    
    public void testNonAuthenticatedUserHasCorrectForwardedUrlAfterInterception() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest(getServletContext(), \"GET\", \"/getManifest.do\");
        MockHttpServletResponse response = new MockHttpServletResponse();
        HttpSession session = request.getSession();
        rioDispatcherServlet.service(request, response);
        assertTrue(\"Not forwarded\", response.getForwardedUrl() != null);
        assertEquals(\"\", \"/WEB-INF/jsp/SecurityNotLoggedInError.jsp\", response.getForwardedUrl());
    }    

}

---

**Affects:** 2.0 final
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7388","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7388/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7388/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7388/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7388","id":398071786,"node_id":"MDU6SXNzdWUzOTgwNzE3ODY=","number":7388,"title":"[url] handler mapping interceptor not being applied after moving to spring 2.0 [SPR-2700]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false,"description":"Issues in web modules (web, webmvc, webflux, websocket)"},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false,"description":"A suggestion or change that we don't feel we should currently apply"}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2006-10-09T08:29:30Z","updated_at":"2019-01-13T22:45:44Z","closed_at":"2006-10-13T03:15:43Z","author_association":"COLLABORATOR","body":"**[Harold Neiper](https://jira.spring.io/secure/ViewProfile.jspa?name=hneiper)** opened **[SPR-2700](https://jira.spring.io/browse/SPR-2700?redirect=false)** and commented\n\nSwitching from 1.2.7 to 2.0Final.  I have a url handler mapping that extends AbstractUrlHandlerMapping. And in my bean configuration I have an interceptor mapped to the \"interceptors\" property of that bean.  The interceptor extends HandlerInterceptorAdapter and implements the preHandle method returning either true or throws ModelAndViewDefiningException with a new **error** view specified if the users session or token is null.  Anyhow when using the 2.0 release the interceptor never seems to get applied.  I have stepped thru with debugging and threw in some outputs to the console to see if the preHandle was getting invoked but it was not.\n\nThe url handler mapping bean:\n\n---\n\n    <bean id=\"Rio.secureHandlerMapping\" \n    \tclass=\"com.ubs.ttt.arch.midtier.component.web.security.SecurityUrlHandlerMapping\">\n        <property name=\"interceptors\">\n            <list>\n                <ref bean=\"Rio.securityInterceptor\"/>\n            </list>\n        </property>\n        <property name=\"urlMap\">\n            <map>\n                <entry key=\"/getManifest.do\" value-ref=\"Rio.GetManifestController\"/>\n            </map>\n        </property>\n    </bean>\n\nThe interceptor:\n\n---\n\npublic class SecurityInterceptor extends HandlerInterceptorAdapter {\n\n    private static final String AUTHORIZED_TOKEN_KEY = \"authorizedUserToken\";\n    \n    public SecurityInterceptor() {}\n    \n    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)\n        throws Exception {\n        Authentication token = this.getAuthenticatedUser(request);\n    \n        if ((token == null || !token.isAuthenticated()) \n                || (token != null && !(token.getSubject() instanceof Subject))) {\n            ModelAndView modelAndView = new ModelAndView(\"SecurityNotLoggedInError\");\n            throw new ModelAndViewDefiningException(modelAndView);\n        } else {\n            return true;\n        }\n    }\n    \n    public Authentication getAuthenticatedUser(HttpServletRequest request) {\n        HttpSession session = request.getSession(false);\n        if (session == null) {return null;}\n        return (Authentication)session.getAttribute(AUTHORIZED_TOKEN_KEY);\n    }       \n\n}\n\nThe url handler mapping:\n\n---\n\nfinal public class SecurityUrlHandlerMapping extends AbstractUrlHandlerMapping {\n\n    private final Map urlMap = new HashMap();\n    \n    /**\n     * Set a Map with URL paths as keys and handler beans as values.\n     * Convenient for population with bean references.\n     * <p>Supports direct URL matches and Ant-style pattern matches.\n     * For syntax details, see the PathMatcher class.\n     * @param urlMap - map with URLs as keys and beans as values\n     * @see org.springframework.util.PathMatcher\n     */\n    public final void setUrlMap(Map urlMap) {\n        this.urlMap.putAll(urlMap);\n    }\n    \n    /**\n     * Map URL paths to handler bean names.\n     * This the typical way of configuring this HandlerMapping.\n     * <p>Supports direct URL matches and Ant-style pattern matches.\n     * For syntax details, see the PathMatcher class.\n     * @param mappings - properties with URLs as keys and bean names as values\n     * @see org.springframework.util.PathMatcher\n     */\n    public final void setMappings(Properties mappings) {\n        this.urlMap.putAll(mappings);\n    }\n    \n    public void initApplicationContext() throws BeansException {\n        if (this.urlMap.isEmpty()) {\n            logger.info(\"Neither 'urlMap' nor 'mappings' set on SimpleUrlHandlerMapping\");\n        } else {\n            Iterator itr = this.urlMap.keySet().iterator();\n            while (itr.hasNext()) {\n                String url = (String) itr.next();\n                Object handler = this.urlMap.get(url);\n                // prepend with slash if it's not present\n                if (!url.startsWith(\"/\")) {\n                    url = \"/\" + url;\n                }\n                registerHandler(url, handler);\n            }\n        }\n    }\n    \n    /**\n     * Return the object this url is referring to allow access \n     * to a bean keyed by the url.\n     * @param beanName - the bean you are looking to return\n     * @return a <code>Object</code> to be cast to a bean of known type.\n     */\n    public Object getObjectMappedForBean(String beanName) {\n        Iterator iter = this.urlMap.keySet().iterator();\n        while (iter.hasNext()) {\n            String url = (String) iter.next();\n            if (url.equals(beanName)) {\n                return this.urlMap.get(url);\n            }\n        }\n        return null;\n    }   \n\n}\n\nAnd finally the testcase:\n\n---\n\npublic class DispatcherServletTestCase extends TestCase {\n\n    private DispatcherServlet rioDispatcherServlet;\n    private MockServletConfig servletConfig; \n    \n    protected void setUp() throws Exception {\n        servletConfig = new MockServletConfig(new MockServletContext(), \"rio\");\n        rioDispatcherServlet = new DispatcherServlet();\n        rioDispatcherServlet.setContextClass(TestWebApplicationContext.class);\n        rioDispatcherServlet.init(servletConfig);\n    }\n    \n    private ServletContext getServletContext() {\n        return servletConfig.getServletContext();\n    }    \n    \n    public void testNonAuthenticatedUserHasCorrectForwardedUrlAfterInterception() throws Exception {\n        MockHttpServletRequest request = new MockHttpServletRequest(getServletContext(), \"GET\", \"/getManifest.do\");\n        MockHttpServletResponse response = new MockHttpServletResponse();\n        HttpSession session = request.getSession();\n        rioDispatcherServlet.service(request, response);\n        assertTrue(\"Not forwarded\", response.getForwardedUrl() != null);\n        assertEquals(\"\", \"/WEB-INF/jsp/SecurityNotLoggedInError.jsp\", response.getForwardedUrl());\n    }    \n\n}\n\n---\n\n**Affects:** 2.0 final\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453311135","453311136","453311137","453311138","453311141"], "labels":["in: web","status: declined"]}