{"id":"9367", "title":"Application Listener does not work for new instances of prototype beans [SPR-4690]", "body":"**[Rafi Feroze](https://jira.spring.io/secure/ViewProfile.jspa?name=rferoze)** opened **[SPR-4690](https://jira.spring.io/browse/SPR-4690?redirect=false)** and commented

1. create an ApplicationListener.

public class BeanListener implements ApplicationListener
{
Object source;
int count;
static int allcount = 0;

public BeanListener()
{
super();

    count = allcount++;
    System.out.println(\"instance created=\" + count);

}

public void onApplicationEvent(ApplicationEvent arg0)
{
System.out.print(\"\" + arg0.getClass().getName() + \" event received by=\" + count);
// if this is null, the listener is created automatically
System.out.println(\", my master is=\" + source);
}

public Object getSource()
{
return source;
}

public void setSource(Object source)
{
this.source = source;
}
}

2. config it in a context

   \\<bean id=\"listener-a\"
   class=\"applistener.BeanListener\"
   singleton=\"false\">
   \\</bean>

3. use it
   BeanListener bl= (BeanListener) SDSApplicationContextFactory.buildContext().getBean(\"listener-a\");
   // if listener's source is null, the listener is not from here
   bl.setSource(main);
   main.bean= bl;

   TestEvent ev= new TestEvent(\"\");
   try
   {
   Thread.currentThread().sleep(3000);
   }
   catch (InterruptedException ev1)
   {
   // TODO Auto-generated catch block
   ev1.printStackTrace();
   }

   SDSApplicationContextFactory.buildContext().publishEvent(ev);

4. output
   instance created=0
   org.springframework.context.event.ContextRefreshedEvent event received by=0       my master is=null
   instance created=1
   applistener.TestEvent event received by=0       my master is=null

note the second instance(instance created=1)'s event handling method was never called. Is it registered with the context?


---

**Affects:** 1.2 final
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9367","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9367/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9367/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9367/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9367","id":398087281,"node_id":"MDU6SXNzdWUzOTgwODcyODE=","number":9367,"title":"Application Listener does not work for new instances of prototype beans [SPR-4690]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2008-04-10T12:48:02Z","updated_at":"2009-02-06T18:27:50Z","closed_at":"2009-02-06T18:27:50Z","author_association":"COLLABORATOR","body":"**[Rafi Feroze](https://jira.spring.io/secure/ViewProfile.jspa?name=rferoze)** opened **[SPR-4690](https://jira.spring.io/browse/SPR-4690?redirect=false)** and commented\n\n1. create an ApplicationListener.\n\npublic class BeanListener implements ApplicationListener\n{\nObject source;\nint count;\nstatic int allcount = 0;\n\npublic BeanListener()\n{\nsuper();\n\n    count = allcount++;\n    System.out.println(\"instance created=\" + count);\n\n}\n\npublic void onApplicationEvent(ApplicationEvent arg0)\n{\nSystem.out.print(\"\" + arg0.getClass().getName() + \" event received by=\" + count);\n// if this is null, the listener is created automatically\nSystem.out.println(\", my master is=\" + source);\n}\n\npublic Object getSource()\n{\nreturn source;\n}\n\npublic void setSource(Object source)\n{\nthis.source = source;\n}\n}\n\n2. config it in a context\n\n   \\<bean id=\"listener-a\"\n   class=\"applistener.BeanListener\"\n   singleton=\"false\">\n   \\</bean>\n\n3. use it\n   BeanListener bl= (BeanListener) SDSApplicationContextFactory.buildContext().getBean(\"listener-a\");\n   // if listener's source is null, the listener is not from here\n   bl.setSource(main);\n   main.bean= bl;\n\n   TestEvent ev= new TestEvent(\"\");\n   try\n   {\n   Thread.currentThread().sleep(3000);\n   }\n   catch (InterruptedException ev1)\n   {\n   // TODO Auto-generated catch block\n   ev1.printStackTrace();\n   }\n\n   SDSApplicationContextFactory.buildContext().publishEvent(ev);\n\n4. output\n   instance created=0\n   org.springframework.context.event.ContextRefreshedEvent event received by=0       my master is=null\n   instance created=1\n   applistener.TestEvent event received by=0       my master is=null\n\nnote the second instance(instance created=1)'s event handling method was never called. Is it registered with the context?\n\n\n---\n\n**Affects:** 1.2 final\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453329475","453329476","453329478"], "labels":["in: core","status: declined","type: enhancement"]}