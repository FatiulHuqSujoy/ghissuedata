{"id":"9255", "title":"PropertyPlaceholderConfigurer fails to resolve properties in combination with ProxyFactoryBean and default-autowire=\"byType\" [SPR-4578]", "body":"**[Lars Vonk](https://jira.spring.io/secure/ViewProfile.jspa?name=lvonk)** opened **[SPR-4578](https://jira.spring.io/browse/SPR-4578?redirect=false)** and commented

This seems related to http://jira.springframework.org/browse/SPR-1953. This only occurs when default-autowire=\"byType\".

beans.xml:

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd\">

    <bean id=\"myUrl\" class=\"java.net.URL\">
    	<constructor-arg index=\"0\" value=\"${test.url}\" />
    </bean>
    
    <bean id=\"myService\"
    	class=\"org.springframework.aop.framework.ProxyFactoryBean\">
    	<property name=\"interfaces\">
    		<value>com.xebia.MyService</value>
    	</property>
    	<property name=\"target\">
    		<bean id=\"myServiceTarget\"
    			class=\"com.xebia.MyServiceImpl\">
    			<constructor-arg index=\"0\" ref=\"myUrl\" />
    		</bean>
    	</property>
    </bean>

\\</beans>

placeholder.xml

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd\"
default-autowire=\"byType\">

    <bean
    	class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">
    	<property name=\"locations\" value=\"classpath:props.properties\" />
    </bean>

\\</beans>

Test class:

public class SpringTest {
`@Test`
public void shouldLoadSpringContext() {
String[] locations = new String[]{\"placeholder.xml\", \"beans.xml\"};
new ClassPathXmlApplicationContext(locations);       
}
}

props.properties
test.url=http://www.nu.nl

Cheers, Lars

---

**Affects:** 2.5.2

**Issue Links:**
- #6646 PropertyPlaceholderConfigurer indirectly instantiates FactoryBeans if it's autowire mode is anything but 'no'

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9255","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9255/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9255/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9255/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9255","id":398086344,"node_id":"MDU6SXNzdWUzOTgwODYzNDQ=","number":9255,"title":"PropertyPlaceholderConfigurer fails to resolve properties in combination with ProxyFactoryBean and default-autowire=\"byType\" [SPR-4578]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/56","html_url":"https://github.com/spring-projects/spring-framework/milestone/56","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/56/labels","id":3960828,"node_id":"MDk6TWlsZXN0b25lMzk2MDgyOA==","number":56,"title":"2.5.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":93,"state":"closed","created_at":"2019-01-10T22:02:59Z","updated_at":"2019-01-11T01:11:58Z","due_on":"2008-04-04T07:00:00Z","closed_at":"2019-01-10T22:02:59Z"},"comments":1,"created_at":"2008-03-13T08:06:47Z","updated_at":"2019-01-11T17:17:09Z","closed_at":"2012-06-19T03:46:54Z","author_association":"COLLABORATOR","body":"**[Lars Vonk](https://jira.spring.io/secure/ViewProfile.jspa?name=lvonk)** opened **[SPR-4578](https://jira.spring.io/browse/SPR-4578?redirect=false)** and commented\n\nThis seems related to http://jira.springframework.org/browse/SPR-1953. This only occurs when default-autowire=\"byType\".\n\nbeans.xml:\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd\">\n\n    <bean id=\"myUrl\" class=\"java.net.URL\">\n    \t<constructor-arg index=\"0\" value=\"${test.url}\" />\n    </bean>\n    \n    <bean id=\"myService\"\n    \tclass=\"org.springframework.aop.framework.ProxyFactoryBean\">\n    \t<property name=\"interfaces\">\n    \t\t<value>com.xebia.MyService</value>\n    \t</property>\n    \t<property name=\"target\">\n    \t\t<bean id=\"myServiceTarget\"\n    \t\t\tclass=\"com.xebia.MyServiceImpl\">\n    \t\t\t<constructor-arg index=\"0\" ref=\"myUrl\" />\n    \t\t</bean>\n    \t</property>\n    </bean>\n\n\\</beans>\n\nplaceholder.xml\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd\"\ndefault-autowire=\"byType\">\n\n    <bean\n    \tclass=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">\n    \t<property name=\"locations\" value=\"classpath:props.properties\" />\n    </bean>\n\n\\</beans>\n\nTest class:\n\npublic class SpringTest {\n`@Test`\npublic void shouldLoadSpringContext() {\nString[] locations = new String[]{\"placeholder.xml\", \"beans.xml\"};\nnew ClassPathXmlApplicationContext(locations);       \n}\n}\n\nprops.properties\ntest.url=http://www.nu.nl\n\nCheers, Lars\n\n---\n\n**Affects:** 2.5.2\n\n**Issue Links:**\n- #6646 PropertyPlaceholderConfigurer indirectly instantiates FactoryBeans if it's autowire mode is anything but 'no'\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453328287"], "labels":["in: core","type: enhancement"]}