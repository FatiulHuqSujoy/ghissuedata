{"id":"18464", "title":"Support Spring validator + JSR in one validator factory bean [SPR-13890]", "body":"**[Wallace Wadge](https://jira.spring.io/secure/ViewProfile.jspa?name=wwadge)** opened **[SPR-13890](https://jira.spring.io/browse/SPR-13890?redirect=false)** and commented

As per the SO post and unless I'm also missing something, out of the box we don't get both Spring Validator + JSR to co-exist. Can I suggest we add this (slightly amended) code:

```java
public abstract class AbstractValidator extends LocalValidatorFactoryBean implements ApplicationContextAware, ConstraintValidatorFactory {


    private ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;

    }

    @Override
    public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
        Map<String, T> beansByNames = applicationContext.getBeansOfType(key);
        if (beansByNames.isEmpty()) {
            try {
                return key.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(\"Could not instantiate constraint validator class '\" + key.getName() + \"'\", e);
            }
        }
        if (beansByNames.size() > 1) {
            throw new RuntimeException(\"Only one bean of type '\" + key.getName() + \"' is allowed in the application context\");
        }
        return beansByNames.values().iterator().next();
    }

    public boolean supports(Class<?> c) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errors) {
        validate(target, errors, (Object[]) null);
    }

    public void validate(Object objectForm, Errors errors, Object... validationHints) {
        super.validate(objectForm, errors, validationHints);
        addExtraValidationWithHints(objectForm, errors, validationHints);
        addExtraValidation(objectForm, errors);
    }

    protected abstract void addExtraValidationWithHints(Object objectForm, Errors errors, Object[] validationHints);

    protected abstract void addExtraValidation(Object objectForm, Errors errors);

    @Override
    public void releaseInstance(ConstraintValidator<?, ?> instance) {

    }

    @Override
    public ExecutableValidator forExecutables() {
        return null;
    }

    @Override
    public ParameterNameProvider getParameterNameProvider() {
        return null;
    }
}
```



---

**Affects:** 4.2.4

**Reference URL:** http://stackoverflow.com/questions/7080684/spring-validator-having-both-annotation-and-validator-implementation
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18464","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18464/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18464/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18464/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18464","id":398188985,"node_id":"MDU6SXNzdWUzOTgxODg5ODU=","number":18464,"title":"Support Spring validator + JSR in one validator factory bean [SPR-13890]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-01-26T11:03:33Z","updated_at":"2019-01-12T02:44:09Z","closed_at":"2019-01-12T02:44:09Z","author_association":"COLLABORATOR","body":"**[Wallace Wadge](https://jira.spring.io/secure/ViewProfile.jspa?name=wwadge)** opened **[SPR-13890](https://jira.spring.io/browse/SPR-13890?redirect=false)** and commented\n\nAs per the SO post and unless I'm also missing something, out of the box we don't get both Spring Validator + JSR to co-exist. Can I suggest we add this (slightly amended) code:\n\n```java\npublic abstract class AbstractValidator extends LocalValidatorFactoryBean implements ApplicationContextAware, ConstraintValidatorFactory {\n\n\n    private ApplicationContext applicationContext;\n\n    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {\n        this.applicationContext = applicationContext;\n\n    }\n\n    @Override\n    public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {\n        Map<String, T> beansByNames = applicationContext.getBeansOfType(key);\n        if (beansByNames.isEmpty()) {\n            try {\n                return key.newInstance();\n            } catch (InstantiationException | IllegalAccessException e) {\n                throw new RuntimeException(\"Could not instantiate constraint validator class '\" + key.getName() + \"'\", e);\n            }\n        }\n        if (beansByNames.size() > 1) {\n            throw new RuntimeException(\"Only one bean of type '\" + key.getName() + \"' is allowed in the application context\");\n        }\n        return beansByNames.values().iterator().next();\n    }\n\n    public boolean supports(Class<?> c) {\n        return true;\n    }\n\n    @Override\n    public void validate(Object target, Errors errors) {\n        validate(target, errors, (Object[]) null);\n    }\n\n    public void validate(Object objectForm, Errors errors, Object... validationHints) {\n        super.validate(objectForm, errors, validationHints);\n        addExtraValidationWithHints(objectForm, errors, validationHints);\n        addExtraValidation(objectForm, errors);\n    }\n\n    protected abstract void addExtraValidationWithHints(Object objectForm, Errors errors, Object[] validationHints);\n\n    protected abstract void addExtraValidation(Object objectForm, Errors errors);\n\n    @Override\n    public void releaseInstance(ConstraintValidator<?, ?> instance) {\n\n    }\n\n    @Override\n    public ExecutableValidator forExecutables() {\n        return null;\n    }\n\n    @Override\n    public ParameterNameProvider getParameterNameProvider() {\n        return null;\n    }\n}\n```\n\n\n\n---\n\n**Affects:** 4.2.4\n\n**Reference URL:** http://stackoverflow.com/questions/7080684/spring-validator-having-both-annotation-and-validator-implementation\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453713159"], "labels":["in: core","status: bulk-closed"]}