{"id":"7640", "title":"Doc: Advice to beans with @Aspect-marked classes don't work [SPR-2954]", "body":"**[visionset](https://jira.spring.io/secure/ViewProfile.jspa?name=visionset)** opened **[SPR-2954](https://jira.spring.io/browse/SPR-2954?redirect=false)** and commented

Simplest Advice does not execute.

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:aop=\"http://www.springframework.org/schema/aop\"
xmlns:tx=\"http://www.springframework.org/schema/tx\"
xsi:schemaLocation=\"
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.0.xsd
http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.0.xsd\">

    <aop:aspectj-autoproxy />
    
    <bean id=\"anotherAspect\" class=\"timeout.helloworld.AnotherAspect\"></bean>
    <bean id=\"helloAspect\" class=\"timeout.helloworld.HelloAspect\"></bean>
    <bean id=\"myWorld\" class=\"timeout.helloworld.worlds.MyWorld\"></bean>
    <bean id=\"helloRunner\" class=\"timeout.helloworld.HelloRunner\"></bean>
    <bean id=\"defaultWorld\" class=\"timeout.helloworld.DefaultWorld\"></bean>

\\</beans>

package timeout.helloworld;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;

`@Aspect`
public class HelloAspect {

    @DeclareParents(value=\"timeout.helloworld.worlds.*+\",
    	defaultImpl=timeout.helloworld.DefaultWorld.class)
    public static World mixin;
    
    /*
     * Execute the following Advice before any method in the .worlds package or
     * subpackage
     */
    //@AfterReturning(\"timeout.helloworld.AnotherAspect.myPointcut()\")
    @AfterReturning(\"execution (* *(..))\")
    public void myAdvice() {
    	System.out.println(\"Here is my advice\");
    	
    }	
    
    public void testAdvice() {	
    	System.out.println(\"HelloAspect.testAdvice()\");
    }

}

package timeout.helloworld;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

`@Aspect`
public class AnotherAspect {

    public void doAccessCheck() {}
    
    @Pointcut(\"execution(* *(..))\")
    public void myPointcut() {}
    
    public void testAdvice() {	
    	System.out.println(\"AnotherAspect.testAdvice()\");
    }

}

package timeout.helloworld;
public interface World {

    String doSomut();

}

package timeout.helloworld.worlds;
public class MyWorld {

    public String doSomut() {
    	return \"My World Doing something!!!!!!!!!\";
    }

}

package timeout.helloworld;
public class DefaultWorld implements World {

    public String doSomut() {	
    	return \"DefaultWorld does something!\";	
    }

}

package timeout.helloworld;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;

`@Aspect`
public class HelloRunner {

    public static void main(String[] args) {
    	
    	BeanFactory factory = new FileSystemXmlApplicationContext(
    		\"build/WEB-INF/classes/applicationContext/test-aspects.xml\");
    	
    	World world = (World) factory.getBean(\"myWorld\");
    	
    	System.out.println(world.doSomut());
    
    	AnotherAspect obj2 = (AnotherAspect) factory.getBean(\"anotherAspect\");		
    	obj2.testAdvice();
    	HelloAspect obj3 = (HelloAspect) factory.getBean(\"helloAspect\");		
    	obj3.testAdvice();	
    	HelloRunner obj4 = (HelloRunner) factory.getBean(\"helloRunner\");		
    	obj4.testAdvice();
    	
    	new HelloRunner().testAdvice();
    }
    
    public void testAdvice() {
    	
    	System.out.println(\"HelloRunner.testAdvice()\");
    }

}

\\<Output>
17-Dec-2006 14:13:29 org.springframework.core.CollectionFactory \\<clinit>
INFO: JDK 1.4+ collections available
17-Dec-2006 14:13:29 org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions
INFO: Loading XML bean definitions from file [D:\\workspace\\Timeout\\build\\WEB-INF\\classes\\applicationContext\\test-aspects.xml]
17-Dec-2006 14:13:29 org.springframework.context.support.AbstractRefreshableApplicationContext refreshBeanFactory
INFO: Bean factory for application context [org.springframework.context.support.FileSystemXmlApplicationContext;hashCode=25253977]: org.springframework.beans.factory.support.DefaultListableBeanFactory defining beans [org.springframework.aop.config.internalAutoProxyCreator,anotherAspect,helloAspect,myWorld,helloRunner,defaultWorld]; root of BeanFactory hierarchy
17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext refresh
INFO: 6 beans defined in application context [org.springframework.context.support.FileSystemXmlApplicationContext;hashCode=25253977]
17-Dec-2006 14:13:29 org.springframework.aop.framework.DefaultAopProxyFactory \\<clinit>
INFO: CGLIB2 available: proxyTargetClass feature enabled
17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext$BeanPostProcessorChecker postProcessAfterInitialization
INFO: Bean 'org.springframework.aop.config.internalAutoProxyCreator' is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext initMessageSource
INFO: Unable to locate MessageSource with name 'messageSource': using default [org.springframework.context.support.DelegatingMessageSource@1bcdbf6]
17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext initApplicationEventMulticaster
INFO: Unable to locate ApplicationEventMulticaster with name 'applicationEventMulticaster': using default [org.springframework.context.event.SimpleApplicationEventMulticaster@16f25a7]
17-Dec-2006 14:13:29 org.springframework.beans.factory.support.DefaultListableBeanFactory preInstantiateSingletons
INFO: Pre-instantiating singletons in factory [org.springframework.beans.factory.support.DefaultListableBeanFactory defining beans [org.springframework.aop.config.internalAutoProxyCreator,anotherAspect,helloAspect,myWorld,helloRunner,defaultWorld]; root of BeanFactory hierarchy]

DefaultWorld does something!
AnotherAspect.testAdvice()
HelloAspect.testAdvice()
HelloRunner.testAdvice()
HelloRunner.testAdvice()
\\</Output>

Expected multiple output of:
\"Here is my Advice\"



---

**Affects:** 2.0 final

**Attachments:**
- [advice-to-atAspectBean-test.patch](https://jira.spring.io/secure/attachment/12280/advice-to-atAspectBean-test.patch) (_4.05 kB_)

1 votes, 1 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7640","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7640/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7640/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7640/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7640","id":398073825,"node_id":"MDU6SXNzdWUzOTgwNzM4MjU=","number":7640,"title":"Doc: Advice to beans with @Aspect-marked classes don't work [SPR-2954]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511933,"node_id":"MDU6TGFiZWwxMTg4NTExOTMz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20task","name":"type: task","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41","html_url":"https://github.com/spring-projects/spring-framework/milestone/41","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41/labels","id":3960813,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxMw==","number":41,"title":"2.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":76,"state":"closed","created_at":"2019-01-10T22:02:41Z","updated_at":"2019-01-11T01:26:07Z","due_on":"2007-04-08T07:00:00Z","closed_at":"2019-01-10T22:02:41Z"},"comments":5,"created_at":"2006-12-17T00:18:12Z","updated_at":"2012-06-19T03:50:24Z","closed_at":"2012-06-19T03:50:24Z","author_association":"COLLABORATOR","body":"**[visionset](https://jira.spring.io/secure/ViewProfile.jspa?name=visionset)** opened **[SPR-2954](https://jira.spring.io/browse/SPR-2954?redirect=false)** and commented\n\nSimplest Advice does not execute.\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:aop=\"http://www.springframework.org/schema/aop\"\nxmlns:tx=\"http://www.springframework.org/schema/tx\"\nxsi:schemaLocation=\"\nhttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd\nhttp://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.0.xsd\nhttp://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.0.xsd\">\n\n    <aop:aspectj-autoproxy />\n    \n    <bean id=\"anotherAspect\" class=\"timeout.helloworld.AnotherAspect\"></bean>\n    <bean id=\"helloAspect\" class=\"timeout.helloworld.HelloAspect\"></bean>\n    <bean id=\"myWorld\" class=\"timeout.helloworld.worlds.MyWorld\"></bean>\n    <bean id=\"helloRunner\" class=\"timeout.helloworld.HelloRunner\"></bean>\n    <bean id=\"defaultWorld\" class=\"timeout.helloworld.DefaultWorld\"></bean>\n\n\\</beans>\n\npackage timeout.helloworld;\nimport org.aspectj.lang.annotation.AfterReturning;\nimport org.aspectj.lang.annotation.Aspect;\nimport org.aspectj.lang.annotation.DeclareParents;\n\n`@Aspect`\npublic class HelloAspect {\n\n    @DeclareParents(value=\"timeout.helloworld.worlds.*+\",\n    \tdefaultImpl=timeout.helloworld.DefaultWorld.class)\n    public static World mixin;\n    \n    /*\n     * Execute the following Advice before any method in the .worlds package or\n     * subpackage\n     */\n    //@AfterReturning(\"timeout.helloworld.AnotherAspect.myPointcut()\")\n    @AfterReturning(\"execution (* *(..))\")\n    public void myAdvice() {\n    \tSystem.out.println(\"Here is my advice\");\n    \t\n    }\t\n    \n    public void testAdvice() {\t\n    \tSystem.out.println(\"HelloAspect.testAdvice()\");\n    }\n\n}\n\npackage timeout.helloworld;\nimport org.aspectj.lang.annotation.Aspect;\nimport org.aspectj.lang.annotation.Pointcut;\n\n`@Aspect`\npublic class AnotherAspect {\n\n    public void doAccessCheck() {}\n    \n    @Pointcut(\"execution(* *(..))\")\n    public void myPointcut() {}\n    \n    public void testAdvice() {\t\n    \tSystem.out.println(\"AnotherAspect.testAdvice()\");\n    }\n\n}\n\npackage timeout.helloworld;\npublic interface World {\n\n    String doSomut();\n\n}\n\npackage timeout.helloworld.worlds;\npublic class MyWorld {\n\n    public String doSomut() {\n    \treturn \"My World Doing something!!!!!!!!!\";\n    }\n\n}\n\npackage timeout.helloworld;\npublic class DefaultWorld implements World {\n\n    public String doSomut() {\t\n    \treturn \"DefaultWorld does something!\";\t\n    }\n\n}\n\npackage timeout.helloworld;\nimport org.aspectj.lang.annotation.Aspect;\nimport org.springframework.beans.factory.BeanFactory;\nimport org.springframework.context.support.FileSystemXmlApplicationContext;\n\n`@Aspect`\npublic class HelloRunner {\n\n    public static void main(String[] args) {\n    \t\n    \tBeanFactory factory = new FileSystemXmlApplicationContext(\n    \t\t\"build/WEB-INF/classes/applicationContext/test-aspects.xml\");\n    \t\n    \tWorld world = (World) factory.getBean(\"myWorld\");\n    \t\n    \tSystem.out.println(world.doSomut());\n    \n    \tAnotherAspect obj2 = (AnotherAspect) factory.getBean(\"anotherAspect\");\t\t\n    \tobj2.testAdvice();\n    \tHelloAspect obj3 = (HelloAspect) factory.getBean(\"helloAspect\");\t\t\n    \tobj3.testAdvice();\t\n    \tHelloRunner obj4 = (HelloRunner) factory.getBean(\"helloRunner\");\t\t\n    \tobj4.testAdvice();\n    \t\n    \tnew HelloRunner().testAdvice();\n    }\n    \n    public void testAdvice() {\n    \t\n    \tSystem.out.println(\"HelloRunner.testAdvice()\");\n    }\n\n}\n\n\\<Output>\n17-Dec-2006 14:13:29 org.springframework.core.CollectionFactory \\<clinit>\nINFO: JDK 1.4+ collections available\n17-Dec-2006 14:13:29 org.springframework.beans.factory.xml.XmlBeanDefinitionReader loadBeanDefinitions\nINFO: Loading XML bean definitions from file [D:\\workspace\\Timeout\\build\\WEB-INF\\classes\\applicationContext\\test-aspects.xml]\n17-Dec-2006 14:13:29 org.springframework.context.support.AbstractRefreshableApplicationContext refreshBeanFactory\nINFO: Bean factory for application context [org.springframework.context.support.FileSystemXmlApplicationContext;hashCode=25253977]: org.springframework.beans.factory.support.DefaultListableBeanFactory defining beans [org.springframework.aop.config.internalAutoProxyCreator,anotherAspect,helloAspect,myWorld,helloRunner,defaultWorld]; root of BeanFactory hierarchy\n17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext refresh\nINFO: 6 beans defined in application context [org.springframework.context.support.FileSystemXmlApplicationContext;hashCode=25253977]\n17-Dec-2006 14:13:29 org.springframework.aop.framework.DefaultAopProxyFactory \\<clinit>\nINFO: CGLIB2 available: proxyTargetClass feature enabled\n17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext$BeanPostProcessorChecker postProcessAfterInitialization\nINFO: Bean 'org.springframework.aop.config.internalAutoProxyCreator' is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)\n17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext initMessageSource\nINFO: Unable to locate MessageSource with name 'messageSource': using default [org.springframework.context.support.DelegatingMessageSource@1bcdbf6]\n17-Dec-2006 14:13:29 org.springframework.context.support.AbstractApplicationContext initApplicationEventMulticaster\nINFO: Unable to locate ApplicationEventMulticaster with name 'applicationEventMulticaster': using default [org.springframework.context.event.SimpleApplicationEventMulticaster@16f25a7]\n17-Dec-2006 14:13:29 org.springframework.beans.factory.support.DefaultListableBeanFactory preInstantiateSingletons\nINFO: Pre-instantiating singletons in factory [org.springframework.beans.factory.support.DefaultListableBeanFactory defining beans [org.springframework.aop.config.internalAutoProxyCreator,anotherAspect,helloAspect,myWorld,helloRunner,defaultWorld]; root of BeanFactory hierarchy]\n\nDefaultWorld does something!\nAnotherAspect.testAdvice()\nHelloAspect.testAdvice()\nHelloRunner.testAdvice()\nHelloRunner.testAdvice()\n\\</Output>\n\nExpected multiple output of:\n\"Here is my Advice\"\n\n\n\n---\n\n**Affects:** 2.0 final\n\n**Attachments:**\n- [advice-to-atAspectBean-test.patch](https://jira.spring.io/secure/attachment/12280/advice-to-atAspectBean-test.patch) (_4.05 kB_)\n\n1 votes, 1 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453313481","453313483","453313484","453313485","453313487"], "labels":["in: core","type: task"]}