{"id":"6144", "title":"Additional Document for Spring - commons-fileupload integration [SPR-1444]", "body":"**[Yujin Kim](https://jira.spring.io/secure/ViewProfile.jspa?name=netexplode)** opened **[SPR-1444](https://jira.spring.io/browse/SPR-1444?redirect=false)** and commented

This is not a bug.  Just attaching additional document you guys can add to spring reference guide.
It took sometime for me to understand how it all works (with different property editors), and I thought it would be nice to have that in the main spring document so people don't have to jump around the hoops to pick this up.

Three Different Ways of using MultipartFile in the command

1. Using Multipart File directly

- Command bean contains MultipartFile property
- no custom property editor required

* controller
  protected void initBinder(
  PortletRequest request,
  PortletRequestDataBinder binder) throws Exception {
  // don't have to do anything
  super.initBinder(request, binder);
  }

* command
  public class FileUploadBean {
  private MultipartFile file

  public void setFile(MultipartFile file) {
  this.file = file;
  }

  public MultipartFile getFile() {
  return file;
  }
  }

2. Using ByteArrayMultipartFileEditor for retrieving uploaded file as byte[]

- Command bean contains byte[] property
- ByteArrayMultipartFileEditor should be registered for byte[]

* controller
  protected void initBinder(
  PortletRequest request,
  PortletRequestDataBinder binder) throws Exception {
  binder.registerCustomEditor(
  byte[].class,
  new ByteArrayMultipartFileEditor());
  super.initBinder(request, binder);
  }

* commmand
  public class FileUploadBean {
  private byte[] file

  public void setFile(byte[] file) {
  this.file = file;
  }

  public byte[] getFile() {
  return file;
  }
  }

3. Using StringMultipartFileEditor for  retrieving uploaded file(text based) as String

- Command bean contains String property
- StringMultipartFileEditor should be registered for String

* controller
  protected void initBinder(
  PortletRequest request,
  PortletRequestDataBinder binder) throws Exception {
  binder.registerCustomEditor(
  String.class,
  new StringMultipartFileEditor());
  super.initBinder(request, binder);
  }

* command
  public class FileUploadBean {
  private String file

  public void setFile(String file) {
  this.file = file;
  }

  public String getFile() {
  return file;
  }
  }



---

**Affects:** 1.2.5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6144","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6144/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6144/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6144/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6144","id":398061272,"node_id":"MDU6SXNzdWUzOTgwNjEyNzI=","number":6144,"title":"Additional Document for Spring - commons-fileupload integration [SPR-1444]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false,"description":"Issues in web modules (web, webmvc, webflux, websocket)"},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false,"description":"A general enhancement"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/31","html_url":"https://github.com/spring-projects/spring-framework/milestone/31","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/31/labels","id":3960802,"node_id":"MDk6TWlsZXN0b25lMzk2MDgwMg==","number":31,"title":"2.0 M5","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":82,"state":"closed","created_at":"2019-01-10T22:02:29Z","updated_at":"2019-01-10T23:47:40Z","due_on":null,"closed_at":"2019-01-10T22:02:29Z"},"comments":1,"created_at":"2005-11-05T07:50:46Z","updated_at":"2012-06-19T03:54:32Z","closed_at":"2012-06-19T03:54:32Z","author_association":"COLLABORATOR","body":"**[Yujin Kim](https://jira.spring.io/secure/ViewProfile.jspa?name=netexplode)** opened **[SPR-1444](https://jira.spring.io/browse/SPR-1444?redirect=false)** and commented\n\nThis is not a bug.  Just attaching additional document you guys can add to spring reference guide.\nIt took sometime for me to understand how it all works (with different property editors), and I thought it would be nice to have that in the main spring document so people don't have to jump around the hoops to pick this up.\n\nThree Different Ways of using MultipartFile in the command\n\n1. Using Multipart File directly\n\n- Command bean contains MultipartFile property\n- no custom property editor required\n\n* controller\n  protected void initBinder(\n  PortletRequest request,\n  PortletRequestDataBinder binder) throws Exception {\n  // don't have to do anything\n  super.initBinder(request, binder);\n  }\n\n* command\n  public class FileUploadBean {\n  private MultipartFile file\n\n  public void setFile(MultipartFile file) {\n  this.file = file;\n  }\n\n  public MultipartFile getFile() {\n  return file;\n  }\n  }\n\n2. Using ByteArrayMultipartFileEditor for retrieving uploaded file as byte[]\n\n- Command bean contains byte[] property\n- ByteArrayMultipartFileEditor should be registered for byte[]\n\n* controller\n  protected void initBinder(\n  PortletRequest request,\n  PortletRequestDataBinder binder) throws Exception {\n  binder.registerCustomEditor(\n  byte[].class,\n  new ByteArrayMultipartFileEditor());\n  super.initBinder(request, binder);\n  }\n\n* commmand\n  public class FileUploadBean {\n  private byte[] file\n\n  public void setFile(byte[] file) {\n  this.file = file;\n  }\n\n  public byte[] getFile() {\n  return file;\n  }\n  }\n\n3. Using StringMultipartFileEditor for  retrieving uploaded file(text based) as String\n\n- Command bean contains String property\n- StringMultipartFileEditor should be registered for String\n\n* controller\n  protected void initBinder(\n  PortletRequest request,\n  PortletRequestDataBinder binder) throws Exception {\n  binder.registerCustomEditor(\n  String.class,\n  new StringMultipartFileEditor());\n  super.initBinder(request, binder);\n  }\n\n* command\n  public class FileUploadBean {\n  private String file\n\n  public void setFile(String file) {\n  this.file = file;\n  }\n\n  public String getFile() {\n  return file;\n  }\n  }\n\n\n\n---\n\n**Affects:** 1.2.5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453299288"], "labels":["in: web","type: enhancement"]}