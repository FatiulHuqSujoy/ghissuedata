{"id":"15102", "title":"Derby v10.9: Embedded database namespace support causes test failures [SPR-10469]", "body":"**[Gunnar Hillert](https://jira.spring.io/secure/ViewProfile.jspa?name=hillert)** opened **[SPR-10469](https://jira.spring.io/browse/SPR-10469?redirect=false)** and commented

For the Spring Integration project, we wanted to upgrade our Derby test dependencies from 10.5.3.0_1 to a more recent version. As soon as we upgraded to a version greater than 10.5.3.0_1, we started to experience failures in our JDBC test suites.

In isolation, the tests would run fine, but as part of the build, we experienced (intermittent) test failures. Some of the errors we saw:

```
Caused by: org.springframework.dao.DataAccessResourceFailureException: Failed to execute database script; nested exception is java.sql.SQLNonTransientConnectionException: No current connection.
    at org.springframework.jdbc.datasource.init.DatabasePopulatorUtils.execute(DatabasePopulatorUtils.java:56)
    at org.springframework.jdbc.datasource.init.DataSourceInitializer.afterPropertiesSet(DataSourceInitializer.java:83)
    at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1514)
    at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1452)
    ... 57 more
Caused by: java.sql.SQLNonTransientConnectionException: No current connection.
    at org.apache.derby.impl.jdbc.SQLExceptionFactory40.getSQLException(Unknown Source)
    at org.apache.derby.impl.jdbc.Util.newEmbedSQLException(Unknown Source)
    at org.apache.derby.impl.jdbc.Util.newEmbedSQLException(Unknown Source)
    at org.apache.derby.impl.jdbc.Util.noCurrentConnection(Unknown Source)
    at org.apache.derby.impl.jdbc.EmbedConnection.checkIfClosed(Unknown Source)
    at org.apache.derby.impl.jdbc.EmbedConnection.createStatement(Unknown Source)
    at org.apache.derby.impl.jdbc.EmbedConnection.createStatement(Unknown Source)
    at org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.executeSqlScript(ResourceDatabasePopulator.java:180)
    at org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.populate(ResourceDatabasePopulator.java:133)
    at org.springframework.jdbc.datasource.init.CompositeDatabasePopulator.populate(CompositeDatabasePopulator.java:55)
    at org.springframework.jdbc.datasource.init.DatabasePopulatorUtils.execute(DatabasePopulatorUtils.java:47)
    ... 60 more
Caused by: java.sql.SQLException: No current connection.
    at org.apache.derby.impl.jdbc.SQLExceptionFactory.getSQLException(Unknown Source)
    at org.apache.derby.impl.jdbc.SQLExceptionFactory40.wrapArgsForTransportAcrossDRDA(Unknown Source)
    ... 71 more
```

```
Caused by: java.sql.SQLException: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. 
	at org.apache.derby.impl.jdbc.SQLExceptionFactory40.getSQLException(Unknown Source)
	at org.apache.derby.impl.jdbc.Util.generateCsSQLException(Unknown Source)
	at org.apache.derby.impl.jdbc.TransactionResourceImpl.wrapInSQLException(Unknown Source)
	at org.apache.derby.impl.jdbc.TransactionResourceImpl.handleException(Unknown Source)
	at org.apache.derby.impl.jdbc.EmbedConnection.handleException(Unknown Source)
	at org.apache.derby.impl.jdbc.ConnectionChild.handleException(Unknown Source)
	at org.apache.derby.impl.jdbc.EmbedStatement.executeStatement(Unknown Source)
	at org.apache.derby.impl.jdbc.EmbedStatement.execute(Unknown Source)
	at org.apache.derby.impl.jdbc.EmbedStatement.execute(Unknown Source)
	at org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.executeSqlScript(ResourceDatabasePopulator.java:185)
	... 44 more
Caused by: java.sql.SQLException: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. 
	at org.apache.derby.impl.jdbc.SQLExceptionFactory.getSQLException(Unknown Source)
	at org.apache.derby.impl.jdbc.SQLExceptionFactory40.wrapArgsForTransportAcrossDRDA(Unknown Source)
	... 54 more
Caused by: ERROR XBCA0: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. 
	at org.apache.derby.iapi.error.StandardException.newException(Unknown Source)
	at org.apache.derby.impl.services.cache.ConcurrentCache.create(Unknown Source)
	at org.apache.derby.impl.store.raw.data.BaseDataFileFactory.addContainer(Unknown Source)
	at org.apache.derby.impl.store.raw.xact.Xact.addContainer(Unknown Source)
	at org.apache.derby.impl.store.access.heap.Heap.create(Unknown Source)
	at org.apache.derby.impl.store.access.heap.HeapConglomerateFactory.createConglomerate(Unknown Source)
	at org.apache.derby.impl.store.access.RAMTransaction.createConglomerate(Unknown Source)
	at org.apache.derby.impl.sql.execute.CreateTableConstantAction.executeConstantAction(Unknown Source)
	at org.apache.derby.impl.sql.execute.MiscResultSet.open(Unknown Source)
	at org.apache.derby.impl.sql.GenericPreparedStatement.executeStmt(Unknown Source)
	at org.apache.derby.impl.sql.GenericPreparedStatement.execute(Unknown Source)
	... 48 more
```

**Workaround**

I was able to solve the issue by switching from:

```
<jdbc:embedded-database id=\"dataSource\" type=\"DERBY\"/>
```

to

```
<bean id=\"dataSource\" class=\"org.springframework.jdbc.datasource.DriverManagerDataSource\" >
    <property name=\"driverClassName\" value=\"org.apache.derby.jdbc.EmbeddedDriver\" />
    <property name=\"url\" value=\"jdbc:derby:memory:myDB;create=true\" />
    <property name=\"username\" value=\"sa\" />
    <property name=\"password\" value=\"sa\" />
</bean>
```

See also: [INT-2992](https://jira.spring.io/browse/INT-2992)

---

**Affects:** 3.1.3

**Attachments:**
- [demo.JdbcPollingChannelAdapterTests.html](https://jira.spring.io/secure/attachment/21166/demo.JdbcPollingChannelAdapterTests.html) (_23.58 kB_)

**Issue Links:**
- #12258 Embedded database support in trouble shutting down Derby
- [INTSCALA-53](https://jira.spring.io/browse/INTSCALA-53) Upgrade to Gradle 1.6
- [INT-2992](https://jira.spring.io/browse/INT-2992) Upgrade Derby Test Dependencies

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/d032c20a54c3289ef26656f4e96321c39c1ad49b, https://github.com/spring-projects/spring-framework/commit/b4d6e27fb3f8db773cbfad1a6e53323de2c1470d

0 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15102","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15102/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15102/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15102/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15102","id":398158500,"node_id":"MDU6SXNzdWUzOTgxNTg1MDA=","number":15102,"title":"Derby v10.9: Embedded database namespace support causes test failures [SPR-10469]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2013-04-16T12:01:06Z","updated_at":"2019-01-11T21:12:03Z","closed_at":"2018-05-03T16:06:34Z","author_association":"COLLABORATOR","body":"**[Gunnar Hillert](https://jira.spring.io/secure/ViewProfile.jspa?name=hillert)** opened **[SPR-10469](https://jira.spring.io/browse/SPR-10469?redirect=false)** and commented\n\nFor the Spring Integration project, we wanted to upgrade our Derby test dependencies from 10.5.3.0_1 to a more recent version. As soon as we upgraded to a version greater than 10.5.3.0_1, we started to experience failures in our JDBC test suites.\n\nIn isolation, the tests would run fine, but as part of the build, we experienced (intermittent) test failures. Some of the errors we saw:\n\n```\nCaused by: org.springframework.dao.DataAccessResourceFailureException: Failed to execute database script; nested exception is java.sql.SQLNonTransientConnectionException: No current connection.\n    at org.springframework.jdbc.datasource.init.DatabasePopulatorUtils.execute(DatabasePopulatorUtils.java:56)\n    at org.springframework.jdbc.datasource.init.DataSourceInitializer.afterPropertiesSet(DataSourceInitializer.java:83)\n    at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1514)\n    at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1452)\n    ... 57 more\nCaused by: java.sql.SQLNonTransientConnectionException: No current connection.\n    at org.apache.derby.impl.jdbc.SQLExceptionFactory40.getSQLException(Unknown Source)\n    at org.apache.derby.impl.jdbc.Util.newEmbedSQLException(Unknown Source)\n    at org.apache.derby.impl.jdbc.Util.newEmbedSQLException(Unknown Source)\n    at org.apache.derby.impl.jdbc.Util.noCurrentConnection(Unknown Source)\n    at org.apache.derby.impl.jdbc.EmbedConnection.checkIfClosed(Unknown Source)\n    at org.apache.derby.impl.jdbc.EmbedConnection.createStatement(Unknown Source)\n    at org.apache.derby.impl.jdbc.EmbedConnection.createStatement(Unknown Source)\n    at org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.executeSqlScript(ResourceDatabasePopulator.java:180)\n    at org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.populate(ResourceDatabasePopulator.java:133)\n    at org.springframework.jdbc.datasource.init.CompositeDatabasePopulator.populate(CompositeDatabasePopulator.java:55)\n    at org.springframework.jdbc.datasource.init.DatabasePopulatorUtils.execute(DatabasePopulatorUtils.java:47)\n    ... 60 more\nCaused by: java.sql.SQLException: No current connection.\n    at org.apache.derby.impl.jdbc.SQLExceptionFactory.getSQLException(Unknown Source)\n    at org.apache.derby.impl.jdbc.SQLExceptionFactory40.wrapArgsForTransportAcrossDRDA(Unknown Source)\n    ... 71 more\n```\n\n```\nCaused by: java.sql.SQLException: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. \n\tat org.apache.derby.impl.jdbc.SQLExceptionFactory40.getSQLException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.Util.generateCsSQLException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.TransactionResourceImpl.wrapInSQLException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.TransactionResourceImpl.handleException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.EmbedConnection.handleException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.ConnectionChild.handleException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.EmbedStatement.executeStatement(Unknown Source)\n\tat org.apache.derby.impl.jdbc.EmbedStatement.execute(Unknown Source)\n\tat org.apache.derby.impl.jdbc.EmbedStatement.execute(Unknown Source)\n\tat org.springframework.jdbc.datasource.init.ResourceDatabasePopulator.executeSqlScript(ResourceDatabasePopulator.java:185)\n\t... 44 more\nCaused by: java.sql.SQLException: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. \n\tat org.apache.derby.impl.jdbc.SQLExceptionFactory.getSQLException(Unknown Source)\n\tat org.apache.derby.impl.jdbc.SQLExceptionFactory40.wrapArgsForTransportAcrossDRDA(Unknown Source)\n\t... 54 more\nCaused by: ERROR XBCA0: Cannot create new object with key Container(0, 16) in ContainerCache cache. The object already exists in the cache. \n\tat org.apache.derby.iapi.error.StandardException.newException(Unknown Source)\n\tat org.apache.derby.impl.services.cache.ConcurrentCache.create(Unknown Source)\n\tat org.apache.derby.impl.store.raw.data.BaseDataFileFactory.addContainer(Unknown Source)\n\tat org.apache.derby.impl.store.raw.xact.Xact.addContainer(Unknown Source)\n\tat org.apache.derby.impl.store.access.heap.Heap.create(Unknown Source)\n\tat org.apache.derby.impl.store.access.heap.HeapConglomerateFactory.createConglomerate(Unknown Source)\n\tat org.apache.derby.impl.store.access.RAMTransaction.createConglomerate(Unknown Source)\n\tat org.apache.derby.impl.sql.execute.CreateTableConstantAction.executeConstantAction(Unknown Source)\n\tat org.apache.derby.impl.sql.execute.MiscResultSet.open(Unknown Source)\n\tat org.apache.derby.impl.sql.GenericPreparedStatement.executeStmt(Unknown Source)\n\tat org.apache.derby.impl.sql.GenericPreparedStatement.execute(Unknown Source)\n\t... 48 more\n```\n\n**Workaround**\n\nI was able to solve the issue by switching from:\n\n```\n<jdbc:embedded-database id=\"dataSource\" type=\"DERBY\"/>\n```\n\nto\n\n```\n<bean id=\"dataSource\" class=\"org.springframework.jdbc.datasource.DriverManagerDataSource\" >\n    <property name=\"driverClassName\" value=\"org.apache.derby.jdbc.EmbeddedDriver\" />\n    <property name=\"url\" value=\"jdbc:derby:memory:myDB;create=true\" />\n    <property name=\"username\" value=\"sa\" />\n    <property name=\"password\" value=\"sa\" />\n</bean>\n```\n\nSee also: [INT-2992](https://jira.spring.io/browse/INT-2992)\n\n---\n\n**Affects:** 3.1.3\n\n**Attachments:**\n- [demo.JdbcPollingChannelAdapterTests.html](https://jira.spring.io/secure/attachment/21166/demo.JdbcPollingChannelAdapterTests.html) (_23.58 kB_)\n\n**Issue Links:**\n- #12258 Embedded database support in trouble shutting down Derby\n- [INTSCALA-53](https://jira.spring.io/browse/INTSCALA-53) Upgrade to Gradle 1.6\n- [INT-2992](https://jira.spring.io/browse/INT-2992) Upgrade Derby Test Dependencies\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/d032c20a54c3289ef26656f4e96321c39c1ad49b, https://github.com/spring-projects/spring-framework/commit/b4d6e27fb3f8db773cbfad1a6e53323de2c1470d\n\n0 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453402413","453402414","453402415","453402416"], "labels":["in: data","status: declined","type: enhancement"]}