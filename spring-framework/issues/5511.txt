{"id":"5511", "title":"Sequence of call with MethodInvokingJobDetailFactoryBean [SPR-784]", "body":"**[Peter Veentjer](https://jira.spring.io/secure/ViewProfile.jspa?name=alarmnummer)** opened **[SPR-784](https://jira.spring.io/browse/SPR-784?redirect=false)** and commented

At the moment it is possible to call a single method on a bean with the MethodInvokingJobDetailFactoryBean. I would like to see a new JobDetailFactoryBean that can do a sequence of calls. Without such a structure I must create a normal pojo just to chain the calls.

I took a look at the MethodInvokdingJobDetailFactoryBean and it wouldn`t be to hard to add such functionality.

[code]
package org.springframework.scheduling.quartz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.MethodInvoker;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class MethodInvokingSequenceJobDetailFactoryBean implements FactoryBean, BeanNameAware, InitializingBean {

    private String name;
    
    private String group = Scheduler.DEFAULT_GROUP;
    
    private boolean concurrent = true;
    
    private String beanName;
    
    private JobDetail jobDetail;
    private List methodInvokerList;
    
    
    /**
     * Set the name of the job.
     * Default is the bean name of this FactoryBean.
     * @see org.quartz.JobDetail#setName
     */
    public void setName(String name) {
    	this.name = name;
    }
    
    /**
     * Set the group of the job.
     * Default is the default group of the Scheduler.
     * @see org.quartz.JobDetail#setGroup
     * @see org.quartz.Scheduler#DEFAULT_GROUP
     */
    public void setGroup(String group) {
    	this.group = group;
    }
    
    /**
     * Specify whether or not multiple jobs should be run in a concurrent
     * fashion. The behavior when one does not want concurrent jobs to be
     * executed is realized through adding the {@link StatefulJob} interface.
     * More information on stateful versus stateless jobs can be found
     * <a href=\"http://www.opensymphony.com/quartz/tutorial.html#jobsMore\">here</a>.
     * <p>The default setting is to run jobs concurrently.
     * @param concurrent whether one wants to execute multiple jobs created
     * by this bean concurrently
     */
    public void setConcurrent(boolean concurrent) {
    	this.concurrent = concurrent;
    }
    
    public void setBeanName(String beanName) {
    	this.beanName = beanName;
    }
    
    
    public void afterPropertiesSet() throws ClassNotFoundException, NoSuchMethodException {
    //	prepare();
    
    	// Use specific name if given, else fall back to bean name.
    	String name = (this.name != null ? this.name : this.beanName);
    
    	// Consider the concurrent flag to choose between stateful and stateless job.
    	Class jobClass = (this.concurrent ? MethodInvokingJob.class : StatefulMethodInvokingJob.class);
    
    	this.jobDetail = new JobDetail(name, this.group, jobClass);
    	this.jobDetail.getJobDataMap().put(\"methodInvokerList\", methodInvokerList);
    	this.jobDetail.setVolatility(true);
    }
    
    public Object getObject() {
    	return this.jobDetail;
    }
    
    public Class getObjectType() {
    	return (this.jobDetail != null) ? this.jobDetail.getClass() : JobDetail.class;
    }
    
    public boolean isSingleton() {
    	return true;
    }
    
    public void setMethodInvokerList(List methodInvokerList){
    	if(methodInvokerList==null)
    	    throw new NullPointerException(\"methodInvokerList can`t be null\");
    	this.methodInvokerList = methodInvokerList;
    }
    
    /**
     * Quartz Job implementation that invokes a specified method.
     * Automatically applied by MethodInvokingJobDetailFactoryBean.
     */
    public static class MethodInvokingJob extends QuartzJobBean {
    
    	protected static final Log logger = LogFactory.getLog(MethodInvokingJob.class);
    
    	private List methodInvokerList;
    
    	/**
    	 * Set the MethodInvoker to use.
    	 */
    	public void setMethodInvokerList(List  methodInvokerList) {
    		this.methodInvokerList = methodInvokerList;
    		
    	}
    
    	private String createErrorMsg(MethodInvoker methodInvoker){
    		return \"Could not invoke method '\" + methodInvoker.getTargetMethod() +
    				\"' on target object [\" + methodInvoker.getTargetObject() + \"]\";
    	}
    	
    	/**
    	 * Invoke the method via the MethodInvoker.
    	 */
    	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
    		MethodInvoker methodInvoker = null;
    		try {
    			for(int k=0;k<methodInvokerList.size();k++){
    				methodInvoker = (MethodInvoker)methodInvokerList.get(k);
    				methodInvoker.prepare();
    				methodInvoker.invoke();
    			}
    		}
    		catch (InvocationTargetException ex) {
    			String errorMsg = createErrorMsg(methodInvoker);
    			logger.warn(errorMsg + \": \" + ex.getTargetException().getMessage());
    			if (ex.getTargetException() instanceof JobExecutionException) {
    				throw (JobExecutionException) ex.getTargetException();
    			}
    			Exception jobEx = (ex.getTargetException() instanceof Exception) ?
    					(Exception) ex.getTargetException() : ex;
    			throw new JobExecutionException(errorMsg, jobEx, false);
    		}
    		catch (Exception ex) {
    			String errorMsg = createErrorMsg(methodInvoker);
    			logger.warn(errorMsg + \": \" + ex.getMessage());
    			throw new JobExecutionException(errorMsg, ex, false);
    		}
    	}
    }
    
    
    /**
     * Extension of the MethodInvokingJob, implementing the StatefulJob interface.
     * Quartz checks whether or not jobs are stateful and if so,
     * won't let jobs interfere with each other.
     */
    public static class StatefulMethodInvokingJob extends MethodInvokingJob implements StatefulJob {
    	// No implementation, just a addition of the tag interface StatefulJob
    	// in order to allow stateful method invoking jobs.
    }

}
[/code]

Only the ArgumentConvertingMethodInvoker is missing.. but it would not be to hard to add it.

example of usage:
[code]
\\<bean id=\"indexWriterJobCronTrigger\"
class=\"org.springframework.scheduling.quartz.CronTriggerBean\">

    	<property name=\"jobDetail\">
    		<bean class=\"com.jph.spring.MethodInvokingSequenceJobDetailFactoryBean\">
    			<property name=\"methodInvokerList\">
    				<list>
    					<bean class=\"org.springframework.util.MethodInvoker\">
    						<property name=\"targetObject\">
    							<ref bean=\"indexWriter\"/>
    						</property>
    
    						<property name=\"targetMethod\">
    							<value>processQueue</value>
    						</property>
    					</bean>
    
    					<bean class=\"org.springframework.util.MethodInvoker\">
    						<property name=\"targetObject\">
    							<ref bean=\"baseSearcher\"/>
    						</property>
    
    						<property name=\"targetMethod\">
    							<value>refreshReader</value>
    						</property>
    					</bean>
    				</list>
    			</property>
    
    			<property name=\"concurrent\">
    				<value>false</value>
    			</property>
    
    		</bean>
    	</property>
    
    	<property name=\"cronExpression\">
    		<!-- iedere minuut -->
    		<value>0 0/1 * * * ?</value>
    	</property>
    </bean>

[/code]

This information also can be found in topic:
http://forum.springframework.org/viewtopic.php?t=3881


---
No further details from [SPR-784](https://jira.spring.io/browse/SPR-784?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5511","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5511/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5511/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5511/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5511","id":398055611,"node_id":"MDU6SXNzdWUzOTgwNTU2MTE=","number":5511,"title":"Sequence of call with MethodInvokingJobDetailFactoryBean [SPR-784]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2005-03-15T04:18:20Z","updated_at":"2012-06-19T09:37:28Z","closed_at":"2012-06-19T09:37:28Z","author_association":"COLLABORATOR","body":"**[Peter Veentjer](https://jira.spring.io/secure/ViewProfile.jspa?name=alarmnummer)** opened **[SPR-784](https://jira.spring.io/browse/SPR-784?redirect=false)** and commented\n\nAt the moment it is possible to call a single method on a bean with the MethodInvokingJobDetailFactoryBean. I would like to see a new JobDetailFactoryBean that can do a sequence of calls. Without such a structure I must create a normal pojo just to chain the calls.\n\nI took a look at the MethodInvokdingJobDetailFactoryBean and it wouldn`t be to hard to add such functionality.\n\n[code]\npackage org.springframework.scheduling.quartz;\n\nimport org.apache.commons.logging.Log;\nimport org.apache.commons.logging.LogFactory;\nimport org.quartz.*;\nimport org.springframework.beans.factory.BeanNameAware;\nimport org.springframework.beans.factory.FactoryBean;\nimport org.springframework.beans.factory.InitializingBean;\nimport org.springframework.util.MethodInvoker;\n\nimport java.lang.reflect.InvocationTargetException;\nimport java.util.List;\n\npublic class MethodInvokingSequenceJobDetailFactoryBean implements FactoryBean, BeanNameAware, InitializingBean {\n\n    private String name;\n    \n    private String group = Scheduler.DEFAULT_GROUP;\n    \n    private boolean concurrent = true;\n    \n    private String beanName;\n    \n    private JobDetail jobDetail;\n    private List methodInvokerList;\n    \n    \n    /**\n     * Set the name of the job.\n     * Default is the bean name of this FactoryBean.\n     * @see org.quartz.JobDetail#setName\n     */\n    public void setName(String name) {\n    \tthis.name = name;\n    }\n    \n    /**\n     * Set the group of the job.\n     * Default is the default group of the Scheduler.\n     * @see org.quartz.JobDetail#setGroup\n     * @see org.quartz.Scheduler#DEFAULT_GROUP\n     */\n    public void setGroup(String group) {\n    \tthis.group = group;\n    }\n    \n    /**\n     * Specify whether or not multiple jobs should be run in a concurrent\n     * fashion. The behavior when one does not want concurrent jobs to be\n     * executed is realized through adding the {@link StatefulJob} interface.\n     * More information on stateful versus stateless jobs can be found\n     * <a href=\"http://www.opensymphony.com/quartz/tutorial.html#jobsMore\">here</a>.\n     * <p>The default setting is to run jobs concurrently.\n     * @param concurrent whether one wants to execute multiple jobs created\n     * by this bean concurrently\n     */\n    public void setConcurrent(boolean concurrent) {\n    \tthis.concurrent = concurrent;\n    }\n    \n    public void setBeanName(String beanName) {\n    \tthis.beanName = beanName;\n    }\n    \n    \n    public void afterPropertiesSet() throws ClassNotFoundException, NoSuchMethodException {\n    //\tprepare();\n    \n    \t// Use specific name if given, else fall back to bean name.\n    \tString name = (this.name != null ? this.name : this.beanName);\n    \n    \t// Consider the concurrent flag to choose between stateful and stateless job.\n    \tClass jobClass = (this.concurrent ? MethodInvokingJob.class : StatefulMethodInvokingJob.class);\n    \n    \tthis.jobDetail = new JobDetail(name, this.group, jobClass);\n    \tthis.jobDetail.getJobDataMap().put(\"methodInvokerList\", methodInvokerList);\n    \tthis.jobDetail.setVolatility(true);\n    }\n    \n    public Object getObject() {\n    \treturn this.jobDetail;\n    }\n    \n    public Class getObjectType() {\n    \treturn (this.jobDetail != null) ? this.jobDetail.getClass() : JobDetail.class;\n    }\n    \n    public boolean isSingleton() {\n    \treturn true;\n    }\n    \n    public void setMethodInvokerList(List methodInvokerList){\n    \tif(methodInvokerList==null)\n    \t    throw new NullPointerException(\"methodInvokerList can`t be null\");\n    \tthis.methodInvokerList = methodInvokerList;\n    }\n    \n    /**\n     * Quartz Job implementation that invokes a specified method.\n     * Automatically applied by MethodInvokingJobDetailFactoryBean.\n     */\n    public static class MethodInvokingJob extends QuartzJobBean {\n    \n    \tprotected static final Log logger = LogFactory.getLog(MethodInvokingJob.class);\n    \n    \tprivate List methodInvokerList;\n    \n    \t/**\n    \t * Set the MethodInvoker to use.\n    \t */\n    \tpublic void setMethodInvokerList(List  methodInvokerList) {\n    \t\tthis.methodInvokerList = methodInvokerList;\n    \t\t\n    \t}\n    \n    \tprivate String createErrorMsg(MethodInvoker methodInvoker){\n    \t\treturn \"Could not invoke method '\" + methodInvoker.getTargetMethod() +\n    \t\t\t\t\"' on target object [\" + methodInvoker.getTargetObject() + \"]\";\n    \t}\n    \t\n    \t/**\n    \t * Invoke the method via the MethodInvoker.\n    \t */\n    \tprotected void executeInternal(JobExecutionContext context) throws JobExecutionException {\n    \t\tMethodInvoker methodInvoker = null;\n    \t\ttry {\n    \t\t\tfor(int k=0;k<methodInvokerList.size();k++){\n    \t\t\t\tmethodInvoker = (MethodInvoker)methodInvokerList.get(k);\n    \t\t\t\tmethodInvoker.prepare();\n    \t\t\t\tmethodInvoker.invoke();\n    \t\t\t}\n    \t\t}\n    \t\tcatch (InvocationTargetException ex) {\n    \t\t\tString errorMsg = createErrorMsg(methodInvoker);\n    \t\t\tlogger.warn(errorMsg + \": \" + ex.getTargetException().getMessage());\n    \t\t\tif (ex.getTargetException() instanceof JobExecutionException) {\n    \t\t\t\tthrow (JobExecutionException) ex.getTargetException();\n    \t\t\t}\n    \t\t\tException jobEx = (ex.getTargetException() instanceof Exception) ?\n    \t\t\t\t\t(Exception) ex.getTargetException() : ex;\n    \t\t\tthrow new JobExecutionException(errorMsg, jobEx, false);\n    \t\t}\n    \t\tcatch (Exception ex) {\n    \t\t\tString errorMsg = createErrorMsg(methodInvoker);\n    \t\t\tlogger.warn(errorMsg + \": \" + ex.getMessage());\n    \t\t\tthrow new JobExecutionException(errorMsg, ex, false);\n    \t\t}\n    \t}\n    }\n    \n    \n    /**\n     * Extension of the MethodInvokingJob, implementing the StatefulJob interface.\n     * Quartz checks whether or not jobs are stateful and if so,\n     * won't let jobs interfere with each other.\n     */\n    public static class StatefulMethodInvokingJob extends MethodInvokingJob implements StatefulJob {\n    \t// No implementation, just a addition of the tag interface StatefulJob\n    \t// in order to allow stateful method invoking jobs.\n    }\n\n}\n[/code]\n\nOnly the ArgumentConvertingMethodInvoker is missing.. but it would not be to hard to add it.\n\nexample of usage:\n[code]\n\\<bean id=\"indexWriterJobCronTrigger\"\nclass=\"org.springframework.scheduling.quartz.CronTriggerBean\">\n\n    \t<property name=\"jobDetail\">\n    \t\t<bean class=\"com.jph.spring.MethodInvokingSequenceJobDetailFactoryBean\">\n    \t\t\t<property name=\"methodInvokerList\">\n    \t\t\t\t<list>\n    \t\t\t\t\t<bean class=\"org.springframework.util.MethodInvoker\">\n    \t\t\t\t\t\t<property name=\"targetObject\">\n    \t\t\t\t\t\t\t<ref bean=\"indexWriter\"/>\n    \t\t\t\t\t\t</property>\n    \n    \t\t\t\t\t\t<property name=\"targetMethod\">\n    \t\t\t\t\t\t\t<value>processQueue</value>\n    \t\t\t\t\t\t</property>\n    \t\t\t\t\t</bean>\n    \n    \t\t\t\t\t<bean class=\"org.springframework.util.MethodInvoker\">\n    \t\t\t\t\t\t<property name=\"targetObject\">\n    \t\t\t\t\t\t\t<ref bean=\"baseSearcher\"/>\n    \t\t\t\t\t\t</property>\n    \n    \t\t\t\t\t\t<property name=\"targetMethod\">\n    \t\t\t\t\t\t\t<value>refreshReader</value>\n    \t\t\t\t\t\t</property>\n    \t\t\t\t\t</bean>\n    \t\t\t\t</list>\n    \t\t\t</property>\n    \n    \t\t\t<property name=\"concurrent\">\n    \t\t\t\t<value>false</value>\n    \t\t\t</property>\n    \n    \t\t</bean>\n    \t</property>\n    \n    \t<property name=\"cronExpression\">\n    \t\t<!-- iedere minuut -->\n    \t\t<value>0 0/1 * * * ?</value>\n    \t</property>\n    </bean>\n\n[/code]\n\nThis information also can be found in topic:\nhttp://forum.springframework.org/viewtopic.php?t=3881\n\n\n---\nNo further details from [SPR-784](https://jira.spring.io/browse/SPR-784?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453292531"], "labels":["in: core","status: declined","type: enhancement"]}