{"id":"10781", "title":"(netbeans,macosx) no declaration can be found for element 'tx:annotation-driven' [SPR-6113]", "body":"**[olivier SAINT-EVE](https://jira.spring.io/secure/ViewProfile.jspa?name=lolveley)** opened **[SPR-6113](https://jira.spring.io/browse/SPR-6113?redirect=false)** and commented

hello,

i'm a newbie in spring, so please be indulgent with me!
here is the issue : I'm following a tutorial about netbeans, spring and JEE(here : http://tahe.developpez.com/java/javaee/ , in French )
I use in my project the following spring-config-dao.xml :

*********************************************************************************************************************

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:util=\"http://www.springframework.org/schema/util\"
xmlns:context=\"http://www.springframework.org/schema/context\"
xmlns:tx=\"http://www.springframework.org/schema/tx\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd\">

\\<!--- couches applicatives -->
\\<bean id=\"cotisationDao\" class=\"dao.CotisationDao\"/>
\\<bean id=\"employeDao\" class=\"dao.EmployeDao\"/>
\\<bean id=\"indemnitesDao\" class=\"dao.IndemnitesDao\"/>
\\<!--- couche de persistance JPA -->

\\<bean id=\"entityManagerFactory\"
class=\"org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean\">
\\<property name=\"dataSource\" ref=\"dataSource\" />
\\<property name=\"jpaVendorAdapter\">
\\<bean
class=\"org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter\">

\\<!--
\\<property name=\"showSql\" value=\"true\" />
-->

\\<property name=\"databasePlatform\"
value=\"org.hibernate.dialect.MySQL5InnoDBDialect\" />
\\<property name=\"generateDdl\" value=\"true\" />
\\</bean>
\\</property>
\\<property name=\"loadTimeWeaver\">
\\<bean
class=\"org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver\" />
\\</property>
\\</bean>

\\<!--- la source de donnéees DBCP -->

\\<bean id=\"dataSource\"
class=\"org.apache.commons.dbcp.BasicDataSource\"
destroy-method=\"close\">
\\<property name=\"driverClassName\" value=\"com.mysql.jdbc.Driver\" />
\\<property name=\"url\" value=\"jdbc:mysql://localhost:3306/jbossdb\" />
\\<property name=\"username\" value=\"myUserName\" />
\\<property name=\"password\" value=\"myPassword\" />
\\</bean>

\\<!--- le gestionnaire de transactions -->

<tx:annotation-driven transaction-manager=\"txManager\" />
\\<bean id=\"txManager\"
class=\"org.springframework.orm.jpa.JpaTransactionManager\">
\\<property name=\"entityManagerFactory\"
ref=\"entityManagerFactory\" />
\\</bean>

\\<!--- traduction des exceptions -->

\\<bean
class=\"org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor\" />

\\<!--- annotations de persistance -->

\\<bean
class=\"org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor\" />

\\</beans>

*********************************************************************************************************************

here is the persistence.xml:

*********************************************************************************************************************

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>
\\<persistence version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/persistence\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd\">
\\<persistence-unit name=\"pam-spring-hibernatePU\" transaction-type=\"RESOURCE_LOCAL\">
\\<provider>org.hibernate.ejb.HibernatePersistence\\</provider>
\\<properties>
\\<property name=\"hibernate.connection.username\" value=\"myUserName\"/>
\\<property name=\"hibernate.connection.driver_class\" value=\"com.mysql.jdbc.Driver\"/>
\\<property name=\"hibernate.connection.password\" value=\"myPassword\"/>
\\<property name=\"hibernate.connection.url\" value=\"jdbc:mysql://localhost:3306/jbossdb\"/>
\\<property name=\"hibernate.cache.provider_class\" value=\"org.hibernate.cache.NoCacheProvider\"/>
\\<property name=\"hibernate.hbm2ddl.auto\" value=\"create-drop\"/>
\\</properties>
\\</persistence-unit>
\\</persistence>
*********************************************************************************************************************
here is the error message:

*********************************************************************************************************************

------------- Standard Error -----------------
log4j:WARN No appenders could be found for logger (org.springframework.context.support.ClassPathXmlApplicationContext).
log4j:WARN Please initialize the log4j system properly.

------------- ---------------- ---------------

Testcase: main(dao.InitDB):        Caused an ERROR
Line 44 in XML document from class path resource [spring-config-dao.xml] is invalid; nested exception is org.xml.sax.SAXParseException: cvc-complex-type.2.4.c: The matching wildcard is strict, but no declaration can be found for element 'tx:annotation-driven'.
org.springframework.beans.factory.xml.XmlBeanDefinitionStoreException: Line 44 in XML document from class path resource [spring-config-dao.xml] is invalid; nested exception is org.xml.sax.SAXParseException: cvc-complex-type.2.4.c: The matching wildcard is strict, but no declaration can be found for element 'tx:annotation-driven'.

*********************************************************************************************************************

the database is running well...
Can you help me?

olivier.



---

**Affects:** 2.5.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10781","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10781/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10781/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10781/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10781","id":398097495,"node_id":"MDU6SXNzdWUzOTgwOTc0OTU=","number":10781,"title":"(netbeans,macosx) no declaration can be found for element 'tx:annotation-driven' [SPR-6113]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2009-09-15T07:41:24Z","updated_at":"2019-01-12T05:33:04Z","closed_at":"2012-06-19T02:28:22Z","author_association":"COLLABORATOR","body":"**[olivier SAINT-EVE](https://jira.spring.io/secure/ViewProfile.jspa?name=lolveley)** opened **[SPR-6113](https://jira.spring.io/browse/SPR-6113?redirect=false)** and commented\n\nhello,\n\ni'm a newbie in spring, so please be indulgent with me!\nhere is the issue : I'm following a tutorial about netbeans, spring and JEE(here : http://tahe.developpez.com/java/javaee/ , in French )\nI use in my project the following spring-config-dao.xml :\n\n*********************************************************************************************************************\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:util=\"http://www.springframework.org/schema/util\"\nxmlns:context=\"http://www.springframework.org/schema/context\"\nxmlns:tx=\"http://www.springframework.org/schema/tx\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd\nhttp://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd\nhttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd\nhttp://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd\">\n\n\\<!--- couches applicatives -->\n\\<bean id=\"cotisationDao\" class=\"dao.CotisationDao\"/>\n\\<bean id=\"employeDao\" class=\"dao.EmployeDao\"/>\n\\<bean id=\"indemnitesDao\" class=\"dao.IndemnitesDao\"/>\n\\<!--- couche de persistance JPA -->\n\n\\<bean id=\"entityManagerFactory\"\nclass=\"org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean\">\n\\<property name=\"dataSource\" ref=\"dataSource\" />\n\\<property name=\"jpaVendorAdapter\">\n\\<bean\nclass=\"org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter\">\n\n\\<!--\n\\<property name=\"showSql\" value=\"true\" />\n-->\n\n\\<property name=\"databasePlatform\"\nvalue=\"org.hibernate.dialect.MySQL5InnoDBDialect\" />\n\\<property name=\"generateDdl\" value=\"true\" />\n\\</bean>\n\\</property>\n\\<property name=\"loadTimeWeaver\">\n\\<bean\nclass=\"org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver\" />\n\\</property>\n\\</bean>\n\n\\<!--- la source de donnéees DBCP -->\n\n\\<bean id=\"dataSource\"\nclass=\"org.apache.commons.dbcp.BasicDataSource\"\ndestroy-method=\"close\">\n\\<property name=\"driverClassName\" value=\"com.mysql.jdbc.Driver\" />\n\\<property name=\"url\" value=\"jdbc:mysql://localhost:3306/jbossdb\" />\n\\<property name=\"username\" value=\"myUserName\" />\n\\<property name=\"password\" value=\"myPassword\" />\n\\</bean>\n\n\\<!--- le gestionnaire de transactions -->\n\n<tx:annotation-driven transaction-manager=\"txManager\" />\n\\<bean id=\"txManager\"\nclass=\"org.springframework.orm.jpa.JpaTransactionManager\">\n\\<property name=\"entityManagerFactory\"\nref=\"entityManagerFactory\" />\n\\</bean>\n\n\\<!--- traduction des exceptions -->\n\n\\<bean\nclass=\"org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor\" />\n\n\\<!--- annotations de persistance -->\n\n\\<bean\nclass=\"org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor\" />\n\n\\</beans>\n\n*********************************************************************************************************************\n\nhere is the persistence.xml:\n\n*********************************************************************************************************************\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\\<persistence version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/persistence\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd\">\n\\<persistence-unit name=\"pam-spring-hibernatePU\" transaction-type=\"RESOURCE_LOCAL\">\n\\<provider>org.hibernate.ejb.HibernatePersistence\\</provider>\n\\<properties>\n\\<property name=\"hibernate.connection.username\" value=\"myUserName\"/>\n\\<property name=\"hibernate.connection.driver_class\" value=\"com.mysql.jdbc.Driver\"/>\n\\<property name=\"hibernate.connection.password\" value=\"myPassword\"/>\n\\<property name=\"hibernate.connection.url\" value=\"jdbc:mysql://localhost:3306/jbossdb\"/>\n\\<property name=\"hibernate.cache.provider_class\" value=\"org.hibernate.cache.NoCacheProvider\"/>\n\\<property name=\"hibernate.hbm2ddl.auto\" value=\"create-drop\"/>\n\\</properties>\n\\</persistence-unit>\n\\</persistence>\n*********************************************************************************************************************\nhere is the error message:\n\n*********************************************************************************************************************\n\n------------- Standard Error -----------------\nlog4j:WARN No appenders could be found for logger (org.springframework.context.support.ClassPathXmlApplicationContext).\nlog4j:WARN Please initialize the log4j system properly.\n\n------------- ---------------- ---------------\n\nTestcase: main(dao.InitDB):        Caused an ERROR\nLine 44 in XML document from class path resource [spring-config-dao.xml] is invalid; nested exception is org.xml.sax.SAXParseException: cvc-complex-type.2.4.c: The matching wildcard is strict, but no declaration can be found for element 'tx:annotation-driven'.\norg.springframework.beans.factory.xml.XmlBeanDefinitionStoreException: Line 44 in XML document from class path resource [spring-config-dao.xml] is invalid; nested exception is org.xml.sax.SAXParseException: cvc-complex-type.2.4.c: The matching wildcard is strict, but no declaration can be found for element 'tx:annotation-driven'.\n\n*********************************************************************************************************************\n\nthe database is running well...\nCan you help me?\n\nolivier.\n\n\n\n---\n\n**Affects:** 2.5.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453341773"], "labels":["status: invalid"]}