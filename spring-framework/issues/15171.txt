{"id":"15171", "title":"DateFormatter and TimeZone:  [SPR-10541]", "body":"**[Enrique Carro](https://jira.spring.io/secure/ViewProfile.jspa?name=srcarro)** opened **[SPR-10541](https://jira.spring.io/browse/SPR-10541?redirect=false)** and commented

This problem arised when you remove **Joda Time Library** from the classpath. Lets suppose that you have an object with two validated fields:

```
  @DateTimeFormat(iso=ISO.DATE)
  private Date creationDate;

  @DateTimeFormat(pattern = \"yyyy-MM-dd\")
  private Date modificationDate;
```

You put the same date in both fields, for example \"04/25/2013\". Then you try to display them with a JSP, in this way:
\\\\

```
<s:eval expression=\"myObj.creationDate\"/><br/>
<s:eval expression=\"myObj.modificationDate\"/>
```

You'll get the next result:
\\\\

```
2013-04-24
2013-04-25
```

I think the problem could be in method **org.springframework.format.datetime.DateFormatter.createDateFormat()**. In the first case (an ISO pattern) a SimpleDateFormat is created and TimeZone is set to **UTC**. My default TimeZone is **UTC+2**, and this produces the _one day minus_ effect.



---

**Affects:** 3.2.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15171","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15171/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15171/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15171/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15171","id":398158869,"node_id":"MDU6SXNzdWUzOTgxNTg4Njk=","number":15171,"title":"DateFormatter and TimeZone:  [SPR-10541]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2013-05-08T04:42:14Z","updated_at":"2019-01-12T03:42:31Z","closed_at":"2019-01-12T03:42:31Z","author_association":"COLLABORATOR","body":"**[Enrique Carro](https://jira.spring.io/secure/ViewProfile.jspa?name=srcarro)** opened **[SPR-10541](https://jira.spring.io/browse/SPR-10541?redirect=false)** and commented\n\nThis problem arised when you remove **Joda Time Library** from the classpath. Lets suppose that you have an object with two validated fields:\n\n```\n  @DateTimeFormat(iso=ISO.DATE)\n  private Date creationDate;\n\n  @DateTimeFormat(pattern = \"yyyy-MM-dd\")\n  private Date modificationDate;\n```\n\nYou put the same date in both fields, for example \"04/25/2013\". Then you try to display them with a JSP, in this way:\n\\\\\n\n```\n<s:eval expression=\"myObj.creationDate\"/><br/>\n<s:eval expression=\"myObj.modificationDate\"/>\n```\n\nYou'll get the next result:\n\\\\\n\n```\n2013-04-24\n2013-04-25\n```\n\nI think the problem could be in method **org.springframework.format.datetime.DateFormatter.createDateFormat()**. In the first case (an ISO pattern) a SimpleDateFormat is created and TimeZone is set to **UTC**. My default TimeZone is **UTC+2**, and this produces the _one day minus_ effect.\n\n\n\n---\n\n**Affects:** 3.2.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453716348"], "labels":["status: bulk-closed"]}