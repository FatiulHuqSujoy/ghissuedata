{"id":"16581", "title":"Support MultiPartFile as part of form backing object in Spring MVC Test [SPR-11965]", "body":"**[Sameer Pawar](https://jira.spring.io/secure/ViewProfile.jspa?name=sameer_pawar)** opened **[SPR-11965](https://jira.spring.io/browse/SPR-11965?redirect=false)** and commented

My controller is like below:

```java
@RequestMapping(method = POST)
protected String processSubmit(HttpServletRequest request,
        @Valid @ModelAttribute(\"binaryUploadForm\") BinaryUploadForm form, BindingResult result, ModelMap model) throws Exception {
```

then I loop through files...

```java
final int numberOfFiles = form.getFiles().size();
for (int i = 0; i < numberOfFiles; i++) {
    final MultipartFile multipartFile = form.getFiles().get(i);
```

In JSP I have a field like below

```html
<input id=\"files0\" type=\"file\" name=\"files[0]\" style=\"font-size:1.2em;height:22px;position:static;\" size=\"48\"/>
```

and form implementation is as below

```java
public List<MultipartFile> getFiles() {
    return files;
}

public void setFiles(List<MultipartFile> files) {
    this.files = files;
}
```

The HTML form is submitted using JavaScript which is quite complex and using EXT JS.

I want to write a test case for this using `MockMvc`.

```java
mockMvc.perform(fileUpload(\"/binary-management/add-binary.html\").file(file)
```

But the above is not helpful as `MultipartFile` is not coming as part of parameter but as a field in form.

Can you please provide this feature?


---

**Affects:** 4.0.5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16581","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16581/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16581/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16581/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16581","id":398169729,"node_id":"MDU6SXNzdWUzOTgxNjk3Mjk=","number":16581,"title":"Support MultiPartFile as part of form backing object in Spring MVC Test [SPR-11965]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2014-07-07T07:33:44Z","updated_at":"2019-01-12T02:29:24Z","closed_at":"2019-01-12T02:29:24Z","author_association":"COLLABORATOR","body":"**[Sameer Pawar](https://jira.spring.io/secure/ViewProfile.jspa?name=sameer_pawar)** opened **[SPR-11965](https://jira.spring.io/browse/SPR-11965?redirect=false)** and commented\n\nMy controller is like below:\n\n```java\n@RequestMapping(method = POST)\nprotected String processSubmit(HttpServletRequest request,\n        @Valid @ModelAttribute(\"binaryUploadForm\") BinaryUploadForm form, BindingResult result, ModelMap model) throws Exception {\n```\n\nthen I loop through files...\n\n```java\nfinal int numberOfFiles = form.getFiles().size();\nfor (int i = 0; i < numberOfFiles; i++) {\n    final MultipartFile multipartFile = form.getFiles().get(i);\n```\n\nIn JSP I have a field like below\n\n```html\n<input id=\"files0\" type=\"file\" name=\"files[0]\" style=\"font-size:1.2em;height:22px;position:static;\" size=\"48\"/>\n```\n\nand form implementation is as below\n\n```java\npublic List<MultipartFile> getFiles() {\n    return files;\n}\n\npublic void setFiles(List<MultipartFile> files) {\n    this.files = files;\n}\n```\n\nThe HTML form is submitted using JavaScript which is quite complex and using EXT JS.\n\nI want to write a test case for this using `MockMvc`.\n\n```java\nmockMvc.perform(fileUpload(\"/binary-management/add-binary.html\").file(file)\n```\n\nBut the above is not helpful as `MultipartFile` is not coming as part of parameter but as a field in form.\n\nCan you please provide this feature?\n\n\n---\n\n**Affects:** 4.0.5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453712133"], "labels":["in: test","status: bulk-closed"]}