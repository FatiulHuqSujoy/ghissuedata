{"id":"19678", "title":"Internal spring boot bug if wildfly libs is used [SPR-15111]", "body":"**[Andrew](https://jira.spring.io/secure/ViewProfile.jspa?name=tbw)** opened **[SPR-15111](https://jira.spring.io/browse/SPR-15111?redirect=false)** and commented

Then spring boot used (without any spring annotations) as dependency for wildfly (10.1) client package project i get this message:

WARN: AMQ212052: Packet PACKET(SessionBindingQueryResponseMessage_V2)[type=-8, channelID=13, packetObject=SessionBindingQueryResponseMessage_V2, exists=true, queueNames=[jms.queue.testQueue], autoCreateJmsQueues=false] was answered out of sequence due to a previous server timeout and it's being ignored
java.lang.Exception: trace

To reproduce this bug:
0. Use attached archive
1. copy wildfly standalone-full.xml to wildfly (10.1) config dir (plus server.keystore in this directory too)

2. copy Server.ejb to wildfly deploy dir

3. start wildfly (standalone.bat -c standalone-full.xml)

4. run: maven clean compile packege exec:exec
   result: no exceptrions, jms is work (spam on console)

5. Then uncomment pom.xml at for lines 15-19 (for enable spring boot dependency), rebuild project and you can see errors at work

   \\<parent>
   \\<groupId>org.springframework.boot\\</groupId>
   \\<artifactId>spring-boot-starter-parent\\</artifactId>
   \\<version>1.4.2.RELEASE\\</version>
   \\</parent>

With 1.4.3 problem is actual too.


---

**Affects:** 4.3.5

**Reference URL:** https://developer.jboss.org/thread/273543?start=15&tstart=0

**Attachments:**
- [test2.7z](https://jira.spring.io/secure/attachment/23680/test2.7z) (_19.10 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19678","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19678/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19678/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19678/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19678","id":398203630,"node_id":"MDU6SXNzdWUzOTgyMDM2MzA=","number":19678,"title":"Internal spring boot bug if wildfly libs is used [SPR-15111]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2017-01-08T08:32:07Z","updated_at":"2019-01-12T05:21:27Z","closed_at":"2017-01-08T11:10:05Z","author_association":"COLLABORATOR","body":"**[Andrew](https://jira.spring.io/secure/ViewProfile.jspa?name=tbw)** opened **[SPR-15111](https://jira.spring.io/browse/SPR-15111?redirect=false)** and commented\n\nThen spring boot used (without any spring annotations) as dependency for wildfly (10.1) client package project i get this message:\n\nWARN: AMQ212052: Packet PACKET(SessionBindingQueryResponseMessage_V2)[type=-8, channelID=13, packetObject=SessionBindingQueryResponseMessage_V2, exists=true, queueNames=[jms.queue.testQueue], autoCreateJmsQueues=false] was answered out of sequence due to a previous server timeout and it's being ignored\njava.lang.Exception: trace\n\nTo reproduce this bug:\n0. Use attached archive\n1. copy wildfly standalone-full.xml to wildfly (10.1) config dir (plus server.keystore in this directory too)\n\n2. copy Server.ejb to wildfly deploy dir\n\n3. start wildfly (standalone.bat -c standalone-full.xml)\n\n4. run: maven clean compile packege exec:exec\n   result: no exceptrions, jms is work (spam on console)\n\n5. Then uncomment pom.xml at for lines 15-19 (for enable spring boot dependency), rebuild project and you can see errors at work\n\n   \\<parent>\n   \\<groupId>org.springframework.boot\\</groupId>\n   \\<artifactId>spring-boot-starter-parent\\</artifactId>\n   \\<version>1.4.2.RELEASE\\</version>\n   \\</parent>\n\nWith 1.4.3 problem is actual too.\n\n\n---\n\n**Affects:** 4.3.5\n\n**Reference URL:** https://developer.jboss.org/thread/273543?start=15&tstart=0\n\n**Attachments:**\n- [test2.7z](https://jira.spring.io/secure/attachment/23680/test2.7z) (_19.10 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453450295"], "labels":["status: invalid"]}