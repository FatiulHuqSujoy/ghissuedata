{"id":"23109", "title":"Strange behavior of WebMvcConfigurer (it stops calling)", "body":"I use the same pattern in a few project to log income requests (just the url and ip address)
That is:

```
public class RequestHandlerInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        
        log.info(\"\"); //logging required information here
        return super.preHandle(request, response, handler);    
    }
}
```

And WebMvcConfigurer implementaition
```
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestHandlerInterceptor());
    }
}
```

And today I noticed that this interceptor doesn't triggers, I mean `addInterceptors` doesn't fire.
The only difference I found is `spring-boot` version.
I took one working project (with 2.1.2 version) and changed it to 2.1.5 and it stopped working. But when I changed it back to 2.1.2 it didn't start working again... Also downgrading 2.1.5 version to 2.1.2 didn't help.
What do I do wrong?
P.S. `@EnableWebMvc` added to AppConfig", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23109","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23109/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23109/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23109/events","html_url":"https://github.com/spring-projects/spring-framework/issues/23109","id":454718668,"node_id":"MDU6SXNzdWU0NTQ3MTg2Njg=","number":23109,"title":"Strange behavior of WebMvcConfigurer (it stops calling)","user":{"login":"DrWifey","id":12988773,"node_id":"MDQ6VXNlcjEyOTg4Nzcz","avatar_url":"https://avatars3.githubusercontent.com/u/12988773?v=4","gravatar_id":"","url":"https://api.github.com/users/DrWifey","html_url":"https://github.com/DrWifey","followers_url":"https://api.github.com/users/DrWifey/followers","following_url":"https://api.github.com/users/DrWifey/following{/other_user}","gists_url":"https://api.github.com/users/DrWifey/gists{/gist_id}","starred_url":"https://api.github.com/users/DrWifey/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/DrWifey/subscriptions","organizations_url":"https://api.github.com/users/DrWifey/orgs","repos_url":"https://api.github.com/users/DrWifey/repos","events_url":"https://api.github.com/users/DrWifey/events{/privacy}","received_events_url":"https://api.github.com/users/DrWifey/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-06-11T14:17:44Z","updated_at":"2019-06-17T09:42:34Z","closed_at":"2019-06-14T12:17:41Z","author_association":"NONE","body":"I use the same pattern in a few project to log income requests (just the url and ip address)\r\nThat is:\r\n\r\n```\r\npublic class RequestHandlerInterceptor extends HandlerInterceptorAdapter {\r\n\r\n    @Override\r\n    public boolean preHandle(HttpServletRequest request,\r\n                             HttpServletResponse response, Object handler) throws Exception {\r\n        \r\n        log.info(\"\"); //logging required information here\r\n        return super.preHandle(request, response, handler);    \r\n    }\r\n}\r\n```\r\n\r\nAnd WebMvcConfigurer implementaition\r\n```\r\n@Configuration\r\npublic class WebConfig implements WebMvcConfigurer {\r\n\r\n    @Override\r\n    public void addInterceptors(InterceptorRegistry registry) {\r\n        registry.addInterceptor(new RequestHandlerInterceptor());\r\n    }\r\n}\r\n```\r\n\r\nAnd today I noticed that this interceptor doesn't triggers, I mean `addInterceptors` doesn't fire.\r\nThe only difference I found is `spring-boot` version.\r\nI took one working project (with 2.1.2 version) and changed it to 2.1.5 and it stopped working. But when I changed it back to 2.1.2 it didn't start working again... Also downgrading 2.1.5 version to 2.1.2 didn't help.\r\nWhat do I do wrong?\r\nP.S. `@EnableWebMvc` added to AppConfig","closed_by":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}}", "commentIds":["501306050","502086947","502612451"], "labels":["in: web","status: invalid"]}