{"id":"18253", "title":"UriComponents needs support for URL building ending with trailing slash [SPR-13678]", "body":"**[Ivan Orešković](https://jira.spring.io/secure/ViewProfile.jspa?name=lopina)** opened **[SPR-13678](https://jira.spring.io/browse/SPR-13678?redirect=false)** and commented

REST conventions suggest that resource paths should end with trailing slash.

When using UriComponentsBuilder to build UriComponents, it has no option to append trailing slash at the end of path segments, before query params and fragments.

```
    @Test
    public void testBuildUriFromNonTrailingSlash() {
        String url = \"http://127.0.0.1:30001/basePath\";
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);

        List<String> prefixPaths = new ArrayList<>();
        prefixPaths.add(\"foo\");
        prefixPaths.add(\"bar\");

        String id = \"baz\";

        List<String> suffixPaths = new ArrayList<>();
        suffixPaths.add(\"123\");
        suffixPaths.add(\"456\");

        Map<String, String> requestParams = new HashMap<>();
        requestParams.put(\"param1\", \"value1\");
        requestParams.put(\"param2\", \"value2\");

        prefixPaths.stream()
                .filter(path -> path != null && !path.toString().trim().isEmpty())
                .forEach(path -> uriBuilder.pathSegment(path));

        if (id != null && !id.toString().trim().isEmpty()) {
            uriBuilder.pathSegment(id.toString().trim());
        }

        suffixPaths.stream()
                .filter(path -> path != null && !path.toString().trim().isEmpty())
                .forEach(path -> uriBuilder.pathSegment(path));


        requestParams.entrySet().stream()
                .filter(e -> stringExists(e.getKey()) && stringExists(e.getValue()))
                .forEach(e -> uriBuilder.queryParam(e.getKey(), wrap(e.getKey())));

        UriComponents uri = uriBuilder.build();
        System.out.println(uri.toUriString());

        UriComponentsPathTrailingSlashDecorator uriWrapper = new UriComponentsPathTrailingSlashDecorator(uri, true);
        System.out.println(uriWrapper.toUriString());
    }
```

gives output

```
http://127.0.0.1:30001/basePath/foo/bar/baz/123/456?param1={param1}&param2={param2}
http://127.0.0.1:30001/basePath/foo/bar/baz/123/456/?param1={param1}&param2={param2}
```

The first line is how the current UriComponents#toUriString method does it, and the second is how it should do it provided the option to end the path with a trailing slash.


---

**Affects:** 4.2.2

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/5f2d34f526f3f2b3c41dbc39a69374e526b6b5aa
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18253","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18253/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18253/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18253/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18253","id":398186559,"node_id":"MDU6SXNzdWUzOTgxODY1NTk=","number":18253,"title":"UriComponents needs support for URL building ending with trailing slash [SPR-13678]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2015-11-12T13:39:05Z","updated_at":"2015-11-17T20:22:06Z","closed_at":"2015-11-17T20:22:06Z","author_association":"COLLABORATOR","body":"**[Ivan Orešković](https://jira.spring.io/secure/ViewProfile.jspa?name=lopina)** opened **[SPR-13678](https://jira.spring.io/browse/SPR-13678?redirect=false)** and commented\n\nREST conventions suggest that resource paths should end with trailing slash.\n\nWhen using UriComponentsBuilder to build UriComponents, it has no option to append trailing slash at the end of path segments, before query params and fragments.\n\n```\n    @Test\n    public void testBuildUriFromNonTrailingSlash() {\n        String url = \"http://127.0.0.1:30001/basePath\";\n        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);\n\n        List<String> prefixPaths = new ArrayList<>();\n        prefixPaths.add(\"foo\");\n        prefixPaths.add(\"bar\");\n\n        String id = \"baz\";\n\n        List<String> suffixPaths = new ArrayList<>();\n        suffixPaths.add(\"123\");\n        suffixPaths.add(\"456\");\n\n        Map<String, String> requestParams = new HashMap<>();\n        requestParams.put(\"param1\", \"value1\");\n        requestParams.put(\"param2\", \"value2\");\n\n        prefixPaths.stream()\n                .filter(path -> path != null && !path.toString().trim().isEmpty())\n                .forEach(path -> uriBuilder.pathSegment(path));\n\n        if (id != null && !id.toString().trim().isEmpty()) {\n            uriBuilder.pathSegment(id.toString().trim());\n        }\n\n        suffixPaths.stream()\n                .filter(path -> path != null && !path.toString().trim().isEmpty())\n                .forEach(path -> uriBuilder.pathSegment(path));\n\n\n        requestParams.entrySet().stream()\n                .filter(e -> stringExists(e.getKey()) && stringExists(e.getValue()))\n                .forEach(e -> uriBuilder.queryParam(e.getKey(), wrap(e.getKey())));\n\n        UriComponents uri = uriBuilder.build();\n        System.out.println(uri.toUriString());\n\n        UriComponentsPathTrailingSlashDecorator uriWrapper = new UriComponentsPathTrailingSlashDecorator(uri, true);\n        System.out.println(uriWrapper.toUriString());\n    }\n```\n\ngives output\n\n```\nhttp://127.0.0.1:30001/basePath/foo/bar/baz/123/456?param1={param1}&param2={param2}\nhttp://127.0.0.1:30001/basePath/foo/bar/baz/123/456/?param1={param1}&param2={param2}\n```\n\nThe first line is how the current UriComponents#toUriString method does it, and the second is how it should do it provided the option to end the path with a trailing slash.\n\n\n---\n\n**Affects:** 4.2.2\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/5f2d34f526f3f2b3c41dbc39a69374e526b6b5aa\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453432715","453432717","453432720","453432723","453432725"], "labels":["in: web","status: declined","type: enhancement"]}