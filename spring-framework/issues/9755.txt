{"id":"9755", "title":"@Transactional annotation doesn't work for scanned component, again [SPR-5082]", "body":"**[Yuwei Zhao](https://jira.spring.io/secure/ViewProfile.jspa?name=wei)** opened **[SPR-5082](https://jira.spring.io/browse/SPR-5082?redirect=false)** and commented

Please see the following example:

```java
@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountDao accountDao;

    public void updateLoginInfo(Account account) {
        accountDao.updateLoginInfo(account);
        someOtherStatementsMightThrowException();
    }
}
```

And everything supports the annotation is configurated:

```xml
<context:annotation-config />
<context:component-scan base-package=\"com.xyz\" />
<tx:annotation-driven transaction-manager=\"transactionManager\" />
```

We use `@Service` annotation on service bean, that is, it could be autowired to any bean that depends on it.

But the `@Transactional` annotation doesn't work, until I remove `@Service` annotation and explicitly define the bean in context configuration xml:

```xml
<bean id=\"accountService\" class=\"com.xyz.service.AccountService\" />
```

I'm wondering if there is any step I missed, or it supposes to work like that? Thanks.

---

**Affects:** 2.5 final

**Attachments:**
- [sptest.zip](https://jira.spring.io/secure/attachment/14541/sptest.zip) (_4.04 MB_)

**Issue Links:**
- #10953 Autowired can not work with `@Transactional`

10 votes, 16 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9755","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9755/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9755/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9755/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9755","id":398090361,"node_id":"MDU6SXNzdWUzOTgwOTAzNjE=","number":9755,"title":"@Transactional annotation doesn't work for scanned component, again [SPR-5082]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512024,"node_id":"MDU6TGFiZWwxMTg4NTEyMDI0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/has:%20votes-jira","name":"has: votes-jira","color":"dfdfdf","default":false},{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":12,"created_at":"2008-08-13T03:15:11Z","updated_at":"2019-01-13T08:02:52Z","closed_at":"2014-11-04T03:29:21Z","author_association":"COLLABORATOR","body":"**[Yuwei Zhao](https://jira.spring.io/secure/ViewProfile.jspa?name=wei)** opened **[SPR-5082](https://jira.spring.io/browse/SPR-5082?redirect=false)** and commented\n\nPlease see the following example:\n\n```java\n@Service\n@Transactional\npublic class AccountService {\n\n    @Autowired\n    private AccountDao accountDao;\n\n    public void updateLoginInfo(Account account) {\n        accountDao.updateLoginInfo(account);\n        someOtherStatementsMightThrowException();\n    }\n}\n```\n\nAnd everything supports the annotation is configurated:\n\n```xml\n<context:annotation-config />\n<context:component-scan base-package=\"com.xyz\" />\n<tx:annotation-driven transaction-manager=\"transactionManager\" />\n```\n\nWe use `@Service` annotation on service bean, that is, it could be autowired to any bean that depends on it.\n\nBut the `@Transactional` annotation doesn't work, until I remove `@Service` annotation and explicitly define the bean in context configuration xml:\n\n```xml\n<bean id=\"accountService\" class=\"com.xyz.service.AccountService\" />\n```\n\nI'm wondering if there is any step I missed, or it supposes to work like that? Thanks.\n\n---\n\n**Affects:** 2.5 final\n\n**Attachments:**\n- [sptest.zip](https://jira.spring.io/secure/attachment/14541/sptest.zip) (_4.04 MB_)\n\n**Issue Links:**\n- #10953 Autowired can not work with `@Transactional`\n\n10 votes, 16 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453333092","453333093","453333095","453333096","453333098","453333099","453333100","453333101","453333103","453333104","453333106","453333108"], "labels":["has: votes-jira","in: data","status: declined"]}