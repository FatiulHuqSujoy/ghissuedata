{"id":"12620", "title":"Jaxb2Marshaller: add parameter for formatted output to oxm namespace [SPR-7965]", "body":"**[Stephan Vollmer](https://jira.spring.io/secure/ViewProfile.jspa?name=svollmer)** opened **[SPR-7965](https://jira.spring.io/browse/SPR-7965?redirect=false)** and commented

Currently, it is not possible to enable formatted output when the `oxm:jaxb2-marshaller` element is used. To set the property `javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT`, the marshaller needs to be declared as a normal bean. Setting the property requires quite a lot of XML code.

Compare these two examples:

```xml
<bean id=\"marshaller\" class=\"org.springframework.oxm.jaxb.Jaxb2Marshaller\">
    <property name=\"classesToBeBound\">
        <list>
            <value>org.example.XmlClass</value>
        </list>
    </property>
    <property name=\"marshallerProperties\">
        <map>
            <entry>
                <key>
                    <util:constant static-field=\"javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT\" />
                </key>
                <value type=\"boolean\">true</value>
            </entry>
        </map>
    </property>
</bean>


<oxm:jaxb2-marshaller id=\"marshaller\" format-output=\"true\">
    <oxm:class-to-be-bound name=\"org.example.XmlClass\" />
</oxm:jaxb2-marshaller>
```

I believe that it is quite a common requirement to enable formatted XML output and many users would benefit from it. Therefore, I suggest this improvement.

Cheers,
Stephan


---

**Affects:** 3.0.5

4 votes, 0 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12620","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12620/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12620/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12620/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12620","id":398110175,"node_id":"MDU6SXNzdWUzOTgxMTAxNzU=","number":12620,"title":"Jaxb2Marshaller: add parameter for formatted output to oxm namespace [SPR-7965]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2011-02-16T02:20:49Z","updated_at":"2018-12-28T11:44:02Z","closed_at":"2018-12-28T11:44:02Z","author_association":"COLLABORATOR","body":"**[Stephan Vollmer](https://jira.spring.io/secure/ViewProfile.jspa?name=svollmer)** opened **[SPR-7965](https://jira.spring.io/browse/SPR-7965?redirect=false)** and commented\n\nCurrently, it is not possible to enable formatted output when the `oxm:jaxb2-marshaller` element is used. To set the property `javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT`, the marshaller needs to be declared as a normal bean. Setting the property requires quite a lot of XML code.\n\nCompare these two examples:\n\n```xml\n<bean id=\"marshaller\" class=\"org.springframework.oxm.jaxb.Jaxb2Marshaller\">\n    <property name=\"classesToBeBound\">\n        <list>\n            <value>org.example.XmlClass</value>\n        </list>\n    </property>\n    <property name=\"marshallerProperties\">\n        <map>\n            <entry>\n                <key>\n                    <util:constant static-field=\"javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT\" />\n                </key>\n                <value type=\"boolean\">true</value>\n            </entry>\n        </map>\n    </property>\n</bean>\n\n\n<oxm:jaxb2-marshaller id=\"marshaller\" format-output=\"true\">\n    <oxm:class-to-be-bound name=\"org.example.XmlClass\" />\n</oxm:jaxb2-marshaller>\n```\n\nI believe that it is quite a common requirement to enable formatted XML output and many users would benefit from it. Therefore, I suggest this improvement.\n\nCheers,\nStephan\n\n\n---\n\n**Affects:** 3.0.5\n\n4 votes, 0 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453357051"], "labels":["in: data","status: declined","type: enhancement"]}