{"id":"13202", "title":"SessionLocaleResolver for portlets, it is not compatible with spring-portlet-mvc [SPR-8558]", "body":"**[Josef Vychtrle](https://jira.spring.io/secure/ViewProfile.jspa?name=lisak)** opened **[SPR-8558](https://jira.spring.io/browse/SPR-8558?redirect=false)** and commented

Relates to #13199

The point is, that SessionLocaleResolver operates with ServletRequest and HttpSession but in portlet environment, you don't have access to ServletRequest which is wrapped in PortletRequest, the method call is delegated to it. The same applies to HttpSession and PortletSession.

Also portlets have 3 scopes (portlet, portal, application) and Locale is part of the PORTAL scope whereas the default scope is PORTLET.

As a result, one has to implement CustomSessionLocaleResolver, that is incompatible with SessionLocaleResolver interface.

```
public class LocalizedMessageSource {
	
	@Autowired
	private MessageSource resource;
	@Autowired
	private PortletSessionLocaleResolver localeResolver;


	public String getMessage(MessageSourceResolvable resolvable) {
		return resource.getMessage(resolvable, getLocale());
	}

	public String getMessage(String code, Object[] args) {
		return resource.getMessage(code, args, getLocale());
	}

	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		return resource.getMessage(code, args, defaultMessage, getLocale());
	}

	private Locale getLocale() {
		return localeResolver.resolveLocale(getCurrentRequest());
	}

	private PortletRequest getCurrentRequest() {
		return ((PortletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}
}
```

```
public class PortletSessionLocaleResolver  {


	public static final String LOCALE_SESSION_ATTRIBUTE_NAME = \"LOCALE\";
	public static final int PORTAL_SCOPE = 0x03;
	
	private Locale defaultLocale;

	public Locale resolveLocale(PortletRequest request) {
		PortletSession session = request.getPortletSession(false);
		Locale locale = (Locale) session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME, PORTAL_SCOPE);
		if (locale == null) {
			locale = determineDefaultLocale(request);
		}
		return locale;
	}

	protected Locale determineDefaultLocale(PortletRequest request) {
		Locale defaultLocale = getDefaultLocale();
		if (defaultLocale == null) {
			defaultLocale = request.getLocale();
		}
		return defaultLocale;
	}

	public Object getSessionAttribute(PortletRequest request, String name) {
		PortletSession session = request.getPortletSession(false);
		return (session != null ? session.getAttribute(name, PORTAL_SCOPE) : null);
	}
	
	public void setSessionAttribute(PortletRequest request, String name, Object value) {
		if (value != null) {
			request.getPortletSession().setAttribute(name, value, PORTAL_SCOPE);
		}
		else {
			PortletSession session = request.getPortletSession(false);
			if (session != null) {
				session.removeAttribute(name);
			}
		}
	}
	
	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	protected Locale getDefaultLocale() {
		return this.defaultLocale;
	}
	
}
```

---

**Affects:** 3.1 M2

**Attachments:**
- [kt9.png](https://jira.spring.io/secure/attachment/18494/kt9.png) (_121.82 kB_)

1 votes, 0 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13202","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13202/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13202/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13202/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13202","id":398113707,"node_id":"MDU6SXNzdWUzOTgxMTM3MDc=","number":13202,"title":"SessionLocaleResolver for portlets, it is not compatible with spring-portlet-mvc [SPR-8558]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2011-07-23T11:34:10Z","updated_at":"2019-01-11T13:22:27Z","closed_at":"2018-12-12T16:39:01Z","author_association":"COLLABORATOR","body":"**[Josef Vychtrle](https://jira.spring.io/secure/ViewProfile.jspa?name=lisak)** opened **[SPR-8558](https://jira.spring.io/browse/SPR-8558?redirect=false)** and commented\n\nRelates to #13199\n\nThe point is, that SessionLocaleResolver operates with ServletRequest and HttpSession but in portlet environment, you don't have access to ServletRequest which is wrapped in PortletRequest, the method call is delegated to it. The same applies to HttpSession and PortletSession.\n\nAlso portlets have 3 scopes (portlet, portal, application) and Locale is part of the PORTAL scope whereas the default scope is PORTLET.\n\nAs a result, one has to implement CustomSessionLocaleResolver, that is incompatible with SessionLocaleResolver interface.\n\n```\npublic class LocalizedMessageSource {\n\t\n\t@Autowired\n\tprivate MessageSource resource;\n\t@Autowired\n\tprivate PortletSessionLocaleResolver localeResolver;\n\n\n\tpublic String getMessage(MessageSourceResolvable resolvable) {\n\t\treturn resource.getMessage(resolvable, getLocale());\n\t}\n\n\tpublic String getMessage(String code, Object[] args) {\n\t\treturn resource.getMessage(code, args, getLocale());\n\t}\n\n\tpublic String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {\n\t\treturn resource.getMessage(code, args, defaultMessage, getLocale());\n\t}\n\n\tprivate Locale getLocale() {\n\t\treturn localeResolver.resolveLocale(getCurrentRequest());\n\t}\n\n\tprivate PortletRequest getCurrentRequest() {\n\t\treturn ((PortletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();\n\t}\n}\n```\n\n```\npublic class PortletSessionLocaleResolver  {\n\n\n\tpublic static final String LOCALE_SESSION_ATTRIBUTE_NAME = \"LOCALE\";\n\tpublic static final int PORTAL_SCOPE = 0x03;\n\t\n\tprivate Locale defaultLocale;\n\n\tpublic Locale resolveLocale(PortletRequest request) {\n\t\tPortletSession session = request.getPortletSession(false);\n\t\tLocale locale = (Locale) session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME, PORTAL_SCOPE);\n\t\tif (locale == null) {\n\t\t\tlocale = determineDefaultLocale(request);\n\t\t}\n\t\treturn locale;\n\t}\n\n\tprotected Locale determineDefaultLocale(PortletRequest request) {\n\t\tLocale defaultLocale = getDefaultLocale();\n\t\tif (defaultLocale == null) {\n\t\t\tdefaultLocale = request.getLocale();\n\t\t}\n\t\treturn defaultLocale;\n\t}\n\n\tpublic Object getSessionAttribute(PortletRequest request, String name) {\n\t\tPortletSession session = request.getPortletSession(false);\n\t\treturn (session != null ? session.getAttribute(name, PORTAL_SCOPE) : null);\n\t}\n\t\n\tpublic void setSessionAttribute(PortletRequest request, String name, Object value) {\n\t\tif (value != null) {\n\t\t\trequest.getPortletSession().setAttribute(name, value, PORTAL_SCOPE);\n\t\t}\n\t\telse {\n\t\t\tPortletSession session = request.getPortletSession(false);\n\t\t\tif (session != null) {\n\t\t\t\tsession.removeAttribute(name);\n\t\t\t}\n\t\t}\n\t}\n\t\n\tpublic void setDefaultLocale(Locale defaultLocale) {\n\t\tthis.defaultLocale = defaultLocale;\n\t}\n\n\tprotected Locale getDefaultLocale() {\n\t\treturn this.defaultLocale;\n\t}\n\t\n}\n```\n\n---\n\n**Affects:** 3.1 M2\n\n**Attachments:**\n- [kt9.png](https://jira.spring.io/secure/attachment/18494/kt9.png) (_121.82 kB_)\n\n1 votes, 0 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453361191"], "labels":["in: web","status: declined","type: enhancement"]}