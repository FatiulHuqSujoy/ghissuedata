{"id":"18890", "title":"Spring MVC: Reusing Resources (sub-resources) [SPR-14318]", "body":"**[Nahshon Unna Tsameret](https://jira.spring.io/secure/ViewProfile.jspa?name=nahsh)** opened **[SPR-14318](https://jira.spring.io/browse/SPR-14318?redirect=false)** and commented

Hi.

I'm Trying to move my server from JAX-RS to Spring MVC. The implementation uses JAX-RS sub-resources, as well as inheriting resources and abstract resource classes.

For example:

The server's data model include entities of type A, that contain several entities of type B, several of type C etc. So each unique B, C, D or E entity, belongs to one specific entity of type A. The application offers REST CRUD operations for these entities, where the access is by A unique ID, or by the sub-entity unique ID. Also, there are many more APIs, such as - reading all B's of a specific A, read all C's with a specific property value, setting a property within n entity and so on.

These two URLs are equivalent:
/As/{a_id}/Bs/{b_id}  
/Bs/{b_id}

In JAX-RS, the implementation is a resource class for each type, where this class is used both as a component, and as a reference from A:

```java
@Component
@Path(\"/As\")
class resourceA {
    @Resource
    ResourceB resourceB;

    @Resource
    ResourceC resourceC;

    @Resource
    ResourceD resourceD;

    @Resource
    ResourceE resourceE;

    @Path(\"/a_id}\")
    @PUT
    public Response createA(...) { ... }

    ...

    @Path(\"/{a_id}/Bs\")
    public getResourceB() {
        return resourceB;
    }
    ...
}

@Component
@Path(\"Bs\")
class ResourceB {
    ...
}
```

As I understand that the concept of sub-resources is not related to spring MVC, I'm trying to understand how to do the migration, without changing the API.

I tried to use multiple paths, but there is a bug:

```java
@RequestMapping(value={\"/As/{a_id}/Bs/{b_id}\", \"/Bs/{ b_id}\"}, method=GET)
public getSpecificBEntity(@PathVariable(\"a_id\") String aId, @PathVariable(\"b_id\") String bId) {...}
```

Now, /As/{a_id}/Bs/{b_id} URL is working, but /Bs/{b_id} returns HTTP status of 400, because there is no way to make a PathVariable optional (required=false).

So I tried delegation:

```java
@RequestMapping(value=\"/As/{a_id}/Bs/{b_id}\", method=GET)
public DifferedResult<String> getSpecificBEntity(@PathVariable(\"a_id\") String aId, @PathVariable(\"b_id\") String bId) {
    ...
}

@RequestMapping(value=\"/Bs/{b_id}\", method=GET)
public DifferedResult<String> getSpecificBEntity(@PathVariable(\"b_id\") String bId) {
    return getSpecificBEntity(null, bId);
}
```

This is working, but the code became huge and unreadable, because each resource includes many handlers, and each one of them should handle multiple URLs.

What is the best approach to implement this set of URLs in Spring MVC?

Thanks!

---

**Affects:** 4.1.7

**Reference URL:** https://stackoverflow.com/questions/37528055/sub-resources-in-spring-mvc

2 votes, 3 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18890","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18890/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18890/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18890/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18890","id":398193751,"node_id":"MDU6SXNzdWUzOTgxOTM3NTE=","number":18890,"title":"Spring MVC: Reusing Resources (sub-resources) [SPR-14318]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2016-05-31T05:09:31Z","updated_at":"2019-01-13T20:33:13Z","closed_at":"2016-05-31T14:10:42Z","author_association":"COLLABORATOR","body":"**[Nahshon Unna Tsameret](https://jira.spring.io/secure/ViewProfile.jspa?name=nahsh)** opened **[SPR-14318](https://jira.spring.io/browse/SPR-14318?redirect=false)** and commented\n\nHi.\n\nI'm Trying to move my server from JAX-RS to Spring MVC. The implementation uses JAX-RS sub-resources, as well as inheriting resources and abstract resource classes.\n\nFor example:\n\nThe server's data model include entities of type A, that contain several entities of type B, several of type C etc. So each unique B, C, D or E entity, belongs to one specific entity of type A. The application offers REST CRUD operations for these entities, where the access is by A unique ID, or by the sub-entity unique ID. Also, there are many more APIs, such as - reading all B's of a specific A, read all C's with a specific property value, setting a property within n entity and so on.\n\nThese two URLs are equivalent:\n/As/{a_id}/Bs/{b_id}  \n/Bs/{b_id}\n\nIn JAX-RS, the implementation is a resource class for each type, where this class is used both as a component, and as a reference from A:\n\n```java\n@Component\n@Path(\"/As\")\nclass resourceA {\n    @Resource\n    ResourceB resourceB;\n\n    @Resource\n    ResourceC resourceC;\n\n    @Resource\n    ResourceD resourceD;\n\n    @Resource\n    ResourceE resourceE;\n\n    @Path(\"/a_id}\")\n    @PUT\n    public Response createA(...) { ... }\n\n    ...\n\n    @Path(\"/{a_id}/Bs\")\n    public getResourceB() {\n        return resourceB;\n    }\n    ...\n}\n\n@Component\n@Path(\"Bs\")\nclass ResourceB {\n    ...\n}\n```\n\nAs I understand that the concept of sub-resources is not related to spring MVC, I'm trying to understand how to do the migration, without changing the API.\n\nI tried to use multiple paths, but there is a bug:\n\n```java\n@RequestMapping(value={\"/As/{a_id}/Bs/{b_id}\", \"/Bs/{ b_id}\"}, method=GET)\npublic getSpecificBEntity(@PathVariable(\"a_id\") String aId, @PathVariable(\"b_id\") String bId) {...}\n```\n\nNow, /As/{a_id}/Bs/{b_id} URL is working, but /Bs/{b_id} returns HTTP status of 400, because there is no way to make a PathVariable optional (required=false).\n\nSo I tried delegation:\n\n```java\n@RequestMapping(value=\"/As/{a_id}/Bs/{b_id}\", method=GET)\npublic DifferedResult<String> getSpecificBEntity(@PathVariable(\"a_id\") String aId, @PathVariable(\"b_id\") String bId) {\n    ...\n}\n\n@RequestMapping(value=\"/Bs/{b_id}\", method=GET)\npublic DifferedResult<String> getSpecificBEntity(@PathVariable(\"b_id\") String bId) {\n    return getSpecificBEntity(null, bId);\n}\n```\n\nThis is working, but the code became huge and unreadable, because each resource includes many handlers, and each one of them should handle multiple URLs.\n\nWhat is the best approach to implement this set of URLs in Spring MVC?\n\nThanks!\n\n---\n\n**Affects:** 4.1.7\n\n**Reference URL:** https://stackoverflow.com/questions/37528055/sub-resources-in-spring-mvc\n\n2 votes, 3 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453440457"], "labels":["in: web","status: declined","type: enhancement"]}