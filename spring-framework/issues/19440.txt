{"id":"19440", "title":"The 500 error occur when receive a indexed parameter equal or greater than autoGrowCollectionLimit [SPR-14874]", "body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-14874](https://jira.spring.io/browse/SPR-14874?redirect=false)** and commented

By default settings, the `autoGrowCollectionLimit` is 256. I tried to request as follow:

```
public class SearchQuery {
    private List<String> ids;
    // ...
}
```

```
@RestController
@RequestMapping(\"/accounts\")
public class AccountRestController {
    @GetMapping
    public List<Account> search(@Validated SearchQuery query) {
        // ...
    }
}
```

I submit a request for `http://localhost:8080/accounts?ids[256]=foo`.

```
Whitelabel Error Page

This application has no explicit mapping for /error, so you are seeing this as a fallback.

Thu Nov 03 12:53:53 JST 2016
There was an unexpected error (type=Internal Server Error, status=500).
Invalid property 'ids[256]' of bean class [com.example.AccountQuery]: Invalid list index in property path 'ids[256]'; nested exception is java.lang.IndexOutOfBoundsException: Index: 256, Size: 0
```

I will expect to be the 400(Bad Request) error in this case.
What do you think ?


---

**Affects:** 4.1.9, 4.2.8, 4.3.3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19440","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19440/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19440/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19440/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19440","id":398200041,"node_id":"MDU6SXNzdWUzOTgyMDAwNDE=","number":19440,"title":"The 500 error occur when receive a indexed parameter equal or greater than autoGrowCollectionLimit [SPR-14874]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-11-03T03:59:42Z","updated_at":"2019-01-12T00:12:03Z","closed_at":"2019-01-12T00:12:03Z","author_association":"COLLABORATOR","body":"**[Kazuki Shimizu](https://jira.spring.io/secure/ViewProfile.jspa?name=kazuki43zoo)** opened **[SPR-14874](https://jira.spring.io/browse/SPR-14874?redirect=false)** and commented\n\nBy default settings, the `autoGrowCollectionLimit` is 256. I tried to request as follow:\n\n```\npublic class SearchQuery {\n    private List<String> ids;\n    // ...\n}\n```\n\n```\n@RestController\n@RequestMapping(\"/accounts\")\npublic class AccountRestController {\n    @GetMapping\n    public List<Account> search(@Validated SearchQuery query) {\n        // ...\n    }\n}\n```\n\nI submit a request for `http://localhost:8080/accounts?ids[256]=foo`.\n\n```\nWhitelabel Error Page\n\nThis application has no explicit mapping for /error, so you are seeing this as a fallback.\n\nThu Nov 03 12:53:53 JST 2016\nThere was an unexpected error (type=Internal Server Error, status=500).\nInvalid property 'ids[256]' of bean class [com.example.AccountQuery]: Invalid list index in property path 'ids[256]'; nested exception is java.lang.IndexOutOfBoundsException: Index: 256, Size: 0\n```\n\nI will expect to be the 400(Bad Request) error in this case.\nWhat do you think ?\n\n\n---\n\n**Affects:** 4.1.9, 4.2.8, 4.3.3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453695025"], "labels":["in: web","status: bulk-closed"]}