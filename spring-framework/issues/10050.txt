{"id":"10050", "title":"@SessionParam or @SessionAttribute Annotation for passing a Session Attribute to a Controller [SPR-5377]", "body":"**[Scott Murphy](https://jira.spring.io/secure/ViewProfile.jspa?name=scottland)** opened **[SPR-5377](https://jira.spring.io/browse/SPR-5377?redirect=false)** and commented

Add an additional Annotation for passing objects in the session to a function.  Currently it is a little cumbersome to do this because you have to pass the HttpServletRequest object to your function even if that is all you use it for.

For example:

```java
@RequestMapping(\"/myController.do\")
public String doGet(HttpServletRequest request) {
    MyObject object = (MyObject) WebUtils.getSessionAttribute(request, \"myObject\");
    return \"MyJSP\";
}
```

Should be replaced with:

```java
@RequestMapping(\"/myController.do\")
public String doGet(@SessionParam(\"myObject\") MyObject myObject) {
    return \"MyJSP\";
}
```

If the object is not in the session, null is passed.

Furthermore, an additional boolean parameter can be supplied to initialize the object and put it in the Session if it is not already there:
e.g.:

```java
@RequestMapping(\"/myController.do\")
public String doGet(@SessionParam(value=\"myObject\", initialize=true) MyObject myObject) {
    return \"MyJSP\";
}
```

If myObject is not in the session, a MyObject object will be created and placed in the Session.

---

**Affects:** 3.0 M1

**Issue Links:**
- #16171 Provide `@ModelAttribute`(required=\"false\") for session attributes
- #18468 Convenient access to session and request attributes in controller methods (_**\"is superseded by\"**_)

4 votes, 4 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10050","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10050/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10050/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10050/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10050","id":398092500,"node_id":"MDU6SXNzdWUzOTgwOTI1MDA=","number":10050,"title":"@SessionParam or @SessionAttribute Annotation for passing a Session Attribute to a Controller [SPR-5377]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":6,"created_at":"2008-12-26T05:35:07Z","updated_at":"2019-01-13T08:01:43Z","closed_at":"2016-01-26T17:10:20Z","author_association":"COLLABORATOR","body":"**[Scott Murphy](https://jira.spring.io/secure/ViewProfile.jspa?name=scottland)** opened **[SPR-5377](https://jira.spring.io/browse/SPR-5377?redirect=false)** and commented\n\nAdd an additional Annotation for passing objects in the session to a function.  Currently it is a little cumbersome to do this because you have to pass the HttpServletRequest object to your function even if that is all you use it for.\n\nFor example:\n\n```java\n@RequestMapping(\"/myController.do\")\npublic String doGet(HttpServletRequest request) {\n    MyObject object = (MyObject) WebUtils.getSessionAttribute(request, \"myObject\");\n    return \"MyJSP\";\n}\n```\n\nShould be replaced with:\n\n```java\n@RequestMapping(\"/myController.do\")\npublic String doGet(@SessionParam(\"myObject\") MyObject myObject) {\n    return \"MyJSP\";\n}\n```\n\nIf the object is not in the session, null is passed.\n\nFurthermore, an additional boolean parameter can be supplied to initialize the object and put it in the Session if it is not already there:\ne.g.:\n\n```java\n@RequestMapping(\"/myController.do\")\npublic String doGet(@SessionParam(value=\"myObject\", initialize=true) MyObject myObject) {\n    return \"MyJSP\";\n}\n```\n\nIf myObject is not in the session, a MyObject object will be created and placed in the Session.\n\n---\n\n**Affects:** 3.0 M1\n\n**Issue Links:**\n- #16171 Provide `@ModelAttribute`(required=\"false\") for session attributes\n- #18468 Convenient access to session and request attributes in controller methods (_**\"is superseded by\"**_)\n\n4 votes, 4 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453335686","453335687","453335689","453335690","453335691","453335693"], "labels":["in: web","status: duplicate","type: enhancement"]}