{"id":"15005", "title":"mvc:message-converters cause different behavior of data mapping [SPR-10372]", "body":"**[deng hui](https://jira.spring.io/secure/ViewProfile.jspa?name=truetime)** opened **[SPR-10372](https://jira.spring.io/browse/SPR-10372?redirect=false)** and commented

when configuration is `<mvc:annotation-driven />` these 2 scenarios work well:

scenario 1:

```
$.ajax({ url :\"a-valid-address\",dataType : 'json').done(function(data){
//data got here is a object after eval by jquery
});
```

scenario 2:

```
$.ajax({type: \"post\",url: \"a-valid-address\",contentType: \"application/json\", data: JSON.stringify(query))
```

```java
@RequestMapping(value = \"/query\", method = RequestMethod.POST)
@ResponseBody
public List<DBObject> query(@RequestBody String body) {}
```

but if configuration change to:

```xml
<mvc:annotation-driven>
     <mvc:message-converters register-defaults=\"true\">
          <bean class=\"org.springframework.http.converter.json.MappingJackson2HttpMessageConverter\">
               <property name=\"objectMapper\">
                        <bean class=\"com.azelea.Converter.jackson.CustomObjectMapper\"></bean>
               </property>
          </bean>
     </mvc:message-converters>
</mvc:annotation-driven>
```

For scenario 1, data retrieved will no be an object but a json string before `eval()` and for scenario 2, I get a message:

```
\"The request sent by the client was syntactically incorrect ().\" by exception of com.fasterxml.jackson.databind.JsonMappingException
```

Some other observations after custom `MappingJackson2HttpMessageConverter` configured:

1) originally, controller is not sensitive with `$.ajax()` call specified `dataType(contentType)` or not, after configuration, `dataType` or `contentType` is required. which means message-converter for String type is impacted, the order of going through converters seems changed.

2) originally, converter will be applied after check controller parameter need it, but after configuration, converter is applied according to `contentType` always despite controller parameter require or not. for example scenario 2 above actually read input by `@RequestBody`, jason conversion is not expected.



---

**Affects:** 3.2.1

0 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15005","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15005/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15005/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15005/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15005","id":398157852,"node_id":"MDU6SXNzdWUzOTgxNTc4NTI=","number":15005,"title":"mvc:message-converters cause different behavior of data mapping [SPR-10372]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":7,"created_at":"2013-03-13T06:40:02Z","updated_at":"2015-09-30T09:31:22Z","closed_at":"2015-09-30T09:31:22Z","author_association":"COLLABORATOR","body":"**[deng hui](https://jira.spring.io/secure/ViewProfile.jspa?name=truetime)** opened **[SPR-10372](https://jira.spring.io/browse/SPR-10372?redirect=false)** and commented\n\nwhen configuration is `<mvc:annotation-driven />` these 2 scenarios work well:\n\nscenario 1:\n\n```\n$.ajax({ url :\"a-valid-address\",dataType : 'json').done(function(data){\n//data got here is a object after eval by jquery\n});\n```\n\nscenario 2:\n\n```\n$.ajax({type: \"post\",url: \"a-valid-address\",contentType: \"application/json\", data: JSON.stringify(query))\n```\n\n```java\n@RequestMapping(value = \"/query\", method = RequestMethod.POST)\n@ResponseBody\npublic List<DBObject> query(@RequestBody String body) {}\n```\n\nbut if configuration change to:\n\n```xml\n<mvc:annotation-driven>\n     <mvc:message-converters register-defaults=\"true\">\n          <bean class=\"org.springframework.http.converter.json.MappingJackson2HttpMessageConverter\">\n               <property name=\"objectMapper\">\n                        <bean class=\"com.azelea.Converter.jackson.CustomObjectMapper\"></bean>\n               </property>\n          </bean>\n     </mvc:message-converters>\n</mvc:annotation-driven>\n```\n\nFor scenario 1, data retrieved will no be an object but a json string before `eval()` and for scenario 2, I get a message:\n\n```\n\"The request sent by the client was syntactically incorrect ().\" by exception of com.fasterxml.jackson.databind.JsonMappingException\n```\n\nSome other observations after custom `MappingJackson2HttpMessageConverter` configured:\n\n1) originally, controller is not sensitive with `$.ajax()` call specified `dataType(contentType)` or not, after configuration, `dataType` or `contentType` is required. which means message-converter for String type is impacted, the order of going through converters seems changed.\n\n2) originally, converter will be applied after check controller parameter need it, but after configuration, converter is applied according to `contentType` always despite controller parameter require or not. for example scenario 2 above actually read input by `@RequestBody`, jason conversion is not expected.\n\n\n\n---\n\n**Affects:** 3.2.1\n\n0 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453401662","453401663","453401664","453401665","453401666","453401667","453401668"], "labels":["in: web","status: declined","type: enhancement"]}