{"id":"21592", "title":"ClientResponse's body ignored on UnsupportedMediaTypeException [SPR-17054]", "body":"**[Denys Ivano](https://jira.spring.io/secure/ViewProfile.jspa?name=denys)** opened **[SPR-17054](https://jira.spring.io/browse/SPR-17054?redirect=false)** and commented

When remote service responses with `Content-Type` that can't be read by `HttpMessageReader`, an instance of `UnsupportedMediaTypeException` is being thrown (see `BodyExtractors.readWithMessageReaders()`). But `ClientResponse`'s body is being ignored in this case.

From `ClientResponse`'s Javadoc:

```java
* <p><strong>NOTE:</strong> When given access to a {@link ClientResponse},
* through the {@code WebClient}
* {@link WebClient.RequestHeadersSpec#exchange() exchange()} method,
* you must always use one of the body or toEntity methods to ensure resources
* are released and avoid potential issues with HTTP connection pooling.
* You can use {@code bodyToMono(Void.class)} if no response content is
* expected. However keep in mind that if the response does have content, the
* connection will be closed and will not be placed back in the pool.
```

So in order to release resources and avoid potential issues with HTTP connection pool, the response body must be consumed.

I've created a test that reproduces this issue:

```java
@Test
public void shouldConsumeBodyOnUnsupportedMediaTypeException() {
    AtomicBoolean bodyConsumed = new AtomicBoolean();
    ExchangeFunction exchangeFunction = mock(ExchangeFunction.class);
    ClientResponse response = ClientResponse.create(HttpStatus.OK)
            .header(HttpHeaders.CONTENT_TYPE, \"application/unknown\")
//                .header(HttpHeaders.CONTENT_TYPE, \"application/json\")
            .body(Flux.defer(() -> {
                DataBufferFactory dataBufferFactory = new DefaultDataBufferFactory();
                return Flux.just(\"{\\\"name\\\": \\\"Hello World!\\\"}\").
                        map(s -> {
                            bodyConsumed.set(true);
                            byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
                            return dataBufferFactory.wrap(bytes);
                        });
            }))
            .build();

    when(exchangeFunction.exchange(any())).thenReturn(Mono.just(response));

    WebClient webClient = WebClient.builder()
            .exchangeFunction(exchangeFunction)
            .build();

    Mono<String> result = webClient.get()
            .retrieve()
            .bodyToMono(TestResponse.class)
            .map(TestResponse::getName);

    StepVerifier.create(result)
            .expectError(UnsupportedMediaTypeException.class)
//                .expectNext(\"Hello World!\")
//                .expectComplete()
            .verify(Duration.ZERO);

    assertTrue(\"Response body wasn't consumed\", bodyConsumed.get());
}

private static class TestResponse {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

---

**Issue Links:**
- #21563 DataBufferUtils#join could leak buffers in case of error from the source
- #22014 WebClient throws \"Only one connection receive subscriber allowed\" when response has content but no Content-Type header

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a410d90439e5f4a682287f144c98801f40afcc41, https://github.com/spring-projects/spring-framework/commit/d0ada5653f02954c7d1b5014e798400e6846828e
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21592","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21592/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21592/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21592/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21592","id":398231007,"node_id":"MDU6SXNzdWUzOTgyMzEwMDc=","number":21592,"title":"ClientResponse's body ignored on UnsupportedMediaTypeException [SPR-17054]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/194","html_url":"https://github.com/spring-projects/spring-framework/milestone/194","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/194/labels","id":3960967,"node_id":"MDk6TWlsZXN0b25lMzk2MDk2Nw==","number":194,"title":"5.1 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":144,"state":"closed","created_at":"2019-01-10T22:05:44Z","updated_at":"2019-01-11T10:39:45Z","due_on":"2018-07-25T07:00:00Z","closed_at":"2019-01-10T22:05:44Z"},"comments":0,"created_at":"2018-07-17T14:30:15Z","updated_at":"2019-01-11T12:47:43Z","closed_at":"2018-11-09T20:41:36Z","author_association":"COLLABORATOR","body":"**[Denys Ivano](https://jira.spring.io/secure/ViewProfile.jspa?name=denys)** opened **[SPR-17054](https://jira.spring.io/browse/SPR-17054?redirect=false)** and commented\n\nWhen remote service responses with `Content-Type` that can't be read by `HttpMessageReader`, an instance of `UnsupportedMediaTypeException` is being thrown (see `BodyExtractors.readWithMessageReaders()`). But `ClientResponse`'s body is being ignored in this case.\n\nFrom `ClientResponse`'s Javadoc:\n\n```java\n* <p><strong>NOTE:</strong> When given access to a {@link ClientResponse},\n* through the {@code WebClient}\n* {@link WebClient.RequestHeadersSpec#exchange() exchange()} method,\n* you must always use one of the body or toEntity methods to ensure resources\n* are released and avoid potential issues with HTTP connection pooling.\n* You can use {@code bodyToMono(Void.class)} if no response content is\n* expected. However keep in mind that if the response does have content, the\n* connection will be closed and will not be placed back in the pool.\n```\n\nSo in order to release resources and avoid potential issues with HTTP connection pool, the response body must be consumed.\n\nI've created a test that reproduces this issue:\n\n```java\n@Test\npublic void shouldConsumeBodyOnUnsupportedMediaTypeException() {\n    AtomicBoolean bodyConsumed = new AtomicBoolean();\n    ExchangeFunction exchangeFunction = mock(ExchangeFunction.class);\n    ClientResponse response = ClientResponse.create(HttpStatus.OK)\n            .header(HttpHeaders.CONTENT_TYPE, \"application/unknown\")\n//                .header(HttpHeaders.CONTENT_TYPE, \"application/json\")\n            .body(Flux.defer(() -> {\n                DataBufferFactory dataBufferFactory = new DefaultDataBufferFactory();\n                return Flux.just(\"{\\\"name\\\": \\\"Hello World!\\\"}\").\n                        map(s -> {\n                            bodyConsumed.set(true);\n                            byte[] bytes = s.getBytes(StandardCharsets.UTF_8);\n                            return dataBufferFactory.wrap(bytes);\n                        });\n            }))\n            .build();\n\n    when(exchangeFunction.exchange(any())).thenReturn(Mono.just(response));\n\n    WebClient webClient = WebClient.builder()\n            .exchangeFunction(exchangeFunction)\n            .build();\n\n    Mono<String> result = webClient.get()\n            .retrieve()\n            .bodyToMono(TestResponse.class)\n            .map(TestResponse::getName);\n\n    StepVerifier.create(result)\n            .expectError(UnsupportedMediaTypeException.class)\n//                .expectNext(\"Hello World!\")\n//                .expectComplete()\n            .verify(Duration.ZERO);\n\n    assertTrue(\"Response body wasn't consumed\", bodyConsumed.get());\n}\n\nprivate static class TestResponse {\n\n    private String name;\n\n    public String getName() {\n        return name;\n    }\n\n    public void setName(String name) {\n        this.name = name;\n    }\n}\n```\n\n---\n\n**Issue Links:**\n- #21563 DataBufferUtils#join could leak buffers in case of error from the source\n- #22014 WebClient throws \"Only one connection receive subscriber allowed\" when response has content but no Content-Type header\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a410d90439e5f4a682287f144c98801f40afcc41, https://github.com/spring-projects/spring-framework/commit/d0ada5653f02954c7d1b5014e798400e6846828e\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["type: bug"]}