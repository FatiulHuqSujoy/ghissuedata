{"id":"7935", "title":"LangNamespaceHandler cannot be registered in DefaultNamespaceHandlerResolver when thrid party library is not found [SPR-3250]", "body":"**[chris tam](https://jira.spring.io/secure/ViewProfile.jspa?name=cltam96)** opened **[SPR-3250](https://jira.spring.io/browse/SPR-3250?redirect=false)** and commented

The problem is reported by Mark Menard in <a href=\"http://forum.springframework.org/showthread.php?t=35956\">Spring support forum</a>. I have checked the source code and find out the problem come from LangNamespaceHandler \"init\" method. Currently the \"init\" method contains the following code:
...
public void init() {
registerScriptBeanDefinitionParser(\"groovy\", GroovyScriptFactory.class);
registerScriptBeanDefinitionParser(\"jruby\", JRubyScriptFactory.class);
registerScriptBeanDefinitionParser(\"bsh\", BshScriptFactory.class);
}
...
The above code will throw ClassNotFoundException when either groovy, beanshell or jruby library is not found in the classpath. The DefaultNamespaceHandlerResolver will not register the \"lang\" namespace when \"init\" method throws the ClassNotFoundException.  The lang namespace requires that both groovy, beanshell and jruby library must be in the classpath. Is it possible to add some checking to the init method of LangNamespaceHandler to avoid the above dependency. For example:
...
public void init() {
if (checkGroovyExists()) {
registerScriptBeanDefinitionParser(\"groovy\", GroovyScriptFactory.class);
}
if (checkJRubyExists()) {
registerScriptBeanDefinitionParser(\"jruby\", JRubyScriptFactory.class);
}
if (checkBshExists()) {
registerScriptBeanDefinitionParser(\"bsh\", BshScriptFactory.class);
}

// what to do if all the above jars are not found???

}

private boolean checkGroovyExists() {
try {
Class.forName(\"groovy.lang.GroovyObject\") ;
return true ;
} catch(Exception e) {
return false ;
}
}

private boolean checkBshExists() {
try {
Class.forName(\"bsh.Interpreter\") ;
return true ;
} catch(Exception e) {
return false ;
}
}

private boolean checkJRubyExists() {
try {
Class.forName(\"org.jruby.runtime.builtin.IRubyObject\") ;
return true ;
} catch(Exception e) {
return false ;
}
}
...
Thanks for all the help from spring team.

cheers
chris tam
xenium

---

**Affects:** 2.0.3

**Issue Links:**
- #7942 Cannot find \"lang\" namespace handler unless all library jars are present (_**\"is duplicated by\"**_)
- #7958 NoClassDefFoundError when groovy isn't on classpath (_**\"is duplicated by\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7935","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7935/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7935/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7935/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7935","id":398076154,"node_id":"MDU6SXNzdWUzOTgwNzYxNTQ=","number":7935,"title":"LangNamespaceHandler cannot be registered in DefaultNamespaceHandlerResolver when thrid party library is not found [SPR-3250]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41","html_url":"https://github.com/spring-projects/spring-framework/milestone/41","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/41/labels","id":3960813,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxMw==","number":41,"title":"2.0.4","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":76,"state":"closed","created_at":"2019-01-10T22:02:41Z","updated_at":"2019-01-11T01:26:07Z","due_on":"2007-04-08T07:00:00Z","closed_at":"2019-01-10T22:02:41Z"},"comments":4,"created_at":"2007-03-12T01:39:44Z","updated_at":"2019-01-11T17:16:29Z","closed_at":"2012-06-19T03:51:16Z","author_association":"COLLABORATOR","body":"**[chris tam](https://jira.spring.io/secure/ViewProfile.jspa?name=cltam96)** opened **[SPR-3250](https://jira.spring.io/browse/SPR-3250?redirect=false)** and commented\n\nThe problem is reported by Mark Menard in <a href=\"http://forum.springframework.org/showthread.php?t=35956\">Spring support forum</a>. I have checked the source code and find out the problem come from LangNamespaceHandler \"init\" method. Currently the \"init\" method contains the following code:\n...\npublic void init() {\nregisterScriptBeanDefinitionParser(\"groovy\", GroovyScriptFactory.class);\nregisterScriptBeanDefinitionParser(\"jruby\", JRubyScriptFactory.class);\nregisterScriptBeanDefinitionParser(\"bsh\", BshScriptFactory.class);\n}\n...\nThe above code will throw ClassNotFoundException when either groovy, beanshell or jruby library is not found in the classpath. The DefaultNamespaceHandlerResolver will not register the \"lang\" namespace when \"init\" method throws the ClassNotFoundException.  The lang namespace requires that both groovy, beanshell and jruby library must be in the classpath. Is it possible to add some checking to the init method of LangNamespaceHandler to avoid the above dependency. For example:\n...\npublic void init() {\nif (checkGroovyExists()) {\nregisterScriptBeanDefinitionParser(\"groovy\", GroovyScriptFactory.class);\n}\nif (checkJRubyExists()) {\nregisterScriptBeanDefinitionParser(\"jruby\", JRubyScriptFactory.class);\n}\nif (checkBshExists()) {\nregisterScriptBeanDefinitionParser(\"bsh\", BshScriptFactory.class);\n}\n\n// what to do if all the above jars are not found???\n\n}\n\nprivate boolean checkGroovyExists() {\ntry {\nClass.forName(\"groovy.lang.GroovyObject\") ;\nreturn true ;\n} catch(Exception e) {\nreturn false ;\n}\n}\n\nprivate boolean checkBshExists() {\ntry {\nClass.forName(\"bsh.Interpreter\") ;\nreturn true ;\n} catch(Exception e) {\nreturn false ;\n}\n}\n\nprivate boolean checkJRubyExists() {\ntry {\nClass.forName(\"org.jruby.runtime.builtin.IRubyObject\") ;\nreturn true ;\n} catch(Exception e) {\nreturn false ;\n}\n}\n...\nThanks for all the help from spring team.\n\ncheers\nchris tam\nxenium\n\n---\n\n**Affects:** 2.0.3\n\n**Issue Links:**\n- #7942 Cannot find \"lang\" namespace handler unless all library jars are present (_**\"is duplicated by\"**_)\n- #7958 NoClassDefFoundError when groovy isn't on classpath (_**\"is duplicated by\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453316163","453316164","453316165","453316166"], "labels":["in: core","type: bug"]}