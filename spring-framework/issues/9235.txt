{"id":"9235", "title":"SqlValue interface for ArgPreparedStatementSetter [SPR-4558]", "body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-4558](https://jira.spring.io/browse/SPR-4558?redirect=false)** and commented

Spring should have

---

interface SqlValue {
Object getSqlValue();
}

---

and StatementCreatorUtils.setParameterValueInternal should have:

---

if (value instanceof SqlValue) value = ((SqlValue) value).getSqlValue();

---

This is needed for implicit conversion of some types to SQL values.

In our projects we have SqlValue interface and we've reimplemented ArgPreparedStatementSetter that to SqlValue. And we've made all our DB-related enums (and some other classes) implement SqlValue. Example:

---

enum UserState implement SqlValue {
UNKNOWN(0),
WAITING_FOR_MODERATION(1),
AVAILABLE(2),
DELETED(3),
BLOCKED(4);
final int value;
Object getSqlValue() { return value; }
}

void updateUserState(long userId, UserState state) {
getSimpleJdbcTemplate().update(\"UPDATE users SET state = ? WHERE user_id = ?\", state, userId);
// instead of
getSimpleJdbcTemplate().update(\"UPDATE users SET state = ? WHERE user_id = ?\", state.value(), userId);
}

---

---

**Affects:** 2.5.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9235","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9235/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9235/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9235/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9235","id":398086200,"node_id":"MDU6SXNzdWUzOTgwODYyMDA=","number":9235,"title":"SqlValue interface for ArgPreparedStatementSetter [SPR-4558]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2008-03-08T05:26:31Z","updated_at":"2019-01-13T21:53:10Z","closed_at":"2012-06-19T09:36:20Z","author_association":"COLLABORATOR","body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-4558](https://jira.spring.io/browse/SPR-4558?redirect=false)** and commented\n\nSpring should have\n\n---\n\ninterface SqlValue {\nObject getSqlValue();\n}\n\n---\n\nand StatementCreatorUtils.setParameterValueInternal should have:\n\n---\n\nif (value instanceof SqlValue) value = ((SqlValue) value).getSqlValue();\n\n---\n\nThis is needed for implicit conversion of some types to SQL values.\n\nIn our projects we have SqlValue interface and we've reimplemented ArgPreparedStatementSetter that to SqlValue. And we've made all our DB-related enums (and some other classes) implement SqlValue. Example:\n\n---\n\nenum UserState implement SqlValue {\nUNKNOWN(0),\nWAITING_FOR_MODERATION(1),\nAVAILABLE(2),\nDELETED(3),\nBLOCKED(4);\nfinal int value;\nObject getSqlValue() { return value; }\n}\n\nvoid updateUserState(long userId, UserState state) {\ngetSimpleJdbcTemplate().update(\"UPDATE users SET state = ? WHERE user_id = ?\", state, userId);\n// instead of\ngetSimpleJdbcTemplate().update(\"UPDATE users SET state = ? WHERE user_id = ?\", state.value(), userId);\n}\n\n---\n\n---\n\n**Affects:** 2.5.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453328077","453328078","453328079"], "labels":["in: data","status: declined","type: enhancement"]}