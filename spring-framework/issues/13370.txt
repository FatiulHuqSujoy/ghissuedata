{"id":"13370", "title":"Support not (!) operator for profile selection [SPR-8728]", "body":"**[Tobias Mattsson](https://jira.spring.io/secure/ViewProfile.jspa?name=tmattsson)** opened **[SPR-8728](https://jira.spring.io/browse/SPR-8728?redirect=false)** and commented

It would be really helpful if the profile attribute could take names of profiles that all has to be active.

For instance:

```
<beans profile=\"production,datacenter_us\">
    ...
</beans>
<beans profile=\"production,datacenter_eu\">
    ...
</beans>
```

As for syntax there would need to be a notation that can express both OR and AND, possibly using | and ,

Like:

```
<beans profile=\"staging|production\">
    ...
</beans>
```

Even better would be if the syntax was in line with other expressions such as SpEL.

---

**Affects:** 3.1 M2

**Issue Links:**
- #13818 Provide boolean logic on bean profiles, i.e. NOT profile. (_**\"is duplicated by\"**_)
- #21010 Profile activation: not operator does not work consistently
- #12637 Support profile exclusivity and/or bean definition finality

7 votes, 12 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13370","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13370/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13370/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13370/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13370","id":398114754,"node_id":"MDU6SXNzdWUzOTgxMTQ3NTQ=","number":13370,"title":"Support not (!) operator for profile selection [SPR-8728]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/84","html_url":"https://github.com/spring-projects/spring-framework/milestone/84","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/84/labels","id":3960857,"node_id":"MDk6TWlsZXN0b25lMzk2MDg1Nw==","number":84,"title":"3.2 M1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":59,"state":"closed","created_at":"2019-01-10T22:03:32Z","updated_at":"2019-01-11T06:37:18Z","due_on":"2012-05-28T07:00:00Z","closed_at":"2019-01-10T22:03:32Z"},"comments":9,"created_at":"2011-09-28T02:40:05Z","updated_at":"2019-01-13T21:41:53Z","closed_at":"2018-02-05T00:53:21Z","author_association":"COLLABORATOR","body":"**[Tobias Mattsson](https://jira.spring.io/secure/ViewProfile.jspa?name=tmattsson)** opened **[SPR-8728](https://jira.spring.io/browse/SPR-8728?redirect=false)** and commented\n\nIt would be really helpful if the profile attribute could take names of profiles that all has to be active.\n\nFor instance:\n\n```\n<beans profile=\"production,datacenter_us\">\n    ...\n</beans>\n<beans profile=\"production,datacenter_eu\">\n    ...\n</beans>\n```\n\nAs for syntax there would need to be a notation that can express both OR and AND, possibly using | and ,\n\nLike:\n\n```\n<beans profile=\"staging|production\">\n    ...\n</beans>\n```\n\nEven better would be if the syntax was in line with other expressions such as SpEL.\n\n---\n\n**Affects:** 3.1 M2\n\n**Issue Links:**\n- #13818 Provide boolean logic on bean profiles, i.e. NOT profile. (_**\"is duplicated by\"**_)\n- #21010 Profile activation: not operator does not work consistently\n- #12637 Support profile exclusivity and/or bean definition finality\n\n7 votes, 12 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453362409","453362410","453362412","453362413","453362414","453362415","453362416","453362418","453362419"], "labels":["in: core","type: enhancement"]}