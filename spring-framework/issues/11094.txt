{"id":"11094", "title":"Placeholders not resolved when using multiple PropertyPlaceHolderConfigurers [SPR-6428]", "body":"**[Gisbert van Rossum](https://jira.spring.io/secure/ViewProfile.jspa?name=gizit)** opened **[SPR-6428](https://jira.spring.io/browse/SPR-6428?redirect=false)** and commented

When using 2 PropertyPlaceHolderConfigurers, placeholders declared (and given a value) in the first PPC are not resolved in the second PPC.

\\<bean id=\"configurer1\" class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">
\\<property name=\"properties\">
\\<bean class=\"java.util.Properties\">
\\<constructor-arg>
\\<map>
\\<entry key=\"resourceDirPlaceHolder\">
\\<value>myResourceDir\\</value>
\\</entry>
\\</map>
\\</constructor-arg>
\\</bean>
\\</property>
\\<property name=\"order\" value=\"1\"/>
\\<property name=\"ignoreUnresolvablePlaceholders\" value=\"true\"/>
\\</bean>
\\<bean id=\"configurer2\" class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">
\\<property name=\"locations\">
\\<list>
\\<value>classpath:${resourceDirPlaceHolder}/props.properties\\</value>
\\</list>
\\</property>
\\</bean>

this configuration results in:

java.io.FileNotFoundException: class path resource [${resourceDirPlaceHolder}/props.properties] cannot be opened because it does not exist

---

**Affects:** 2.5.6

**Attachments:**
- [multiplePPCs.zip](https://jira.spring.io/secure/attachment/15954/multiplePPCs.zip) (_4.84 kB_)

**Issue Links:**
- #10389 only the first PropertyPlaceHolderConfigurer is initialized. Others are not (_**\"is duplicated by\"**_)
- #10389 only the first PropertyPlaceHolderConfigurer is initialized. Others are not

**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/09aea36859bb0d806eb4085a2c154cb781bd8345

2 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11094","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11094/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11094/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11094/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11094","id":398099799,"node_id":"MDU6SXNzdWUzOTgwOTk3OTk=","number":11094,"title":"Placeholders not resolved when using multiple PropertyPlaceHolderConfigurers [SPR-6428]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2009-11-23T22:39:28Z","updated_at":"2019-01-12T16:38:48Z","closed_at":"2011-07-02T01:52:20Z","author_association":"COLLABORATOR","body":"**[Gisbert van Rossum](https://jira.spring.io/secure/ViewProfile.jspa?name=gizit)** opened **[SPR-6428](https://jira.spring.io/browse/SPR-6428?redirect=false)** and commented\n\nWhen using 2 PropertyPlaceHolderConfigurers, placeholders declared (and given a value) in the first PPC are not resolved in the second PPC.\n\n\\<bean id=\"configurer1\" class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">\n\\<property name=\"properties\">\n\\<bean class=\"java.util.Properties\">\n\\<constructor-arg>\n\\<map>\n\\<entry key=\"resourceDirPlaceHolder\">\n\\<value>myResourceDir\\</value>\n\\</entry>\n\\</map>\n\\</constructor-arg>\n\\</bean>\n\\</property>\n\\<property name=\"order\" value=\"1\"/>\n\\<property name=\"ignoreUnresolvablePlaceholders\" value=\"true\"/>\n\\</bean>\n\\<bean id=\"configurer2\" class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">\n\\<property name=\"locations\">\n\\<list>\n\\<value>classpath:${resourceDirPlaceHolder}/props.properties\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\nthis configuration results in:\n\njava.io.FileNotFoundException: class path resource [${resourceDirPlaceHolder}/props.properties] cannot be opened because it does not exist\n\n---\n\n**Affects:** 2.5.6\n\n**Attachments:**\n- [multiplePPCs.zip](https://jira.spring.io/secure/attachment/15954/multiplePPCs.zip) (_4.84 kB_)\n\n**Issue Links:**\n- #10389 only the first PropertyPlaceHolderConfigurer is initialized. Others are not (_**\"is duplicated by\"**_)\n- #10389 only the first PropertyPlaceHolderConfigurer is initialized. Others are not\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/09aea36859bb0d806eb4085a2c154cb781bd8345\n\n2 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453344381","453344382","453344383","453344384","453344385","453344387"], "labels":["in: core","status: declined"]}