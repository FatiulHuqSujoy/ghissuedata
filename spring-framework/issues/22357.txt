{"id":"22357", "title":"Adding @ModelAttribute is breaking Flux request body", "body":"On a controller method that processes `multipart/form-data` with `Flux<Part>`, adding a `@ModelAttribute` results in a `Request body input error` error (which wraps an `Only one connection receive subscriber allowed.` error).

Controller:

```kotlin
@RestController
class DemoController {
    @PostMapping(\"/working\")
    fun working(@RequestBody body: Flux<Part>): Flux<String> = body.map { it.name() + \"\\n\" }

    @PostMapping(\"/not-working\")
    fun notWorking(@ModelAttribute foo: String, @RequestBody body: Flux<Part>): Flux<String> = body.map { it.name() + \"\\n\" }
}
```

Making the request with curl:

```bash
curl -i -X POST http://localhost:8080/working --form 'name=@/path/to/some/file'
curl -i -X POST http://localhost:8080/not-working --form 'name=@/path/to/some/file'
```

Output:

```
HTTP/1.1 100 Continue

HTTP/1.1 200 OK
transfer-encoding: chunked
Content-Type: text/plain;charset=UTF-8

name
```

```
HTTP/1.1 100 Continue

HTTP/1.1 100 Continue

HTTP/1.1 500 Internal Server Error
Content-Type: application/json;charset=UTF-8
Content-Length: 148

{\"timestamp\":\"2019-02-06T11:03:12.242+0000\",\"path\":\"/not-working\",\"status\":500,\"error\":\"Internal Server Error\",\"message\":\"Request body input error\"}
```

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22357","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22357/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22357/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22357/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22357","id":407184835,"node_id":"MDU6SXNzdWU0MDcxODQ4MzU=","number":22357,"title":"Adding @ModelAttribute is breaking Flux request body","user":{"login":"lukasjapan","id":18628030,"node_id":"MDQ6VXNlcjE4NjI4MDMw","avatar_url":"https://avatars0.githubusercontent.com/u/18628030?v=4","gravatar_id":"","url":"https://api.github.com/users/lukasjapan","html_url":"https://github.com/lukasjapan","followers_url":"https://api.github.com/users/lukasjapan/followers","following_url":"https://api.github.com/users/lukasjapan/following{/other_user}","gists_url":"https://api.github.com/users/lukasjapan/gists{/gist_id}","starred_url":"https://api.github.com/users/lukasjapan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukasjapan/subscriptions","organizations_url":"https://api.github.com/users/lukasjapan/orgs","repos_url":"https://api.github.com/users/lukasjapan/repos","events_url":"https://api.github.com/users/lukasjapan/events{/privacy}","received_events_url":"https://api.github.com/users/lukasjapan/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2019-02-06T11:11:07Z","updated_at":"2019-02-07T17:16:51Z","closed_at":"2019-02-07T04:50:46Z","author_association":"NONE","body":"On a controller method that processes `multipart/form-data` with `Flux<Part>`, adding a `@ModelAttribute` results in a `Request body input error` error (which wraps an `Only one connection receive subscriber allowed.` error).\r\n\r\nController:\r\n\r\n```kotlin\r\n@RestController\r\nclass DemoController {\r\n    @PostMapping(\"/working\")\r\n    fun working(@RequestBody body: Flux<Part>): Flux<String> = body.map { it.name() + \"\\n\" }\r\n\r\n    @PostMapping(\"/not-working\")\r\n    fun notWorking(@ModelAttribute foo: String, @RequestBody body: Flux<Part>): Flux<String> = body.map { it.name() + \"\\n\" }\r\n}\r\n```\r\n\r\nMaking the request with curl:\r\n\r\n```bash\r\ncurl -i -X POST http://localhost:8080/working --form 'name=@/path/to/some/file'\r\ncurl -i -X POST http://localhost:8080/not-working --form 'name=@/path/to/some/file'\r\n```\r\n\r\nOutput:\r\n\r\n```\r\nHTTP/1.1 100 Continue\r\n\r\nHTTP/1.1 200 OK\r\ntransfer-encoding: chunked\r\nContent-Type: text/plain;charset=UTF-8\r\n\r\nname\r\n```\r\n\r\n```\r\nHTTP/1.1 100 Continue\r\n\r\nHTTP/1.1 100 Continue\r\n\r\nHTTP/1.1 500 Internal Server Error\r\nContent-Type: application/json;charset=UTF-8\r\nContent-Length: 148\r\n\r\n{\"timestamp\":\"2019-02-06T11:03:12.242+0000\",\"path\":\"/not-working\",\"status\":500,\"error\":\"Internal Server Error\",\"message\":\"Request body input error\"}\r\n```\r\n\r\n","closed_by":{"login":"lukasjapan","id":18628030,"node_id":"MDQ6VXNlcjE4NjI4MDMw","avatar_url":"https://avatars0.githubusercontent.com/u/18628030?v=4","gravatar_id":"","url":"https://api.github.com/users/lukasjapan","html_url":"https://github.com/lukasjapan","followers_url":"https://api.github.com/users/lukasjapan/followers","following_url":"https://api.github.com/users/lukasjapan/following{/other_user}","gists_url":"https://api.github.com/users/lukasjapan/gists{/gist_id}","starred_url":"https://api.github.com/users/lukasjapan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lukasjapan/subscriptions","organizations_url":"https://api.github.com/users/lukasjapan/orgs","repos_url":"https://api.github.com/users/lukasjapan/repos","events_url":"https://api.github.com/users/lukasjapan/events{/privacy}","received_events_url":"https://api.github.com/users/lukasjapan/received_events","type":"User","site_admin":false}}", "commentIds":["460986799","461019296","461262983","461284313","461287536"], "labels":["status: invalid"]}