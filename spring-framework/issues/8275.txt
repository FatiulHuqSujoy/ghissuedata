{"id":"8275", "title":"DelegatingActionProxy and DelegatingRequestProcessor don't set the ActionServlet on the delegate Action [SPR-3593]", "body":"**[Danny Hurlburt](https://jira.spring.io/secure/ViewProfile.jspa?name=dhurlburtusa)** opened **[SPR-3593](https://jira.spring.io/browse/SPR-3593?redirect=false)*** and commented

DelegatingActionProxy and DelegatingRequestProcessor don't set the ActionServlet on the delegate Action. Many Strut's Actions call this.getServlet().getServletContext() to get access to the ServletContext. Without setting the ActionServlet, these classes violate the contract set by the Action class.

The following is an excerpt of the Action's Javadoc:

* 

\\<p>When an \\<code>Action\\</code> instance is first created, the controller
* will call \\<code>setServlet\\</code> with a non-null argument to
* identify the servlet instance to which this Action is attached.
* When the servlet is to be shut down (or restarted), the
* \\<code>setServlet\\</code> method will be called with a \\<code>null\\</code>
* argument, which can be used to clean up any allocated resources in use
* by this Action.\\</p>

The following are the patches for both classes:

--- C:/Temp/Original/DelegatingActionProxy.java	Fri Jun 15 18:23:42 2007
+++ C:/Temp/Fixed/DelegatingActionProxy.java	Fri Jun 15 18:29:29 2007
@@ -115,13 +115,16 @@
* `@return` the delegate Action
* `@throws` BeansException if thrown by WebApplicationContext methods
* `@see` #determineActionBeanName
  */
  protected Action getDelegateAction(ActionMapping mapping) throws BeansException {

- 

      WebApplicationContext wac = getWebApplicationContext(getServlet(), mapping.getModuleConfig());

+ 

      ActionServlet servlet = getServlet();

+ 

      WebApplicationContext wac = getWebApplicationContext(servlet, mapping.getModuleConfig());
      String beanName = determineActionBeanName(mapping);

- 

      return (Action) wac.getBean(beanName, Action.class);

+ 

      Action delegateAction = (Action) wac.getBean(beanName, Action.class);

+ 

      delegateAction.setServlet(servlet);

+ 

      return delegateAction;

  }

  /**

  * Fetch ContextLoaderPlugIn's WebApplicationContext from the ServletContext,
  * falling back to the root WebApplicationContext. This context is supposed

---

--- C:/Temp/Original/DelegatingRequestProcessor.java	Fri Jun 15 18:44:24 2007
+++ C:/Temp/Fixed/DelegatingRequestProcessor.java	Fri Jun 15 18:43:16 2007
@@ -163,11 +163,13 @@
protected Action getDelegateAction(ActionMapping mapping) throws BeansException {
String beanName = determineActionBeanName(mapping);
if (!getWebApplicationContext().containsBean(beanName)) {
return null;
}

---

    return (Action) getWebApplicationContext().getBean(beanName, Action.class);

+ 

      Action delegateAction = (Action) getWebApplicationContext().getBean(beanName, Action.class);

+ 

      delegateAction.setServlet(servlet);

+ 

      return delegateAction;

  }

  /**

  * Determine the name of the Action bean, to be looked up in
  * the WebApplicationContext.

---

**Affects:** 2.0.5

**Attachments:**
- [DelegatingActionProxy.patch](https://jira.spring.io/secure/attachment/12672/DelegatingActionProxy.patch) (_1.05 kB_)
- [DelegatingRequestProcessor.patch](https://jira.spring.io/secure/attachment/12673/DelegatingRequestProcessor.patch) (_761 bytes_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8275","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8275/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8275/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8275/events","html_url":"https://github.com/spring-projects/spring-framework/issues/8275","id":398078786,"node_id":"MDU6SXNzdWUzOTgwNzg3ODY=","number":8275,"title":"DelegatingActionProxy and DelegatingRequestProcessor don't set the ActionServlet on the delegate Action [SPR-3593]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2007-06-15T12:11:14Z","updated_at":"2019-01-13T08:08:09Z","closed_at":"2007-06-17T04:41:06Z","author_association":"COLLABORATOR","body":"**[Danny Hurlburt](https://jira.spring.io/secure/ViewProfile.jspa?name=dhurlburtusa)** opened **[SPR-3593](https://jira.spring.io/browse/SPR-3593?redirect=false)*** and commented\n\nDelegatingActionProxy and DelegatingRequestProcessor don't set the ActionServlet on the delegate Action. Many Strut's Actions call this.getServlet().getServletContext() to get access to the ServletContext. Without setting the ActionServlet, these classes violate the contract set by the Action class.\n\nThe following is an excerpt of the Action's Javadoc:\n\n* \n\n\\<p>When an \\<code>Action\\</code> instance is first created, the controller\n* will call \\<code>setServlet\\</code> with a non-null argument to\n* identify the servlet instance to which this Action is attached.\n* When the servlet is to be shut down (or restarted), the\n* \\<code>setServlet\\</code> method will be called with a \\<code>null\\</code>\n* argument, which can be used to clean up any allocated resources in use\n* by this Action.\\</p>\n\nThe following are the patches for both classes:\n\n--- C:/Temp/Original/DelegatingActionProxy.java\tFri Jun 15 18:23:42 2007\n+++ C:/Temp/Fixed/DelegatingActionProxy.java\tFri Jun 15 18:29:29 2007\n@@ -115,13 +115,16 @@\n* `@return` the delegate Action\n* `@throws` BeansException if thrown by WebApplicationContext methods\n* `@see` #determineActionBeanName\n  */\n  protected Action getDelegateAction(ActionMapping mapping) throws BeansException {\n\n- \n\n      WebApplicationContext wac = getWebApplicationContext(getServlet(), mapping.getModuleConfig());\n\n+ \n\n      ActionServlet servlet = getServlet();\n\n+ \n\n      WebApplicationContext wac = getWebApplicationContext(servlet, mapping.getModuleConfig());\n      String beanName = determineActionBeanName(mapping);\n\n- \n\n      return (Action) wac.getBean(beanName, Action.class);\n\n+ \n\n      Action delegateAction = (Action) wac.getBean(beanName, Action.class);\n\n+ \n\n      delegateAction.setServlet(servlet);\n\n+ \n\n      return delegateAction;\n\n  }\n\n  /**\n\n  * Fetch ContextLoaderPlugIn's WebApplicationContext from the ServletContext,\n  * falling back to the root WebApplicationContext. This context is supposed\n\n---\n\n--- C:/Temp/Original/DelegatingRequestProcessor.java\tFri Jun 15 18:44:24 2007\n+++ C:/Temp/Fixed/DelegatingRequestProcessor.java\tFri Jun 15 18:43:16 2007\n@@ -163,11 +163,13 @@\nprotected Action getDelegateAction(ActionMapping mapping) throws BeansException {\nString beanName = determineActionBeanName(mapping);\nif (!getWebApplicationContext().containsBean(beanName)) {\nreturn null;\n}\n\n---\n\n    return (Action) getWebApplicationContext().getBean(beanName, Action.class);\n\n+ \n\n      Action delegateAction = (Action) getWebApplicationContext().getBean(beanName, Action.class);\n\n+ \n\n      delegateAction.setServlet(servlet);\n\n+ \n\n      return delegateAction;\n\n  }\n\n  /**\n\n  * Determine the name of the Action bean, to be looked up in\n  * the WebApplicationContext.\n\n---\n\n**Affects:** 2.0.5\n\n**Attachments:**\n- [DelegatingActionProxy.patch](https://jira.spring.io/secure/attachment/12672/DelegatingActionProxy.patch) (_1.05 kB_)\n- [DelegatingRequestProcessor.patch](https://jira.spring.io/secure/attachment/12673/DelegatingRequestProcessor.patch) (_761 bytes_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453319211"], "labels":["in: web","status: declined"]}