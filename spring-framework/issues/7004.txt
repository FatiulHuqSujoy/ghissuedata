{"id":"7004", "title":"SqlParameter should support scale for double values [SPR-2315]", "body":"**[Sebastian Radics](https://jira.spring.io/secure/ViewProfile.jspa?name=radicss)** opened **[SPR-2315](https://jira.spring.io/browse/SPR-2315?redirect=false)** and commented

A double-object insert with SqlUpdate cannot be done with a loose of precision.  20.25 is stored as 20.00. See simple-Test-Method:

public void testSimpleDoubleInsert() throws Exception{
JdbcTemplate v_jdbcTemplate = new JdbcTemplate(getDataSource());
//direct double write - works fine
v_jdbcTemplate.execute(\"create table cmdb2adm.test(id integer not null, wert decimal(15, 2) not null)\");
v_jdbcTemplate.execute(\"insert into cmdb2adm.test(id,wert) values(1,20.25)\");
final int[] v_resultCount = {0};
v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){
public void processRow(ResultSet a_rs) throws SQLException {
assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);
v_resultCount[0]++;
}
});
assertEquals(1,v_resultCount[0]);

    //insert with prepared statement and direct double write- works fine
    v_resultCount[0]=0;
    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");
    Connection v_conn = getDataSource().getConnection();
    PreparedStatement v_stmt = v_conn.prepareStatement(\"insert into cmdb2adm.test(id,wert) values(?,?)\");
    v_stmt.setInt(1,1);
    v_stmt.setDouble(2,20.25);
    v_stmt.execute();
    v_stmt.close();
    v_conn.close();
    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){
         public void processRow(ResultSet a_rs) throws SQLException {
              assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);
              v_resultCount[0]++;
         }
    });
    assertEquals(1,v_resultCount[0]);
    //insert with prepared statement and double-Object write- works fine  -->Use of special setObject-Method
    v_resultCount[0]=0;
    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");
    v_conn = getDataSource().getConnection();
    v_stmt = v_conn.prepareStatement(\"insert into cmdb2adm.test(id,wert) values(?,?)\");
    v_stmt.setInt(1,1);
    v_stmt.setObject(2,new Double(20.25),Types.DOUBLE,2);
    v_stmt.execute();
    v_stmt.close();
    v_conn.close();
    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){
         public void processRow(ResultSet a_rs) throws SQLException {
              assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);
              v_resultCount[0]++;
         }
    });
    assertEquals(1,v_resultCount[0]);
    //insert with sql-Update doesn't work - Problem ist SqlParameter - no Support for Double-Decimal-Range
    v_resultCount[0]=0;
    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");
    SqlUpdate v_update = new SqlUpdate(getDataSource(),\"insert into cmdb2adm.test(id,wert) values(?,?)\");
    v_update.declareParameter(new SqlParameter(\"id\",Types.INTEGER));
    v_update.declareParameter(new SqlParameter(\"wert\",Types.DOUBLE));
    Object[] v_values = new Object[]{Integer.valueOf(1),\"20.25\"};
    v_update.update(v_values);
    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){
         public void processRow(ResultSet a_rs) throws SQLException {

//Bug
assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);   
v_resultCount[0]++;
}
});
assertEquals(1,v_resultCount[0]);
}

---

**Affects:** 1.2.8

**Attachments:**
- [SqlParameter.java](https://jira.spring.io/secure/attachment/11815/SqlParameter.java) (_4.85 kB_)
- [StatementCreatorUtils.java](https://jira.spring.io/secure/attachment/11816/StatementCreatorUtils.java) (_8.57 kB_)

**Issue Links:**
- #8397 SqlOutParameter should support scale

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7004","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7004/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7004/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7004/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7004","id":398068510,"node_id":"MDU6SXNzdWUzOTgwNjg1MTA=","number":7004,"title":"SqlParameter should support scale for double values [SPR-2315]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false,"description":"Issues in data modules (jdbc, orm, oxm, tx)"},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false,"description":"A general enhancement"}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/42","html_url":"https://github.com/spring-projects/spring-framework/milestone/42","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/42/labels","id":3960814,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxNA==","number":42,"title":"2.0.5","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":58,"state":"closed","created_at":"2019-01-10T22:02:42Z","updated_at":"2019-01-11T00:35:01Z","due_on":"2007-05-05T07:00:00Z","closed_at":"2019-01-10T22:02:42Z"},"comments":3,"created_at":"2006-07-17T20:22:03Z","updated_at":"2019-01-11T18:33:10Z","closed_at":"2012-06-19T03:52:47Z","author_association":"COLLABORATOR","body":"**[Sebastian Radics](https://jira.spring.io/secure/ViewProfile.jspa?name=radicss)** opened **[SPR-2315](https://jira.spring.io/browse/SPR-2315?redirect=false)** and commented\n\nA double-object insert with SqlUpdate cannot be done with a loose of precision.  20.25 is stored as 20.00. See simple-Test-Method:\n\npublic void testSimpleDoubleInsert() throws Exception{\nJdbcTemplate v_jdbcTemplate = new JdbcTemplate(getDataSource());\n//direct double write - works fine\nv_jdbcTemplate.execute(\"create table cmdb2adm.test(id integer not null, wert decimal(15, 2) not null)\");\nv_jdbcTemplate.execute(\"insert into cmdb2adm.test(id,wert) values(1,20.25)\");\nfinal int[] v_resultCount = {0};\nv_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){\npublic void processRow(ResultSet a_rs) throws SQLException {\nassertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);\nv_resultCount[0]++;\n}\n});\nassertEquals(1,v_resultCount[0]);\n\n    //insert with prepared statement and direct double write- works fine\n    v_resultCount[0]=0;\n    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");\n    Connection v_conn = getDataSource().getConnection();\n    PreparedStatement v_stmt = v_conn.prepareStatement(\"insert into cmdb2adm.test(id,wert) values(?,?)\");\n    v_stmt.setInt(1,1);\n    v_stmt.setDouble(2,20.25);\n    v_stmt.execute();\n    v_stmt.close();\n    v_conn.close();\n    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){\n         public void processRow(ResultSet a_rs) throws SQLException {\n              assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);\n              v_resultCount[0]++;\n         }\n    });\n    assertEquals(1,v_resultCount[0]);\n    //insert with prepared statement and double-Object write- works fine  -->Use of special setObject-Method\n    v_resultCount[0]=0;\n    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");\n    v_conn = getDataSource().getConnection();\n    v_stmt = v_conn.prepareStatement(\"insert into cmdb2adm.test(id,wert) values(?,?)\");\n    v_stmt.setInt(1,1);\n    v_stmt.setObject(2,new Double(20.25),Types.DOUBLE,2);\n    v_stmt.execute();\n    v_stmt.close();\n    v_conn.close();\n    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){\n         public void processRow(ResultSet a_rs) throws SQLException {\n              assertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);\n              v_resultCount[0]++;\n         }\n    });\n    assertEquals(1,v_resultCount[0]);\n    //insert with sql-Update doesn't work - Problem ist SqlParameter - no Support for Double-Decimal-Range\n    v_resultCount[0]=0;\n    v_jdbcTemplate.execute(\"delete from cmdb2adm.test\");\n    SqlUpdate v_update = new SqlUpdate(getDataSource(),\"insert into cmdb2adm.test(id,wert) values(?,?)\");\n    v_update.declareParameter(new SqlParameter(\"id\",Types.INTEGER));\n    v_update.declareParameter(new SqlParameter(\"wert\",Types.DOUBLE));\n    Object[] v_values = new Object[]{Integer.valueOf(1),\"20.25\"};\n    v_update.update(v_values);\n    v_jdbcTemplate.query(\"select id,wert from cmdb2adm.test\",new RowCallbackHandler(){\n         public void processRow(ResultSet a_rs) throws SQLException {\n\n//Bug\nassertEquals(20.25,a_rs.getDouble(\"wert\"),0.001);   \nv_resultCount[0]++;\n}\n});\nassertEquals(1,v_resultCount[0]);\n}\n\n---\n\n**Affects:** 1.2.8\n\n**Attachments:**\n- [SqlParameter.java](https://jira.spring.io/secure/attachment/11815/SqlParameter.java) (_4.85 kB_)\n- [StatementCreatorUtils.java](https://jira.spring.io/secure/attachment/11816/StatementCreatorUtils.java) (_8.57 kB_)\n\n**Issue Links:**\n- #8397 SqlOutParameter should support scale\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453307370","453307372","453307373"], "labels":["in: core","in: data","type: enhancement"]}