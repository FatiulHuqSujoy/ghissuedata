{"id":"13779", "title":"Autowiring fails, when jars are loaded as per skinny war concept where classes involved have default accessors [SPR-9140]", "body":"**[RSKB](https://jira.spring.io/secure/ViewProfile.jspa?name=rashmibhojwani)** opened **[SPR-9140](https://jira.spring.io/browse/SPR-9140?redirect=false)** and commented

Our application is based on skinny war concept(http://maven.apache.org/plugins/maven-war-plugin/examples/skinny-wars.html).
.
|-- META-INF
|   `-- application.xml
|-- lib
|    -- core application jars
|    -- spring jars
|    -- common jar
|-- war1-1.0.0.war
|     --war1-jar1-1.0.0.jar
`   - war2-1.0.0.war
|     --war2-jar1-1.0.0.jar

We have spring jars and other core application jars loaded at application level. The common jar and the core application jars have their own applicationContext.xml. Also the wars have their own web-jars that have applicationContext.xml specific to each war. We noticed, that autowiring(in core application jar classes) failed at the application startup and we see errors as given in the logs files attached herewith.

Following are the observations:

1. Autowiring works fine, if all the jars loaded at application level, are moved to individual wars(WEB-INF/lib).
2. Autowiring works fine, if the classes in the core application jars have public accessors, which is default for security and encapsulation purposes.

We have to follow the skinny war concept and the core application jars cannot be modified.

Please suggest!

Thanks
Rashmi.


---

**Affects:** 3.0 GA

**Attachments:**
- [SystemErr.log](https://jira.spring.io/secure/attachment/19443/SystemErr.log) (_72.32 kB_)
- [SystemOut.log](https://jira.spring.io/secure/attachment/19442/SystemOut.log) (_145.32 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13779","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13779/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13779/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13779/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13779","id":398117421,"node_id":"MDU6SXNzdWUzOTgxMTc0MjE=","number":13779,"title":"Autowiring fails, when jars are loaded as per skinny war concept where classes involved have default accessors [SPR-9140]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2012-02-20T21:51:22Z","updated_at":"2019-01-12T05:29:03Z","closed_at":"2012-03-08T04:03:02Z","author_association":"COLLABORATOR","body":"**[RSKB](https://jira.spring.io/secure/ViewProfile.jspa?name=rashmibhojwani)** opened **[SPR-9140](https://jira.spring.io/browse/SPR-9140?redirect=false)** and commented\n\nOur application is based on skinny war concept(http://maven.apache.org/plugins/maven-war-plugin/examples/skinny-wars.html).\n.\n|-- META-INF\n|   `-- application.xml\n|-- lib\n|    -- core application jars\n|    -- spring jars\n|    -- common jar\n|-- war1-1.0.0.war\n|     --war1-jar1-1.0.0.jar\n`   - war2-1.0.0.war\n|     --war2-jar1-1.0.0.jar\n\nWe have spring jars and other core application jars loaded at application level. The common jar and the core application jars have their own applicationContext.xml. Also the wars have their own web-jars that have applicationContext.xml specific to each war. We noticed, that autowiring(in core application jar classes) failed at the application startup and we see errors as given in the logs files attached herewith.\n\nFollowing are the observations:\n\n1. Autowiring works fine, if all the jars loaded at application level, are moved to individual wars(WEB-INF/lib).\n2. Autowiring works fine, if the classes in the core application jars have public accessors, which is default for security and encapsulation purposes.\n\nWe have to follow the skinny war concept and the core application jars cannot be modified.\n\nPlease suggest!\n\nThanks\nRashmi.\n\n\n---\n\n**Affects:** 3.0 GA\n\n**Attachments:**\n- [SystemErr.log](https://jira.spring.io/secure/attachment/19443/SystemErr.log) (_72.32 kB_)\n- [SystemOut.log](https://jira.spring.io/secure/attachment/19442/SystemOut.log) (_145.32 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453365763","453365764","453365765","453365767","453365769"], "labels":["in: core","in: data","status: invalid"]}