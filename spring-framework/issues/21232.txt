{"id":"21232", "title":"Safari doesn't receive chunked data using WebFlux's SSE [SPR-16691]", "body":"**[Adrian Marszałek](https://jira.spring.io/secure/ViewProfile.jspa?name=szinek)** opened **[SPR-16691](https://jira.spring.io/browse/SPR-16691?redirect=false)** and commented

Complete source code:

```java
@Configuration
class Routing {

    @Bean
    fun booksRouter(handler: BooksHandler) = router {
        (\"/books\" and accept(MediaType.APPLICATION_JSON)).nest {
            GET(\"/\", handler::getAll)
        }
    }
}


data class Book(val title: String,
                val author: String)

@Component
class BooksHandler(private val repository: BookRepository) {

    fun getAll(request: ServerRequest): Mono<ServerResponse> {
        val interval = Flux.interval(Duration.ofSeconds(1))

        val books = listOf(Book(\"title\", \"author\"),
                Book(\"title\", \"author\"),
                Book(\"title\", \"author\"),
                Book(\"title\", \"author\")).toFlux()
        return ok().bodyToServerSentEvents(Flux.zip(interval, books).map({ it.t2 }))
    }
}
```

The whole project generated from Intializr: Spring Boot 2.0.0 with Kotlin 1.2.31.
Run the application and access the resource directly through Safari: http://localhost:8080/books
Instead of receiving a book every second, Safari waits for as long as the Flux.zip() takes and then it receives the whole list of books.


---
No further details from [SPR-16691](https://jira.spring.io/browse/SPR-16691?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21232","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21232/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21232/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21232/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21232","id":398225803,"node_id":"MDU6SXNzdWUzOTgyMjU4MDM=","number":21232,"title":"Safari doesn't receive chunked data using WebFlux's SSE [SPR-16691]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2018-04-04T10:59:16Z","updated_at":"2019-01-12T05:19:02Z","closed_at":"2018-04-04T14:11:29Z","author_association":"COLLABORATOR","body":"**[Adrian Marszałek](https://jira.spring.io/secure/ViewProfile.jspa?name=szinek)** opened **[SPR-16691](https://jira.spring.io/browse/SPR-16691?redirect=false)** and commented\n\nComplete source code:\n\n```java\n@Configuration\nclass Routing {\n\n    @Bean\n    fun booksRouter(handler: BooksHandler) = router {\n        (\"/books\" and accept(MediaType.APPLICATION_JSON)).nest {\n            GET(\"/\", handler::getAll)\n        }\n    }\n}\n\n\ndata class Book(val title: String,\n                val author: String)\n\n@Component\nclass BooksHandler(private val repository: BookRepository) {\n\n    fun getAll(request: ServerRequest): Mono<ServerResponse> {\n        val interval = Flux.interval(Duration.ofSeconds(1))\n\n        val books = listOf(Book(\"title\", \"author\"),\n                Book(\"title\", \"author\"),\n                Book(\"title\", \"author\"),\n                Book(\"title\", \"author\")).toFlux()\n        return ok().bodyToServerSentEvents(Flux.zip(interval, books).map({ it.t2 }))\n    }\n}\n```\n\nThe whole project generated from Intializr: Spring Boot 2.0.0 with Kotlin 1.2.31.\nRun the application and access the resource directly through Safari: http://localhost:8080/books\nInstead of receiving a book every second, Safari waits for as long as the Flux.zip() takes and then it receives the whole list of books.\n\n\n---\nNo further details from [SPR-16691](https://jira.spring.io/browse/SPR-16691?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453470152"], "labels":["status: invalid"]}