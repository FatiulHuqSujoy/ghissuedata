{"id":"4847", "title":"ArrayIndexOutOfBoundsException with AOP [SPR-113]", "body":"**[Robert Newson](https://jira.spring.io/secure/ViewProfile.jspa?name=rnewson)** opened **[SPR-113](https://jira.spring.io/browse/SPR-113?redirect=false)** and commented

I use AOP for logging. On occasion I can get the following exception. If I remove the autoproxy bean, the problem no longer happens. I've appended my aop logging bean setup up after the exception trace.

java.lang.ArrayIndexOutOfBoundsException: -1
at org.apache.oro.text.regex.Perl5Matcher.__match(Unknown Source)
at org.apache.oro.text.regex.Perl5Matcher.__match(Unknown Source)
at org.apache.oro.text.regex.Perl5Matcher.__tryExpression(Unknown Source)
at org.apache.oro.text.regex.Perl5Matcher.matches(Unknown Source)
at org.apache.oro.text.regex.Perl5Matcher.matches(Unknown Source)
at org.springframework.aop.support.RegexpMethodPointcut.matches(RegexpMethodPointcut.java:110)
at org.springframework.aop.framework.AdvisorChainFactoryUtils.calculateInterceptorsAndDynamicInterceptionAdvice(AdvisorChainFacto
ryUtils.java:60)
at org.springframework.aop.framework.HashMapCachingAdvisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(HashMapCaching
AdvisorChainFactory.java:57)
at org.springframework.aop.framework.JdkDynamicAopProxy.invoke(JdkDynamicAopProxy.java:129)

\\<!--- AOP Logging -->

\\<bean id=\"autoproxy\" class=\"org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator\" 
dependency-check=\"none\"/>

\\<bean id=\"errorAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"
dependency-check=\"none\">
\\<property name=\"advice\">\\<ref local=\"logThrowsAdvice\"/>\\</property>
\\<property name=\"patterns\">
\\<list>
\\<value>com.connected.*\\</value>
\\</list>
\\</property>
\\</bean>

\\<bean id=\"beforeAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"
dependency-check=\"none\">
\\<property name=\"advice\">\\<ref local=\"logBeforeAdvice\"/>\\</property>
\\<property name=\"patterns\">
\\<list>
\\<value>.*.Store.*storeAsset\\</value>
\\</list>
\\</property>
\\</bean>

\\<bean id=\"afterAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"
dependency-check=\"none\">
\\<property name=\"advice\">\\<ref local=\"logAfterAdvice\"/>\\</property>
\\<property name=\"patterns\">
\\<list>
\\<value>.*.Source.*hasNextAsset\\</value>
\\</list>
\\</property>
\\</bean>

\\<bean id=\"logBeforeAdvice\" class=\"com.connected.aop.LogBeforeAdvice\">
\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>
\\</bean>
\\<bean id=\"logAfterAdvice\" class=\"com.connected.aop.LogAfterAdvice\">
\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>
\\</bean>
\\<bean id=\"logThrowsAdvice\" class=\"com.connected.aop.LogThrowsAdvice\">
\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>
\\</bean>
\\<bean id=\"logFormatter\" class=\"com.connected.aop.LogFormatter\"/>
\\<!-- End of AOP -->


---

**Affects:** 1.0 final, 1.0.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4847","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4847/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4847/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4847/events","html_url":"https://github.com/spring-projects/spring-framework/issues/4847","id":398049065,"node_id":"MDU6SXNzdWUzOTgwNDkwNjU=","number":4847,"title":"ArrayIndexOutOfBoundsException with AOP [SPR-113]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2004-05-03T01:20:47Z","updated_at":"2019-01-12T16:30:22Z","closed_at":"2004-05-27T23:35:14Z","author_association":"COLLABORATOR","body":"**[Robert Newson](https://jira.spring.io/secure/ViewProfile.jspa?name=rnewson)** opened **[SPR-113](https://jira.spring.io/browse/SPR-113?redirect=false)** and commented\n\nI use AOP for logging. On occasion I can get the following exception. If I remove the autoproxy bean, the problem no longer happens. I've appended my aop logging bean setup up after the exception trace.\n\njava.lang.ArrayIndexOutOfBoundsException: -1\nat org.apache.oro.text.regex.Perl5Matcher.__match(Unknown Source)\nat org.apache.oro.text.regex.Perl5Matcher.__match(Unknown Source)\nat org.apache.oro.text.regex.Perl5Matcher.__tryExpression(Unknown Source)\nat org.apache.oro.text.regex.Perl5Matcher.matches(Unknown Source)\nat org.apache.oro.text.regex.Perl5Matcher.matches(Unknown Source)\nat org.springframework.aop.support.RegexpMethodPointcut.matches(RegexpMethodPointcut.java:110)\nat org.springframework.aop.framework.AdvisorChainFactoryUtils.calculateInterceptorsAndDynamicInterceptionAdvice(AdvisorChainFacto\nryUtils.java:60)\nat org.springframework.aop.framework.HashMapCachingAdvisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(HashMapCaching\nAdvisorChainFactory.java:57)\nat org.springframework.aop.framework.JdkDynamicAopProxy.invoke(JdkDynamicAopProxy.java:129)\n\n\\<!--- AOP Logging -->\n\n\\<bean id=\"autoproxy\" class=\"org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator\" \ndependency-check=\"none\"/>\n\n\\<bean id=\"errorAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"\ndependency-check=\"none\">\n\\<property name=\"advice\">\\<ref local=\"logThrowsAdvice\"/>\\</property>\n\\<property name=\"patterns\">\n\\<list>\n\\<value>com.connected.*\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\n\\<bean id=\"beforeAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"\ndependency-check=\"none\">\n\\<property name=\"advice\">\\<ref local=\"logBeforeAdvice\"/>\\</property>\n\\<property name=\"patterns\">\n\\<list>\n\\<value>.*.Store.*storeAsset\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\n\\<bean id=\"afterAdvisor\" class=\"org.springframework.aop.support.RegexpMethodPointcutAdvisor\"\ndependency-check=\"none\">\n\\<property name=\"advice\">\\<ref local=\"logAfterAdvice\"/>\\</property>\n\\<property name=\"patterns\">\n\\<list>\n\\<value>.*.Source.*hasNextAsset\\</value>\n\\</list>\n\\</property>\n\\</bean>\n\n\\<bean id=\"logBeforeAdvice\" class=\"com.connected.aop.LogBeforeAdvice\">\n\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>\n\\</bean>\n\\<bean id=\"logAfterAdvice\" class=\"com.connected.aop.LogAfterAdvice\">\n\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>\n\\</bean>\n\\<bean id=\"logThrowsAdvice\" class=\"com.connected.aop.LogThrowsAdvice\">\n\\<property name=\"logFormatter\">\\<ref local=\"logFormatter\"/>\\</property>\n\\</bean>\n\\<bean id=\"logFormatter\" class=\"com.connected.aop.LogFormatter\"/>\n\\<!-- End of AOP -->\n\n\n---\n\n**Affects:** 1.0 final, 1.0.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453285147","453285149","453285150","453285152"], "labels":["in: core","status: declined"]}