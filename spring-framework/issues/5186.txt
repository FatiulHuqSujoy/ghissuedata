{"id":"5186", "title":"Dependency checking not happening correctly in XmlWebApplicationContext [SPR-457]", "body":"**[Matthew Sgarlata](https://jira.spring.io/secure/ViewProfile.jspa?name=sgarlatm)** opened **[SPR-457](https://jira.spring.io/browse/SPR-457?redirect=false)** and commented

Autowiring is not correctly checking dependencies in my application context in Spring 1.1.1.  I have dependency checking by object and autowire by name turned on at the global \\<beans> level.  Below are examples of things that work and things that don't.  IMHO, all examples should be equivalent in terms of Spring's behavior.  Basically, emailSenderDelegate is dependent on ScoreboardUtil, and the application context isn't filling in this dependency automatically for me.

\\<!--
EXAMPLE 1 (DOESN'T WORK) -
Depending on Spring's autowiring capabilities to detect dependencies
-->

\\<beans
default-dependency-check=\"objects\"
default-autowire=\"byName\">

    <bean
        id=\"emailSenderDelegate\"
        class=\"com.spider.scoreboard.email.SmtpEmailSender\"
        dependency-check=\"none\">
        <property name=\"host\">
            <value>${smtp.host}</value>
        </property>
        <property name=\"port\">
            <value>${smtp.port}</value>
        </property>
        <property name=\"username\">
            <value>${smtp.username}</value>
        </property>
        <property name=\"password\">
            <value>${smtp.password}</value>
        </property>
    </bean>
    
    <bean
        id=\"scoreboardUtil\"
        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>

\\</beans>
\\<!--
EXAMPLE 2 (DOESN'T WORK) -
Explicitly stating that emailSenderDelegate depends on scoreboardUtil
-->

\\<beans
default-dependency-check=\"objects\"
default-autowire=\"byName\">

    <bean
        id=\"emailSenderDelegate\"
        class=\"com.spider.scoreboard.email.SmtpEmailSender\"
        dependency-check=\"none\"
        depends-on=\"scoreboardUtil\">
        <property name=\"host\">
            <value>${smtp.host}</value>
        </property>
        <property name=\"port\">
            <value>${smtp.port}</value>
        </property>
        <property name=\"username\">
            <value>${smtp.username}</value>
        </property>
        <property name=\"password\">
            <value>${smtp.password}</value>
        </property>
    </bean>
    
    <bean
        id=\"scoreboardUtil\"
        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>

\\</beans>
\\<!--
EXAMPLE 3 (DOESN'T WORK) -
Explicitly stating *and* supplying the scoreboardUtil dependency to the bean.
-->

\\<beans
default-dependency-check=\"objects\"
default-autowire=\"byName\">

    <bean
        id=\"emailSenderDelegate\"
        class=\"com.spider.scoreboard.email.SmtpEmailSender\"
        dependency-check=\"none\"
        depends-on=\"scoreboardUtil\">
        <property name=\"host\">
            <value>${smtp.host}</value>
        </property>
        <property name=\"port\">
            <value>${smtp.port}</value>
        </property>
        <property name=\"username\">
            <value>${smtp.username}</value>
        </property>
        <property name=\"password\">
            <value>${smtp.password}</value>
        </property>
        <property name=\"scoreboardUtil\">
            <ref bean=\"scoreboardUtil\"/>
        </property>
    </bean>
    
    <bean
        id=\"scoreboardUtil\"
        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>

\\</beans>
\\<!--
EXAMPLE 4 (DOES WORK) -
Explicitly stating *and* supplying the scoreboardUtil dependency to the bean.  This time though, we move the scoreboardUtil property declaration up higher.  That way, scoreboardUtil is already initialized by the time the host, port, and other properties are set (all of these setters actually make use of the scoreboardUtil property)
-->

\\<beans
default-dependency-check=\"objects\"
default-autowire=\"byName\">

    <bean
        id=\"emailSenderDelegate\"
        class=\"com.spider.scoreboard.email.SmtpEmailSender\"
        dependency-check=\"none\"
        depends-on=\"scoreboardUtil\">
        <property name=\"scoreboardUtil\">
            <ref bean=\"scoreboardUtil\"/>
        </property>
        <property name=\"host\">
            <value>${smtp.host}</value>
        </property>
        <property name=\"port\">
            <value>${smtp.port}</value>
        </property>
        <property name=\"username\">
            <value>${smtp.username}</value>
        </property>
        <property name=\"password\">
            <value>${smtp.password}</value>
        </property>
    </bean>
    
    <bean
        id=\"scoreboardUtil\"
        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>

\\</beans>


---

**Affects:** 1.1.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5186","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5186/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5186/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5186/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5186","id":398052674,"node_id":"MDU6SXNzdWUzOTgwNTI2NzQ=","number":5186,"title":"Dependency checking not happening correctly in XmlWebApplicationContext [SPR-457]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2004-11-13T06:13:24Z","updated_at":"2019-01-12T16:41:59Z","closed_at":"2004-11-14T05:51:53Z","author_association":"COLLABORATOR","body":"**[Matthew Sgarlata](https://jira.spring.io/secure/ViewProfile.jspa?name=sgarlatm)** opened **[SPR-457](https://jira.spring.io/browse/SPR-457?redirect=false)** and commented\n\nAutowiring is not correctly checking dependencies in my application context in Spring 1.1.1.  I have dependency checking by object and autowire by name turned on at the global \\<beans> level.  Below are examples of things that work and things that don't.  IMHO, all examples should be equivalent in terms of Spring's behavior.  Basically, emailSenderDelegate is dependent on ScoreboardUtil, and the application context isn't filling in this dependency automatically for me.\n\n\\<!--\nEXAMPLE 1 (DOESN'T WORK) -\nDepending on Spring's autowiring capabilities to detect dependencies\n-->\n\n\\<beans\ndefault-dependency-check=\"objects\"\ndefault-autowire=\"byName\">\n\n    <bean\n        id=\"emailSenderDelegate\"\n        class=\"com.spider.scoreboard.email.SmtpEmailSender\"\n        dependency-check=\"none\">\n        <property name=\"host\">\n            <value>${smtp.host}</value>\n        </property>\n        <property name=\"port\">\n            <value>${smtp.port}</value>\n        </property>\n        <property name=\"username\">\n            <value>${smtp.username}</value>\n        </property>\n        <property name=\"password\">\n            <value>${smtp.password}</value>\n        </property>\n    </bean>\n    \n    <bean\n        id=\"scoreboardUtil\"\n        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>\n\n\\</beans>\n\\<!--\nEXAMPLE 2 (DOESN'T WORK) -\nExplicitly stating that emailSenderDelegate depends on scoreboardUtil\n-->\n\n\\<beans\ndefault-dependency-check=\"objects\"\ndefault-autowire=\"byName\">\n\n    <bean\n        id=\"emailSenderDelegate\"\n        class=\"com.spider.scoreboard.email.SmtpEmailSender\"\n        dependency-check=\"none\"\n        depends-on=\"scoreboardUtil\">\n        <property name=\"host\">\n            <value>${smtp.host}</value>\n        </property>\n        <property name=\"port\">\n            <value>${smtp.port}</value>\n        </property>\n        <property name=\"username\">\n            <value>${smtp.username}</value>\n        </property>\n        <property name=\"password\">\n            <value>${smtp.password}</value>\n        </property>\n    </bean>\n    \n    <bean\n        id=\"scoreboardUtil\"\n        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>\n\n\\</beans>\n\\<!--\nEXAMPLE 3 (DOESN'T WORK) -\nExplicitly stating *and* supplying the scoreboardUtil dependency to the bean.\n-->\n\n\\<beans\ndefault-dependency-check=\"objects\"\ndefault-autowire=\"byName\">\n\n    <bean\n        id=\"emailSenderDelegate\"\n        class=\"com.spider.scoreboard.email.SmtpEmailSender\"\n        dependency-check=\"none\"\n        depends-on=\"scoreboardUtil\">\n        <property name=\"host\">\n            <value>${smtp.host}</value>\n        </property>\n        <property name=\"port\">\n            <value>${smtp.port}</value>\n        </property>\n        <property name=\"username\">\n            <value>${smtp.username}</value>\n        </property>\n        <property name=\"password\">\n            <value>${smtp.password}</value>\n        </property>\n        <property name=\"scoreboardUtil\">\n            <ref bean=\"scoreboardUtil\"/>\n        </property>\n    </bean>\n    \n    <bean\n        id=\"scoreboardUtil\"\n        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>\n\n\\</beans>\n\\<!--\nEXAMPLE 4 (DOES WORK) -\nExplicitly stating *and* supplying the scoreboardUtil dependency to the bean.  This time though, we move the scoreboardUtil property declaration up higher.  That way, scoreboardUtil is already initialized by the time the host, port, and other properties are set (all of these setters actually make use of the scoreboardUtil property)\n-->\n\n\\<beans\ndefault-dependency-check=\"objects\"\ndefault-autowire=\"byName\">\n\n    <bean\n        id=\"emailSenderDelegate\"\n        class=\"com.spider.scoreboard.email.SmtpEmailSender\"\n        dependency-check=\"none\"\n        depends-on=\"scoreboardUtil\">\n        <property name=\"scoreboardUtil\">\n            <ref bean=\"scoreboardUtil\"/>\n        </property>\n        <property name=\"host\">\n            <value>${smtp.host}</value>\n        </property>\n        <property name=\"port\">\n            <value>${smtp.port}</value>\n        </property>\n        <property name=\"username\">\n            <value>${smtp.username}</value>\n        </property>\n        <property name=\"password\">\n            <value>${smtp.password}</value>\n        </property>\n    </bean>\n    \n    <bean\n        id=\"scoreboardUtil\"\n        class=\"com.spider.scoreboard.util.ScoreboardUtil\"/>\n\n\\</beans>\n\n\n---\n\n**Affects:** 1.1.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453289293","453289295","453289298","453289299"], "labels":["in: core","status: declined"]}