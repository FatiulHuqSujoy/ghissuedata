{"id":"19612", "title":"Allow controller parameter annotations (@RequestBody, @PathVariable...) to be defined on interfaces or parent classes [SPR-15046]", "body":"**[Kiril Karaatanassov](https://jira.spring.io/secure/ViewProfile.jspa?name=karaatanasov)** opened **[SPR-15046](https://jira.spring.io/browse/SPR-15046?redirect=false)** and commented

We are trying to use external REST API definition that would generate interfaces one implements in Java. For the time being we try to use Swagger.

Unfortunately Spring does not allow us to separate the API definition from implementation as the parameter annotations on controllers are not read from interfaces. Instead of that Spring always looks into the concrete implementation whose purpose is to implement a contract not define it. Weirdly enough the `@RequestMapping` annotation can be declared on the interface and is picked up by the implementing controller. It would be great if similar functionality is provided for `@RequestBody`, `@PathVariable`, `@RequestHeader` etc. that are defined on the individual parameters.

It seems that a potential fix has to update/extend the implementation of

`Annotation[] org.springframework.core.MethodParameter.getParameterAnnotations()`

this is called by

`MethodParameter[] org.springframework.web.method.HandlerMethod.initMethodParameters()`

called by
`org.springframework.web.method.HandlerMethod` constructor which in turn is called by

`org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping.createHandlerMethod(Object, Method)`.

There is a sample but hacky workaround found on StackOverflow. See

http://stackoverflow.com/questions/8002514/spring-mvc-annotated-controller-interface/8005644#8005644

It does require some creativity to put in practical use with Spring Boot i.e. `WebMvcRegistrations`

So the basic ask is to pick up parameter annotations for controller from implemented interfaces e.g.

```
public interface MyControllerInterface {

    @RequestMapping(path=\"/test\", method=RequestMethod.POST)
    public ResponseEntity<String> echo(@RequestBody String input);

}

@RestController
public class MyControllerImpl implements MyControllerInterface {
    @Override
    public ResponseEntity<String> echo(String input) {
        return new ResponseEntity<>(input, HttpStatus.OK);
    }
}
```

It may make sense to to use `@RestController` or alike annotation on the interfaces whose methods will be checked.

PS I filed this to Spring Boot and was redirected here https://github.com/spring-projects/spring-boot/issues/7730

---

**Affects:** 4.3 GA

**Issue Links:**
- #15682 Enable REST controller method parameter annotations on an interface (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19612","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19612/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19612/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19612/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19612","id":398202612,"node_id":"MDU6SXNzdWUzOTgyMDI2MTI=","number":19612,"title":"Allow controller parameter annotations (@RequestBody, @PathVariable...) to be defined on interfaces or parent classes [SPR-15046]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2016-12-22T23:30:20Z","updated_at":"2019-01-11T15:16:39Z","closed_at":"2016-12-23T07:24:39Z","author_association":"COLLABORATOR","body":"**[Kiril Karaatanassov](https://jira.spring.io/secure/ViewProfile.jspa?name=karaatanasov)** opened **[SPR-15046](https://jira.spring.io/browse/SPR-15046?redirect=false)** and commented\n\nWe are trying to use external REST API definition that would generate interfaces one implements in Java. For the time being we try to use Swagger.\n\nUnfortunately Spring does not allow us to separate the API definition from implementation as the parameter annotations on controllers are not read from interfaces. Instead of that Spring always looks into the concrete implementation whose purpose is to implement a contract not define it. Weirdly enough the `@RequestMapping` annotation can be declared on the interface and is picked up by the implementing controller. It would be great if similar functionality is provided for `@RequestBody`, `@PathVariable`, `@RequestHeader` etc. that are defined on the individual parameters.\n\nIt seems that a potential fix has to update/extend the implementation of\n\n`Annotation[] org.springframework.core.MethodParameter.getParameterAnnotations()`\n\nthis is called by\n\n`MethodParameter[] org.springframework.web.method.HandlerMethod.initMethodParameters()`\n\ncalled by\n`org.springframework.web.method.HandlerMethod` constructor which in turn is called by\n\n`org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping.createHandlerMethod(Object, Method)`.\n\nThere is a sample but hacky workaround found on StackOverflow. See\n\nhttp://stackoverflow.com/questions/8002514/spring-mvc-annotated-controller-interface/8005644#8005644\n\nIt does require some creativity to put in practical use with Spring Boot i.e. `WebMvcRegistrations`\n\nSo the basic ask is to pick up parameter annotations for controller from implemented interfaces e.g.\n\n```\npublic interface MyControllerInterface {\n\n    @RequestMapping(path=\"/test\", method=RequestMethod.POST)\n    public ResponseEntity<String> echo(@RequestBody String input);\n\n}\n\n@RestController\npublic class MyControllerImpl implements MyControllerInterface {\n    @Override\n    public ResponseEntity<String> echo(String input) {\n        return new ResponseEntity<>(input, HttpStatus.OK);\n    }\n}\n```\n\nIt may make sense to to use `@RestController` or alike annotation on the interfaces whose methods will be checked.\n\nPS I filed this to Spring Boot and was redirected here https://github.com/spring-projects/spring-boot/issues/7730\n\n---\n\n**Affects:** 4.3 GA\n\n**Issue Links:**\n- #15682 Enable REST controller method parameter annotations on an interface (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453449320"], "labels":["in: web","status: duplicate","type: enhancement"]}