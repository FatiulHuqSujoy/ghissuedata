{"id":"13573", "title":"Caching Abstraction ignores method name? [SPR-8933]", "body":"**[Alexander Derenbach](https://jira.spring.io/secure/ViewProfile.jspa?name=derenbacha)** opened **[SPR-8933](https://jira.spring.io/browse/SPR-8933?redirect=false)** and commented

When I read the reference documentation I thought the caching abstraction is on method level, but it seems it is only on the parameter value of the methods.

If I have several methods with the same parameter type it get always the same return value:

`@Component`
public class CacheTestImpl implements CacheTest {
`@Cacheable`(\"databaseCache\")
public Long test1() {
return 1L;
}

    @Cacheable(\"databaseCache\")
    public Long test2() {
        return 2L;
    }
    
    @Cacheable(\"databaseCache\")
    public Long test3() {
        return 3L;
    }
    
       
    @Cacheable(\"databaseCache\")
    public String test4() {
        return \"4\";
    }

}

Calling now:

    System.out.println(test.test1());
    System.out.println(test.test2());
    System.out.println(test.test3());
    System.out.println(test.test4());

results in:

1
1
1
ClassCastException: java.lang.Long cannot be cast to java.lang.String

Is this the desired behaviour? I would expect:

1
2
3
4

If I use different caches it works.

I can't access github from my place (firewall) so I have added a tar with a small maven project showing this problem.

Greets Alex

---

**Affects:** 3.1 GA

**Attachments:**
- [cacheproblem.tar](https://jira.spring.io/secure/attachment/19224/cacheproblem.tar) (_70.00 kB_)
- [MethodAwareCacheKeyGenerator.java](https://jira.spring.io/secure/attachment/20892/MethodAwareCacheKeyGenerator.java) (_455 bytes_)

**Issue Links:**
- #16358 Cacheable javadoc wrongly explains how cache keys are generated

5 votes, 9 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13573","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13573/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13573/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13573/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13573","id":398116092,"node_id":"MDU6SXNzdWUzOTgxMTYwOTI=","number":13573,"title":"Caching Abstraction ignores method name? [SPR-8933]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":null,"comments":11,"created_at":"2011-12-15T08:48:21Z","updated_at":"2019-01-12T16:37:07Z","closed_at":"2014-07-01T08:54:03Z","author_association":"COLLABORATOR","body":"**[Alexander Derenbach](https://jira.spring.io/secure/ViewProfile.jspa?name=derenbacha)** opened **[SPR-8933](https://jira.spring.io/browse/SPR-8933?redirect=false)** and commented\n\nWhen I read the reference documentation I thought the caching abstraction is on method level, but it seems it is only on the parameter value of the methods.\n\nIf I have several methods with the same parameter type it get always the same return value:\n\n`@Component`\npublic class CacheTestImpl implements CacheTest {\n`@Cacheable`(\"databaseCache\")\npublic Long test1() {\nreturn 1L;\n}\n\n    @Cacheable(\"databaseCache\")\n    public Long test2() {\n        return 2L;\n    }\n    \n    @Cacheable(\"databaseCache\")\n    public Long test3() {\n        return 3L;\n    }\n    \n       \n    @Cacheable(\"databaseCache\")\n    public String test4() {\n        return \"4\";\n    }\n\n}\n\nCalling now:\n\n    System.out.println(test.test1());\n    System.out.println(test.test2());\n    System.out.println(test.test3());\n    System.out.println(test.test4());\n\nresults in:\n\n1\n1\n1\nClassCastException: java.lang.Long cannot be cast to java.lang.String\n\nIs this the desired behaviour? I would expect:\n\n1\n2\n3\n4\n\nIf I use different caches it works.\n\nI can't access github from my place (firewall) so I have added a tar with a small maven project showing this problem.\n\nGreets Alex\n\n---\n\n**Affects:** 3.1 GA\n\n**Attachments:**\n- [cacheproblem.tar](https://jira.spring.io/secure/attachment/19224/cacheproblem.tar) (_70.00 kB_)\n- [MethodAwareCacheKeyGenerator.java](https://jira.spring.io/secure/attachment/20892/MethodAwareCacheKeyGenerator.java) (_455 bytes_)\n\n**Issue Links:**\n- #16358 Cacheable javadoc wrongly explains how cache keys are generated\n\n5 votes, 9 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453364017","453364018","453364019","453364020","453364021","453364023","453364025","453364026","453364028","453364029","453364030"], "labels":["in: core","status: declined"]}