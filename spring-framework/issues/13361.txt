{"id":"13361", "title":"@ComponentScan(includeFilters=@Filter(...)) fails when @Import'ed [SPR-8719]", "body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-8719](https://jira.spring.io/browse/SPR-8719?redirect=false)** and commented

`@ComponentScan` does not work in `@Import-ed` configuration.

```
public class Coin { }

public class Main {

    @Configuration
    @ComponentScan(basePackageClasses = Main.class,
            useDefaultFilters = false,
            includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Coin.class))
    public static class Conf1 {
    }

    @Configuration
    @Import(Conf1.class)
    public static class Conf2 {

    }

    public static void main(String[] args) {
        System.out.println(\"loading Conf1\"); // works fine
        new AnnotationConfigApplicationContext(Conf1.class).getBean(Coin.class);

        System.out.println(\"loading Conf2\"); // cannot find a bean
        new AnnotationConfigApplicationContext(Conf2.class).getBean(Coin.class);
        System.out.println(\"Unreachable in Spring 3.1.0.M2\");
    }

}
```

Outputs:

```
loading Conf1
loading Conf2
Exception in thread \"main\" org.springframework.beans.factory.NoSuchBeanDefinitionException: No unique bean of type [ru.yandex.commune.junk.stepancheg.spring.shinderuk.Coin] is defined: expected single bean but found 0: 
	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBean(DefaultListableBeanFactory.java:269)
	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1101)
	at ru.yandex.commune.junk.stepancheg.spring.shinderuk.Main.main(Main.java:31)
```

Probably related to #12956.

---

**Affects:** 3.1 M2

**Issue Links:**
- #13670 `@ComponentScan` with includeFilters on `@Import-ed` context does not work (_**\"duplicates\"**_)
- #12634 `@ComponentScan` does not work when referenced from XML config (_**\"is duplicated by\"**_)
- #13738 ClassPathBeanDefinitionScanner vs ClassPathBeanDefinitionScanner: difference in behavior when dealing with `@ComponentScan` excludeFilters

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/d9f7fdd120409fff4491561215e5b2dda74e2b02

2 votes, 0 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13361","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13361/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13361/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13361/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13361","id":398114714,"node_id":"MDU6SXNzdWUzOTgxMTQ3MTQ=","number":13361,"title":"@ComponentScan(includeFilters=@Filter(...)) fails when @Import'ed [SPR-8719]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2011-09-23T12:06:14Z","updated_at":"2019-01-13T07:07:45Z","closed_at":"2012-02-09T04:47:13Z","author_association":"COLLABORATOR","body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-8719](https://jira.spring.io/browse/SPR-8719?redirect=false)** and commented\n\n`@ComponentScan` does not work in `@Import-ed` configuration.\n\n```\npublic class Coin { }\n\npublic class Main {\n\n    @Configuration\n    @ComponentScan(basePackageClasses = Main.class,\n            useDefaultFilters = false,\n            includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Coin.class))\n    public static class Conf1 {\n    }\n\n    @Configuration\n    @Import(Conf1.class)\n    public static class Conf2 {\n\n    }\n\n    public static void main(String[] args) {\n        System.out.println(\"loading Conf1\"); // works fine\n        new AnnotationConfigApplicationContext(Conf1.class).getBean(Coin.class);\n\n        System.out.println(\"loading Conf2\"); // cannot find a bean\n        new AnnotationConfigApplicationContext(Conf2.class).getBean(Coin.class);\n        System.out.println(\"Unreachable in Spring 3.1.0.M2\");\n    }\n\n}\n```\n\nOutputs:\n\n```\nloading Conf1\nloading Conf2\nException in thread \"main\" org.springframework.beans.factory.NoSuchBeanDefinitionException: No unique bean of type [ru.yandex.commune.junk.stepancheg.spring.shinderuk.Coin] is defined: expected single bean but found 0: \n\tat org.springframework.beans.factory.support.DefaultListableBeanFactory.getBean(DefaultListableBeanFactory.java:269)\n\tat org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1101)\n\tat ru.yandex.commune.junk.stepancheg.spring.shinderuk.Main.main(Main.java:31)\n```\n\nProbably related to #12956.\n\n---\n\n**Affects:** 3.1 M2\n\n**Issue Links:**\n- #13670 `@ComponentScan` with includeFilters on `@Import-ed` context does not work (_**\"duplicates\"**_)\n- #12634 `@ComponentScan` does not work when referenced from XML config (_**\"is duplicated by\"**_)\n- #13738 ClassPathBeanDefinitionScanner vs ClassPathBeanDefinitionScanner: difference in behavior when dealing with `@ComponentScan` excludeFilters\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/d9f7fdd120409fff4491561215e5b2dda74e2b02\n\n2 votes, 0 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453362361","453362363","453362365"], "labels":["in: core","status: duplicate"]}