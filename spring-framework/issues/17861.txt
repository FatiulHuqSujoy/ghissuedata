{"id":"17861", "title":"Multiple requests with SockJS provides 404 error [SPR-13270]", "body":"**[Cecchinato Bastien](https://jira.spring.io/secure/ViewProfile.jspa?name=zebasto)** opened **[SPR-13270](https://jira.spring.io/browse/SPR-13270?redirect=false)** and commented

Hi there !

I'm using SockJS with ember-cli (using the following module https://github.com/kenobifoundation/ember-cli-sockjs) and I'm getting a bug on IE10 when sending messages throw the \"simulated\" websocket.

My JS code is :

```js
_onSocketOpen: function() {
    Ember.Logger.info(\"Socket vers le serveur ouverte\");
    if (this.get('hasContactAccess')) {
      this._subscribeToTopic('contact-request/count');
    }

    if (this.get('hasFundAppAccess')) {
      this._subscribeToTopic('funding-application/count');
    }
  },

  _subscribeToTopic: function(topicName) {
    Ember.Logger.info(\"Sourscription au topic \" + topicName);
    this.get('socket').send('SUBSCRIBE:' + topicName + ':' + this.get('token'));
  },
```

I'm not using STOMP as messaging protocol (but it is much alike as we do). When the first subscription is made, then the response is 204. But on the second subscription, the servers responds 404 no matter the request.

My config is the following :

```java
@Configuration
@EnableWebSocket
@EnableScheduling
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry
                .addHandler(delegateSocketHandler(), \"/socket\", \"/socket/**\")
                .setAllowedOrigins(\"*\")
                .withSockJS();
    }

    @Bean
    public WebSocketHandler delegateSocketHandler() {
        return new DelegateSocketHandler();
    }
}
```

And the DelegateSocketHandler is :

```java

@Slf4j
public class DelegateSocketHandler extends TextWebSocketHandler {

    /**
     * Pattern définissant la méthode avec laquelle on va souscrire ou désouscrire à un \"topic\"
     * La commande doit prendre la forme suivante :
     * <ul>
     *     <li>SUBSCRIBE:topic-name/type:token-oauth</li>
     *     <li>UNSUBSCRIBE:topic-name/type:token-oauth</li>
     * </ul>
     */
    private static final Pattern COMMAND = Pattern.compile(\"(SUBSCRIBE|UNSUBSCRIBE):([a-z-]+/[a-z-]+):([0-9a-f-]+)\");

    @Inject
    private List<AbstractSocketHandler> socketHandlers;

    @Inject
    private ObjectMapper objectMapper;

    @Inject
    private TokenStore tokenStore;

    /**
     * Cette map permet de stocker la liste des indicateurs sur lequels une websocket à souscrit
     */
    private Map<WebSocketSession, Set<String>> webSocketSessionMap = new ConcurrentHashMap<>();

    /**
     * Implémentation du handler de requête entrante sur la WebSocket ouverte par le client
     *
     * @param session
     *            La session WebSocket créée par Spring
     * @param message
     *            Le message entrant qui doit être de la forme SUBSCRIBE:topic-name/type:token-oauth
     */
    @Override
    public final void handleTextMessage(WebSocketSession session, TextMessage message) {
        log.debug(\"Réception d'une requête WebSocket sur la session {} et le message {}\", session, message.getPayload());
        Matcher matcher = COMMAND.matcher(message.getPayload());
        Assert.isTrue(matcher.matches());

        SocketAction action = SocketAction.valueOf(matcher.group(1));
        String topicName = matcher.group(2);
        String token = matcher.group(3);

        OAuth2Authentication authentication = tokenStore.readAuthentication(token);

        Assert.notNull(authentication);
        Assert.isTrue(socketHandlers.stream().anyMatch(handler -> handler.getTopicName().equals(topicName)));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        if (session.getPrincipal() == null) {
            injectPrincipal(session, authentication);
        }
        handleRequest(session, action, topicName);
    }

    // Some code here but not relevant for my problem.
}
```

I don't get why the second call gets a 404. Is it normal because of a specific design when the fallback is used ? I've attached the IE10 network capture so you can have a look.

Thanks in advance.

Regards,


---

**Affects:** 4.1.6

**Attachments:**
- [NetworkData.xml](https://jira.spring.io/secure/attachment/22957/NetworkData.xml) (_3.11 MB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17861","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17861/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17861/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17861/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17861","id":398182095,"node_id":"MDU6SXNzdWUzOTgxODIwOTU=","number":17861,"title":"Multiple requests with SockJS provides 404 error [SPR-13270]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":6,"created_at":"2015-07-24T01:52:48Z","updated_at":"2019-01-11T18:02:27Z","closed_at":"2015-08-21T07:54:10Z","author_association":"COLLABORATOR","body":"**[Cecchinato Bastien](https://jira.spring.io/secure/ViewProfile.jspa?name=zebasto)** opened **[SPR-13270](https://jira.spring.io/browse/SPR-13270?redirect=false)** and commented\n\nHi there !\n\nI'm using SockJS with ember-cli (using the following module https://github.com/kenobifoundation/ember-cli-sockjs) and I'm getting a bug on IE10 when sending messages throw the \"simulated\" websocket.\n\nMy JS code is :\n\n```js\n_onSocketOpen: function() {\n    Ember.Logger.info(\"Socket vers le serveur ouverte\");\n    if (this.get('hasContactAccess')) {\n      this._subscribeToTopic('contact-request/count');\n    }\n\n    if (this.get('hasFundAppAccess')) {\n      this._subscribeToTopic('funding-application/count');\n    }\n  },\n\n  _subscribeToTopic: function(topicName) {\n    Ember.Logger.info(\"Sourscription au topic \" + topicName);\n    this.get('socket').send('SUBSCRIBE:' + topicName + ':' + this.get('token'));\n  },\n```\n\nI'm not using STOMP as messaging protocol (but it is much alike as we do). When the first subscription is made, then the response is 204. But on the second subscription, the servers responds 404 no matter the request.\n\nMy config is the following :\n\n```java\n@Configuration\n@EnableWebSocket\n@EnableScheduling\npublic class WebSocketConfig implements WebSocketConfigurer {\n    @Override\n    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {\n        registry\n                .addHandler(delegateSocketHandler(), \"/socket\", \"/socket/**\")\n                .setAllowedOrigins(\"*\")\n                .withSockJS();\n    }\n\n    @Bean\n    public WebSocketHandler delegateSocketHandler() {\n        return new DelegateSocketHandler();\n    }\n}\n```\n\nAnd the DelegateSocketHandler is :\n\n```java\n\n@Slf4j\npublic class DelegateSocketHandler extends TextWebSocketHandler {\n\n    /**\n     * Pattern définissant la méthode avec laquelle on va souscrire ou désouscrire à un \"topic\"\n     * La commande doit prendre la forme suivante :\n     * <ul>\n     *     <li>SUBSCRIBE:topic-name/type:token-oauth</li>\n     *     <li>UNSUBSCRIBE:topic-name/type:token-oauth</li>\n     * </ul>\n     */\n    private static final Pattern COMMAND = Pattern.compile(\"(SUBSCRIBE|UNSUBSCRIBE):([a-z-]+/[a-z-]+):([0-9a-f-]+)\");\n\n    @Inject\n    private List<AbstractSocketHandler> socketHandlers;\n\n    @Inject\n    private ObjectMapper objectMapper;\n\n    @Inject\n    private TokenStore tokenStore;\n\n    /**\n     * Cette map permet de stocker la liste des indicateurs sur lequels une websocket à souscrit\n     */\n    private Map<WebSocketSession, Set<String>> webSocketSessionMap = new ConcurrentHashMap<>();\n\n    /**\n     * Implémentation du handler de requête entrante sur la WebSocket ouverte par le client\n     *\n     * @param session\n     *            La session WebSocket créée par Spring\n     * @param message\n     *            Le message entrant qui doit être de la forme SUBSCRIBE:topic-name/type:token-oauth\n     */\n    @Override\n    public final void handleTextMessage(WebSocketSession session, TextMessage message) {\n        log.debug(\"Réception d'une requête WebSocket sur la session {} et le message {}\", session, message.getPayload());\n        Matcher matcher = COMMAND.matcher(message.getPayload());\n        Assert.isTrue(matcher.matches());\n\n        SocketAction action = SocketAction.valueOf(matcher.group(1));\n        String topicName = matcher.group(2);\n        String token = matcher.group(3);\n\n        OAuth2Authentication authentication = tokenStore.readAuthentication(token);\n\n        Assert.notNull(authentication);\n        Assert.isTrue(socketHandlers.stream().anyMatch(handler -> handler.getTopicName().equals(topicName)));\n\n        SecurityContextHolder.getContext().setAuthentication(authentication);\n        if (session.getPrincipal() == null) {\n            injectPrincipal(session, authentication);\n        }\n        handleRequest(session, action, topicName);\n    }\n\n    // Some code here but not relevant for my problem.\n}\n```\n\nI don't get why the second call gets a 404. Is it normal because of a specific design when the fallback is used ? I've attached the IE10 network capture so you can have a look.\n\nThanks in advance.\n\nRegards,\n\n\n---\n\n**Affects:** 4.1.6\n\n**Attachments:**\n- [NetworkData.xml](https://jira.spring.io/secure/attachment/22957/NetworkData.xml) (_3.11 MB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453428163","453428164","453428166","453428168","453428169","453428170"], "labels":["in: web"]}