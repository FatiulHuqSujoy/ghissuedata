{"id":"17438", "title":"HttpSessionHandshakeInterceptor's getSession(request) [SPR-12840]", "body":"**[Keesun Baik](https://jira.spring.io/secure/ViewProfile.jspa?name=keesun)** opened **[SPR-12840](https://jira.spring.io/browse/SPR-12840?redirect=false)** and commented

On the [websocket document](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html#websocket-server-handshake), it describes about **HttpSessionHandshakeInterceptor** and I've used it but I didn't get the session's id for the request. So, I've debugged it for a while and I got the reason why I didn't get it.

```
private HttpSession getSession(ServerHttpRequest request) {
	if (request instanceof ServletServerHttpRequest) {
		ServletServerHttpRequest serverRequest = (ServletServerHttpRequest) request;
		return serverRequest.getServletRequest().getSession(false);
	}
	return null;
}
```

As you can see, the **getSession(false)** is used to get the HttpSession from ServletServerHttpRequest, and (as you may know) it returns session object or null if there's no current session. That's why I can't see the session id when I tried to connect websocket without using getSession() before.

like below:

```
@RequestMapping(\"/\")
public String lounge() {
    return \"/lounge/index.html\";
}
```

I didn't use getSession() explicitly or implicitly. The controller is just serving a static HTML file like this:

```
<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <title>lounge</title>
</head>
<body>
<p>Hi there,</p>
<p>
    <a href=\"javascript:entrance()\">Entrance</a>
</p>

<script src=\"https://d1fxtkz8shb9d2.cloudfront.net/sockjs-0.3.js\"></script>
<script type=\"application/javascript\">
function entrance() {
    var sock = new SockJS('http://localhost:8080/echo');
    sock.onopen = function() {
        console.log('open');
        sock.send('message');
    };
    sock.onmessage = function(e) {
        console.log('message', e);
    };
    sock.onclose = function() {
        console.log('close');
    };
}
</script>
</body>
</html>
```

In this case, **HttpSessionHandshakeInterceptor** can't provide the session id, even though I config like this.

```
@Override
public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    registry.addHandler(echoHandler(), \"/echo\")
            .addInterceptors(new HttpSessionHandshakeInterceptor())
            .withSockJS();
}
```

It was a little bit long to describe, I just create this issue to know why you made decision to use **getSession(false)** and I want to suggest how about using **getSession()** or providing an option that the developers can use it for a flag to decide to use getSession(true) or getSession(false).

And, If you agree with me and If you don't mind, I want to contribute with this issue using pull-request on the Github.

Thanks for reading.


---

**Affects:** 4.1.5

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/73d6d30951cb145bbd6b2e2fcdc0388019b15ae9
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17438","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17438/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17438/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17438/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17438","id":398177907,"node_id":"MDU6SXNzdWUzOTgxNzc5MDc=","number":17438,"title":"HttpSessionHandshakeInterceptor's getSession(request) [SPR-12840]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/134","html_url":"https://github.com/spring-projects/spring-framework/milestone/134","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/134/labels","id":3960907,"node_id":"MDk6TWlsZXN0b25lMzk2MDkwNw==","number":134,"title":"4.2 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":235,"state":"closed","created_at":"2019-01-10T22:04:32Z","updated_at":"2019-01-11T08:26:53Z","due_on":"2015-05-25T07:00:00Z","closed_at":"2019-01-10T22:04:32Z"},"comments":4,"created_at":"2015-03-20T21:52:20Z","updated_at":"2019-01-14T05:13:06Z","closed_at":"2015-05-26T01:15:07Z","author_association":"COLLABORATOR","body":"**[Keesun Baik](https://jira.spring.io/secure/ViewProfile.jspa?name=keesun)** opened **[SPR-12840](https://jira.spring.io/browse/SPR-12840?redirect=false)** and commented\n\nOn the [websocket document](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html#websocket-server-handshake), it describes about **HttpSessionHandshakeInterceptor** and I've used it but I didn't get the session's id for the request. So, I've debugged it for a while and I got the reason why I didn't get it.\n\n```\nprivate HttpSession getSession(ServerHttpRequest request) {\n\tif (request instanceof ServletServerHttpRequest) {\n\t\tServletServerHttpRequest serverRequest = (ServletServerHttpRequest) request;\n\t\treturn serverRequest.getServletRequest().getSession(false);\n\t}\n\treturn null;\n}\n```\n\nAs you can see, the **getSession(false)** is used to get the HttpSession from ServletServerHttpRequest, and (as you may know) it returns session object or null if there's no current session. That's why I can't see the session id when I tried to connect websocket without using getSession() before.\n\nlike below:\n\n```\n@RequestMapping(\"/\")\npublic String lounge() {\n    return \"/lounge/index.html\";\n}\n```\n\nI didn't use getSession() explicitly or implicitly. The controller is just serving a static HTML file like this:\n\n```\n<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n    <meta charset=\"UTF-8\">\n    <title>lounge</title>\n</head>\n<body>\n<p>Hi there,</p>\n<p>\n    <a href=\"javascript:entrance()\">Entrance</a>\n</p>\n\n<script src=\"https://d1fxtkz8shb9d2.cloudfront.net/sockjs-0.3.js\"></script>\n<script type=\"application/javascript\">\nfunction entrance() {\n    var sock = new SockJS('http://localhost:8080/echo');\n    sock.onopen = function() {\n        console.log('open');\n        sock.send('message');\n    };\n    sock.onmessage = function(e) {\n        console.log('message', e);\n    };\n    sock.onclose = function() {\n        console.log('close');\n    };\n}\n</script>\n</body>\n</html>\n```\n\nIn this case, **HttpSessionHandshakeInterceptor** can't provide the session id, even though I config like this.\n\n```\n@Override\npublic void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {\n    registry.addHandler(echoHandler(), \"/echo\")\n            .addInterceptors(new HttpSessionHandshakeInterceptor())\n            .withSockJS();\n}\n```\n\nIt was a little bit long to describe, I just create this issue to know why you made decision to use **getSession(false)** and I want to suggest how about using **getSession()** or providing an option that the developers can use it for a flag to decide to use getSession(true) or getSession(false).\n\nAnd, If you agree with me and If you don't mind, I want to contribute with this issue using pull-request on the Github.\n\nThanks for reading.\n\n\n---\n\n**Affects:** 4.1.5\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/73d6d30951cb145bbd6b2e2fcdc0388019b15ae9\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453423855","453423858","453423861","453423863"], "labels":["in: web","type: enhancement"]}