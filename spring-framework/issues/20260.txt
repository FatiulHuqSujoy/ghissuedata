{"id":"20260", "title":"WebClient fails to transform empty json array to emtpy flux [SPR-15703]", "body":"**[Marvin Schramm](https://jira.spring.io/secure/ViewProfile.jspa?name=marvinschramm)** opened **[SPR-15703](https://jira.spring.io/browse/SPR-15703?redirect=false)** and commented

The WebClient to consume a json api, the client seem to fail on transforming empty json array response to an empty flux.

Error Case PR https://github.com/spring-projects/spring-framework-issues/pull/159
Simple code example for demonstration. I will create an issue repository entry for this.

```java
@RestController
public class TestController {


    @RequestMapping(\"/fail\")
    public Flux<TestDTO> fail() {
        return WebClient.builder().baseUrl(\"http://localhost:8080\")
                .build().get().uri(\"/empty\")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flux()
                .flatMap(clientResponse -> clientResponse.bodyToFlux(TestDTO.class));
    }

    @RequestMapping(\"/success\")
    public Flux<TestDTO> sucess() {
        return WebClient.builder().baseUrl(\"http://localhost:8080\")
                .build().get().uri(\"/single\")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flux()
                .flatMap(clientResponse -> clientResponse.bodyToFlux(TestDTO.class));
    }

    @RequestMapping(\"/empty\")
    public List<TestDTO> emptyArray() {
        return Collections.emptyList();
    }

    @RequestMapping(\"/single\")
    public List<TestDTO> singleItem() {
        return Collections.singletonList(new TestDTO(\"1\"));
    }

    public static class TestDTO {
        private String id;

        @JsonCreator
        public TestDTO(@JsonProperty(\"id\") String id) {
            this.id = id;
        }

        @JsonProperty(\"id\")
        public String getId() {
            return id;
        }
    }
}

```

---

**Issue Links:**
- #20238 WebClient's handling of empty bodies seems broken (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20260","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20260/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20260/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20260/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20260","id":398211565,"node_id":"MDU6SXNzdWUzOTgyMTE1NjU=","number":20260,"title":"WebClient fails to transform empty json array to emtpy flux [SPR-15703]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-06-26T08:54:45Z","updated_at":"2019-01-12T16:45:07Z","closed_at":"2017-06-26T09:31:23Z","author_association":"COLLABORATOR","body":"**[Marvin Schramm](https://jira.spring.io/secure/ViewProfile.jspa?name=marvinschramm)** opened **[SPR-15703](https://jira.spring.io/browse/SPR-15703?redirect=false)** and commented\n\nThe WebClient to consume a json api, the client seem to fail on transforming empty json array response to an empty flux.\n\nError Case PR https://github.com/spring-projects/spring-framework-issues/pull/159\nSimple code example for demonstration. I will create an issue repository entry for this.\n\n```java\n@RestController\npublic class TestController {\n\n\n    @RequestMapping(\"/fail\")\n    public Flux<TestDTO> fail() {\n        return WebClient.builder().baseUrl(\"http://localhost:8080\")\n                .build().get().uri(\"/empty\")\n                .accept(MediaType.APPLICATION_JSON)\n                .exchange()\n                .flux()\n                .flatMap(clientResponse -> clientResponse.bodyToFlux(TestDTO.class));\n    }\n\n    @RequestMapping(\"/success\")\n    public Flux<TestDTO> sucess() {\n        return WebClient.builder().baseUrl(\"http://localhost:8080\")\n                .build().get().uri(\"/single\")\n                .accept(MediaType.APPLICATION_JSON)\n                .exchange()\n                .flux()\n                .flatMap(clientResponse -> clientResponse.bodyToFlux(TestDTO.class));\n    }\n\n    @RequestMapping(\"/empty\")\n    public List<TestDTO> emptyArray() {\n        return Collections.emptyList();\n    }\n\n    @RequestMapping(\"/single\")\n    public List<TestDTO> singleItem() {\n        return Collections.singletonList(new TestDTO(\"1\"));\n    }\n\n    public static class TestDTO {\n        private String id;\n\n        @JsonCreator\n        public TestDTO(@JsonProperty(\"id\") String id) {\n            this.id = id;\n        }\n\n        @JsonProperty(\"id\")\n        public String getId() {\n            return id;\n        }\n    }\n}\n\n```\n\n---\n\n**Issue Links:**\n- #20238 WebClient's handling of empty bodies seems broken (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453457456","453457458","453457460"], "labels":["in: web","status: duplicate"]}