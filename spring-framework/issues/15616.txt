{"id":"15616", "title":"JavaConfig Bean overriding with addition [SPR-10988]", "body":"**[Benoit Lacelle](https://jira.spring.io/secure/ViewProfile.jspa?name=blasd)** opened **[SPR-10988](https://jira.spring.io/browse/SPR-10988?redirect=false)** and commented

I consider injection of beans as a List of automatically detected beans: I introduce several beans implementing the same interface and inject all of them as a List in a later bean.

I've not been able to find official documentation related to this feature. My single source is http://www.coderanch.com/t/605509/Spring/Java-config-autowired-List

Consider this feature, I have an issue with Bean overring: I would like to override a bean defined throught a no-arg method with a bean defined with the List of detected beans. However, spring behave like the second bean definition does not exist.

It can be reproduced with the following test:

```
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class SpringTest {

	@Test
	public void shouldtestSpringDifferentMethodNames() {
		AnnotationConfigApplicationContext ctx2 = new AnnotationConfigApplicationContext(AConfig.class, CConfig.class);
		Assert.assertEquals(\"overriden\", ctx2.getBean(\"bean\"));
	}

	@Configuration
	public static class AConfig {

		@Bean
		public Object bean() {
			return \"not overriden\";
		}

	}

	@Configuration
	public static class CConfig extends AConfig {

		@Bean
		public Date anotherBean() {
			return new Date();
		}

		@Bean
		public Object bean(List<? extends Date> someDate) {
			return \"overriden\";
		}

	}

}
```

If this is an expected behavior, how can I achieve such an overriding?

I asked the same question on SOF: http://stackoverflow.com/questions/19377789/javaconfig-bean-overriding-failing-with-list-injected

Thanks

---

**Affects:** 3.2.3

**Issue Links:**
- #16046 ObjectProvider iterable/stream access for \"beans of type\" resolution in `@Bean` methods
- #15653 ConfigurationClass.validate() should allow for overloading in general or not at all

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/b093b8495416cd3f05a32add1c671174341bd595, https://github.com/spring-projects/spring-framework/commit/b00c31a6204ee05230c5fecbeaf3a4de73a1215e, https://github.com/spring-projects/spring-framework/commit/78c10cd242f44680d9536721d34fedd80359a218

0 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15616","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15616/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15616/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15616/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15616","id":398162116,"node_id":"MDU6SXNzdWUzOTgxNjIxMTY=","number":15616,"title":"JavaConfig Bean overriding with addition [SPR-10988]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/93","html_url":"https://github.com/spring-projects/spring-framework/milestone/93","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/93/labels","id":3960866,"node_id":"MDk6TWlsZXN0b25lMzk2MDg2Ng==","number":93,"title":"3.2.5","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":59,"state":"closed","created_at":"2019-01-10T22:03:43Z","updated_at":"2019-01-11T11:17:34Z","due_on":"2013-11-05T08:00:00Z","closed_at":"2019-01-10T22:03:43Z"},"comments":21,"created_at":"2013-10-15T02:35:42Z","updated_at":"2019-01-13T06:57:49Z","closed_at":"2014-02-12T07:17:11Z","author_association":"COLLABORATOR","body":"**[Benoit Lacelle](https://jira.spring.io/secure/ViewProfile.jspa?name=blasd)** opened **[SPR-10988](https://jira.spring.io/browse/SPR-10988?redirect=false)** and commented\n\nI consider injection of beans as a List of automatically detected beans: I introduce several beans implementing the same interface and inject all of them as a List in a later bean.\n\nI've not been able to find official documentation related to this feature. My single source is http://www.coderanch.com/t/605509/Spring/Java-config-autowired-List\n\nConsider this feature, I have an issue with Bean overring: I would like to override a bean defined throught a no-arg method with a bean defined with the List of detected beans. However, spring behave like the second bean definition does not exist.\n\nIt can be reproduced with the following test:\n\n```\nimport java.util.Date;\nimport java.util.List;\n\nimport org.junit.Assert;\nimport org.junit.Test;\nimport org.springframework.context.annotation.AnnotationConfigApplicationContext;\nimport org.springframework.context.annotation.Bean;\nimport org.springframework.context.annotation.Configuration;\n\npublic class SpringTest {\n\n\t@Test\n\tpublic void shouldtestSpringDifferentMethodNames() {\n\t\tAnnotationConfigApplicationContext ctx2 = new AnnotationConfigApplicationContext(AConfig.class, CConfig.class);\n\t\tAssert.assertEquals(\"overriden\", ctx2.getBean(\"bean\"));\n\t}\n\n\t@Configuration\n\tpublic static class AConfig {\n\n\t\t@Bean\n\t\tpublic Object bean() {\n\t\t\treturn \"not overriden\";\n\t\t}\n\n\t}\n\n\t@Configuration\n\tpublic static class CConfig extends AConfig {\n\n\t\t@Bean\n\t\tpublic Date anotherBean() {\n\t\t\treturn new Date();\n\t\t}\n\n\t\t@Bean\n\t\tpublic Object bean(List<? extends Date> someDate) {\n\t\t\treturn \"overriden\";\n\t\t}\n\n\t}\n\n}\n```\n\nIf this is an expected behavior, how can I achieve such an overriding?\n\nI asked the same question on SOF: http://stackoverflow.com/questions/19377789/javaconfig-bean-overriding-failing-with-list-injected\n\nThanks\n\n---\n\n**Affects:** 3.2.3\n\n**Issue Links:**\n- #16046 ObjectProvider iterable/stream access for \"beans of type\" resolution in `@Bean` methods\n- #15653 ConfigurationClass.validate() should allow for overloading in general or not at all\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/b093b8495416cd3f05a32add1c671174341bd595, https://github.com/spring-projects/spring-framework/commit/b00c31a6204ee05230c5fecbeaf3a4de73a1215e, https://github.com/spring-projects/spring-framework/commit/78c10cd242f44680d9536721d34fedd80359a218\n\n0 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453406154","453406157","453406158","453406160","453406163","453406164","453406167","453406169","453406171","453406174","453406175","453406178","453406180","453406183","453406184","453406185","453406186","453406187","453406188","453406190","453406191"], "labels":["in: core","type: bug"]}