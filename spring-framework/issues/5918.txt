{"id":"5918", "title":"MBeanExporter attempts to register duplicate MBeans causing javax.management.InstanceAlreadyExistsException [SPR-1216]", "body":"**[Cameron Clarke](https://jira.spring.io/secure/ViewProfile.jspa?name=cam)** opened **[SPR-1216](https://jira.spring.io/browse/SPR-1216?redirect=false)*** and commented

Specific scenario, running JUnit test cases extending {`@link` org.springframework.test.AbstractTransactionalSpringContextTests} where the application utilises org.hibernate.jmx.StatisticsService as defined in spring-data-access-context.xml (see below), test fail due to :

\\<testcase classname=\"TestActionTypeDaoImpl\" name=\"testGetAll\" time=\"17.375\">
\\<error message=\"Error creating bean with name &apos;jmxExporter&apos; defined in class path resource [spring-data-access-context.xml]: Initialization of bean failed; nested exception is javax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats\" type=\"org.springframework.beans.factory.BeanCreationException\">org.springframework.beans.factory.BeanCreationException: Error creating bean with name &apos;jmxExporter&apos; defined in class path resource [spring-data-access-context.xml]: Initialization of bean failed; nested exception is javax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats
javax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats
at com.sun.jmx.mbeanserver.RepositorySupport.addMBean(RepositorySupport.java:452)
at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.internal_addObject(DefaultMBeanServerInterceptor.java:1410)
at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerObject(DefaultMBeanServerInterceptor.java:936)
at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerMBean(DefaultMBeanServerInterceptor.java:337)
at com.sun.jmx.mbeanserver.JmxMBeanServer.registerMBean(JmxMBeanServer.java:497)
at org.springframework.jmx.export.MBeanExporter.registerMBean(MBeanExporter.java:413)
at org.springframework.jmx.export.MBeanExporter.registerBeanInstance(MBeanExporter.java:389)
at org.springframework.jmx.export.MBeanExporter.registerBeanNameOrInstance(MBeanExporter.java:369)
at org.springframework.jmx.export.MBeanExporter.registerBeans(MBeanExporter.java:296)
at org.springframework.jmx.export.MBeanExporter.afterPropertiesSet(MBeanExporter.java:240)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1003)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:348)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:226)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:147)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:275)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)
at org.springframework.context.support.ClassPathXmlApplicationContext.&lt;init&gt;(ClassPathXmlApplicationContext.java:80)
at org.springframework.context.support.ClassPathXmlApplicationContext.&lt;init&gt;(ClassPathXmlApplicationContext.java:65)
at org.springframework.test.AbstractSpringContextTests.loadContextLocations(AbstractSpringContextTests.java:121)
at org.springframework.test.AbstractDependencyInjectionSpringContextTests.loadContextLocations(AbstractDependencyInjectionSpringContextTests.java:159)
at org.springframework.test.AbstractSpringContextTests.getContext(AbstractSpringContextTests.java:101)
at org.springframework.test.AbstractDependencyInjectionSpringContextTests.setUp(AbstractDependencyInjectionSpringContextTests.java:127)
\\</error>

spring-data-access-context.xml defined as ....

...
\\<bean id=\"jmxExporter\"
class=\"org.springframework.jmx.export.MBeanExporter\"
depends-on=\"jmxServer\">
\\<property name=\"beans\">
\\<map>
\\<entry key=\"Hibernate:name=hbmStats\">
\\<ref bean=\"hibernateStatisticsJMXBean\"/>
\\</entry>
\\</map>
\\</property>
\\</bean>

    <bean id=\"hibernateStatisticsJMXBean\"      class=\"org.hibernate.jmx.StatisticsService\">
        <property name=\"statisticsEnabled\">
            <value>true</value>
        </property>
        <property name=\"sessionFactory\">
            <ref bean=\"sessionFactory\"/>
        </property>
    </bean>
    
    <bean id=\"jmxServer\"         class=\"org.springframework.jmx.support.MBeanServerFactoryBean\">
        <property name=\"defaultDomain\">
            <value>dummyDomain</value>
        </property>
    </bean>

...

My current work around class....

/** The SpringFrameworks (v.1.2.3) {`@link` org.springframework.jmx.export.MBeanExporter} does not detect
* MBeans already registered and may attempt to register duplicate MBeans.  Either a unique / dynamic
* object naming convention could be employed to avoid this or alternatively, simply ignore
* {`@link` javax.management.InstanceAlreadyExistsException} exceptions which is the policy
* employed by this class.
* 
* `@author` Cameron Clarke
* `@version` $Id: $
  */

import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jmx.export.MBeanExporter;

import javax.management.JMException;
import javax.management.InstanceAlreadyExistsException;

public class SpringMBeanExporter extends MBeanExporter implements BeanFactoryAware, InitializingBean, DisposableBean
{
protected void registerBeans() throws JMException
{
try
{
super.registerBeans();
} catch (InstanceAlreadyExistsException ex)
{
logger.error(\"Instance already exists, registering JMX bean failed.\", ex);
}
}
}

---

**Affects:** 1.2.3

**Issue Links:**
- #5921 JMX InstanceAlreadyExistsException when deploying same app twice (_**\"is duplicated by\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5918","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5918/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5918/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5918/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5918","id":398059283,"node_id":"MDU6SXNzdWUzOTgwNTkyODM=","number":5918,"title":"MBeanExporter attempts to register duplicate MBeans causing javax.management.InstanceAlreadyExistsException [SPR-1216]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/23","html_url":"https://github.com/spring-projects/spring-framework/milestone/23","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/23/labels","id":3960793,"node_id":"MDk6TWlsZXN0b25lMzk2MDc5Mw==","number":23,"title":"1.2.5","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":40,"state":"closed","created_at":"2019-01-10T22:02:19Z","updated_at":"2019-01-11T00:10:04Z","due_on":null,"closed_at":"2019-01-10T22:02:19Z"},"comments":2,"created_at":"2005-08-08T22:17:20Z","updated_at":"2019-01-11T19:21:22Z","closed_at":"2012-06-19T03:54:30Z","author_association":"COLLABORATOR","body":"**[Cameron Clarke](https://jira.spring.io/secure/ViewProfile.jspa?name=cam)** opened **[SPR-1216](https://jira.spring.io/browse/SPR-1216?redirect=false)*** and commented\n\nSpecific scenario, running JUnit test cases extending {`@link` org.springframework.test.AbstractTransactionalSpringContextTests} where the application utilises org.hibernate.jmx.StatisticsService as defined in spring-data-access-context.xml (see below), test fail due to :\n\n\\<testcase classname=\"TestActionTypeDaoImpl\" name=\"testGetAll\" time=\"17.375\">\n\\<error message=\"Error creating bean with name &apos;jmxExporter&apos; defined in class path resource [spring-data-access-context.xml]: Initialization of bean failed; nested exception is javax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats\" type=\"org.springframework.beans.factory.BeanCreationException\">org.springframework.beans.factory.BeanCreationException: Error creating bean with name &apos;jmxExporter&apos; defined in class path resource [spring-data-access-context.xml]: Initialization of bean failed; nested exception is javax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats\njavax.management.InstanceAlreadyExistsException: Hibernate:name=hbmStats\nat com.sun.jmx.mbeanserver.RepositorySupport.addMBean(RepositorySupport.java:452)\nat com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.internal_addObject(DefaultMBeanServerInterceptor.java:1410)\nat com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerObject(DefaultMBeanServerInterceptor.java:936)\nat com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerMBean(DefaultMBeanServerInterceptor.java:337)\nat com.sun.jmx.mbeanserver.JmxMBeanServer.registerMBean(JmxMBeanServer.java:497)\nat org.springframework.jmx.export.MBeanExporter.registerMBean(MBeanExporter.java:413)\nat org.springframework.jmx.export.MBeanExporter.registerBeanInstance(MBeanExporter.java:389)\nat org.springframework.jmx.export.MBeanExporter.registerBeanNameOrInstance(MBeanExporter.java:369)\nat org.springframework.jmx.export.MBeanExporter.registerBeans(MBeanExporter.java:296)\nat org.springframework.jmx.export.MBeanExporter.afterPropertiesSet(MBeanExporter.java:240)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1003)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:348)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:226)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:147)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:275)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)\nat org.springframework.context.support.ClassPathXmlApplicationContext.&lt;init&gt;(ClassPathXmlApplicationContext.java:80)\nat org.springframework.context.support.ClassPathXmlApplicationContext.&lt;init&gt;(ClassPathXmlApplicationContext.java:65)\nat org.springframework.test.AbstractSpringContextTests.loadContextLocations(AbstractSpringContextTests.java:121)\nat org.springframework.test.AbstractDependencyInjectionSpringContextTests.loadContextLocations(AbstractDependencyInjectionSpringContextTests.java:159)\nat org.springframework.test.AbstractSpringContextTests.getContext(AbstractSpringContextTests.java:101)\nat org.springframework.test.AbstractDependencyInjectionSpringContextTests.setUp(AbstractDependencyInjectionSpringContextTests.java:127)\n\\</error>\n\nspring-data-access-context.xml defined as ....\n\n...\n\\<bean id=\"jmxExporter\"\nclass=\"org.springframework.jmx.export.MBeanExporter\"\ndepends-on=\"jmxServer\">\n\\<property name=\"beans\">\n\\<map>\n\\<entry key=\"Hibernate:name=hbmStats\">\n\\<ref bean=\"hibernateStatisticsJMXBean\"/>\n\\</entry>\n\\</map>\n\\</property>\n\\</bean>\n\n    <bean id=\"hibernateStatisticsJMXBean\"      class=\"org.hibernate.jmx.StatisticsService\">\n        <property name=\"statisticsEnabled\">\n            <value>true</value>\n        </property>\n        <property name=\"sessionFactory\">\n            <ref bean=\"sessionFactory\"/>\n        </property>\n    </bean>\n    \n    <bean id=\"jmxServer\"         class=\"org.springframework.jmx.support.MBeanServerFactoryBean\">\n        <property name=\"defaultDomain\">\n            <value>dummyDomain</value>\n        </property>\n    </bean>\n\n...\n\nMy current work around class....\n\n/** The SpringFrameworks (v.1.2.3) {`@link` org.springframework.jmx.export.MBeanExporter} does not detect\n* MBeans already registered and may attempt to register duplicate MBeans.  Either a unique / dynamic\n* object naming convention could be employed to avoid this or alternatively, simply ignore\n* {`@link` javax.management.InstanceAlreadyExistsException} exceptions which is the policy\n* employed by this class.\n* \n* `@author` Cameron Clarke\n* `@version` $Id: $\n  */\n\nimport org.springframework.beans.factory.BeanFactoryAware;\nimport org.springframework.beans.factory.DisposableBean;\nimport org.springframework.beans.factory.InitializingBean;\nimport org.springframework.jmx.export.MBeanExporter;\n\nimport javax.management.JMException;\nimport javax.management.InstanceAlreadyExistsException;\n\npublic class SpringMBeanExporter extends MBeanExporter implements BeanFactoryAware, InitializingBean, DisposableBean\n{\nprotected void registerBeans() throws JMException\n{\ntry\n{\nsuper.registerBeans();\n} catch (InstanceAlreadyExistsException ex)\n{\nlogger.error(\"Instance already exists, registering JMX bean failed.\", ex);\n}\n}\n}\n\n---\n\n**Affects:** 1.2.3\n\n**Issue Links:**\n- #5921 JMX InstanceAlreadyExistsException when deploying same app twice (_**\"is duplicated by\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453296792","453296793"], "labels":["in: core","type: bug"]}