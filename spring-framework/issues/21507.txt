{"id":"21507", "title":"FluxSink emitting does not happen when next called during Flux create [SPR-16969]", "body":"**[Chris Roberts](https://jira.spring.io/secure/ViewProfile.jspa?name=springalong)** opened **[SPR-16969](https://jira.spring.io/browse/SPR-16969?redirect=false)** and commented

Not sure if an issue, or I'm misunderstanding webflux operation. I'm trying to create a Flux\\<String> which is used by the client when data is available, ie: streamed string by string

Spring boot 2.0.1, using chrome

http://localhost:8080/demo -> Flux stream , streams the results

http://localhost:8080/demo2 -> Flux create, streams the results

 

Spring boot 2.0.3:

http://localhost:8080/demo -> Flux stream , streams the results

http://localhost:8080/demo2 -> Flux create, does not stream but waits for the all results

 

The reactor docs say: 

\"The more advanced form of programmatic creation of a `Flux`, `create` can work asynchronously or synchronously and is suitable for multiple emissions per round.\"

 

 

 

 

 

 


---

**Affects:** 5.0.7

**Reference URL:** http://projectreactor.io/docs/core/release/reference/

**Attachments:**
- [DemoController.java](https://jira.spring.io/secure/attachment/25749/DemoController.java) (_1.15 kB_)
- [pom.xml](https://jira.spring.io/secure/attachment/25750/pom.xml) (_1.35 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21507","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21507/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21507/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21507/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21507","id":398229804,"node_id":"MDU6SXNzdWUzOTgyMjk4MDQ=","number":21507,"title":"FluxSink emitting does not happen when next called during Flux create [SPR-16969]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2018-06-22T07:34:56Z","updated_at":"2019-01-12T16:21:01Z","closed_at":"2018-07-05T04:00:53Z","author_association":"COLLABORATOR","body":"**[Chris Roberts](https://jira.spring.io/secure/ViewProfile.jspa?name=springalong)** opened **[SPR-16969](https://jira.spring.io/browse/SPR-16969?redirect=false)** and commented\n\nNot sure if an issue, or I'm misunderstanding webflux operation. I'm trying to create a Flux\\<String> which is used by the client when data is available, ie: streamed string by string\n\nSpring boot 2.0.1, using chrome\n\nhttp://localhost:8080/demo -> Flux stream , streams the results\n\nhttp://localhost:8080/demo2 -> Flux create, streams the results\n\n \n\nSpring boot 2.0.3:\n\nhttp://localhost:8080/demo -> Flux stream , streams the results\n\nhttp://localhost:8080/demo2 -> Flux create, does not stream but waits for the all results\n\n \n\nThe reactor docs say: \n\n\"The more advanced form of programmatic creation of a `Flux`, `create` can work asynchronously or synchronously and is suitable for multiple emissions per round.\"\n\n \n\n \n\n \n\n \n\n \n\n \n\n\n---\n\n**Affects:** 5.0.7\n\n**Reference URL:** http://projectreactor.io/docs/core/release/reference/\n\n**Attachments:**\n- [DemoController.java](https://jira.spring.io/secure/attachment/25749/DemoController.java) (_1.15 kB_)\n- [pom.xml](https://jira.spring.io/secure/attachment/25750/pom.xml) (_1.35 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453473729","453473731","453473733","453473734","453473737"], "labels":["in: web","status: declined"]}