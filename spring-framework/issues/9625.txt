{"id":"9625", "title":"Default message Listener commiting even when no message is received [SPR-4950]", "body":"**[Prasanna Tuladhar](https://jira.spring.io/secure/ViewProfile.jspa?name=ptuladhar)** opened **[SPR-4950](https://jira.spring.io/browse/SPR-4950?redirect=false)** and commented

The DefaultMessageListener class commits even when no message is received. This operation can be fairly expensive related if one is using JTA transaction manager .

applicationContext.xml

---

    <bean id=\"emailConnectionFactory\"
    	class=\"org.springframework.jndi.JndiObjectFactoryBean\">
    	<property name=\"jndiName\">
    		<value>com.wirecard.jms.cf.emailforwarder</value>
    	</property>
    	<property name=\"resourceRef\" value=\"true\" />
    </bean>
    
    
    <bean id=\"emailQueue\"
    	class=\"org.springframework.jndi.JndiObjectFactoryBean\">
    	<property name=\"jndiName\"
    		value=\"com.wirecard.jms.queue.emailforwarder.request\" />
    	<property name=\"resourceRef\" value=\"true\" />
    </bean>
    
    <bean id=\"jmsDestinationResolver\"
    	class=\"org.springframework.jms.support.destination.JndiDestinationResolver\">
    	<property name=\"cache\">
    		<value>true</value>
    	</property>
    	<property name=\"resourceRef\" value=\"true\"/>
    </bean>
    
    <bean id=\"jmsTemplateForEmailForwarding\"
    	class=\"org.springframework.jms.core.JmsTemplate\">
    	<property name=\"connectionFactory\">
    		<ref bean=\"emailConnectionFactory\" />
    	</property>
    	<property name=\"destinationResolver\">
    		<ref bean=\"jmsDestinationResolver\" />
    	</property>
    	<property name=\"defaultDestination\" ref=\"emailQueue\" />
    </bean>
    
    <bean id=\"jmsContainerForEmail\"
    	class=\"org.springframework.jms.listener.DefaultMessageListenerContainer\">
    	<property name=\"connectionFactory\" ref=\"emailConnectionFactory\" />
    	<property name=\"destination\" ref=\"emailQueue\" />
    	<property name=\"messageListener\" ref=\"emailConsumer\" />
    	<property name=\"transactionManager\" ref=\"txManager\" />
    	<property name=\"concurrentConsumers\" value=\"5\" />
    	<property name=\"maxConcurrentConsumers\" value=\"50\" />
    	<property name=\"receiveTimeout\" value=\"5000\" />
    </bean>

web.xml

---

    <resource-ref>
    	<res-ref-name>billingWM</res-ref-name>
    	<res-type>commonj.work.WorkManager</res-type>
    	<res-auth>Container</res-auth>
    	<res-sharing-scope>Shareable</res-sharing-scope>
    </resource-ref>
    
    <resource-ref>
    	<res-ref-name>
    		com.wirecard.jms.cf.billingscheduler
    	</res-ref-name>
    	<res-type>javax.jms.QueueConnectionFactory</res-type>
    	<res-auth>Container</res-auth>
    	<res-sharing-scope>Shareable</res-sharing-scope>
    </resource-ref>
    
    <resource-ref>
    	<res-ref-name>com.wirecard.jms.cf.emailforwarder</res-ref-name>
    	<res-type>javax.jms.QueueConnectionFactory</res-type>
    	<res-auth>Container</res-auth>
    	<res-sharing-scope>Shareable</res-sharing-scope>
    </resource-ref>
    
    <resource-env-ref>
    	<resource-env-ref-name>
    		jms/email
    	</resource-env-ref-name>
    	<resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>
    </resource-env-ref>
    
    <resource-env-ref>
    	<resource-env-ref-name>
    		jms/scheduler
    	</resource-env-ref-name>
    	<resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>
    </resource-env-ref>

---

**Affects:** 2.5.4

3 votes, 10 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9625","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9625/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9625/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9625/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9625","id":398089385,"node_id":"MDU6SXNzdWUzOTgwODkzODU=","number":9625,"title":"Default message Listener commiting even when no message is received [SPR-4950]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511784,"node_id":"MDU6TGFiZWwxMTg4NTExNzg0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20messaging","name":"in: messaging","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2008-06-24T20:30:54Z","updated_at":"2019-01-13T21:52:06Z","closed_at":"2015-09-22T17:34:35Z","author_association":"COLLABORATOR","body":"**[Prasanna Tuladhar](https://jira.spring.io/secure/ViewProfile.jspa?name=ptuladhar)** opened **[SPR-4950](https://jira.spring.io/browse/SPR-4950?redirect=false)** and commented\n\nThe DefaultMessageListener class commits even when no message is received. This operation can be fairly expensive related if one is using JTA transaction manager .\n\napplicationContext.xml\n\n---\n\n    <bean id=\"emailConnectionFactory\"\n    \tclass=\"org.springframework.jndi.JndiObjectFactoryBean\">\n    \t<property name=\"jndiName\">\n    \t\t<value>com.wirecard.jms.cf.emailforwarder</value>\n    \t</property>\n    \t<property name=\"resourceRef\" value=\"true\" />\n    </bean>\n    \n    \n    <bean id=\"emailQueue\"\n    \tclass=\"org.springframework.jndi.JndiObjectFactoryBean\">\n    \t<property name=\"jndiName\"\n    \t\tvalue=\"com.wirecard.jms.queue.emailforwarder.request\" />\n    \t<property name=\"resourceRef\" value=\"true\" />\n    </bean>\n    \n    <bean id=\"jmsDestinationResolver\"\n    \tclass=\"org.springframework.jms.support.destination.JndiDestinationResolver\">\n    \t<property name=\"cache\">\n    \t\t<value>true</value>\n    \t</property>\n    \t<property name=\"resourceRef\" value=\"true\"/>\n    </bean>\n    \n    <bean id=\"jmsTemplateForEmailForwarding\"\n    \tclass=\"org.springframework.jms.core.JmsTemplate\">\n    \t<property name=\"connectionFactory\">\n    \t\t<ref bean=\"emailConnectionFactory\" />\n    \t</property>\n    \t<property name=\"destinationResolver\">\n    \t\t<ref bean=\"jmsDestinationResolver\" />\n    \t</property>\n    \t<property name=\"defaultDestination\" ref=\"emailQueue\" />\n    </bean>\n    \n    <bean id=\"jmsContainerForEmail\"\n    \tclass=\"org.springframework.jms.listener.DefaultMessageListenerContainer\">\n    \t<property name=\"connectionFactory\" ref=\"emailConnectionFactory\" />\n    \t<property name=\"destination\" ref=\"emailQueue\" />\n    \t<property name=\"messageListener\" ref=\"emailConsumer\" />\n    \t<property name=\"transactionManager\" ref=\"txManager\" />\n    \t<property name=\"concurrentConsumers\" value=\"5\" />\n    \t<property name=\"maxConcurrentConsumers\" value=\"50\" />\n    \t<property name=\"receiveTimeout\" value=\"5000\" />\n    </bean>\n\nweb.xml\n\n---\n\n    <resource-ref>\n    \t<res-ref-name>billingWM</res-ref-name>\n    \t<res-type>commonj.work.WorkManager</res-type>\n    \t<res-auth>Container</res-auth>\n    \t<res-sharing-scope>Shareable</res-sharing-scope>\n    </resource-ref>\n    \n    <resource-ref>\n    \t<res-ref-name>\n    \t\tcom.wirecard.jms.cf.billingscheduler\n    \t</res-ref-name>\n    \t<res-type>javax.jms.QueueConnectionFactory</res-type>\n    \t<res-auth>Container</res-auth>\n    \t<res-sharing-scope>Shareable</res-sharing-scope>\n    </resource-ref>\n    \n    <resource-ref>\n    \t<res-ref-name>com.wirecard.jms.cf.emailforwarder</res-ref-name>\n    \t<res-type>javax.jms.QueueConnectionFactory</res-type>\n    \t<res-auth>Container</res-auth>\n    \t<res-sharing-scope>Shareable</res-sharing-scope>\n    </resource-ref>\n    \n    <resource-env-ref>\n    \t<resource-env-ref-name>\n    \t\tjms/email\n    \t</resource-env-ref-name>\n    \t<resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>\n    </resource-env-ref>\n    \n    <resource-env-ref>\n    \t<resource-env-ref-name>\n    \t\tjms/scheduler\n    \t</resource-env-ref-name>\n    \t<resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>\n    </resource-env-ref>\n\n---\n\n**Affects:** 2.5.4\n\n3 votes, 10 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453331875"], "labels":["in: messaging","status: declined","type: enhancement"]}