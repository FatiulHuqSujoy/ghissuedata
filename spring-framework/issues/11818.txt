{"id":"11818", "title":"Correct typos: ch 19 [SPR-7159]", "body":"**[Cristian George Rapauzu](https://jira.spring.io/secure/ViewProfile.jspa?name=cg_rapauzu)** opened **[SPR-7159](https://jira.spring.io/browse/SPR-7159?redirect=false)** and commented

Document URL: http://static.springsource.org/spring/docs/current/spring-framework-reference/html/
Spring 3.0 - Chapter 19. Remoting and web services using Spring

Location: 19.1 Introduction
Problem: Confusing text: \"four remoting technologies:\" is followed by six same-level bullets
Text: Currently, Spring supports four remoting technologies: ...
Suggestion: Restructure list:
1. RMI
2. HTTP-based
   2.1. HTTP Invoker
   2.2. Hessian
   2.3. Burlap
3. Web Services
   3.1. JAX-RPC
   3.2. JAX-WS
4. JMS

Location: 19.3.1 Wiring up the DispatcherServlet for Hessian and co.
Problem: Missing word \"is\": \"this an excerpt from\" instead of \"this is an excerpt from\"
Text: this an excerpt from 'web.xml'
Suggestion: Consistency - use filename as title for all code samples
(web.xml , WEB-INF/applicationContext.xml, remoting-servlet.xml)

Location: 19.3.5 Applying HTTP basic authentication to a service exposed through Hessian or Burlap
Problem: Missing word \"is\": \"This an example\" instead of \"This is an example\"
Text: This an example where we explicitly mention the BeanNameUrlHandlerMapping and ...

Location: 19.4 Exposing services using HTTP invokers
Problem: Inconsistent case usage for HTTP: \"Spring Http invokers\" instead of \"Spring HTTP invokers\"
Text: Spring Http invokers use the standard Java serialization mechanism to expose services through HTTP ...

Location: 19.4.1 Exposing the service object
Problem: Typo - extra char \"s\": \"for a service objects\" instead of \"for a service object\"
Text: Setting up the HTTP invoker infrastructure for a service objects much resembles the way you would do using Hessian or Burlap.

Location: 19.4.1 Exposing the service object
Problem: Typo - missing char \"I\": \"f you are running\" instead of \"If you are running\"
Text: f you are running outside of a servlet container and are using ...

Location: 19.4.1 Exposing the service object
Problem: Inconsistent text style usage for class names (SimpleHttpServerFactoryBean, SimpleHttpInvokerServiceExporter): normal text instead of fixed-width text
Text: You can configure the SimpleHttpServerFactoryBean together with a SimpleHttpInvokerServiceExporter as is shown in this example

Location: 19.5.2 Accessing web services using JAX-RPC
Problem: Typo - missing char \"t\": \"this a startup time\" instead of \"this at startup time\"
Text: Spring needs this a startup time to create the JAX-RPC Service.

Location: 19.5.8 Accessing web services using JAX-WS
Problem: Typo - missing char \"t\": \"this a startup time\" instead of \"this at startup time\"
Text: Spring needs this a startup time to create the JAX-WS Service.

Location: 19.5.9 Exposing web services using XFire
Problem: Missing word \"is\": \"context that shipping\" instead of \"context that is shipping\"
Text: Exposing XFire is done using a XFire context that shipping with XFire itself in combination with a RemoteExporter-style bean you have to add to your WebApplicationContext.

Location: 19.8 Considerations when choosing a technology
Problem: Typo - missing char \"r\": \"consider you needs\" instead of \"consider your needs\"
Text: You should carefully consider you needs, the services your exposing and the objects you'll be sending over the wire when choosing a technology.

Location: 19.8 Considerations when choosing a technology
Problem: Typo: \"the services your exposing\" instead of \"the services you are exposing\"
Text: You should carefully consider you needs, the services your exposing and the objects you'll be sending over the wire when choosing a technology.

Location: 19.8 Considerations when choosing a technology
Problem: Typo - missing char \"s\": \"it support\" instead of \"it supports\"
Text: RMI is a fairly heavy-weight protocol in that it support full-object serialization which is important when using a complex data model that needs serialization over the wire.

Location: 19.9.1 RestTemplate
Problem: Typo - missing char \".\" to break in two phrases: \"RestClientException will be thrown, this behavior can be changed\" instead of \"RestClientException will be thrown. This behavior can be changed\"
Text: In case of an exception processing the HTTP request, an exception of the type RestClientException will be thrown, this behavior can be changed by plugging in another ResponseErrorHandler  implementation into the RestTemplate.

Location: 19.9.1.1 Dealing with request and response headers
Problem: Typo (probably) - text mentions \"execute method\", but code mentions \"exchange\" method
Text: Besides the methods described above, the RestTemplate  also has the exchange method, which can be used for arbitrary HTTP method execution based on the HttpEntity  class. Perhaps most importantly, the execute method can be used to add request headers and read response headers. For example:
... HttpEntity\\<String> response = template.exchange(...)


---

**Affects:** 3.0.1

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a45d33a158a18e43287a3b6941018f05c1908c46
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11818","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11818/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11818/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11818/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11818","id":398104923,"node_id":"MDU6SXNzdWUzOTgxMDQ5MjM=","number":11818,"title":"Correct typos: ch 19 [SPR-7159]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511868,"node_id":"MDU6TGFiZWwxMTg4NTExODY4","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20documentation","name":"type: documentation","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70","html_url":"https://github.com/spring-projects/spring-framework/milestone/70","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70/labels","id":3960843,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mw==","number":70,"title":"3.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":117,"state":"closed","created_at":"2019-01-10T22:03:16Z","updated_at":"2019-01-11T02:35:54Z","due_on":"2010-06-13T07:00:00Z","closed_at":"2019-01-10T22:03:16Z"},"comments":1,"created_at":"2010-05-02T10:21:38Z","updated_at":"2012-06-19T03:42:58Z","closed_at":"2012-06-19T03:42:58Z","author_association":"COLLABORATOR","body":"**[Cristian George Rapauzu](https://jira.spring.io/secure/ViewProfile.jspa?name=cg_rapauzu)** opened **[SPR-7159](https://jira.spring.io/browse/SPR-7159?redirect=false)** and commented\n\nDocument URL: http://static.springsource.org/spring/docs/current/spring-framework-reference/html/\nSpring 3.0 - Chapter 19. Remoting and web services using Spring\n\nLocation: 19.1 Introduction\nProblem: Confusing text: \"four remoting technologies:\" is followed by six same-level bullets\nText: Currently, Spring supports four remoting technologies: ...\nSuggestion: Restructure list:\n1. RMI\n2. HTTP-based\n   2.1. HTTP Invoker\n   2.2. Hessian\n   2.3. Burlap\n3. Web Services\n   3.1. JAX-RPC\n   3.2. JAX-WS\n4. JMS\n\nLocation: 19.3.1 Wiring up the DispatcherServlet for Hessian and co.\nProblem: Missing word \"is\": \"this an excerpt from\" instead of \"this is an excerpt from\"\nText: this an excerpt from 'web.xml'\nSuggestion: Consistency - use filename as title for all code samples\n(web.xml , WEB-INF/applicationContext.xml, remoting-servlet.xml)\n\nLocation: 19.3.5 Applying HTTP basic authentication to a service exposed through Hessian or Burlap\nProblem: Missing word \"is\": \"This an example\" instead of \"This is an example\"\nText: This an example where we explicitly mention the BeanNameUrlHandlerMapping and ...\n\nLocation: 19.4 Exposing services using HTTP invokers\nProblem: Inconsistent case usage for HTTP: \"Spring Http invokers\" instead of \"Spring HTTP invokers\"\nText: Spring Http invokers use the standard Java serialization mechanism to expose services through HTTP ...\n\nLocation: 19.4.1 Exposing the service object\nProblem: Typo - extra char \"s\": \"for a service objects\" instead of \"for a service object\"\nText: Setting up the HTTP invoker infrastructure for a service objects much resembles the way you would do using Hessian or Burlap.\n\nLocation: 19.4.1 Exposing the service object\nProblem: Typo - missing char \"I\": \"f you are running\" instead of \"If you are running\"\nText: f you are running outside of a servlet container and are using ...\n\nLocation: 19.4.1 Exposing the service object\nProblem: Inconsistent text style usage for class names (SimpleHttpServerFactoryBean, SimpleHttpInvokerServiceExporter): normal text instead of fixed-width text\nText: You can configure the SimpleHttpServerFactoryBean together with a SimpleHttpInvokerServiceExporter as is shown in this example\n\nLocation: 19.5.2 Accessing web services using JAX-RPC\nProblem: Typo - missing char \"t\": \"this a startup time\" instead of \"this at startup time\"\nText: Spring needs this a startup time to create the JAX-RPC Service.\n\nLocation: 19.5.8 Accessing web services using JAX-WS\nProblem: Typo - missing char \"t\": \"this a startup time\" instead of \"this at startup time\"\nText: Spring needs this a startup time to create the JAX-WS Service.\n\nLocation: 19.5.9 Exposing web services using XFire\nProblem: Missing word \"is\": \"context that shipping\" instead of \"context that is shipping\"\nText: Exposing XFire is done using a XFire context that shipping with XFire itself in combination with a RemoteExporter-style bean you have to add to your WebApplicationContext.\n\nLocation: 19.8 Considerations when choosing a technology\nProblem: Typo - missing char \"r\": \"consider you needs\" instead of \"consider your needs\"\nText: You should carefully consider you needs, the services your exposing and the objects you'll be sending over the wire when choosing a technology.\n\nLocation: 19.8 Considerations when choosing a technology\nProblem: Typo: \"the services your exposing\" instead of \"the services you are exposing\"\nText: You should carefully consider you needs, the services your exposing and the objects you'll be sending over the wire when choosing a technology.\n\nLocation: 19.8 Considerations when choosing a technology\nProblem: Typo - missing char \"s\": \"it support\" instead of \"it supports\"\nText: RMI is a fairly heavy-weight protocol in that it support full-object serialization which is important when using a complex data model that needs serialization over the wire.\n\nLocation: 19.9.1 RestTemplate\nProblem: Typo - missing char \".\" to break in two phrases: \"RestClientException will be thrown, this behavior can be changed\" instead of \"RestClientException will be thrown. This behavior can be changed\"\nText: In case of an exception processing the HTTP request, an exception of the type RestClientException will be thrown, this behavior can be changed by plugging in another ResponseErrorHandler  implementation into the RestTemplate.\n\nLocation: 19.9.1.1 Dealing with request and response headers\nProblem: Typo (probably) - text mentions \"execute method\", but code mentions \"exchange\" method\nText: Besides the methods described above, the RestTemplate  also has the exchange method, which can be used for arbitrary HTTP method execution based on the HttpEntity  class. Perhaps most importantly, the execute method can be used to add request headers and read response headers. For example:\n... HttpEntity\\<String> response = template.exchange(...)\n\n\n---\n\n**Affects:** 3.0.1\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a45d33a158a18e43287a3b6941018f05c1908c46\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453350632"], "labels":["type: documentation"]}