{"id":"19466", "title":"component-scan for .groovy files does not work on JDK 9 [SPR-14900]", "body":"**[Alexander](https://jira.spring.io/secure/ViewProfile.jspa?name=chabapok)** opened **[SPR-14900](https://jira.spring.io/browse/SPR-14900?redirect=false)** and commented

\"component-scan\" function not working on java-9 (tested on jdk-9-ea+143) with groovy configuration:

```
package prob;
import org.springframework.context.support.GenericGroovyApplicationContext;
public class Main {
    public static void main(String[] args) {
        GenericGroovyApplicationContext ggac = new GenericGroovyApplicationContext(\"classpath:beans.groovy\");
        Foo foo = (Foo) ggac.getBean(\"foo\");
        System.out.println(foo.ok? \"Ok\":\"FAIL!\");
    }
}


package prob;
public class Foo {
    boolean ok;
    @javax.annotation.PostConstruct
    public void pc(){
        ok = true;
    }
}

beans.groovy:

import prob.Foo
this.beans {
    xmlns([ctx:'http://www.springframework.org/schema/context'])
    ctx.'component-scan'('base-package':'prob')
    foo(Foo)
}

```

java8 output: Ok
java9 output: FAIL!


---

**Affects:** 5.0 M3
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19466","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19466/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19466/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19466/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19466","id":398200347,"node_id":"MDU6SXNzdWUzOTgyMDAzNDc=","number":19466,"title":"component-scan for .groovy files does not work on JDK 9 [SPR-14900]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2016-11-11T20:15:44Z","updated_at":"2019-01-12T00:12:00Z","closed_at":"2019-01-12T00:12:00Z","author_association":"COLLABORATOR","body":"**[Alexander](https://jira.spring.io/secure/ViewProfile.jspa?name=chabapok)** opened **[SPR-14900](https://jira.spring.io/browse/SPR-14900?redirect=false)** and commented\n\n\"component-scan\" function not working on java-9 (tested on jdk-9-ea+143) with groovy configuration:\n\n```\npackage prob;\nimport org.springframework.context.support.GenericGroovyApplicationContext;\npublic class Main {\n    public static void main(String[] args) {\n        GenericGroovyApplicationContext ggac = new GenericGroovyApplicationContext(\"classpath:beans.groovy\");\n        Foo foo = (Foo) ggac.getBean(\"foo\");\n        System.out.println(foo.ok? \"Ok\":\"FAIL!\");\n    }\n}\n\n\npackage prob;\npublic class Foo {\n    boolean ok;\n    @javax.annotation.PostConstruct\n    public void pc(){\n        ok = true;\n    }\n}\n\nbeans.groovy:\n\nimport prob.Foo\nthis.beans {\n    xmlns([ctx:'http://www.springframework.org/schema/context'])\n    ctx.'component-scan'('base-package':'prob')\n    foo(Foo)\n}\n\n```\n\njava8 output: Ok\njava9 output: FAIL!\n\n\n---\n\n**Affects:** 5.0 M3\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453695014"], "labels":["in: core","status: bulk-closed"]}