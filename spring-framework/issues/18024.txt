{"id":"18024", "title":"Possible bug on Spring 'tags/form' for input title and placeholder attributes working with Spring 'message' element [SPR-13444]", "body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-13444](https://jira.spring.io/browse/SPR-13444?redirect=false)** and commented

I am working with Spring 4.2.0

I have the following:

```html
<%@ page language=\"java\" contentType=\"text/html; charset=UTF-8\" pageEncoding=\"UTF-8\"%>
<%@ taglib prefix=\"spring\" uri=\"http://www.springframework.org/tags\"%>
<%@ taglib prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%>

...
<label for=\"fecha\"><spring:message code=\"persona.fecha.label\" text=\"_fecha\" /></label>
<form:input path=\"fecha\" 
          size=\"50\" 
          readonly=\"true\" 
          title=\"<spring:message code='persona.fecha.title' />\" />
...	
<input type=\"submit\" 
           value=\"<spring:message code='button.register' text='_button.register' />\" />
```

i18n (MessageSource) works fine.
For the _label_ and _submit_ the <spring:message> works fine.

The problem is for the **input**'s _title_ attribute. It always shows the raw content.
It means **<spring:message code='persona.fecha.title' />** is **not** _tranformed_ or _interpreted_

I can confirm that using <%@ taglib prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%> and
for **<form:input** the STS with Ctrl+Space can show the **title** attribute.

Seems it is a bug because:

If I use (html directly within the JSP file)

```html
<input type=\"text\" id=\"abc\" 
           name=\"abc\" 
           placeholder=\"enter input\" 
           title=\"<spring:message code='persona.fecha.title' />\"
           />
```

**<spring:message code='persona.fecha.title' />** is really _tranformed_ or _interpreted_

Therefore works fine...

**Warning**: Same problem for the **placeholder** attribute.

```html
<form:input path=\"fecha\" size=\"50\"
          placeholder=\"<spring:message code='persona.fecha.title' />\"
          title=\"<spring:message code='persona.fecha.title' />\" />
```



---

**Affects:** 4.2.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18024","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18024/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18024/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18024/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18024","id":398183934,"node_id":"MDU6SXNzdWUzOTgxODM5MzQ=","number":18024,"title":"Possible bug on Spring 'tags/form' for input title and placeholder attributes working with Spring 'message' element [SPR-13444]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2015-09-07T17:31:15Z","updated_at":"2019-01-12T16:22:53Z","closed_at":"2015-09-25T08:11:35Z","author_association":"COLLABORATOR","body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-13444](https://jira.spring.io/browse/SPR-13444?redirect=false)** and commented\n\nI am working with Spring 4.2.0\n\nI have the following:\n\n```html\n<%@ page language=\"java\" contentType=\"text/html; charset=UTF-8\" pageEncoding=\"UTF-8\"%>\n<%@ taglib prefix=\"spring\" uri=\"http://www.springframework.org/tags\"%>\n<%@ taglib prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%>\n\n...\n<label for=\"fecha\"><spring:message code=\"persona.fecha.label\" text=\"_fecha\" /></label>\n<form:input path=\"fecha\" \n          size=\"50\" \n          readonly=\"true\" \n          title=\"<spring:message code='persona.fecha.title' />\" />\n...\t\n<input type=\"submit\" \n           value=\"<spring:message code='button.register' text='_button.register' />\" />\n```\n\ni18n (MessageSource) works fine.\nFor the _label_ and _submit_ the <spring:message> works fine.\n\nThe problem is for the **input**'s _title_ attribute. It always shows the raw content.\nIt means **<spring:message code='persona.fecha.title' />** is **not** _tranformed_ or _interpreted_\n\nI can confirm that using <%@ taglib prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%> and\nfor **<form:input** the STS with Ctrl+Space can show the **title** attribute.\n\nSeems it is a bug because:\n\nIf I use (html directly within the JSP file)\n\n```html\n<input type=\"text\" id=\"abc\" \n           name=\"abc\" \n           placeholder=\"enter input\" \n           title=\"<spring:message code='persona.fecha.title' />\"\n           />\n```\n\n**<spring:message code='persona.fecha.title' />** is really _tranformed_ or _interpreted_\n\nTherefore works fine...\n\n**Warning**: Same problem for the **placeholder** attribute.\n\n```html\n<form:input path=\"fecha\" size=\"50\"\n          placeholder=\"<spring:message code='persona.fecha.title' />\"\n          title=\"<spring:message code='persona.fecha.title' />\" />\n```\n\n\n\n---\n\n**Affects:** 4.2.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453430126","453430128","453430130"], "labels":["in: web","status: declined"]}