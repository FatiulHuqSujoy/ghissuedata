{"id":"17618", "title":"Spring bean not autowiring when using Groovy closures [SPR-13027]", "body":"**[Mark Manders](https://jira.spring.io/secure/ViewProfile.jspa?name=mmjmanders)** opened **[SPR-13027](https://jira.spring.io/browse/SPR-13027?redirect=false)** and commented

When using the Spring framework in combination with the Groovy language, it is impossible to access `@Autowired` beans from a Groovy closure.

Exception in thread \"main\" java.lang.IllegalStateException: Failed to execute CommandLineRunner
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.callGroovyObjectGetProperty(AbstractCallSite.java:304)
at org.springframework.boot.SpringApplication.runCommandLineRunners(SpringApplication.java:675)
at com.example.closures.ClosuresApplication$_run_closure1.doCall(ClosuresApplication.groovy:22)
at org.springframework.boot.SpringApplication.afterRefresh(SpringApplication.java:690)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at org.springframework.boot.SpringApplication.run(SpringApplication.java:321)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
at org.springframework.boot.SpringApplication.run(SpringApplication.java:957)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
at org.springframework.boot.SpringApplication.run(SpringApplication.java:946)
at java.lang.reflect.Method.invoke(Method.java:497)
at org.springframework.boot.SpringApplication$run.call(Unknown Source)
at org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:90)
at org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:45)
at groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:324)
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:110)
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:130)
at org.codehaus.groovy.runtime.metaclass.ClosureMetaClass.invokeMethod(ClosureMetaClass.java:292)
at com.example.closures.ClosuresApplication.main(ClosuresApplication.groovy:27)
at groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1016)
Caused by: groovy.lang.MissingPropertyException: No such property: bank for class: com.example.closures.ClosuresApplication$$EnhancerBySpringCGLIB$$44735576
at groovy.lang.Closure.call(Closure.java:423)
at groovy.lang.Closure.call(Closure.java:439)
at org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2027)
at org.codehaus.groovy.runtime.ScriptBytecodeAdapter.unwrap(ScriptBytecodeAdapter.java:51)
at org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2012)
at org.codehaus.groovy.runtime.callsite.PogoGetPropertySite.getProperty(PogoGetPropertySite.java:49)
at org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2053)
at org.codehaus.groovy.runtime.dgm$162.invoke(Unknown Source)
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.callGroovyObjectGetProperty(AbstractCallSite.java:304)
at org.codehaus.groovy.runtime.callsite.PojoMetaMethodSite$PojoMetaMethodSiteNoUnwrapNoCoerce.invoke(PojoMetaMethodSite.java:271)
at org.codehaus.groovy.runtime.callsite.PojoMetaMethodSite.call(PojoMetaMethodSite.java:53)
at com.example.closures.ClosuresApplication$_run_closure1.doCall(ClosuresApplication.groovy:22)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:45)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:110)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:122)
at java.lang.reflect.Method.invoke(Method.java:497)
at com.example.closures.ClosuresApplication.run(ClosuresApplication.groovy:21)
at org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:90)
at org.springframework.boot.SpringApplication.runCommandLineRunners(SpringApplication.java:672)
... 9 common frames omitted

is thrown from within the 'each' closure in the main class:

`@SpringBootApplication`
class ClosuresApplication implements CommandLineRunner {

    @Autowired
    private Bank bank
    
    @Override
    void run(String... args) throws Exception {
        for (def i = 0; i < 10; i++) {
            printf 'Bank %02d: %s%n', (i + 1), bank
        }
    
        (1..10).each {
            printf 'Bank %02d: %s%n', it, bank
        }
    }
    
    static void main(String[] args) {
        SpringApplication.run ClosuresApplication, args
    }

}


---

**Affects:** 4.1.6

**Reference URL:** http://stackoverflow.com/questions/29920281/exception-accessing-spring-managed-bean-from-groovy-closure
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17618","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17618/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17618/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17618/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17618","id":398179652,"node_id":"MDU6SXNzdWUzOTgxNzk2NTI=","number":17618,"title":"Spring bean not autowiring when using Groovy closures [SPR-13027]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-05-14T01:04:16Z","updated_at":"2019-01-12T00:16:30Z","closed_at":"2019-01-12T00:16:30Z","author_association":"COLLABORATOR","body":"**[Mark Manders](https://jira.spring.io/secure/ViewProfile.jspa?name=mmjmanders)** opened **[SPR-13027](https://jira.spring.io/browse/SPR-13027?redirect=false)** and commented\n\nWhen using the Spring framework in combination with the Groovy language, it is impossible to access `@Autowired` beans from a Groovy closure.\n\nException in thread \"main\" java.lang.IllegalStateException: Failed to execute CommandLineRunner\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callGroovyObjectGetProperty(AbstractCallSite.java:304)\nat org.springframework.boot.SpringApplication.runCommandLineRunners(SpringApplication.java:675)\nat com.example.closures.ClosuresApplication$_run_closure1.doCall(ClosuresApplication.groovy:22)\nat org.springframework.boot.SpringApplication.afterRefresh(SpringApplication.java:690)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat org.springframework.boot.SpringApplication.run(SpringApplication.java:321)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\nat org.springframework.boot.SpringApplication.run(SpringApplication.java:957)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\nat org.springframework.boot.SpringApplication.run(SpringApplication.java:946)\nat java.lang.reflect.Method.invoke(Method.java:497)\nat org.springframework.boot.SpringApplication$run.call(Unknown Source)\nat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:90)\nat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:45)\nat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:324)\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:110)\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:130)\nat org.codehaus.groovy.runtime.metaclass.ClosureMetaClass.invokeMethod(ClosureMetaClass.java:292)\nat com.example.closures.ClosuresApplication.main(ClosuresApplication.groovy:27)\nat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1016)\nCaused by: groovy.lang.MissingPropertyException: No such property: bank for class: com.example.closures.ClosuresApplication$$EnhancerBySpringCGLIB$$44735576\nat groovy.lang.Closure.call(Closure.java:423)\nat groovy.lang.Closure.call(Closure.java:439)\nat org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2027)\nat org.codehaus.groovy.runtime.ScriptBytecodeAdapter.unwrap(ScriptBytecodeAdapter.java:51)\nat org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2012)\nat org.codehaus.groovy.runtime.callsite.PogoGetPropertySite.getProperty(PogoGetPropertySite.java:49)\nat org.codehaus.groovy.runtime.DefaultGroovyMethods.each(DefaultGroovyMethods.java:2053)\nat org.codehaus.groovy.runtime.dgm$162.invoke(Unknown Source)\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callGroovyObjectGetProperty(AbstractCallSite.java:304)\nat org.codehaus.groovy.runtime.callsite.PojoMetaMethodSite$PojoMetaMethodSiteNoUnwrapNoCoerce.invoke(PojoMetaMethodSite.java:271)\nat org.codehaus.groovy.runtime.callsite.PojoMetaMethodSite.call(PojoMetaMethodSite.java:53)\nat com.example.closures.ClosuresApplication$_run_closure1.doCall(ClosuresApplication.groovy:22)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:45)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:110)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\nat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:122)\nat java.lang.reflect.Method.invoke(Method.java:497)\nat com.example.closures.ClosuresApplication.run(ClosuresApplication.groovy:21)\nat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:90)\nat org.springframework.boot.SpringApplication.runCommandLineRunners(SpringApplication.java:672)\n... 9 common frames omitted\n\nis thrown from within the 'each' closure in the main class:\n\n`@SpringBootApplication`\nclass ClosuresApplication implements CommandLineRunner {\n\n    @Autowired\n    private Bank bank\n    \n    @Override\n    void run(String... args) throws Exception {\n        for (def i = 0; i < 10; i++) {\n            printf 'Bank %02d: %s%n', (i + 1), bank\n        }\n    \n        (1..10).each {\n            printf 'Bank %02d: %s%n', it, bank\n        }\n    }\n    \n    static void main(String[] args) {\n        SpringApplication.run ClosuresApplication, args\n    }\n\n}\n\n\n---\n\n**Affects:** 4.1.6\n\n**Reference URL:** http://stackoverflow.com/questions/29920281/exception-accessing-spring-managed-bean-from-groovy-closure\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453698519"], "labels":["in: core","status: bulk-closed"]}