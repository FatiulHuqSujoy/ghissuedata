{"id":"6673", "title":"Aspects can not have a proxied property [SPR-1980]", "body":"**[Luc Dew](https://jira.spring.io/secure/ViewProfile.jspa?name=bengali)** opened **[SPR-1980](https://jira.spring.io/browse/SPR-1980?redirect=false)** and commented

Hi,
I think I found a bug with Spring's aspects.
Actually I wanted to inject a HibernateDAO in a Spring aspect.
My HibernateDAO implements a JobDAO interface.
The hibernateDAO also extends HibernateDaoSupport and I made it transactional
with the configuration below. The problem is that if the DAO is used a property
in an aspect, it looses its transactional \"aspect\". Below, if i comment out
the section with beforeAdviceJobAuth bean and jobAuthorizationAspect,
my DAO is transactional, Hibernate session is flushed, transaction is committed.
If I uncomment the declarations of the beans (beforeAdviceJobAuth bean and jobAuthorizationAspect)
like below, the DAO is not transactional anymore.
Hibernate session is flushed when a method of the HibernateDAO ends but transaction
is never commited (or rollbacked).
I think it is linked to the fact the DAO is injected in a Spring aspect and therefore proxied and
I don't know why but it looses its transaction behaviour.

    ...
    <bean id=\"hibernatebaseDao\" abstract=\"true\">
        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>
    </bean>
    
    <bean id=\"jobDAO\" parent=\"hibernatebaseDao\" class=\"com.fis.integ2.dao.hibernate.HibernateJobDAO\"/>
    
    <aop:config>
    	<aop:advisor pointcut=\"execution(* *..JobDAO.*(..))\"
               advice-ref=\"txAdvice\"/>
    </aop:config>
    
    <tx:advice id=\"txAdvice\" transaction-manager=\"transactionManager\">
    	<tx:attributes>
        	<tx:method name=\"insert*\"/>
        	<tx:method name=\"update*\"/>
        	<tx:method name=\"delete*\"/>
         	<tx:method name=\"*\" read-only=\"true\"/>
     	</tx:attributes>		
    </tx:advice>
    
        <!-- If commented out below the DAO works -->
    <aop:config proxy-target-class=\"true\">
    	  <aop:aspect id=\"beforeAdviceJobAuth\" ref=\"jobAuthorizationAspect\">
    	  	<aop:advice 
    			kind=\"before\"
    			method=\"checkForAuthorization\" arg-names=\"jobid\"
    			pointcut=\"(execution(* com.fis.integ2.jobqueue.JobExecutorService.stopJob(..)) and args (jobid)) || ( execution(* com.fis.integ2.jobqueue.JobExecutorService.removeJob(..)) and args(jobid)) \"/>
    	  </aop:aspect>
    </aop:config>
    
    
    <bean id=\"jobAuthorizationAspect\" class=\"com.fis.integ2.security.JobAuthorizationAspect\">
    	<property name=\"jobDAO\">
    		<ref bean=\"jobDAO\"/>
    	</property>
    </bean>



---

**Affects:** 2.0 M4

**Attachments:**
- [springaop.zip](https://jira.spring.io/secure/attachment/11631/springaop.zip) (_14.54 kB_)
- [springaop.zip](https://jira.spring.io/secure/attachment/11620/springaop.zip) (_10.35 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6673","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6673/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6673/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6673/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6673","id":398065773,"node_id":"MDU6SXNzdWUzOTgwNjU3NzM=","number":6673,"title":"Aspects can not have a proxied property [SPR-1980]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false,"description":"A general bug"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/33","html_url":"https://github.com/spring-projects/spring-framework/milestone/33","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/33/labels","id":3960804,"node_id":"MDk6TWlsZXN0b25lMzk2MDgwNA==","number":33,"title":"2.0 RC2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":58,"state":"closed","created_at":"2019-01-10T22:02:31Z","updated_at":"2019-01-10T23:53:16Z","due_on":null,"closed_at":"2019-01-10T22:02:31Z"},"comments":10,"created_at":"2006-05-03T19:43:36Z","updated_at":"2012-06-19T03:52:25Z","closed_at":"2012-06-19T03:52:25Z","author_association":"COLLABORATOR","body":"**[Luc Dew](https://jira.spring.io/secure/ViewProfile.jspa?name=bengali)** opened **[SPR-1980](https://jira.spring.io/browse/SPR-1980?redirect=false)** and commented\n\nHi,\nI think I found a bug with Spring's aspects.\nActually I wanted to inject a HibernateDAO in a Spring aspect.\nMy HibernateDAO implements a JobDAO interface.\nThe hibernateDAO also extends HibernateDaoSupport and I made it transactional\nwith the configuration below. The problem is that if the DAO is used a property\nin an aspect, it looses its transactional \"aspect\". Below, if i comment out\nthe section with beforeAdviceJobAuth bean and jobAuthorizationAspect,\nmy DAO is transactional, Hibernate session is flushed, transaction is committed.\nIf I uncomment the declarations of the beans (beforeAdviceJobAuth bean and jobAuthorizationAspect)\nlike below, the DAO is not transactional anymore.\nHibernate session is flushed when a method of the HibernateDAO ends but transaction\nis never commited (or rollbacked).\nI think it is linked to the fact the DAO is injected in a Spring aspect and therefore proxied and\nI don't know why but it looses its transaction behaviour.\n\n    ...\n    <bean id=\"hibernatebaseDao\" abstract=\"true\">\n        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>\n    </bean>\n    \n    <bean id=\"jobDAO\" parent=\"hibernatebaseDao\" class=\"com.fis.integ2.dao.hibernate.HibernateJobDAO\"/>\n    \n    <aop:config>\n    \t<aop:advisor pointcut=\"execution(* *..JobDAO.*(..))\"\n               advice-ref=\"txAdvice\"/>\n    </aop:config>\n    \n    <tx:advice id=\"txAdvice\" transaction-manager=\"transactionManager\">\n    \t<tx:attributes>\n        \t<tx:method name=\"insert*\"/>\n        \t<tx:method name=\"update*\"/>\n        \t<tx:method name=\"delete*\"/>\n         \t<tx:method name=\"*\" read-only=\"true\"/>\n     \t</tx:attributes>\t\t\n    </tx:advice>\n    \n        <!-- If commented out below the DAO works -->\n    <aop:config proxy-target-class=\"true\">\n    \t  <aop:aspect id=\"beforeAdviceJobAuth\" ref=\"jobAuthorizationAspect\">\n    \t  \t<aop:advice \n    \t\t\tkind=\"before\"\n    \t\t\tmethod=\"checkForAuthorization\" arg-names=\"jobid\"\n    \t\t\tpointcut=\"(execution(* com.fis.integ2.jobqueue.JobExecutorService.stopJob(..)) and args (jobid)) || ( execution(* com.fis.integ2.jobqueue.JobExecutorService.removeJob(..)) and args(jobid)) \"/>\n    \t  </aop:aspect>\n    </aop:config>\n    \n    \n    <bean id=\"jobAuthorizationAspect\" class=\"com.fis.integ2.security.JobAuthorizationAspect\">\n    \t<property name=\"jobDAO\">\n    \t\t<ref bean=\"jobDAO\"/>\n    \t</property>\n    </bean>\n\n\n\n---\n\n**Affects:** 2.0 M4\n\n**Attachments:**\n- [springaop.zip](https://jira.spring.io/secure/attachment/11631/springaop.zip) (_14.54 kB_)\n- [springaop.zip](https://jira.spring.io/secure/attachment/11620/springaop.zip) (_10.35 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453304347","453304348","453304349","453304351","453304353","453304354","453304355","453304357","453304358","453304360"], "labels":["in: core","type: bug"]}