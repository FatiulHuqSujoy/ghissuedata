{"id":"20551", "title":"UriComponentsBuilder inconsistent encode/decode query params behavior [SPR-16002]", "body":"**[Vsevolod Kalinin](https://jira.spring.io/secure/ViewProfile.jspa?name=flash)** opened **[SPR-16002](https://jira.spring.io/browse/SPR-16002?redirect=false)** and commented

After working with that for quite some time I have a strong feeling that something is wrong with `UriComponentsBuilder` query params encoding/decoding. Will try to explain it step by step now + my vision to the problem (still with hope of doing something wrong/missing something :)). Will use _fromHttpUrl(..)_ in examples, but the behavior is the same for all _from*(..)_ methods of all `*UriComponentsBuilder` classes (and it hurt even more when using something like _fromHttpRequest(..)_).

## Use cases

So, consider I want to take current request URL, modify it (add/remove query params, etc.) and eventually use it somewhere else. Again there are _fromHttpUrl(..)_ calls below for simplicity; IRL they are _fromHttpRequest(..)_, etc.

1. h3. Double-encoded query params from original URL

```java
UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom%26Jerry\");
ucb.queryParam(\"album\", \"S&M\");
ucb.toUriString(); // http://host.com?name=Tom%2526Jerry&album=S%26M
```

It returns `http://host.com?name=Tom%2526Jerry&album=S%26M` which is wrong (see double-encoded initial _name_ param).
I was only able to solve the above with something like:

```java
UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom%26Jerry\");
ucb.queryParam(\"album\", java.net.URLEncoder.encode(\"S&M\"));
ucb.build(true).toUriString();
```

Now it returns the correct `http://host.com?name=Tom%26Jerry&album=S%26M`, but the whole thing got a bit ugly.
1. h3. application/x-www-form-urlencoded whitespaces
   Now the request has application/x-www-form-urlencoded whitespace (converted to +) and taught by the bitter experience above we use `ucb.build(true).toUriString()`:

```java
UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom+Jerry\");
ucb.queryParam(\"album\", \"S&M\");
ucb.build(true).toUriString();
```

But it's _java.lang.IllegalArgumentException_ now - _UriComponentsBuilder_ doesn't like \"+\".

Reverting to the initial approach doesn't work either:

```java
UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom+Jerry\");
ucb.queryParam(\"album\", \"S&M\");
ucb.toUriString();
```

results in `http://host.com?name=Tom%2BJerry&album=S%26M` - note \"+\" being encoded now.

The only option is to do an ugly workaround mentioned in #14805.

## Probable solution

IMHO it would have been much more convenient if `UriComponentsBuilder` decoded query params while parsing (presumably with application/x-www-form-urlencoded support; at least when creating from a request where MIME type should be available). This way both cases above are solved + a test

```java
final String requestHttpUrl = <any valid request URL>;
final String processedHttpUrl = UriComponentsBuilder.fromHttpUrl(requestHttpUrl).toUriString();
assertEquals(processedHttpUrl, requestHttpUrl);
```

will pass for any valid URL which is what I'd expect. Although backward compatibility concern won't make it happen easily...

Looking forward for help with this one :D

---

**Affects:** 4.3.11

**Issue Links:**
- #19394 UriComponentBuilder doesn't work with encoded HTTP URL having '+'. (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20551","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20551/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20551/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20551/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20551","id":398215928,"node_id":"MDU6SXNzdWUzOTgyMTU5Mjg=","number":20551,"title":"UriComponentsBuilder inconsistent encode/decode query params behavior [SPR-16002]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2017-09-25T14:22:32Z","updated_at":"2019-01-12T16:44:53Z","closed_at":"2018-05-24T09:13:06Z","author_association":"COLLABORATOR","body":"**[Vsevolod Kalinin](https://jira.spring.io/secure/ViewProfile.jspa?name=flash)** opened **[SPR-16002](https://jira.spring.io/browse/SPR-16002?redirect=false)** and commented\n\nAfter working with that for quite some time I have a strong feeling that something is wrong with `UriComponentsBuilder` query params encoding/decoding. Will try to explain it step by step now + my vision to the problem (still with hope of doing something wrong/missing something :)). Will use _fromHttpUrl(..)_ in examples, but the behavior is the same for all _from*(..)_ methods of all `*UriComponentsBuilder` classes (and it hurt even more when using something like _fromHttpRequest(..)_).\n\n## Use cases\n\nSo, consider I want to take current request URL, modify it (add/remove query params, etc.) and eventually use it somewhere else. Again there are _fromHttpUrl(..)_ calls below for simplicity; IRL they are _fromHttpRequest(..)_, etc.\n\n1. h3. Double-encoded query params from original URL\n\n```java\nUriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom%26Jerry\");\nucb.queryParam(\"album\", \"S&M\");\nucb.toUriString(); // http://host.com?name=Tom%2526Jerry&album=S%26M\n```\n\nIt returns `http://host.com?name=Tom%2526Jerry&album=S%26M` which is wrong (see double-encoded initial _name_ param).\nI was only able to solve the above with something like:\n\n```java\nUriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom%26Jerry\");\nucb.queryParam(\"album\", java.net.URLEncoder.encode(\"S&M\"));\nucb.build(true).toUriString();\n```\n\nNow it returns the correct `http://host.com?name=Tom%26Jerry&album=S%26M`, but the whole thing got a bit ugly.\n1. h3. application/x-www-form-urlencoded whitespaces\n   Now the request has application/x-www-form-urlencoded whitespace (converted to +) and taught by the bitter experience above we use `ucb.build(true).toUriString()`:\n\n```java\nUriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom+Jerry\");\nucb.queryParam(\"album\", \"S&M\");\nucb.build(true).toUriString();\n```\n\nBut it's _java.lang.IllegalArgumentException_ now - _UriComponentsBuilder_ doesn't like \"+\".\n\nReverting to the initial approach doesn't work either:\n\n```java\nUriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(\"http://host.com?name=Tom+Jerry\");\nucb.queryParam(\"album\", \"S&M\");\nucb.toUriString();\n```\n\nresults in `http://host.com?name=Tom%2BJerry&album=S%26M` - note \"+\" being encoded now.\n\nThe only option is to do an ugly workaround mentioned in #14805.\n\n## Probable solution\n\nIMHO it would have been much more convenient if `UriComponentsBuilder` decoded query params while parsing (presumably with application/x-www-form-urlencoded support; at least when creating from a request where MIME type should be available). This way both cases above are solved + a test\n\n```java\nfinal String requestHttpUrl = <any valid request URL>;\nfinal String processedHttpUrl = UriComponentsBuilder.fromHttpUrl(requestHttpUrl).toUriString();\nassertEquals(processedHttpUrl, requestHttpUrl);\n```\n\nwill pass for any valid URL which is what I'd expect. Although backward compatibility concern won't make it happen easily...\n\nLooking forward for help with this one :D\n\n---\n\n**Affects:** 4.3.11\n\n**Issue Links:**\n- #19394 UriComponentBuilder doesn't work with encoded HTTP URL having '+'. (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453461352","453461354","453461358"], "labels":["in: web","status: duplicate"]}