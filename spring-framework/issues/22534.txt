{"id":"22534", "title":"The afterHandshake method of WebSocketHandler  does not work", "body":"spring-websocket-4.3.17.RELEASE

I implemented a `HandshakeInterceptor` to authenticate the user but I found that its `afterHandshake` method doesn't work, whether the `beforeHandshake` method returns `false` or throws an exception. So what is the use of this method?

The code of `HandshakeInterceptorChain` as follow:

```java
public class HandshakeInterceptorChain {

	private static final Log logger = LogFactory.getLog(HandshakeInterceptorChain.class);

	private final List<HandshakeInterceptor> interceptors;

	private final WebSocketHandler 

	private int interceptorIndex = -1;


	public HandshakeInterceptorChain(List<HandshakeInterceptor> interceptors, WebSocketHandler wsHandler) {
		this.interceptors = (interceptors != null ? interceptors : Collections.<HandshakeInterceptor>emptyList());
		this.wsHandler = wsHandler;
	}


	public boolean applyBeforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
			Map<String, Object> attributes) throws Exception {

		for (int i = 0; i < this.interceptors.size(); i++) {
			HandshakeInterceptor interceptor = this.interceptors.get(i);
			if (!interceptor.beforeHandshake(request, response, this.wsHandler, attributes)) {
				if (logger.isDebugEnabled()) {
					logger.debug(interceptor + \" returns false from beforeHandshake - precluding handshake\");
				}
				applyAfterHandshake(request, response, null);
				return false;
			}
			this.interceptorIndex = i;
		}
		return true;
	}


	public void applyAfterHandshake(ServerHttpRequest request, ServerHttpResponse response, Exception failure) {
		for (int i = this.interceptorIndex; i >= 0; i--) {
			HandshakeInterceptor interceptor = this.interceptors.get(i);
			try {
				interceptor.afterHandshake(request, response, this.wsHandler, failure);
			}
			catch (Throwable ex) {
				if (logger.isWarnEnabled()) {
					logger.warn(interceptor + \" threw exception in afterHandshake: \" + ex);
				}
			}
		}
	}

}
```

The code of `WebSocketHttpRequestHandler` as follow:

```java
	@Override
	public void handleRequest(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws ServletException, IOException {

		ServerHttpRequest request = new ServletServerHttpRequest(servletRequest);
		ServerHttpResponse response = new ServletServerHttpResponse(servletResponse);

		HandshakeInterceptorChain chain = new HandshakeInterceptorChain(this.interceptors, this.wsHandler);
		HandshakeFailureException failure = null;

		try {
			if (logger.isDebugEnabled()) {
				logger.debug(servletRequest.getMethod() + \" \" + servletRequest.getRequestURI());
			}
			Map<String, Object> attributes = new HashMap<String, Object>();
			if (!chain.applyBeforeHandshake(request, response, attributes)) {
				return;
			}
			this.handshakeHandler.doHandshake(request, response, this.wsHandler, attributes);
			chain.applyAfterHandshake(request, response, null);
			response.close();
		}
		catch (HandshakeFailureException ex) {
			failure = ex;
		}
		catch (Throwable ex) {
			failure = new HandshakeFailureException(\"Uncaught failure for request \" + request.getURI(), ex);
		}
		finally {
			if (failure != null) {
				chain.applyAfterHandshake(request, response, failure);
				throw failure;
			}
		}
	}
```", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22534","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22534/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22534/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22534/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22534","id":418106916,"node_id":"MDU6SXNzdWU0MTgxMDY5MTY=","number":22534,"title":"The afterHandshake method of WebSocketHandler  does not work","user":{"login":"lzz666","id":28105288,"node_id":"MDQ6VXNlcjI4MTA1Mjg4","avatar_url":"https://avatars1.githubusercontent.com/u/28105288?v=4","gravatar_id":"","url":"https://api.github.com/users/lzz666","html_url":"https://github.com/lzz666","followers_url":"https://api.github.com/users/lzz666/followers","following_url":"https://api.github.com/users/lzz666/following{/other_user}","gists_url":"https://api.github.com/users/lzz666/gists{/gist_id}","starred_url":"https://api.github.com/users/lzz666/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/lzz666/subscriptions","organizations_url":"https://api.github.com/users/lzz666/orgs","repos_url":"https://api.github.com/users/lzz666/repos","events_url":"https://api.github.com/users/lzz666/events{/privacy}","received_events_url":"https://api.github.com/users/lzz666/received_events","type":"User","site_admin":false},"labels":[{"id":1189480587,"node_id":"MDU6TGFiZWwxMTg5NDgwNTg3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/for:%20stackoverflow","name":"for: stackoverflow","color":"c5def5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-03-07T03:03:31Z","updated_at":"2019-05-08T18:24:03Z","closed_at":"2019-05-08T18:23:29Z","author_association":"NONE","body":"spring-websocket-4.3.17.RELEASE\r\n\r\nI implemented a `HandshakeInterceptor` to authenticate the user but I found that its `afterHandshake` method doesn't work, whether the `beforeHandshake` method returns `false` or throws an exception. So what is the use of this method?\r\n\r\nThe code of `HandshakeInterceptorChain` as follow:\r\n\r\n```java\r\npublic class HandshakeInterceptorChain {\r\n\r\n\tprivate static final Log logger = LogFactory.getLog(HandshakeInterceptorChain.class);\r\n\r\n\tprivate final List<HandshakeInterceptor> interceptors;\r\n\r\n\tprivate final WebSocketHandler \r\n\r\n\tprivate int interceptorIndex = -1;\r\n\r\n\r\n\tpublic HandshakeInterceptorChain(List<HandshakeInterceptor> interceptors, WebSocketHandler wsHandler) {\r\n\t\tthis.interceptors = (interceptors != null ? interceptors : Collections.<HandshakeInterceptor>emptyList());\r\n\t\tthis.wsHandler = wsHandler;\r\n\t}\r\n\r\n\r\n\tpublic boolean applyBeforeHandshake(ServerHttpRequest request, ServerHttpResponse response,\r\n\t\t\tMap<String, Object> attributes) throws Exception {\r\n\r\n\t\tfor (int i = 0; i < this.interceptors.size(); i++) {\r\n\t\t\tHandshakeInterceptor interceptor = this.interceptors.get(i);\r\n\t\t\tif (!interceptor.beforeHandshake(request, response, this.wsHandler, attributes)) {\r\n\t\t\t\tif (logger.isDebugEnabled()) {\r\n\t\t\t\t\tlogger.debug(interceptor + \" returns false from beforeHandshake - precluding handshake\");\r\n\t\t\t\t}\r\n\t\t\t\tapplyAfterHandshake(request, response, null);\r\n\t\t\t\treturn false;\r\n\t\t\t}\r\n\t\t\tthis.interceptorIndex = i;\r\n\t\t}\r\n\t\treturn true;\r\n\t}\r\n\r\n\r\n\tpublic void applyAfterHandshake(ServerHttpRequest request, ServerHttpResponse response, Exception failure) {\r\n\t\tfor (int i = this.interceptorIndex; i >= 0; i--) {\r\n\t\t\tHandshakeInterceptor interceptor = this.interceptors.get(i);\r\n\t\t\ttry {\r\n\t\t\t\tinterceptor.afterHandshake(request, response, this.wsHandler, failure);\r\n\t\t\t}\r\n\t\t\tcatch (Throwable ex) {\r\n\t\t\t\tif (logger.isWarnEnabled()) {\r\n\t\t\t\t\tlogger.warn(interceptor + \" threw exception in afterHandshake: \" + ex);\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n\r\n}\r\n```\r\n\r\nThe code of `WebSocketHttpRequestHandler` as follow:\r\n\r\n```java\r\n\t@Override\r\n\tpublic void handleRequest(HttpServletRequest servletRequest, HttpServletResponse servletResponse)\r\n\t\t\tthrows ServletException, IOException {\r\n\r\n\t\tServerHttpRequest request = new ServletServerHttpRequest(servletRequest);\r\n\t\tServerHttpResponse response = new ServletServerHttpResponse(servletResponse);\r\n\r\n\t\tHandshakeInterceptorChain chain = new HandshakeInterceptorChain(this.interceptors, this.wsHandler);\r\n\t\tHandshakeFailureException failure = null;\r\n\r\n\t\ttry {\r\n\t\t\tif (logger.isDebugEnabled()) {\r\n\t\t\t\tlogger.debug(servletRequest.getMethod() + \" \" + servletRequest.getRequestURI());\r\n\t\t\t}\r\n\t\t\tMap<String, Object> attributes = new HashMap<String, Object>();\r\n\t\t\tif (!chain.applyBeforeHandshake(request, response, attributes)) {\r\n\t\t\t\treturn;\r\n\t\t\t}\r\n\t\t\tthis.handshakeHandler.doHandshake(request, response, this.wsHandler, attributes);\r\n\t\t\tchain.applyAfterHandshake(request, response, null);\r\n\t\t\tresponse.close();\r\n\t\t}\r\n\t\tcatch (HandshakeFailureException ex) {\r\n\t\t\tfailure = ex;\r\n\t\t}\r\n\t\tcatch (Throwable ex) {\r\n\t\t\tfailure = new HandshakeFailureException(\"Uncaught failure for request \" + request.getURI(), ex);\r\n\t\t}\r\n\t\tfinally {\r\n\t\t\tif (failure != null) {\r\n\t\t\t\tchain.applyAfterHandshake(request, response, failure);\r\n\t\t\t\tthrow failure;\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n```","closed_by":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}}", "commentIds":["490596861","490596949"], "labels":["for: stackoverflow"]}