{"id":"21516", "title":"CachedIntrospectionResults should build complete descriptor for setter/getter across interface hierarchy [SPR-16978]", "body":"**[Petar Tahchiev](https://jira.spring.io/secure/ViewProfile.jspa?name=ptahchiev)** opened **[SPR-16978](https://jira.spring.io/browse/SPR-16978?redirect=false)** and commented

Hello,
consider the following setup:

```
// code from spring-security
public interface UserDetails extends Serializable {
  Collection<? extends GrantedAuthority> getAuthorities();

  String getPassword();

  String getUsername();

  boolean isAccountNonExpired();

  boolean isAccountNonLocked();

  boolean isCredentialsNonExpired();

  boolean isEnabled();
}

public interface MyUserDetails extends UserDetails {
    Locale getLocale();

    void setLocale(final Locale locale);

    /* setters for super properties */

    void setUsername(String username);

    void setPassword(String password);
}

public interface CustomerDetails extends MyUsersDetails {
  String getPhone();

  void setPhone(String phone);
}
```

Now invoke ` BeanUtils.getPropertyDescriptor(CustomerDetails.class, \"username\");` and you will see that the returned `PropertyDescriptor` has correct `writeMethod` but the `readMethod` is null. As a result, if I try to submit a `CustomerDetails` form in spring-mvc
I get this error:

```
2018-06-26 18:13:33,604 org.apache.catalina.core.ContainerBase.[Tomcat].[localhost].[/storefront].[dispatcherServlet] [https-jsse-nio-127.0.0.1-8112-exec-5] ERROR: Servlet.service() for servlet [dispatcherServlet] in context with path [/storefront] threw exception [Request processing failed; nested exception is java.lang.IllegalArgumentException: Method must not be null] with root cause
java.lang.IllegalArgumentException: Method must not be null
	at org.springframework.util.Assert.notNull(Assert.java:193)
	at org.springframework.core.MethodParameter.<init>(MethodParameter.java:120)
	at org.springframework.core.MethodParameter.<init>(MethodParameter.java:106)
	at org.springframework.data.web.MapDataBinder$MapPropertyAccessor.setPropertyValue(MapDataBinder.java:193)
	at org.springframework.beans.AbstractPropertyAccessor.setPropertyValue(AbstractPropertyAccessor.java:67)
	at org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:97)
	at org.springframework.validation.DataBinder.applyPropertyValues(DataBinder.java:839)
	at org.springframework.validation.DataBinder.doBind(DataBinder.java:735)
	at org.springframework.web.bind.WebDataBinder.doBind(WebDataBinder.java:197)
	at org.springframework.validation.DataBinder.bind(DataBinder.java:720
```

I believe this is because in `CachedIntrospectionResults` a look goes through all super interfaces:

```
while (clazz != null && clazz != Object.class) {
     Class<?>[] ifcs = clazz.getInterfaces();
     for (Class<?> ifc : ifcs) {
              if (!ClassUtils.isJavaLanguageInterface(ifc)) {
                       for (PropertyDescriptor pd : getBeanInfo(ifc).getPropertyDescriptors()) {
```

until `Object` is reached and for each finds the property descriptor. However when it reaches `MyUserDetails` it finds the setter method and creates a `PropertyDescriptor` with null read method which is wrong because the read method is defined in the parent interface.

---

**Affects:** 5.0.7

**Issue Links:**
- #18772 Java 8 default methods not detected as bean properties
- #20869 CachedIntrospectionResults should use BeanInfoFactory when introspecting implemented interfaces
- #21110 Consider caching interface-derived BeanInfo instances in CachedIntrospectionResults

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/bf5fe46fa912eb86ad6fb340d3e56cacda568865
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21516","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21516/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21516/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21516/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21516","id":398229923,"node_id":"MDU6SXNzdWUzOTgyMjk5MjM=","number":21516,"title":"CachedIntrospectionResults should build complete descriptor for setter/getter across interface hierarchy [SPR-16978]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/194","html_url":"https://github.com/spring-projects/spring-framework/milestone/194","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/194/labels","id":3960967,"node_id":"MDk6TWlsZXN0b25lMzk2MDk2Nw==","number":194,"title":"5.1 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":144,"state":"closed","created_at":"2019-01-10T22:05:44Z","updated_at":"2019-01-11T10:39:45Z","due_on":"2018-07-25T07:00:00Z","closed_at":"2019-01-10T22:05:44Z"},"comments":0,"created_at":"2018-06-26T15:28:49Z","updated_at":"2019-01-11T16:09:28Z","closed_at":"2018-07-26T08:09:30Z","author_association":"COLLABORATOR","body":"**[Petar Tahchiev](https://jira.spring.io/secure/ViewProfile.jspa?name=ptahchiev)** opened **[SPR-16978](https://jira.spring.io/browse/SPR-16978?redirect=false)** and commented\n\nHello,\nconsider the following setup:\n\n```\n// code from spring-security\npublic interface UserDetails extends Serializable {\n  Collection<? extends GrantedAuthority> getAuthorities();\n\n  String getPassword();\n\n  String getUsername();\n\n  boolean isAccountNonExpired();\n\n  boolean isAccountNonLocked();\n\n  boolean isCredentialsNonExpired();\n\n  boolean isEnabled();\n}\n\npublic interface MyUserDetails extends UserDetails {\n    Locale getLocale();\n\n    void setLocale(final Locale locale);\n\n    /* setters for super properties */\n\n    void setUsername(String username);\n\n    void setPassword(String password);\n}\n\npublic interface CustomerDetails extends MyUsersDetails {\n  String getPhone();\n\n  void setPhone(String phone);\n}\n```\n\nNow invoke ` BeanUtils.getPropertyDescriptor(CustomerDetails.class, \"username\");` and you will see that the returned `PropertyDescriptor` has correct `writeMethod` but the `readMethod` is null. As a result, if I try to submit a `CustomerDetails` form in spring-mvc\nI get this error:\n\n```\n2018-06-26 18:13:33,604 org.apache.catalina.core.ContainerBase.[Tomcat].[localhost].[/storefront].[dispatcherServlet] [https-jsse-nio-127.0.0.1-8112-exec-5] ERROR: Servlet.service() for servlet [dispatcherServlet] in context with path [/storefront] threw exception [Request processing failed; nested exception is java.lang.IllegalArgumentException: Method must not be null] with root cause\njava.lang.IllegalArgumentException: Method must not be null\n\tat org.springframework.util.Assert.notNull(Assert.java:193)\n\tat org.springframework.core.MethodParameter.<init>(MethodParameter.java:120)\n\tat org.springframework.core.MethodParameter.<init>(MethodParameter.java:106)\n\tat org.springframework.data.web.MapDataBinder$MapPropertyAccessor.setPropertyValue(MapDataBinder.java:193)\n\tat org.springframework.beans.AbstractPropertyAccessor.setPropertyValue(AbstractPropertyAccessor.java:67)\n\tat org.springframework.beans.AbstractPropertyAccessor.setPropertyValues(AbstractPropertyAccessor.java:97)\n\tat org.springframework.validation.DataBinder.applyPropertyValues(DataBinder.java:839)\n\tat org.springframework.validation.DataBinder.doBind(DataBinder.java:735)\n\tat org.springframework.web.bind.WebDataBinder.doBind(WebDataBinder.java:197)\n\tat org.springframework.validation.DataBinder.bind(DataBinder.java:720\n```\n\nI believe this is because in `CachedIntrospectionResults` a look goes through all super interfaces:\n\n```\nwhile (clazz != null && clazz != Object.class) {\n     Class<?>[] ifcs = clazz.getInterfaces();\n     for (Class<?> ifc : ifcs) {\n              if (!ClassUtils.isJavaLanguageInterface(ifc)) {\n                       for (PropertyDescriptor pd : getBeanInfo(ifc).getPropertyDescriptors()) {\n```\n\nuntil `Object` is reached and for each finds the property descriptor. However when it reaches `MyUserDetails` it finds the setter method and creates a `PropertyDescriptor` with null read method which is wrong because the read method is defined in the parent interface.\n\n---\n\n**Affects:** 5.0.7\n\n**Issue Links:**\n- #18772 Java 8 default methods not detected as bean properties\n- #20869 CachedIntrospectionResults should use BeanInfoFactory when introspecting implemented interfaces\n- #21110 Consider caching interface-derived BeanInfo instances in CachedIntrospectionResults\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/bf5fe46fa912eb86ad6fb340d3e56cacda568865\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["in: core","type: enhancement"]}