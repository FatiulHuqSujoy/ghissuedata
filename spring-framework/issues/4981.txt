{"id":"4981", "title":"Quartz: Setting flag to overwrite existing jobs doesn't update triggers [SPR-250]", "body":"**[Bryan Keller](https://jira.spring.io/secure/ViewProfile.jspa?name=bryanck)** opened **[SPR-250](https://jira.spring.io/browse/SPR-250?redirect=false)** and commented

This applies to the Quartz scheduler support.

When setting the overwriteExistingJobs flag on the org.springframework.scheduling.quartz.SchedulerFactoryBean, any triggers that have been updated since the last startup time will not be overwritten. If you change the trigger's criteria, and are persisting the schedules via the dataSource property, the trigger will not be updated when your server is restarted. Te old trigger criteria remain in effect.

An example:

Create a persistence schedule using something similar to the following applicationContext.xml snippet (see bottom). Now stop your server, change the trigger params (e.g. cronExpression), and restart the server. The original trigger's cronExpression remains in effect - the new values are not used.

Solution:

Call scheduler.rescheduleJob(trigger) to reschedule the trigger on startup.

From SchedulerFactoryBean.java.registerJobsAndTriggers()

    // register Triggers
    if (this.triggers != null) {
         for (Iterator it = this.triggers.iterator(); it.hasNext();) {
              Trigger trigger = (Trigger) it.next();
              if (this.scheduler.getTrigger(trigger.getName(), trigger.getGroup()) == null) {
                       // check if the Trigger is aware of an associated JobDetail
                       if (trigger instanceof JobDetailAwareTrigger) {
                                JobDetail jobDetail = ((JobDetailAwareTrigger) trigger).getJobDetail();
                                // automatically register the JobDetail too
                                if (!this.jobDetails.contains(jobDetail) && addJobToScheduler(jobDetail)) {
                                         this.jobDetails.add(jobDetail);
                                }
                       }
                       this.scheduler.scheduleJob(trigger);
              } else if (this.overwriteExistingJobs) {
                       this.scheduler.rescheduleJob(
                           trigger.getName(), trigger.getGroup(), trigger
                       );
              }
         }
    }

snippet from applicationContext.xml:

    <!-- Quartz Scheduler, with pre-registered triggers -->
    <!-- Will automatically start scheduling on context startup -->
    <bean id=\"scheduler\" class=\"org.springframework.scheduling.quartz.SchedulerFactoryBean\">
    	<property name=\"dataSource\"><ref bean=\"dataSource\"/></property>
    	<property name=\"overwriteExistingJobs\"><value>true</value></property>
    	<property name=\"applicationContextSchedulerContextKey\"><value>appContext</value></property>
    	<property name=\"triggers\">
    		<list>
    			<ref local=\"messageCleanupTrigger\"/>
    		</list>
    	</property>
    </bean>
    
    <!-- Trigger that fires to clean up unneeded messages in the queue -->
    <!-- Registered by the \"scheduler\" bean -->
    <bean id=\"messageCleanupTrigger\" class=\"org.springframework.scheduling.quartz.CronTriggerBean\">
    	<property name=\"jobDetail\"><ref bean=\"messageCleanupJobDetail\"/></property>
    	<property name=\"cronExpression\"><value>0 0 6 * * ?</value></property>
    </bean>
    
    <!-- Job definition to clean up old messages in the queue -->
    <bean id=\"messageCleanupJobDetail\" class=\"org.springframework.scheduling.quartz.JobDetailBean\">
    	<property name=\"jobClass\">
    		<value>com.airprism.service.msg.impl.CleanupJob</value>
    	</property>
    </bean>



---

**Affects:** 1.1 RC1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4981","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4981/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4981/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4981/events","html_url":"https://github.com/spring-projects/spring-framework/issues/4981","id":398050479,"node_id":"MDU6SXNzdWUzOTgwNTA0Nzk=","number":4981,"title":"Quartz: Setting flag to overwrite existing jobs doesn't update triggers [SPR-250]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2004-08-09T07:57:02Z","updated_at":"2019-01-11T13:27:06Z","closed_at":"2004-08-10T00:33:19Z","author_association":"COLLABORATOR","body":"**[Bryan Keller](https://jira.spring.io/secure/ViewProfile.jspa?name=bryanck)** opened **[SPR-250](https://jira.spring.io/browse/SPR-250?redirect=false)** and commented\n\nThis applies to the Quartz scheduler support.\n\nWhen setting the overwriteExistingJobs flag on the org.springframework.scheduling.quartz.SchedulerFactoryBean, any triggers that have been updated since the last startup time will not be overwritten. If you change the trigger's criteria, and are persisting the schedules via the dataSource property, the trigger will not be updated when your server is restarted. Te old trigger criteria remain in effect.\n\nAn example:\n\nCreate a persistence schedule using something similar to the following applicationContext.xml snippet (see bottom). Now stop your server, change the trigger params (e.g. cronExpression), and restart the server. The original trigger's cronExpression remains in effect - the new values are not used.\n\nSolution:\n\nCall scheduler.rescheduleJob(trigger) to reschedule the trigger on startup.\n\nFrom SchedulerFactoryBean.java.registerJobsAndTriggers()\n\n    // register Triggers\n    if (this.triggers != null) {\n         for (Iterator it = this.triggers.iterator(); it.hasNext();) {\n              Trigger trigger = (Trigger) it.next();\n              if (this.scheduler.getTrigger(trigger.getName(), trigger.getGroup()) == null) {\n                       // check if the Trigger is aware of an associated JobDetail\n                       if (trigger instanceof JobDetailAwareTrigger) {\n                                JobDetail jobDetail = ((JobDetailAwareTrigger) trigger).getJobDetail();\n                                // automatically register the JobDetail too\n                                if (!this.jobDetails.contains(jobDetail) && addJobToScheduler(jobDetail)) {\n                                         this.jobDetails.add(jobDetail);\n                                }\n                       }\n                       this.scheduler.scheduleJob(trigger);\n              } else if (this.overwriteExistingJobs) {\n                       this.scheduler.rescheduleJob(\n                           trigger.getName(), trigger.getGroup(), trigger\n                       );\n              }\n         }\n    }\n\nsnippet from applicationContext.xml:\n\n    <!-- Quartz Scheduler, with pre-registered triggers -->\n    <!-- Will automatically start scheduling on context startup -->\n    <bean id=\"scheduler\" class=\"org.springframework.scheduling.quartz.SchedulerFactoryBean\">\n    \t<property name=\"dataSource\"><ref bean=\"dataSource\"/></property>\n    \t<property name=\"overwriteExistingJobs\"><value>true</value></property>\n    \t<property name=\"applicationContextSchedulerContextKey\"><value>appContext</value></property>\n    \t<property name=\"triggers\">\n    \t\t<list>\n    \t\t\t<ref local=\"messageCleanupTrigger\"/>\n    \t\t</list>\n    \t</property>\n    </bean>\n    \n    <!-- Trigger that fires to clean up unneeded messages in the queue -->\n    <!-- Registered by the \"scheduler\" bean -->\n    <bean id=\"messageCleanupTrigger\" class=\"org.springframework.scheduling.quartz.CronTriggerBean\">\n    \t<property name=\"jobDetail\"><ref bean=\"messageCleanupJobDetail\"/></property>\n    \t<property name=\"cronExpression\"><value>0 0 6 * * ?</value></property>\n    </bean>\n    \n    <!-- Job definition to clean up old messages in the queue -->\n    <bean id=\"messageCleanupJobDetail\" class=\"org.springframework.scheduling.quartz.JobDetailBean\">\n    \t<property name=\"jobClass\">\n    \t\t<value>com.airprism.service.msg.impl.CleanupJob</value>\n    \t</property>\n    </bean>\n\n\n\n---\n\n**Affects:** 1.1 RC1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453286693"], "labels":["in: core"]}