{"id":"10232", "title":"Incorrect injection type, when setting a generic parameterized property [SPR-5561]", "body":"**[Timo Rumland](https://jira.spring.io/secure/ViewProfile.jspa?name=patb)** opened **[SPR-5561](https://jira.spring.io/browse/SPR-5561?redirect=false)** and commented

Problem with injecting a generic property. Spring should detect the correct type of the class attribute, but it only injects a String. Problem can be reproduced with this two simple classes:

```
public class GenericInjectTest {
  private Ship< Integer > ship = new Ship< Integer >();

  private void beanInit() {
    System.out.println( ship.getData() );
    System.out.println( ship.getData().getClass() );
  }

  public Ship< Integer > getShip() {
    return ship;
  }

  public void setShip( Ship< Integer > ship ) {
    this.ship = ship;
  }
}
```

(the beanInit method gets called by Spring, it is defined as the default bean init method).

```
public class Ship< T > {
  private T data;

  public T getData() {
    return data;
  }

  public void setData( T data ) {
    this.data = data;
  }
}
```

Here is the (important part of the) Spring context configuration:

```xml
<bean class=\"com.test.spring.GenericInjectTest\">
  <property name=\"ship.data\" value=\"123\" />
</bean>
```

Looking at the bean definition, we can see that I want to set the property \"ship.data\" to \"123\". Since ship is parameterized with \"Integer\" in the GenericInjectTest class, I would think that Spring is able to detect the correct runtime type, which is Integer, and would convert the \"123\" to a real Integer.

But this is not the case. When I run the program, the line

```
System.out.println( ship.getData() );
```

prints \"123\". But the next line

```
System.out.println( ship.getData().getClass() );
```

throws the exception

```
java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
```

So Spring has injected a String for the property \"ship.data\", not an Integer.


---

**Affects:** 2.5.4, 2.5.5, 2.5.6

**Reference URL:** http://forum.springframework.org/showthread.php?p=230821

2 votes, 7 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10232","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10232/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10232/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10232/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10232","id":398093694,"node_id":"MDU6SXNzdWUzOTgwOTM2OTQ=","number":10232,"title":"Incorrect injection type, when setting a generic parameterized property [SPR-5561]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2009-03-10T10:47:34Z","updated_at":"2015-09-22T17:34:33Z","closed_at":"2015-09-22T17:34:33Z","author_association":"COLLABORATOR","body":"**[Timo Rumland](https://jira.spring.io/secure/ViewProfile.jspa?name=patb)** opened **[SPR-5561](https://jira.spring.io/browse/SPR-5561?redirect=false)** and commented\n\nProblem with injecting a generic property. Spring should detect the correct type of the class attribute, but it only injects a String. Problem can be reproduced with this two simple classes:\n\n```\npublic class GenericInjectTest {\n  private Ship< Integer > ship = new Ship< Integer >();\n\n  private void beanInit() {\n    System.out.println( ship.getData() );\n    System.out.println( ship.getData().getClass() );\n  }\n\n  public Ship< Integer > getShip() {\n    return ship;\n  }\n\n  public void setShip( Ship< Integer > ship ) {\n    this.ship = ship;\n  }\n}\n```\n\n(the beanInit method gets called by Spring, it is defined as the default bean init method).\n\n```\npublic class Ship< T > {\n  private T data;\n\n  public T getData() {\n    return data;\n  }\n\n  public void setData( T data ) {\n    this.data = data;\n  }\n}\n```\n\nHere is the (important part of the) Spring context configuration:\n\n```xml\n<bean class=\"com.test.spring.GenericInjectTest\">\n  <property name=\"ship.data\" value=\"123\" />\n</bean>\n```\n\nLooking at the bean definition, we can see that I want to set the property \"ship.data\" to \"123\". Since ship is parameterized with \"Integer\" in the GenericInjectTest class, I would think that Spring is able to detect the correct runtime type, which is Integer, and would convert the \"123\" to a real Integer.\n\nBut this is not the case. When I run the program, the line\n\n```\nSystem.out.println( ship.getData() );\n```\n\nprints \"123\". But the next line\n\n```\nSystem.out.println( ship.getData().getClass() );\n```\n\nthrows the exception\n\n```\njava.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer\n```\n\nSo Spring has injected a String for the property \"ship.data\", not an Integer.\n\n\n---\n\n**Affects:** 2.5.4, 2.5.5, 2.5.6\n\n**Reference URL:** http://forum.springframework.org/showthread.php?p=230821\n\n2 votes, 7 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453337085","453337086","453337087","453337088"], "labels":["in: core","status: declined","type: enhancement"]}