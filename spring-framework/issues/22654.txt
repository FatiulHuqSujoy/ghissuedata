{"id":"22654", "title":"Regression in merged annotation resolution", "body":"Related to #22586.

Merged annotations used as meta annotations seems to no longer resolve attributes.
Simplified test below passes for 5.1.5.RELEASE and fails for current 5.2.0.BUILD-SNAPSHOT. 

*Result for 5.1.5*: `BaseAnnotation(name=custom-name, val1=true)`
*Result for 5.2.0*: `BaseAnnotation(name=base-name, val1=false)`

```java
@Test
public void resolveWithCustomAnnotationUsingComposedAnnotationAsMeta() {

  java.lang.reflect.Field field = ReflectionUtils.findField(Sample.class,
      it -> it.getName().equals(\"value\"));

  BaseAnnotation annotation = AnnotatedElementUtils.findMergedAnnotation(field, BaseAnnotation.class);

  assertThat(annotation.name()).isEqualTo(\"custom-name\");
  assertThat(annotation.val1()).isTrue();
}

static class Sample {

  @CustomAnnotationWithComposedMeta 
  String value;
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD })
@interface BaseAnnotation {

  String name() default \"base-name\";
  boolean val1() default false;
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD })
@BaseAnnotation
@interface ComposedAnnotation {

  @AliasFor(annotation = BaseAnnotation.class, attribute = \"name\")
  String customName() default \"composed-name\";

  @AliasFor(annotation = BaseAnnotation.class, attribute = \"val1\")
  boolean foo() default false;
}


@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@ComposedAnnotation(customName = \"custom-name\", foo = true)
@interface CustomAnnotationWithComposedMeta {}
```

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22654","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22654/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22654/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22654/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22654","id":424895133,"node_id":"MDU6SXNzdWU0MjQ4OTUxMzM=","number":22654,"title":"Regression in merged annotation resolution","user":{"login":"christophstrobl","id":2317257,"node_id":"MDQ6VXNlcjIzMTcyNTc=","avatar_url":"https://avatars1.githubusercontent.com/u/2317257?v=4","gravatar_id":"","url":"https://api.github.com/users/christophstrobl","html_url":"https://github.com/christophstrobl","followers_url":"https://api.github.com/users/christophstrobl/followers","following_url":"https://api.github.com/users/christophstrobl/following{/other_user}","gists_url":"https://api.github.com/users/christophstrobl/gists{/gist_id}","starred_url":"https://api.github.com/users/christophstrobl/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/christophstrobl/subscriptions","organizations_url":"https://api.github.com/users/christophstrobl/orgs","repos_url":"https://api.github.com/users/christophstrobl/repos","events_url":"https://api.github.com/users/christophstrobl/events{/privacy}","received_events_url":"https://api.github.com/users/christophstrobl/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511994,"node_id":"MDU6TGFiZWwxMTg4NTExOTk0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20regression","name":"type: regression","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/203","html_url":"https://github.com/spring-projects/spring-framework/milestone/203","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/203/labels","id":3960976,"node_id":"MDk6TWlsZXN0b25lMzk2MDk3Ng==","number":203,"title":"5.2 M1","description":"","creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":148,"state":"closed","created_at":"2019-01-10T22:05:55Z","updated_at":"2019-04-10T15:34:57Z","due_on":"2019-04-10T07:00:00Z","closed_at":"2019-04-10T09:50:59Z"},"comments":2,"created_at":"2019-03-25T13:14:20Z","updated_at":"2019-03-26T00:35:43Z","closed_at":"2019-03-26T00:35:43Z","author_association":"MEMBER","body":"Related to #22586.\r\n\r\nMerged annotations used as meta annotations seems to no longer resolve attributes.\r\nSimplified test below passes for 5.1.5.RELEASE and fails for current 5.2.0.BUILD-SNAPSHOT. \r\n\r\n*Result for 5.1.5*: `BaseAnnotation(name=custom-name, val1=true)`\r\n*Result for 5.2.0*: `BaseAnnotation(name=base-name, val1=false)`\r\n\r\n```java\r\n@Test\r\npublic void resolveWithCustomAnnotationUsingComposedAnnotationAsMeta() {\r\n\r\n  java.lang.reflect.Field field = ReflectionUtils.findField(Sample.class,\r\n      it -> it.getName().equals(\"value\"));\r\n\r\n  BaseAnnotation annotation = AnnotatedElementUtils.findMergedAnnotation(field, BaseAnnotation.class);\r\n\r\n  assertThat(annotation.name()).isEqualTo(\"custom-name\");\r\n  assertThat(annotation.val1()).isTrue();\r\n}\r\n\r\nstatic class Sample {\r\n\r\n  @CustomAnnotationWithComposedMeta \r\n  String value;\r\n}\r\n\r\n@Retention(RetentionPolicy.RUNTIME)\r\n@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD })\r\n@interface BaseAnnotation {\r\n\r\n  String name() default \"base-name\";\r\n  boolean val1() default false;\r\n}\r\n\r\n@Retention(RetentionPolicy.RUNTIME)\r\n@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD })\r\n@BaseAnnotation\r\n@interface ComposedAnnotation {\r\n\r\n  @AliasFor(annotation = BaseAnnotation.class, attribute = \"name\")\r\n  String customName() default \"composed-name\";\r\n\r\n  @AliasFor(annotation = BaseAnnotation.class, attribute = \"val1\")\r\n  boolean foo() default false;\r\n}\r\n\r\n\r\n@Retention(RetentionPolicy.RUNTIME)\r\n@Target({ ElementType.FIELD })\r\n@ComposedAnnotation(customName = \"custom-name\", foo = true)\r\n@interface CustomAnnotationWithComposedMeta {}\r\n```\r\n\r\n","closed_by":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}}", "commentIds":["476191579","476397756"], "labels":["in: core","type: regression"]}