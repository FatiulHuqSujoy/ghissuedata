{"id":"17605", "title":"Commons FileUpload failed after Spring framework upgrade [SPR-13014]", "body":"**[william zhou](https://jira.spring.io/secure/ViewProfile.jspa?name=william_zhou)** opened **[SPR-13014](https://jira.spring.io/browse/SPR-13014?redirect=false)** and commented

We recently upgraded from Spring framework 3.2.2 to 4.1.6 for our RESTful Services. We have a custom HTTP message reader for multipart request parsing using Apache Commons FileUpload. The code is like this:

```java
@Override
public T read(Class<T> clazz, HttpInputMessage inputMessage)
		throws HttpMessageNotReadableException, IOException {
	if (inputMessage instanceof ServletServerHttpRequest
			&& ServletFileUpload.isMultipartContent(
                               ((ServletServerHttpRequest) inputMessage).getServletRequest())) {
		ServletFileUpload upload = new ServletFileUpload();
		try {
			FileItemIterator iterator = upload
				.getItemIterator(
                                     ((ServletServerHttpRequest)inputMessage).getServletRequest());
			T po = null;
			if (iterator.hasNext()) {
					po = parseDocument(iterator.next(), clazz);
			...
}
```

It works fine while we were using Spring framework 3.2.2. However, after we upgraded to the Spring framework 4.1.6, the multipart request failed as the 1st part was dropped.

After debugging into the Spring code, we found that **org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor** has been changed that, in Spring 3.2.2, it wraps the servlet request's inputstream as a PushbackInputStream **only when `@RequestBody` is not required**.

```java
RequestBody annot = methodParam.getParameterAnnotation(RequestBody.class);
if (!annot.required()) {
     ...
     inputMessage = new ServletServerHttpRequest(servletRequest) {
		@Override
		public InputStream getBody() throws IOException {
		// Form POST should not get here
		return pushbackInputStream;
	}
};
```

And in Spring 4.1.6, it always wraps the inputstream. This makes our code failed because Commons FileUpload takes the **HttpServletRequest** as the input parameter, and the wrapped inputstream is just ignored.

To make it work, we have to workaround our code, like below:

```java
final InputStream bodyWrapper = inputMessage.getBody();
 HttpServletRequest requestWrapper = new HttpServletRequestWrapper(((ServletServerHttpRequest) inputMessage)
       .getServletRequest()) {
          @Override
          public ServletInputStream getInputStream() throws IOException {
                   return new ServletInputStreamWrapper(bodyWrapper);
          }
};

FileItemIterator iterator = upload.getItemIterator(requestWrapper);
```

However, this should really be fixed by Spring code.

---

**Affects:** 4.1.6

**Issue Links:**
- #17376 Re-allow handling empty request body

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a899e066f2944aea29e51bc281f4f30cda86cc9b
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17605","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17605/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17605/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17605/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17605","id":398179561,"node_id":"MDU6SXNzdWUzOTgxNzk1NjE=","number":17605,"title":"Commons FileUpload failed after Spring framework upgrade [SPR-13014]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/131","html_url":"https://github.com/spring-projects/spring-framework/milestone/131","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/131/labels","id":3960904,"node_id":"MDk6TWlsZXN0b25lMzk2MDkwNA==","number":131,"title":"4.1.7","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":50,"state":"closed","created_at":"2019-01-10T22:04:29Z","updated_at":"2019-01-11T08:28:24Z","due_on":"2015-06-29T07:00:00Z","closed_at":"2019-01-10T22:04:29Z"},"comments":2,"created_at":"2015-05-11T07:48:57Z","updated_at":"2019-01-11T16:05:03Z","closed_at":"2015-06-30T10:41:38Z","author_association":"COLLABORATOR","body":"**[william zhou](https://jira.spring.io/secure/ViewProfile.jspa?name=william_zhou)** opened **[SPR-13014](https://jira.spring.io/browse/SPR-13014?redirect=false)** and commented\n\nWe recently upgraded from Spring framework 3.2.2 to 4.1.6 for our RESTful Services. We have a custom HTTP message reader for multipart request parsing using Apache Commons FileUpload. The code is like this:\n\n```java\n@Override\npublic T read(Class<T> clazz, HttpInputMessage inputMessage)\n\t\tthrows HttpMessageNotReadableException, IOException {\n\tif (inputMessage instanceof ServletServerHttpRequest\n\t\t\t&& ServletFileUpload.isMultipartContent(\n                               ((ServletServerHttpRequest) inputMessage).getServletRequest())) {\n\t\tServletFileUpload upload = new ServletFileUpload();\n\t\ttry {\n\t\t\tFileItemIterator iterator = upload\n\t\t\t\t.getItemIterator(\n                                     ((ServletServerHttpRequest)inputMessage).getServletRequest());\n\t\t\tT po = null;\n\t\t\tif (iterator.hasNext()) {\n\t\t\t\t\tpo = parseDocument(iterator.next(), clazz);\n\t\t\t...\n}\n```\n\nIt works fine while we were using Spring framework 3.2.2. However, after we upgraded to the Spring framework 4.1.6, the multipart request failed as the 1st part was dropped.\n\nAfter debugging into the Spring code, we found that **org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor** has been changed that, in Spring 3.2.2, it wraps the servlet request's inputstream as a PushbackInputStream **only when `@RequestBody` is not required**.\n\n```java\nRequestBody annot = methodParam.getParameterAnnotation(RequestBody.class);\nif (!annot.required()) {\n     ...\n     inputMessage = new ServletServerHttpRequest(servletRequest) {\n\t\t@Override\n\t\tpublic InputStream getBody() throws IOException {\n\t\t// Form POST should not get here\n\t\treturn pushbackInputStream;\n\t}\n};\n```\n\nAnd in Spring 4.1.6, it always wraps the inputstream. This makes our code failed because Commons FileUpload takes the **HttpServletRequest** as the input parameter, and the wrapped inputstream is just ignored.\n\nTo make it work, we have to workaround our code, like below:\n\n```java\nfinal InputStream bodyWrapper = inputMessage.getBody();\n HttpServletRequest requestWrapper = new HttpServletRequestWrapper(((ServletServerHttpRequest) inputMessage)\n       .getServletRequest()) {\n          @Override\n          public ServletInputStream getInputStream() throws IOException {\n                   return new ServletInputStreamWrapper(bodyWrapper);\n          }\n};\n\nFileItemIterator iterator = upload.getItemIterator(requestWrapper);\n```\n\nHowever, this should really be fixed by Spring code.\n\n---\n\n**Affects:** 4.1.6\n\n**Issue Links:**\n- #17376 Re-allow handling empty request body\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/a899e066f2944aea29e51bc281f4f30cda86cc9b\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453425633","453425635"], "labels":["in: core","type: bug"]}