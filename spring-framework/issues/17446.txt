{"id":"17446", "title":"POST of Spring edit form on Tomcat 8 fails with 405 while on tomcat 7 works [SPR-12848]", "body":"**[Chris Korakidis](https://jira.spring.io/secure/ViewProfile.jspa?name=ckorakidis)** opened **[SPR-12848](https://jira.spring.io/browse/SPR-12848?redirect=false)** and commented

example end-point:

```
@RequestMapping(value=\"/edit/{id}\", method=RequestMethod.PUT)
	public ModelAndView edditingTeam(@ModelAttribute Team team, @PathVariable Integer id) {..}
```

Example jsp:

```
<form:form method=\"PUT\" modelAttribute=\"team\" action=\"${pageContext.request.contextPath}/team/edit/${team.id}.html\">
    <form:errors cssClass=\"errors\" delimiter=\"&lt;p/&gt;\" />
.....
```

web.xml:

```
<filter>
  <filter-name>HttpMethodFilter</filter-name>
  <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
</filter>
<filter-mapping>
  <filter-name>HttpMethodFilter</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>
```

There's attached also the full poc

Works on tomcat 7.0.55
Fails on tomcat 8.0.0-RC5, 8.0.9, 8.0.11, 8.0.20


---

**Affects:** 4.1.1

**Attachments:**
- [405.png](https://jira.spring.io/secure/attachment/22715/405.png) (_91.15 kB_)
- [spring.zip](https://jira.spring.io/secure/attachment/22718/spring.zip) (_59.02 kB_)

**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/8672ddde304464701e7d1add8159bb655c052650

1 votes, 4 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17446","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17446/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17446/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17446/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17446","id":398177999,"node_id":"MDU6SXNzdWUzOTgxNzc5OTk=","number":17446,"title":"POST of Spring edit form on Tomcat 8 fails with 405 while on tomcat 7 works [SPR-12848]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2015-03-24T13:06:11Z","updated_at":"2019-01-12T16:23:29Z","closed_at":"2015-12-28T16:03:50Z","author_association":"COLLABORATOR","body":"**[Chris Korakidis](https://jira.spring.io/secure/ViewProfile.jspa?name=ckorakidis)** opened **[SPR-12848](https://jira.spring.io/browse/SPR-12848?redirect=false)** and commented\n\nexample end-point:\n\n```\n@RequestMapping(value=\"/edit/{id}\", method=RequestMethod.PUT)\n\tpublic ModelAndView edditingTeam(@ModelAttribute Team team, @PathVariable Integer id) {..}\n```\n\nExample jsp:\n\n```\n<form:form method=\"PUT\" modelAttribute=\"team\" action=\"${pageContext.request.contextPath}/team/edit/${team.id}.html\">\n    <form:errors cssClass=\"errors\" delimiter=\"&lt;p/&gt;\" />\n.....\n```\n\nweb.xml:\n\n```\n<filter>\n  <filter-name>HttpMethodFilter</filter-name>\n  <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>\n</filter>\n<filter-mapping>\n  <filter-name>HttpMethodFilter</filter-name>\n  <url-pattern>/*</url-pattern>\n</filter-mapping>\n```\n\nThere's attached also the full poc\n\nWorks on tomcat 7.0.55\nFails on tomcat 8.0.0-RC5, 8.0.9, 8.0.11, 8.0.20\n\n\n---\n\n**Affects:** 4.1.1\n\n**Attachments:**\n- [405.png](https://jira.spring.io/secure/attachment/22715/405.png) (_91.15 kB_)\n- [spring.zip](https://jira.spring.io/secure/attachment/22718/spring.zip) (_59.02 kB_)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework-issues/commit/8672ddde304464701e7d1add8159bb655c052650\n\n1 votes, 4 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453423956","453423959","453423962","453423964","453423965"], "labels":["status: declined"]}