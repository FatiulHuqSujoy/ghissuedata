{"id":"19832", "title":"Introduce @FromModel as substitute to @ModelAttribute(binding=false) [SPR-15267]", "body":"**[Jochen Pier](https://jira.spring.io/secure/ViewProfile.jspa?name=jochenpier)** opened **[SPR-15267](https://jira.spring.io/browse/SPR-15267?redirect=false)** and commented

Dear Spring-Team,

we often use `@ModelAttribute` to fetch Model-Attributes from the model in controller actions.

Example:

```java
@RequestMapping(\"/testAtrribute\")
public @ResponseBody String action(@ModelAttribute(name = \"attr\", binding=false) String attr) {
     return \"Received from Model:\" + attr;
}
```

The problem: If we forget the \"binding=false\" attribute, we get a really heavy security risk. Additionally, I think that getting something from the existing Model (which was constructed from former processing) or binding data to request are very different things, that should get different names.

So I suggest to introduce a new name and deprecate the \"binding=false\" attribute.
Name-suggestion: `@FromModel`(name=\"xyz\")

Example:

```java
@RequestMapping(\"/testAtrribute\")
public @ResponseBody String action(@FromModel(name = \"attr\") String attr) {
     return \"Received from Model:\" + attr;
}
```

And maybe another suggestion:
Rename the parameter-binding annotation too.
So

```java
@RequestMapping(\"/testAtrribute\")
public @ResponseBody String action(@FromRequest(\"Book\") Book book) {
     return \"Received from Request:\" + book.getTitle();
}
```

Thank you for spring!

---

**Issue Links:**
- #17982 Prevent binding for `@ModelAttribute`

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19832","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19832/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19832/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19832/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19832","id":398205840,"node_id":"MDU6SXNzdWUzOTgyMDU4NDA=","number":19832,"title":"Introduce @FromModel as substitute to @ModelAttribute(binding=false) [SPR-15267]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2017-02-18T20:52:13Z","updated_at":"2019-01-13T05:17:10Z","closed_at":"2018-12-13T14:50:55Z","author_association":"COLLABORATOR","body":"**[Jochen Pier](https://jira.spring.io/secure/ViewProfile.jspa?name=jochenpier)** opened **[SPR-15267](https://jira.spring.io/browse/SPR-15267?redirect=false)** and commented\n\nDear Spring-Team,\n\nwe often use `@ModelAttribute` to fetch Model-Attributes from the model in controller actions.\n\nExample:\n\n```java\n@RequestMapping(\"/testAtrribute\")\npublic @ResponseBody String action(@ModelAttribute(name = \"attr\", binding=false) String attr) {\n     return \"Received from Model:\" + attr;\n}\n```\n\nThe problem: If we forget the \"binding=false\" attribute, we get a really heavy security risk. Additionally, I think that getting something from the existing Model (which was constructed from former processing) or binding data to request are very different things, that should get different names.\n\nSo I suggest to introduce a new name and deprecate the \"binding=false\" attribute.\nName-suggestion: `@FromModel`(name=\"xyz\")\n\nExample:\n\n```java\n@RequestMapping(\"/testAtrribute\")\npublic @ResponseBody String action(@FromModel(name = \"attr\") String attr) {\n     return \"Received from Model:\" + attr;\n}\n```\n\nAnd maybe another suggestion:\nRename the parameter-binding annotation too.\nSo\n\n```java\n@RequestMapping(\"/testAtrribute\")\npublic @ResponseBody String action(@FromRequest(\"Book\") Book book) {\n     return \"Received from Request:\" + book.getTitle();\n}\n```\n\nThank you for spring!\n\n---\n\n**Issue Links:**\n- #17982 Prevent binding for `@ModelAttribute`\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453452195","453452198","453452199","453452202"], "labels":["status: declined","type: enhancement"]}