{"id":"7394", "title":"oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'. Unable to Start OC4J Server [SPR-2706]", "body":"**[Vigil Bose](https://jira.spring.io/secure/ViewProfile.jspa?name=vbose)** opened **[SPR-2706](https://jira.spring.io/browse/SPR-2706?redirect=false)** and commented

We are currently working on an enterprise project for one of our biggest clients. We are using Spring 2.0 and Spring Web Flow 1.0 Rc4. While deploying the application to Oracle 9ias 10.1.2.0.2 in our development environment, we encountered the following error. We experienced the same error in Spring 2.0 RC2 and it was fixed in Spring 2.0 RC3 by Rick Evans. The ticket No is SPR - 2290. Kindly fix the bug. I will attach the XMLParser Oracle uses so you guys can test it. This is a critical error and we are unable to move forward.

EXCEPTION FROM THE LOG FILE:

---

06/10/11 17:58:18 Started
06/10/11 17:58:21 drlm: jsp: init
06/10/11 17:58:21 drlm: context: init
06/10/11 17:58:21 drlm: Loading Spring root WebApplicationContext
06/10/11 17:58:52 drlm: Error initializing servlet
org.springframework.beans.factory.BeanDefinitionStoreException: Line 8 in XML document from ServletContext resource [/WEB-INF/drlm-servlet.xml] is invalid; nested exception is oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'
Caused by: oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'
at oracle.xml.parser.v2.XMLError.flushErrorHandler(XMLError.java:444)
at oracle.xml.parser.v2.XMLError.flushErrors1(XMLError.java:303)
at oracle.xml.parser.v2.NonValidatingParser.parseDocument(NonValidatingParser.java:290)
at oracle.xml.parser.v2.XMLParser.parse(XMLParser.java:196)
at oracle.xml.jaxp.JXDocumentBuilder.parse(JXDocumentBuilder.java:151)
at org.springframework.beans.factory.xml.DefaultDocumentLoader.loadDocument(DefaultDocumentLoader.java:77)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.doLoadBeanDefinitions(XmlBeanDefinitionReader.java:405)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:357)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:334)
at org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:126)
at org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:142)
at org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:123)
at org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:91)
at org.springframework.context.support.AbstractRefreshableApplicationContext.refreshBeanFactory(AbstractRefreshableApplicationContext.java:94)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:294)
at org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:156)
at org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:246)
at org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:184)
at org.springframework.web.context.ContextLoaderServlet.init(ContextLoaderServlet.java:83)
at javax.servlet.GenericServlet.init(GenericServlet.java:258)
at com.evermind.server.http.HttpApplication.loadServlet(HttpApplication.java:2354)
at com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4795)
at com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)
at com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)
at com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)
at com.evermind.server.Application.getHttpApplication(Application.java:890)
at com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)
at com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)
at com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)
at com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)
at com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)
at com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)
at com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)
at com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)
at java.lang.Thread.run(Thread.java:534)
06/10/11 17:58:52 drlm: Error preloading servlet
javax.servlet.ServletException: Error initializing servlet
at com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4846)
at com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)
at com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)
at com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)
at com.evermind.server.Application.getHttpApplication(Application.java:890)
at com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)
at com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)
at com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)
at com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)
at com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)
at com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)
at com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)
at com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)
at java.lang.Thread.run(Thread.java:534)
06/10/11 17:58:52 drlm: drlm: init
06/10/11 17:58:52 drlm: Loading WebApplicationContext for Spring FrameworkServlet 'drlm'
06/10/11 17:58:52 drlm: Error initializing servlet
org.springframework.beans.factory.BeanDefinitionStoreException: Line 8 in XML document from ServletContext resource [/WEB-INF/drlm-servlet.xml] is invalid; nested exception is oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'
Caused by: oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'
at oracle.xml.parser.v2.XMLError.flushErrorHandler(XMLError.java:444)
at oracle.xml.parser.v2.XMLError.flushErrors1(XMLError.java:303)
at oracle.xml.parser.v2.NonValidatingParser.parseDocument(NonValidatingParser.java:290)
at oracle.xml.parser.v2.XMLParser.parse(XMLParser.java:196)
at oracle.xml.jaxp.JXDocumentBuilder.parse(JXDocumentBuilder.java:151)
at org.springframework.beans.factory.xml.DefaultDocumentLoader.loadDocument(DefaultDocumentLoader.java:77)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.doLoadBeanDefinitions(XmlBeanDefinitionReader.java:405)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:357)
at org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:334)
at org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:126)
at org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:142)
at org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:123)
at org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:91)
at org.springframework.context.support.AbstractRefreshableApplicationContext.refreshBeanFactory(AbstractRefreshableApplicationContext.java:94)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:294)
at org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:156)
at org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:246)
at org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:184)
at org.springframework.web.context.ContextLoaderServlet.init(ContextLoaderServlet.java:83)
at javax.servlet.GenericServlet.init(GenericServlet.java:258)
at com.evermind.server.http.HttpApplication.loadServlet(HttpApplication.java:2354)
at com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4795)
at com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)
at com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)
at com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)
at com.evermind.server.Application.getHttpApplication(Application.java:890)
at com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)
at com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)
at com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)
at com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)
at com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)
at com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)
at com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)
at com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)
at java.lang.Thread.run(Thread.java:534)
06/10/11 17:58:52 drlm: Error preloading servlet
javax.servlet.ServletException: Error initializing servlet
at com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4846)
at com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)
at com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)
at com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)
at com.evermind.server.Application.getHttpApplication(Application.java:890)
at com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)
at com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)
at com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)
at com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)
at com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)
at com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)
at com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)
at com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)
at java.lang.Thread.run(Thread.java:534)
06/10/11 17:58:52 drlm: dwr-invoker: init
06/10/11 17:58:53 drlm: Started

drlm-servlet.xml file:

---

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:flow=\"http://www.springframework.org/schema/webflow-config\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://www.springframework.org/schema/webflow-config
http://www.springframework.org/schema/webflow-config/spring-webflow-config-1.0.xsd\">

\\<!--
- DispatcherServlet application context for Device Registration and Listing Application web tier.
  -->

  <!---- ========================= MESSAGE SOURCE DEFINITION ========================= --><!--->- Message source for this context, loaded from localized \"messages_xx\" files.
  - Could also reside in the root application context, as it is generic,
  - but is currently just used within DRLM's web tier.
    -->

  <bean id=\"messageSource\" class=\"org.springframework.context.support.ResourceBundleMessageSource\">
  	<property name=\"basename\" value=\"messages\"/>
  </bean>

  <!----Enabling DWR as a standard controller  -->

  <bean id=\"dwrController\" lazy-init=\"true\" class= \"org.springframework.web.servlet.mvc.ServletWrappingController\">
  <property name=\"servletClass\"><value>uk.ltd.getahead.dwr.DWRServlet</value></property>
  <property name=\"servletName\"><value>dwr-invoker</value></property>
  <property name=\"initParameters\"><props> <prop key=\"debug\">true</prop></props></property>
  </bean>

  <!----
  	A general purpose controller for the entire \"DRLM\" application, 
  	exposed at the /drlm.htm URL.  The id of a flow to launch should be passed
  	in using the \"_flowId\" request parameter: e.g. /drlm.htm?_flowId=new-reg-flow
  -->

  <bean name=\"flowController\" class=\"org.springframework.webflow.executor.mvc.FlowController\">
  <property name=\"flowExecutor\" ref=\"flowExecutor\"/>
  </bean>

  <!---- Url mapping definitions -->

  <bean id=\"urlMapping\" class=\"org.springframework.web.servlet.handler.SimpleUrlHandlerMapping\">
  		<property name=\"order\" value=\"1\"/>
  		 <!-- <property name=\"interceptors\">
  			<list>
  				<ref bean=\"signonInterceptor\"/>
  			</list> 
  		</property>-->
  <property name=\"mappings\"> <!--  secure mappings require user session -->
  <props>
  <prop key=\"/drlm.htm\">flowController</prop>
  				  <prop key=\"/mainMenu.htm\">drlmMainMenuController</prop>                                 
  </props>
  </property>
  </bean>    

  <bean class=\"org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping\">
  	<property name=\"order\" value=\"2\"/>
  </bean>

  <!---- Maps flow view-state view names to JSP templates -->

  <bean id=\"viewResolver\" class=\"org.springframework.web.servlet.view.InternalResourceViewResolver\">
  	<property name=\"prefix\" value=\"/WEB-INF/jsp/\"/>
  	<property name=\"suffix\" value=\".jsp\"/>
  </bean>

  <!---- Launches new flow executions and resumes existing executions.
  This executor is configured with a continuation-based repository that
  manages continuation state in the user session. -->

  <flow:executor id=\"flowExecutor\" registry-ref=\"flowRegistry\" repository-type=\"continuation\"/>

  <!---- Creates the registry of flow definitions for this application -->

  <flow:registry id=\"flowRegistry\">
  <flow:location path=\"file:html/WEB-INF/flows/**/*-flow.xml\"/>
  </flow:registry>

  <!---- Validator Class for Registration Form -->

  <bean id=\"registrationFormValidator\" class=\"gov.fda.furls.drlm.web.validator.RegistrationFormValidator\"/>

  <!---- FormAction for Registration form  -->

  <bean id=\"registrationAction\" class=\"gov.fda.furls.drlm.web.action.RegistrationAction\">
  	<property name=\"validator\" ref=\"registrationFormValidator\"/>
  	<property name=\"accountServices\" ref=\"accountServices\" />
  	<property name=\"registrationLookUpServices\" ref=\"registrationLookUpServices\" />
  </bean>

  <!----  added for controller by Yanqi -->

  <bean id=\"drlmMainMenuController\" class=\"gov.fda.furls.drlm.web.controller.DRLMMainMenuController\">
  </bean>	

\\</beans>

---

**Affects:** 2.0 final

**Attachments:**
- [xmlparserv2.jar](https://jira.spring.io/secure/attachment/12019/xmlparserv2.jar) (_872.27 kB_)

**Issue Links:**
- #7316 http://www.springframework.org/schema/jee doesn't validate in XMLSpy (_**\"is depended on by\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7394","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7394/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7394/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7394/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7394","id":398071858,"node_id":"MDU6SXNzdWUzOTgwNzE4NTg=","number":7394,"title":"oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'. Unable to Start OC4J Server [SPR-2706]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false,"description":"Issues in web modules (web, webmvc, webflux, websocket)"},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false,"description":"A suggestion or change that we don't feel we should currently apply"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":9,"created_at":"2006-10-11T09:11:38Z","updated_at":"2019-01-13T22:45:42Z","closed_at":"2007-12-11T07:10:30Z","author_association":"COLLABORATOR","body":"**[Vigil Bose](https://jira.spring.io/secure/ViewProfile.jspa?name=vbose)** opened **[SPR-2706](https://jira.spring.io/browse/SPR-2706?redirect=false)** and commented\n\nWe are currently working on an enterprise project for one of our biggest clients. We are using Spring 2.0 and Spring Web Flow 1.0 Rc4. While deploying the application to Oracle 9ias 10.1.2.0.2 in our development environment, we encountered the following error. We experienced the same error in Spring 2.0 RC2 and it was fixed in Spring 2.0 RC3 by Rick Evans. The ticket No is SPR - 2290. Kindly fix the bug. I will attach the XMLParser Oracle uses so you guys can test it. This is a critical error and we are unable to move forward.\n\nEXCEPTION FROM THE LOG FILE:\n\n---\n\n06/10/11 17:58:18 Started\n06/10/11 17:58:21 drlm: jsp: init\n06/10/11 17:58:21 drlm: context: init\n06/10/11 17:58:21 drlm: Loading Spring root WebApplicationContext\n06/10/11 17:58:52 drlm: Error initializing servlet\norg.springframework.beans.factory.BeanDefinitionStoreException: Line 8 in XML document from ServletContext resource [/WEB-INF/drlm-servlet.xml] is invalid; nested exception is oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'\nCaused by: oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'\nat oracle.xml.parser.v2.XMLError.flushErrorHandler(XMLError.java:444)\nat oracle.xml.parser.v2.XMLError.flushErrors1(XMLError.java:303)\nat oracle.xml.parser.v2.NonValidatingParser.parseDocument(NonValidatingParser.java:290)\nat oracle.xml.parser.v2.XMLParser.parse(XMLParser.java:196)\nat oracle.xml.jaxp.JXDocumentBuilder.parse(JXDocumentBuilder.java:151)\nat org.springframework.beans.factory.xml.DefaultDocumentLoader.loadDocument(DefaultDocumentLoader.java:77)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.doLoadBeanDefinitions(XmlBeanDefinitionReader.java:405)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:357)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:334)\nat org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:126)\nat org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:142)\nat org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:123)\nat org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:91)\nat org.springframework.context.support.AbstractRefreshableApplicationContext.refreshBeanFactory(AbstractRefreshableApplicationContext.java:94)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:294)\nat org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:156)\nat org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:246)\nat org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:184)\nat org.springframework.web.context.ContextLoaderServlet.init(ContextLoaderServlet.java:83)\nat javax.servlet.GenericServlet.init(GenericServlet.java:258)\nat com.evermind.server.http.HttpApplication.loadServlet(HttpApplication.java:2354)\nat com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4795)\nat com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)\nat com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)\nat com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)\nat com.evermind.server.Application.getHttpApplication(Application.java:890)\nat com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)\nat com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)\nat com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)\nat com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)\nat com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)\nat com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)\nat com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)\nat com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)\nat java.lang.Thread.run(Thread.java:534)\n06/10/11 17:58:52 drlm: Error preloading servlet\njavax.servlet.ServletException: Error initializing servlet\nat com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4846)\nat com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)\nat com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)\nat com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)\nat com.evermind.server.Application.getHttpApplication(Application.java:890)\nat com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)\nat com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)\nat com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)\nat com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)\nat com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)\nat com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)\nat com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)\nat com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)\nat java.lang.Thread.run(Thread.java:534)\n06/10/11 17:58:52 drlm: drlm: init\n06/10/11 17:58:52 drlm: Loading WebApplicationContext for Spring FrameworkServlet 'drlm'\n06/10/11 17:58:52 drlm: Error initializing servlet\norg.springframework.beans.factory.BeanDefinitionStoreException: Line 8 in XML document from ServletContext resource [/WEB-INF/drlm-servlet.xml] is invalid; nested exception is oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'\nCaused by: oracle.xml.parser.schema.XSDException: Duplicated definition for: 'identifiedType'\nat oracle.xml.parser.v2.XMLError.flushErrorHandler(XMLError.java:444)\nat oracle.xml.parser.v2.XMLError.flushErrors1(XMLError.java:303)\nat oracle.xml.parser.v2.NonValidatingParser.parseDocument(NonValidatingParser.java:290)\nat oracle.xml.parser.v2.XMLParser.parse(XMLParser.java:196)\nat oracle.xml.jaxp.JXDocumentBuilder.parse(JXDocumentBuilder.java:151)\nat org.springframework.beans.factory.xml.DefaultDocumentLoader.loadDocument(DefaultDocumentLoader.java:77)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.doLoadBeanDefinitions(XmlBeanDefinitionReader.java:405)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:357)\nat org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:334)\nat org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:126)\nat org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:142)\nat org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:123)\nat org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:91)\nat org.springframework.context.support.AbstractRefreshableApplicationContext.refreshBeanFactory(AbstractRefreshableApplicationContext.java:94)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:294)\nat org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:156)\nat org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:246)\nat org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:184)\nat org.springframework.web.context.ContextLoaderServlet.init(ContextLoaderServlet.java:83)\nat javax.servlet.GenericServlet.init(GenericServlet.java:258)\nat com.evermind.server.http.HttpApplication.loadServlet(HttpApplication.java:2354)\nat com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4795)\nat com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)\nat com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)\nat com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)\nat com.evermind.server.Application.getHttpApplication(Application.java:890)\nat com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)\nat com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)\nat com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)\nat com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)\nat com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)\nat com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)\nat com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)\nat com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)\nat java.lang.Thread.run(Thread.java:534)\n06/10/11 17:58:52 drlm: Error preloading servlet\njavax.servlet.ServletException: Error initializing servlet\nat com.evermind.server.http.HttpApplication.findServlet(HttpApplication.java:4846)\nat com.evermind.server.http.HttpApplication.initPreloadServlets(HttpApplication.java:4889)\nat com.evermind.server.http.HttpApplication.initDynamic(HttpApplication.java:1015)\nat com.evermind.server.http.HttpApplication.\\<init>(HttpApplication.java:549)\nat com.evermind.server.Application.getHttpApplication(Application.java:890)\nat com.evermind.server.http.HttpServer.getHttpApplication(HttpServer.java:707)\nat com.evermind.server.http.HttpSite.initApplications(HttpSite.java:625)\nat com.evermind.server.http.HttpSite.setConfig(HttpSite.java:278)\nat com.evermind.server.http.HttpServer.setSites(HttpServer.java:278)\nat com.evermind.server.http.HttpServer.setConfig(HttpServer.java:179)\nat com.evermind.server.ApplicationServer.initializeHttp(ApplicationServer.java:2394)\nat com.evermind.server.ApplicationServer.setConfig(ApplicationServer.java:1551)\nat com.evermind.server.ApplicationServerLauncher.run(ApplicationServerLauncher.java:92)\nat java.lang.Thread.run(Thread.java:534)\n06/10/11 17:58:52 drlm: dwr-invoker: init\n06/10/11 17:58:53 drlm: Started\n\ndrlm-servlet.xml file:\n\n---\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:flow=\"http://www.springframework.org/schema/webflow-config\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans\nhttp://www.springframework.org/schema/beans/spring-beans-2.0.xsd\nhttp://www.springframework.org/schema/webflow-config\nhttp://www.springframework.org/schema/webflow-config/spring-webflow-config-1.0.xsd\">\n\n\\<!--\n- DispatcherServlet application context for Device Registration and Listing Application web tier.\n  -->\n\n  <!---- ========================= MESSAGE SOURCE DEFINITION ========================= --><!--->- Message source for this context, loaded from localized \"messages_xx\" files.\n  - Could also reside in the root application context, as it is generic,\n  - but is currently just used within DRLM's web tier.\n    -->\n\n  <bean id=\"messageSource\" class=\"org.springframework.context.support.ResourceBundleMessageSource\">\n  \t<property name=\"basename\" value=\"messages\"/>\n  </bean>\n\n  <!----Enabling DWR as a standard controller  -->\n\n  <bean id=\"dwrController\" lazy-init=\"true\" class= \"org.springframework.web.servlet.mvc.ServletWrappingController\">\n  <property name=\"servletClass\"><value>uk.ltd.getahead.dwr.DWRServlet</value></property>\n  <property name=\"servletName\"><value>dwr-invoker</value></property>\n  <property name=\"initParameters\"><props> <prop key=\"debug\">true</prop></props></property>\n  </bean>\n\n  <!----\n  \tA general purpose controller for the entire \"DRLM\" application, \n  \texposed at the /drlm.htm URL.  The id of a flow to launch should be passed\n  \tin using the \"_flowId\" request parameter: e.g. /drlm.htm?_flowId=new-reg-flow\n  -->\n\n  <bean name=\"flowController\" class=\"org.springframework.webflow.executor.mvc.FlowController\">\n  <property name=\"flowExecutor\" ref=\"flowExecutor\"/>\n  </bean>\n\n  <!---- Url mapping definitions -->\n\n  <bean id=\"urlMapping\" class=\"org.springframework.web.servlet.handler.SimpleUrlHandlerMapping\">\n  \t\t<property name=\"order\" value=\"1\"/>\n  \t\t <!-- <property name=\"interceptors\">\n  \t\t\t<list>\n  \t\t\t\t<ref bean=\"signonInterceptor\"/>\n  \t\t\t</list> \n  \t\t</property>-->\n  <property name=\"mappings\"> <!--  secure mappings require user session -->\n  <props>\n  <prop key=\"/drlm.htm\">flowController</prop>\n  \t\t\t\t  <prop key=\"/mainMenu.htm\">drlmMainMenuController</prop>                                 \n  </props>\n  </property>\n  </bean>    \n\n  <bean class=\"org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping\">\n  \t<property name=\"order\" value=\"2\"/>\n  </bean>\n\n  <!---- Maps flow view-state view names to JSP templates -->\n\n  <bean id=\"viewResolver\" class=\"org.springframework.web.servlet.view.InternalResourceViewResolver\">\n  \t<property name=\"prefix\" value=\"/WEB-INF/jsp/\"/>\n  \t<property name=\"suffix\" value=\".jsp\"/>\n  </bean>\n\n  <!---- Launches new flow executions and resumes existing executions.\n  This executor is configured with a continuation-based repository that\n  manages continuation state in the user session. -->\n\n  <flow:executor id=\"flowExecutor\" registry-ref=\"flowRegistry\" repository-type=\"continuation\"/>\n\n  <!---- Creates the registry of flow definitions for this application -->\n\n  <flow:registry id=\"flowRegistry\">\n  <flow:location path=\"file:html/WEB-INF/flows/**/*-flow.xml\"/>\n  </flow:registry>\n\n  <!---- Validator Class for Registration Form -->\n\n  <bean id=\"registrationFormValidator\" class=\"gov.fda.furls.drlm.web.validator.RegistrationFormValidator\"/>\n\n  <!---- FormAction for Registration form  -->\n\n  <bean id=\"registrationAction\" class=\"gov.fda.furls.drlm.web.action.RegistrationAction\">\n  \t<property name=\"validator\" ref=\"registrationFormValidator\"/>\n  \t<property name=\"accountServices\" ref=\"accountServices\" />\n  \t<property name=\"registrationLookUpServices\" ref=\"registrationLookUpServices\" />\n  </bean>\n\n  <!----  added for controller by Yanqi -->\n\n  <bean id=\"drlmMainMenuController\" class=\"gov.fda.furls.drlm.web.controller.DRLMMainMenuController\">\n  </bean>\t\n\n\\</beans>\n\n---\n\n**Affects:** 2.0 final\n\n**Attachments:**\n- [xmlparserv2.jar](https://jira.spring.io/secure/attachment/12019/xmlparserv2.jar) (_872.27 kB_)\n\n**Issue Links:**\n- #7316 http://www.springframework.org/schema/jee doesn't validate in XMLSpy (_**\"is depended on by\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453311241","453311242","453311243","453311246","453311247","453311249","453311252","453311253","453311254"], "labels":["in: core","in: web","status: declined"]}