{"id":"10367", "title":"Cannot map handler - There is already handler with @Controller and not unique requestMappings [SPR-5697]", "body":"**[Bertalan Fodor](https://jira.spring.io/secure/ViewProfile.jspa?name=fodber)** opened **[SPR-5697](https://jira.spring.io/browse/SPR-5697?redirect=false)** and commented

Using     <context:component-scan> with `@Controller` annotation I get automatic mappings.
For example:

```
@Controller
public class FormController becomes: /form/*
```

If I have a request handler in FormController like

```
@RequestMapping(\"init.do\")
public String init() { }
```

it gets mapped to `/form/init.do`
Alright this way.
But if I create another controller like

```
@Controller public class ContactController { 

    @RequestMapping(\"init.do\")
    public String init() { }

}
```

it isn't mapped to `/contact/init.do` as I would expect, but throws the error:
Cannot map handler [formController] to URL path [init.do]: There is already handler [com.company.FormController@f8b79a] mapped.


---

**Affects:** 2.5.6

1 votes, 1 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10367","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10367/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10367/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10367/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10367","id":398094608,"node_id":"MDU6SXNzdWUzOTgwOTQ2MDg=","number":10367,"title":"Cannot map handler - There is already handler with @Controller and not unique requestMappings [SPR-5697]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2009-04-24T01:33:30Z","updated_at":"2019-01-12T16:27:09Z","closed_at":"2010-02-17T08:43:31Z","author_association":"COLLABORATOR","body":"**[Bertalan Fodor](https://jira.spring.io/secure/ViewProfile.jspa?name=fodber)** opened **[SPR-5697](https://jira.spring.io/browse/SPR-5697?redirect=false)** and commented\n\nUsing     <context:component-scan> with `@Controller` annotation I get automatic mappings.\nFor example:\n\n```\n@Controller\npublic class FormController becomes: /form/*\n```\n\nIf I have a request handler in FormController like\n\n```\n@RequestMapping(\"init.do\")\npublic String init() { }\n```\n\nit gets mapped to `/form/init.do`\nAlright this way.\nBut if I create another controller like\n\n```\n@Controller public class ContactController { \n\n    @RequestMapping(\"init.do\")\n    public String init() { }\n\n}\n```\n\nit isn't mapped to `/contact/init.do` as I would expect, but throws the error:\nCannot map handler [formController] to URL path [init.do]: There is already handler [com.company.FormController@f8b79a] mapped.\n\n\n---\n\n**Affects:** 2.5.6\n\n1 votes, 1 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453338234","453338236"], "labels":["status: declined"]}