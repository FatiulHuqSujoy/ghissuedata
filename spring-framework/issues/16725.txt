{"id":"16725", "title":"ServerEndpointExporter causes application context refresh to fail with an NPE when used in a Spring Boot app [SPR-12109]", "body":"**[Andy Wilkinson](https://jira.spring.io/secure/ViewProfile.jspa?name=awilkinson)** opened **[SPR-12109](https://jira.spring.io/browse/SPR-12109?redirect=false)** and commented

This Boot app illustrates the problem:

```
package sample;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;
import org.springframework.web.socket.server.standard.SpringConfigurator;

@ComponentScan
@EnableAutoConfiguration
@Configuration
public class Application {
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public EchoEndpoint echoEndpoint() {
        return new EchoEndpoint();
    }
    
    @Bean
    public ServerEndpointExporter endpointExporter() {
    	return new ServerEndpointExporter();
    }
    
    @ServerEndpoint(value = \"/echo\", configurator = SpringConfigurator.class)
    private static class EchoEndpoint {

        @OnMessage
        public void handleMessage(Session session, String message) throws IOException {
            session.getBasicRemote().sendText(\"echo: \" + message);
        }
    }
}
```

Running it fails with a `NullPointerException`:

```
Exception in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'endpointExporter' defined in class sample.Application: Initialization of bean failed; nested exception is java.lang.IllegalStateException: Failed to get javax.websocket.server.ServerContainer via ServletContext attribute
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:547)
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:475)
	at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:302)
	at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:228)
	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:298)
	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:198)
	at org.springframework.context.support.PostProcessorRegistrationDelegate.registerBeanPostProcessors(PostProcessorRegistrationDelegate.java:232)
	at org.springframework.context.support.AbstractApplicationContext.registerBeanPostProcessors(AbstractApplicationContext.java:618)
	at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:467)
	at org.springframework.boot.context.embedded.EmbeddedWebApplicationContext.refresh(EmbeddedWebApplicationContext.java:120)
	at org.springframework.boot.SpringApplication.refresh(SpringApplication.java:691)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:320)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:952)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:941)
	at sample.Application.main(Application.java:26)
Caused by: java.lang.IllegalStateException: Failed to get javax.websocket.server.ServerContainer via ServletContext attribute
	at org.springframework.web.socket.server.standard.ServerEndpointExporter.getServerContainer(ServerEndpointExporter.java:113)
	at org.springframework.web.socket.server.standard.ServerEndpointExporter.setApplicationContext(ServerEndpointExporter.java:86)
	at org.springframework.context.support.ApplicationContextAwareProcessor.invokeAwareInterfaces(ApplicationContextAwareProcessor.java:119)
	at org.springframework.context.support.ApplicationContextAwareProcessor.postProcessBeforeInitialization(ApplicationContextAwareProcessor.java:94)
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyBeanPostProcessorsBeforeInitialization(AbstractAutowireCapableBeanFactory.java:407)
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1545)
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:539)
	... 14 more
Caused by: java.lang.NullPointerException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.springframework.web.socket.server.standard.ServerEndpointExporter.getServerContainer(ServerEndpointExporter.java:110)
	... 20 more
```

`ServerEndpointExporter` assumes that `WebApplicationContext.getServletContext()` will return a non-null value when it's called from within `setApplicationContext(context)`. This assumption doesn't hold true in a Boot application as the embedded Tomcat server hasn't been started yet.

A work around is to replace the `ServerEndpointExporter` bean with the following:

```
@Bean
public ServletContextAware endpointExporterInitializer(final ApplicationContext applicationContext) {
    return new ServletContextAware() {

		@Override
		public void setServletContext(ServletContext servletContext) {
			ServerEndpointExporter serverEndpointExporter = new ServerEndpointExporter();
				serverEndpointExporter.setApplicationContext(applicationContext);
			try {
				serverEndpointExporter.afterPropertiesSet();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}				
		}    		
    };
}
```

This defers `ServerEndpointExporter`'s processing until a time when the `ServletContext` is available

---

**Affects:** 4.0.6

**Reference URL:** http://stackoverflow.com/questions/25390100/using-java-api-for-websocket-jsr-356-with-spring-boot/25425384#25425384

**Issue Links:**
- #22131 Error running the application as WAR in tomcat 9
- #16945 ServerEndpointExporter causes refresh to fail with java.lang.IllegalStateException: javax.websocket.server.ServerContainer not available

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/379e5abd833d5126450d218e427460ad652cd5fc, https://github.com/spring-projects/spring-framework/commit/11805b6a5d9e2ae959e544c0a2daa87e127ebbc9

**Backported to:** [4.0.7](https://github.com/spring-projects/spring-framework/milestone/119?closed=1)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16725","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16725/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16725/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16725/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16725","id":398170999,"node_id":"MDU6SXNzdWUzOTgxNzA5OTk=","number":16725,"title":"ServerEndpointExporter causes application context refresh to fail with an NPE when used in a Spring Boot app [SPR-12109]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/124","html_url":"https://github.com/spring-projects/spring-framework/milestone/124","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/124/labels","id":3960897,"node_id":"MDk6TWlsZXN0b25lMzk2MDg5Nw==","number":124,"title":"4.1 GA","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":66,"state":"closed","created_at":"2019-01-10T22:04:20Z","updated_at":"2019-01-11T07:55:37Z","due_on":"2014-09-03T07:00:00Z","closed_at":"2019-01-10T22:04:20Z"},"comments":4,"created_at":"2014-08-21T06:18:36Z","updated_at":"2019-01-11T17:23:51Z","closed_at":"2018-12-13T13:42:15Z","author_association":"COLLABORATOR","body":"**[Andy Wilkinson](https://jira.spring.io/secure/ViewProfile.jspa?name=awilkinson)** opened **[SPR-12109](https://jira.spring.io/browse/SPR-12109?redirect=false)** and commented\n\nThis Boot app illustrates the problem:\n\n```\npackage sample;\n\nimport java.io.IOException;\n\nimport javax.servlet.ServletContext;\nimport javax.websocket.OnMessage;\nimport javax.websocket.Session;\nimport javax.websocket.server.ServerEndpoint;\n\nimport org.springframework.boot.SpringApplication;\nimport org.springframework.boot.autoconfigure.EnableAutoConfiguration;\nimport org.springframework.context.ApplicationContext;\nimport org.springframework.context.annotation.Bean;\nimport org.springframework.context.annotation.ComponentScan;\nimport org.springframework.context.annotation.Configuration;\nimport org.springframework.web.context.ServletContextAware;\nimport org.springframework.web.socket.server.standard.ServerEndpointExporter;\nimport org.springframework.web.socket.server.standard.SpringConfigurator;\n\n@ComponentScan\n@EnableAutoConfiguration\n@Configuration\npublic class Application {\n\t\n    public static void main(String[] args) {\n        SpringApplication.run(Application.class, args);\n    }\n    \n    @Bean\n    public EchoEndpoint echoEndpoint() {\n        return new EchoEndpoint();\n    }\n    \n    @Bean\n    public ServerEndpointExporter endpointExporter() {\n    \treturn new ServerEndpointExporter();\n    }\n    \n    @ServerEndpoint(value = \"/echo\", configurator = SpringConfigurator.class)\n    private static class EchoEndpoint {\n\n        @OnMessage\n        public void handleMessage(Session session, String message) throws IOException {\n            session.getBasicRemote().sendText(\"echo: \" + message);\n        }\n    }\n}\n```\n\nRunning it fails with a `NullPointerException`:\n\n```\nException in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'endpointExporter' defined in class sample.Application: Initialization of bean failed; nested exception is java.lang.IllegalStateException: Failed to get javax.websocket.server.ServerContainer via ServletContext attribute\n\tat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:547)\n\tat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:475)\n\tat org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:302)\n\tat org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:228)\n\tat org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:298)\n\tat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:198)\n\tat org.springframework.context.support.PostProcessorRegistrationDelegate.registerBeanPostProcessors(PostProcessorRegistrationDelegate.java:232)\n\tat org.springframework.context.support.AbstractApplicationContext.registerBeanPostProcessors(AbstractApplicationContext.java:618)\n\tat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:467)\n\tat org.springframework.boot.context.embedded.EmbeddedWebApplicationContext.refresh(EmbeddedWebApplicationContext.java:120)\n\tat org.springframework.boot.SpringApplication.refresh(SpringApplication.java:691)\n\tat org.springframework.boot.SpringApplication.run(SpringApplication.java:320)\n\tat org.springframework.boot.SpringApplication.run(SpringApplication.java:952)\n\tat org.springframework.boot.SpringApplication.run(SpringApplication.java:941)\n\tat sample.Application.main(Application.java:26)\nCaused by: java.lang.IllegalStateException: Failed to get javax.websocket.server.ServerContainer via ServletContext attribute\n\tat org.springframework.web.socket.server.standard.ServerEndpointExporter.getServerContainer(ServerEndpointExporter.java:113)\n\tat org.springframework.web.socket.server.standard.ServerEndpointExporter.setApplicationContext(ServerEndpointExporter.java:86)\n\tat org.springframework.context.support.ApplicationContextAwareProcessor.invokeAwareInterfaces(ApplicationContextAwareProcessor.java:119)\n\tat org.springframework.context.support.ApplicationContextAwareProcessor.postProcessBeforeInitialization(ApplicationContextAwareProcessor.java:94)\n\tat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyBeanPostProcessorsBeforeInitialization(AbstractAutowireCapableBeanFactory.java:407)\n\tat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1545)\n\tat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:539)\n\t... 14 more\nCaused by: java.lang.NullPointerException\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:483)\n\tat org.springframework.web.socket.server.standard.ServerEndpointExporter.getServerContainer(ServerEndpointExporter.java:110)\n\t... 20 more\n```\n\n`ServerEndpointExporter` assumes that `WebApplicationContext.getServletContext()` will return a non-null value when it's called from within `setApplicationContext(context)`. This assumption doesn't hold true in a Boot application as the embedded Tomcat server hasn't been started yet.\n\nA work around is to replace the `ServerEndpointExporter` bean with the following:\n\n```\n@Bean\npublic ServletContextAware endpointExporterInitializer(final ApplicationContext applicationContext) {\n    return new ServletContextAware() {\n\n\t\t@Override\n\t\tpublic void setServletContext(ServletContext servletContext) {\n\t\t\tServerEndpointExporter serverEndpointExporter = new ServerEndpointExporter();\n\t\t\t\tserverEndpointExporter.setApplicationContext(applicationContext);\n\t\t\ttry {\n\t\t\t\tserverEndpointExporter.afterPropertiesSet();\n\t\t\t} catch (Exception e) {\n\t\t\t\tthrow new RuntimeException(e);\n\t\t\t}\t\t\t\t\n\t\t}    \t\t\n    };\n}\n```\n\nThis defers `ServerEndpointExporter`'s processing until a time when the `ServletContext` is available\n\n---\n\n**Affects:** 4.0.6\n\n**Reference URL:** http://stackoverflow.com/questions/25390100/using-java-api-for-websocket-jsr-356-with-spring-boot/25425384#25425384\n\n**Issue Links:**\n- #22131 Error running the application as WAR in tomcat 9\n- #16945 ServerEndpointExporter causes refresh to fail with java.lang.IllegalStateException: javax.websocket.server.ServerContainer not available\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/379e5abd833d5126450d218e427460ad652cd5fc, https://github.com/spring-projects/spring-framework/commit/11805b6a5d9e2ae959e544c0a2daa87e127ebbc9\n\n**Backported to:** [4.0.7](https://github.com/spring-projects/spring-framework/milestone/119?closed=1)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453416141","453416142","453416143","453416146"], "labels":["in: web","status: backported","type: bug"]}