{"id":"4989", "title":"BlobBinaryType for Blob usage with Hibernate [SPR-258]", "body":"**[Nitzan Niv](https://jira.spring.io/secure/ViewProfile.jspa?name=nitzann)** opened **[SPR-258](https://jira.spring.io/browse/SPR-258?redirect=false)** and commented

Hibernate UserType implementation for byte[] that gets mapped to BLOB.
Based on Spring's StringClobType.

---

public class BlobBinaryType implements UserType {

    protected final LobHandler lobHandler;
    
    public BlobBinaryType() {
    	this.lobHandler = LocalSessionFactoryBean.getConfigTimeLobHandler();
    }
    public int[] sqlTypes() {
    	return new int[] {Types.BLOB};
    }
    public Class returnedClass() {
    	return byte[].class;
    }
    public boolean equals(Object x, Object y) {
        return (x == y) || 
          	   (x != null && y != null && java.util.Arrays.equals((byte[]) x, (byte[]) y)); 
    }
    public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws SQLException {
    	if (this.lobHandler == null) {
    		throw new IllegalStateException(\"No LobHandler found for configuration - lobHandler property must be set on LocalSessionFactoryBean\");
    	}
    	return this.lobHandler.getBlobAsBytes(rs, rs.findColumn(names[0]));
    }
    public void nullSafeSet(PreparedStatement st, Object value, int index) throws SQLException {
        if (this.lobHandler == null) {
    		throw new IllegalStateException(\"No LobHandler found for configuration - lobHandler property must be set on LocalSessionFactoryBean\");
    	}
    	if (!TransactionSynchronizationManager.isSynchronizationActive()) {
    		throw new IllegalStateException(\"BlobBinaryType requires active transaction synchronization\");
    	}
    	LobCreator lobCreator = this.lobHandler.getLobCreator();
    	lobCreator.setBlobAsBytes(st, index, (byte[]) value);
    	TransactionSynchronizationManager.registerSynchronization(new LobCreatorSynchronization(lobCreator));
    }
    public Object deepCopy(Object value) {
        // check: create new byte[]?
    	return value;
    }
    public boolean isMutable() {
    	return false;
    }	
    
    /**
     * Callback for resource cleanup at the end of a transaction.
     * Invokes LobCreator.close to clean up temporary LOBs that might have been created.
     * @see org.springframework.jdbc.support.lob.LobCreator#close
     */
    private static class LobCreatorSynchronization extends TransactionSynchronizationAdapter {
    
        private final LobCreator lobCreator;
    
    	private LobCreatorSynchronization(LobCreator lobCreator) {
    		this.lobCreator = lobCreator;
    	}
    
    	public void beforeCompletion() {
    		this.lobCreator.close();
    	}
    }

}

---

No further details from [SPR-258](https://jira.spring.io/browse/SPR-258?redirect=false)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4989","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4989/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4989/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/4989/events","html_url":"https://github.com/spring-projects/spring-framework/issues/4989","id":398050619,"node_id":"MDU6SXNzdWUzOTgwNTA2MTk=","number":4989,"title":"BlobBinaryType for Blob usage with Hibernate [SPR-258]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/9","html_url":"https://github.com/spring-projects/spring-framework/milestone/9","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/9/labels","id":3960778,"node_id":"MDk6TWlsZXN0b25lMzk2MDc3OA==","number":9,"title":"1.1 RC2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":27,"state":"closed","created_at":"2019-01-10T22:02:01Z","updated_at":"2019-01-10T22:51:20Z","due_on":null,"closed_at":"2019-01-10T22:02:01Z"},"comments":1,"created_at":"2004-08-11T20:26:37Z","updated_at":"2019-01-13T22:54:24Z","closed_at":"2004-08-14T04:26:38Z","author_association":"COLLABORATOR","body":"**[Nitzan Niv](https://jira.spring.io/secure/ViewProfile.jspa?name=nitzann)** opened **[SPR-258](https://jira.spring.io/browse/SPR-258?redirect=false)** and commented\n\nHibernate UserType implementation for byte[] that gets mapped to BLOB.\nBased on Spring's StringClobType.\n\n---\n\npublic class BlobBinaryType implements UserType {\n\n    protected final LobHandler lobHandler;\n    \n    public BlobBinaryType() {\n    \tthis.lobHandler = LocalSessionFactoryBean.getConfigTimeLobHandler();\n    }\n    public int[] sqlTypes() {\n    \treturn new int[] {Types.BLOB};\n    }\n    public Class returnedClass() {\n    \treturn byte[].class;\n    }\n    public boolean equals(Object x, Object y) {\n        return (x == y) || \n          \t   (x != null && y != null && java.util.Arrays.equals((byte[]) x, (byte[]) y)); \n    }\n    public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws SQLException {\n    \tif (this.lobHandler == null) {\n    \t\tthrow new IllegalStateException(\"No LobHandler found for configuration - lobHandler property must be set on LocalSessionFactoryBean\");\n    \t}\n    \treturn this.lobHandler.getBlobAsBytes(rs, rs.findColumn(names[0]));\n    }\n    public void nullSafeSet(PreparedStatement st, Object value, int index) throws SQLException {\n        if (this.lobHandler == null) {\n    \t\tthrow new IllegalStateException(\"No LobHandler found for configuration - lobHandler property must be set on LocalSessionFactoryBean\");\n    \t}\n    \tif (!TransactionSynchronizationManager.isSynchronizationActive()) {\n    \t\tthrow new IllegalStateException(\"BlobBinaryType requires active transaction synchronization\");\n    \t}\n    \tLobCreator lobCreator = this.lobHandler.getLobCreator();\n    \tlobCreator.setBlobAsBytes(st, index, (byte[]) value);\n    \tTransactionSynchronizationManager.registerSynchronization(new LobCreatorSynchronization(lobCreator));\n    }\n    public Object deepCopy(Object value) {\n        // check: create new byte[]?\n    \treturn value;\n    }\n    public boolean isMutable() {\n    \treturn false;\n    }\t\n    \n    /**\n     * Callback for resource cleanup at the end of a transaction.\n     * Invokes LobCreator.close to clean up temporary LOBs that might have been created.\n     * @see org.springframework.jdbc.support.lob.LobCreator#close\n     */\n    private static class LobCreatorSynchronization extends TransactionSynchronizationAdapter {\n    \n        private final LobCreator lobCreator;\n    \n    \tprivate LobCreatorSynchronization(LobCreator lobCreator) {\n    \t\tthis.lobCreator = lobCreator;\n    \t}\n    \n    \tpublic void beforeCompletion() {\n    \t\tthis.lobCreator.close();\n    \t}\n    }\n\n}\n\n---\n\nNo further details from [SPR-258](https://jira.spring.io/browse/SPR-258?redirect=false)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453286837"], "labels":["in: data","type: enhancement"]}