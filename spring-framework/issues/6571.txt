{"id":"6571", "title":"Provide BeanRegistrar interface and extend ApplicationContext implementations to call registrars defined in the application context [SPR-1877]", "body":"**[Chris Lee](https://jira.spring.io/secure/ViewProfile.jspa?name=chrislee)** opened **[SPR-1877](https://jira.spring.io/browse/SPR-1877?redirect=false)** and commented

Provide a hook & interface (BeanRegistrar) by which beans can be dynamically registered as the application context starts up.

We are using this to register custom JSP tags annotated with `@Configurable` (if they aren't already manually registered); there are certainly other uses.

BeanRegistrar:

public interface BeanRegistrar
{
void registerBeans( BeanDefinitionRegistry beanDefinitionRegistry ) throws BeansException;
}

XmlApplicationContext changes:

`@SuppressWarnings`( \"unchecked\" )
`@Override`
protected void loadBeanDefinitions( DefaultListableBeanFactory beanFactory ) throws IOException
{
super.loadBeanDefinitions( beanFactory );

        registerBeans( beanFactory );
    }
    
    private void registerBeans( DefaultListableBeanFactory beanFactory )
    {
        String[] beanRegistrarNames = BeanFactoryUtils.beanNamesForTypeIncludingAncestors(
                beanFactory, BeanRegistrar.class, false, false );
    
        for( String beanRegistrarName : beanRegistrarNames )
        {
            BeanRegistrar registrar = (BeanRegistrar)beanFactory.getBean( beanRegistrarName );
            registrar.registerBeans( beanFactory );
        }
    }



---

**Affects:** 2.1 M4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6571","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6571/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6571/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6571/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6571","id":398064905,"node_id":"MDU6SXNzdWUzOTgwNjQ5MDU=","number":6571,"title":"Provide BeanRegistrar interface and extend ApplicationContext implementations to call registrars defined in the application context [SPR-1877]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false,"description":"Issues in core modules (aop, beans, core, context, expression)"},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false,"description":"A duplicate of another issue"},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false,"description":"A general enhancement"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2006-04-10T07:36:30Z","updated_at":"2012-06-13T08:30:04Z","closed_at":"2012-06-13T08:30:04Z","author_association":"COLLABORATOR","body":"**[Chris Lee](https://jira.spring.io/secure/ViewProfile.jspa?name=chrislee)** opened **[SPR-1877](https://jira.spring.io/browse/SPR-1877?redirect=false)** and commented\n\nProvide a hook & interface (BeanRegistrar) by which beans can be dynamically registered as the application context starts up.\n\nWe are using this to register custom JSP tags annotated with `@Configurable` (if they aren't already manually registered); there are certainly other uses.\n\nBeanRegistrar:\n\npublic interface BeanRegistrar\n{\nvoid registerBeans( BeanDefinitionRegistry beanDefinitionRegistry ) throws BeansException;\n}\n\nXmlApplicationContext changes:\n\n`@SuppressWarnings`( \"unchecked\" )\n`@Override`\nprotected void loadBeanDefinitions( DefaultListableBeanFactory beanFactory ) throws IOException\n{\nsuper.loadBeanDefinitions( beanFactory );\n\n        registerBeans( beanFactory );\n    }\n    \n    private void registerBeans( DefaultListableBeanFactory beanFactory )\n    {\n        String[] beanRegistrarNames = BeanFactoryUtils.beanNamesForTypeIncludingAncestors(\n                beanFactory, BeanRegistrar.class, false, false );\n    \n        for( String beanRegistrarName : beanRegistrarNames )\n        {\n            BeanRegistrar registrar = (BeanRegistrar)beanFactory.getBean( beanRegistrarName );\n            registrar.registerBeans( beanFactory );\n        }\n    }\n\n\n\n---\n\n**Affects:** 2.1 M4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453303414"], "labels":["in: core","status: duplicate","type: enhancement"]}