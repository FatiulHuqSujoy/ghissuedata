{"id":"21820", "title":"Regression in 5.1 RC3: BeanDefinitionOverrideException against mock config [SPR-17287]", "body":"**[David J. M. Karlsen](https://jira.spring.io/secure/ViewProfile.jspa?name=davidkarlsen)** opened **[SPR-17287](https://jira.spring.io/browse/SPR-17287?redirect=false)** and commented

Using spring boot 2.1.0.M3, which is spring-framework v5.1.0RC3, I get:

 

```

ava.lang.IllegalStateException: Failed to load ApplicationContext Caused by: org.springframework.beans.factory.support.BeanDefinitionOverrideException: Invalid bean definition with name 'cachedConnectionFactory' defined in class path resource [com/edb/fs/tac/jfr/srv/ws/infra/mq/MqConfig.class]: Cannot register bean definition [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=true; factoryBeanName=mqConfig; factoryMethodName=connectionFactory; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/edb/fs/tac/jfr/srv/ws/infra/mq/MqConfig.class]] for bean 'cachedConnectionFactory': There is already [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=camelCxfTransportTest.MockConfig; factoryMethodName=cachedConnectionFactory; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/edb/fs/tac/jfr/srv/ws/CamelCxfTransportTest$MockConfig.class]] bound.

```

 

which I do not do with latest GA releases of sb / sf.

 

The mock config is used as an inner config bean in another test:

```

@Configuration
static class MockConfig {
 @Bean
 public DestinationManager destinationManager() {
 return new DestinationManager();
 }

 @Bean
 public ConfigurationManager configurationManager() {
 return new ConfigurationManager();
 }

 @Bean
 public MockConnectionFactory cachedConnectionFactory( ConfigurationManager configurationManager,
 DestinationManager destinationManager ) {
 return new MockConnectionFactory(destinationManager,configurationManager);
 }

 @Bean
 public JmsTransactionManager jmsTransactionManager(ConnectionFactory connectionFactory) {
 return new JmsTransactionManager(connectionFactory);
 }

 @Bean
 public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
 return new JmsTemplate(connectionFactory);
 }
}

```

My failing smoke test:

```

@ExtendWith( SpringExtension.class )
@ActiveProfiles( {
 \"eammock\",
 \"noOpCache\",
 \"dev\"
})
@SpringBootTest( classes = \\{ Application.class } )
public class ApplicationBootupSmokeTest
 extends AbstractTest
{

 @Inject
 private ConfigurableWebApplicationContext webApplicationContext;

 @Test
 public void testAbleToCreateWebAppContext() {
 assertThat(webApplicationContext ).isNotNull();
 assertThat(webApplicationContext.isActive() ).isTrue();
 }
}

```

 

I don't think the other test should pick up the 1st tests inner mock-config?

---

**Affects:** 5.1 RC3

**Issue Links:**
- #21838 Regression in Spring 5.1

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21820","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21820/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21820/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21820/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21820","id":398234167,"node_id":"MDU6SXNzdWUzOTgyMzQxNjc=","number":21820,"title":"Regression in 5.1 RC3: BeanDefinitionOverrideException against mock config [SPR-17287]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false},"assignees":[{"login":"bclozel","id":103264,"node_id":"MDQ6VXNlcjEwMzI2NA==","avatar_url":"https://avatars2.githubusercontent.com/u/103264?v=4","gravatar_id":"","url":"https://api.github.com/users/bclozel","html_url":"https://github.com/bclozel","followers_url":"https://api.github.com/users/bclozel/followers","following_url":"https://api.github.com/users/bclozel/following{/other_user}","gists_url":"https://api.github.com/users/bclozel/gists{/gist_id}","starred_url":"https://api.github.com/users/bclozel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bclozel/subscriptions","organizations_url":"https://api.github.com/users/bclozel/orgs","repos_url":"https://api.github.com/users/bclozel/repos","events_url":"https://api.github.com/users/bclozel/events{/privacy}","received_events_url":"https://api.github.com/users/bclozel/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2018-09-18T18:11:18Z","updated_at":"2019-01-12T05:18:11Z","closed_at":"2018-09-27T03:08:50Z","author_association":"COLLABORATOR","body":"**[David J. M. Karlsen](https://jira.spring.io/secure/ViewProfile.jspa?name=davidkarlsen)** opened **[SPR-17287](https://jira.spring.io/browse/SPR-17287?redirect=false)** and commented\n\nUsing spring boot 2.1.0.M3, which is spring-framework v5.1.0RC3, I get:\n\n \n\n```\n\r\nava.lang.IllegalStateException: Failed to load ApplicationContext Caused by: org.springframework.beans.factory.support.BeanDefinitionOverrideException: Invalid bean definition with name 'cachedConnectionFactory' defined in class path resource [com/edb/fs/tac/jfr/srv/ws/infra/mq/MqConfig.class]: Cannot register bean definition [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=true; factoryBeanName=mqConfig; factoryMethodName=connectionFactory; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/edb/fs/tac/jfr/srv/ws/infra/mq/MqConfig.class]] for bean 'cachedConnectionFactory': There is already [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=camelCxfTransportTest.MockConfig; factoryMethodName=cachedConnectionFactory; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/edb/fs/tac/jfr/srv/ws/CamelCxfTransportTest$MockConfig.class]] bound.\r\n\r\n```\n\n \n\nwhich I do not do with latest GA releases of sb / sf.\n\n \n\nThe mock config is used as an inner config bean in another test:\n\n```\n\r\n@Configuration\r\nstatic class MockConfig {\r\n @Bean\r\n public DestinationManager destinationManager() {\r\n return new DestinationManager();\r\n }\r\n\r\n @Bean\r\n public ConfigurationManager configurationManager() {\r\n return new ConfigurationManager();\r\n }\r\n\r\n @Bean\r\n public MockConnectionFactory cachedConnectionFactory( ConfigurationManager configurationManager,\r\n DestinationManager destinationManager ) {\r\n return new MockConnectionFactory(destinationManager,configurationManager);\r\n }\r\n\r\n @Bean\r\n public JmsTransactionManager jmsTransactionManager(ConnectionFactory connectionFactory) {\r\n return new JmsTransactionManager(connectionFactory);\r\n }\r\n\r\n @Bean\r\n public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {\r\n return new JmsTemplate(connectionFactory);\r\n }\r\n}\r\n\r\n```\n\nMy failing smoke test:\n\n```\n\r\n@ExtendWith( SpringExtension.class )\r\n@ActiveProfiles( {\r\n \"eammock\",\r\n \"noOpCache\",\r\n \"dev\"\r\n})\r\n@SpringBootTest( classes = \\{ Application.class } )\r\npublic class ApplicationBootupSmokeTest\r\n extends AbstractTest\r\n{\r\n\r\n @Inject\r\n private ConfigurableWebApplicationContext webApplicationContext;\r\n\r\n @Test\r\n public void testAbleToCreateWebAppContext() {\r\n assertThat(webApplicationContext ).isNotNull();\r\n assertThat(webApplicationContext.isActive() ).isTrue();\r\n }\r\n}\r\n\r\n```\n\n \n\nI don't think the other test should pick up the 1st tests inner mock-config?\n\n---\n\n**Affects:** 5.1 RC3\n\n**Issue Links:**\n- #21838 Regression in Spring 5.1\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453477310","453477311"], "labels":["in: core","status: invalid"]}