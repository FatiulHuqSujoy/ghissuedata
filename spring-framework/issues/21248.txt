{"id":"21248", "title":"@GroupSequenceProvider Not Provided the Bean [SPR-16707]", "body":"**[Peter Luttrell](https://jira.spring.io/secure/ViewProfile.jspa?name=pluttrell)** opened **[SPR-16707](https://jira.spring.io/browse/SPR-16707?redirect=false)** and commented

Using SpringBoot 2.0.1, the BeanValidation support is provided by the HibernateValidator. The HibernateValidator provides a feature that is not part of the BeanValidations spec that allows you to define a `@GroupSequenceProvider` to control the list and order of groups used during validation: https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/?v=6.0#_code_groupsequenceprovider_code

I've tried to use this, and execution path enters the GroupSequenceProvider that I've defined for an entity, but the bean supplied to the getValidationGroups method is always null, so we can't dynamically assigned the groups based on the state of the supplied value.

Here's a simple example:

```java
@RestController
public class CarController {
  @PostMapping(\"/cars\")
  Car register(@RequestBody @Valid Car car) {
    return car;
  }
}
```

```java
@GroupSequenceProvider(CarGroupSequenceProvider.class)
public class Car {

  @NotEmpty
  private String make;

  @NotEmpty(groups = RegistrationGroup.class)
  private String vin;

  private String registrationCode;

  public String getMake() { return make; }
  public void setMake(String make) { this.make = make; }
  public String getVin() { return vin; }
  public void setVin(String vin) { this.vin = vin; }
  public String getRegistrationCode() { return registrationCode; } 
  public void setRegistrationCode(String registrationCode) { this.registrationCode = registrationCode; }
}
```

```java
public interface RegistrationGroup {}
```

```java
public class CarGroupSequenceProvider implements DefaultGroupSequenceProvider<Car> {
   public CarGroupSequenceProvider() {}

  @Override
  public List<Class<?>> getValidationGroups(Car car) {
    List<Class<?>> defaultGroupSequence = new ArrayList<>();
    if(car.getRegistrationCode() != null){
      defaultGroupSequence.add(RegistrationGroup.class);
    }
    return defaultGroupSequence;
  }
}
```

In this last code snippet, the parameter `car` is always null, so we can't look at the state of it to determine if additional Groups should be defined.


---

**Affects:** 5.0.5

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/22edab852da456bea728daf84df09f0047e1d13f

1 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21248","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21248/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21248/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21248/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21248","id":398226004,"node_id":"MDU6SXNzdWUzOTgyMjYwMDQ=","number":21248,"title":"@GroupSequenceProvider Not Provided the Bean [SPR-16707]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2018-04-10T19:26:46Z","updated_at":"2019-01-12T16:21:29Z","closed_at":"2018-05-30T08:55:56Z","author_association":"COLLABORATOR","body":"**[Peter Luttrell](https://jira.spring.io/secure/ViewProfile.jspa?name=pluttrell)** opened **[SPR-16707](https://jira.spring.io/browse/SPR-16707?redirect=false)** and commented\n\nUsing SpringBoot 2.0.1, the BeanValidation support is provided by the HibernateValidator. The HibernateValidator provides a feature that is not part of the BeanValidations spec that allows you to define a `@GroupSequenceProvider` to control the list and order of groups used during validation: https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/?v=6.0#_code_groupsequenceprovider_code\n\nI've tried to use this, and execution path enters the GroupSequenceProvider that I've defined for an entity, but the bean supplied to the getValidationGroups method is always null, so we can't dynamically assigned the groups based on the state of the supplied value.\n\nHere's a simple example:\n\n```java\n@RestController\npublic class CarController {\n  @PostMapping(\"/cars\")\n  Car register(@RequestBody @Valid Car car) {\n    return car;\n  }\n}\n```\n\n```java\n@GroupSequenceProvider(CarGroupSequenceProvider.class)\npublic class Car {\n\n  @NotEmpty\n  private String make;\n\n  @NotEmpty(groups = RegistrationGroup.class)\n  private String vin;\n\n  private String registrationCode;\n\n  public String getMake() { return make; }\n  public void setMake(String make) { this.make = make; }\n  public String getVin() { return vin; }\n  public void setVin(String vin) { this.vin = vin; }\n  public String getRegistrationCode() { return registrationCode; } \n  public void setRegistrationCode(String registrationCode) { this.registrationCode = registrationCode; }\n}\n```\n\n```java\npublic interface RegistrationGroup {}\n```\n\n```java\npublic class CarGroupSequenceProvider implements DefaultGroupSequenceProvider<Car> {\n   public CarGroupSequenceProvider() {}\n\n  @Override\n  public List<Class<?>> getValidationGroups(Car car) {\n    List<Class<?>> defaultGroupSequence = new ArrayList<>();\n    if(car.getRegistrationCode() != null){\n      defaultGroupSequence.add(RegistrationGroup.class);\n    }\n    return defaultGroupSequence;\n  }\n}\n```\n\nIn this last code snippet, the parameter `car` is always null, so we can't look at the state of it to determine if additional Groups should be defined.\n\n\n---\n\n**Affects:** 5.0.5\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/22edab852da456bea728daf84df09f0047e1d13f\n\n1 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453470344"], "labels":["in: web","status: declined"]}