{"id":"13078", "title":"Regression in support for property write method return type covariance [SPR-8432]", "body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-8432](https://jira.spring.io/browse/SPR-8432?redirect=false)** and commented

Regression in 3.1 M2. When subclass overrides getter with narrower return type, Spring fails to set property.

```
 
public class ReadWritePropertyErrorMain { 

    public static void main(String[] args) { 

        final String contextXml = 
            \"<beans xmlns='http://www.springframework.org/schema/beans&#39;\" + 
            \" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance&#39;\" + 
            \" xsi:schemaLocation='\" + 
            \" http://www.springframework.org/schema/beans\" + 
            \" http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\" + 
            \" '\" + 
            \">\" + 
            \" <bean id='t' class='\" + MyTemplate5.class.getName() + \"'/>\" + 
            \" <bean class='\" + MyDaoSupport5.class.getName() + \"'>\" + 
            \" <property name='myTemplate4' ref='t'/>\" + 
            \" </bean>\" + 
            \"</beans>\"; 

        AbstractXmlApplicationContext applicationContext = new AbstractXmlApplicationContext() { 
            @Override 
            protected Resource[] getConfigResources() { 
                return new Resource[] { new ByteArrayResource(contextXml.getBytes()) }; 
            } 
        }; 

        applicationContext.refresh(); 

        System.out.println(\"$\"); 
    } 
} 

public class MyDaoSupport4 { 

    private MyTemplate4 myTemplate4; 

    public void setMyTemplate4(MyTemplate4 jdbcTemplate4) { 
        this.myTemplate4 = jdbcTemplate4; 
    } 

    public MyTemplate4 getMyTemplate4() { 
        return myTemplate4; 
    } 

} 

public class MyDaoSupport5 extends MyDaoSupport4 { 

    @Override 
    public MyTemplate5 getMyTemplate4() { // <-- narrow subtype is here 
        return (MyTemplate5) super.getMyTemplate4(); 
    } 

} 

public class MyTemplate4 { } 

public class MyTemplate5 extends MyTemplate4 { } 
```

Error:

```
 
Exception in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'ru.yandex.commune.junk.stepancheg.spring.readWrite.MyDaoSupport5#0' defined in resource loaded from byte array: Initialization of bean failed; nested exception is java.lang.IllegalStateException: Read and write parameter types are not the same 
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:527) 
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:456) 
at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:295) 
at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:225) 
at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:292) 
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:194) 
at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:580) 
at org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:913) 
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:464) 
at ru.yandex.commune.junk.stepancheg.spring.readWrite.ReadWriteProperty.main(ReadWriteProperty.java:36) 
Caused by: java.lang.IllegalStateException: Read and write parameter types are not the same 
at org.springframework.core.convert.Property.resolveMethodParameter(Property.java:137) 
at org.springframework.core.convert.Property.<init>(Property.java:56) 
at org.springframework.beans.BeanWrapperImpl.property(BeanWrapperImpl.java:1174) 
at org.springframework.beans.BeanWrapperImpl.convertForProperty(BeanWrapperImpl.java:497) 
at org.springframework.beans.BeanWrapperImpl.convertForProperty(BeanWrapperImpl.java:490) 
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.convertForProperty(AbstractAutowireCapableBeanFactory.java:1371) 
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1330)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:1086) 
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:517) 
... 9 more 
```

---

**Affects:** 3.1 M2

**Attachments:**
- [mylyn-context.zip](https://jira.spring.io/secure/attachment/18346/mylyn-context.zip) (_8.61 kB_)

**Issue Links:**
- #14663 Overhaul non-void JavaBean write method support

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4a8be690998f43a9253305e09f96ad83058ef540, https://github.com/spring-projects/spring-framework/commit/0ee12563b038797a137ac67b13e5dd6d3d918a7f, https://github.com/spring-projects/spring-framework/commit/5dc2d56600368e27da113dc31facd487b555b16d
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13078","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13078/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13078/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13078/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13078","id":398112867,"node_id":"MDU6SXNzdWUzOTgxMTI4Njc=","number":13078,"title":"Regression in support for property write method return type covariance [SPR-8432]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511994,"node_id":"MDU6TGFiZWwxMTg4NTExOTk0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20regression","name":"type: regression","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/77","html_url":"https://github.com/spring-projects/spring-framework/milestone/77","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/77/labels","id":3960850,"node_id":"MDk6TWlsZXN0b25lMzk2MDg1MA==","number":77,"title":"3.1 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":140,"state":"closed","created_at":"2019-01-10T22:03:24Z","updated_at":"2019-01-11T03:23:16Z","due_on":"2011-10-10T07:00:00Z","closed_at":"2019-01-10T22:03:24Z"},"comments":4,"created_at":"2011-06-10T06:48:01Z","updated_at":"2019-01-11T13:37:31Z","closed_at":"2012-11-25T04:25:27Z","author_association":"COLLABORATOR","body":"**[Stepan Koltsov](https://jira.spring.io/secure/ViewProfile.jspa?name=yozh)** opened **[SPR-8432](https://jira.spring.io/browse/SPR-8432?redirect=false)** and commented\n\nRegression in 3.1 M2. When subclass overrides getter with narrower return type, Spring fails to set property.\n\n```\n \npublic class ReadWritePropertyErrorMain { \n\n    public static void main(String[] args) { \n\n        final String contextXml = \n            \"<beans xmlns='http://www.springframework.org/schema/beans&#39;\" + \n            \" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance&#39;\" + \n            \" xsi:schemaLocation='\" + \n            \" http://www.springframework.org/schema/beans\" + \n            \" http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\" + \n            \" '\" + \n            \">\" + \n            \" <bean id='t' class='\" + MyTemplate5.class.getName() + \"'/>\" + \n            \" <bean class='\" + MyDaoSupport5.class.getName() + \"'>\" + \n            \" <property name='myTemplate4' ref='t'/>\" + \n            \" </bean>\" + \n            \"</beans>\"; \n\n        AbstractXmlApplicationContext applicationContext = new AbstractXmlApplicationContext() { \n            @Override \n            protected Resource[] getConfigResources() { \n                return new Resource[] { new ByteArrayResource(contextXml.getBytes()) }; \n            } \n        }; \n\n        applicationContext.refresh(); \n\n        System.out.println(\"$\"); \n    } \n} \n\npublic class MyDaoSupport4 { \n\n    private MyTemplate4 myTemplate4; \n\n    public void setMyTemplate4(MyTemplate4 jdbcTemplate4) { \n        this.myTemplate4 = jdbcTemplate4; \n    } \n\n    public MyTemplate4 getMyTemplate4() { \n        return myTemplate4; \n    } \n\n} \n\npublic class MyDaoSupport5 extends MyDaoSupport4 { \n\n    @Override \n    public MyTemplate5 getMyTemplate4() { // <-- narrow subtype is here \n        return (MyTemplate5) super.getMyTemplate4(); \n    } \n\n} \n\npublic class MyTemplate4 { } \n\npublic class MyTemplate5 extends MyTemplate4 { } \n```\n\nError:\n\n```\n \nException in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'ru.yandex.commune.junk.stepancheg.spring.readWrite.MyDaoSupport5#0' defined in resource loaded from byte array: Initialization of bean failed; nested exception is java.lang.IllegalStateException: Read and write parameter types are not the same \nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:527) \nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:456) \nat org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:295) \nat org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:225) \nat org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:292) \nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:194) \nat org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:580) \nat org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:913) \nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:464) \nat ru.yandex.commune.junk.stepancheg.spring.readWrite.ReadWriteProperty.main(ReadWriteProperty.java:36) \nCaused by: java.lang.IllegalStateException: Read and write parameter types are not the same \nat org.springframework.core.convert.Property.resolveMethodParameter(Property.java:137) \nat org.springframework.core.convert.Property.<init>(Property.java:56) \nat org.springframework.beans.BeanWrapperImpl.property(BeanWrapperImpl.java:1174) \nat org.springframework.beans.BeanWrapperImpl.convertForProperty(BeanWrapperImpl.java:497) \nat org.springframework.beans.BeanWrapperImpl.convertForProperty(BeanWrapperImpl.java:490) \nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.convertForProperty(AbstractAutowireCapableBeanFactory.java:1371) \nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.applyPropertyValues(AbstractAutowireCapableBeanFactory.java:1330)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:1086) \nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:517) \n... 9 more \n```\n\n---\n\n**Affects:** 3.1 M2\n\n**Attachments:**\n- [mylyn-context.zip](https://jira.spring.io/secure/attachment/18346/mylyn-context.zip) (_8.61 kB_)\n\n**Issue Links:**\n- #14663 Overhaul non-void JavaBean write method support\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4a8be690998f43a9253305e09f96ad83058ef540, https://github.com/spring-projects/spring-framework/commit/0ee12563b038797a137ac67b13e5dd6d3d918a7f, https://github.com/spring-projects/spring-framework/commit/5dc2d56600368e27da113dc31facd487b555b16d\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453360112","453360115","453360116","453360117"], "labels":["in: core","type: regression"]}