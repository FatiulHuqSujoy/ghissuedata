{"id":"13684", "title":"Support simplified main class application context / dependency management [SPR-9044]", "body":"**[Bob Tiernay](https://jira.spring.io/secure/ViewProfile.jspa?name=btiernay)** opened **[SPR-9044](https://jira.spring.io/browse/SPR-9044?redirect=false)** and commented

## Problem

One common request I see quite frequently on the internet is the ability to have spring autowire a main class:

- http://hvijay.wordpress.com/2011/07/02/spring-dependency-injection-in-applications-main-class/
- http://forum.springsource.org/archive/index.php/t-47933.html
- http://stackoverflow.com/questions/6820724/how-to-inject-properties-into-a-spring-bean-from-main-class
- http://stackoverflow.com/questions/8313070/spring-bean-injection-in-main-method-class
- http://stackoverflow.com/questions/3659720/spring-3-autowire-in-standalone-application
- http://stackoverflow.com/questions/4787719/spring-console-application-configured-using-annotations
- http://forum.springsource.org/showthread.php?75991-ContextConfiguration

There is no shortage of questions / solutions to this problem which illustrates a need for a standardized approach.

## Solution

What I am proposing is combination of features between the following existing classes:

1. `org.springframework.test.context.junit4.SpringJUnit4ClassRunner` - provides functionality of the Spring TestContext Framework to standard JUnit 4.5+ tests
2. `org.springframework.test.context.ContextConfiguration` - class-level metadata that is used to determine how to load and configure an ApplicationContext for test classes
3. `org.springframework.web.context.support.SpringBeanAutowiringSupport` a convenient base class for self-autowiring classes that gets constructed within a Spring-based web application.

Thus provinding first class support for console based application context management and DI.

## Features

1. Autowiring of main class dependencies
2. Auto-destruction of application context
3. Ability in the future to handle command line arguments as beans

## Properties

1. Standardization
2. Consistency with other APIs
3. Minimal configuration
4. Intent revealing

## Example

My visions is something like the following:

```
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MainSupport;
import org.springframework.context.ContextConfiguration;
import org.springframework.context.support.AnnotationConfigContextLoader;

@ContextConfiguration(classes=AppConfig.class, loader=AnnotationConfigContextLoader.class)
public class Main extends MainSupport {
    
    @Autowired
    private Service service;

    public static void main( String[] args ) {
        // MainSupport.execute
        execute(args);
    }
    
    @Main
    public void main() {
        service.run();
    }
    
}
```

The way this would work is as follows:

1. JVM calls `main`
2. `main` delegates to super class method `MainSupport.execute` provided by spring (analog of `SpringBeanAutowiringSupport`)
3. `MainSupport.execute` reads the `@ContextConfiguration` annotation and looks for a `@Main` annotation (analog of `@Test`)
4. `MainSupport.execute` creates the application context
5. `MainSupport.execute` create an application `Main` class instance
6. `MainSupport.execute` injects dependencies
7. `MainSupport.execute` wraps instance in a proxy that will advise the method annotated with `@Main`. This proxy will first call the annotated method and then destroy the application context

---

**Issue Links:**
- #12732 Provide Java main() for launching Spring applications

4 votes, 10 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13684","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13684/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13684/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13684/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13684","id":398116777,"node_id":"MDU6SXNzdWUzOTgxMTY3Nzc=","number":13684,"title":"Support simplified main class application context / dependency management [SPR-9044]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2012-01-21T07:06:07Z","updated_at":"2019-01-11T22:24:56Z","closed_at":"2013-08-06T15:00:00Z","author_association":"COLLABORATOR","body":"**[Bob Tiernay](https://jira.spring.io/secure/ViewProfile.jspa?name=btiernay)** opened **[SPR-9044](https://jira.spring.io/browse/SPR-9044?redirect=false)** and commented\n\n## Problem\n\nOne common request I see quite frequently on the internet is the ability to have spring autowire a main class:\n\n- http://hvijay.wordpress.com/2011/07/02/spring-dependency-injection-in-applications-main-class/\n- http://forum.springsource.org/archive/index.php/t-47933.html\n- http://stackoverflow.com/questions/6820724/how-to-inject-properties-into-a-spring-bean-from-main-class\n- http://stackoverflow.com/questions/8313070/spring-bean-injection-in-main-method-class\n- http://stackoverflow.com/questions/3659720/spring-3-autowire-in-standalone-application\n- http://stackoverflow.com/questions/4787719/spring-console-application-configured-using-annotations\n- http://forum.springsource.org/showthread.php?75991-ContextConfiguration\n\nThere is no shortage of questions / solutions to this problem which illustrates a need for a standardized approach.\n\n## Solution\n\nWhat I am proposing is combination of features between the following existing classes:\n\n1. `org.springframework.test.context.junit4.SpringJUnit4ClassRunner` - provides functionality of the Spring TestContext Framework to standard JUnit 4.5+ tests\n2. `org.springframework.test.context.ContextConfiguration` - class-level metadata that is used to determine how to load and configure an ApplicationContext for test classes\n3. `org.springframework.web.context.support.SpringBeanAutowiringSupport` a convenient base class for self-autowiring classes that gets constructed within a Spring-based web application.\n\nThus provinding first class support for console based application context management and DI.\n\n## Features\n\n1. Autowiring of main class dependencies\n2. Auto-destruction of application context\n3. Ability in the future to handle command line arguments as beans\n\n## Properties\n\n1. Standardization\n2. Consistency with other APIs\n3. Minimal configuration\n4. Intent revealing\n\n## Example\n\nMy visions is something like the following:\n\n```\nimport org.springframework.beans.factory.annotation.Autowired;\nimport org.springframework.context.MainSupport;\nimport org.springframework.context.ContextConfiguration;\nimport org.springframework.context.support.AnnotationConfigContextLoader;\n\n@ContextConfiguration(classes=AppConfig.class, loader=AnnotationConfigContextLoader.class)\npublic class Main extends MainSupport {\n    \n    @Autowired\n    private Service service;\n\n    public static void main( String[] args ) {\n        // MainSupport.execute\n        execute(args);\n    }\n    \n    @Main\n    public void main() {\n        service.run();\n    }\n    \n}\n```\n\nThe way this would work is as follows:\n\n1. JVM calls `main`\n2. `main` delegates to super class method `MainSupport.execute` provided by spring (analog of `SpringBeanAutowiringSupport`)\n3. `MainSupport.execute` reads the `@ContextConfiguration` annotation and looks for a `@Main` annotation (analog of `@Test`)\n4. `MainSupport.execute` creates the application context\n5. `MainSupport.execute` create an application `Main` class instance\n6. `MainSupport.execute` injects dependencies\n7. `MainSupport.execute` wraps instance in a proxy that will advise the method annotated with `@Main`. This proxy will first call the annotated method and then destroy the application context\n\n---\n\n**Issue Links:**\n- #12732 Provide Java main() for launching Spring applications\n\n4 votes, 10 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453364963","453364964","453364966","453364967"], "labels":["in: core","status: declined","type: enhancement"]}