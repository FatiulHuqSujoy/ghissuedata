{"id":"14139", "title":"Environment (if provided by an ApplicationContext) should be able to convert String->Resource [SPR-9505]", "body":"**[Dave Syer](https://jira.spring.io/secure/ViewProfile.jspa?name=david_syer)** opened **[SPR-9505](https://jira.spring.io/browse/SPR-9505?redirect=false)** and commented

Environment (if provided by an ApplicationContext) should be able to convert String->Resource.  Example, today I have to inject a ResourceLoader explicitly:

```
public class ExampleConfiguration {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@PostConstruct
	protected void initialize() {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScript(resourceLoader.getResource(environment.getProperty(\"batch.schema.script\")));
		DatabasePopulatorUtils.execute(populator , dataSource());
	}
...
```

It would be better to be able to do this

```
public class ExampleConfiguration {
	
	@Autowired
	private Environment environment;
	
	@PostConstruct
	protected void initialize() {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScript(environment.getProperty(\"batch.schema.script\", Resource.class));
		DatabasePopulatorUtils.execute(populator , dataSource());
	}
...
```



---

**Affects:** 3.1.1

1 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14139","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14139/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14139/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14139/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14139","id":398151184,"node_id":"MDU6SXNzdWUzOTgxNTExODQ=","number":14139,"title":"Environment (if provided by an ApplicationContext) should be able to convert String->Resource [SPR-9505]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2012-06-13T08:24:15Z","updated_at":"2015-09-09T12:16:00Z","closed_at":"2015-09-09T12:16:00Z","author_association":"COLLABORATOR","body":"**[Dave Syer](https://jira.spring.io/secure/ViewProfile.jspa?name=david_syer)** opened **[SPR-9505](https://jira.spring.io/browse/SPR-9505?redirect=false)** and commented\n\nEnvironment (if provided by an ApplicationContext) should be able to convert String->Resource.  Example, today I have to inject a ResourceLoader explicitly:\n\n```\npublic class ExampleConfiguration {\n\t\n\t@Autowired\n\tprivate Environment environment;\n\t\n\t@Autowired\n\tprivate ResourceLoader resourceLoader;\n\t\n\t@PostConstruct\n\tprotected void initialize() {\n\t\tResourceDatabasePopulator populator = new ResourceDatabasePopulator();\n\t\tpopulator.addScript(resourceLoader.getResource(environment.getProperty(\"batch.schema.script\")));\n\t\tDatabasePopulatorUtils.execute(populator , dataSource());\n\t}\n...\n```\n\nIt would be better to be able to do this\n\n```\npublic class ExampleConfiguration {\n\t\n\t@Autowired\n\tprivate Environment environment;\n\t\n\t@PostConstruct\n\tprotected void initialize() {\n\t\tResourceDatabasePopulator populator = new ResourceDatabasePopulator();\n\t\tpopulator.addScript(environment.getProperty(\"batch.schema.script\", Resource.class));\n\t\tDatabasePopulatorUtils.execute(populator , dataSource());\n\t}\n...\n```\n\n\n\n---\n\n**Affects:** 3.1.1\n\n1 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453394834","453394835"], "labels":["in: core","status: declined","type: enhancement"]}