{"id":"12115", "title":"PropertiesLoaderSupport throws error \"'whatever' does not carry a filename\" if you feed a ByteArrayResource to it. [SPR-7457]", "body":"**[Haroldo de Oliveira Pinheiro](https://jira.spring.io/secure/ViewProfile.jspa?name=haroldo-ok-ats)** opened **[SPR-7457](https://jira.spring.io/browse/SPR-7457?redirect=false)** and commented

I'm having a problem where I'm trying to feed a ByteArrayResource to a PropertyPlaceholderConfigurer, and getting an error like \"PropertiesLoaderSupport throws error \"'whatever' does not carry a filename\" if you feed a ByteArrayResource to it.\"
I saw a previous similar issue at https://jira.springframework.org/browse/SPR-5068, where it was argued that doing so is unusual and unnecessary. But in my case, I'm using the ByteArrayResource to hold some values that are actually computed at runtime, and I wouldn't really like to have to create a temporary file just for that single reason.

The XML mapping is like this:

{{
\\<bean class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">
\\<property name=\"locations\">
\\<bean class=\"com.ats.framework.service.config.impl.ApplicationConfigResourceArrayFactory\">
\\<property name=\"applicationConfigService\" ref=\"applicationConfigService\" />
\\<property name=\"configLocation\" value=\"@{aplicacao.config}/application.properties\" />
\\</bean>
\\</property>
\\</bean>
}}

ApplicationConfigResourceArrayFactory:

{{
public class ApplicationConfigResourceArrayFactory implements FactoryBean, ResourceLoaderAware {
private static final Logger logger = Logger.getLogger(ApplicationConfigResourceArrayFactory.class);

    private static final String RESOURCE_PATH_PROPERTY = \"aplicacao.config\";	
    private static final String RESOURCE_PATH_REFERENCE = \"@{\" + RESOURCE_PATH_PROPERTY + \"}\";	
    private static final String COMENTARIO_PROPRIEDADES_SISTEMA = \"Propriedades do sistema montadas em tempo de execução.\";
    
    private ApplicationConfigService applicationConfigService;
    private ResourceLoader resourceLoader;
    private String[] configLocations;
    
    @Override
    public Object getObject() throws Exception {
    	String[] nomes = this.getConfigLocations();
    	List<Resource> lista = new ArrayList<Resource>(nomes.length);
    	
    	this.loadConfigFiles(lista, nomes);		
    	this.loadComputedProperties(lista);
    	
    	return lista.toArray(new Resource[lista.size()]);
    }
    
    private void loadComputedProperties(List<Resource> lista)
    		throws IOException {
    	Properties properties = new Properties();
    	properties.put(RESOURCE_PATH_PROPERTY, this.getURIBase().toString());
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
    	properties.store(bout, COMENTARIO_PROPRIEDADES_SISTEMA);
    	lista.add(new ByteArrayResource(bout.toByteArray(), COMENTARIO_PROPRIEDADES_SISTEMA));
    }
    
    private void loadConfigFiles(List<Resource> lista, String[] nomes) {
    	for (int i = 0; i != nomes.length; i++) {
    		String caminho = nomes[i].replace(RESOURCE_PATH_REFERENCE, this.getURIBase().toString());
    		logger.debug(\"Caminho: \" + caminho);
    		try {
    			Resource[] resources = ((ResourcePatternResolver)this.getResourceLoader()).getResources(caminho);
    			lista.addAll(Arrays.asList(resources));
    		} catch (Exception e) {
    			logger.warn(\"Falhou em carregar os resources no caminho: \" + caminho, e);
    		}
    	}
    }
    
    @Override
    @SuppressWarnings(\"unchecked\")
    public Class getObjectType() {
    	return new Resource[0].getClass();
    }
    
    @Override
    public boolean isSingleton() {
    	return true;
    }
    
    private URI getURIBase() {
    	return this.getApplicationConfigService().getApplicationConfigPath().toURI();
    }
    
    public ApplicationConfigService getApplicationConfigService() {
    	if (this.applicationConfigService == null) {
    		throw new ConfigurationException(\"Serviço de configuração de aplicação não informado.\");
    	}
    	return this.applicationConfigService;
    }
    public void setApplicationConfigService(
    		ApplicationConfigService applicationConfigService) {
    	this.applicationConfigService = applicationConfigService;
    }
    
    public String[] getConfigLocations() {
    	if (this.configLocations == null) {
    		throw new ConfigurationException(\"Localização dos arquivos de configuração não foi informada.\");
    	}
    	return this.configLocations;
    }
    public void setConfigLocation(String[] configLocations) {
    	this.configLocations = configLocations;
    }
    
    public ResourceLoader getResourceLoader() {
    	if (this.resourceLoader == null) {
    		throw new ConfigurationException(\"ResourceLoader não informado.\");
    	}
    	return this.resourceLoader;
    }
    public void setResourceLoader(ResourceLoader resourceLoader) {
    	this.resourceLoader = resourceLoader;
    }

}
}}


---

**Affects:** 2.5.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12115","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12115/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12115/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12115/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12115","id":398106803,"node_id":"MDU6SXNzdWUzOTgxMDY4MDM=","number":12115,"title":"PropertiesLoaderSupport throws error \"'whatever' does not carry a filename\" if you feed a ByteArrayResource to it. [SPR-7457]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2010-08-12T07:13:32Z","updated_at":"2010-08-12T15:15:34Z","closed_at":"2010-08-12T15:15:34Z","author_association":"COLLABORATOR","body":"**[Haroldo de Oliveira Pinheiro](https://jira.spring.io/secure/ViewProfile.jspa?name=haroldo-ok-ats)** opened **[SPR-7457](https://jira.spring.io/browse/SPR-7457?redirect=false)** and commented\n\nI'm having a problem where I'm trying to feed a ByteArrayResource to a PropertyPlaceholderConfigurer, and getting an error like \"PropertiesLoaderSupport throws error \"'whatever' does not carry a filename\" if you feed a ByteArrayResource to it.\"\nI saw a previous similar issue at https://jira.springframework.org/browse/SPR-5068, where it was argued that doing so is unusual and unnecessary. But in my case, I'm using the ByteArrayResource to hold some values that are actually computed at runtime, and I wouldn't really like to have to create a temporary file just for that single reason.\n\nThe XML mapping is like this:\n\n{{\n\\<bean class=\"org.springframework.beans.factory.config.PropertyPlaceholderConfigurer\">\n\\<property name=\"locations\">\n\\<bean class=\"com.ats.framework.service.config.impl.ApplicationConfigResourceArrayFactory\">\n\\<property name=\"applicationConfigService\" ref=\"applicationConfigService\" />\n\\<property name=\"configLocation\" value=\"@{aplicacao.config}/application.properties\" />\n\\</bean>\n\\</property>\n\\</bean>\n}}\n\nApplicationConfigResourceArrayFactory:\n\n{{\npublic class ApplicationConfigResourceArrayFactory implements FactoryBean, ResourceLoaderAware {\nprivate static final Logger logger = Logger.getLogger(ApplicationConfigResourceArrayFactory.class);\n\n    private static final String RESOURCE_PATH_PROPERTY = \"aplicacao.config\";\t\n    private static final String RESOURCE_PATH_REFERENCE = \"@{\" + RESOURCE_PATH_PROPERTY + \"}\";\t\n    private static final String COMENTARIO_PROPRIEDADES_SISTEMA = \"Propriedades do sistema montadas em tempo de execução.\";\n    \n    private ApplicationConfigService applicationConfigService;\n    private ResourceLoader resourceLoader;\n    private String[] configLocations;\n    \n    @Override\n    public Object getObject() throws Exception {\n    \tString[] nomes = this.getConfigLocations();\n    \tList<Resource> lista = new ArrayList<Resource>(nomes.length);\n    \t\n    \tthis.loadConfigFiles(lista, nomes);\t\t\n    \tthis.loadComputedProperties(lista);\n    \t\n    \treturn lista.toArray(new Resource[lista.size()]);\n    }\n    \n    private void loadComputedProperties(List<Resource> lista)\n    \t\tthrows IOException {\n    \tProperties properties = new Properties();\n    \tproperties.put(RESOURCE_PATH_PROPERTY, this.getURIBase().toString());\n    \tByteArrayOutputStream bout = new ByteArrayOutputStream();\n    \tproperties.store(bout, COMENTARIO_PROPRIEDADES_SISTEMA);\n    \tlista.add(new ByteArrayResource(bout.toByteArray(), COMENTARIO_PROPRIEDADES_SISTEMA));\n    }\n    \n    private void loadConfigFiles(List<Resource> lista, String[] nomes) {\n    \tfor (int i = 0; i != nomes.length; i++) {\n    \t\tString caminho = nomes[i].replace(RESOURCE_PATH_REFERENCE, this.getURIBase().toString());\n    \t\tlogger.debug(\"Caminho: \" + caminho);\n    \t\ttry {\n    \t\t\tResource[] resources = ((ResourcePatternResolver)this.getResourceLoader()).getResources(caminho);\n    \t\t\tlista.addAll(Arrays.asList(resources));\n    \t\t} catch (Exception e) {\n    \t\t\tlogger.warn(\"Falhou em carregar os resources no caminho: \" + caminho, e);\n    \t\t}\n    \t}\n    }\n    \n    @Override\n    @SuppressWarnings(\"unchecked\")\n    public Class getObjectType() {\n    \treturn new Resource[0].getClass();\n    }\n    \n    @Override\n    public boolean isSingleton() {\n    \treturn true;\n    }\n    \n    private URI getURIBase() {\n    \treturn this.getApplicationConfigService().getApplicationConfigPath().toURI();\n    }\n    \n    public ApplicationConfigService getApplicationConfigService() {\n    \tif (this.applicationConfigService == null) {\n    \t\tthrow new ConfigurationException(\"Serviço de configuração de aplicação não informado.\");\n    \t}\n    \treturn this.applicationConfigService;\n    }\n    public void setApplicationConfigService(\n    \t\tApplicationConfigService applicationConfigService) {\n    \tthis.applicationConfigService = applicationConfigService;\n    }\n    \n    public String[] getConfigLocations() {\n    \tif (this.configLocations == null) {\n    \t\tthrow new ConfigurationException(\"Localização dos arquivos de configuração não foi informada.\");\n    \t}\n    \treturn this.configLocations;\n    }\n    public void setConfigLocation(String[] configLocations) {\n    \tthis.configLocations = configLocations;\n    }\n    \n    public ResourceLoader getResourceLoader() {\n    \tif (this.resourceLoader == null) {\n    \t\tthrow new ConfigurationException(\"ResourceLoader não informado.\");\n    \t}\n    \treturn this.resourceLoader;\n    }\n    public void setResourceLoader(ResourceLoader resourceLoader) {\n    \tthis.resourceLoader = resourceLoader;\n    }\n\n}\n}}\n\n\n---\n\n**Affects:** 2.5.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453352977","453352978","453352979"], "labels":["in: core","status: declined","type: enhancement"]}