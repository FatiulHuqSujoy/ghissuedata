{"id":"12924", "title":"using jsp:include to include spring action caused infinite loop [SPR-8276]", "body":"**[Hui Chen](https://jira.spring.io/secure/ViewProfile.jspa?name=chenhuieb)** opened **[SPR-8276](https://jira.spring.io/browse/SPR-8276?redirect=false)** and commented

Config the web app to use tiles as ViewResolver.

Create a parent jsp page and child jsp page, in parent jsp apge, use jsp:include to call an action which renders the child jsp page.

<jsp:include page=\"/getChild.action\"></jsp:include>

Defined the view in tiles.

\\<tiles-definitions>
\\<definition name=\"parent_page\" template=\"/WEB-INF/jsp/parent_page.jsp\">
\\</definition>
\\<definition name=\"child_page\" template=\"/WEB-INF/jsp/child_page.jsp\">
\\</definition>
\\</tiles-definitions>

Create two controllers GetParentController and GetChildController, which simply returns \"parent_page\" and \"child_page\" as view respectively.

When trying to call /getParent.action, it causes an infinite loop and JVM crashes.

It used to work under Spring 2.5.6 and Tiles 2.0.4.

And it also works if I use InternalResourceViewResolver.



---

**Affects:** 3.0.5

**Attachments:**
- [controller.zip](https://jira.spring.io/secure/attachment/18080/controller.zip) (_2.12 kB_)
- [WebContent.zip](https://jira.spring.io/secure/attachment/18079/WebContent.zip) (_4.43 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12924","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12924/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12924/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12924/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12924","id":398111965,"node_id":"MDU6SXNzdWUzOTgxMTE5NjU=","number":12924,"title":"using jsp:include to include spring action caused infinite loop [SPR-8276]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2011-04-26T11:51:09Z","updated_at":"2019-01-12T16:25:41Z","closed_at":"2018-12-26T16:40:20Z","author_association":"COLLABORATOR","body":"**[Hui Chen](https://jira.spring.io/secure/ViewProfile.jspa?name=chenhuieb)** opened **[SPR-8276](https://jira.spring.io/browse/SPR-8276?redirect=false)** and commented\n\nConfig the web app to use tiles as ViewResolver.\n\nCreate a parent jsp page and child jsp page, in parent jsp apge, use jsp:include to call an action which renders the child jsp page.\n\n<jsp:include page=\"/getChild.action\"></jsp:include>\n\nDefined the view in tiles.\n\n\\<tiles-definitions>\n\\<definition name=\"parent_page\" template=\"/WEB-INF/jsp/parent_page.jsp\">\n\\</definition>\n\\<definition name=\"child_page\" template=\"/WEB-INF/jsp/child_page.jsp\">\n\\</definition>\n\\</tiles-definitions>\n\nCreate two controllers GetParentController and GetChildController, which simply returns \"parent_page\" and \"child_page\" as view respectively.\n\nWhen trying to call /getParent.action, it causes an infinite loop and JVM crashes.\n\nIt used to work under Spring 2.5.6 and Tiles 2.0.4.\n\nAnd it also works if I use InternalResourceViewResolver.\n\n\n\n---\n\n**Affects:** 3.0.5\n\n**Attachments:**\n- [controller.zip](https://jira.spring.io/secure/attachment/18080/controller.zip) (_2.12 kB_)\n- [WebContent.zip](https://jira.spring.io/secure/attachment/18079/WebContent.zip) (_4.43 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453359068"], "labels":["in: core","status: declined"]}