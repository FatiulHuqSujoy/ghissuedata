{"id":"5318", "title":"Integration of EJB CMT with Spring Framework [SPR-590]", "body":"**[Deepak Kalra](https://jira.spring.io/secure/ViewProfile.jspa?name=kalradeepak)** opened **[SPR-590](https://jira.spring.io/browse/SPR-590?redirect=false)** and commented

Hi

I am working on integration of EJB CMT with Spring Framework.

Case 1. Client is direct calling spring managed pojo with PROPAGATION_REQUIRED.
Case 2. Client is calling Remote Bean method demarcated as RequiresNew.Inside the method a spring managed pojo is called with PROPAGATION_REQUIRED.

I am getting the error in Case 2 as Its not able to find JTA UserTransaction

Case 1
I am able to run the case 1 without any error and data is getting inserted As its getting the JTA UserTransaction

///////////////////////////////////////////////////////
INFO: Creating shared instance of singleton bean 'platformTransactionManager'
Dec 29, 2004 7:21:18 AM org.springframework.transaction.jta.JtaTransactionManager lookupUserTransaction
INFO: Using JTA UserTransaction [com.ibm.ws.Transaction.JTA.UserTransactionImpl@2e5e28db] from JNDI location [jta/usertransaction]
///////////////////////////////////////////////////////

Pls see EventTestClient for the same.

Case 2
I am getting the error. Pls see EJBSpringClient
for the same. Somehow Its not able to find JTA UserTransaction but
in case 1 Its able to find. Only difference is client is calling the EJB where as in case 1 does not
-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-
Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.
-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-

This is my applicationContext.xml file

-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>
\\<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">
\\<beans>

    <bean id=\"myTestDataSource\" class=\"org.springframework.jndi.JndiObjectFactoryBean\">
    	<property name=\"jndiName\">
    		<value>jdbc/atomicDB</value>
    	</property>
    </bean>
    
    
    <bean id=\"mySessionFactory\" class=\"org.springframework.orm.hibernate.LocalSessionFactoryBean\">
    	<property name=\"dataSource\">
    		<ref local=\"myTestDataSource\" />
    	</property>
    	<property name=\"hibernateProperties\">
    		<props>
    			<prop key=\"hibernate.dialect\">net.sf.hibernate.dialect.OracleDialect</prop>
    		</props>
    	</property>
    	<property name=\"mappingResources\">
    		<list>
    			<value>com/atomic/springjndi/Event.hbm.xml</value>
    		</list>
    	</property>
    </bean>
    
    <bean id=\"myEventDAO\" class=\"com.atomic.springjndi.EventDAOImpl\">
    	<property name=\"sessionFactory\">
    		<ref bean=\"mySessionFactory\" />
    	</property>
    </bean>
    
    <bean id=\"myEventServiceTarget\" class=\"com.atomic.springjndi.EventServiceImpl\">
    	<property name=\"eventDao\">
    		<ref bean=\"myEventDAO\" />
    	</property>
    </bean>
    
    <!-- TransactionManager-->
    
    <bean id=\"myTransactionManager\" class=\"org.springframework.transaction.jta.WebSphereTransactionManagerFactoryBean\" />
    
    <bean id=\"platformTransactionManager\" class=\"org.springframework.transaction.jta.JtaTransactionManager\">
    	<property name=\"transactionManager\">
    		<ref local=\"myTransactionManager\" />
    	</property>
    	<property name=\"userTransactionName\">
    		<value>jta/usertransaction</value>
    	</property>
    </bean>
    <!-- TransactionManager-->
    
    <!-- ProxyFactoryBean-->
    <bean id=\"myEventService\" class=\"org.springframework.transaction.interceptor.TransactionProxyFactoryBean\">
    	<property name=\"transactionManager\">
    		<ref bean=\"platformTransactionManager\" />
    	</property>
    	<property name=\"target\">
    		<ref bean=\"myEventServiceTarget\" />
    	</property>
    	<property name=\"transactionAttributes\">
    		<props>
    			<prop key=\"checkAllEvents*\">PROPAGATION_SUPPORTS</prop>
    			<prop key=\"insertEvents*\">PROPAGATION_SUPPORTS</prop>
    		</props>
    	</property>
    </bean>
    <!-- ProxyFactoryBean-->

\\</beans>
-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-

Complete Error for Case 2

//////////////////////////////////////////////////////////////////
InitialContext javax.naming.InitialContext@4ee0ec52
EJBSpringTransactionHome IOR:0000000000000043524d493a636f6d2e61746f6d69632e656a62737072696e672e454a42537072696e675472616e73616374696f6e486f6d653a303030303030303030303030303030300000000000010000000000000130000102000000000a6c6f63616c686f73740010c8000000a64a4d4249000000124773e3aa37643062633737336533616166633334000000240000008249454a5002008e629f84077365727665723103454a4200000068acac0002000100290000005f5f686f6d654f66486f6d6573235f5f686f6d654f66486f6d6573235f5f686f6d654f66486f6d65730870726f6a656374454a42537072696e6723656a624d6f64756c652e6a617223454a42537072696e675472616e73616374696f6e0000000000070000000100000014000000000501000100000000000101000000000049424d0a00000008000000011420000100000026000000020002000049424d040000000500050101020000000000001f0000000400000003000000200000000400000001000000250000000400000003
EJBSpringTransaction 1 IOR:000000000000003f524d493a636f6d2e61746f6d69632e656a62737072696e672e454a42537072696e675472616e73616374696f6e3a303030303030303030303030303030300000000000010000000000000104000102000000000a6c6f63616c686f73740010c80000007c4a4d4249000000124773e3aa37643062633737336533616166633334000000240000005849454a500200eb53a395077365727665723103454a420000003eacac00020001013300000070726f6a656374454a42537072696e6723656a624d6f64756c652e6a617223454a42537072696e675472616e73616374696f6e000000070000000100000014000000000501000100000000000101000000000049424d0a00000008000000011420000100000026000000020002000049424d040000000500050101020000000000001f0000000400000003000000200000000400000001000000250000000400000003
EJBSpringTransaction RemoteException java.rmi.ServerException: RemoteException occurred in server thread; nested exception is:
java.rmi.RemoteException:

Trace from server: 1198777258 at host localhost >>
java.rmi.RemoteException: ; nested exception is:
org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.
at com.ibm.ejs.container.RemoteExceptionMappingStrategy.setUncheckedException(RemoteExceptionMappingStrategy.java:196)
at com.ibm.ejs.container.EJSDeployedSupport.setUncheckedException(EJSDeployedSupport.java:296)
at com.atomic.ejbspring.EJSRemoteStatelessEJBSpringTransaction_f9c64424.springServiceLayer(EJSRemoteStatelessEJBSpringTransaction_f9c64424.java:41)
at com.atomic.ejbspring._EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.springServiceLayer(_EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.java:154)
at com.atomic.ejbspring._EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie._invoke(_EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.java:98)
at com.ibm.CORBA.iiop.ServerDelegate.dispatchInvokeHandler(ServerDelegate.java:608)
at com.ibm.CORBA.iiop.ServerDelegate.dispatch(ServerDelegate.java:461)
at com.ibm.rmi.iiop.ORB.process(ORB.java:432)
at com.ibm.CORBA.iiop.ORB.process(ORB.java:1728)
at com.ibm.rmi.iiop.Connection.doWork(Connection.java:2227)
at com.ibm.rmi.iiop.WorkUnitImpl.doWork(WorkUnitImpl.java:65)
at com.ibm.ejs.oa.pool.PooledThread.run(ThreadPool.java:95)
at com.ibm.ws.util.ThreadPool$Worker.run(ThreadPool.java:912)
Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:318)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:223)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:236)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:159)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:261)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)
at org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:80)
at org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:65)
at org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:56)
at com.atomic.ejbspring.EJBSpringTransactionBean.springServiceLayer(EJBSpringTransactionBean.java:53)
at com.atomic.ejbspring.EJSRemoteStatelessEJBSpringTransaction_f9c64424.springServiceLayer(EJSRemoteStatelessEJBSpringTransaction_f9c64424.java:35)
... 10 more
Caused by: org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.
at org.springframework.transaction.jta.JtaTransactionManager.lookupUserTransaction(JtaTransactionManager.java:359)
at org.springframework.transaction.jta.JtaTransactionManager.afterPropertiesSet(JtaTransactionManager.java:312)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1037)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:305)
... 20 more
Caused by: com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object. [Root exception is javax.naming.ConfigurationException]
at com.ibm.ws.naming.util.Helpers.processSerializedObjectForLookupExt(Helpers.java:931)
at com.ibm.ws.naming.util.Helpers.processSerializedObjectForLookup(Helpers.java:680)
at com.ibm.ws.naming.jndicos.CNContextImpl.processResolveResults(CNContextImpl.java:1712)
at com.ibm.ws.naming.jndicos.CNContextImpl.doLookup(CNContextImpl.java:1567)
at com.ibm.ws.naming.jndicos.CNContextImpl.doLookup(CNContextImpl.java:1480)
at com.ibm.ws.naming.jndicos.CNContextImpl.lookupExt(CNContextImpl.java:1187)
at com.ibm.ws.naming.jndicos.CNContextImpl.lookup(CNContextImpl.java:1067)
at com.ibm.ws.naming.util.WsnInitCtx.lookup(WsnInitCtx.java:144)
at javax.naming.InitialContext.lookup(InitialContext.java:361)
at org.springframework.jndi.JndiTemplate$1.doInContext(JndiTemplate.java:123)
at org.springframework.jndi.JndiTemplate.execute(JndiTemplate.java:85)
at org.springframework.jndi.JndiTemplate.lookup(JndiTemplate.java:121)
at org.springframework.transaction.jta.JtaTransactionManager.lookupUserTransaction(JtaTransactionManager.java:347)
... 23 more
<<  END server: 1198777258 at host localhost

; nested exception is:
org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.

//////////////////////////////////////////////////////////////////

Pls help me in this regard

Kind Regards
Deepak

---

**Affects:** 1.1.3

**Attachments:**
- [EJBSpringClient.java](https://jira.spring.io/secure/attachment/10399/EJBSpringClient.java) (_2.47 kB_)
- [EventTestClient.java](https://jira.spring.io/secure/attachment/10400/EventTestClient.java) (_2.18 kB_)

**Issue Links:**
- #5314 EJB initiating the Tansaction CMT (_**\"is duplicated by\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5318","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5318/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5318/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5318/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5318","id":398053845,"node_id":"MDU6SXNzdWUzOTgwNTM4NDU=","number":5318,"title":"Integration of EJB CMT with Spring Framework [SPR-590]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":5,"created_at":"2004-12-28T17:26:10Z","updated_at":"2019-01-11T12:41:30Z","closed_at":"2005-01-31T08:33:57Z","author_association":"COLLABORATOR","body":"**[Deepak Kalra](https://jira.spring.io/secure/ViewProfile.jspa?name=kalradeepak)** opened **[SPR-590](https://jira.spring.io/browse/SPR-590?redirect=false)** and commented\n\nHi\n\nI am working on integration of EJB CMT with Spring Framework.\n\nCase 1. Client is direct calling spring managed pojo with PROPAGATION_REQUIRED.\nCase 2. Client is calling Remote Bean method demarcated as RequiresNew.Inside the method a spring managed pojo is called with PROPAGATION_REQUIRED.\n\nI am getting the error in Case 2 as Its not able to find JTA UserTransaction\n\nCase 1\nI am able to run the case 1 without any error and data is getting inserted As its getting the JTA UserTransaction\n\n///////////////////////////////////////////////////////\nINFO: Creating shared instance of singleton bean 'platformTransactionManager'\nDec 29, 2004 7:21:18 AM org.springframework.transaction.jta.JtaTransactionManager lookupUserTransaction\nINFO: Using JTA UserTransaction [com.ibm.ws.Transaction.JTA.UserTransactionImpl@2e5e28db] from JNDI location [jta/usertransaction]\n///////////////////////////////////////////////////////\n\nPls see EventTestClient for the same.\n\nCase 2\nI am getting the error. Pls see EJBSpringClient\nfor the same. Somehow Its not able to find JTA UserTransaction but\nin case 1 Its able to find. Only difference is client is calling the EJB where as in case 1 does not\n-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-\nError creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.\n-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-\n\nThis is my applicationContext.xml file\n\n-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\\<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">\n\\<beans>\n\n    <bean id=\"myTestDataSource\" class=\"org.springframework.jndi.JndiObjectFactoryBean\">\n    \t<property name=\"jndiName\">\n    \t\t<value>jdbc/atomicDB</value>\n    \t</property>\n    </bean>\n    \n    \n    <bean id=\"mySessionFactory\" class=\"org.springframework.orm.hibernate.LocalSessionFactoryBean\">\n    \t<property name=\"dataSource\">\n    \t\t<ref local=\"myTestDataSource\" />\n    \t</property>\n    \t<property name=\"hibernateProperties\">\n    \t\t<props>\n    \t\t\t<prop key=\"hibernate.dialect\">net.sf.hibernate.dialect.OracleDialect</prop>\n    \t\t</props>\n    \t</property>\n    \t<property name=\"mappingResources\">\n    \t\t<list>\n    \t\t\t<value>com/atomic/springjndi/Event.hbm.xml</value>\n    \t\t</list>\n    \t</property>\n    </bean>\n    \n    <bean id=\"myEventDAO\" class=\"com.atomic.springjndi.EventDAOImpl\">\n    \t<property name=\"sessionFactory\">\n    \t\t<ref bean=\"mySessionFactory\" />\n    \t</property>\n    </bean>\n    \n    <bean id=\"myEventServiceTarget\" class=\"com.atomic.springjndi.EventServiceImpl\">\n    \t<property name=\"eventDao\">\n    \t\t<ref bean=\"myEventDAO\" />\n    \t</property>\n    </bean>\n    \n    <!-- TransactionManager-->\n    \n    <bean id=\"myTransactionManager\" class=\"org.springframework.transaction.jta.WebSphereTransactionManagerFactoryBean\" />\n    \n    <bean id=\"platformTransactionManager\" class=\"org.springframework.transaction.jta.JtaTransactionManager\">\n    \t<property name=\"transactionManager\">\n    \t\t<ref local=\"myTransactionManager\" />\n    \t</property>\n    \t<property name=\"userTransactionName\">\n    \t\t<value>jta/usertransaction</value>\n    \t</property>\n    </bean>\n    <!-- TransactionManager-->\n    \n    <!-- ProxyFactoryBean-->\n    <bean id=\"myEventService\" class=\"org.springframework.transaction.interceptor.TransactionProxyFactoryBean\">\n    \t<property name=\"transactionManager\">\n    \t\t<ref bean=\"platformTransactionManager\" />\n    \t</property>\n    \t<property name=\"target\">\n    \t\t<ref bean=\"myEventServiceTarget\" />\n    \t</property>\n    \t<property name=\"transactionAttributes\">\n    \t\t<props>\n    \t\t\t<prop key=\"checkAllEvents*\">PROPAGATION_SUPPORTS</prop>\n    \t\t\t<prop key=\"insertEvents*\">PROPAGATION_SUPPORTS</prop>\n    \t\t</props>\n    \t</property>\n    </bean>\n    <!-- ProxyFactoryBean-->\n\n\\</beans>\n-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-\n\nComplete Error for Case 2\n\n//////////////////////////////////////////////////////////////////\nInitialContext javax.naming.InitialContext@4ee0ec52\nEJBSpringTransactionHome IOR:0000000000000043524d493a636f6d2e61746f6d69632e656a62737072696e672e454a42537072696e675472616e73616374696f6e486f6d653a303030303030303030303030303030300000000000010000000000000130000102000000000a6c6f63616c686f73740010c8000000a64a4d4249000000124773e3aa37643062633737336533616166633334000000240000008249454a5002008e629f84077365727665723103454a4200000068acac0002000100290000005f5f686f6d654f66486f6d6573235f5f686f6d654f66486f6d6573235f5f686f6d654f66486f6d65730870726f6a656374454a42537072696e6723656a624d6f64756c652e6a617223454a42537072696e675472616e73616374696f6e0000000000070000000100000014000000000501000100000000000101000000000049424d0a00000008000000011420000100000026000000020002000049424d040000000500050101020000000000001f0000000400000003000000200000000400000001000000250000000400000003\nEJBSpringTransaction 1 IOR:000000000000003f524d493a636f6d2e61746f6d69632e656a62737072696e672e454a42537072696e675472616e73616374696f6e3a303030303030303030303030303030300000000000010000000000000104000102000000000a6c6f63616c686f73740010c80000007c4a4d4249000000124773e3aa37643062633737336533616166633334000000240000005849454a500200eb53a395077365727665723103454a420000003eacac00020001013300000070726f6a656374454a42537072696e6723656a624d6f64756c652e6a617223454a42537072696e675472616e73616374696f6e000000070000000100000014000000000501000100000000000101000000000049424d0a00000008000000011420000100000026000000020002000049424d040000000500050101020000000000001f0000000400000003000000200000000400000001000000250000000400000003\nEJBSpringTransaction RemoteException java.rmi.ServerException: RemoteException occurred in server thread; nested exception is:\njava.rmi.RemoteException:\n\nTrace from server: 1198777258 at host localhost >>\njava.rmi.RemoteException: ; nested exception is:\norg.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.\nat com.ibm.ejs.container.RemoteExceptionMappingStrategy.setUncheckedException(RemoteExceptionMappingStrategy.java:196)\nat com.ibm.ejs.container.EJSDeployedSupport.setUncheckedException(EJSDeployedSupport.java:296)\nat com.atomic.ejbspring.EJSRemoteStatelessEJBSpringTransaction_f9c64424.springServiceLayer(EJSRemoteStatelessEJBSpringTransaction_f9c64424.java:41)\nat com.atomic.ejbspring._EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.springServiceLayer(_EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.java:154)\nat com.atomic.ejbspring._EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie._invoke(_EJSRemoteStatelessEJBSpringTransaction_f9c64424_Tie.java:98)\nat com.ibm.CORBA.iiop.ServerDelegate.dispatchInvokeHandler(ServerDelegate.java:608)\nat com.ibm.CORBA.iiop.ServerDelegate.dispatch(ServerDelegate.java:461)\nat com.ibm.rmi.iiop.ORB.process(ORB.java:432)\nat com.ibm.CORBA.iiop.ORB.process(ORB.java:1728)\nat com.ibm.rmi.iiop.Connection.doWork(Connection.java:2227)\nat com.ibm.rmi.iiop.WorkUnitImpl.doWork(WorkUnitImpl.java:65)\nat com.ibm.ejs.oa.pool.PooledThread.run(ThreadPool.java:95)\nat com.ibm.ws.util.ThreadPool$Worker.run(ThreadPool.java:912)\nCaused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:318)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:223)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:236)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:159)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:261)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)\nat org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:80)\nat org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:65)\nat org.springframework.context.support.ClassPathXmlApplicationContext.\\<init>(ClassPathXmlApplicationContext.java:56)\nat com.atomic.ejbspring.EJBSpringTransactionBean.springServiceLayer(EJBSpringTransactionBean.java:53)\nat com.atomic.ejbspring.EJSRemoteStatelessEJBSpringTransaction_f9c64424.springServiceLayer(EJSRemoteStatelessEJBSpringTransaction_f9c64424.java:35)\n... 10 more\nCaused by: org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.\nat org.springframework.transaction.jta.JtaTransactionManager.lookupUserTransaction(JtaTransactionManager.java:359)\nat org.springframework.transaction.jta.JtaTransactionManager.afterPropertiesSet(JtaTransactionManager.java:312)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1037)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:305)\n... 20 more\nCaused by: com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object. [Root exception is javax.naming.ConfigurationException]\nat com.ibm.ws.naming.util.Helpers.processSerializedObjectForLookupExt(Helpers.java:931)\nat com.ibm.ws.naming.util.Helpers.processSerializedObjectForLookup(Helpers.java:680)\nat com.ibm.ws.naming.jndicos.CNContextImpl.processResolveResults(CNContextImpl.java:1712)\nat com.ibm.ws.naming.jndicos.CNContextImpl.doLookup(CNContextImpl.java:1567)\nat com.ibm.ws.naming.jndicos.CNContextImpl.doLookup(CNContextImpl.java:1480)\nat com.ibm.ws.naming.jndicos.CNContextImpl.lookupExt(CNContextImpl.java:1187)\nat com.ibm.ws.naming.jndicos.CNContextImpl.lookup(CNContextImpl.java:1067)\nat com.ibm.ws.naming.util.WsnInitCtx.lookup(WsnInitCtx.java:144)\nat javax.naming.InitialContext.lookup(InitialContext.java:361)\nat org.springframework.jndi.JndiTemplate$1.doInContext(JndiTemplate.java:123)\nat org.springframework.jndi.JndiTemplate.execute(JndiTemplate.java:85)\nat org.springframework.jndi.JndiTemplate.lookup(JndiTemplate.java:121)\nat org.springframework.transaction.jta.JtaTransactionManager.lookupUserTransaction(JtaTransactionManager.java:347)\n... 23 more\n<<  END server: 1198777258 at host localhost\n\n; nested exception is:\norg.springframework.beans.factory.BeanCreationException: Error creating bean with name 'platformTransactionManager' defined in class path resource [com/atomic/springjndi/applicationContext.xml]: Initialization of bean failed; nested exception is org.springframework.transaction.TransactionSystemException: JTA UserTransaction is not available at JNDI location [jta/usertransaction]; nested exception is com.ibm.websphere.naming.CannotInstantiateObjectException: Exception occurred while the JNDI NamingManager was processing a javax.naming.Reference object.\n\n//////////////////////////////////////////////////////////////////\n\nPls help me in this regard\n\nKind Regards\nDeepak\n\n---\n\n**Affects:** 1.1.3\n\n**Attachments:**\n- [EJBSpringClient.java](https://jira.spring.io/secure/attachment/10399/EJBSpringClient.java) (_2.47 kB_)\n- [EventTestClient.java](https://jira.spring.io/secure/attachment/10400/EventTestClient.java) (_2.18 kB_)\n\n**Issue Links:**\n- #5314 EJB initiating the Tansaction CMT (_**\"is duplicated by\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453290535","453290537","453290538","453290543","453290544"], "labels":["in: data"]}