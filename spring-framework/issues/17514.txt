{"id":"17514", "title":"ConfigurationClassEnhancer.enhanceFactoryBean is not transparent for method calls other than getObject() [SPR-12915]", "body":"**[Adrian Moos](https://jira.spring.io/secure/ViewProfile.jspa?name=bedag-moo)** opened **[SPR-12915](https://jira.spring.io/browse/SPR-12915?redirect=false)** and commented

When a `@Bean` method returns an instance of a FactoryBean, Spring proxies the factory bean, redirecting calls to getObject() to applicationContext.getBean().

The relevant code reads:

```
	/**
	 * Create a subclass proxy that intercepts calls to getObject(), delegating to the current BeanFactory
	 * instead of creating a new instance. These proxies are created only when calling a FactoryBean from
	 * within a Bean method, allowing for proper scoping semantics even when working against the FactoryBean
	 * instance directly. If a FactoryBean instance is fetched through the container via &-dereferencing,
	 * it will not be proxied. This too is aligned with the way XML configuration works.
	 */
	private Object enhanceFactoryBean(Class<?> fbClass, final ConfigurableBeanFactory beanFactory,
			final String beanName) throws InstantiationException, IllegalAccessException {

		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(fbClass);
		enhancer.setUseFactory(false);
		enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
		enhancer.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
				if (method.getName().equals(\"getObject\") && args.length == 0) {
					return beanFactory.getBean(beanName);
				}
				return proxy.invokeSuper(obj, args); // bug here?
			}
		});
		return enhancer.create();
	}
```

In the marked line, obj refers to the proxy object.

Therefore, calls to methods other than getObject() are forwarded to the super implementation **on the proxy object**, which has a different state than the FactoryBean it proxies.

This breaks the following usecase:

```
        @Bean
        protected DBTool dbTool() {
            return new DBTool(hibernateSessionFactory());
        }

        @Bean
        protected AnnotationSessionFactoryBean hibernateSessionFactory() {
             // hibernate setup goes here
        }
```

where DBTool has a method:

```
public void createSchema() {
    annotationSessionFactory.createDatabaseSchema();
}
```

which now throws

```
java.lang.IllegalStateException: SessionFactory not initialized yet
	at org.springframework.orm.hibernate3.AbstractSessionFactoryBean.getSessionFactory(AbstractSessionFactoryBean.java:215)
	at org.springframework.orm.hibernate3.LocalSessionFactoryBean.createDatabaseSchema(LocalSessionFactoryBean.java:989)
	at org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc.CGLIB$createDatabaseSchema$12(<generated>)
	at org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc$$FastClassBySpringCGLIB$$52787a0.invoke(<generated>)
	at org.springframework.cglib.proxy.MethodProxy.invokeSuper(MethodProxy.java:228)
	at org.springframework.context.annotation.ConfigurationClassEnhancer$BeanMethodInterceptor$1.intercept(ConfigurationClassEnhancer.java:383)
	at org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc.createDatabaseSchema(<generated>)
	at ch.bedag.ste.app.cf.hibernate.framework.DBTool.createSchema(DBTool.java:23)
```

because the proxy object is a factory that has not been configured.

---

**Affects:** 4.1.2

**Issue Links:**
- #11268 Calls to FactoryBean `@Bean` methods cause ClassCastException
- #17686 CGLIB code generation failure for cross-`@Bean` FactoryBean call

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/063a720ac0f0848bc86946b891df27b828d58946, https://github.com/spring-projects/spring-framework/commit/1da98b054210b1bcb4e65cde66e378e034b1baa3, https://github.com/spring-projects/spring-framework/commit/6783ba2903e35ab3d13b9202272b3affea4826f2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17514","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17514/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17514/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17514/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17514","id":398178623,"node_id":"MDU6SXNzdWUzOTgxNzg2MjM=","number":17514,"title":"ConfigurationClassEnhancer.enhanceFactoryBean is not transparent for method calls other than getObject() [SPR-12915]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/134","html_url":"https://github.com/spring-projects/spring-framework/milestone/134","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/134/labels","id":3960907,"node_id":"MDk6TWlsZXN0b25lMzk2MDkwNw==","number":134,"title":"4.2 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":235,"state":"closed","created_at":"2019-01-10T22:04:32Z","updated_at":"2019-01-11T08:26:53Z","due_on":"2015-05-25T07:00:00Z","closed_at":"2019-01-10T22:04:32Z"},"comments":3,"created_at":"2015-04-15T09:44:05Z","updated_at":"2019-01-14T04:43:19Z","closed_at":"2015-06-04T08:45:18Z","author_association":"COLLABORATOR","body":"**[Adrian Moos](https://jira.spring.io/secure/ViewProfile.jspa?name=bedag-moo)** opened **[SPR-12915](https://jira.spring.io/browse/SPR-12915?redirect=false)** and commented\n\nWhen a `@Bean` method returns an instance of a FactoryBean, Spring proxies the factory bean, redirecting calls to getObject() to applicationContext.getBean().\n\nThe relevant code reads:\n\n```\n\t/**\n\t * Create a subclass proxy that intercepts calls to getObject(), delegating to the current BeanFactory\n\t * instead of creating a new instance. These proxies are created only when calling a FactoryBean from\n\t * within a Bean method, allowing for proper scoping semantics even when working against the FactoryBean\n\t * instance directly. If a FactoryBean instance is fetched through the container via &-dereferencing,\n\t * it will not be proxied. This too is aligned with the way XML configuration works.\n\t */\n\tprivate Object enhanceFactoryBean(Class<?> fbClass, final ConfigurableBeanFactory beanFactory,\n\t\t\tfinal String beanName) throws InstantiationException, IllegalAccessException {\n\n\t\tEnhancer enhancer = new Enhancer();\n\t\tenhancer.setSuperclass(fbClass);\n\t\tenhancer.setUseFactory(false);\n\t\tenhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);\n\t\tenhancer.setCallback(new MethodInterceptor() {\n\t\t\t@Override\n\t\t\tpublic Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {\n\t\t\t\tif (method.getName().equals(\"getObject\") && args.length == 0) {\n\t\t\t\t\treturn beanFactory.getBean(beanName);\n\t\t\t\t}\n\t\t\t\treturn proxy.invokeSuper(obj, args); // bug here?\n\t\t\t}\n\t\t});\n\t\treturn enhancer.create();\n\t}\n```\n\nIn the marked line, obj refers to the proxy object.\n\nTherefore, calls to methods other than getObject() are forwarded to the super implementation **on the proxy object**, which has a different state than the FactoryBean it proxies.\n\nThis breaks the following usecase:\n\n```\n        @Bean\n        protected DBTool dbTool() {\n            return new DBTool(hibernateSessionFactory());\n        }\n\n        @Bean\n        protected AnnotationSessionFactoryBean hibernateSessionFactory() {\n             // hibernate setup goes here\n        }\n```\n\nwhere DBTool has a method:\n\n```\npublic void createSchema() {\n    annotationSessionFactory.createDatabaseSchema();\n}\n```\n\nwhich now throws\n\n```\njava.lang.IllegalStateException: SessionFactory not initialized yet\n\tat org.springframework.orm.hibernate3.AbstractSessionFactoryBean.getSessionFactory(AbstractSessionFactoryBean.java:215)\n\tat org.springframework.orm.hibernate3.LocalSessionFactoryBean.createDatabaseSchema(LocalSessionFactoryBean.java:989)\n\tat org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc.CGLIB$createDatabaseSchema$12(<generated>)\n\tat org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc$$FastClassBySpringCGLIB$$52787a0.invoke(<generated>)\n\tat org.springframework.cglib.proxy.MethodProxy.invokeSuper(MethodProxy.java:228)\n\tat org.springframework.context.annotation.ConfigurationClassEnhancer$BeanMethodInterceptor$1.intercept(ConfigurationClassEnhancer.java:383)\n\tat org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean$$EnhancerBySpringCGLIB$$2fba14bc.createDatabaseSchema(<generated>)\n\tat ch.bedag.ste.app.cf.hibernate.framework.DBTool.createSchema(DBTool.java:23)\n```\n\nbecause the proxy object is a factory that has not been configured.\n\n---\n\n**Affects:** 4.1.2\n\n**Issue Links:**\n- #11268 Calls to FactoryBean `@Bean` methods cause ClassCastException\n- #17686 CGLIB code generation failure for cross-`@Bean` FactoryBean call\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/063a720ac0f0848bc86946b891df27b828d58946, https://github.com/spring-projects/spring-framework/commit/1da98b054210b1bcb4e65cde66e378e034b1baa3, https://github.com/spring-projects/spring-framework/commit/6783ba2903e35ab3d13b9202272b3affea4826f2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453424647","453424650","453424655"], "labels":["in: core","type: bug"]}