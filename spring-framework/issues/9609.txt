{"id":"9609", "title":"problem with 'SimpleJdbcTemplate.update'  [SPR-4934]", "body":"**[Ahsan Mahboob](https://jira.spring.io/secure/ViewProfile.jspa?name=ahsanmahboob)** opened **[SPR-4934](https://jira.spring.io/browse/SPR-4934?redirect=false)** and commented

Hello,

I am facing problem with 'SimpleJdbcTemplate.update' used in a realtime multi-threaded application. Sometimes not working properly with updating records (3% updates found

improper), specialyl 'datetime' field. Following are the details of Spring/MySQL versions used:

=> Spring-Version: 2.0-m5

=> MySQL Version: 5.0.18

=> Code snippet is as follows:

SimpleJdbcTemplate jdbcTemplate = new SimpleJdbcTemplate( dataSource );

return jdbcTemplate.update( \"update call_agent_detail set end_time=?, flag=? \"
+ \"where id=? and init_time=?\",
  new Object[] { 	detail.getEndTime(),
  detail.getFlag(),
  detail.getId(),
  detail.getInitTime()
  } );

Help in this regard will be highly appreciated.

Thanks,

Ahsan Mahboob
Sr. SE
i2c Inc.


---

**Affects:** 2.0 M5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9609","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9609/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9609/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9609/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9609","id":398089273,"node_id":"MDU6SXNzdWUzOTgwODkyNzM=","number":9609,"title":"problem with 'SimpleJdbcTemplate.update'  [SPR-4934]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2008-06-18T20:47:56Z","updated_at":"2019-01-11T18:28:38Z","closed_at":"2008-09-17T21:31:42Z","author_association":"COLLABORATOR","body":"**[Ahsan Mahboob](https://jira.spring.io/secure/ViewProfile.jspa?name=ahsanmahboob)** opened **[SPR-4934](https://jira.spring.io/browse/SPR-4934?redirect=false)** and commented\n\nHello,\n\nI am facing problem with 'SimpleJdbcTemplate.update' used in a realtime multi-threaded application. Sometimes not working properly with updating records (3% updates found\n\nimproper), specialyl 'datetime' field. Following are the details of Spring/MySQL versions used:\n\n=> Spring-Version: 2.0-m5\n\n=> MySQL Version: 5.0.18\n\n=> Code snippet is as follows:\n\nSimpleJdbcTemplate jdbcTemplate = new SimpleJdbcTemplate( dataSource );\n\nreturn jdbcTemplate.update( \"update call_agent_detail set end_time=?, flag=? \"\n+ \"where id=? and init_time=?\",\n  new Object[] { \tdetail.getEndTime(),\n  detail.getFlag(),\n  detail.getId(),\n  detail.getInitTime()\n  } );\n\nHelp in this regard will be highly appreciated.\n\nThanks,\n\nAhsan Mahboob\nSr. SE\ni2c Inc.\n\n\n---\n\n**Affects:** 2.0 M5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453331732"], "labels":[]}