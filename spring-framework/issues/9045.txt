{"id":"9045", "title":"Autowiring failed when one of the bean contains factory-method [SPR-4367]", "body":"**[G](https://jira.spring.io/secure/ViewProfile.jspa?name=geniusgroup)** opened **[SPR-4367](https://jira.spring.io/browse/SPR-4367?redirect=false)** and commented

Autowiring failed when one of the bean contains factory-method (Static setter). Even we don't apply autowire in that bean, it will also crash the whole framework.

Reference: http://forum.springframework.org/showthread.php?t=48399

Partial XML:

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:context=\"http://www.springframework.org/schema/context\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-2.5.xsd\">

<context:annotation-config />

\\<bean id=\"hibernateUtil\" class=\"com.foo.hibernate.HibernateUtil\"
factory-method=\"setSessionFactory\">
\\<constructor-arg ref=\"sessionFactory\" />
\\</bean>
\\</beans>

Java:
package com.foo.hibernate;

import org.hibernate.SessionFactory;

public class HibernateUtil {

    private static SessionFactory sessionFactory_;
    
    public static SessionFactory getSessionFactory() { return sessionFactory_; }
    public static void setSessionFactory(SessionFactory sessionFactory) { sessionFactory_ = sessionFactory; }

}

Error:
ERROR|14:45:57,234| ContextLoader:215 - Context initialization failed
org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'hibernateUtil' defined in ServletContext resource [/WEB-INF/conf/applicationContext-hibernate.xml]: Initialization of bean failed; nested exception is java.lang.NullPointerException
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:445)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory$1.run(AbstractAutowireCapableBeanFactory.java:383)
at java.security.AccessController.doPrivileged(Native Method)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:353)
at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:245)
at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:169)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:242)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:164)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:400)
at org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:736)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:369)
at org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:261)
at org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:199)
at org.springframework.web.context.ContextLoaderListener.contextInitialized(ContextLoaderListener.java:45)
at org.apache.catalina.core.StandardContext.listenerStart(StandardContext.java:3830)
at org.apache.catalina.core.StandardContext.start(StandardContext.java:4337)
at org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1045)
at org.apache.catalina.core.StandardHost.start(StandardHost.java:719)
at org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1045)
at org.apache.catalina.core.StandardEngine.start(StandardEngine.java:443)
at org.apache.catalina.core.StandardService.start(StandardService.java:516)
at org.apache.catalina.core.StandardServer.start(StandardServer.java:710)
at org.apache.catalina.startup.Catalina.start(Catalina.java:566)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
at java.lang.reflect.Method.invoke(Unknown Source)
at org.apache.catalina.startup.Bootstrap.start(Bootstrap.java:288)
at org.apache.catalina.startup.Bootstrap.main(Bootstrap.java:413)
Caused by: java.lang.NullPointerException
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:876)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:437)
... 28 more

---

**Affects:** 2.5.1

**Issue Links:**
- #9024 NPE in AbstractAutowireCapableBeanFactory#populateBean() if bean wrapper is null and InstantiationAwareBeanPostProcessor are registered (_**\"duplicates\"**_)
- #9075 CLONE -Autowiring failed when one of the bean contains factory-method (_**\"is duplicated by\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9045","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9045/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9045/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9045/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9045","id":398084830,"node_id":"MDU6SXNzdWUzOTgwODQ4MzA=","number":9045,"title":"Autowiring failed when one of the bean contains factory-method [SPR-4367]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2008-01-21T21:34:18Z","updated_at":"2019-01-12T16:51:07Z","closed_at":"2012-06-19T02:28:16Z","author_association":"COLLABORATOR","body":"**[G](https://jira.spring.io/secure/ViewProfile.jspa?name=geniusgroup)** opened **[SPR-4367](https://jira.spring.io/browse/SPR-4367?redirect=false)** and commented\n\nAutowiring failed when one of the bean contains factory-method (Static setter). Even we don't apply autowire in that bean, it will also crash the whole framework.\n\nReference: http://forum.springframework.org/showthread.php?t=48399\n\nPartial XML:\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:context=\"http://www.springframework.org/schema/context\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd\nhttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-2.5.xsd\">\n\n<context:annotation-config />\n\n\\<bean id=\"hibernateUtil\" class=\"com.foo.hibernate.HibernateUtil\"\nfactory-method=\"setSessionFactory\">\n\\<constructor-arg ref=\"sessionFactory\" />\n\\</bean>\n\\</beans>\n\nJava:\npackage com.foo.hibernate;\n\nimport org.hibernate.SessionFactory;\n\npublic class HibernateUtil {\n\n    private static SessionFactory sessionFactory_;\n    \n    public static SessionFactory getSessionFactory() { return sessionFactory_; }\n    public static void setSessionFactory(SessionFactory sessionFactory) { sessionFactory_ = sessionFactory; }\n\n}\n\nError:\nERROR|14:45:57,234| ContextLoader:215 - Context initialization failed\norg.springframework.beans.factory.BeanCreationException: Error creating bean with name 'hibernateUtil' defined in ServletContext resource [/WEB-INF/conf/applicationContext-hibernate.xml]: Initialization of bean failed; nested exception is java.lang.NullPointerException\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:445)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory$1.run(AbstractAutowireCapableBeanFactory.java:383)\nat java.security.AccessController.doPrivileged(Native Method)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:353)\nat org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:245)\nat org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:169)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:242)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:164)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:400)\nat org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:736)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:369)\nat org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:261)\nat org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:199)\nat org.springframework.web.context.ContextLoaderListener.contextInitialized(ContextLoaderListener.java:45)\nat org.apache.catalina.core.StandardContext.listenerStart(StandardContext.java:3830)\nat org.apache.catalina.core.StandardContext.start(StandardContext.java:4337)\nat org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1045)\nat org.apache.catalina.core.StandardHost.start(StandardHost.java:719)\nat org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1045)\nat org.apache.catalina.core.StandardEngine.start(StandardEngine.java:443)\nat org.apache.catalina.core.StandardService.start(StandardService.java:516)\nat org.apache.catalina.core.StandardServer.start(StandardServer.java:710)\nat org.apache.catalina.startup.Catalina.start(Catalina.java:566)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\nat java.lang.reflect.Method.invoke(Unknown Source)\nat org.apache.catalina.startup.Bootstrap.start(Bootstrap.java:288)\nat org.apache.catalina.startup.Bootstrap.main(Bootstrap.java:413)\nCaused by: java.lang.NullPointerException\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.populateBean(AbstractAutowireCapableBeanFactory.java:876)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:437)\n... 28 more\n\n---\n\n**Affects:** 2.5.1\n\n**Issue Links:**\n- #9024 NPE in AbstractAutowireCapableBeanFactory#populateBean() if bean wrapper is null and InstantiationAwareBeanPostProcessor are registered (_**\"duplicates\"**_)\n- #9075 CLONE -Autowiring failed when one of the bean contains factory-method (_**\"is duplicated by\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453326155","453326157","453326159","453326160"], "labels":["in: core","status: duplicate"]}