{"id":"20390", "title":"Access to request parameters via @RequestParam within form PUT request handlers is broken (HttpPutFormContentFilter) [SPR-15835]", "body":"**[Jonas Havers](https://jira.spring.io/secure/ViewProfile.jspa?name=jonashavers)** opened **[SPR-15835](https://jira.spring.io/browse/SPR-15835?redirect=false)** and commented

Until spring-web 4.3.9, I used the hidden HTML form parameter `_method=\"put\"` to submit data for entity changes. After passing the `HiddenHttpMethodFilter`, the request method is set to PUT. The [fix](https://github.com/spring-projects/spring-framework/commit/eb8454789429f30bbbe0a32d36406603f7d5ee25) in the `HttpPutFormContentFilter` for the issue #20308 (spring-web 4.3.10) breaks the possibility to access HTML form field values via `@RequestParam` in Spring controllers. If this break is intended, how can I make reasonable use of `HiddenHttpMethodFilter` now?

An example HTML form to showcase the issue:

```
<form id=\"form\" action=\"/demo-issue\" method=\"POST\" enctype=\"application/x-www-form-urlencoded\">
  <input type=\"hidden\" name=\"_method\" value=\"PUT\">
  <input type=\"hidden\" name=\"hiddenField\" value=\"testHidden\">
  <input name=\"publicField\" value=\"testPublic\">
  <button>Post form</button>
</form>
```

The given enctype \"application/x-www-form-urlencoded\" is the default and can therefore be left out.

A corresponding controller:

```
@Controller
@RequestMapping(\"/demo-issue\")
class DemoIssueController {
  @PutMapping
  public ResponseEntity<String> demoIssue(HttpServletRequest request, @RequestParam String hiddenField, @RequestParam String publicField) {
    return ResponseEntity.ok(String.format(\"hidden=%s;public=%s\", hiddenField, publicField));
  }
}
```

The result after submitting the form is:

```
There was an unexpected error (type=Bad Request, status=400).
Required String parameter 'hiddenField' is not present
```

The expected result in the response body is:

```
hidden=testHidden;public=testPublic
```

There is a workaround by using `enctype=\"multipart/form-data` in the form because it does not match the condition in the `HttpPutFormContentFilter#isFormContentType` method (which might be another issue).

---

**Affects:** 4.3.10

**Issue Links:**
- #20383 Parameter values are null when making a PUT request (_**\"duplicates\"**_)
- #20308 MockMvc duplicates PUT Parameter value

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20390","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20390/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20390/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20390/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20390","id":398213698,"node_id":"MDU6SXNzdWUzOTgyMTM2OTg=","number":20390,"title":"Access to request parameters via @RequestParam within form PUT request handlers is broken (HttpPutFormContentFilter) [SPR-15835]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2017-07-31T11:40:24Z","updated_at":"2019-01-12T16:45:04Z","closed_at":"2017-07-31T13:00:13Z","author_association":"COLLABORATOR","body":"**[Jonas Havers](https://jira.spring.io/secure/ViewProfile.jspa?name=jonashavers)** opened **[SPR-15835](https://jira.spring.io/browse/SPR-15835?redirect=false)** and commented\n\nUntil spring-web 4.3.9, I used the hidden HTML form parameter `_method=\"put\"` to submit data for entity changes. After passing the `HiddenHttpMethodFilter`, the request method is set to PUT. The [fix](https://github.com/spring-projects/spring-framework/commit/eb8454789429f30bbbe0a32d36406603f7d5ee25) in the `HttpPutFormContentFilter` for the issue #20308 (spring-web 4.3.10) breaks the possibility to access HTML form field values via `@RequestParam` in Spring controllers. If this break is intended, how can I make reasonable use of `HiddenHttpMethodFilter` now?\n\nAn example HTML form to showcase the issue:\n\n```\n<form id=\"form\" action=\"/demo-issue\" method=\"POST\" enctype=\"application/x-www-form-urlencoded\">\n  <input type=\"hidden\" name=\"_method\" value=\"PUT\">\n  <input type=\"hidden\" name=\"hiddenField\" value=\"testHidden\">\n  <input name=\"publicField\" value=\"testPublic\">\n  <button>Post form</button>\n</form>\n```\n\nThe given enctype \"application/x-www-form-urlencoded\" is the default and can therefore be left out.\n\nA corresponding controller:\n\n```\n@Controller\n@RequestMapping(\"/demo-issue\")\nclass DemoIssueController {\n  @PutMapping\n  public ResponseEntity<String> demoIssue(HttpServletRequest request, @RequestParam String hiddenField, @RequestParam String publicField) {\n    return ResponseEntity.ok(String.format(\"hidden=%s;public=%s\", hiddenField, publicField));\n  }\n}\n```\n\nThe result after submitting the form is:\n\n```\nThere was an unexpected error (type=Bad Request, status=400).\nRequired String parameter 'hiddenField' is not present\n```\n\nThe expected result in the response body is:\n\n```\nhidden=testHidden;public=testPublic\n```\n\nThere is a workaround by using `enctype=\"multipart/form-data` in the form because it does not match the condition in the `HttpPutFormContentFilter#isFormContentType` method (which might be another issue).\n\n---\n\n**Affects:** 4.3.10\n\n**Issue Links:**\n- #20383 Parameter values are null when making a PUT request (_**\"duplicates\"**_)\n- #20308 MockMvc duplicates PUT Parameter value\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453459396","453459398"], "labels":["in: web","status: duplicate"]}