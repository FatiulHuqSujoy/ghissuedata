{"id":"19285", "title":"Possible Bug: MappingJackson2XmlHttpMessageConverter does not share same ObjectMapper than MappingJackson2HttpMessageConverter [SPR-14720]", "body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-14720](https://jira.spring.io/browse/SPR-14720?redirect=false)** and commented

I want start here because is related with `HttpMessageConverter` s

After to read: [Latest Jackson integration improvements in Spring](https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring)

I have the following for `JSON`:

```java
@Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){
    	MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    	converter.setObjectMapper(jackson2ObjectMapperBuilder().build());
    	converter.setPrettyPrint(true);
    	return converter;
    }

    @Bean
    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder(){
    	Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    	builder.simpleDateFormat(\"yyyy-MM-dd\");
    	return builder;
    }
```

I've confirmed that `setObjectMapper` requires an `ObjectMapper` from the `com.fasterxml.jackson.databind` package.

Now I have the following for `XML`:

```java
@Bean
public MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter(){
		MappingJackson2XmlHttpMessageConverter converter = new MappingJackson2XmlHttpMessageConverter();
		converter.setPrettyPrint(true);
		converter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_XML));
		converter.setObjectMapper(jackson2ObjectMapperBuilder().build());
		return converter;
}
```

See I am using again `converter.setObjectMapper(jackson2ObjectMapperBuilder().build())` sentence and I've confirmed that the `converter.setObjectMapper` method requires the same `ObjectMapper` type from the `com.fasterxml.jackson.databind` package.

I've confirmed both scenarios just doing `Ctrl + click` to the `setObjectMapper` method and then `ObjectMapper` type parameter.

Therefore `MappingJackson2HttpMessageConverter.setObjectMapper` and `MappingJackson2XmlHttpMessageConverter.setObjectMapper` methods use the same `ObjectMapper` type

Now the `Jackson2ObjectMapperBuilder().build()` returns an `ObjectMapper` from the same `com.fasterxml.jackson.databind` package. Until here all seem fine.

But when Spring creates the `Spring ApplicationContext` through testings I get the following error message:

```java
Caused by: java.lang.IllegalArgumentException: class com.fasterxml.jackson.databind.ObjectMapper is not assignable to class com.fasterxml.jackson.dataformat.xml.XmlMapper
	at org.springframework.util.Assert.isAssignable(Assert.java:376)
	at org.springframework.util.Assert.isAssignable(Assert.java:359)
	at org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter.setObjectMapper(MappingJackson2XmlHttpMessageConverter.java:72)
	at com.manuel.jordan.config.httpmessageconverter.HttpMessageConverterXmlConfig.mappingJackson2XmlHttpMessageConverter(HttpMessageConverterXmlConfig.java:69)
	a
```

Even when the error is closely related with `Jackson` I need your confirmation it is really true before to report this issue in its Github account. Perhaps is an error of configuration by my side.


---

**Affects:** 4.3 GA, 4.3.1, 4.3.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19285","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19285/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19285/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19285/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19285","id":398198243,"node_id":"MDU6SXNzdWUzOTgxOTgyNDM=","number":19285,"title":"Possible Bug: MappingJackson2XmlHttpMessageConverter does not share same ObjectMapper than MappingJackson2HttpMessageConverter [SPR-14720]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2016-09-15T13:33:51Z","updated_at":"2019-01-12T05:21:58Z","closed_at":"2016-09-15T14:52:39Z","author_association":"COLLABORATOR","body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-14720](https://jira.spring.io/browse/SPR-14720?redirect=false)** and commented\n\nI want start here because is related with `HttpMessageConverter` s\n\nAfter to read: [Latest Jackson integration improvements in Spring](https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring)\n\nI have the following for `JSON`:\n\n```java\n@Bean\n    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){\n    \tMappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();\n    \tconverter.setObjectMapper(jackson2ObjectMapperBuilder().build());\n    \tconverter.setPrettyPrint(true);\n    \treturn converter;\n    }\n\n    @Bean\n    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder(){\n    \tJackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();\n    \tbuilder.simpleDateFormat(\"yyyy-MM-dd\");\n    \treturn builder;\n    }\n```\n\nI've confirmed that `setObjectMapper` requires an `ObjectMapper` from the `com.fasterxml.jackson.databind` package.\n\nNow I have the following for `XML`:\n\n```java\n@Bean\npublic MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter(){\n\t\tMappingJackson2XmlHttpMessageConverter converter = new MappingJackson2XmlHttpMessageConverter();\n\t\tconverter.setPrettyPrint(true);\n\t\tconverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_XML));\n\t\tconverter.setObjectMapper(jackson2ObjectMapperBuilder().build());\n\t\treturn converter;\n}\n```\n\nSee I am using again `converter.setObjectMapper(jackson2ObjectMapperBuilder().build())` sentence and I've confirmed that the `converter.setObjectMapper` method requires the same `ObjectMapper` type from the `com.fasterxml.jackson.databind` package.\n\nI've confirmed both scenarios just doing `Ctrl + click` to the `setObjectMapper` method and then `ObjectMapper` type parameter.\n\nTherefore `MappingJackson2HttpMessageConverter.setObjectMapper` and `MappingJackson2XmlHttpMessageConverter.setObjectMapper` methods use the same `ObjectMapper` type\n\nNow the `Jackson2ObjectMapperBuilder().build()` returns an `ObjectMapper` from the same `com.fasterxml.jackson.databind` package. Until here all seem fine.\n\nBut when Spring creates the `Spring ApplicationContext` through testings I get the following error message:\n\n```java\nCaused by: java.lang.IllegalArgumentException: class com.fasterxml.jackson.databind.ObjectMapper is not assignable to class com.fasterxml.jackson.dataformat.xml.XmlMapper\n\tat org.springframework.util.Assert.isAssignable(Assert.java:376)\n\tat org.springframework.util.Assert.isAssignable(Assert.java:359)\n\tat org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter.setObjectMapper(MappingJackson2XmlHttpMessageConverter.java:72)\n\tat com.manuel.jordan.config.httpmessageconverter.HttpMessageConverterXmlConfig.mappingJackson2XmlHttpMessageConverter(HttpMessageConverterXmlConfig.java:69)\n\ta\n```\n\nEven when the error is closely related with `Jackson` I need your confirmation it is really true before to report this issue in its Github account. Perhaps is an error of configuration by my side.\n\n\n---\n\n**Affects:** 4.3 GA, 4.3.1, 4.3.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453445074","453445075"], "labels":["in: web","status: invalid"]}