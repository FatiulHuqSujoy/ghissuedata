{"id":"12664", "title":"WebSphereUowTransactionManager does not reset transaction timeout for UOW_TYPE_GLOBAL_TRANSACTION to default value once set [SPR-8009]", "body":"**[Lukas Herman](https://jira.spring.io/secure/ViewProfile.jspa?name=lukas.herman)** opened **[SPR-8009](https://jira.spring.io/browse/SPR-8009?redirect=false)** and commented

The following code does not reflect the use case when Timer Manager executes bean method annotated with `@Transactional(timeout >= 0)`. This timeout value is propagated to next executed thread, causing timeout exceptions for `@Transactional()` methods relying on default timeout.

```
if (definition.getTimeout() > TransactionDefinition.TIMEOUT_DEFAULT) {
  this.uowManager.setUOWTimeout(uowType, definition.getTimeout());
}
```

A simple test case causes the '`defaultTransaction`' method fail with message:
`WTRN0006W: Transaction 0000012E723....0000001 has timed out after 10 seconds.`

```xml
<bean name=\"taskScheduler\" class=\"org.springframework.scheduling.commonj.TimerManagerTaskScheduler\">
    <property name=\"timerManagerName\" value=\"tm/default\"/>
    <property name=\"resourceRef\" value=\"true\"/>
</bean>
<bean name=\"taskExecutor\" class=\"org.springframework.scheduling.commonj.WorkManagerTaskExecutor\">
    <property name=\"workManagerName\" value=\"wm/default\"/>
    <property name=\"resourceRef\" value=\"true\"/>
</bean>

<bean name=\"transactionTimeoutTestBean\" class=\"some.TransactionTimeoutTest\"/>

<task:scheduled-tasks scheduler=\"taskScheduler\">
    <task:scheduled ref=\"transactionTimeoutTestBean\" method=\"defaultTransaction\" fixed-rate=\"15000\"/>
    <task:scheduled ref=\"transactionTimeoutTestBean\" method=\"transactionTimeout10\" fixed-rate=\"12000\"/>
```

```
public class TransactionTimeoutTest {
    @Transactional(rollbackFor = Exception.class)
    public void defaultTransaction() throws Exception {
        Thread.sleep(11000);
    }

    @Transactional(timeout = 10, rollbackFor = Exception.class)
    public void transactionTimeout10() throws Exception {
        Thread.sleep(2000);
    }
}
```

According to the [WebSphere API](http://publib.boulder.ibm.com/infocenter/wasinfo/v6r0/index.jsp?topic=/com.ibm.websphere.javadoc.doc/public_html/spi/com/ibm/wsspi/uow/UOWManager.html), the correct code should be

```
if (definition.getTimeout() > TransactionDefinition.TIMEOUT_DEFAULT) {
  this.uowManager.setUOWTimeout(uowType, definition.getTimeout());
} else {
  if (uowType == UOWManager.UOW_TYPE_GLOBAL_TRANSACTION) {
    this.uowManager.setUOWTimeout(uowType, 0);
  }
}
```



---

**Affects:** 3.1 M1

1 votes, 1 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12664","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12664/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12664/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12664/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12664","id":398110513,"node_id":"MDU6SXNzdWUzOTgxMTA1MTM=","number":12664,"title":"WebSphereUowTransactionManager does not reset transaction timeout for UOW_TYPE_GLOBAL_TRANSACTION to default value once set [SPR-8009]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2011-03-01T09:14:27Z","updated_at":"2019-01-12T16:26:03Z","closed_at":"2018-12-28T11:46:32Z","author_association":"COLLABORATOR","body":"**[Lukas Herman](https://jira.spring.io/secure/ViewProfile.jspa?name=lukas.herman)** opened **[SPR-8009](https://jira.spring.io/browse/SPR-8009?redirect=false)** and commented\n\nThe following code does not reflect the use case when Timer Manager executes bean method annotated with `@Transactional(timeout >= 0)`. This timeout value is propagated to next executed thread, causing timeout exceptions for `@Transactional()` methods relying on default timeout.\n\n```\nif (definition.getTimeout() > TransactionDefinition.TIMEOUT_DEFAULT) {\n  this.uowManager.setUOWTimeout(uowType, definition.getTimeout());\n}\n```\n\nA simple test case causes the '`defaultTransaction`' method fail with message:\n`WTRN0006W: Transaction 0000012E723....0000001 has timed out after 10 seconds.`\n\n```xml\n<bean name=\"taskScheduler\" class=\"org.springframework.scheduling.commonj.TimerManagerTaskScheduler\">\n    <property name=\"timerManagerName\" value=\"tm/default\"/>\n    <property name=\"resourceRef\" value=\"true\"/>\n</bean>\n<bean name=\"taskExecutor\" class=\"org.springframework.scheduling.commonj.WorkManagerTaskExecutor\">\n    <property name=\"workManagerName\" value=\"wm/default\"/>\n    <property name=\"resourceRef\" value=\"true\"/>\n</bean>\n\n<bean name=\"transactionTimeoutTestBean\" class=\"some.TransactionTimeoutTest\"/>\n\n<task:scheduled-tasks scheduler=\"taskScheduler\">\n    <task:scheduled ref=\"transactionTimeoutTestBean\" method=\"defaultTransaction\" fixed-rate=\"15000\"/>\n    <task:scheduled ref=\"transactionTimeoutTestBean\" method=\"transactionTimeout10\" fixed-rate=\"12000\"/>\n```\n\n```\npublic class TransactionTimeoutTest {\n    @Transactional(rollbackFor = Exception.class)\n    public void defaultTransaction() throws Exception {\n        Thread.sleep(11000);\n    }\n\n    @Transactional(timeout = 10, rollbackFor = Exception.class)\n    public void transactionTimeout10() throws Exception {\n        Thread.sleep(2000);\n    }\n}\n```\n\nAccording to the [WebSphere API](http://publib.boulder.ibm.com/infocenter/wasinfo/v6r0/index.jsp?topic=/com.ibm.websphere.javadoc.doc/public_html/spi/com/ibm/wsspi/uow/UOWManager.html), the correct code should be\n\n```\nif (definition.getTimeout() > TransactionDefinition.TIMEOUT_DEFAULT) {\n  this.uowManager.setUOWTimeout(uowType, definition.getTimeout());\n} else {\n  if (uowType == UOWManager.UOW_TYPE_GLOBAL_TRANSACTION) {\n    this.uowManager.setUOWTimeout(uowType, 0);\n  }\n}\n```\n\n\n\n---\n\n**Affects:** 3.1 M1\n\n1 votes, 1 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453357388","453357389"], "labels":["in: data","status: declined"]}