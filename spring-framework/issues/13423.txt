{"id":"13423", "title":"JaxbElementPayloadMethodProcessor supports some RequestPayload that should not be supported [SPR-8780]", "body":"**[omar elmandour](https://jira.spring.io/secure/ViewProfile.jspa?name=ramo)** opened **[SPR-8780](https://jira.spring.io/browse/SPR-8780?redirect=false)** and commented

While i was trying to migrate some endPoint to the new  EndPoint model with  `@RequestPayload`, i have encountered this issue.

Previously during the migration, any bad migration will safely failed against any invocation with the well known exception :

```java
No adapter for endpoint YYY. Is your endpoint annotated with @Endpoint, or does it implement ...
```

But let say that the result of the migration, the endpoint looks like :

```java
public void saveTheWorld(@RequestPayload JAXBElement<org.yourOwnPojo.Person> request) {}
```

It should failed because org.yourOwnPojo.Person is not a jaxb one.
This tests below show that it will not fail.

```java
 package org.springframework.ws.server.endpoint.adapter.method.jaxb;

import java.lang.reflect.Method;

import javax.xml.bind.JAXBElement;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.MethodParameter;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
/**
 * 
 * @author elmandour omar
 */
public class TestProcessor {
	
	@Test
	public void testShouldNotSupportTheRequestPayload(){
		JaxbElementPayloadMethodProcessor processor = new JaxbElementPayloadMethodProcessor();
		Method mymethod = SupermanEndPoint.class.getMethods()[0];
		Assert.assertTrue(processor.supportsRequestPayloadParameter(new MethodParameter(mymethod,0)));
	}
}

 class SupermanEndPoint {
    public void saveTheWorld(@RequestPayload JAXBElement<NotAJaxbObject> request) {}
  }
 
 class NotAJaxbObject{}
```

The marshalling will works fine and the request object is created but with null or default values (int..)

You might wonder how can someone confuse his own pojo object with a jaxb one ?
Like me by having typically the same name, for the jaxb and your pojo object.

Maybe the supportsRequestPayloadParameter should test that the embedded object is a real jaxb one (with `@XmlType` annotation from the package javax.xml.bind.annotation ?)

Thanks for reading me


---
No further details from [SPR-8780](https://jira.spring.io/browse/SPR-8780?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13423","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13423/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13423/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13423/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13423","id":398115100,"node_id":"MDU6SXNzdWUzOTgxMTUxMDA=","number":13423,"title":"JaxbElementPayloadMethodProcessor supports some RequestPayload that should not be supported [SPR-8780]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false},"assignees":[{"login":"poutsma","id":330665,"node_id":"MDQ6VXNlcjMzMDY2NQ==","avatar_url":"https://avatars2.githubusercontent.com/u/330665?v=4","gravatar_id":"","url":"https://api.github.com/users/poutsma","html_url":"https://github.com/poutsma","followers_url":"https://api.github.com/users/poutsma/followers","following_url":"https://api.github.com/users/poutsma/following{/other_user}","gists_url":"https://api.github.com/users/poutsma/gists{/gist_id}","starred_url":"https://api.github.com/users/poutsma/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/poutsma/subscriptions","organizations_url":"https://api.github.com/users/poutsma/orgs","repos_url":"https://api.github.com/users/poutsma/repos","events_url":"https://api.github.com/users/poutsma/events{/privacy}","received_events_url":"https://api.github.com/users/poutsma/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2011-09-06T10:34:05Z","updated_at":"2011-12-06T13:32:07Z","closed_at":"2011-12-06T13:32:07Z","author_association":"COLLABORATOR","body":"**[omar elmandour](https://jira.spring.io/secure/ViewProfile.jspa?name=ramo)** opened **[SPR-8780](https://jira.spring.io/browse/SPR-8780?redirect=false)** and commented\n\nWhile i was trying to migrate some endPoint to the new  EndPoint model with  `@RequestPayload`, i have encountered this issue.\n\nPreviously during the migration, any bad migration will safely failed against any invocation with the well known exception :\n\n```java\nNo adapter for endpoint YYY. Is your endpoint annotated with @Endpoint, or does it implement ...\n```\n\nBut let say that the result of the migration, the endpoint looks like :\n\n```java\npublic void saveTheWorld(@RequestPayload JAXBElement<org.yourOwnPojo.Person> request) {}\n```\n\nIt should failed because org.yourOwnPojo.Person is not a jaxb one.\nThis tests below show that it will not fail.\n\n```java\n package org.springframework.ws.server.endpoint.adapter.method.jaxb;\n\nimport java.lang.reflect.Method;\n\nimport javax.xml.bind.JAXBElement;\n\nimport org.junit.Assert;\nimport org.junit.Test;\nimport org.springframework.core.MethodParameter;\nimport org.springframework.ws.server.endpoint.annotation.RequestPayload;\n/**\n * \n * @author elmandour omar\n */\npublic class TestProcessor {\n\t\n\t@Test\n\tpublic void testShouldNotSupportTheRequestPayload(){\n\t\tJaxbElementPayloadMethodProcessor processor = new JaxbElementPayloadMethodProcessor();\n\t\tMethod mymethod = SupermanEndPoint.class.getMethods()[0];\n\t\tAssert.assertTrue(processor.supportsRequestPayloadParameter(new MethodParameter(mymethod,0)));\n\t}\n}\n\n class SupermanEndPoint {\n    public void saveTheWorld(@RequestPayload JAXBElement<NotAJaxbObject> request) {}\n  }\n \n class NotAJaxbObject{}\n```\n\nThe marshalling will works fine and the request object is created but with null or default values (int..)\n\nYou might wonder how can someone confuse his own pojo object with a jaxb one ?\nLike me by having typically the same name, for the jaxb and your pojo object.\n\nMaybe the supportsRequestPayloadParameter should test that the embedded object is a real jaxb one (with `@XmlType` annotation from the package javax.xml.bind.annotation ?)\n\nThanks for reading me\n\n\n---\nNo further details from [SPR-8780](https://jira.spring.io/browse/SPR-8780?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453362835","453362836"], "labels":["in: data","status: declined","type: enhancement"]}