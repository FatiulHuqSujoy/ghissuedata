{"id":"19453", "title":"Execute getBeansWithAnnotation when bean has a @RefreshScope annotation returns unexpected number of results [SPR-14887]", "body":"**[Sander Wartenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=sanderwartenberg)** opened **[SPR-14887](https://jira.spring.io/browse/SPR-14887?redirect=false)** and commented

We have a problem using the `@RefreshScope` annotation when using the following method.
For context, we are using Spring Boot 1.3.8 with Spring Cloud Config 1.0.6.

```java
@RefreshScope
@ServiceFactory
public class MyServiceFactory implements IServiceFactory {}
```

Custom annotation:

```java
@Component
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ServiceFactory {}
```

```java
public final class AnnotatedServiceFactory {

private void loadFactories(ApplicationContext ctx, String projectName) {
    Map<String, Object> beans = ctx.getBeansWithAnnotation(ServiceFactory.class);
    
    // I expected one bean, received 2 beans.
  }
}
```

We have a class 'AnnotatedServiceFactory' which scans the beans in the applicationContext which have an annotation called 'ServiceFactory'.

We created a class called 'MyServiceFactory' with the `@ServiceFactory` annotation but also the `@RefreshScope` annotation because we want to refresh this bean when Spring Cloud Config refresh is called.

In the method loadFactories I execute ctx.getBeansWithAnnotation and I expected to receive 1 bean but I got 2. The first is called 'scopedTarget.myServiceFactory' and the second is called 'myServiceFactory'.

Can you verify if this is a bug or expected behaviour? If it is expected, is there a way to filter those 'scopedTarget.*' beans?


---

**Affects:** 4.2.7
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19453","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19453/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19453/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19453/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19453","id":398200181,"node_id":"MDU6SXNzdWUzOTgyMDAxODE=","number":19453,"title":"Execute getBeansWithAnnotation when bean has a @RefreshScope annotation returns unexpected number of results [SPR-14887]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2016-11-08T13:28:11Z","updated_at":"2019-01-12T00:12:01Z","closed_at":"2019-01-12T00:12:01Z","author_association":"COLLABORATOR","body":"**[Sander Wartenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=sanderwartenberg)** opened **[SPR-14887](https://jira.spring.io/browse/SPR-14887?redirect=false)** and commented\n\nWe have a problem using the `@RefreshScope` annotation when using the following method.\nFor context, we are using Spring Boot 1.3.8 with Spring Cloud Config 1.0.6.\n\n```java\n@RefreshScope\n@ServiceFactory\npublic class MyServiceFactory implements IServiceFactory {}\n```\n\nCustom annotation:\n\n```java\n@Component\n@Retention(RetentionPolicy.RUNTIME)\n@Target(ElementType.TYPE)\npublic @interface ServiceFactory {}\n```\n\n```java\npublic final class AnnotatedServiceFactory {\n\nprivate void loadFactories(ApplicationContext ctx, String projectName) {\n    Map<String, Object> beans = ctx.getBeansWithAnnotation(ServiceFactory.class);\n    \n    // I expected one bean, received 2 beans.\n  }\n}\n```\n\nWe have a class 'AnnotatedServiceFactory' which scans the beans in the applicationContext which have an annotation called 'ServiceFactory'.\n\nWe created a class called 'MyServiceFactory' with the `@ServiceFactory` annotation but also the `@RefreshScope` annotation because we want to refresh this bean when Spring Cloud Config refresh is called.\n\nIn the method loadFactories I execute ctx.getBeansWithAnnotation and I expected to receive 1 bean but I got 2. The first is called 'scopedTarget.myServiceFactory' and the second is called 'myServiceFactory'.\n\nCan you verify if this is a bug or expected behaviour? If it is expected, is there a way to filter those 'scopedTarget.*' beans?\n\n\n---\n\n**Affects:** 4.2.7\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453446978","453446980","453446983","453695018"], "labels":["in: core","status: bulk-closed"]}