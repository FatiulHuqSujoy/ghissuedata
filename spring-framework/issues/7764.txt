{"id":"7764", "title":"Chapter 8 Typos [SPR-3078]", "body":"**[Karl Moore](https://jira.spring.io/secure/ViewProfile.jspa?name=karldmoore)** opened **[SPR-3078](https://jira.spring.io/browse/SPR-3078?redirect=false)** and commented

[b]8.3.2. Dependency Injection of test fixtures[/b]

    <!-- this bean will be injected into the HibernateTitleDaoTests class -->
    <bean id=\"titleDao\" class=\"com/foo/dao/hibernate/HibernateTitleDao\">
        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>
    </bean>

should be

    <!-- this bean will be injected into the HibernateTitleDaoTests class -->
    <bean id=\"titleDao\" class=\"com.foo.dao.hibernate.HibernateTitleDao\">
        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>
    </bean>

[b]8.3.5. Example[/b]

public class HibernateClinicTests extends AbstractClinicTests {

protected String[] getConfigLocations() {
return new String[] {
'/org/springframework/samples/petclinic/hibernate/applicationContext-hibernate.xml'
};
}
}

should be

public class HibernateClinicTests extends AbstractClinicTests {

protected String[] getConfigLocations() {
return new String[] {
\"/org/springframework/samples/petclinic/hibernate/applicationContext-hibernate.xml\"
};
}
}


---

**Affects:** 2.0.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7764","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7764/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7764/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7764/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7764","id":398074884,"node_id":"MDU6SXNzdWUzOTgwNzQ4ODQ=","number":7764,"title":"Chapter 8 Typos [SPR-3078]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511868,"node_id":"MDU6TGFiZWwxMTg4NTExODY4","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20documentation","name":"type: documentation","color":"e3d9fc","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/40","html_url":"https://github.com/spring-projects/spring-framework/milestone/40","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/40/labels","id":3960811,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxMQ==","number":40,"title":"2.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":140,"state":"closed","created_at":"2019-01-10T22:02:40Z","updated_at":"2019-01-11T00:55:00Z","due_on":null,"closed_at":"2019-01-10T22:02:40Z"},"comments":2,"created_at":"2007-01-25T06:49:42Z","updated_at":"2012-06-19T03:51:01Z","closed_at":"2012-06-19T03:51:01Z","author_association":"COLLABORATOR","body":"**[Karl Moore](https://jira.spring.io/secure/ViewProfile.jspa?name=karldmoore)** opened **[SPR-3078](https://jira.spring.io/browse/SPR-3078?redirect=false)** and commented\n\n[b]8.3.2. Dependency Injection of test fixtures[/b]\n\n    <!-- this bean will be injected into the HibernateTitleDaoTests class -->\n    <bean id=\"titleDao\" class=\"com/foo/dao/hibernate/HibernateTitleDao\">\n        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>\n    </bean>\n\nshould be\n\n    <!-- this bean will be injected into the HibernateTitleDaoTests class -->\n    <bean id=\"titleDao\" class=\"com.foo.dao.hibernate.HibernateTitleDao\">\n        <property name=\"sessionFactory\" ref=\"sessionFactory\"/>\n    </bean>\n\n[b]8.3.5. Example[/b]\n\npublic class HibernateClinicTests extends AbstractClinicTests {\n\nprotected String[] getConfigLocations() {\nreturn new String[] {\n'/org/springframework/samples/petclinic/hibernate/applicationContext-hibernate.xml'\n};\n}\n}\n\nshould be\n\npublic class HibernateClinicTests extends AbstractClinicTests {\n\nprotected String[] getConfigLocations() {\nreturn new String[] {\n\"/org/springframework/samples/petclinic/hibernate/applicationContext-hibernate.xml\"\n};\n}\n}\n\n\n---\n\n**Affects:** 2.0.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453314561","453314562"], "labels":["type: documentation","type: enhancement"]}