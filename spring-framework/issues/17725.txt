{"id":"17725", "title":"@ResponseBody doesn't work when used on the type level [SPR-13134]", "body":"**[俞火江](https://jira.spring.io/secure/ViewProfile.jspa?name=jddxf)** opened **[SPR-13134](https://jira.spring.io/browse/SPR-13134?redirect=false)** and commented

When I add `@ResponseBody` on the method level as follows:

```java
@Controller
//@ResponseBody
public class UserController {
	
	@RequestMapping(\"/hello\")
	@ResponseBody
	public String hello() {
		return \"hello\";
	}
}
```

It works as expected.

However, if I add the annotation on the type level:

```java
@Controller
@ResponseBody
public class UserController {
	
	@RequestMapping(\"/hello\")
	//@ResponseBody
	public String hello() {
		return \"hello\";
	}
}
```

Then an exception is thrown:
Servlet.service() for servlet [dispatcher] in context with path [/practice] threw exception [Circular view path [hello]: would dispatch back to the current handler URL [/practice/main/hello] again. Check your ViewResolver setup! (Hint: This may be the result of an unspecified view, due to default view name generation.)] with root cause
javax.servlet.ServletException: Circular view path [hello]: would dispatch back to the current handler URL [/practice/main/hello] again. Check your ViewResolver setup! (Hint: This may be the result of an unspecified view, due to default view name generation.)
at org.springframework.web.servlet.view.InternalResourceView.prepareForRendering(InternalResourceView.java:205)
at org.springframework.web.servlet.view.InternalResourceView.renderMergedOutputModel(InternalResourceView.java:145)
at org.springframework.web.servlet.view.AbstractView.render(AbstractView.java:303)
at org.springframework.web.servlet.DispatcherServlet.render(DispatcherServlet.java:1244)
at org.springframework.web.servlet.DispatcherServlet.processDispatchResult(DispatcherServlet.java:1027)
at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:971)
at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:893)
at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:966)
at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:857)


---

**Affects:** 4.1.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17725","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17725/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17725/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17725/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17725","id":398180790,"node_id":"MDU6SXNzdWUzOTgxODA3OTA=","number":17725,"title":"@ResponseBody doesn't work when used on the type level [SPR-13134]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":6,"created_at":"2015-06-15T19:08:17Z","updated_at":"2019-01-12T16:23:17Z","closed_at":"2015-06-17T01:12:09Z","author_association":"COLLABORATOR","body":"**[俞火江](https://jira.spring.io/secure/ViewProfile.jspa?name=jddxf)** opened **[SPR-13134](https://jira.spring.io/browse/SPR-13134?redirect=false)** and commented\n\nWhen I add `@ResponseBody` on the method level as follows:\n\n```java\n@Controller\n//@ResponseBody\npublic class UserController {\n\t\n\t@RequestMapping(\"/hello\")\n\t@ResponseBody\n\tpublic String hello() {\n\t\treturn \"hello\";\n\t}\n}\n```\n\nIt works as expected.\n\nHowever, if I add the annotation on the type level:\n\n```java\n@Controller\n@ResponseBody\npublic class UserController {\n\t\n\t@RequestMapping(\"/hello\")\n\t//@ResponseBody\n\tpublic String hello() {\n\t\treturn \"hello\";\n\t}\n}\n```\n\nThen an exception is thrown:\nServlet.service() for servlet [dispatcher] in context with path [/practice] threw exception [Circular view path [hello]: would dispatch back to the current handler URL [/practice/main/hello] again. Check your ViewResolver setup! (Hint: This may be the result of an unspecified view, due to default view name generation.)] with root cause\njavax.servlet.ServletException: Circular view path [hello]: would dispatch back to the current handler URL [/practice/main/hello] again. Check your ViewResolver setup! (Hint: This may be the result of an unspecified view, due to default view name generation.)\nat org.springframework.web.servlet.view.InternalResourceView.prepareForRendering(InternalResourceView.java:205)\nat org.springframework.web.servlet.view.InternalResourceView.renderMergedOutputModel(InternalResourceView.java:145)\nat org.springframework.web.servlet.view.AbstractView.render(AbstractView.java:303)\nat org.springframework.web.servlet.DispatcherServlet.render(DispatcherServlet.java:1244)\nat org.springframework.web.servlet.DispatcherServlet.processDispatchResult(DispatcherServlet.java:1027)\nat org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:971)\nat org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:893)\nat org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:966)\nat org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:857)\n\n\n---\n\n**Affects:** 4.1.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453426905","453426906","453426907","453426910","453426911","453426912"], "labels":["in: web","status: declined"]}