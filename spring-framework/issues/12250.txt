{"id":"12250", "title":"@InitBinder not called before @Controller method execution [SPR-7594]", "body":"**[Stefan Schmidt](https://jira.spring.io/secure/ViewProfile.jspa?name=sschmidt)** opened **[SPR-7594](https://jira.spring.io/browse/SPR-7594?redirect=false)** and commented

The following code in a `@Controller` will fails (extract from PetController.java):

```
@RequestMapping(params = { \"find=ByOwner\", \"form\" }, method = RequestMethod.GET)
public String findPetsByOwnerForm(Model model) {
   model.addAttribute(\"owners\", Owner.findAllOwners()); // throws \"ConverterNotFoundException: No converter found capable of converting from 'com.springsource.petclinic.domain.Owner' to 'java.lang.String'\"
     return \"pets/findPetsByOwner\";
}

@InitBinder
void registerConverters(WebDataBinder binder) {
  if (binder.getConversionService() instanceof GenericConversionService) {
     GenericConversionService conversionService = (GenericConversionService) binder.getConversionService();
     conversionService.addConverter(getOwnerConverter());
  }
}

Converter<Owner, String> getOwnerConverter() {
  return new Converter<Owner, String>() {
    public String convert(Owner owner) {
      return new StringBuilder().append(owner.getFirstName()).toString();
    }
  };
}
```

This is because registerConverters never gets called before findPetsByOwnerForm is being invoked, so the Owner to String converter does not get registered.

Steps to reproduce:

1. Create MYSQL database called 'petclinic', with username 'root' (change in src/main/resources/META-INF/spring/database.properties otherwise)
2. unpack attached application and run 'mvn tomcat:run' from project root
3. Create new Owner and Pet at http://localhost:8080/petclinic
4. Click on Pet > Find by Owner and see that the drop down is populated and the converter is registered and works
5. Restart application and click Pet > Find by Owner again - this time it fails for lack of registered converter
6. Click Pet > List all Pets and then Pet > Find by Owner and it works again (converter is registered)

Not sure if I have configured something incorrectly but it looks like a bug to me.



---

**Affects:** 3.0.4

**Attachments:**
- [petclinic.tar](https://jira.spring.io/secure/attachment/17141/petclinic.tar) (_420.00 kB_)

**Issue Links:**
- [ROO-1387](https://jira.spring.io/browse/ROO-1387) ConverterNotFoundException: No converter found (_**\"is depended on by\"**_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12250","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12250/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12250/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12250/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12250","id":398107738,"node_id":"MDU6SXNzdWUzOTgxMDc3Mzg=","number":12250,"title":"@InitBinder not called before @Controller method execution [SPR-7594]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2010-09-27T04:15:03Z","updated_at":"2019-01-12T05:31:32Z","closed_at":"2010-09-29T07:52:28Z","author_association":"COLLABORATOR","body":"**[Stefan Schmidt](https://jira.spring.io/secure/ViewProfile.jspa?name=sschmidt)** opened **[SPR-7594](https://jira.spring.io/browse/SPR-7594?redirect=false)** and commented\n\nThe following code in a `@Controller` will fails (extract from PetController.java):\n\n```\n@RequestMapping(params = { \"find=ByOwner\", \"form\" }, method = RequestMethod.GET)\npublic String findPetsByOwnerForm(Model model) {\n   model.addAttribute(\"owners\", Owner.findAllOwners()); // throws \"ConverterNotFoundException: No converter found capable of converting from 'com.springsource.petclinic.domain.Owner' to 'java.lang.String'\"\n     return \"pets/findPetsByOwner\";\n}\n\n@InitBinder\nvoid registerConverters(WebDataBinder binder) {\n  if (binder.getConversionService() instanceof GenericConversionService) {\n     GenericConversionService conversionService = (GenericConversionService) binder.getConversionService();\n     conversionService.addConverter(getOwnerConverter());\n  }\n}\n\nConverter<Owner, String> getOwnerConverter() {\n  return new Converter<Owner, String>() {\n    public String convert(Owner owner) {\n      return new StringBuilder().append(owner.getFirstName()).toString();\n    }\n  };\n}\n```\n\nThis is because registerConverters never gets called before findPetsByOwnerForm is being invoked, so the Owner to String converter does not get registered.\n\nSteps to reproduce:\n\n1. Create MYSQL database called 'petclinic', with username 'root' (change in src/main/resources/META-INF/spring/database.properties otherwise)\n2. unpack attached application and run 'mvn tomcat:run' from project root\n3. Create new Owner and Pet at http://localhost:8080/petclinic\n4. Click on Pet > Find by Owner and see that the drop down is populated and the converter is registered and works\n5. Restart application and click Pet > Find by Owner again - this time it fails for lack of registered converter\n6. Click Pet > List all Pets and then Pet > Find by Owner and it works again (converter is registered)\n\nNot sure if I have configured something incorrectly but it looks like a bug to me.\n\n\n\n---\n\n**Affects:** 3.0.4\n\n**Attachments:**\n- [petclinic.tar](https://jira.spring.io/secure/attachment/17141/petclinic.tar) (_420.00 kB_)\n\n**Issue Links:**\n- [ROO-1387](https://jira.spring.io/browse/ROO-1387) ConverterNotFoundException: No converter found (_**\"is depended on by\"**_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453354149","453354151","453354153","453354155"], "labels":["in: web","status: invalid"]}