{"id":"11525", "title":"Scheduled tasks seem to run twice [SPR-6859]", "body":"**[Matt Young](https://jira.spring.io/secure/ViewProfile.jspa?name=attack7)** opened **[SPR-6859](https://jira.spring.io/browse/SPR-6859?redirect=false)** and commented

I posted this on the forum, but with no response... I wondered if you all could look into it.

I was stubbing out some code that I'd like to run on a schedule, and just to test it, I have it running once every 60 seconds. I noticed that in my logging, the method seems to be executed twice. To make sure it wasn't a logging issue, I put a static int counter into the class and have it increment on each call. So I can confirm the method is actually being run twice.

I suspect that this is something like doing a context:component-scan twice-over and resulting in double execution, but can't seem to locate the problem.  I thought I would check first if there were any pointers.

The class
Code:
/**
* Date: Feb 15, 2010

* Time: 10:54:36 AM
  */
  `@Component`
  public class DataHarvestingServiceImpl implements DataHarvestingService {
  private static final Logger logger = Logger.getLogger(DataHarvestingServiceImpl.class);

  private static int count = 0;

  `@Override`
  //  Every night at 1 AM
  // `@Scheduled`(cron = \"* * 1 * * ?\")

  `@Scheduled`(cron = \"0 * * * * ?\")
  public void collectSocialMediaData() {
  logger.info(\"Starting data retrieval at \" + new Date(System.currentTimeMillis()));

      logger.info(\"Finished media data retrieval at \" + new Date(System.currentTimeMillis()));
      System.out.println(\"count is \" + count++);

  }

}
The configuration

Code:

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:task=\"http://www.springframework.org/schema/task\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.0.xsd\">

    <!--  This is the bit of configuration that allows spring to search for annotations
          that indicate that particular methods should be run at particular times.  -->
    
    <task:scheduler id=\"searchScheduler\"/>
    
    <task:executor id=\"searchExecutor\" pool-size=\"1\"/>
    
    <task:annotation-driven executor=\"searchExecutor\" scheduler=\"searchScheduler\"/>

\\</beans>
The output I see:
Code:
11316 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:56:00 CST 2010
11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:56:00 CST 2010
count is 0
11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:56:00 CST 2010
11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:56:00 CST 2010
count is 1
71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:57:00 CST 2010
71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:57:00 CST 2010
count is 2
71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:57:00 CST 2010
71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:57:00 CST 2010
count is 3
Any insight very much appreciated.

---

**Affects:** 3.0 GA

**Reference URL:** http://forum.springsource.org/showthread.php?t=84747

**Issue Links:**
- #11322 ScheduledAnnotationBeanPostProcessor registers schedules twice in web application (_**\"duplicates\"**_)
- #14094 CronSequenceGenerator fails to accurately compute earliest next date when using second expression
- #11669 CronTrigger is not triggered at correct time

5 votes, 21 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11525","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11525/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11525/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11525/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11525","id":398103048,"node_id":"MDU6SXNzdWUzOTgxMDMwNDg=","number":11525,"title":"Scheduled tasks seem to run twice [SPR-6859]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/90","html_url":"https://github.com/spring-projects/spring-framework/milestone/90","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/90/labels","id":3960863,"node_id":"MDk6TWlsZXN0b25lMzk2MDg2Mw==","number":90,"title":"3.2.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":107,"state":"closed","created_at":"2019-01-10T22:03:39Z","updated_at":"2019-01-11T07:45:47Z","due_on":"2013-03-13T07:00:00Z","closed_at":"2019-01-10T22:03:39Z"},"comments":23,"created_at":"2010-02-17T03:36:40Z","updated_at":"2019-01-11T17:01:48Z","closed_at":"2013-10-09T13:59:28Z","author_association":"COLLABORATOR","body":"**[Matt Young](https://jira.spring.io/secure/ViewProfile.jspa?name=attack7)** opened **[SPR-6859](https://jira.spring.io/browse/SPR-6859?redirect=false)** and commented\n\nI posted this on the forum, but with no response... I wondered if you all could look into it.\n\nI was stubbing out some code that I'd like to run on a schedule, and just to test it, I have it running once every 60 seconds. I noticed that in my logging, the method seems to be executed twice. To make sure it wasn't a logging issue, I put a static int counter into the class and have it increment on each call. So I can confirm the method is actually being run twice.\n\nI suspect that this is something like doing a context:component-scan twice-over and resulting in double execution, but can't seem to locate the problem.  I thought I would check first if there were any pointers.\n\nThe class\nCode:\n/**\n* Date: Feb 15, 2010\n\n* Time: 10:54:36 AM\n  */\n  `@Component`\n  public class DataHarvestingServiceImpl implements DataHarvestingService {\n  private static final Logger logger = Logger.getLogger(DataHarvestingServiceImpl.class);\n\n  private static int count = 0;\n\n  `@Override`\n  //  Every night at 1 AM\n  // `@Scheduled`(cron = \"* * 1 * * ?\")\n\n  `@Scheduled`(cron = \"0 * * * * ?\")\n  public void collectSocialMediaData() {\n  logger.info(\"Starting data retrieval at \" + new Date(System.currentTimeMillis()));\n\n      logger.info(\"Finished media data retrieval at \" + new Date(System.currentTimeMillis()));\n      System.out.println(\"count is \" + count++);\n\n  }\n\n}\nThe configuration\n\nCode:\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:task=\"http://www.springframework.org/schema/task\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.0.xsd\">\n\n    <!--  This is the bit of configuration that allows spring to search for annotations\n          that indicate that particular methods should be run at particular times.  -->\n    \n    <task:scheduler id=\"searchScheduler\"/>\n    \n    <task:executor id=\"searchExecutor\" pool-size=\"1\"/>\n    \n    <task:annotation-driven executor=\"searchExecutor\" scheduler=\"searchScheduler\"/>\n\n\\</beans>\nThe output I see:\nCode:\n11316 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:56:00 CST 2010\n11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:56:00 CST 2010\ncount is 0\n11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:56:00 CST 2010\n11321 [searchScheduler-1] INFO  com.vodori.cms.feature.socialMedia.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:56:00 CST 2010\ncount is 1\n71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:57:00 CST 2010\n71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:57:00 CST 2010\ncount is 2\n71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Starting data retrieval at Mon Feb 15 14:57:00 CST 2010\n71318 [searchScheduler-1] INFO  xxx.service.impl.DataHarvestingServiceImpl  - Finished media data retrieval at Mon Feb 15 14:57:00 CST 2010\ncount is 3\nAny insight very much appreciated.\n\n---\n\n**Affects:** 3.0 GA\n\n**Reference URL:** http://forum.springsource.org/showthread.php?t=84747\n\n**Issue Links:**\n- #11322 ScheduledAnnotationBeanPostProcessor registers schedules twice in web application (_**\"duplicates\"**_)\n- #14094 CronSequenceGenerator fails to accurately compute earliest next date when using second expression\n- #11669 CronTrigger is not triggered at correct time\n\n5 votes, 21 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453348251","453348252","453348253","453348254","453348255","453348257","453348258","453348261","453348262","453348263","453348265","453348266","453348268","453348269","453348270","453348271","453348272","453348274","453348276","453348277","453348278","453348279","453348281"], "labels":["in: core","type: bug"]}