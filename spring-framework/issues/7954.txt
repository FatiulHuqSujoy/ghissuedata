{"id":"7954", "title":"New Java 6 ServiceLoader support in the springframework core [SPR-3269]", "body":"**[Ben Tels](https://jira.spring.io/secure/ViewProfile.jspa?name=bentels)** opened **[SPR-3269](https://jira.spring.io/browse/SPR-3269?redirect=false)** and commented

L.S.

The Java SE 6 platform introduces the concept of a ServiceLoader, to replace the class.forName() construct. I think it would be useful to have a FactoryBean in the Spring Framework that would make it possible to inject services defined in a loader configuration file transparently, so clients don't have to know the details of the ServiceLoader. Something like this perhaps:

public class ServiceLoaderProxyFactory implements FactoryBean {

    private Class serviceType;
    private List serviceList;
    
    public synchronized Object getObject() throws Exception {
    	if (serviceList == null) {
    		loadServices();
    	}
    	return serviceList;
    }
    
    @SuppressWarnings(\"unchecked\")
    private void loadServices() {
    	serviceList = new ArrayList();
    	ServiceLoader<?> loader = ServiceLoader.load(serviceType);
    	for (Object service : loader) {
    		serviceList.add(service);
    	}
    }
    
    public Class getObjectType() {
    	// TODO Auto-generated method stub
    	return List.class;
    }
    
    public boolean isSingleton() {
    	// TODO Auto-generated method stub
    	return true;
    }
    
    public void setServiceType(final Class serviceType) {
    	this.serviceType = serviceType;
    }

}

Which could then obviously be used like this:

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

<beans:beans xmlns:beans=\"http://www.springframework.org/schema/beans\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd\">
<beans:bean id=\"services\" class=\"[whichever package].ServiceLoaderProxyFactory\">
<beans:property name=\"serviceType\">
<beans:value type=\"java.lang.Class\">[The interface being implemented by the services]</beans:value>
</beans:property>
</beans:bean>
</beans:beans>

Thanks!

BenTels


---
No further details from [SPR-3269](https://jira.spring.io/browse/SPR-3269?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7954","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7954/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7954/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7954/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7954","id":398076304,"node_id":"MDU6SXNzdWUzOTgwNzYzMDQ=","number":7954,"title":"New Java 6 ServiceLoader support in the springframework core [SPR-3269]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/48","html_url":"https://github.com/spring-projects/spring-framework/milestone/48","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/48/labels","id":3960820,"node_id":"MDk6TWlsZXN0b25lMzk2MDgyMA==","number":48,"title":"2.1 M2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":30,"state":"closed","created_at":"2019-01-10T22:02:49Z","updated_at":"2019-01-11T00:41:08Z","due_on":"2007-05-29T07:00:00Z","closed_at":"2019-01-10T22:02:49Z"},"comments":3,"created_at":"2007-03-15T23:11:06Z","updated_at":"2012-06-19T03:50:17Z","closed_at":"2012-06-19T03:50:17Z","author_association":"COLLABORATOR","body":"**[Ben Tels](https://jira.spring.io/secure/ViewProfile.jspa?name=bentels)** opened **[SPR-3269](https://jira.spring.io/browse/SPR-3269?redirect=false)** and commented\n\nL.S.\n\nThe Java SE 6 platform introduces the concept of a ServiceLoader, to replace the class.forName() construct. I think it would be useful to have a FactoryBean in the Spring Framework that would make it possible to inject services defined in a loader configuration file transparently, so clients don't have to know the details of the ServiceLoader. Something like this perhaps:\n\npublic class ServiceLoaderProxyFactory implements FactoryBean {\n\n    private Class serviceType;\n    private List serviceList;\n    \n    public synchronized Object getObject() throws Exception {\n    \tif (serviceList == null) {\n    \t\tloadServices();\n    \t}\n    \treturn serviceList;\n    }\n    \n    @SuppressWarnings(\"unchecked\")\n    private void loadServices() {\n    \tserviceList = new ArrayList();\n    \tServiceLoader<?> loader = ServiceLoader.load(serviceType);\n    \tfor (Object service : loader) {\n    \t\tserviceList.add(service);\n    \t}\n    }\n    \n    public Class getObjectType() {\n    \t// TODO Auto-generated method stub\n    \treturn List.class;\n    }\n    \n    public boolean isSingleton() {\n    \t// TODO Auto-generated method stub\n    \treturn true;\n    }\n    \n    public void setServiceType(final Class serviceType) {\n    \tthis.serviceType = serviceType;\n    }\n\n}\n\nWhich could then obviously be used like this:\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<beans:beans xmlns:beans=\"http://www.springframework.org/schema/beans\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd\">\n<beans:bean id=\"services\" class=\"[whichever package].ServiceLoaderProxyFactory\">\n<beans:property name=\"serviceType\">\n<beans:value type=\"java.lang.Class\">[The interface being implemented by the services]</beans:value>\n</beans:property>\n</beans:bean>\n</beans:beans>\n\nThanks!\n\nBenTels\n\n\n---\nNo further details from [SPR-3269](https://jira.spring.io/browse/SPR-3269?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453316358","453316361","453316363"], "labels":["in: core","type: enhancement"]}