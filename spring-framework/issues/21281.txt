{"id":"21281", "title":"Regression RequestRejectedException: The request was rejected because the URL was not normalized [SPR-16740]", "body":"**[Caleb Cushing](https://jira.spring.io/secure/ViewProfile.jspa?name=xenoterracide)** opened **[SPR-16740](https://jira.spring.io/browse/SPR-16740?redirect=false)** and commented

it's possible this regression belongs to a more specific component of spring, but I'm not sure where the best spot to report this is at this time

in Brussels-SR6, this code worked, now updating to SR9 and it's broken

```java
@Bean( name = \"viewResolver\" )
public InternalResourceViewResolver viewResolver() {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
    viewResolver.setViewClass( JstlView.class );
    viewResolver.setPrefix( \"/jsp/\" );
    viewResolver.setSuffix( \".jsp\" );
    return viewResolver;
}
```

this configuration was adapted from an older XML configuration a while ago.

Now when trying to visit a jsp? or at least some I get this.

```java
Apr 17, 2018 4:25:56 PM org.apache.catalina.core.StandardWrapperValve invoke
SEVERE: Servlet.service() for servlet [jsp] in context with path [] threw exception
org.springframework.security.web.firewall.RequestRejectedException: The request was rejected because the URL was not normalized.
	at org.springframework.security.web.firewall.StrictHttpFirewall.getFirewalledRequest(StrictHttpFirewall.java:248)
	at org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:193)
	at org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:177)
	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:347)
	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:263)
	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)
	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)
	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)
	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)
	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)
	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)
	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)
	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)
	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)
	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)
	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)
	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:790)
	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1459)
	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
	at java.lang.Thread.run(Thread.java:748)
```

i'm guessing this has something to do with one of the recent security vulns. internally when I debug the servelet context is `//jsp/...`. note: I think we were using `/jsp/` instead of `/jsp` because we didn't want to match `/jspFoo.jsp`, only the nested path.

**Update** That doesn't just fix it... we have a failing test with SAML. `Forwarded URL expected:</jsp/testSamlResponse.jsp> but was:</jsptestSamlResponse.jsp>`

not sure yet if it' simple to fix, at a glance it calls this piece of controller

```java
    private static final String TEST_SAML_RESPONSE_VIEW = \"testSamlResponse\";

    @RequestMapping( value = \"/generateSamlResponse\", method = RequestMethod.POST )
    public String doPost( Model model, HttpServletRequest request ) {

        final String serverName = request.getParameter( \"redirectHost\" );
        final String propertyFileName = request.getParameter( \"propertiesFileName\" );
        final SamlAssertionProperties properties = samlTestService.loadConfigration( \"classpath:/\" + propertyFileName );

        // Override properties from file with user selections and generate assertion
        updateProperties( request, properties );
        final String assertion = samlTestService.buildAssertion( properties );
        final String url = StringUtilsDex.isEmpty( serverName ) ? \"/saml/SSO\" : serverName;
        model.addAttribute( \"assertion\", assertion );
        model.addAttribute( \"url\", url );
        return TEST_SAML_RESPONSE_VIEW;
    }
```

**Update 2** This became an issue as of Brussels SR7

 

**Update 3** this looks like how we actually need to fix this, worth stating that `login` in this case is the name of the view. I don't know that removing the /jsp/ mappings are necessary, but neither do they seem to be relevant. Not sure how it ends up being `//jsp/..`

```java
--- a/dex-ui/src/main/webapp/WEB-INF/web.xml
+++ b/dex-ui/src/main/webapp/WEB-INF/web.xml
@@ -30,41 +30,6 @@
                <url-pattern>*.jsp</url-pattern>
        </servlet-mapping>
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/jsp/login.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/jsp/error.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/jsp/version.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/accessdenied.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/jsp/register.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/forgotPassword.jsp</url-pattern>
-       </servlet-mapping>
-
-       <servlet-mapping>
-               <servlet-name>jsp</servlet-name>
-               <url-pattern>/verifyUser.jsp</url-pattern>
-       </servlet-mapping>
-
        <error-page>
                <error-code>400</error-code>
                <location>/public/error/400</location>
@@ -86,7 +51,7 @@
        </error-page>        <welcome-file-list>
-               <welcome-file>/jsp/login.jsp</welcome-file>
+               <welcome-file>login</welcome-file>
        </welcome-file-list> 
```



---

**Affects:** 4.3.16

**Attachments:**
- [image-2018-04-20-12-08-57-772.png](https://jira.spring.io/secure/attachment/25589/image-2018-04-20-12-08-57-772.png) (_122.58 kB_)
- [image-2018-04-20-13-15-25-857.png](https://jira.spring.io/secure/attachment/25590/image-2018-04-20-13-15-25-857.png) (_149.34 kB_)
- [jsp-regression.zip](https://jira.spring.io/secure/attachment/25692/jsp-regression.zip) (_81.94 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21281","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21281/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21281/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21281/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21281","id":398226396,"node_id":"MDU6SXNzdWUzOTgyMjYzOTY=","number":21281,"title":"Regression RequestRejectedException: The request was rejected because the URL was not normalized [SPR-16740]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":null,"comments":12,"created_at":"2018-04-17T21:46:32Z","updated_at":"2019-01-12T16:21:25Z","closed_at":"2018-05-07T20:27:56Z","author_association":"COLLABORATOR","body":"**[Caleb Cushing](https://jira.spring.io/secure/ViewProfile.jspa?name=xenoterracide)** opened **[SPR-16740](https://jira.spring.io/browse/SPR-16740?redirect=false)** and commented\n\nit's possible this regression belongs to a more specific component of spring, but I'm not sure where the best spot to report this is at this time\n\nin Brussels-SR6, this code worked, now updating to SR9 and it's broken\n\n```java\n@Bean( name = \"viewResolver\" )\npublic InternalResourceViewResolver viewResolver() {\n    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();\n    viewResolver.setViewClass( JstlView.class );\n    viewResolver.setPrefix( \"/jsp/\" );\n    viewResolver.setSuffix( \".jsp\" );\n    return viewResolver;\n}\n```\n\nthis configuration was adapted from an older XML configuration a while ago.\n\nNow when trying to visit a jsp? or at least some I get this.\n\n```java\nApr 17, 2018 4:25:56 PM org.apache.catalina.core.StandardWrapperValve invoke\nSEVERE: Servlet.service() for servlet [jsp] in context with path [] threw exception\norg.springframework.security.web.firewall.RequestRejectedException: The request was rejected because the URL was not normalized.\n\tat org.springframework.security.web.firewall.StrictHttpFirewall.getFirewalledRequest(StrictHttpFirewall.java:248)\n\tat org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:193)\n\tat org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:177)\n\tat org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:347)\n\tat org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:263)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\n\tat org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\n\tat org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\n\tat org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\n\tat org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\n\tat org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\n\tat org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\n\tat org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\n\tat org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\n\tat org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:790)\n\tat org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1459)\n\tat org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\n\tat org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\n\tat java.lang.Thread.run(Thread.java:748)\n```\n\ni'm guessing this has something to do with one of the recent security vulns. internally when I debug the servelet context is `//jsp/...`. note: I think we were using `/jsp/` instead of `/jsp` because we didn't want to match `/jspFoo.jsp`, only the nested path.\n\n**Update** That doesn't just fix it... we have a failing test with SAML. `Forwarded URL expected:</jsp/testSamlResponse.jsp> but was:</jsptestSamlResponse.jsp>`\n\nnot sure yet if it' simple to fix, at a glance it calls this piece of controller\n\n```java\n    private static final String TEST_SAML_RESPONSE_VIEW = \"testSamlResponse\";\n\n    @RequestMapping( value = \"/generateSamlResponse\", method = RequestMethod.POST )\n    public String doPost( Model model, HttpServletRequest request ) {\n\n        final String serverName = request.getParameter( \"redirectHost\" );\n        final String propertyFileName = request.getParameter( \"propertiesFileName\" );\n        final SamlAssertionProperties properties = samlTestService.loadConfigration( \"classpath:/\" + propertyFileName );\n\n        // Override properties from file with user selections and generate assertion\n        updateProperties( request, properties );\n        final String assertion = samlTestService.buildAssertion( properties );\n        final String url = StringUtilsDex.isEmpty( serverName ) ? \"/saml/SSO\" : serverName;\n        model.addAttribute( \"assertion\", assertion );\n        model.addAttribute( \"url\", url );\n        return TEST_SAML_RESPONSE_VIEW;\n    }\n```\n\n**Update 2** This became an issue as of Brussels SR7\n\n \n\n**Update 3** this looks like how we actually need to fix this, worth stating that `login` in this case is the name of the view. I don't know that removing the /jsp/ mappings are necessary, but neither do they seem to be relevant. Not sure how it ends up being `//jsp/..`\n\n```java\n--- a/dex-ui/src/main/webapp/WEB-INF/web.xml\n+++ b/dex-ui/src/main/webapp/WEB-INF/web.xml\n@@ -30,41 +30,6 @@\n                <url-pattern>*.jsp</url-pattern>\n        </servlet-mapping>\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/jsp/login.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/jsp/error.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/jsp/version.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/accessdenied.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/jsp/register.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/forgotPassword.jsp</url-pattern>\n-       </servlet-mapping>\n-\n-       <servlet-mapping>\n-               <servlet-name>jsp</servlet-name>\n-               <url-pattern>/verifyUser.jsp</url-pattern>\n-       </servlet-mapping>\n-\n        <error-page>\n                <error-code>400</error-code>\n                <location>/public/error/400</location>\n@@ -86,7 +51,7 @@\n        </error-page>        <welcome-file-list>\n-               <welcome-file>/jsp/login.jsp</welcome-file>\n+               <welcome-file>login</welcome-file>\n        </welcome-file-list> \n```\n\n\n\n---\n\n**Affects:** 4.3.16\n\n**Attachments:**\n- [image-2018-04-20-12-08-57-772.png](https://jira.spring.io/secure/attachment/25589/image-2018-04-20-12-08-57-772.png) (_122.58 kB_)\n- [image-2018-04-20-13-15-25-857.png](https://jira.spring.io/secure/attachment/25590/image-2018-04-20-13-15-25-857.png) (_149.34 kB_)\n- [jsp-regression.zip](https://jira.spring.io/secure/attachment/25692/jsp-regression.zip) (_81.94 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453470703","453470705","453470707","453470708","453470712","453470713","453470715","453470716","453470719","453470720","453470722","453470723"], "labels":["in: web","status: declined"]}