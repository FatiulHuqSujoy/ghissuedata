{"id":"14331", "title":"DefaultAnnotationHandlerMapping is not able to find correct handler [SPR-9697]", "body":"**[Arun Kumar Kandregula](https://jira.spring.io/secure/ViewProfile.jspa?name=arun7it)** opened **[SPR-9697](https://jira.spring.io/browse/SPR-9697?redirect=false)** and commented

Background:
I am developing a Portlet using Spring MVC framework deployed in liferay 5.x server.
Currently I am using 3.0.0.RELEASE. Everything is working fine as expected.
That is, when I use annotations like

`@RenderMapping`(params=\"myaction=editFolderForm\")
`@RenderMapping`(params=\"myaction=editEntryForm\")
`@RenderMapping`  
`@ActionMapping`(params=\"myaction=editEntry\")
etc.

DefaultAnnotationHandlerMapping is working as expected in finding a handler for every request.

But, for some valid reason, I have to use more latest version which is 3.1.2.RELEASE instead of 3.0.0.RELEASE.

I observed that DefaultAnnotationHandlerMapping IS NOT WORKING as expected in finding a handler for every request. I figured out what is the problem by debugging the internals of Spring framework.
I want to explain it clearly so that someone can tell me if this is a Bug.

In the parent class of DefaultAnnotationHandlerMapping which is AbstractMapBasedHandlerMapping  :

    package org.springframework.web.portlet.handler;
    public abstract class AbstractMapBasedHandlerMapping<K> extends AbstractHandlerMapping {
    ....
    .... 
    
    /**
     * Determines a handler for the computed lookup key for the given request.
     * @see #getLookupKey
     */
    @Override
    @SuppressWarnings(\"unchecked\")
    protected Object getHandlerInternal(PortletRequest request) throws Exception {
    	K lookupKey = getLookupKey(request);
    	Object handler = this.handlerMap.get(lookupKey);
    	if (handler != null && logger.isDebugEnabled()) {
    		logger.debug(\"Key [\" + lookupKey + \"] -> handler [\" + handler + \"]\");
    	}
    	if (handler instanceof Map) {
    		Map<PortletRequestMappingPredicate, Object> predicateMap =
    				(Map<PortletRequestMappingPredicate, Object>) handler;
    		List<PortletRequestMappingPredicate> predicates =
    				new LinkedList<PortletRequestMappingPredicate>(predicateMap.keySet());
    
    
    	LINE 81:	Collections.sort(predicates); ///////////////// PROBLEM
    
    
    
    
    		for (PortletRequestMappingPredicate predicate : predicates) {
    			if (predicate.match(request)) {
    				predicate.validate(request);
    				return predicateMap.get(predicate);
    			}
    		}
    		return null;
    	}
    	return handler;
    }
    ....
    ....
    }

This sorting is screwed up in Spring 3.1.2 and is working perfectly well in Spring 3.0.0.
In next two sections I ll tell you why sorting matters and how it is screwed up in Spring 3.1.2.

> **why sorting matters ?**

This HandlerMapping is searching a sorted linked list node by node till it finds  match for a particular handler. In my code base, I have multiple Controllers whose methods are mapped with following annotations like

`@RenderMapping`  --- > default

`@RenderMapping`(params=\"myaction=editEntryController\")

`@RenderMapping`(params=\"myaction=editFolderController\")

etc.

The Collections.sort() depends on compareTo(..) method of each XXXPredicate class.

when a request comes first it should be checked if myaction parameter is equal to \"editEntryController\", \"editFolderController\", ...and finally if nothing matches, thn only default controller which is annotated with `@RenderMapping` annotation should be matched.

With Spring 3.0.0, its working exactly like that as expected.
Where as wth Spring 3.2.1, its not behaving like that.

With both the versions, before sorting, the list is the same.

    myaction=editEntry, 
    myaction=editEntryForm, 
    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@123bea8a,
    myaction=REDIRECT_TO_DEFAULT_PAGE, 
     ,      ---------------------------------> This empty string corrsponds to the default @RenderMapping
     myaction=selectFolderEntries, 
     myaction=searchResults, 
     myaction=addEntry, 
     myaction=addEntryForm, 
     myaction=showMyEntries, 
     myaction=showRecentEntries, 
     org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@4f1e9e2d, 
     myaction=editFolder, 
     myaction=editFolderForm,
     myaction=addFolder, 
     myaction=addFolderForm

After sorting,

With Spring 3.0.0,

    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@123bea8a, 
    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@4f1e9e2d, 
    myaction=editEntry, 
    myaction=editEntryForm, 
    myaction=REDIRECT_TO_DEFAULT_PAGE, 
    myaction=selectFolderEntries, 
    myaction=searchResults, 
    myaction=addEntry, 
    myaction=addEntryForm, 
    myaction=showMyEntries, 
    myaction=showRecentEntries, 
    myaction=editFolder, 
    myaction=editFolderForm, 
    myaction=addFolder, 
    myaction=addFolderForm,
                 ---------------> Default mapping i.e. @RenderMapping

With Spring 3.1.2 ( ignore things like [ ] ),

    [myaction=editEntry]
    [myaction=editEntryForm]
    deleteFolder
    [myaction=REDIRECT_TO_DEFAULT_PAGE]
    []        --------------------------> this is wrongly placed in middle.
    [myaction=selectFolderEntries]
    [myaction=searchResults]
    [myaction=addEntry]
    [myaction=addEntryForm]
    [myaction=showMyEntries]
    [myaction=showRecentEntries]
    deleteEntry
    [myaction=editFolder]
    [myaction=editFolderForm]
    [myaction=addFolder]
    [myaction=addFolderForm]
    null

This is a linked list. And each mapping is checked from the first node.
When ever the default [] i.e. an empty mapping is found in the middle of the list, true is being returned as if that is the right handler and rest of the handlers are not checked.

So is this a bug in Spring framework 3.2.1 ?



---

**Affects:** 3.1.1, 3.1.2, 3.2 M1

**Reference URL:** http://stackoverflow.com/questions/11996771/is-this-a-bug-in-spring-3-1-2-specifically-spring-portlet-mvcs

1 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14331","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14331/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14331/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14331/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14331","id":398152515,"node_id":"MDU6SXNzdWUzOTgxNTI1MTU=","number":14331,"title":"DefaultAnnotationHandlerMapping is not able to find correct handler [SPR-9697]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2012-08-16T16:19:53Z","updated_at":"2019-01-12T16:36:03Z","closed_at":"2018-12-25T10:56:47Z","author_association":"COLLABORATOR","body":"**[Arun Kumar Kandregula](https://jira.spring.io/secure/ViewProfile.jspa?name=arun7it)** opened **[SPR-9697](https://jira.spring.io/browse/SPR-9697?redirect=false)** and commented\n\nBackground:\nI am developing a Portlet using Spring MVC framework deployed in liferay 5.x server.\nCurrently I am using 3.0.0.RELEASE. Everything is working fine as expected.\nThat is, when I use annotations like\n\n`@RenderMapping`(params=\"myaction=editFolderForm\")\n`@RenderMapping`(params=\"myaction=editEntryForm\")\n`@RenderMapping`  \n`@ActionMapping`(params=\"myaction=editEntry\")\netc.\n\nDefaultAnnotationHandlerMapping is working as expected in finding a handler for every request.\n\nBut, for some valid reason, I have to use more latest version which is 3.1.2.RELEASE instead of 3.0.0.RELEASE.\n\nI observed that DefaultAnnotationHandlerMapping IS NOT WORKING as expected in finding a handler for every request. I figured out what is the problem by debugging the internals of Spring framework.\nI want to explain it clearly so that someone can tell me if this is a Bug.\n\nIn the parent class of DefaultAnnotationHandlerMapping which is AbstractMapBasedHandlerMapping  :\n\n    package org.springframework.web.portlet.handler;\n    public abstract class AbstractMapBasedHandlerMapping<K> extends AbstractHandlerMapping {\n    ....\n    .... \n    \n    /**\n     * Determines a handler for the computed lookup key for the given request.\n     * @see #getLookupKey\n     */\n    @Override\n    @SuppressWarnings(\"unchecked\")\n    protected Object getHandlerInternal(PortletRequest request) throws Exception {\n    \tK lookupKey = getLookupKey(request);\n    \tObject handler = this.handlerMap.get(lookupKey);\n    \tif (handler != null && logger.isDebugEnabled()) {\n    \t\tlogger.debug(\"Key [\" + lookupKey + \"] -> handler [\" + handler + \"]\");\n    \t}\n    \tif (handler instanceof Map) {\n    \t\tMap<PortletRequestMappingPredicate, Object> predicateMap =\n    \t\t\t\t(Map<PortletRequestMappingPredicate, Object>) handler;\n    \t\tList<PortletRequestMappingPredicate> predicates =\n    \t\t\t\tnew LinkedList<PortletRequestMappingPredicate>(predicateMap.keySet());\n    \n    \n    \tLINE 81:\tCollections.sort(predicates); ///////////////// PROBLEM\n    \n    \n    \n    \n    \t\tfor (PortletRequestMappingPredicate predicate : predicates) {\n    \t\t\tif (predicate.match(request)) {\n    \t\t\t\tpredicate.validate(request);\n    \t\t\t\treturn predicateMap.get(predicate);\n    \t\t\t}\n    \t\t}\n    \t\treturn null;\n    \t}\n    \treturn handler;\n    }\n    ....\n    ....\n    }\n\nThis sorting is screwed up in Spring 3.1.2 and is working perfectly well in Spring 3.0.0.\nIn next two sections I ll tell you why sorting matters and how it is screwed up in Spring 3.1.2.\n\n> **why sorting matters ?**\n\nThis HandlerMapping is searching a sorted linked list node by node till it finds  match for a particular handler. In my code base, I have multiple Controllers whose methods are mapped with following annotations like\n\n`@RenderMapping`  --- > default\n\n`@RenderMapping`(params=\"myaction=editEntryController\")\n\n`@RenderMapping`(params=\"myaction=editFolderController\")\n\netc.\n\nThe Collections.sort() depends on compareTo(..) method of each XXXPredicate class.\n\nwhen a request comes first it should be checked if myaction parameter is equal to \"editEntryController\", \"editFolderController\", ...and finally if nothing matches, thn only default controller which is annotated with `@RenderMapping` annotation should be matched.\n\nWith Spring 3.0.0, its working exactly like that as expected.\nWhere as wth Spring 3.2.1, its not behaving like that.\n\nWith both the versions, before sorting, the list is the same.\n\n    myaction=editEntry, \n    myaction=editEntryForm, \n    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@123bea8a,\n    myaction=REDIRECT_TO_DEFAULT_PAGE, \n     ,      ---------------------------------> This empty string corrsponds to the default @RenderMapping\n     myaction=selectFolderEntries, \n     myaction=searchResults, \n     myaction=addEntry, \n     myaction=addEntryForm, \n     myaction=showMyEntries, \n     myaction=showRecentEntries, \n     org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@4f1e9e2d, \n     myaction=editFolder, \n     myaction=editFolderForm,\n     myaction=addFolder, \n     myaction=addFolderForm\n\nAfter sorting,\n\nWith Spring 3.0.0,\n\n    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@123bea8a, \n    org.springframework.web.portlet.mvc.annotation.DefaultAnnotationHandlerMapping$ResourceMappingPredicate@4f1e9e2d, \n    myaction=editEntry, \n    myaction=editEntryForm, \n    myaction=REDIRECT_TO_DEFAULT_PAGE, \n    myaction=selectFolderEntries, \n    myaction=searchResults, \n    myaction=addEntry, \n    myaction=addEntryForm, \n    myaction=showMyEntries, \n    myaction=showRecentEntries, \n    myaction=editFolder, \n    myaction=editFolderForm, \n    myaction=addFolder, \n    myaction=addFolderForm,\n                 ---------------> Default mapping i.e. @RenderMapping\n\nWith Spring 3.1.2 ( ignore things like [ ] ),\n\n    [myaction=editEntry]\n    [myaction=editEntryForm]\n    deleteFolder\n    [myaction=REDIRECT_TO_DEFAULT_PAGE]\n    []        --------------------------> this is wrongly placed in middle.\n    [myaction=selectFolderEntries]\n    [myaction=searchResults]\n    [myaction=addEntry]\n    [myaction=addEntryForm]\n    [myaction=showMyEntries]\n    [myaction=showRecentEntries]\n    deleteEntry\n    [myaction=editFolder]\n    [myaction=editFolderForm]\n    [myaction=addFolder]\n    [myaction=addFolderForm]\n    null\n\nThis is a linked list. And each mapping is checked from the first node.\nWhen ever the default [] i.e. an empty mapping is found in the middle of the list, true is being returned as if that is the right handler and rest of the handlers are not checked.\n\nSo is this a bug in Spring framework 3.2.1 ?\n\n\n\n---\n\n**Affects:** 3.1.1, 3.1.2, 3.2 M1\n\n**Reference URL:** http://stackoverflow.com/questions/11996771/is-this-a-bug-in-spring-3-1-2-specifically-spring-portlet-mvcs\n\n1 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453396282","453396283"], "labels":["in: web","status: declined"]}