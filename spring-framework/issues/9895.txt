{"id":"9895", "title":"@Configureable doesn't work right [SPR-5221]", "body":"**[ljc yu](https://jira.spring.io/secure/ViewProfile.jspa?name=ljcyu)** opened **[SPR-5221](https://jira.spring.io/browse/SPR-5221?redirect=false)** and commented

when the main method is in the same class annoted by `@Configurable`, then run this class, `@Configural` doesn't work, there are not any properties being set.

---

Person.java

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.io.\\*;
import org.springframework.context.support.*;
`@Configurable`(\"person\")
public class Person{
private String name;
private int age;
public void setName(String name){
System.out.println(\"set name by spring\");
this.name=name;
}
public void setAge(int age){
System.out.println(\"set age by spring\");
this.age=age;
}
public String toString(){
return name+\"@\"+age;
}
public static void main(String[] args){
new ClassPathXmlApplicationContext(\"person.xml\");
System.out.println(\"new Person()\");
Person aPerson=new Person();
System.out.println(aPerson);
}
}

---

person.xml

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"   xmlns:context=\"http://www.springframework.org/schema/context\"  xsi:schemaLocation=\"http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://www.springframework.org/schema/context
http://www.springframework.org/schema/context/spring-context-2.5.xsd\">
<context:annotation-config />
<context:load-time-weaver />
\\<bean id=\"person\" class=\"Person\" abstract=\"true\">
\\<property name=\"age\" value=\"12\"/>
\\<property name=\"name\" value=\"spring\"/>
\\</bean>
\\</beans>

then run with spring-agent.jar, java Person, the result is error:
new Person()
null@0

but if the main method is in any other class, the result is right:
new Person()
spring@12

---

**Affects:** 2.5.5

**Issue Links:**
- #9811 `@Configurable` does not work when type is already loaded due to Java bytecode verification (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9895","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9895/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9895/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9895/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9895","id":398091403,"node_id":"MDU6SXNzdWUzOTgwOTE0MDM=","number":9895,"title":"@Configureable doesn't work right [SPR-5221]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2008-10-17T03:16:31Z","updated_at":"2019-01-13T08:02:22Z","closed_at":"2008-10-17T03:42:03Z","author_association":"COLLABORATOR","body":"**[ljc yu](https://jira.spring.io/secure/ViewProfile.jspa?name=ljcyu)** opened **[SPR-5221](https://jira.spring.io/browse/SPR-5221?redirect=false)** and commented\n\nwhen the main method is in the same class annoted by `@Configurable`, then run this class, `@Configural` doesn't work, there are not any properties being set.\n\n---\n\nPerson.java\n\nimport org.springframework.beans.factory.annotation.Configurable;\nimport org.springframework.core.io.\\*;\nimport org.springframework.context.support.*;\n`@Configurable`(\"person\")\npublic class Person{\nprivate String name;\nprivate int age;\npublic void setName(String name){\nSystem.out.println(\"set name by spring\");\nthis.name=name;\n}\npublic void setAge(int age){\nSystem.out.println(\"set age by spring\");\nthis.age=age;\n}\npublic String toString(){\nreturn name+\"@\"+age;\n}\npublic static void main(String[] args){\nnew ClassPathXmlApplicationContext(\"person.xml\");\nSystem.out.println(\"new Person()\");\nPerson aPerson=new Person();\nSystem.out.println(aPerson);\n}\n}\n\n---\n\nperson.xml\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"   xmlns:context=\"http://www.springframework.org/schema/context\"  xsi:schemaLocation=\"http://www.springframework.org/schema/beans\nhttp://www.springframework.org/schema/beans/spring-beans-2.0.xsd\nhttp://www.springframework.org/schema/context\nhttp://www.springframework.org/schema/context/spring-context-2.5.xsd\">\n<context:annotation-config />\n<context:load-time-weaver />\n\\<bean id=\"person\" class=\"Person\" abstract=\"true\">\n\\<property name=\"age\" value=\"12\"/>\n\\<property name=\"name\" value=\"spring\"/>\n\\</bean>\n\\</beans>\n\nthen run with spring-agent.jar, java Person, the result is error:\nnew Person()\nnull@0\n\nbut if the main method is in any other class, the result is right:\nnew Person()\nspring@12\n\n---\n\n**Affects:** 2.5.5\n\n**Issue Links:**\n- #9811 `@Configurable` does not work when type is already loaded due to Java bytecode verification (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453334507"], "labels":["in: core","status: duplicate"]}