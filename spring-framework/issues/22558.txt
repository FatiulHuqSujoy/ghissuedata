{"id":"22558", "title":"Wrong behavior in extended configuration class", "body":"I’ve tried to create a minimal example that demonstrates an application context behavior that surprises me.

Given are the following configuration classes:
```java
class FirstServiceConfig
{
    @Value(\"${firstServiceUrl:url1}\")
    private String firstServiceUrl;

    String getServiceUrl() {
        return firstServiceUrl;
    }

    @Bean
    Service firstService() {
        return new Service(getServiceUrl());
    }
}
```
```java
class SecondServiceConfig extends FirstServiceConfig
{
    @Value(\"${secondServiceUrl:url2}\")
    private String secondServiceUrl;

    @Override
    String getServiceUrl() {
        return secondServiceUrl;
    }

    @Bean
    Service secondService() {
        return new Service(getServiceUrl());
    }
}
```
```java
class Service
{
    private final String url;

    Service(String url) {
        this.url = url;
    }

    String getUrl() {
        return url;
    }
}
```

The following example creates an application context and obtains both service beans:
```java
AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
context.register(FirstServiceConfig.class, SecondServiceConfig.class);
context.refresh();
Service firstService = context.getBean(\"firstService\", Service.class);
Service secondService = context.getBean(\"secondService\", Service.class);
System.out.println(\"firstService.url = \" + firstService.getUrl());
System.out.println(\"secondService.url = \" + secondService.getUrl());
```

I would expect the output
```
firstService.url = url1
firstService.url = url2
```

However, the actual output is
```
firstService.url = url2
firstService.url = url2
```

I can observe that behavior in various Spring versions: `4.3.5`, `5.0.0`, `5.1.5`

Interestingly, the behavior changes if the order of registration is changed:
```diff
- context.register(FirstServiceConfig.class, SecondServiceConfig.class);
+ context.register(SecondServiceConfig.class, FirstServiceConfig.class);
```

Is this behavior a bug or a feature? :slightly_smiling_face:", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22558","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22558/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22558/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22558/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22558","id":419405360,"node_id":"MDU6SXNzdWU0MTk0MDUzNjA=","number":22558,"title":"Wrong behavior in extended configuration class","user":{"login":"bwaldvogel","id":29931,"node_id":"MDQ6VXNlcjI5OTMx","avatar_url":"https://avatars2.githubusercontent.com/u/29931?v=4","gravatar_id":"","url":"https://api.github.com/users/bwaldvogel","html_url":"https://github.com/bwaldvogel","followers_url":"https://api.github.com/users/bwaldvogel/followers","following_url":"https://api.github.com/users/bwaldvogel/following{/other_user}","gists_url":"https://api.github.com/users/bwaldvogel/gists{/gist_id}","starred_url":"https://api.github.com/users/bwaldvogel/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/bwaldvogel/subscriptions","organizations_url":"https://api.github.com/users/bwaldvogel/orgs","repos_url":"https://api.github.com/users/bwaldvogel/repos","events_url":"https://api.github.com/users/bwaldvogel/events{/privacy}","received_events_url":"https://api.github.com/users/bwaldvogel/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2019-03-11T10:49:54Z","updated_at":"2019-03-11T20:11:43Z","closed_at":"2019-03-11T11:08:03Z","author_association":"NONE","body":"I’ve tried to create a minimal example that demonstrates an application context behavior that surprises me.\r\n\r\nGiven are the following configuration classes:\r\n```java\r\nclass FirstServiceConfig\r\n{\r\n    @Value(\"${firstServiceUrl:url1}\")\r\n    private String firstServiceUrl;\r\n\r\n    String getServiceUrl() {\r\n        return firstServiceUrl;\r\n    }\r\n\r\n    @Bean\r\n    Service firstService() {\r\n        return new Service(getServiceUrl());\r\n    }\r\n}\r\n```\r\n```java\r\nclass SecondServiceConfig extends FirstServiceConfig\r\n{\r\n    @Value(\"${secondServiceUrl:url2}\")\r\n    private String secondServiceUrl;\r\n\r\n    @Override\r\n    String getServiceUrl() {\r\n        return secondServiceUrl;\r\n    }\r\n\r\n    @Bean\r\n    Service secondService() {\r\n        return new Service(getServiceUrl());\r\n    }\r\n}\r\n```\r\n```java\r\nclass Service\r\n{\r\n    private final String url;\r\n\r\n    Service(String url) {\r\n        this.url = url;\r\n    }\r\n\r\n    String getUrl() {\r\n        return url;\r\n    }\r\n}\r\n```\r\n\r\nThe following example creates an application context and obtains both service beans:\r\n```java\r\nAnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();\r\ncontext.register(FirstServiceConfig.class, SecondServiceConfig.class);\r\ncontext.refresh();\r\nService firstService = context.getBean(\"firstService\", Service.class);\r\nService secondService = context.getBean(\"secondService\", Service.class);\r\nSystem.out.println(\"firstService.url = \" + firstService.getUrl());\r\nSystem.out.println(\"secondService.url = \" + secondService.getUrl());\r\n```\r\n\r\nI would expect the output\r\n```\r\nfirstService.url = url1\r\nfirstService.url = url2\r\n```\r\n\r\nHowever, the actual output is\r\n```\r\nfirstService.url = url2\r\nfirstService.url = url2\r\n```\r\n\r\nI can observe that behavior in various Spring versions: `4.3.5`, `5.0.0`, `5.1.5`\r\n\r\nInterestingly, the behavior changes if the order of registration is changed:\r\n```diff\r\n- context.register(FirstServiceConfig.class, SecondServiceConfig.class);\r\n+ context.register(SecondServiceConfig.class, FirstServiceConfig.class);\r\n```\r\n\r\nIs this behavior a bug or a feature? :slightly_smiling_face:","closed_by":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}}", "commentIds":["471497109","471503204","471707128"], "labels":["status: invalid"]}