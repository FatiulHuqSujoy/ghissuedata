{"id":"5117", "title":"Provide next to successView a successRedirect and successForward [SPR-387]", "body":"**[Stefaan Destoop](https://jira.spring.io/secure/ViewProfile.jspa?name=sdestoop)** opened **[SPR-387](https://jira.spring.io/browse/SPR-387?redirect=false)** and commented

Currently, you can only specify a succes view in the SimpleFormController.  However, there are many occasions were you want to redirect or forward to another page when the form was processed successful.

With this solution, you need to specify in your Spring configuration  file one of the three possible properties;
successView: view when the form has been processed successfully (such as  the case now)
successForward: forward to the internal URL when the form has been  processed successfully.
successRedirect: redirect to the URL when the form has been processed  successfully.

Patch for SimpleFormController:
/**
* A controller to forward to on success
  \\*/
  private String successForward;
  /**

* A valid url to redirect to on success
  */
  private String successRedirect;

  /**

  * Returns the successForward controller name.
  * `@return` Returns the successForward controller name.
    \\*/
    public String getSuccessForward() {
    return successForward;
    }
    /**
  * Sets the successForward controller name.
  * `@param` successForward The successForward controller name to set.
    */
    public void setSuccessForward(String successForward) {
    if (getSuccessView() != null) {
    throw new ApplicationContextException(\"Cannot set

successForward when successView already set\");
}
if (getSuccessRedirect() != null) {
throw new ApplicationContextException(\"Cannot set
successForward when successRedirect already set\");
}
this.successForward = successForward;
}
/**
* Returns the successRedirect URL.
* `@return` Returns the successRedirect URL.
  \\*/
  public String getSuccessRedirect() {
  return successRedirect;
  }
  /**
* Sets the successRedirect URL.
* `@param` successRedirect The successRedirect URL to set.
  */
  public void setSuccessRedirect(String successRedirect) {
  if (getSuccessView() != null) {
  throw new ApplicationContextException(\"Cannot set
  successRedirect when successView already set\");
  }
  if (getSuccessForward() != null) {
  throw new ApplicationContextException(\"Cannot set
  successRedirect when successForward already set\");
  }
  this.successRedirect = successRedirect;
  }

// Replace the setSuccessView:
/**
* Sets the successView
* `@param` successView The successView to set.
  */
  public void setSuccessView(String successView) {
  if (getSuccessForward() != null) {
  throw new ApplicationContextException(\"Cannot set
  successView when successForward already set\");
  }
  if (getSuccessRedirect() != null) {
  throw new ApplicationContextException(\"Cannot set
  successView when successRedirect already set\");
  }
  super.setSuccessView(successView);
  }
  */

// replace the onSubmit:
/**
* Simpler onSubmit version. Called by the default implementation of
  the onSubmit
* version with all parameters.
* 

\\<p>Default implementation calls onSubmit(command), using the
returned ModelAndView
* if actually implemented in a subclass. Else, the default behavior
  is applied:
* rendering the success view if one is set with the command and
  Errors instance as model OR
* rendering the success forward if one is set with the command and
  Errors instance as model OR
* rendering the success redirect if one is set with the command and
  Errors instance as model.
* 

\\<p>Subclasses can override this to provide custom submission
handling that
* does not need request and response.
* 

\\<p>Call \\<code>errors.getModel()\\</code> to populate the
ModelAndView model
* with the command and the Errors instance, under the specified
  command name,
* as expected by the \"spring:bind\" tag.
* `@param` command form object with request parameters bound onto it
* `@param` errors Errors instance without errors
* `@return` the prepared model and view, or null
* `@throws` Exception in case of errors
* `@see` #onSubmit(HttpServletRequest, HttpServletResponse, Object,
  BindException)
* `@see` #onSubmit(Object)
* `@see` #setSuccessView
* `@see` #setSuccessForward
* `@see` #setSuccessRedirect
* `@see` org.springframework.validation.Errors
* `@see` org.springframework.validation.BindException#getModel
  */
  protected ModelAndView onSubmit(Object command, BindException
  errors) throws Exception {
  ModelAndView mv = onSubmit(command);
  if (mv != null) {
  // simplest onSubmit version implemented in custom subclass
  return mv;
  }
  else {
  // default behavior: render success view
  if (getSuccessView() != null) {
  return new ModelAndView(getSuccessView(),
  errors.getModel());
  }
  else if (getSuccessForward() != null) {
  return new ModelAndView(new
  InternalResourceView(getSuccessForward()),
  errors.getModel());
  }
  else if (getSuccessRedirect() != null) {
  return new ModelAndView(new
  RedirectView(getSuccessRedirect()), errors.getModel());
  }
  else {
  throw new ServletException(\"successView, successForward
  or successRedirect isn't set\");
  }
  }
  }



---

**Affects:** 1.1.1

2 votes, 1 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5117","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5117/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5117/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5117/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5117","id":398051984,"node_id":"MDU6SXNzdWUzOTgwNTE5ODQ=","number":5117,"title":"Provide next to successView a successRedirect and successForward [SPR-387]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/13","html_url":"https://github.com/spring-projects/spring-framework/milestone/13","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/13/labels","id":3960783,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Mw==","number":13,"title":"1.1.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":49,"state":"closed","created_at":"2019-01-10T22:02:06Z","updated_at":"2019-01-10T23:03:08Z","due_on":null,"closed_at":"2019-01-10T22:02:06Z"},"comments":11,"created_at":"2004-10-10T20:57:00Z","updated_at":"2019-01-11T14:01:44Z","closed_at":"2004-11-20T22:12:08Z","author_association":"COLLABORATOR","body":"**[Stefaan Destoop](https://jira.spring.io/secure/ViewProfile.jspa?name=sdestoop)** opened **[SPR-387](https://jira.spring.io/browse/SPR-387?redirect=false)** and commented\n\nCurrently, you can only specify a succes view in the SimpleFormController.  However, there are many occasions were you want to redirect or forward to another page when the form was processed successful.\n\nWith this solution, you need to specify in your Spring configuration  file one of the three possible properties;\nsuccessView: view when the form has been processed successfully (such as  the case now)\nsuccessForward: forward to the internal URL when the form has been  processed successfully.\nsuccessRedirect: redirect to the URL when the form has been processed  successfully.\n\nPatch for SimpleFormController:\n/**\n* A controller to forward to on success\n  \\*/\n  private String successForward;\n  /**\n\n* A valid url to redirect to on success\n  */\n  private String successRedirect;\n\n  /**\n\n  * Returns the successForward controller name.\n  * `@return` Returns the successForward controller name.\n    \\*/\n    public String getSuccessForward() {\n    return successForward;\n    }\n    /**\n  * Sets the successForward controller name.\n  * `@param` successForward The successForward controller name to set.\n    */\n    public void setSuccessForward(String successForward) {\n    if (getSuccessView() != null) {\n    throw new ApplicationContextException(\"Cannot set\n\nsuccessForward when successView already set\");\n}\nif (getSuccessRedirect() != null) {\nthrow new ApplicationContextException(\"Cannot set\nsuccessForward when successRedirect already set\");\n}\nthis.successForward = successForward;\n}\n/**\n* Returns the successRedirect URL.\n* `@return` Returns the successRedirect URL.\n  \\*/\n  public String getSuccessRedirect() {\n  return successRedirect;\n  }\n  /**\n* Sets the successRedirect URL.\n* `@param` successRedirect The successRedirect URL to set.\n  */\n  public void setSuccessRedirect(String successRedirect) {\n  if (getSuccessView() != null) {\n  throw new ApplicationContextException(\"Cannot set\n  successRedirect when successView already set\");\n  }\n  if (getSuccessForward() != null) {\n  throw new ApplicationContextException(\"Cannot set\n  successRedirect when successForward already set\");\n  }\n  this.successRedirect = successRedirect;\n  }\n\n// Replace the setSuccessView:\n/**\n* Sets the successView\n* `@param` successView The successView to set.\n  */\n  public void setSuccessView(String successView) {\n  if (getSuccessForward() != null) {\n  throw new ApplicationContextException(\"Cannot set\n  successView when successForward already set\");\n  }\n  if (getSuccessRedirect() != null) {\n  throw new ApplicationContextException(\"Cannot set\n  successView when successRedirect already set\");\n  }\n  super.setSuccessView(successView);\n  }\n  */\n\n// replace the onSubmit:\n/**\n* Simpler onSubmit version. Called by the default implementation of\n  the onSubmit\n* version with all parameters.\n* \n\n\\<p>Default implementation calls onSubmit(command), using the\nreturned ModelAndView\n* if actually implemented in a subclass. Else, the default behavior\n  is applied:\n* rendering the success view if one is set with the command and\n  Errors instance as model OR\n* rendering the success forward if one is set with the command and\n  Errors instance as model OR\n* rendering the success redirect if one is set with the command and\n  Errors instance as model.\n* \n\n\\<p>Subclasses can override this to provide custom submission\nhandling that\n* does not need request and response.\n* \n\n\\<p>Call \\<code>errors.getModel()\\</code> to populate the\nModelAndView model\n* with the command and the Errors instance, under the specified\n  command name,\n* as expected by the \"spring:bind\" tag.\n* `@param` command form object with request parameters bound onto it\n* `@param` errors Errors instance without errors\n* `@return` the prepared model and view, or null\n* `@throws` Exception in case of errors\n* `@see` #onSubmit(HttpServletRequest, HttpServletResponse, Object,\n  BindException)\n* `@see` #onSubmit(Object)\n* `@see` #setSuccessView\n* `@see` #setSuccessForward\n* `@see` #setSuccessRedirect\n* `@see` org.springframework.validation.Errors\n* `@see` org.springframework.validation.BindException#getModel\n  */\n  protected ModelAndView onSubmit(Object command, BindException\n  errors) throws Exception {\n  ModelAndView mv = onSubmit(command);\n  if (mv != null) {\n  // simplest onSubmit version implemented in custom subclass\n  return mv;\n  }\n  else {\n  // default behavior: render success view\n  if (getSuccessView() != null) {\n  return new ModelAndView(getSuccessView(),\n  errors.getModel());\n  }\n  else if (getSuccessForward() != null) {\n  return new ModelAndView(new\n  InternalResourceView(getSuccessForward()),\n  errors.getModel());\n  }\n  else if (getSuccessRedirect() != null) {\n  return new ModelAndView(new\n  RedirectView(getSuccessRedirect()), errors.getModel());\n  }\n  else {\n  throw new ServletException(\"successView, successForward\n  or successRedirect isn't set\");\n  }\n  }\n  }\n\n\n\n---\n\n**Affects:** 1.1.1\n\n2 votes, 1 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453288540","453288541","453288542","453288543","453288546","453288547","453288549","453288551","453288552","453288555","453288559"], "labels":["in: web","type: enhancement"]}