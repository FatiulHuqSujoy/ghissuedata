{"id":"23908", "title":"Incorrect Javadoc for WebMvcConfigurer#addInterceptors", "body":"**Affects:** \\<5.1.x, 5.0.x>

---
In spring 5.1.x also in 5.0.x, resourceHandlerMapping picks up interceptors configured for controllers only.

[WebMvcConfigurationSupport.java](https://github.com/spring-projects/spring-framework/blob/5.1.x/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java#L499)

```java
@Bean
@Nullable
public HandlerMapping resourceHandlerMapping() {
	Assert.state(this.applicationContext != null, \"No ApplicationContext set\");
	Assert.state(this.servletContext != null, \"No ServletContext set\");

	ResourceHandlerRegistry registry = new ResourceHandlerRegistry(this.applicationContext,
			this.servletContext, mvcContentNegotiationManager(), mvcUrlPathHelper());
	addResourceHandlers(registry);

	AbstractHandlerMapping handlerMapping = registry.getHandlerMapping();
	if (handlerMapping == null) {
		return null;
	}
	handlerMapping.setPathMatcher(mvcPathMatcher());
	handlerMapping.setUrlPathHelper(mvcUrlPathHelper());
	handlerMapping.setInterceptors(getInterceptors()); // ==> here it adds all configured interceptors even those which are for controllers only.
	handlerMapping.setCorsConfigurations(getCorsConfigurations());
	return handlerMapping;
}
```

As per documentation of [WebMvcConfigurer](https://github.com/spring-projects/spring-framework/blob/5.1.x/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurer.java#L103)
```
	/**
	 * Add Spring MVC lifecycle interceptors for pre- and post-processing of
	 * controller method invocations. Interceptors can be registered to apply
	 * to all requests or be limited to a subset of URL patterns.
	 * <p><strong>Note</strong> that interceptors registered here only apply to
	 * controllers and not to resource handler requests. To intercept requests for
	 * static resources either declare a
	 * {@link org.springframework.web.servlet.handler.MappedInterceptor MappedInterceptor}
	 * bean or switch to advanced configuration mode by extending
	 * {@link org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
	 * WebMvcConfigurationSupport} and then override {@code resourceHandlerMapping}.
	 */
	default void addInterceptors(InterceptorRegistry registry) {
	}
```

So any interceptor added using **addInterceptors** in WebMvcConfigurerAdapter or WebMvcConfigurer should not apply to resource handlerMapping which is not happening anymore.


", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23908","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23908/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23908/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/23908/events","html_url":"https://github.com/spring-projects/spring-framework/issues/23908","id":516146696,"node_id":"MDU6SXNzdWU1MTYxNDY2OTY=","number":23908,"title":"Incorrect Javadoc for WebMvcConfigurer#addInterceptors","user":{"login":"samarthrastogi","id":830682,"node_id":"MDQ6VXNlcjgzMDY4Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/830682?v=4","gravatar_id":"","url":"https://api.github.com/users/samarthrastogi","html_url":"https://github.com/samarthrastogi","followers_url":"https://api.github.com/users/samarthrastogi/followers","following_url":"https://api.github.com/users/samarthrastogi/following{/other_user}","gists_url":"https://api.github.com/users/samarthrastogi/gists{/gist_id}","starred_url":"https://api.github.com/users/samarthrastogi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/samarthrastogi/subscriptions","organizations_url":"https://api.github.com/users/samarthrastogi/orgs","repos_url":"https://api.github.com/users/samarthrastogi/repos","events_url":"https://api.github.com/users/samarthrastogi/events{/privacy}","received_events_url":"https://api.github.com/users/samarthrastogi/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1189497922,"node_id":"MDU6TGFiZWwxMTg5NDk3OTIy","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20superseded","name":"status: superseded","color":"fef2c0","default":false},{"id":1188511933,"node_id":"MDU6TGFiZWwxMTg4NTExOTMz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20task","name":"type: task","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-11-01T14:28:15Z","updated_at":"2019-11-06T21:59:14Z","closed_at":"2019-11-04T12:40:49Z","author_association":"NONE","body":"**Affects:** \\<5.1.x, 5.0.x>\r\n\r\n---\r\nIn spring 5.1.x also in 5.0.x, resourceHandlerMapping picks up interceptors configured for controllers only.\r\n\r\n[WebMvcConfigurationSupport.java](https://github.com/spring-projects/spring-framework/blob/5.1.x/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java#L499)\r\n\r\n```java\r\n@Bean\r\n@Nullable\r\npublic HandlerMapping resourceHandlerMapping() {\r\n\tAssert.state(this.applicationContext != null, \"No ApplicationContext set\");\r\n\tAssert.state(this.servletContext != null, \"No ServletContext set\");\r\n\r\n\tResourceHandlerRegistry registry = new ResourceHandlerRegistry(this.applicationContext,\r\n\t\t\tthis.servletContext, mvcContentNegotiationManager(), mvcUrlPathHelper());\r\n\taddResourceHandlers(registry);\r\n\r\n\tAbstractHandlerMapping handlerMapping = registry.getHandlerMapping();\r\n\tif (handlerMapping == null) {\r\n\t\treturn null;\r\n\t}\r\n\thandlerMapping.setPathMatcher(mvcPathMatcher());\r\n\thandlerMapping.setUrlPathHelper(mvcUrlPathHelper());\r\n\thandlerMapping.setInterceptors(getInterceptors()); // ==> here it adds all configured interceptors even those which are for controllers only.\r\n\thandlerMapping.setCorsConfigurations(getCorsConfigurations());\r\n\treturn handlerMapping;\r\n}\r\n```\r\n\r\nAs per documentation of [WebMvcConfigurer](https://github.com/spring-projects/spring-framework/blob/5.1.x/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurer.java#L103)\r\n```\r\n\t/**\r\n\t * Add Spring MVC lifecycle interceptors for pre- and post-processing of\r\n\t * controller method invocations. Interceptors can be registered to apply\r\n\t * to all requests or be limited to a subset of URL patterns.\r\n\t * <p><strong>Note</strong> that interceptors registered here only apply to\r\n\t * controllers and not to resource handler requests. To intercept requests for\r\n\t * static resources either declare a\r\n\t * {@link org.springframework.web.servlet.handler.MappedInterceptor MappedInterceptor}\r\n\t * bean or switch to advanced configuration mode by extending\r\n\t * {@link org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport\r\n\t * WebMvcConfigurationSupport} and then override {@code resourceHandlerMapping}.\r\n\t */\r\n\tdefault void addInterceptors(InterceptorRegistry registry) {\r\n\t}\r\n```\r\n\r\nSo any interceptor added using **addInterceptors** in WebMvcConfigurerAdapter or WebMvcConfigurer should not apply to resource handlerMapping which is not happening anymore.\r\n\r\n\r\n","closed_by":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}}", "commentIds":["549126699"], "labels":["in: web","status: backported","status: superseded","type: task"]}