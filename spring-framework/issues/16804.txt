{"id":"16804", "title":"Bean's type is changed unexpectedly when Spring AOP is used [SPR-12190]", "body":"**[Jeff Fang](https://jira.spring.io/secure/ViewProfile.jspa?name=fangji.jeff)** opened **[SPR-12190](https://jira.spring.io/browse/SPR-12190?redirect=false)** and commented

When a bean whose class type implements an interface is intercepted by a Spring AOP, the bean is not the original bean class type, but is the JDK proxy class which implements the bean's interface.

For example, we define an ICalculatorService interface, and an IntensiveCalculatorService class implementing the ICalculatorService interface. And we define an intensiveCalculatorService bean with the IntensiveCalculatorService class in the spring configuraiton file, and intercept the IntensiveCalculatorService.calculator method with spring AOP.

The unexpected thing occurs. We cannot cast the \"intensiveCalculatorService\" bean to an IntensiveCalculatorService class object, though the bean defined as IntensiveCalculatorService class, because it actually is not an object of IntensiveCalculatorService class, but is JDK proxy object which implements ICalculatorService interface.

Let's imagine the scenario, a developer A only defined the intensiveCalculatorService bean, and cast the bean to a IntensiveCalculatorService type . It's definitely correct because there is no AOP defined. But the developer A's coworker B want to intercept the IntensiveCalculatorService.calcualtor to do something before the caculator. He defined the AOP aspect in the spring configuration file. The original code of using IntensiveCalculatorService type bean throwed ClassCastException.

The severity thing is that developer A and B have no sense of the exception happening.

**Though we could use proxy-target-class=\"true\" to force the use of CGLib proxy to solve the problem, I think it needs improvement in spring AOP. Bean's type is changed unexpectedly when Spring AOP is used without proxy-target-class property set. Maybe there are some conerns to try the JDK original proxy mechanism first, and avoid depending the thirdparty lib. At least I think the issue needs enhancement to warn users the unexpected behavior of bean's type being changed.**

The code is below:

    package com.performance.service;
    public interface ICalculatorService {
       public void calculator(int count);
    }
    
    package com.performance.service;
    public class IntensiveCalculatorService implements ICalculatorService
    {
       public String type = \"intensive\";
      
       public void testObjectSelfMethod(){
            System. out.println(\"This is IntensiveCalculatorService Self Method\");
       }
    
       public void calculator(int count){
            System. out.println(\"intensive calculator\");
        }
    }

The Spring configuration file is below:

    <aop:config >  
       <aop:aspect id= \"TestAspect\" ref =\"aspectBean\">   
            <aop:pointcut id= \"ics\" 
                expression=\"execution(* com.performance.service.IntensiveCalculatorService.calculator(..))\" />   
            <aop:around pointcut-ref=\"ics\" method=\"aroundMethod\"/>    
        </aop:aspect>   
    </aop:config >  
    
    <bean id=\"aspectBean\" class=\"com.performance.aspect.TestAspect\" />
    
    <bean id= \"intensiveCalculatorService\" class=\"com.performance.service.IntensiveCalculatorService\" />

The following code will throw ClassCastException in the runtime.

    IntensiveCalculatorService intensiveCalculatorService = (IntensiveCalculatorService)context.getBean(\"intensiveCalculatorService\" );

Exception in thread \"main\" java.lang.ClassCastException: $Proxy0 cannot be cast to com.performance.service.IntensiveCalculatorService
at com.performance.main.ApplicationMain.main(ApplicationMain.java:20)

---

**Issue Links:**
- #8346 Add support for mixed, fine-grained JDK- and CGLIB-based proxying

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16804","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16804/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16804/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16804/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16804","id":398171722,"node_id":"MDU6SXNzdWUzOTgxNzE3MjI=","number":16804,"title":"Bean's type is changed unexpectedly when Spring AOP is used [SPR-12190]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2014-09-14T08:33:44Z","updated_at":"2019-01-12T00:19:08Z","closed_at":"2019-01-12T00:19:08Z","author_association":"COLLABORATOR","body":"**[Jeff Fang](https://jira.spring.io/secure/ViewProfile.jspa?name=fangji.jeff)** opened **[SPR-12190](https://jira.spring.io/browse/SPR-12190?redirect=false)** and commented\n\nWhen a bean whose class type implements an interface is intercepted by a Spring AOP, the bean is not the original bean class type, but is the JDK proxy class which implements the bean's interface.\n\nFor example, we define an ICalculatorService interface, and an IntensiveCalculatorService class implementing the ICalculatorService interface. And we define an intensiveCalculatorService bean with the IntensiveCalculatorService class in the spring configuraiton file, and intercept the IntensiveCalculatorService.calculator method with spring AOP.\n\nThe unexpected thing occurs. We cannot cast the \"intensiveCalculatorService\" bean to an IntensiveCalculatorService class object, though the bean defined as IntensiveCalculatorService class, because it actually is not an object of IntensiveCalculatorService class, but is JDK proxy object which implements ICalculatorService interface.\n\nLet's imagine the scenario, a developer A only defined the intensiveCalculatorService bean, and cast the bean to a IntensiveCalculatorService type . It's definitely correct because there is no AOP defined. But the developer A's coworker B want to intercept the IntensiveCalculatorService.calcualtor to do something before the caculator. He defined the AOP aspect in the spring configuration file. The original code of using IntensiveCalculatorService type bean throwed ClassCastException.\n\nThe severity thing is that developer A and B have no sense of the exception happening.\n\n**Though we could use proxy-target-class=\"true\" to force the use of CGLib proxy to solve the problem, I think it needs improvement in spring AOP. Bean's type is changed unexpectedly when Spring AOP is used without proxy-target-class property set. Maybe there are some conerns to try the JDK original proxy mechanism first, and avoid depending the thirdparty lib. At least I think the issue needs enhancement to warn users the unexpected behavior of bean's type being changed.**\n\nThe code is below:\n\n    package com.performance.service;\n    public interface ICalculatorService {\n       public void calculator(int count);\n    }\n    \n    package com.performance.service;\n    public class IntensiveCalculatorService implements ICalculatorService\n    {\n       public String type = \"intensive\";\n      \n       public void testObjectSelfMethod(){\n            System. out.println(\"This is IntensiveCalculatorService Self Method\");\n       }\n    \n       public void calculator(int count){\n            System. out.println(\"intensive calculator\");\n        }\n    }\n\nThe Spring configuration file is below:\n\n    <aop:config >  \n       <aop:aspect id= \"TestAspect\" ref =\"aspectBean\">   \n            <aop:pointcut id= \"ics\" \n                expression=\"execution(* com.performance.service.IntensiveCalculatorService.calculator(..))\" />   \n            <aop:around pointcut-ref=\"ics\" method=\"aroundMethod\"/>    \n        </aop:aspect>   \n    </aop:config >  \n    \n    <bean id=\"aspectBean\" class=\"com.performance.aspect.TestAspect\" />\n    \n    <bean id= \"intensiveCalculatorService\" class=\"com.performance.service.IntensiveCalculatorService\" />\n\nThe following code will throw ClassCastException in the runtime.\n\n    IntensiveCalculatorService intensiveCalculatorService = (IntensiveCalculatorService)context.getBean(\"intensiveCalculatorService\" );\n\nException in thread \"main\" java.lang.ClassCastException: $Proxy0 cannot be cast to com.performance.service.IntensiveCalculatorService\nat com.performance.main.ApplicationMain.main(ApplicationMain.java:20)\n\n---\n\n**Issue Links:**\n- #8346 Add support for mixed, fine-grained JDK- and CGLIB-based proxying\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453699022"], "labels":["in: core","status: bulk-closed"]}