{"id":"12569", "title":"JavaMailSenderImpl misses a synchronized block for sending mails [SPR-7914]", "body":"**[Michael Wyraz](https://jira.spring.io/secure/ViewProfile.jspa?name=micw)** opened **[SPR-7914](https://jira.spring.io/browse/SPR-7914?redirect=false)** and commented

In JavaMailSenderImpl.doSend everything after
Transport transport = getTransport(getSession());
until
finally {
transport.close();
}
has to be in a synchronized(transport) block.

The reason is that JavaMail (at least the sun implementation) requires this. Otherwise connecting fails if assertions are enabled:
java.lang.AssertionError
at com.sun.mail.smtp.SMTPTransport.readServerResponse(SMTPTransport.java:1578)
at com.sun.mail.smtp.SMTPTransport.openServer(SMTPTransport.java:1369)
at com.sun.mail.smtp.SMTPTransport.protocolConnect(SMTPTransport.java:412)
at javax.mail.Service.connect(Service.java:248)
[...]

The failing code in the implementation is:
protected int readServerResponse() throws MessagingException {
assert Thread.holdsLock(this);

Probably ignoring this will result in strange race conditions. At least it avoids sending of mail with assertions enabled.


---

**Affects:** 2.5.6

1 votes, 4 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12569","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12569/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12569/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12569/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12569","id":398109866,"node_id":"MDU6SXNzdWUzOTgxMDk4NjY=","number":12569,"title":"JavaMailSenderImpl misses a synchronized block for sending mails [SPR-7914]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2011-01-27T06:53:07Z","updated_at":"2019-01-12T03:31:31Z","closed_at":"2019-01-12T03:31:31Z","author_association":"COLLABORATOR","body":"**[Michael Wyraz](https://jira.spring.io/secure/ViewProfile.jspa?name=micw)** opened **[SPR-7914](https://jira.spring.io/browse/SPR-7914?redirect=false)** and commented\n\nIn JavaMailSenderImpl.doSend everything after\nTransport transport = getTransport(getSession());\nuntil\nfinally {\ntransport.close();\n}\nhas to be in a synchronized(transport) block.\n\nThe reason is that JavaMail (at least the sun implementation) requires this. Otherwise connecting fails if assertions are enabled:\njava.lang.AssertionError\nat com.sun.mail.smtp.SMTPTransport.readServerResponse(SMTPTransport.java:1578)\nat com.sun.mail.smtp.SMTPTransport.openServer(SMTPTransport.java:1369)\nat com.sun.mail.smtp.SMTPTransport.protocolConnect(SMTPTransport.java:412)\nat javax.mail.Service.connect(Service.java:248)\n[...]\n\nThe failing code in the implementation is:\nprotected int readServerResponse() throws MessagingException {\nassert Thread.holdsLock(this);\n\nProbably ignoring this will result in strange race conditions. At least it avoids sending of mail with assertions enabled.\n\n\n---\n\n**Affects:** 2.5.6\n\n1 votes, 4 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453356687","453356688","453715789"], "labels":["in: core","status: bulk-closed"]}