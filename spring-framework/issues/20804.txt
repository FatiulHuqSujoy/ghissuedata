{"id":"20804", "title":"@CreationTimestamp and @UpdateTimestamp don't work in Spring Kotlin [SPR-16257]", "body":"**[cdxf](https://jira.spring.io/secure/ViewProfile.jspa?name=snoobvn)** opened **[SPR-16257](https://jira.spring.io/browse/SPR-16257?redirect=false)** and commented

I tried with Java and it still works, but not with Kotlin. both use Spring Boot 2.0

Kotlin (even with `@Temporal` it doesn't work too):

```java

@Entity
class Post(
        @get:NotBlank
        var name: String = \"\",
        val content: String = \"\"
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val id: Int? = null
    @ManyToMany
    @Cascade(CascadeType.ALL)
    val tags: MutableSet<Tag> = mutableSetOf()

    @CreationTimestamp
    lateinit var createDate: Date


    @UpdateTimestamp
    lateinit var updateDate: Date

    fun addTag(tag: Tag) {
        this.tags.add(tag)
        tag.posts.add(this)
    }
}
```

The result:
!kotlin.png|thumbnail!

Java:

```java

@Entity
public class Post {

    public Post(@NotBlank String name, String content) {
        this.name = name;
        this.content = content;
    }

    public void addTag(Tag tag) {
        this.tags.add(tag);
        tag.posts.add(this);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Integer id;
    @NotBlank
    String name;
    String content;
    @ManyToMany
    @JoinColumn(name=\"tags\")
    @Cascade(CascadeType.ALL)
    public Set<Tag> tags = new HashSet();

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = \"created\")
    Date createDate;
    @Column(name = \"updated\")
    @UpdateTimestamp
    Date updateDate;
}
```

The result:

!java.png|thumbnail!

Below is my sample project:
[^project.zip]


---

**Affects:** 5.0.2

**Attachments:**
- [java.png](https://jira.spring.io/secure/attachment/25312/java.png) (_3.71 kB_)
- [kotlin.png](https://jira.spring.io/secure/attachment/25311/kotlin.png) (_3.44 kB_)
- [project.zip](https://jira.spring.io/secure/attachment/25313/project.zip) (_56.62 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20804","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20804/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20804/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/20804/events","html_url":"https://github.com/spring-projects/spring-framework/issues/20804","id":398219610,"node_id":"MDU6SXNzdWUzOTgyMTk2MTA=","number":20804,"title":"@CreationTimestamp and @UpdateTimestamp don't work in Spring Kotlin [SPR-16257]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2017-12-03T08:53:18Z","updated_at":"2019-01-12T05:19:49Z","closed_at":"2018-01-26T13:27:35Z","author_association":"COLLABORATOR","body":"**[cdxf](https://jira.spring.io/secure/ViewProfile.jspa?name=snoobvn)** opened **[SPR-16257](https://jira.spring.io/browse/SPR-16257?redirect=false)** and commented\n\nI tried with Java and it still works, but not with Kotlin. both use Spring Boot 2.0\n\nKotlin (even with `@Temporal` it doesn't work too):\n\n```java\n\n@Entity\nclass Post(\n        @get:NotBlank\n        var name: String = \"\",\n        val content: String = \"\"\n) {\n    @Id\n    @GeneratedValue(strategy = GenerationType.SEQUENCE)\n    val id: Int? = null\n    @ManyToMany\n    @Cascade(CascadeType.ALL)\n    val tags: MutableSet<Tag> = mutableSetOf()\n\n    @CreationTimestamp\n    lateinit var createDate: Date\n\n\n    @UpdateTimestamp\n    lateinit var updateDate: Date\n\n    fun addTag(tag: Tag) {\n        this.tags.add(tag)\n        tag.posts.add(this)\n    }\n}\n```\n\nThe result:\n!kotlin.png|thumbnail!\n\nJava:\n\n```java\n\n@Entity\npublic class Post {\n\n    public Post(@NotBlank String name, String content) {\n        this.name = name;\n        this.content = content;\n    }\n\n    public void addTag(Tag tag) {\n        this.tags.add(tag);\n        tag.posts.add(this);\n    }\n\n    @Id\n    @GeneratedValue(strategy = GenerationType.SEQUENCE)\n    Integer id;\n    @NotBlank\n    String name;\n    String content;\n    @ManyToMany\n    @JoinColumn(name=\"tags\")\n    @Cascade(CascadeType.ALL)\n    public Set<Tag> tags = new HashSet();\n\n    @CreationTimestamp\n    @Temporal(TemporalType.TIMESTAMP)\n    @Column(name = \"created\")\n    Date createDate;\n    @Column(name = \"updated\")\n    @UpdateTimestamp\n    Date updateDate;\n}\n```\n\nThe result:\n\n!java.png|thumbnail!\n\nBelow is my sample project:\n[^project.zip]\n\n\n---\n\n**Affects:** 5.0.2\n\n**Attachments:**\n- [java.png](https://jira.spring.io/secure/attachment/25312/java.png) (_3.71 kB_)\n- [kotlin.png](https://jira.spring.io/secure/attachment/25311/kotlin.png) (_3.44 kB_)\n- [project.zip](https://jira.spring.io/secure/attachment/25313/project.zip) (_56.62 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453464613","453464615"], "labels":["in: data","status: invalid"]}