{"id":"14384", "title":"Problem with the @Scheduled Annotation [SPR-9750]", "body":"**[Oussama Zoghlami](https://jira.spring.io/secure/ViewProfile.jspa?name=oussama.zoghlami)** opened **[SPR-9750](https://jira.spring.io/browse/SPR-9750?redirect=false)** and commented

Hi all,

When i try to schedule my batch job from the spring batch admin context, using the `@Scheduled` annotation (for example every minute), it's runned twice !!!

Here is my configuration :

--- Scheduler.java
`@Scheduled`(cron = \"0 * * * * 1-5\")
public void testCron(){
logger.debug(\"Testing the cron ...\");
}

--- applicationContext-scheduling.xml

\\<!-- Class containing the annotated methods -->
\\<bean id=\"...\" class=\"... Class containing the annotated methods\" />

<task:scheduler id=\"scheduler\" />

<task:executor id=\"executor\" pool-size=\"5\"/>

<task:annotation-driven scheduler=\"scheduler\" executor=\"executor\" />

PS: When i use an XML based configuration (by using <task:scheduled>), i have not this problem.



---

**Affects:** 3.1.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14384","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14384/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14384/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14384/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14384","id":398152920,"node_id":"MDU6SXNzdWUzOTgxNTI5MjA=","number":14384,"title":"Problem with the @Scheduled Annotation [SPR-9750]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2012-09-04T03:05:32Z","updated_at":"2019-01-12T05:28:07Z","closed_at":"2018-12-28T09:30:17Z","author_association":"COLLABORATOR","body":"**[Oussama Zoghlami](https://jira.spring.io/secure/ViewProfile.jspa?name=oussama.zoghlami)** opened **[SPR-9750](https://jira.spring.io/browse/SPR-9750?redirect=false)** and commented\n\nHi all,\n\nWhen i try to schedule my batch job from the spring batch admin context, using the `@Scheduled` annotation (for example every minute), it's runned twice !!!\n\nHere is my configuration :\n\n--- Scheduler.java\n`@Scheduled`(cron = \"0 * * * * 1-5\")\npublic void testCron(){\nlogger.debug(\"Testing the cron ...\");\n}\n\n--- applicationContext-scheduling.xml\n\n\\<!-- Class containing the annotated methods -->\n\\<bean id=\"...\" class=\"... Class containing the annotated methods\" />\n\n<task:scheduler id=\"scheduler\" />\n\n<task:executor id=\"executor\" pool-size=\"5\"/>\n\n<task:annotation-driven scheduler=\"scheduler\" executor=\"executor\" />\n\nPS: When i use an XML based configuration (by using <task:scheduled>), i have not this problem.\n\n\n\n---\n\n**Affects:** 3.1.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453396687"], "labels":["in: core","status: invalid"]}