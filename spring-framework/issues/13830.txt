{"id":"13830", "title":"new method 'createThreadPoolExecutor' in ThreadPoolTaskExecutor [SPR-9192]", "body":"**[Andreas Lichtenstein](https://jira.spring.io/secure/ViewProfile.jspa?name=anlichte)** opened **[SPR-9192](https://jira.spring.io/browse/SPR-9192?redirect=false)** and commented

Hello,

i need the possibility to override the creation of ThreadPoolExecutor in initialize(). here my recommendation:

\\<code>
/**
* Creates the BlockingQueue and the ThreadPoolExecutor.
*
* `@see` #createQueue
*/
public void initialize()
{
if (logger.isInfoEnabled())
{
logger.info(\"Initializing ThreadPoolExecutor\" + (this.beanName != null ? \" '\" + this.beanName + \"'\" : \"\"));
}

    	if (!this.threadNamePrefixSet && this.beanName != null)
    	{
    		setThreadNamePrefix(this.beanName + \"-\");
    	}
    	
    	this.threadPoolExecutor = createThreadPoolExecutor(this.corePoolSize, this.maxPoolSize, this.keepAliveSeconds, TimeUnit.SECONDS, createQueue(this.queueCapacity), this.threadFactory, this.rejectedExecutionHandler);
    	
    	if (this.allowCoreThreadTimeOut)
    	{
    		this.threadPoolExecutor.allowCoreThreadTimeOut(true);
    	}
    }
    
    protected ThreadPoolExecutor createThreadPoolExecutor(int corePoolSize, int maxPoolSize, long keepAliveSeconds, TimeUnit timeUnit, BlockingQueue<Runnable> queue, ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler)
    {
    	return new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveSeconds, timeUnit, queue, threadFactory, rejectedExecutionHandler);
    }

\\<code>
thx

---

**Affects:** 3.1.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13830","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13830/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13830/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13830/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13830","id":398117746,"node_id":"MDU6SXNzdWUzOTgxMTc3NDY=","number":13830,"title":"new method 'createThreadPoolExecutor' in ThreadPoolTaskExecutor [SPR-9192]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2012-03-02T07:10:06Z","updated_at":"2019-01-13T07:05:58Z","closed_at":"2019-01-12T00:27:30Z","author_association":"COLLABORATOR","body":"**[Andreas Lichtenstein](https://jira.spring.io/secure/ViewProfile.jspa?name=anlichte)** opened **[SPR-9192](https://jira.spring.io/browse/SPR-9192?redirect=false)** and commented\n\nHello,\n\ni need the possibility to override the creation of ThreadPoolExecutor in initialize(). here my recommendation:\n\n\\<code>\n/**\n* Creates the BlockingQueue and the ThreadPoolExecutor.\n*\n* `@see` #createQueue\n*/\npublic void initialize()\n{\nif (logger.isInfoEnabled())\n{\nlogger.info(\"Initializing ThreadPoolExecutor\" + (this.beanName != null ? \" '\" + this.beanName + \"'\" : \"\"));\n}\n\n    \tif (!this.threadNamePrefixSet && this.beanName != null)\n    \t{\n    \t\tsetThreadNamePrefix(this.beanName + \"-\");\n    \t}\n    \t\n    \tthis.threadPoolExecutor = createThreadPoolExecutor(this.corePoolSize, this.maxPoolSize, this.keepAliveSeconds, TimeUnit.SECONDS, createQueue(this.queueCapacity), this.threadFactory, this.rejectedExecutionHandler);\n    \t\n    \tif (this.allowCoreThreadTimeOut)\n    \t{\n    \t\tthis.threadPoolExecutor.allowCoreThreadTimeOut(true);\n    \t}\n    }\n    \n    protected ThreadPoolExecutor createThreadPoolExecutor(int corePoolSize, int maxPoolSize, long keepAliveSeconds, TimeUnit timeUnit, BlockingQueue<Runnable> queue, ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler)\n    {\n    \treturn new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveSeconds, timeUnit, queue, threadFactory, rejectedExecutionHandler);\n    }\n\n\\<code>\nthx\n\n---\n\n**Affects:** 3.1.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453716692"], "labels":["in: core","status: bulk-closed"]}