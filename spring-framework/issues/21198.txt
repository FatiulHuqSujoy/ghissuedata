{"id":"21198", "title":"Binding configuration properties dynamically is lot harder than it needs to be [SPR-16657]", "body":"**[Abhijit Sarkar](https://jira.spring.io/secure/ViewProfile.jspa?name=asarkar)** opened **[SPR-16657](https://jira.spring.io/browse/SPR-16657?redirect=false)** and commented

I've a use case where I need to bind configuration properties based on a prefix determined at runtime.

```
interface Condition {
    companion object {
        const val PREFIX = \"touchstone.condition\"
    }

    enum class Phase {
        PRE, POST
    }

    fun run(chunkContext: ChunkContext): ExitStatus

    fun phase(): Phase = Phase.PRE

    fun order(): Int = 1

    fun shouldRun(): Boolean = true

    val qualifiedName: String
        get() = listOf(PREFIX, phase().name, javaClass.simpleName)
                .joinToString(separator = \".\") { it.toLowerCase(Locale.ENGLISH) }
}
```

Each implementation of the above interface can override the values `order` and `shouldRun` using usual Boot configuration overrides. This is not unlike how [Hystrix Configurations](https://github.com/Netflix/Hystrix/wiki/Configuration) work; in other words, not unprecedented. However, doing this in practice is a lot harder than it needs to be, due to unnecessary package-level access of some classes in the framework, and in general lack of support for the Open/closed principal. Let's break it down:

1. `ConfigurationPropertiesBinder.bind(Bindable<?> target)` method is `public` and looks promising since creating a `Bindable` is not hard, but then it does

```
Assert.state(annotation != null, \"Missing @ConfigurationProperties on \" + target);
```

which is a problem when dynamically creating a target. The annotation can be synthesized (more on that later), so it's not technically necessary for it to be specified at compile time.

2. Methods `getValidators` and `getBindHandler` called from `bind` are `private` scoped, so I had to copy-paste them with minor modifications.

3. `bind` creates a `Binder`, which uses quite a few classes with package-scoped constructor. Thus, in order to create a `Binder`, I'd to resort to the hack of putting the following class in `org.springframework.boot.context.properties` package. This is completely unnecessary.

```
class BinderFactory {
    companion object {
        private fun propertyEditorInitializer(ctx: ApplicationContext): Consumer<PropertyEditorRegistry>? {
            return if (ctx is ConfigurableApplicationContext) {
                Consumer {
                    ctx.beanFactory.copyRegisteredEditorsTo(it)
                }
            } else null
        }

        fun newBinder(ctx: ApplicationContext): Binder {
            val propertySources = PropertySourcesDeducer(ctx)
                    .propertySources
            return Binder(
                    ConfigurationPropertySources.from(propertySources),
                    PropertySourcesPlaceholdersResolver(propertySources),
                    ConversionServiceDeducer(ctx).conversionService,
                    propertyEditorInitializer(ctx)
            )
        }
    }
}
```

4. Having done all that, now I can write the following, apparently simple, code:

```
val annotation = synthesizeAnnotation(qn)
val target = bindable(annotation)
val handler = bindHandler(annotation)

BinderFactory.newBinder(ctx).bind(qn, target, handler)
qn to target.value.get()
```

where `synthesizeAnnotation` uses `AnnotationUtils.synthesizeAnnotation` to do what it claims. The rest of the private methods calls should be obvious from the context.

**Possible fix**:

1. An overloaded version of `bind` accepting a `ConfigurationProperties annotation` should solve this problem while maintaining backward compatibility.
2. Creating a `BinderFactory` class will not require changing the package scope of the various constructors used by `Binder` for those who want to use it directly without going through `ConfigurationPropertiesBinder`.
3. I see no reason why `bindHandler` as shown below can't be a `public static` method.

```
private fun bindHandler(annotation: ConfigurationProperties): BindHandler {
    var handler = BindHandler.DEFAULT
    if (annotation.ignoreInvalidFields) {
        handler = IgnoreErrorsBindHandler(handler)
    }
    if (!annotation.ignoreUnknownFields) {
        val filter = UnboundElementsSourceFilter()
        handler = NoUnboundElementsBindHandler(handler, filter)
    }
    return handler
}
```



---

**Affects:** 5.0.4
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21198","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21198/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21198/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21198/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21198","id":398225266,"node_id":"MDU6SXNzdWUzOTgyMjUyNjY=","number":21198,"title":"Binding configuration properties dynamically is lot harder than it needs to be [SPR-16657]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-03-28T17:49:28Z","updated_at":"2019-01-12T05:19:08Z","closed_at":"2018-03-28T22:51:19Z","author_association":"COLLABORATOR","body":"**[Abhijit Sarkar](https://jira.spring.io/secure/ViewProfile.jspa?name=asarkar)** opened **[SPR-16657](https://jira.spring.io/browse/SPR-16657?redirect=false)** and commented\n\nI've a use case where I need to bind configuration properties based on a prefix determined at runtime.\n\n```\ninterface Condition {\n    companion object {\n        const val PREFIX = \"touchstone.condition\"\n    }\n\n    enum class Phase {\n        PRE, POST\n    }\n\n    fun run(chunkContext: ChunkContext): ExitStatus\n\n    fun phase(): Phase = Phase.PRE\n\n    fun order(): Int = 1\n\n    fun shouldRun(): Boolean = true\n\n    val qualifiedName: String\n        get() = listOf(PREFIX, phase().name, javaClass.simpleName)\n                .joinToString(separator = \".\") { it.toLowerCase(Locale.ENGLISH) }\n}\n```\n\nEach implementation of the above interface can override the values `order` and `shouldRun` using usual Boot configuration overrides. This is not unlike how [Hystrix Configurations](https://github.com/Netflix/Hystrix/wiki/Configuration) work; in other words, not unprecedented. However, doing this in practice is a lot harder than it needs to be, due to unnecessary package-level access of some classes in the framework, and in general lack of support for the Open/closed principal. Let's break it down:\n\n1. `ConfigurationPropertiesBinder.bind(Bindable<?> target)` method is `public` and looks promising since creating a `Bindable` is not hard, but then it does\n\n```\nAssert.state(annotation != null, \"Missing @ConfigurationProperties on \" + target);\n```\n\nwhich is a problem when dynamically creating a target. The annotation can be synthesized (more on that later), so it's not technically necessary for it to be specified at compile time.\n\n2. Methods `getValidators` and `getBindHandler` called from `bind` are `private` scoped, so I had to copy-paste them with minor modifications.\n\n3. `bind` creates a `Binder`, which uses quite a few classes with package-scoped constructor. Thus, in order to create a `Binder`, I'd to resort to the hack of putting the following class in `org.springframework.boot.context.properties` package. This is completely unnecessary.\n\n```\nclass BinderFactory {\n    companion object {\n        private fun propertyEditorInitializer(ctx: ApplicationContext): Consumer<PropertyEditorRegistry>? {\n            return if (ctx is ConfigurableApplicationContext) {\n                Consumer {\n                    ctx.beanFactory.copyRegisteredEditorsTo(it)\n                }\n            } else null\n        }\n\n        fun newBinder(ctx: ApplicationContext): Binder {\n            val propertySources = PropertySourcesDeducer(ctx)\n                    .propertySources\n            return Binder(\n                    ConfigurationPropertySources.from(propertySources),\n                    PropertySourcesPlaceholdersResolver(propertySources),\n                    ConversionServiceDeducer(ctx).conversionService,\n                    propertyEditorInitializer(ctx)\n            )\n        }\n    }\n}\n```\n\n4. Having done all that, now I can write the following, apparently simple, code:\n\n```\nval annotation = synthesizeAnnotation(qn)\nval target = bindable(annotation)\nval handler = bindHandler(annotation)\n\nBinderFactory.newBinder(ctx).bind(qn, target, handler)\nqn to target.value.get()\n```\n\nwhere `synthesizeAnnotation` uses `AnnotationUtils.synthesizeAnnotation` to do what it claims. The rest of the private methods calls should be obvious from the context.\n\n**Possible fix**:\n\n1. An overloaded version of `bind` accepting a `ConfigurationProperties annotation` should solve this problem while maintaining backward compatibility.\n2. Creating a `BinderFactory` class will not require changing the package scope of the various constructors used by `Binder` for those who want to use it directly without going through `ConfigurationPropertiesBinder`.\n3. I see no reason why `bindHandler` as shown below can't be a `public static` method.\n\n```\nprivate fun bindHandler(annotation: ConfigurationProperties): BindHandler {\n    var handler = BindHandler.DEFAULT\n    if (annotation.ignoreInvalidFields) {\n        handler = IgnoreErrorsBindHandler(handler)\n    }\n    if (!annotation.ignoreUnknownFields) {\n        val filter = UnboundElementsSourceFilter()\n        handler = NoUnboundElementsBindHandler(handler, filter)\n    }\n    return handler\n}\n```\n\n\n\n---\n\n**Affects:** 5.0.4\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453469666","453469668","453469669"], "labels":["in: core","status: invalid"]}