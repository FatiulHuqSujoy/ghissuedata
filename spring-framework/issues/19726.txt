{"id":"19726", "title":"Collection autowiring does not resolve field-level type variable against containing class [SPR-15160]", "body":"**[thomas weidner](https://jira.spring.io/secure/ViewProfile.jspa?name=thomas001le)** opened **[SPR-15160](https://jira.spring.io/browse/SPR-15160?redirect=false)** and commented

Spring fails to autowire fields correctly, if the field contains a generic type variable, which is later bound. It does not matter if the type variable is bound by deriving a concrete (not generic) type or just defining a bean of the generic type with fixed type parameters.

Spring either finds not all beans or too many beans, whereas the second case is especially troublesome as it breaks static type checking.

Consider two beans of types `Foo` and `Bar` both implementing an interface `Base`. Moreover, we have a type `BeanCollector<C>` which autowires a single field of type `List<C>`, a field of type `C` and prints both values after construction. Additionally, a `BaseBeanCollector<C` behaves the same way, but the type variable has a type bound on `Base`.We define the following beans:

```java
	@Bean
	public BeanCollector<Foo> fooBeanCollector() {
		return new BeanCollector<>(\"foo\");
	}

	@Bean
	public BeanCollector<Bar> barBeanCollector() {
		return new BeanCollector<>(\"bar\");
	}

	@Bean
	public BaseBeanCollector<Foo> fooBaseBeanCollector() {
		return new BaseBeanCollector<Foo>(\"foo base\");
	}

	@Bean
	public BaseBeanCollector<Bar> barBaseBeanCollector() {
		return new BaseBeanCollector<>(\"bar base\");
	}


	@Bean
	public BaseBeanCollector<Foo> fooBaseBeanCollectorAnonClass() {
		return new BaseBeanCollector<Foo>(\"foo base anonymous class\") {};
	}

	@Bean
	public BaseBeanCollector<Bar> barBaseBeanCollectorAnonClass() {
		return new BaseBeanCollector<Bar>(\"bar base anonymous class\") {};
	}


	@Bean
	public BeanCollector<Foo> fooBeanCollectorAnonClass() {
		return new BeanCollector<Foo>(\"foo anonymous class\") {};
	}

	@Bean
	public BeanCollector<Bar> barBeanCollectorAnonClass() {
		return new BeanCollector<Bar>(\"bar anonymous class\") {};
	}

	@Bean
	public FooBeanCollector fooBeanCollectorExtraClass() {
		return new FooBeanCollector();
	}

	@Bean
	public FooBaseBeanCollector fooBaseBeanCollectorExtraClass() {
		return new FooBaseBeanCollector();
	}
```

Here, `Foo(Base)BeanCollector` are classes which extend `(Base)BeanCollector<Foo>`.

The really remarkable output of this code is:

```
Beans for foo base: [com.example.Bar@7fb4f2a9, com.example.Foo@4dc27487]
Beans for foo: [] and org.springframework.jmx.export.annotation.AnnotationMBeanExporter@13df2a8c
Beans for bar base: [com.example.Bar@7fb4f2a9, com.example.Foo@4dc27487]
Beans for foo base anonymous class: [com.example.Foo@4dc27487]
Beans for bar base anonymous class: [com.example.Bar@7fb4f2a9]
Beans for foo anonymous class: [] and com.example.Foo@4dc27487
Beans for bar anonymous class: [] and com.example.Bar@7fb4f2a9
Beans for foo extra class: [] and com.example.Foo@4dc27487
Beans for foo base extra class: [com.example.Foo@4dc27487]
Beans for bar: [] and org.springframework.jmx.export.annotation.AnnotationMBeanExporter@13df2a8c
```

The following table shows the autowiring behaviour:
| | collector instance without extra Type | collector instance as anonymous type | collector instance as class type |
| no type bound |  empty bean list, arbitrary single bean | empty bean list | empty bean list |
| type bound | too many beans in list | OK | OK |

Either, spring should behave as expected, or report an error that autowiring using type variables is not supported. The current behaviour can trigger all kind of subtle bugs.

---

**Affects:** 4.3.5

**Reference URL:** https://github.com/thomas001/spring-generic-autowire-bug

**Issue Links:**
- #16146 Introspect factory method return type for type variable resolution at injection points

1 votes, 3 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19726","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19726/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19726/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19726/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19726","id":398204221,"node_id":"MDU6SXNzdWUzOTgyMDQyMjE=","number":19726,"title":"Collection autowiring does not resolve field-level type variable against containing class [SPR-15160]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/155","html_url":"https://github.com/spring-projects/spring-framework/milestone/155","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/155/labels","id":3960928,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyOA==","number":155,"title":"4.3.6","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":64,"state":"closed","created_at":"2019-01-10T22:04:57Z","updated_at":"2019-01-11T09:33:40Z","due_on":"2017-01-24T08:00:00Z","closed_at":"2019-01-10T22:04:57Z"},"comments":2,"created_at":"2017-01-18T12:37:08Z","updated_at":"2019-01-11T15:29:30Z","closed_at":"2017-01-25T14:18:15Z","author_association":"COLLABORATOR","body":"**[thomas weidner](https://jira.spring.io/secure/ViewProfile.jspa?name=thomas001le)** opened **[SPR-15160](https://jira.spring.io/browse/SPR-15160?redirect=false)** and commented\n\nSpring fails to autowire fields correctly, if the field contains a generic type variable, which is later bound. It does not matter if the type variable is bound by deriving a concrete (not generic) type or just defining a bean of the generic type with fixed type parameters.\n\nSpring either finds not all beans or too many beans, whereas the second case is especially troublesome as it breaks static type checking.\n\nConsider two beans of types `Foo` and `Bar` both implementing an interface `Base`. Moreover, we have a type `BeanCollector<C>` which autowires a single field of type `List<C>`, a field of type `C` and prints both values after construction. Additionally, a `BaseBeanCollector<C` behaves the same way, but the type variable has a type bound on `Base`.We define the following beans:\n\n```java\n\t@Bean\n\tpublic BeanCollector<Foo> fooBeanCollector() {\n\t\treturn new BeanCollector<>(\"foo\");\n\t}\n\n\t@Bean\n\tpublic BeanCollector<Bar> barBeanCollector() {\n\t\treturn new BeanCollector<>(\"bar\");\n\t}\n\n\t@Bean\n\tpublic BaseBeanCollector<Foo> fooBaseBeanCollector() {\n\t\treturn new BaseBeanCollector<Foo>(\"foo base\");\n\t}\n\n\t@Bean\n\tpublic BaseBeanCollector<Bar> barBaseBeanCollector() {\n\t\treturn new BaseBeanCollector<>(\"bar base\");\n\t}\n\n\n\t@Bean\n\tpublic BaseBeanCollector<Foo> fooBaseBeanCollectorAnonClass() {\n\t\treturn new BaseBeanCollector<Foo>(\"foo base anonymous class\") {};\n\t}\n\n\t@Bean\n\tpublic BaseBeanCollector<Bar> barBaseBeanCollectorAnonClass() {\n\t\treturn new BaseBeanCollector<Bar>(\"bar base anonymous class\") {};\n\t}\n\n\n\t@Bean\n\tpublic BeanCollector<Foo> fooBeanCollectorAnonClass() {\n\t\treturn new BeanCollector<Foo>(\"foo anonymous class\") {};\n\t}\n\n\t@Bean\n\tpublic BeanCollector<Bar> barBeanCollectorAnonClass() {\n\t\treturn new BeanCollector<Bar>(\"bar anonymous class\") {};\n\t}\n\n\t@Bean\n\tpublic FooBeanCollector fooBeanCollectorExtraClass() {\n\t\treturn new FooBeanCollector();\n\t}\n\n\t@Bean\n\tpublic FooBaseBeanCollector fooBaseBeanCollectorExtraClass() {\n\t\treturn new FooBaseBeanCollector();\n\t}\n```\n\nHere, `Foo(Base)BeanCollector` are classes which extend `(Base)BeanCollector<Foo>`.\n\nThe really remarkable output of this code is:\n\n```\nBeans for foo base: [com.example.Bar@7fb4f2a9, com.example.Foo@4dc27487]\nBeans for foo: [] and org.springframework.jmx.export.annotation.AnnotationMBeanExporter@13df2a8c\nBeans for bar base: [com.example.Bar@7fb4f2a9, com.example.Foo@4dc27487]\nBeans for foo base anonymous class: [com.example.Foo@4dc27487]\nBeans for bar base anonymous class: [com.example.Bar@7fb4f2a9]\nBeans for foo anonymous class: [] and com.example.Foo@4dc27487\nBeans for bar anonymous class: [] and com.example.Bar@7fb4f2a9\nBeans for foo extra class: [] and com.example.Foo@4dc27487\nBeans for foo base extra class: [com.example.Foo@4dc27487]\nBeans for bar: [] and org.springframework.jmx.export.annotation.AnnotationMBeanExporter@13df2a8c\n```\n\nThe following table shows the autowiring behaviour:\n| | collector instance without extra Type | collector instance as anonymous type | collector instance as class type |\n| no type bound |  empty bean list, arbitrary single bean | empty bean list | empty bean list |\n| type bound | too many beans in list | OK | OK |\n\nEither, spring should behave as expected, or report an error that autowiring using type variables is not supported. The current behaviour can trigger all kind of subtle bugs.\n\n---\n\n**Affects:** 4.3.5\n\n**Reference URL:** https://github.com/thomas001/spring-generic-autowire-bug\n\n**Issue Links:**\n- #16146 Introspect factory method return type for type variable resolution at injection points\n\n1 votes, 3 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453450786","453450787"], "labels":["in: core","type: bug"]}