{"id":"14251", "title":"Transparent authentication fails when POSTing to Kerberos web service using the Simple HTTP Client [SPR-9617]", "body":"**[Ed Sherington](https://jira.spring.io/secure/ViewProfile.jspa?name=sherington)** opened **[SPR-9617](https://jira.spring.io/browse/SPR-9617?redirect=false)** and commented

We have a suite of Kerberos-authenticated web services running with Spring 3.0.x without any problems.

We upgraded those web services to run with Spring 3.1.1 and encountered one change in Spring's HTTP client behavior that causes us a failure.

The one change is related only to POST requests and their interaction with our Kerberos implementation.

Ordinarily when we invoke a web service we expect this behavior:

1. We send the initial request to the server;
2. The server responds with a standard 401 Not Authorized response;
3. The server sets an \"authenticate\" header on the response;
4. The client receives the 401 and the authenticate header response, and transparently retries the request but this time it also sends the required Kerberos ticket;
5. The server now authenticates the ticket and the web service call succeeds.

These steps all worked just fine for us with Spring 3.0.x, and these steps **mostly** work for us with Spring 3.1.x.

What we see with Spring 3.1.x is that a POST request will never get authenticated and will consequently always fail. Tracing the HTTP requests and responses shows that the transparent retry and authentication never occurs -- we only see the first 401 response.

I believe the problem is related to the implementation of this class, which is a refactoring of a class from 3.0.x:

```
final class SimpleBufferingClientHttpRequest extends AbstractBufferingClientHttpRequest {

	@Override
	protected ClientHttpResponse executeInternal(HttpHeaders headers, byte[] bufferedOutput) throws IOException {
		for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
			String headerName = entry.getKey();
			for (String headerValue : entry.getValue()) {
				this.connection.addRequestProperty(headerName, headerValue);
			}
		}

		if (this.connection.getDoOutput()) {
			this.connection.setFixedLengthStreamingMode(bufferedOutput.length);
		}
		this.connection.connect();
		if (this.connection.getDoOutput()) {
			FileCopyUtils.copy(bufferedOutput, this.connection.getOutputStream());
		}

		return new SimpleClientHttpResponse(this.connection);
	}

}
```

In particular this part:

```
if (this.connection.getDoOutput()) {
	this.connection.setFixedLengthStreamingMode(bufferedOutput.length);
}
```

In our case of POST, `getDoOutput()` is `true`, and then I think setting the connection into fixed length streaming mode is preventing the transparent authentication that we need.

Our solution is to provide our own implementation of `ClientHttpRequestFactory` and associated request/response implementations that restores the old behavior in Spring 3.0.x.

So we do have a workaround, but we'd rather use unchanged core classes of course.


---

**Affects:** 3.1.1

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/92ad66bf10b570dde25af44d8ff3f958dfec2a9d

0 votes, 7 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14251","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14251/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14251/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14251/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14251","id":398151950,"node_id":"MDU6SXNzdWUzOTgxNTE5NTA=","number":14251,"title":"Transparent authentication fails when POSTing to Kerberos web service using the Simple HTTP Client [SPR-9617]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/90","html_url":"https://github.com/spring-projects/spring-framework/milestone/90","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/90/labels","id":3960863,"node_id":"MDk6TWlsZXN0b25lMzk2MDg2Mw==","number":90,"title":"3.2.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":107,"state":"closed","created_at":"2019-01-10T22:03:39Z","updated_at":"2019-01-11T07:45:47Z","due_on":"2013-03-13T07:00:00Z","closed_at":"2019-01-10T22:03:39Z"},"comments":5,"created_at":"2012-07-22T23:28:10Z","updated_at":"2013-03-06T11:29:53Z","closed_at":"2013-03-06T11:29:53Z","author_association":"COLLABORATOR","body":"**[Ed Sherington](https://jira.spring.io/secure/ViewProfile.jspa?name=sherington)** opened **[SPR-9617](https://jira.spring.io/browse/SPR-9617?redirect=false)** and commented\n\nWe have a suite of Kerberos-authenticated web services running with Spring 3.0.x without any problems.\n\nWe upgraded those web services to run with Spring 3.1.1 and encountered one change in Spring's HTTP client behavior that causes us a failure.\n\nThe one change is related only to POST requests and their interaction with our Kerberos implementation.\n\nOrdinarily when we invoke a web service we expect this behavior:\n\n1. We send the initial request to the server;\n2. The server responds with a standard 401 Not Authorized response;\n3. The server sets an \"authenticate\" header on the response;\n4. The client receives the 401 and the authenticate header response, and transparently retries the request but this time it also sends the required Kerberos ticket;\n5. The server now authenticates the ticket and the web service call succeeds.\n\nThese steps all worked just fine for us with Spring 3.0.x, and these steps **mostly** work for us with Spring 3.1.x.\n\nWhat we see with Spring 3.1.x is that a POST request will never get authenticated and will consequently always fail. Tracing the HTTP requests and responses shows that the transparent retry and authentication never occurs -- we only see the first 401 response.\n\nI believe the problem is related to the implementation of this class, which is a refactoring of a class from 3.0.x:\n\n```\nfinal class SimpleBufferingClientHttpRequest extends AbstractBufferingClientHttpRequest {\n\n\t@Override\n\tprotected ClientHttpResponse executeInternal(HttpHeaders headers, byte[] bufferedOutput) throws IOException {\n\t\tfor (Map.Entry<String, List<String>> entry : headers.entrySet()) {\n\t\t\tString headerName = entry.getKey();\n\t\t\tfor (String headerValue : entry.getValue()) {\n\t\t\t\tthis.connection.addRequestProperty(headerName, headerValue);\n\t\t\t}\n\t\t}\n\n\t\tif (this.connection.getDoOutput()) {\n\t\t\tthis.connection.setFixedLengthStreamingMode(bufferedOutput.length);\n\t\t}\n\t\tthis.connection.connect();\n\t\tif (this.connection.getDoOutput()) {\n\t\t\tFileCopyUtils.copy(bufferedOutput, this.connection.getOutputStream());\n\t\t}\n\n\t\treturn new SimpleClientHttpResponse(this.connection);\n\t}\n\n}\n```\n\nIn particular this part:\n\n```\nif (this.connection.getDoOutput()) {\n\tthis.connection.setFixedLengthStreamingMode(bufferedOutput.length);\n}\n```\n\nIn our case of POST, `getDoOutput()` is `true`, and then I think setting the connection into fixed length streaming mode is preventing the transparent authentication that we need.\n\nOur solution is to provide our own implementation of `ClientHttpRequestFactory` and associated request/response implementations that restores the old behavior in Spring 3.0.x.\n\nSo we do have a workaround, but we'd rather use unchanged core classes of course.\n\n\n---\n\n**Affects:** 3.1.1\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/92ad66bf10b570dde25af44d8ff3f958dfec2a9d\n\n0 votes, 7 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453395677","453395678","453395680","453395682","453395684"], "labels":["in: web","type: enhancement"]}