{"id":"14117", "title":"BeanWrapper should allow reading of Boolean properties accessible via 'is' methods [SPR-9482]", "body":"**[Fried Hoeben](https://jira.spring.io/secure/ViewProfile.jspa?name=fhoeben)** opened **[SPR-9482](https://jira.spring.io/browse/SPR-9482?redirect=false)** and commented

When accessing bean properties via a BeanWrapper Boolean properties whose getter method uses an 'is' prefix rather than a 'get' prefix are not seen as readable.
If the type of the property is boolean (i.e. primitive) it is readable.

Please also allow reading of Boolean properties using the 'is' prefix.

As a workaround we created our own getValue() implementation, but that is not really what we would like:

```
    private static Object getValue(BeanWrapper wrapper, String propertyName) {
        Object result = null;
        if (wrapper.isReadableProperty(propertyName)) {
            result = wrapper.getPropertyValue(propertyName);
        } else {
            PropertyDescriptor propertyDescriptor = wrapper.getPropertyDescriptor(propertyName);
            Class<?> propertyType = propertyDescriptor.getPropertyType();
            if (Boolean.class.equals(propertyType)) {
                String name = StringUtils.capitalize(propertyName);
                Object expected = wrapper.getWrappedInstance();
                Method m = ReflectionUtils.findMethod(expected.getClass(), \"is\" + name);
                if (m != null && m.getReturnType().equals(Boolean.class)) {
                    result = ReflectionUtils.invokeMethod(m, expected);
                } else {
                    throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));
                }
            } else {
                throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));
            }
        }
        return result;
    }

    private static String createErrorMsg(BeanWrapper wrapper, String propertyName) {
        return propertyName + \" can not be read on: \" + wrapper.getWrappedClass();
    }
```



---

**Affects:** 3.1.1

**Attachments:**
- [IsPropertyTest.java](https://jira.spring.io/secure/attachment/19826/IsPropertyTest.java) (_3.33 kB_)

1 votes, 1 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14117","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14117/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14117/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14117/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14117","id":398151030,"node_id":"MDU6SXNzdWUzOTgxNTEwMzA=","number":14117,"title":"BeanWrapper should allow reading of Boolean properties accessible via 'is' methods [SPR-9482]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2012-06-07T04:55:05Z","updated_at":"2019-01-12T02:47:54Z","closed_at":"2019-01-12T02:47:54Z","author_association":"COLLABORATOR","body":"**[Fried Hoeben](https://jira.spring.io/secure/ViewProfile.jspa?name=fhoeben)** opened **[SPR-9482](https://jira.spring.io/browse/SPR-9482?redirect=false)** and commented\n\nWhen accessing bean properties via a BeanWrapper Boolean properties whose getter method uses an 'is' prefix rather than a 'get' prefix are not seen as readable.\nIf the type of the property is boolean (i.e. primitive) it is readable.\n\nPlease also allow reading of Boolean properties using the 'is' prefix.\n\nAs a workaround we created our own getValue() implementation, but that is not really what we would like:\n\n```\n    private static Object getValue(BeanWrapper wrapper, String propertyName) {\n        Object result = null;\n        if (wrapper.isReadableProperty(propertyName)) {\n            result = wrapper.getPropertyValue(propertyName);\n        } else {\n            PropertyDescriptor propertyDescriptor = wrapper.getPropertyDescriptor(propertyName);\n            Class<?> propertyType = propertyDescriptor.getPropertyType();\n            if (Boolean.class.equals(propertyType)) {\n                String name = StringUtils.capitalize(propertyName);\n                Object expected = wrapper.getWrappedInstance();\n                Method m = ReflectionUtils.findMethod(expected.getClass(), \"is\" + name);\n                if (m != null && m.getReturnType().equals(Boolean.class)) {\n                    result = ReflectionUtils.invokeMethod(m, expected);\n                } else {\n                    throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));\n                }\n            } else {\n                throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));\n            }\n        }\n        return result;\n    }\n\n    private static String createErrorMsg(BeanWrapper wrapper, String propertyName) {\n        return propertyName + \" can not be read on: \" + wrapper.getWrappedClass();\n    }\n```\n\n\n\n---\n\n**Affects:** 3.1.1\n\n**Attachments:**\n- [IsPropertyTest.java](https://jira.spring.io/secure/attachment/19826/IsPropertyTest.java) (_3.33 kB_)\n\n1 votes, 1 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453394640","453394641","453713458"], "labels":["in: core","status: bulk-closed"]}