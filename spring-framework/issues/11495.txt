{"id":"11495", "title":"using Scanner for command reading after loading ApplicationContext, causes a bug in veriso 3.0 and later nightly builds [SPR-6829]", "body":"**[oner ekiz](https://jira.spring.io/secure/ViewProfile.jspa?name=ekizoner)** opened **[SPR-6829](https://jira.spring.io/browse/SPR-6829?redirect=false)** and commented

using Scanner for command reading after loading ApplicationContext, causes a bug in veriso 3.0 and later nightly builds. when i removed spring 3.0 libraries and include 2.5 verison instead, this bug does not occure.

Here is my codes, to produce it

applicationContext.xml

---

\\<bean id=\"myThread\" class=\"test.MyThread\">
\\<property name=\"service\" ref=\"service\"/>\\</bean>
\\<bean id=\"service\" class=\"test.Service\">
\\<property name=\"dao\" ref=\"dao\"/>
\\</bean>
\\<bean id=\"dao\" class=\"test.Dao\">
\\<property name=\"dataSource\" ref=\"dataSource\"/>
\\</bean>
\\</beans>

applicationContext-persistence.xml

---

\\<bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">
\\<property name=\"driverClassName\" value=\"${database.driver}\"/>
\\<property name=\"url\" value=\"${database.url}\"/>
\\<property name=\"username\" value=\"${database.user}\"/>
\\<property name=\"password\" value=\"${database.password}\"/>
\\</bean>

Classes in test package:
Service.java

---

package test;
public class Service {

    private Dao dao;
    
    public void anyDBCall(){
    	dao.anyDBCall();
    }
    
    public void setDao(Dao dao) {
    	this.dao = dao;
    }

}

Dao.java

---

package test;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
public class Dao {
private JdbcTemplate jdbcTemplate;
public void anyDBCall(){
try{
System.out.println(\"CALL ?\");

                StringBuilder sb = new StringBuilder();
    	sb.append(\"Select MessageId from Message Where MessageId=1\"); //any query, for simplicity
    	jdbcTemplate.execute(sb.toString());
    
    	System.out.println(\"CALLED.\");
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

}

MyThread.java

---

package test;
public class MyThread implements Runnable{
private Service service;

    @Override
    public void run() {
    	while(true){
    		try {
    			service.anyDBCall();
    			
    			Thread.sleep(10*1000);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    public void setService(Service service) {
    	this.service = service;
    }

}

AppStarter.java

---

package test;
import java.util.Scanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class AppStarter {

    public AppStarter(){
    	String[] configFiles = new String[] {\"applicationContext.xml\", \"applicationContext-persistence.xml\"};
    	ApplicationContext context = new ClassPathXmlApplicationContext(configFiles);
    	
    	MyThread myThread = (MyThread)context.getBean(\"myThread\");
    	new Thread(myThread).start();
    	
    	//THIS PART IS CAUSING TO THE BUG
    	//IF YOU CLOSE THESE Scanner PART, EVERYTHING WORKS.
    	Scanner sc = new Scanner(System.in);
    	while(true){
    		String command = sc.nextLine();
    	}
    	
    }
    
    public static void main(String[] a){
    	new AppStarter();
    }

}

libraries:

---

commons-collections-3.2.jar
commons-dbcp.jar
commons-logging.jar
commons-pool.jar
jtds-1.2.jar
org.springframework.**********-3.0.0.RELEASE.jar  (spring 3.0 libraries)
spring-2.5.jar (spring 2.5 library)

using 3.0 libraries causes the program to hang on
\"jdbcTemplate.execute(sb.toString());\" line on Dao.
if i remove the Scanner lines in AppStarter.java, program does not stops.

using 2.5 library does not cause any thing wheather Scanner line is removed or not.

---

**Affects:** 3.0 GA

**Reference URL:** http://forum.springsource.org/showthread.php?t=84534
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11495","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11495/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11495/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11495/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11495","id":398102871,"node_id":"MDU6SXNzdWUzOTgxMDI4NzE=","number":11495,"title":"using Scanner for command reading after loading ApplicationContext, causes a bug in veriso 3.0 and later nightly builds [SPR-6829]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2010-02-12T00:26:07Z","updated_at":"2019-01-13T21:47:05Z","closed_at":"2018-12-26T10:37:25Z","author_association":"COLLABORATOR","body":"**[oner ekiz](https://jira.spring.io/secure/ViewProfile.jspa?name=ekizoner)** opened **[SPR-6829](https://jira.spring.io/browse/SPR-6829?redirect=false)** and commented\n\nusing Scanner for command reading after loading ApplicationContext, causes a bug in veriso 3.0 and later nightly builds. when i removed spring 3.0 libraries and include 2.5 verison instead, this bug does not occure.\n\nHere is my codes, to produce it\n\napplicationContext.xml\n\n---\n\n\\<bean id=\"myThread\" class=\"test.MyThread\">\n\\<property name=\"service\" ref=\"service\"/>\\</bean>\n\\<bean id=\"service\" class=\"test.Service\">\n\\<property name=\"dao\" ref=\"dao\"/>\n\\</bean>\n\\<bean id=\"dao\" class=\"test.Dao\">\n\\<property name=\"dataSource\" ref=\"dataSource\"/>\n\\</bean>\n\\</beans>\n\napplicationContext-persistence.xml\n\n---\n\n\\<bean id=\"dataSource\" class=\"org.apache.commons.dbcp.BasicDataSource\" destroy-method=\"close\">\n\\<property name=\"driverClassName\" value=\"${database.driver}\"/>\n\\<property name=\"url\" value=\"${database.url}\"/>\n\\<property name=\"username\" value=\"${database.user}\"/>\n\\<property name=\"password\" value=\"${database.password}\"/>\n\\</bean>\n\nClasses in test package:\nService.java\n\n---\n\npackage test;\npublic class Service {\n\n    private Dao dao;\n    \n    public void anyDBCall(){\n    \tdao.anyDBCall();\n    }\n    \n    public void setDao(Dao dao) {\n    \tthis.dao = dao;\n    }\n\n}\n\nDao.java\n\n---\n\npackage test;\n\nimport javax.sql.DataSource;\n\nimport org.springframework.jdbc.core.JdbcTemplate;\npublic class Dao {\nprivate JdbcTemplate jdbcTemplate;\npublic void anyDBCall(){\ntry{\nSystem.out.println(\"CALL ?\");\n\n                StringBuilder sb = new StringBuilder();\n    \tsb.append(\"Select MessageId from Message Where MessageId=1\"); //any query, for simplicity\n    \tjdbcTemplate.execute(sb.toString());\n    \n    \tSystem.out.println(\"CALLED.\");\n    \t}catch(Exception e){\n    \t\te.printStackTrace();\n    \t}\n    }\n    \n    public void setDataSource(DataSource dataSource) {\n        this.jdbcTemplate = new JdbcTemplate(dataSource);\n    }\n\n}\n\nMyThread.java\n\n---\n\npackage test;\npublic class MyThread implements Runnable{\nprivate Service service;\n\n    @Override\n    public void run() {\n    \twhile(true){\n    \t\ttry {\n    \t\t\tservice.anyDBCall();\n    \t\t\t\n    \t\t\tThread.sleep(10*1000);\n    \t\t} catch (Exception e) {\n    \t\t\te.printStackTrace();\n    \t\t}\n    \t}\n    }\n    \n    public void setService(Service service) {\n    \tthis.service = service;\n    }\n\n}\n\nAppStarter.java\n\n---\n\npackage test;\nimport java.util.Scanner;\nimport org.springframework.context.ApplicationContext;\nimport org.springframework.context.support.ClassPathXmlApplicationContext;\npublic class AppStarter {\n\n    public AppStarter(){\n    \tString[] configFiles = new String[] {\"applicationContext.xml\", \"applicationContext-persistence.xml\"};\n    \tApplicationContext context = new ClassPathXmlApplicationContext(configFiles);\n    \t\n    \tMyThread myThread = (MyThread)context.getBean(\"myThread\");\n    \tnew Thread(myThread).start();\n    \t\n    \t//THIS PART IS CAUSING TO THE BUG\n    \t//IF YOU CLOSE THESE Scanner PART, EVERYTHING WORKS.\n    \tScanner sc = new Scanner(System.in);\n    \twhile(true){\n    \t\tString command = sc.nextLine();\n    \t}\n    \t\n    }\n    \n    public static void main(String[] a){\n    \tnew AppStarter();\n    }\n\n}\n\nlibraries:\n\n---\n\ncommons-collections-3.2.jar\ncommons-dbcp.jar\ncommons-logging.jar\ncommons-pool.jar\njtds-1.2.jar\norg.springframework.**********-3.0.0.RELEASE.jar  (spring 3.0 libraries)\nspring-2.5.jar (spring 2.5 library)\n\nusing 3.0 libraries causes the program to hang on\n\"jdbcTemplate.execute(sb.toString());\" line on Dao.\nif i remove the Scanner lines in AppStarter.java, program does not stops.\n\nusing 2.5 library does not cause any thing wheather Scanner line is removed or not.\n\n---\n\n**Affects:** 3.0 GA\n\n**Reference URL:** http://forum.springsource.org/showthread.php?t=84534\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453348074"], "labels":["in: data","status: invalid"]}