{"id":"5547", "title":"MBeanExporter incorrectly handles AOP proxies [SPR-820]", "body":"**[Barry Kaplan](https://jira.spring.io/secure/ViewProfile.jspa?name=memelet)** opened **[SPR-820](https://jira.spring.io/browse/SPR-820?redirect=false)** and commented

In the method registerSimpleBean there is a check to see if the mbean to register is an aop-proxy:

    Class beanClass = (AopUtils.isAopProxy(bean) 
        ? bean.getClass().getSuperclass() : bean.getClass());

This check statement does not yield the target bean.

I have created around advice for a bean. The bean's class (in the above statement) is $Proxy9 containing a JdkDynamicAopProxy.

Here is the complete bean config (sorry, jira is going to mangle this  bad):

\\<bean id=\"autoProxyCreator\"
class=\"org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator\" />

\\<bean id=\"model.shutdownExceptionHandler\"
class=\"org.blackboxtrader.liquidityarbitrage.application.ModelShutdownExceptionHandler\">
\\<property name=\"model\">
\\<ref local=\"model\" />
\\</property>
\\</bean>

public class ModelShutdownExceptionHandler extends StaticMethodMatcherPointcutAdvisor
implements InitializingBean {

    private static final Logger log = Logger.getLogger(ModelShutdownExceptionHandler.class);
    
    private Model model;
    
    public void setModel(Model model) {
        this.model = model;
    }
    
    @Override
    public void setAdvice(Advice advice) {
        throw new UnsupportedOperationException();
    }
    
    public void afterPropertiesSet() throws Exception {
        if (model == null) {
            throw new IllegalArgumentException(\"Model not set\");
        }
        super.setAdvice(new ShutdownModelAdvice());
    }
    
    public boolean matches(Method method, Class targetClass) {
        return matchesEventListenerOnEvent(method, targetClass);
    }
    
    private boolean matchesEventListenerOnEvent(Method method, Class targetClass) {
        return EventListener.class.isAssignableFrom(targetClass)
                && method.getName().equals(\"onEvent\");
    }
    
    private class ShutdownModelAdvice implements MethodInterceptor {
    
        public Object invoke(MethodInvocation invocation) throws Throwable {
            try {
                invocation.proceed();
            } catch (Throwable t) {
                log.fatal(\"Unandled exception, closing model\", t);
                model.close();
            }
            return null;
        }
    }

}



---

**Affects:** 1.2 RC1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5547","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5547/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5547/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5547/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5547","id":398055958,"node_id":"MDU6SXNzdWUzOTgwNTU5NTg=","number":5547,"title":"MBeanExporter incorrectly handles AOP proxies [SPR-820]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/16","html_url":"https://github.com/spring-projects/spring-framework/milestone/16","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/16/labels","id":3960786,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Ng==","number":16,"title":"1.2 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":79,"state":"closed","created_at":"2019-01-10T22:02:10Z","updated_at":"2019-01-10T23:08:29Z","due_on":null,"closed_at":"2019-01-10T22:02:10Z"},"comments":2,"created_at":"2005-03-24T11:45:51Z","updated_at":"2005-03-24T17:38:44Z","closed_at":"2005-03-24T17:38:44Z","author_association":"COLLABORATOR","body":"**[Barry Kaplan](https://jira.spring.io/secure/ViewProfile.jspa?name=memelet)** opened **[SPR-820](https://jira.spring.io/browse/SPR-820?redirect=false)** and commented\n\nIn the method registerSimpleBean there is a check to see if the mbean to register is an aop-proxy:\n\n    Class beanClass = (AopUtils.isAopProxy(bean) \n        ? bean.getClass().getSuperclass() : bean.getClass());\n\nThis check statement does not yield the target bean.\n\nI have created around advice for a bean. The bean's class (in the above statement) is $Proxy9 containing a JdkDynamicAopProxy.\n\nHere is the complete bean config (sorry, jira is going to mangle this  bad):\n\n\\<bean id=\"autoProxyCreator\"\nclass=\"org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator\" />\n\n\\<bean id=\"model.shutdownExceptionHandler\"\nclass=\"org.blackboxtrader.liquidityarbitrage.application.ModelShutdownExceptionHandler\">\n\\<property name=\"model\">\n\\<ref local=\"model\" />\n\\</property>\n\\</bean>\n\npublic class ModelShutdownExceptionHandler extends StaticMethodMatcherPointcutAdvisor\nimplements InitializingBean {\n\n    private static final Logger log = Logger.getLogger(ModelShutdownExceptionHandler.class);\n    \n    private Model model;\n    \n    public void setModel(Model model) {\n        this.model = model;\n    }\n    \n    @Override\n    public void setAdvice(Advice advice) {\n        throw new UnsupportedOperationException();\n    }\n    \n    public void afterPropertiesSet() throws Exception {\n        if (model == null) {\n            throw new IllegalArgumentException(\"Model not set\");\n        }\n        super.setAdvice(new ShutdownModelAdvice());\n    }\n    \n    public boolean matches(Method method, Class targetClass) {\n        return matchesEventListenerOnEvent(method, targetClass);\n    }\n    \n    private boolean matchesEventListenerOnEvent(Method method, Class targetClass) {\n        return EventListener.class.isAssignableFrom(targetClass)\n                && method.getName().equals(\"onEvent\");\n    }\n    \n    private class ShutdownModelAdvice implements MethodInterceptor {\n    \n        public Object invoke(MethodInvocation invocation) throws Throwable {\n            try {\n                invocation.proceed();\n            } catch (Throwable t) {\n                log.fatal(\"Unandled exception, closing model\", t);\n                model.close();\n            }\n            return null;\n        }\n    }\n\n}\n\n\n\n---\n\n**Affects:** 1.2 RC1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453292948","453292949"], "labels":["in: core","type: bug"]}