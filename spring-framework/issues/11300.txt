{"id":"11300", "title":"Make CompletionService(s) work with TaskSchedulers [SPR-6634]", "body":"**[Joern Barthel](https://jira.spring.io/secure/ViewProfile.jspa?name=jbar)** opened **[SPR-6634](https://jira.spring.io/browse/SPR-6634?redirect=false)** and commented

This could be solved by using a simple decorator like so:

```
public class CompletionServiceTaskSchedulerDecorator<T> implements CompletionService<T>, TaskScheduler {

    private CompletionService<T> completionService;

    private Executor executor;
    private TaskScheduler taskScheduler;

    @PostConstruct
    public void initializeCompletionService() {
        completionService = new ExecutorCompletionService<T>(executor);
    }

    @Required
    public <T extends Executor & TaskScheduler> void setTaskScheduler(T taskScheduler) {
        this.executor = taskScheduler;
        this.taskScheduler = taskScheduler;
    }

    public Future<T> submit(Callable<T> task) {
        return completionService.submit(task);
    }

    public Future<T> submit(Runnable task, T result) {
        return completionService.submit(task, result);
    }

    public Future<T> take() throws InterruptedException {
        return completionService.take();
    }

    public Future<T> poll() {
        return completionService.poll();
    }

    public Future<T> poll(long timeout, TimeUnit unit) throws InterruptedException {
        return completionService.poll(timeout, unit);
    }

    public ScheduledFuture schedule(Runnable runnable, Trigger trigger) {
        return taskScheduler.schedule(delegate(runnable), trigger);
    }

    public ScheduledFuture schedule(Runnable runnable, Date date) {
        return taskScheduler.schedule(delegate(runnable), date);
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, Date date, long l) {
        return taskScheduler.scheduleAtFixedRate(delegate(runnable), date, l);
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long l) {
        return taskScheduler.scheduleAtFixedRate(delegate(runnable), l);
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, Date date, long l) {
        return taskScheduler.scheduleWithFixedDelay(delegate(runnable), date, l);
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long l) {
        return taskScheduler.scheduleWithFixedDelay(delegate(runnable), l);
    }

    protected Runnable delegate(final Runnable runnable) {

        return new Runnable() {

            @SuppressWarnings({\"unchecked\"})
            public void run() {
                completionService.submit(runnable, null);
            }

        };

    }

}
```

The dual use of the class as TaskScheduler and CompletionService is due to the chicken/egg problem of the latter requiring an Executor instance during construction. This could probably solved in a more obvious way but the one presented here works. One could use it in an application context like this:

```xml
<alias name=\"scheduler\" alias=\"schedulerCompletionService\"/>

<bean id=\"scheduler\" class=\"...CompletionServiceTaskSchedulerDecorator\">
	<property name=\"taskScheduler\">
    	<bean class=\"org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler\"
        	p:poolSize=\"100\">
        	<property name=\"errorHandler\">
        		<util:constant static-field=\"org.springframework.scheduling.support.TaskUtils.LOG_AND_PROPAGATE_ERROR_HANDLER\"/>
        	</property>
	</bean>
	</property>
</bean>
```



---

**Affects:** 3.0 GA
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11300","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11300/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11300/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11300/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11300","id":398101461,"node_id":"MDU6SXNzdWUzOTgxMDE0NjE=","number":11300,"title":"Make CompletionService(s) work with TaskSchedulers [SPR-6634]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2010-01-03T23:28:40Z","updated_at":"2015-09-22T17:34:32Z","closed_at":"2015-09-22T17:34:32Z","author_association":"COLLABORATOR","body":"**[Joern Barthel](https://jira.spring.io/secure/ViewProfile.jspa?name=jbar)** opened **[SPR-6634](https://jira.spring.io/browse/SPR-6634?redirect=false)** and commented\n\nThis could be solved by using a simple decorator like so:\n\n```\npublic class CompletionServiceTaskSchedulerDecorator<T> implements CompletionService<T>, TaskScheduler {\n\n    private CompletionService<T> completionService;\n\n    private Executor executor;\n    private TaskScheduler taskScheduler;\n\n    @PostConstruct\n    public void initializeCompletionService() {\n        completionService = new ExecutorCompletionService<T>(executor);\n    }\n\n    @Required\n    public <T extends Executor & TaskScheduler> void setTaskScheduler(T taskScheduler) {\n        this.executor = taskScheduler;\n        this.taskScheduler = taskScheduler;\n    }\n\n    public Future<T> submit(Callable<T> task) {\n        return completionService.submit(task);\n    }\n\n    public Future<T> submit(Runnable task, T result) {\n        return completionService.submit(task, result);\n    }\n\n    public Future<T> take() throws InterruptedException {\n        return completionService.take();\n    }\n\n    public Future<T> poll() {\n        return completionService.poll();\n    }\n\n    public Future<T> poll(long timeout, TimeUnit unit) throws InterruptedException {\n        return completionService.poll(timeout, unit);\n    }\n\n    public ScheduledFuture schedule(Runnable runnable, Trigger trigger) {\n        return taskScheduler.schedule(delegate(runnable), trigger);\n    }\n\n    public ScheduledFuture schedule(Runnable runnable, Date date) {\n        return taskScheduler.schedule(delegate(runnable), date);\n    }\n\n    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, Date date, long l) {\n        return taskScheduler.scheduleAtFixedRate(delegate(runnable), date, l);\n    }\n\n    public ScheduledFuture scheduleAtFixedRate(Runnable runnable, long l) {\n        return taskScheduler.scheduleAtFixedRate(delegate(runnable), l);\n    }\n\n    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, Date date, long l) {\n        return taskScheduler.scheduleWithFixedDelay(delegate(runnable), date, l);\n    }\n\n    public ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long l) {\n        return taskScheduler.scheduleWithFixedDelay(delegate(runnable), l);\n    }\n\n    protected Runnable delegate(final Runnable runnable) {\n\n        return new Runnable() {\n\n            @SuppressWarnings({\"unchecked\"})\n            public void run() {\n                completionService.submit(runnable, null);\n            }\n\n        };\n\n    }\n\n}\n```\n\nThe dual use of the class as TaskScheduler and CompletionService is due to the chicken/egg problem of the latter requiring an Executor instance during construction. This could probably solved in a more obvious way but the one presented here works. One could use it in an application context like this:\n\n```xml\n<alias name=\"scheduler\" alias=\"schedulerCompletionService\"/>\n\n<bean id=\"scheduler\" class=\"...CompletionServiceTaskSchedulerDecorator\">\n\t<property name=\"taskScheduler\">\n    \t<bean class=\"org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler\"\n        \tp:poolSize=\"100\">\n        \t<property name=\"errorHandler\">\n        \t\t<util:constant static-field=\"org.springframework.scheduling.support.TaskUtils.LOG_AND_PROPAGATE_ERROR_HANDLER\"/>\n        \t</property>\n\t</bean>\n\t</property>\n</bean>\n```\n\n\n\n---\n\n**Affects:** 3.0 GA\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453346241"], "labels":["in: core","status: declined","type: enhancement"]}