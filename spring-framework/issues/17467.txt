{"id":"17467", "title":" Inter-bean dependencies using @Component classes [SPR-12869]", "body":"**[Ciprian](https://jira.spring.io/secure/ViewProfile.jspa?name=ciprianspring)** opened **[SPR-12869](https://jira.spring.io/browse/SPR-12869?redirect=false)** and commented

I don't know if the documentation is wrong and hasn't been updated or if it's an actual bug.
In part 3 (Core technologies) of the documentation at 5.12 (Java-based container configuration) there is a paragraph in injecting inter-bean dependencies that states the following: \"...inter-bean dependencies only works when the `@Bean` method is declared within a `@Configuration` class. You cannot declare inter-bean dependencies using plain `@Component` classes.\"
Here is a simple example that shows that you can use inter-bean dependency inside `@Component` classes

public class App {
public static void main(String[] args) {
AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
applicationContext.register(SimpleComponent.class);
applicationContext.refresh();

        Movie movie = applicationContext.getBean(Movie.class);
        System.out.println(movie.getSongName());
    }
    
    @Component
    static class SimpleComponent {
        @Bean
        public Movie getMovie() {
            return new Movie(getSong());
        }
    
        @Bean
        public Song getSong() {
            return new Song(\"Popular band - Popular song\");
        }
    }
    
    static class Movie {
        private Song song;
    
        public Movie(Song song) {
            this.song = song;
        }
    
        public String getSongName() {
            return song.getName();
        }
    }
    
    static class Song {
        private String name;
    
        public Song(String name) {
            this.name = name;
        }
    
        public String getName() {
            return name;
        }
    }

}


---
No further details from [SPR-12869](https://jira.spring.io/browse/SPR-12869?redirect=false)", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17467","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17467/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17467/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17467/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17467","id":398178170,"node_id":"MDU6SXNzdWUzOTgxNzgxNzA=","number":17467,"title":" Inter-bean dependencies using @Component classes [SPR-12869]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":2,"created_at":"2015-03-31T04:06:37Z","updated_at":"2019-01-12T16:23:24Z","closed_at":"2015-03-31T07:08:38Z","author_association":"COLLABORATOR","body":"**[Ciprian](https://jira.spring.io/secure/ViewProfile.jspa?name=ciprianspring)** opened **[SPR-12869](https://jira.spring.io/browse/SPR-12869?redirect=false)** and commented\n\nI don't know if the documentation is wrong and hasn't been updated or if it's an actual bug.\nIn part 3 (Core technologies) of the documentation at 5.12 (Java-based container configuration) there is a paragraph in injecting inter-bean dependencies that states the following: \"...inter-bean dependencies only works when the `@Bean` method is declared within a `@Configuration` class. You cannot declare inter-bean dependencies using plain `@Component` classes.\"\nHere is a simple example that shows that you can use inter-bean dependency inside `@Component` classes\n\npublic class App {\npublic static void main(String[] args) {\nAnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();\napplicationContext.register(SimpleComponent.class);\napplicationContext.refresh();\n\n        Movie movie = applicationContext.getBean(Movie.class);\n        System.out.println(movie.getSongName());\n    }\n    \n    @Component\n    static class SimpleComponent {\n        @Bean\n        public Movie getMovie() {\n            return new Movie(getSong());\n        }\n    \n        @Bean\n        public Song getSong() {\n            return new Song(\"Popular band - Popular song\");\n        }\n    }\n    \n    static class Movie {\n        private Song song;\n    \n        public Movie(Song song) {\n            this.song = song;\n        }\n    \n        public String getSongName() {\n            return song.getName();\n        }\n    }\n    \n    static class Song {\n        private String name;\n    \n        public Song(String name) {\n            this.name = name;\n        }\n    \n        public String getName() {\n            return name;\n        }\n    }\n\n}\n\n\n---\nNo further details from [SPR-12869](https://jira.spring.io/browse/SPR-12869?redirect=false)","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453424156","453424158"], "labels":["in: core","status: declined"]}