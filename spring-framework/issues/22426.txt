{"id":"22426", "title":"Events extending from PayloadApplicationEvent and implementing an interface fail to match @EventListener argument", "body":"Spring version: 5.0.10.RELEASE

This bug is fully reproducable with the following classes/interfaces (omitted import/package lines):

```java
public interface Auditable {}

public class MyEventClass<GT> extends PayloadApplicationEvent<GT> implements Auditable {
    public MyEventClass(Object source, GT payload) {
        super(source, payload);
    }
    public String toString() {
        return \"Payload: \"+getPayload();
    }
}

@Component
public class Listener {
    @EventListener
    public void onEvent(Auditable a) {
        System.out.println(a);
    }
}

@Component
public class Publisher {
    private final ApplicationEventPublisher publisher;
    @Autowired
    public Publisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }
    public void publish(String text) {
        publisher.publishEvent(new MyEventClass<>(this, text));
    }
}

public class Tst {
    public static void main(String... args) {
        ApplicationContext ac = new AnnotationConfigApplicationContext(\"tst\");
        ac.getBean(Publisher.class).publish(\"xyz\");
    }
}
```
Expected behaviour:
When I run this application I expect the text `Payload: xyz` to appear

Actual behaviour:
I got an exception when running the program. The output is:
```
xxxx org.springframework.context.support.AbstractApplicationContext prepareRefresh
INFO: Refreshing org.springframework.context.annotation.AnnotationConfigApplicationContext@1e80bfe8: startup date [xxxx]; root of context hierarchy
Exception in thread \"main\" java.lang.IllegalStateException: argument type mismatch
HandlerMethod details:
Bean [tst.Listener]
Method [public void tst.Listener.onEvent(tst.Auditable)]
Resolved arguments:
[0] [type=java.lang.String] [value=text]

        at org.springframework.context.event.ApplicationListenerMethodAdapter.doInvoke(ApplicationListenerMethodAdapter.java:265)
        at org.springframework.context.event.ApplicationListenerMethodAdapter.processEvent(ApplicationListenerMethodAdapter.java:180)
        at org.springframework.context.event.ApplicationListenerMethodAdapter.onApplicationEvent(ApplicationListenerMethodAdapter.java:142)
        at org.springframework.context.event.SimpleApplicationEventMulticaster.doInvokeListener(SimpleApplicationEventMulticaster.java:172)
        at org.springframework.context.event.SimpleApplicationEventMulticaster.invokeListener(SimpleApplicationEventMulticaster.java:165)
        at org.springframework.context.event.SimpleApplicationEventMulticaster.multicastEvent(SimpleApplicationEventMulticaster.java:139)
        at org.springframework.context.support.AbstractApplicationContext.publishEvent(AbstractApplicationContext.java:400)
        at org.springframework.context.support.AbstractApplicationContext.publishEvent(AbstractApplicationContext.java:354)
        at tst.Publisher.publish(Publisher.java:17)
        at tst.Tst.main(Tst.java:10)
Caused by: java.lang.IllegalArgumentException: argument type mismatch
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
        at java.lang.reflect.Method.invoke(Unknown Source)
        at org.springframework.context.event.ApplicationListenerMethodAdapter.doInvoke(ApplicationListenerMethodAdapter.java:261)
        ... 9 more
```
The root cause of the problem is found in:
    `org.springframework.context.event.ApplicationListenerMethodAdapter::resolveArguments(ApplicationEvent event)`
Explanation: as the documentation states: this method resolves the arguments to pass to the event listener. It does do so using the following statement: (line 206..212)

	if ((eventClass == null || !ApplicationEvent.class.isAssignableFrom(eventClass)) &&
			event instanceof PayloadApplicationEvent) {
		return new Object[] {((PayloadApplicationEvent) event).getPayload()};
	}
	else {
		return new Object[] {event};
	}

In this application the relevant state:
Class<?> eventClass = the interface Auditable
ApplicationEvent event = the instance of MyEventClass with the String \"xyz\" as payload

Since the if condition holds the method returns the payload. But the payload is a String and not an Auditable and the actual call will have the wrong argument.

Possible solution:
The condition should fail when the event can be casted to the eventClass. (To keep it 'almost' backwards compatible one could also check that the eventclass is an interface)

For the time being there is also a workaround by rewriting the EventClass:
```java
public class MyEventClass<GT> extends ApplicationEvent implements Auditable, ResolvableTypeProvider {
    private final GT payload;
    public MyEventClass(Object source, GT payload) {
        super(source);
        this.payload = payload;
    }
    public String toString() {
        return \"Payload: \"+getPayload();
    }
    public GT getPayload() {
        return this.payload;
    }
    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(getPayload()));
    }
}
```
This works because:
- The ResolvableTypeProvider (implemented by PayloadApplicationEvent) is still implemented.
- The condition fails because the event no longer is an instance of the PayloadApplicationEvent
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22426","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22426/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22426/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22426/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22426","id":411443765,"node_id":"MDU6SXNzdWU0MTE0NDM3NjU=","number":22426,"title":"Events extending from PayloadApplicationEvent and implementing an interface fail to match @EventListener argument","user":{"login":"xfredk","id":35698219,"node_id":"MDQ6VXNlcjM1Njk4MjE5","avatar_url":"https://avatars2.githubusercontent.com/u/35698219?v=4","gravatar_id":"","url":"https://api.github.com/users/xfredk","html_url":"https://github.com/xfredk","followers_url":"https://api.github.com/users/xfredk/followers","following_url":"https://api.github.com/users/xfredk/following{/other_user}","gists_url":"https://api.github.com/users/xfredk/gists{/gist_id}","starred_url":"https://api.github.com/users/xfredk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/xfredk/subscriptions","organizations_url":"https://api.github.com/users/xfredk/orgs","repos_url":"https://api.github.com/users/xfredk/repos","events_url":"https://api.github.com/users/xfredk/events{/privacy}","received_events_url":"https://api.github.com/users/xfredk/received_events","type":"User","site_admin":false},"labels":[{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/210","html_url":"https://github.com/spring-projects/spring-framework/milestone/210","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/210/labels","id":4008678,"node_id":"MDk6TWlsZXN0b25lNDAwODY3OA==","number":210,"title":"5.1.6","description":"","creator":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":48,"state":"closed","created_at":"2019-01-28T17:08:14Z","updated_at":"2019-04-09T15:07:07Z","due_on":"2019-03-31T07:00:00Z","closed_at":"2019-03-31T13:18:44Z"},"comments":0,"created_at":"2019-02-18T12:04:20Z","updated_at":"2019-02-25T17:28:56Z","closed_at":"2019-02-25T16:59:20Z","author_association":"NONE","body":"Spring version: 5.0.10.RELEASE\r\n\r\nThis bug is fully reproducable with the following classes/interfaces (omitted import/package lines):\r\n\r\n```java\r\npublic interface Auditable {}\r\n\r\npublic class MyEventClass<GT> extends PayloadApplicationEvent<GT> implements Auditable {\r\n    public MyEventClass(Object source, GT payload) {\r\n        super(source, payload);\r\n    }\r\n    public String toString() {\r\n        return \"Payload: \"+getPayload();\r\n    }\r\n}\r\n\r\n@Component\r\npublic class Listener {\r\n    @EventListener\r\n    public void onEvent(Auditable a) {\r\n        System.out.println(a);\r\n    }\r\n}\r\n\r\n@Component\r\npublic class Publisher {\r\n    private final ApplicationEventPublisher publisher;\r\n    @Autowired\r\n    public Publisher(ApplicationEventPublisher publisher) {\r\n        this.publisher = publisher;\r\n    }\r\n    public void publish(String text) {\r\n        publisher.publishEvent(new MyEventClass<>(this, text));\r\n    }\r\n}\r\n\r\npublic class Tst {\r\n    public static void main(String... args) {\r\n        ApplicationContext ac = new AnnotationConfigApplicationContext(\"tst\");\r\n        ac.getBean(Publisher.class).publish(\"xyz\");\r\n    }\r\n}\r\n```\r\nExpected behaviour:\r\nWhen I run this application I expect the text `Payload: xyz` to appear\r\n\r\nActual behaviour:\r\nI got an exception when running the program. The output is:\r\n```\r\nxxxx org.springframework.context.support.AbstractApplicationContext prepareRefresh\r\nINFO: Refreshing org.springframework.context.annotation.AnnotationConfigApplicationContext@1e80bfe8: startup date [xxxx]; root of context hierarchy\r\nException in thread \"main\" java.lang.IllegalStateException: argument type mismatch\r\nHandlerMethod details:\r\nBean [tst.Listener]\r\nMethod [public void tst.Listener.onEvent(tst.Auditable)]\r\nResolved arguments:\r\n[0] [type=java.lang.String] [value=text]\r\n\r\n        at org.springframework.context.event.ApplicationListenerMethodAdapter.doInvoke(ApplicationListenerMethodAdapter.java:265)\r\n        at org.springframework.context.event.ApplicationListenerMethodAdapter.processEvent(ApplicationListenerMethodAdapter.java:180)\r\n        at org.springframework.context.event.ApplicationListenerMethodAdapter.onApplicationEvent(ApplicationListenerMethodAdapter.java:142)\r\n        at org.springframework.context.event.SimpleApplicationEventMulticaster.doInvokeListener(SimpleApplicationEventMulticaster.java:172)\r\n        at org.springframework.context.event.SimpleApplicationEventMulticaster.invokeListener(SimpleApplicationEventMulticaster.java:165)\r\n        at org.springframework.context.event.SimpleApplicationEventMulticaster.multicastEvent(SimpleApplicationEventMulticaster.java:139)\r\n        at org.springframework.context.support.AbstractApplicationContext.publishEvent(AbstractApplicationContext.java:400)\r\n        at org.springframework.context.support.AbstractApplicationContext.publishEvent(AbstractApplicationContext.java:354)\r\n        at tst.Publisher.publish(Publisher.java:17)\r\n        at tst.Tst.main(Tst.java:10)\r\nCaused by: java.lang.IllegalArgumentException: argument type mismatch\r\n        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n        at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n        at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n        at java.lang.reflect.Method.invoke(Unknown Source)\r\n        at org.springframework.context.event.ApplicationListenerMethodAdapter.doInvoke(ApplicationListenerMethodAdapter.java:261)\r\n        ... 9 more\r\n```\r\nThe root cause of the problem is found in:\r\n    `org.springframework.context.event.ApplicationListenerMethodAdapter::resolveArguments(ApplicationEvent event)`\r\nExplanation: as the documentation states: this method resolves the arguments to pass to the event listener. It does do so using the following statement: (line 206..212)\r\n\r\n\tif ((eventClass == null || !ApplicationEvent.class.isAssignableFrom(eventClass)) &&\r\n\t\t\tevent instanceof PayloadApplicationEvent) {\r\n\t\treturn new Object[] {((PayloadApplicationEvent) event).getPayload()};\r\n\t}\r\n\telse {\r\n\t\treturn new Object[] {event};\r\n\t}\r\n\r\nIn this application the relevant state:\r\nClass<?> eventClass = the interface Auditable\r\nApplicationEvent event = the instance of MyEventClass with the String \"xyz\" as payload\r\n\r\nSince the if condition holds the method returns the payload. But the payload is a String and not an Auditable and the actual call will have the wrong argument.\r\n\r\nPossible solution:\r\nThe condition should fail when the event can be casted to the eventClass. (To keep it 'almost' backwards compatible one could also check that the eventclass is an interface)\r\n\r\nFor the time being there is also a workaround by rewriting the EventClass:\r\n```java\r\npublic class MyEventClass<GT> extends ApplicationEvent implements Auditable, ResolvableTypeProvider {\r\n    private final GT payload;\r\n    public MyEventClass(Object source, GT payload) {\r\n        super(source);\r\n        this.payload = payload;\r\n    }\r\n    public String toString() {\r\n        return \"Payload: \"+getPayload();\r\n    }\r\n    public GT getPayload() {\r\n        return this.payload;\r\n    }\r\n    @Override\r\n    public ResolvableType getResolvableType() {\r\n        return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(getPayload()));\r\n    }\r\n}\r\n```\r\nThis works because:\r\n- The ResolvableTypeProvider (implemented by PayloadApplicationEvent) is still implemented.\r\n- The condition fails because the event no longer is an instance of the PayloadApplicationEvent\r\n","closed_by":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["status: backported","type: bug"]}