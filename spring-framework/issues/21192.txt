{"id":"21192", "title":"Revise StringUtils.parseLocale(String) for proper handling of corner cases [SPR-16651]", "body":"**[Michael Decker](https://jira.spring.io/secure/ViewProfile.jspa?name=kenori)** opened **[SPR-16651](https://jira.spring.io/browse/SPR-16651?redirect=false)** and commented

We are using Spring Boot 2.0.0.RELEASE (jdk1.8.0_144, Windows 10)

We had to realize, that `org.springframework.util.StringUtils.parseLocaleString(String)` does not return always the same instance, as it was created from:

```
from '' (und) to 'null (null)'
from 'sr_BA_#Latn' (sr-Latn-BA) to 'sr_BA_#Latn (sr-BA)'
from 'ja_JP_JP_#u-ca-japanese' (ja-JP-u-ca-japanese-x-lvariant-JP) to 'ja_JP_JP_#u-ca-japanese (ja-JP-x-lvariant-JP)'
from 'sr_ME_#Latn' (sr-Latn-ME) to 'sr_ME_#Latn (sr-ME)'
from 'sr__#Latn' (sr-Latn) to 'sr_#LATN (sr)'
from 'th_TH_TH_#u-nu-thai' (th-TH-u-nu-thai-x-lvariant-TH) to 'th_TH_TH_#u-nu-thai (th-TH-x-lvariant-TH)'
from 'sr_RS_#Latn' (sr-Latn-RS) to 'sr_RS_#Latn (sr-RS)'
```

The test is:

```java

import static org.junit.Assert.assertEquals;
import static org.springframework.util.StringUtils.parseLocale;
import static org.springframework.util.StringUtils.parseLocaleString;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class SpringStringUtilsParseLocaleTest {

  @Test
  public void testParseLocale() {
    List<String> failing = Stream.of(Locale.getAvailableLocales()) //
        .filter(locale -> !locale.equals(parseLocale(locale.toString()))) //
        .map(locale -> String.format(\"from '%s' (%s) to '%s (%s)'\", locale, toLanguageTag(locale),
            parseLocale(locale.toString()), toLanguageTag(parseLocale(locale.toString())))) //
        .collect(Collectors.toList());

    // assertEquals(Collections.emptyList(), failing);
    assertEquals(\"\", failing.stream().collect(Collectors.joining(\"\\n\")));
  }

  @Test
  public void testParseLocaleString() {
    List<String> failing = Stream.of(Locale.getAvailableLocales()) //
        .filter(locale -> !locale.equals(parseLocaleString(locale.toString()))) //
        .map(locale -> String.format(\"from '%s' (%s) to '%s (%s)'\", locale, toLanguageTag(locale),
            parseLocaleString(locale.toString()), toLanguageTag(parseLocaleString(locale.toString())))) //
        .collect(Collectors.toList());

    // assertEquals(Collections.emptyList(), failing);
    assertEquals(\"\", failing.stream().collect(Collectors.joining(\"\\n\")));
  }

  private static String toLanguageTag(Locale locale) {
    return locale == null ? null : locale.toLanguageTag();
  }

}
```

---

**Affects:** 5.0.4

**Issue Links:**
- #19283 StringUtils.parseLocaleString(): does not parse locale string in java 7 for Serbian (Latin)
- #11228 StringUtils#parseLocaleString(String) does not work for empty locale
- #12254 StringUtils#parseLocaleString(String) with Variant when no Country
- #20736 StringToLocaleConverter should allow BCP 47 values
- #21241 CookieLocaleResolver is not RFC6265 compliant when setting a locale and time zone

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/cab35aa788a1edbd647b4a8bd6e798524838f67e, https://github.com/spring-projects/spring-framework/commit/55563c16b5744288c0cb73fde0a07ba9e4d97e09, https://github.com/spring-projects/spring-framework/commit/88e4006790dccbabc0ee4448a0b70c6912584818
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21192","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21192/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21192/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/21192/events","html_url":"https://github.com/spring-projects/spring-framework/issues/21192","id":398225149,"node_id":"MDU6SXNzdWUzOTgyMjUxNDk=","number":21192,"title":"Revise StringUtils.parseLocale(String) for proper handling of corner cases [SPR-16651]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/189","html_url":"https://github.com/spring-projects/spring-framework/milestone/189","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/189/labels","id":3960962,"node_id":"MDk6TWlsZXN0b25lMzk2MDk2Mg==","number":189,"title":"5.0.8","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":40,"state":"closed","created_at":"2019-01-10T22:05:38Z","updated_at":"2019-01-11T10:39:47Z","due_on":"2018-07-25T07:00:00Z","closed_at":"2019-01-10T22:05:38Z"},"comments":3,"created_at":"2018-03-28T14:11:49Z","updated_at":"2019-01-11T15:07:40Z","closed_at":"2018-07-26T08:09:36Z","author_association":"COLLABORATOR","body":"**[Michael Decker](https://jira.spring.io/secure/ViewProfile.jspa?name=kenori)** opened **[SPR-16651](https://jira.spring.io/browse/SPR-16651?redirect=false)** and commented\n\nWe are using Spring Boot 2.0.0.RELEASE (jdk1.8.0_144, Windows 10)\n\nWe had to realize, that `org.springframework.util.StringUtils.parseLocaleString(String)` does not return always the same instance, as it was created from:\n\n```\nfrom '' (und) to 'null (null)'\nfrom 'sr_BA_#Latn' (sr-Latn-BA) to 'sr_BA_#Latn (sr-BA)'\nfrom 'ja_JP_JP_#u-ca-japanese' (ja-JP-u-ca-japanese-x-lvariant-JP) to 'ja_JP_JP_#u-ca-japanese (ja-JP-x-lvariant-JP)'\nfrom 'sr_ME_#Latn' (sr-Latn-ME) to 'sr_ME_#Latn (sr-ME)'\nfrom 'sr__#Latn' (sr-Latn) to 'sr_#LATN (sr)'\nfrom 'th_TH_TH_#u-nu-thai' (th-TH-u-nu-thai-x-lvariant-TH) to 'th_TH_TH_#u-nu-thai (th-TH-x-lvariant-TH)'\nfrom 'sr_RS_#Latn' (sr-Latn-RS) to 'sr_RS_#Latn (sr-RS)'\n```\n\nThe test is:\n\n```java\n\nimport static org.junit.Assert.assertEquals;\nimport static org.springframework.util.StringUtils.parseLocale;\nimport static org.springframework.util.StringUtils.parseLocaleString;\n\nimport java.util.List;\nimport java.util.Locale;\nimport java.util.stream.Collectors;\nimport java.util.stream.Stream;\n\nimport org.junit.Test;\n\npublic class SpringStringUtilsParseLocaleTest {\n\n  @Test\n  public void testParseLocale() {\n    List<String> failing = Stream.of(Locale.getAvailableLocales()) //\n        .filter(locale -> !locale.equals(parseLocale(locale.toString()))) //\n        .map(locale -> String.format(\"from '%s' (%s) to '%s (%s)'\", locale, toLanguageTag(locale),\n            parseLocale(locale.toString()), toLanguageTag(parseLocale(locale.toString())))) //\n        .collect(Collectors.toList());\n\n    // assertEquals(Collections.emptyList(), failing);\n    assertEquals(\"\", failing.stream().collect(Collectors.joining(\"\\n\")));\n  }\n\n  @Test\n  public void testParseLocaleString() {\n    List<String> failing = Stream.of(Locale.getAvailableLocales()) //\n        .filter(locale -> !locale.equals(parseLocaleString(locale.toString()))) //\n        .map(locale -> String.format(\"from '%s' (%s) to '%s (%s)'\", locale, toLanguageTag(locale),\n            parseLocaleString(locale.toString()), toLanguageTag(parseLocaleString(locale.toString())))) //\n        .collect(Collectors.toList());\n\n    // assertEquals(Collections.emptyList(), failing);\n    assertEquals(\"\", failing.stream().collect(Collectors.joining(\"\\n\")));\n  }\n\n  private static String toLanguageTag(Locale locale) {\n    return locale == null ? null : locale.toLanguageTag();\n  }\n\n}\n```\n\n---\n\n**Affects:** 5.0.4\n\n**Issue Links:**\n- #19283 StringUtils.parseLocaleString(): does not parse locale string in java 7 for Serbian (Latin)\n- #11228 StringUtils#parseLocaleString(String) does not work for empty locale\n- #12254 StringUtils#parseLocaleString(String) with Variant when no Country\n- #20736 StringToLocaleConverter should allow BCP 47 values\n- #21241 CookieLocaleResolver is not RFC6265 compliant when setting a locale and time zone\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/cab35aa788a1edbd647b4a8bd6e798524838f67e, https://github.com/spring-projects/spring-framework/commit/55563c16b5744288c0cb73fde0a07ba9e4d97e09, https://github.com/spring-projects/spring-framework/commit/88e4006790dccbabc0ee4448a0b70c6912584818\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453469553","453469555","453469557"], "labels":["in: core","type: enhancement"]}