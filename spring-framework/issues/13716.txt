{"id":"13716", "title":"Provide more convenient way to load Properties into a JavaConfig configuration [SPR-9078]", "body":"**[Oliver Drotbohm](https://jira.spring.io/secure/ViewProfile.jspa?name=olivergierke)** opened **[SPR-9078](https://jira.spring.io/browse/SPR-9078?redirect=false)** and commented

Currently `@PropertySource` allows loading property files into the `ApplicationContext`'s environment in a very convenient manner:

```java
@Configuration
@PropertySource(\"classpath:sample.properties\")
public class Config {
  
  @Autowired
  Environment environment;

  @Bean
  public Object doSomething() {
    environment.getProperty(...);
  }
}
```

However, if you'd rather like to load the properties into a dedicated `Properties` instance one has to use a `PropertiesFactoryBean` like this:

```java
@Configuration
public class Config {

  @Bean
  public Properties properties() {
    PropertiesFactoryBean factory = new PropertiesFactoryBean();
    factory.setLocation(new ClasspathResource(\"sample.properties\");
    factory.afterPropertiesSet();
    return factory.getObject();
  }
}
```

which is quite a lot of ceremony. Would be cool if one could just shortcut it to something like this:

```java
@Configuration
public class Config {

  @PropertySource(\"classpath:sample.properties\")
  Properties properties;
}
```



---

**Affects:** 3.1 GA

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/078a1c5db84467a52e9de4078d139ee6fded08c7

1 votes, 2 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13716","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13716/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13716/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13716/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13716","id":398116974,"node_id":"MDU6SXNzdWUzOTgxMTY5NzQ=","number":13716,"title":"Provide more convenient way to load Properties into a JavaConfig configuration [SPR-9078]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2012-01-31T03:45:49Z","updated_at":"2013-01-24T05:05:06Z","closed_at":"2013-01-24T05:05:06Z","author_association":"COLLABORATOR","body":"**[Oliver Drotbohm](https://jira.spring.io/secure/ViewProfile.jspa?name=olivergierke)** opened **[SPR-9078](https://jira.spring.io/browse/SPR-9078?redirect=false)** and commented\n\nCurrently `@PropertySource` allows loading property files into the `ApplicationContext`'s environment in a very convenient manner:\n\n```java\n@Configuration\n@PropertySource(\"classpath:sample.properties\")\npublic class Config {\n  \n  @Autowired\n  Environment environment;\n\n  @Bean\n  public Object doSomething() {\n    environment.getProperty(...);\n  }\n}\n```\n\nHowever, if you'd rather like to load the properties into a dedicated `Properties` instance one has to use a `PropertiesFactoryBean` like this:\n\n```java\n@Configuration\npublic class Config {\n\n  @Bean\n  public Properties properties() {\n    PropertiesFactoryBean factory = new PropertiesFactoryBean();\n    factory.setLocation(new ClasspathResource(\"sample.properties\");\n    factory.afterPropertiesSet();\n    return factory.getObject();\n  }\n}\n```\n\nwhich is quite a lot of ceremony. Would be cool if one could just shortcut it to something like this:\n\n```java\n@Configuration\npublic class Config {\n\n  @PropertySource(\"classpath:sample.properties\")\n  Properties properties;\n}\n```\n\n\n\n---\n\n**Affects:** 3.1 GA\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/078a1c5db84467a52e9de4078d139ee6fded08c7\n\n1 votes, 2 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453365197","453365198"], "labels":["in: core","status: declined","type: enhancement"]}