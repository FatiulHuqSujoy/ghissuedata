{"id":"11810", "title":"Add factory-class and factory-method to context:component-scan [SPR-7151]", "body":"**[Neale Upstone](https://jira.spring.io/secure/ViewProfile.jspa?name=nealeu)** opened **[SPR-7151](https://jira.spring.io/browse/SPR-7151?redirect=false)** and commented

I would like to be able to create proxied objects for autowiring by type using component-scan:

An example with mocks would be:

```xml
<context:component-scan 
    base-package=\"org.my.services\"
    factory-class=\"org.easymock.EasyMock\" 
    factory-method=\"createMock\"
    qualifier=\"mock\"
/>
```

instead of the following for each service bean:

```xml
<bean name=\"funService\" class=\"org.easymock.EasyMock\" factory-method=\"createMock\"  >
	<qualifier value=\"mock\"/>
	<constructor-arg value=\"org.my.service.FunService\" />
</bean>
```



---

**Affects:** 3.0.2
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11810","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11810/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11810/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11810/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11810","id":398104878,"node_id":"MDU6SXNzdWUzOTgxMDQ4Nzg=","number":11810,"title":"Add factory-class and factory-method to context:component-scan [SPR-7151]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2010-04-29T03:29:38Z","updated_at":"2018-12-28T11:26:20Z","closed_at":"2018-12-28T11:26:20Z","author_association":"COLLABORATOR","body":"**[Neale Upstone](https://jira.spring.io/secure/ViewProfile.jspa?name=nealeu)** opened **[SPR-7151](https://jira.spring.io/browse/SPR-7151?redirect=false)** and commented\n\nI would like to be able to create proxied objects for autowiring by type using component-scan:\n\nAn example with mocks would be:\n\n```xml\n<context:component-scan \n    base-package=\"org.my.services\"\n    factory-class=\"org.easymock.EasyMock\" \n    factory-method=\"createMock\"\n    qualifier=\"mock\"\n/>\n```\n\ninstead of the following for each service bean:\n\n```xml\n<bean name=\"funService\" class=\"org.easymock.EasyMock\" factory-method=\"createMock\"  >\n\t<qualifier value=\"mock\"/>\n\t<constructor-arg value=\"org.my.service.FunService\" />\n</bean>\n```\n\n\n\n---\n\n**Affects:** 3.0.2\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453350551","453350552"], "labels":["in: core","status: declined","type: enhancement"]}