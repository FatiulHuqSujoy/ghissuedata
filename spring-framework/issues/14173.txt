{"id":"14173", "title":"Bridge methods in @Configuration classes \"randomly\" leads to infinite recursion during context [SPR-9539]", "body":"**[Marcus Schulte](https://jira.spring.io/secure/ViewProfile.jspa?name=mschulte)** opened **[SPR-9539](https://jira.spring.io/browse/SPR-9539?redirect=false)** and commented

The following code runs fine on some JRE versions, but regularly (not always) fails on others, among them JDK 1.7.0_05.
It works always fine iff the covariant override of `Number x()` is avoided.

```
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class BugTest {
    @Autowired
    Number z;

    @Test
    public void doNothingInParticular() {
        Assert.assertThat( z, CoreMatchers.<Number>equalTo(new Long(13)));
    }

    public interface I {
        Number x();
        String y();
    }

    @Configuration
    public static class C implements I {
        @Override @Bean
        public Long x() {
            return 13L;
        }

        @Override @Bean
        public String y() {
            return \"17\";
        }
    }
}
```

I've traced it down to an apparent failure of cglib's `MethodProxy.invokeSuper()` method. It seems to randomly mix-up indexes of methods it is using in the FastClass thingy whenever bridge-methods are involved.
I've tried different versions of cglib, including 2.2.2.


---

**Affects:** 3.1.1

**Attachments:**
- [threads_report.txt](https://jira.spring.io/secure/attachment/19989/threads_report.txt) (_683.25 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14173","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14173/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14173/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14173/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14173","id":398151453,"node_id":"MDU6SXNzdWUzOTgxNTE0NTM=","number":14173,"title":"Bridge methods in @Configuration classes \"randomly\" leads to infinite recursion during context [SPR-9539]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2012-06-24T12:47:44Z","updated_at":"2019-01-12T05:28:30Z","closed_at":"2018-12-27T12:33:02Z","author_association":"COLLABORATOR","body":"**[Marcus Schulte](https://jira.spring.io/secure/ViewProfile.jspa?name=mschulte)** opened **[SPR-9539](https://jira.spring.io/browse/SPR-9539?redirect=false)** and commented\n\nThe following code runs fine on some JRE versions, but regularly (not always) fails on others, among them JDK 1.7.0_05.\nIt works always fine iff the covariant override of `Number x()` is avoided.\n\n```\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(loader = AnnotationConfigContextLoader.class)\npublic class BugTest {\n    @Autowired\n    Number z;\n\n    @Test\n    public void doNothingInParticular() {\n        Assert.assertThat( z, CoreMatchers.<Number>equalTo(new Long(13)));\n    }\n\n    public interface I {\n        Number x();\n        String y();\n    }\n\n    @Configuration\n    public static class C implements I {\n        @Override @Bean\n        public Long x() {\n            return 13L;\n        }\n\n        @Override @Bean\n        public String y() {\n            return \"17\";\n        }\n    }\n}\n```\n\nI've traced it down to an apparent failure of cglib's `MethodProxy.invokeSuper()` method. It seems to randomly mix-up indexes of methods it is using in the FastClass thingy whenever bridge-methods are involved.\nI've tried different versions of cglib, including 2.2.2.\n\n\n---\n\n**Affects:** 3.1.1\n\n**Attachments:**\n- [threads_report.txt](https://jira.spring.io/secure/attachment/19989/threads_report.txt) (_683.25 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453395128"], "labels":["in: core","status: invalid"]}