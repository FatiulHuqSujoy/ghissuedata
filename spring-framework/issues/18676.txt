{"id":"18676", "title":"Make localized field names lookup extensible in JSR-303 validation messages [SPR-14104]", "body":"**[Bertrand Guay-Paquet](https://jira.spring.io/secure/ViewProfile.jspa?name=berniegp)** opened **[SPR-14104](https://jira.spring.io/browse/SPR-14104?redirect=false)** and commented

When using JSR-303 annotations for validation, Spring looks up error messages in message bundles and makes available the field name as well as the annotation attribute values in alphabetical order. These arguments are determined by LocalValidatorFactoryBean via its superclass SpringValidatorAdapter with this cose:

```java
protected Object[] getArgumentsForConstraint(String objectName, String field, ConstraintDescriptor<?> descriptor) {
	List<Object> arguments = new LinkedList<Object>();
       // HERE are established the message keys for resolving the field name
	String[] codes = new String[] {objectName + Errors.NESTED_PATH_SEPARATOR + field, field};
	arguments.add(new DefaultMessageSourceResolvable(codes, field));
	// Using a TreeMap for alphabetical ordering of attribute names
	Map<String, Object> attributesToExpose = new TreeMap<String, Object>();
	for (Map.Entry<String, Object> entry : descriptor.getAttributes().entrySet()) {
		String attributeName = entry.getKey();
		Object attributeValue = entry.getValue();
		if (!internalAnnotationAttributes.contains(attributeName)) {
			attributesToExpose.put(attributeName, attributeValue);
		}
	}
	arguments.addAll(attributesToExpose.values());
	return arguments.toArray(new Object[arguments.size()]);
}
```

Currently, for a field named \"fooField\" of the class \"barClass\", the message keys would be [\"barClass.fooField\", \"fooField\"]. I believe this was implemented in #11073.

This can then be used in a message.properties file (for a `@Size` annotation) like so:

```
Size={0} must be between {1} and {2}
```

where {0} is either the localized field name (using the 2 keys described above) or the field name itself as a fallback.

This works great, but does not allow for using \"namespaced\" common field names in messages.properties like so:

```
labels.firstName=First Name
labels.lastName=Last Name
labels.etc=...
```

I coded this custom validator factory bean which achieves this:

```java
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public Validator getValidator() {
		CustomValidator factory = new CustomValidator();
		return factory;
	}

	private static class CustomValidator extends OptionalValidatorFactoryBean {

		private static final String FIELD_NAME_PREFIX = \"labels\";
		
		@Override
		protected Object[] getArgumentsForConstraint(String objectName, String field,
				ConstraintDescriptor<?> descriptor) {
			Object[] arguments = super.getArgumentsForConstraint(objectName, field, descriptor);
			
			// Add a custom message code for the field name
			if (arguments.length > 0 && arguments[0] instanceof DefaultMessageSourceResolvable) {
				DefaultMessageSourceResolvable fieldArgument = (DefaultMessageSourceResolvable) arguments[0];
				String[] codes = fieldArgument.getCodes();
				String[] extendedCodes = new String[codes.length + 1];
				extendedCodes[0] = FIELD_NAME_PREFIX + Errors.NESTED_PATH_SEPARATOR + field;
				System.arraycopy(codes, 0, extendedCodes, 1, codes.length);
				arguments[0] = new DefaultMessageSourceResolvable(extendedCodes, field);
			}
			return arguments;
		}
	}
}
```

Now, for the feature request: I think it would be useful to externalize the choice of field name message codes to a protected method which could be overwritten by child classes. In this way, the implementation would be much cleaner and more robust.

To be clear, I'm proposing to change this line in SpringValidatorAdapter :

```
String[] codes = new String[] {objectName + Errors.NESTED_PATH_SEPARATOR + field, field};
```

to something like :

```
String[] codes = getFieldCodes(objectName, field);
```

---

**Affects:** 4.2.5

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/696dcb72a5089642367ddf73fb34a231a9abd68a
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18676","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18676/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18676/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18676/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18676","id":398191479,"node_id":"MDU6SXNzdWUzOTgxOTE0Nzk=","number":18676,"title":"Make localized field names lookup extensible in JSR-303 validation messages [SPR-14104]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/147","html_url":"https://github.com/spring-projects/spring-framework/milestone/147","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/147/labels","id":3960920,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyMA==","number":147,"title":"4.3 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":179,"state":"closed","created_at":"2019-01-10T22:04:48Z","updated_at":"2019-01-11T08:58:51Z","due_on":"2016-04-05T07:00:00Z","closed_at":"2019-01-10T22:04:48Z"},"comments":4,"created_at":"2016-03-31T21:41:44Z","updated_at":"2019-06-25T08:23:45Z","closed_at":"2016-04-06T08:38:10Z","author_association":"COLLABORATOR","body":"**[Bertrand Guay-Paquet](https://jira.spring.io/secure/ViewProfile.jspa?name=berniegp)** opened **[SPR-14104](https://jira.spring.io/browse/SPR-14104?redirect=false)** and commented\n\nWhen using JSR-303 annotations for validation, Spring looks up error messages in message bundles and makes available the field name as well as the annotation attribute values in alphabetical order. These arguments are determined by LocalValidatorFactoryBean via its superclass SpringValidatorAdapter with this cose:\n\n```java\nprotected Object[] getArgumentsForConstraint(String objectName, String field, ConstraintDescriptor<?> descriptor) {\n\tList<Object> arguments = new LinkedList<Object>();\n       // HERE are established the message keys for resolving the field name\n\tString[] codes = new String[] {objectName + Errors.NESTED_PATH_SEPARATOR + field, field};\n\targuments.add(new DefaultMessageSourceResolvable(codes, field));\n\t// Using a TreeMap for alphabetical ordering of attribute names\n\tMap<String, Object> attributesToExpose = new TreeMap<String, Object>();\n\tfor (Map.Entry<String, Object> entry : descriptor.getAttributes().entrySet()) {\n\t\tString attributeName = entry.getKey();\n\t\tObject attributeValue = entry.getValue();\n\t\tif (!internalAnnotationAttributes.contains(attributeName)) {\n\t\t\tattributesToExpose.put(attributeName, attributeValue);\n\t\t}\n\t}\n\targuments.addAll(attributesToExpose.values());\n\treturn arguments.toArray(new Object[arguments.size()]);\n}\n```\n\nCurrently, for a field named \"fooField\" of the class \"barClass\", the message keys would be [\"barClass.fooField\", \"fooField\"]. I believe this was implemented in #11073.\n\nThis can then be used in a message.properties file (for a `@Size` annotation) like so:\n\n```\nSize={0} must be between {1} and {2}\n```\n\nwhere {0} is either the localized field name (using the 2 keys described above) or the field name itself as a fallback.\n\nThis works great, but does not allow for using \"namespaced\" common field names in messages.properties like so:\n\n```\nlabels.firstName=First Name\nlabels.lastName=Last Name\nlabels.etc=...\n```\n\nI coded this custom validator factory bean which achieves this:\n\n```java\n@Configuration\npublic class WebConfig extends WebMvcConfigurerAdapter {\n\n\t@Override\n\tpublic Validator getValidator() {\n\t\tCustomValidator factory = new CustomValidator();\n\t\treturn factory;\n\t}\n\n\tprivate static class CustomValidator extends OptionalValidatorFactoryBean {\n\n\t\tprivate static final String FIELD_NAME_PREFIX = \"labels\";\n\t\t\n\t\t@Override\n\t\tprotected Object[] getArgumentsForConstraint(String objectName, String field,\n\t\t\t\tConstraintDescriptor<?> descriptor) {\n\t\t\tObject[] arguments = super.getArgumentsForConstraint(objectName, field, descriptor);\n\t\t\t\n\t\t\t// Add a custom message code for the field name\n\t\t\tif (arguments.length > 0 && arguments[0] instanceof DefaultMessageSourceResolvable) {\n\t\t\t\tDefaultMessageSourceResolvable fieldArgument = (DefaultMessageSourceResolvable) arguments[0];\n\t\t\t\tString[] codes = fieldArgument.getCodes();\n\t\t\t\tString[] extendedCodes = new String[codes.length + 1];\n\t\t\t\textendedCodes[0] = FIELD_NAME_PREFIX + Errors.NESTED_PATH_SEPARATOR + field;\n\t\t\t\tSystem.arraycopy(codes, 0, extendedCodes, 1, codes.length);\n\t\t\t\targuments[0] = new DefaultMessageSourceResolvable(extendedCodes, field);\n\t\t\t}\n\t\t\treturn arguments;\n\t\t}\n\t}\n}\n```\n\nNow, for the feature request: I think it would be useful to externalize the choice of field name message codes to a protected method which could be overwritten by child classes. In this way, the implementation would be much cleaner and more robust.\n\nTo be clear, I'm proposing to change this line in SpringValidatorAdapter :\n\n```\nString[] codes = new String[] {objectName + Errors.NESTED_PATH_SEPARATOR + field, field};\n```\n\nto something like :\n\n```\nString[] codes = getFieldCodes(objectName, field);\n```\n\n---\n\n**Affects:** 4.2.5\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/696dcb72a5089642367ddf73fb34a231a9abd68a\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453438067","453438069","504735316","505341473"], "labels":["in: core","type: enhancement"]}