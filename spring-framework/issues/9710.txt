{"id":"9710", "title":"Add extension to TransactionSynchronizationManager to allow custom resolution of unbound transaction resources to allow \"lazy-enlistment\" TransactionManagers [SPR-5035]", "body":"**[Ben Rowlands](https://jira.spring.io/secure/ViewProfile.jspa?name=benrowlands)** opened **[SPR-5035](https://jira.spring.io/browse/SPR-5035?redirect=false)** and commented

Spring has no extension point to provide custom resolution of transaction resources. This hook is required to implement a \"lazy enlistment\" transaction manager.

A \"lazy-enlistement\" transaction manager differs from Springs \"out-the-box\" single resource transaction managers in that transaction resources (JMS connections or JDBC connections) are bound to the transaction when they are first used. The transaction manager collects resources during the life of the transaction rather than forcing developers to declare the resources they anticipate they will use upfront. Our developers find this programming model simpler and vastly reduces configuration overhead. Once a transaction manager is defined and code executed inside the transaction boundary any resources used are implicitly bound to the transaction.

The following pattern is used extensively throughout Springs resource handling code, for example in DataSourceUtils and ConnectionFactoryUtils:

public Resource doGetResource(ResourceFactory factory) {

Resource resource = getResourceFromTSM(factory);

// (1) resource found in transaction
if (resource != null){
return resource;
}

// (2) resource not found, create a new resource
resource = factory.getResource();

if (isSynchronizationActive()) {
addResourceToTSM(factory,resource);
addSynchronizationToCleanupResource(resource);
}

return resource;
}

Branch (2) may appear to show lazy creation of the resource however this can't be used for the following reasons:

    * No mechanism exists to get notified of the resource's addition to the TransactionSynchronizationManager.
    * The resource cleanup synchronization may cleanup the resource before the transaction manager enters the commit phase (as a result of closing of the connection in beforeCompletion). There is no way to disable this synchronization from been added.
    * There is no control over configuration of the resource, for example, after getting a JDBC Connection (the Resource) from a DataSource (the ResourceFacotry?) one must disable auto-commit in addition to other connection preparation such as configuring the isolation level. 

Spring's transaction managers pre-populate the TransactionSynchronizationManager with the resource(s) they will manage so transactional resources are always found and branch (1) is taken.

A transaction manager supporting \"lazy enlistment\" would need to plug in its own resource resolution strategy before Spring takes its fallback logic in (2).

We propose an extension point to be added to TransactionSynchronizationManager. A sketch of the extension point API is shown below:

interface TransactionResourceResolver
{
public Object resolveResource(Object key);
}

The following API would be used to register a custom strategy with the current transaction:

TransactionSynchronizationManager.setResourceResolver( resolver );

resolveResource() is invoked from TransactionSynchronizationManager#getResource(key) when the key is unbound. To clarify, the logic in getResource() would follow this pattern:

public Object getResource(Object key) {

// 1) Check resources map
Object resource = resources.get(key);

if (resource != null){
return resource;
}

// 2) Consult custom strategy
Object resource = resolver.resolveResource(key);
if (resource != null)
{
resources.put(key, resource);
return resource;
}

// 3) Give up
return null;
}

This small extension enables the implementation of the \"lazy enlistment\" Transaction Manager. Note, this is similar to the request in #7996. This new issue clarifies the requests made in the comments with a focus on lazy enlistment.

---

**Issue Links:**
- #7996 Provide listener for TransactionSynchronizationManager

6 votes, 10 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9710","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9710/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9710/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9710/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9710","id":398089970,"node_id":"MDU6SXNzdWUzOTgwODk5NzA=","number":9710,"title":"Add extension to TransactionSynchronizationManager to allow custom resolution of unbound transaction resources to allow \"lazy-enlistment\" TransactionManagers [SPR-5035]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2008-07-23T20:50:40Z","updated_at":"2019-01-11T14:33:42Z","closed_at":"2015-09-22T17:34:36Z","author_association":"COLLABORATOR","body":"**[Ben Rowlands](https://jira.spring.io/secure/ViewProfile.jspa?name=benrowlands)** opened **[SPR-5035](https://jira.spring.io/browse/SPR-5035?redirect=false)** and commented\n\nSpring has no extension point to provide custom resolution of transaction resources. This hook is required to implement a \"lazy enlistment\" transaction manager.\n\nA \"lazy-enlistement\" transaction manager differs from Springs \"out-the-box\" single resource transaction managers in that transaction resources (JMS connections or JDBC connections) are bound to the transaction when they are first used. The transaction manager collects resources during the life of the transaction rather than forcing developers to declare the resources they anticipate they will use upfront. Our developers find this programming model simpler and vastly reduces configuration overhead. Once a transaction manager is defined and code executed inside the transaction boundary any resources used are implicitly bound to the transaction.\n\nThe following pattern is used extensively throughout Springs resource handling code, for example in DataSourceUtils and ConnectionFactoryUtils:\n\npublic Resource doGetResource(ResourceFactory factory) {\n\nResource resource = getResourceFromTSM(factory);\n\n// (1) resource found in transaction\nif (resource != null){\nreturn resource;\n}\n\n// (2) resource not found, create a new resource\nresource = factory.getResource();\n\nif (isSynchronizationActive()) {\naddResourceToTSM(factory,resource);\naddSynchronizationToCleanupResource(resource);\n}\n\nreturn resource;\n}\n\nBranch (2) may appear to show lazy creation of the resource however this can't be used for the following reasons:\n\n    * No mechanism exists to get notified of the resource's addition to the TransactionSynchronizationManager.\n    * The resource cleanup synchronization may cleanup the resource before the transaction manager enters the commit phase (as a result of closing of the connection in beforeCompletion). There is no way to disable this synchronization from been added.\n    * There is no control over configuration of the resource, for example, after getting a JDBC Connection (the Resource) from a DataSource (the ResourceFacotry?) one must disable auto-commit in addition to other connection preparation such as configuring the isolation level. \n\nSpring's transaction managers pre-populate the TransactionSynchronizationManager with the resource(s) they will manage so transactional resources are always found and branch (1) is taken.\n\nA transaction manager supporting \"lazy enlistment\" would need to plug in its own resource resolution strategy before Spring takes its fallback logic in (2).\n\nWe propose an extension point to be added to TransactionSynchronizationManager. A sketch of the extension point API is shown below:\n\ninterface TransactionResourceResolver\n{\npublic Object resolveResource(Object key);\n}\n\nThe following API would be used to register a custom strategy with the current transaction:\n\nTransactionSynchronizationManager.setResourceResolver( resolver );\n\nresolveResource() is invoked from TransactionSynchronizationManager#getResource(key) when the key is unbound. To clarify, the logic in getResource() would follow this pattern:\n\npublic Object getResource(Object key) {\n\n// 1) Check resources map\nObject resource = resources.get(key);\n\nif (resource != null){\nreturn resource;\n}\n\n// 2) Consult custom strategy\nObject resource = resolver.resolveResource(key);\nif (resource != null)\n{\nresources.put(key, resource);\nreturn resource;\n}\n\n// 3) Give up\nreturn null;\n}\n\nThis small extension enables the implementation of the \"lazy enlistment\" Transaction Manager. Note, this is similar to the request in #7996. This new issue clarifies the requests made in the comments with a focus on lazy enlistment.\n\n---\n\n**Issue Links:**\n- #7996 Provide listener for TransactionSynchronizationManager\n\n6 votes, 10 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453332577"], "labels":["in: data","status: declined","type: enhancement"]}