{"id":"14295", "title":"@Transactional with txManager name is cumbersome with base dao classes [SPR-9661]", "body":"**[matthew inger](https://jira.spring.io/secure/ViewProfile.jspa?name=mattinger)** opened **[SPR-9661](https://jira.spring.io/browse/SPR-9661?redirect=false)** and commented

By embedding the transaction manager name in the `@Transactional` anotations, using base dao classes is cumbersome.  Example:

`@Transactional`
public class GenericDao<K extends Serializable,E> {
protected Class\\<E> mappedClass;
protected SessionFactory sessionFactory;

    public GenericDao(Class<E> mappedClass, SessionFactory sessionFactory) {
        this.mappedClass = mappedClass;
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(readOnly=false)
    public E getById(K key) {
        return sessionFactory.getCurrentSession().get(mappedClass, key);
    }
    
    public E merge(E entity) {
        return sessionFactory.getCurrentSession().merge(entity);
    }

}

Now we have two subclasses, each of which will use different sessionfactory, and transaction manager (we don't need jta).

`@Transactional`(\"fooTxManager\")
public class FooDao extends GenericDao<Long, Foo> {
...
}

`@Transactional`(\"barTxManager\")
public class FooDao extends GenericDao<Long, Foo> {
...
}

The problem is that we'd have to override the getById() method otherwise that method will use the default transaction manager.  Not a big deal with 1 method, but it is a big deal when there's a bunch of methods.

`@Transactional`(\"barTxManager\")
public class FooDao extends GenericDao<Long, Foo> {
`@Override`
`@Transactional`(\"barTxManager\")
public Foo getById(Long key) {
return super.getById(key);
}
...
}

I think a better approach would be to either:

a) Allow the transaction manager name, when left blank, to fallback to the annotation at the class level.
b) Separate the transaction semantics from the manager name with a new annotation:

`@TransactionManager`(\"barTxManager\")


---

**Affects:** 3.1.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14295","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14295/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14295/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/14295/events","html_url":"https://github.com/spring-projects/spring-framework/issues/14295","id":398152284,"node_id":"MDU6SXNzdWUzOTgxNTIyODQ=","number":14295,"title":"@Transactional with txManager name is cumbersome with base dao classes [SPR-9661]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2012-08-01T08:26:10Z","updated_at":"2019-01-12T02:46:56Z","closed_at":"2019-01-12T02:46:56Z","author_association":"COLLABORATOR","body":"**[matthew inger](https://jira.spring.io/secure/ViewProfile.jspa?name=mattinger)** opened **[SPR-9661](https://jira.spring.io/browse/SPR-9661?redirect=false)** and commented\n\nBy embedding the transaction manager name in the `@Transactional` anotations, using base dao classes is cumbersome.  Example:\n\n`@Transactional`\npublic class GenericDao<K extends Serializable,E> {\nprotected Class\\<E> mappedClass;\nprotected SessionFactory sessionFactory;\n\n    public GenericDao(Class<E> mappedClass, SessionFactory sessionFactory) {\n        this.mappedClass = mappedClass;\n        this.sessionFactory = sessionFactory;\n    }\n    \n    @Transactional(readOnly=false)\n    public E getById(K key) {\n        return sessionFactory.getCurrentSession().get(mappedClass, key);\n    }\n    \n    public E merge(E entity) {\n        return sessionFactory.getCurrentSession().merge(entity);\n    }\n\n}\n\nNow we have two subclasses, each of which will use different sessionfactory, and transaction manager (we don't need jta).\n\n`@Transactional`(\"fooTxManager\")\npublic class FooDao extends GenericDao<Long, Foo> {\n...\n}\n\n`@Transactional`(\"barTxManager\")\npublic class FooDao extends GenericDao<Long, Foo> {\n...\n}\n\nThe problem is that we'd have to override the getById() method otherwise that method will use the default transaction manager.  Not a big deal with 1 method, but it is a big deal when there's a bunch of methods.\n\n`@Transactional`(\"barTxManager\")\npublic class FooDao extends GenericDao<Long, Foo> {\n`@Override`\n`@Transactional`(\"barTxManager\")\npublic Foo getById(Long key) {\nreturn super.getById(key);\n}\n...\n}\n\nI think a better approach would be to either:\n\na) Allow the transaction manager name, when left blank, to fallback to the annotation at the class level.\nb) Separate the transaction semantics from the manager name with a new annotation:\n\n`@TransactionManager`(\"barTxManager\")\n\n\n---\n\n**Affects:** 3.1.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453396028","453396029","453713392"], "labels":["in: data","status: bulk-closed"]}