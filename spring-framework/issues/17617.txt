{"id":"17617", "title":"Improve ImportBeanDefinitionRegistrar for Generic Injection  [SPR-13026]", "body":"**[cemo koc](https://jira.spring.io/secure/ViewProfile.jspa?name=cemokoc)** opened **[SPR-13026](https://jira.spring.io/browse/SPR-13026?redirect=false)** and commented

Dear Juergen,

Since release of Spring 4.0, Spring supports java generic injection [1]. I came across a case that I could not find a solution. I would like to create a service with generic types programatically. Let me to give an example:

###### Library

```java
interface Animal{}

interface GenericAnimalRepository {
   <A extends Animal> A getAnimal(Class<A> clazz, int animalId);
}


interface AnimalService<A extends Animal> {
   A getAnimal(int animalId);
}

@Service
class AnimalServiceImpl<A extends Animal> implements AnimalService<A> {

   private Class<A> clazz;

   GenericAnimalRepository animalRepository;

   public AnimalServiceImpl(Class<A> clazz, GenericAnimalRepository animalRepository) {
      this.clazz = clazz;
      this.animalRepository = animalRepository;
   }

   @Override
   public A getAnimal(int animalId) {
      return animalRepository.getAnimal(clazz, animalId);
   }
   
}

```

###### Client

```

class Dog implements Animal{}

class Cat implements Animal{}

```

Here I would like to create both AnimalService\\<Dog> and AnimalService\\<Cat>. However my library does not aware of Animal implementations directly. At runtime It can access them as an Animal subclass but at compile time it does not aware of them. Generic Injection with Spring 4 is using GenericTypeResolver which is based on reflection. What I would like to create is that A BeanDefinition with custom Generic Types. I would like to build my BeanDefinitionBuilder with Dog or Cat and expecting correctly to be injected into my beans as this:

```
@Service
class MyService{
    
    @Autowired
     AnimalService<Dog> dogService;

     @Autowired
     AnimalService<Cat> catService;    

} 

```

What I am considering currently is that improving generic injection to support custom provided generic types. Hope that I am clear.

[1]: https://spring.io/blog/2013/12/03/spring-framework-4-0-and-java-generics



---

**Affects:** 4.1.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17617","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17617/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17617/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17617/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17617","id":398179649,"node_id":"MDU6SXNzdWUzOTgxNzk2NDk=","number":17617,"title":"Improve ImportBeanDefinitionRegistrar for Generic Injection  [SPR-13026]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2015-05-14T00:02:48Z","updated_at":"2019-01-12T00:16:33Z","closed_at":"2019-01-12T00:16:33Z","author_association":"COLLABORATOR","body":"**[cemo koc](https://jira.spring.io/secure/ViewProfile.jspa?name=cemokoc)** opened **[SPR-13026](https://jira.spring.io/browse/SPR-13026?redirect=false)** and commented\n\nDear Juergen,\n\nSince release of Spring 4.0, Spring supports java generic injection [1]. I came across a case that I could not find a solution. I would like to create a service with generic types programatically. Let me to give an example:\n\n###### Library\n\n```java\ninterface Animal{}\n\ninterface GenericAnimalRepository {\n   <A extends Animal> A getAnimal(Class<A> clazz, int animalId);\n}\n\n\ninterface AnimalService<A extends Animal> {\n   A getAnimal(int animalId);\n}\n\n@Service\nclass AnimalServiceImpl<A extends Animal> implements AnimalService<A> {\n\n   private Class<A> clazz;\n\n   GenericAnimalRepository animalRepository;\n\n   public AnimalServiceImpl(Class<A> clazz, GenericAnimalRepository animalRepository) {\n      this.clazz = clazz;\n      this.animalRepository = animalRepository;\n   }\n\n   @Override\n   public A getAnimal(int animalId) {\n      return animalRepository.getAnimal(clazz, animalId);\n   }\n   \n}\n\n```\n\n###### Client\n\n```\n\nclass Dog implements Animal{}\n\nclass Cat implements Animal{}\n\n```\n\nHere I would like to create both AnimalService\\<Dog> and AnimalService\\<Cat>. However my library does not aware of Animal implementations directly. At runtime It can access them as an Animal subclass but at compile time it does not aware of them. Generic Injection with Spring 4 is using GenericTypeResolver which is based on reflection. What I would like to create is that A BeanDefinition with custom Generic Types. I would like to build my BeanDefinitionBuilder with Dog or Cat and expecting correctly to be injected into my beans as this:\n\n```\n@Service\nclass MyService{\n    \n    @Autowired\n     AnimalService<Dog> dogService;\n\n     @Autowired\n     AnimalService<Cat> catService;    \n\n} \n\n```\n\nWhat I am considering currently is that improving generic injection to support custom provided generic types. Hope that I am clear.\n\n[1]: https://spring.io/blog/2013/12/03/spring-framework-4-0-and-java-generics\n\n\n\n---\n\n**Affects:** 4.1.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453698524"], "labels":["status: bulk-closed"]}