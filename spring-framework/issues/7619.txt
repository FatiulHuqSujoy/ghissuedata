{"id":"7619", "title":"Spring Beans Isn't Self Contained for Load-Time Weaving [SPR-2933]", "body":"**[Ron Bodkin](https://jira.spring.io/secure/ViewProfile.jspa?name=rbodkin)** opened **[SPR-2933](https://jira.spring.io/browse/SPR-2933?redirect=false)** and commented

The spring-beans.jar includes a META-INF/aop.xml file that references aspects that aren't included in the jar:

    <aspect name=\"org.springframework.beans.factory.aspectj.AnnotationBeanConfigurerAspect\"/>
    <aspect name=\"org.springframework.transaction.aspectj.AnnotationTransactionAspect\"/>

This results in errors, e.g., when deploying the webflow phonebook application to a JVM with load-time weaving. Work-around options: remove the aop.xml file or include spring-aspects.jar

Error output:

Dec 9, 2006 4:53:08 PM org.aspectj.weaver.tools.Jdk14Trace error
SEVERE: register definition failed
java.lang.RuntimeException: Cannot register non aspect: org$springframework$bean
s$factory$aspectj$AnnotationBeanConfigurerAspect , org.springframework.beans.fac
tory.aspectj.AnnotationBeanConfigurerAspect
at org.aspectj.weaver.bcel.BcelWeaver.addLibraryAspect(BcelWeaver.java:2
05)
at org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.registerAspects
(ClassLoaderWeavingAdaptor.java:399)
at org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.registerDefinit
ions(ClassLoaderWeavingAdaptor.java:240)
at org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.initialize(Clas
sLoaderWeavingAdaptor.java:152)
at org.aspectj.weaver.loadtime.Aj$ExplicitlyInitializedClassLoaderWeavin
gAdaptor.initialize(Aj.java:151)
at org.aspectj.weaver.loadtime.Aj$ExplicitlyInitializedClassLoaderWeavin
gAdaptor.getWeavingAdaptor(Aj.java:156)
at org.aspectj.weaver.loadtime.Aj$WeaverContainer.getWeaver(Aj.java:122)

    at org.aspectj.weaver.loadtime.Aj.preProcess(Aj.java:73)
    at org.aspectj.weaver.loadtime.ClassPreProcessorAgentAdapter.transform(C

lassPreProcessorAgentAdapter.java:55)
at sun.instrument.TransformerManager.transform(TransformerManager.java:1
22)
at sun.instrument.InstrumentationImpl.transform(InstrumentationImpl.java
:155)
at java.lang.ClassLoader.defineClass1(Native Method)
at java.lang.ClassLoader.defineClass(ClassLoader.java:620)
at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:12
4)
at org.apache.catalina.loader.WebappClassLoader.findClassInternal(Webapp
ClassLoader.java:1815)
at org.apache.catalina.loader.WebappClassLoader.findClass(WebappClassLoa
der.java:869)
at org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoa
der.java:1322)
at org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoa
der.java:1201)
at org.apache.commons.logging.impl.LogFactoryImpl$1.run(LogFactoryImpl.j
ava:441)
at java.security.AccessController.doPrivileged(Native Method)
at org.apache.commons.logging.impl.LogFactoryImpl.loadClass(LogFactoryIm
pl.java:435)
at org.apache.commons.logging.impl.LogFactoryImpl.isLog4JAvailable(LogFa
ctoryImpl.java:505)
at org.apache.commons.logging.impl.LogFactoryImpl.getLogClassName(LogFac
toryImpl.java:327)
at org.apache.commons.logging.impl.LogFactoryImpl.getLogConstructor(LogF
actoryImpl.java:368)
at org.apache.commons.logging.impl.LogFactoryImpl.newInstance(LogFactory
Impl.java:529)
at org.apache.commons.logging.impl.LogFactoryImpl.getInstance(LogFactory
Impl.java:235)
at org.apache.commons.logging.LogFactory.getLog(LogFactory.java:370)
at org.apache.catalina.core.ContainerBase.getLogger(ContainerBase.java:3
80)
at org.apache.catalina.core.StandardContext.start(StandardContext.java:4
114)
at org.apache.catalina.core.ContainerBase.addChildInternal(ContainerBase
.java:759)
at org.apache.catalina.core.ContainerBase.addChild(ContainerBase.java:73
9)
at org.apache.catalina.core.StandardHost.addChild(StandardHost.java:524)

        at org.apache.catalina.startup.HostConfig.deployWAR(HostConfig.java:809)
    
        at org.apache.catalina.startup.HostConfig.deployWARs(HostConfig.java:698

)
at org.apache.catalina.startup.HostConfig.deployApps(HostConfig.java:472
)
at org.apache.catalina.startup.HostConfig.start(HostConfig.java:1122)
at org.apache.catalina.startup.HostConfig.lifecycleEvent(HostConfig.java
:310)
at org.apache.catalina.util.LifecycleSupport.fireLifecycleEvent(Lifecycl
eSupport.java:119)
at org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1021)

        at org.apache.catalina.core.StandardHost.start(StandardHost.java:718)
        at org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1013)
    
        at org.apache.catalina.core.StandardEngine.start(StandardEngine.java:442

)
at org.apache.catalina.core.StandardService.start(StandardService.java:4
50)
at org.apache.catalina.core.StandardServer.start(StandardServer.java:709
)
at org.apache.catalina.startup.Catalina.start(Catalina.java:551)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.
java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAcces
sorImpl.java:25)
at java.lang.reflect.Method.invoke(Method.java:585)
at org.apache.catalina.startup.Bootstrap.start(Bootstrap.java:294)
at org.apache.catalina.startup.Bootstrap.main(Bootstrap.java:432)



---

**Affects:** 2.0.1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7619","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7619/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7619/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7619/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7619","id":398073652,"node_id":"MDU6SXNzdWUzOTgwNzM2NTI=","number":7619,"title":"Spring Beans Isn't Self Contained for Load-Time Weaving [SPR-2933]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/38","html_url":"https://github.com/spring-projects/spring-framework/milestone/38","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/38/labels","id":3960809,"node_id":"MDk6TWlsZXN0b25lMzk2MDgwOQ==","number":38,"title":"2.0.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":89,"state":"closed","created_at":"2019-01-10T22:02:37Z","updated_at":"2019-01-11T00:16:36Z","due_on":null,"closed_at":"2019-01-10T22:02:37Z"},"comments":2,"created_at":"2006-12-09T11:01:35Z","updated_at":"2012-06-19T03:50:18Z","closed_at":"2012-06-19T03:50:18Z","author_association":"COLLABORATOR","body":"**[Ron Bodkin](https://jira.spring.io/secure/ViewProfile.jspa?name=rbodkin)** opened **[SPR-2933](https://jira.spring.io/browse/SPR-2933?redirect=false)** and commented\n\nThe spring-beans.jar includes a META-INF/aop.xml file that references aspects that aren't included in the jar:\n\n    <aspect name=\"org.springframework.beans.factory.aspectj.AnnotationBeanConfigurerAspect\"/>\n    <aspect name=\"org.springframework.transaction.aspectj.AnnotationTransactionAspect\"/>\n\nThis results in errors, e.g., when deploying the webflow phonebook application to a JVM with load-time weaving. Work-around options: remove the aop.xml file or include spring-aspects.jar\n\nError output:\n\nDec 9, 2006 4:53:08 PM org.aspectj.weaver.tools.Jdk14Trace error\nSEVERE: register definition failed\njava.lang.RuntimeException: Cannot register non aspect: org$springframework$bean\ns$factory$aspectj$AnnotationBeanConfigurerAspect , org.springframework.beans.fac\ntory.aspectj.AnnotationBeanConfigurerAspect\nat org.aspectj.weaver.bcel.BcelWeaver.addLibraryAspect(BcelWeaver.java:2\n05)\nat org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.registerAspects\n(ClassLoaderWeavingAdaptor.java:399)\nat org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.registerDefinit\nions(ClassLoaderWeavingAdaptor.java:240)\nat org.aspectj.weaver.loadtime.ClassLoaderWeavingAdaptor.initialize(Clas\nsLoaderWeavingAdaptor.java:152)\nat org.aspectj.weaver.loadtime.Aj$ExplicitlyInitializedClassLoaderWeavin\ngAdaptor.initialize(Aj.java:151)\nat org.aspectj.weaver.loadtime.Aj$ExplicitlyInitializedClassLoaderWeavin\ngAdaptor.getWeavingAdaptor(Aj.java:156)\nat org.aspectj.weaver.loadtime.Aj$WeaverContainer.getWeaver(Aj.java:122)\n\n    at org.aspectj.weaver.loadtime.Aj.preProcess(Aj.java:73)\n    at org.aspectj.weaver.loadtime.ClassPreProcessorAgentAdapter.transform(C\n\nlassPreProcessorAgentAdapter.java:55)\nat sun.instrument.TransformerManager.transform(TransformerManager.java:1\n22)\nat sun.instrument.InstrumentationImpl.transform(InstrumentationImpl.java\n:155)\nat java.lang.ClassLoader.defineClass1(Native Method)\nat java.lang.ClassLoader.defineClass(ClassLoader.java:620)\nat java.security.SecureClassLoader.defineClass(SecureClassLoader.java:12\n4)\nat org.apache.catalina.loader.WebappClassLoader.findClassInternal(Webapp\nClassLoader.java:1815)\nat org.apache.catalina.loader.WebappClassLoader.findClass(WebappClassLoa\nder.java:869)\nat org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoa\nder.java:1322)\nat org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoa\nder.java:1201)\nat org.apache.commons.logging.impl.LogFactoryImpl$1.run(LogFactoryImpl.j\nava:441)\nat java.security.AccessController.doPrivileged(Native Method)\nat org.apache.commons.logging.impl.LogFactoryImpl.loadClass(LogFactoryIm\npl.java:435)\nat org.apache.commons.logging.impl.LogFactoryImpl.isLog4JAvailable(LogFa\nctoryImpl.java:505)\nat org.apache.commons.logging.impl.LogFactoryImpl.getLogClassName(LogFac\ntoryImpl.java:327)\nat org.apache.commons.logging.impl.LogFactoryImpl.getLogConstructor(LogF\nactoryImpl.java:368)\nat org.apache.commons.logging.impl.LogFactoryImpl.newInstance(LogFactory\nImpl.java:529)\nat org.apache.commons.logging.impl.LogFactoryImpl.getInstance(LogFactory\nImpl.java:235)\nat org.apache.commons.logging.LogFactory.getLog(LogFactory.java:370)\nat org.apache.catalina.core.ContainerBase.getLogger(ContainerBase.java:3\n80)\nat org.apache.catalina.core.StandardContext.start(StandardContext.java:4\n114)\nat org.apache.catalina.core.ContainerBase.addChildInternal(ContainerBase\n.java:759)\nat org.apache.catalina.core.ContainerBase.addChild(ContainerBase.java:73\n9)\nat org.apache.catalina.core.StandardHost.addChild(StandardHost.java:524)\n\n        at org.apache.catalina.startup.HostConfig.deployWAR(HostConfig.java:809)\n    \n        at org.apache.catalina.startup.HostConfig.deployWARs(HostConfig.java:698\n\n)\nat org.apache.catalina.startup.HostConfig.deployApps(HostConfig.java:472\n)\nat org.apache.catalina.startup.HostConfig.start(HostConfig.java:1122)\nat org.apache.catalina.startup.HostConfig.lifecycleEvent(HostConfig.java\n:310)\nat org.apache.catalina.util.LifecycleSupport.fireLifecycleEvent(Lifecycl\neSupport.java:119)\nat org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1021)\n\n        at org.apache.catalina.core.StandardHost.start(StandardHost.java:718)\n        at org.apache.catalina.core.ContainerBase.start(ContainerBase.java:1013)\n    \n        at org.apache.catalina.core.StandardEngine.start(StandardEngine.java:442\n\n)\nat org.apache.catalina.core.StandardService.start(StandardService.java:4\n50)\nat org.apache.catalina.core.StandardServer.start(StandardServer.java:709\n)\nat org.apache.catalina.startup.Catalina.start(Catalina.java:551)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.\njava:39)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAcces\nsorImpl.java:25)\nat java.lang.reflect.Method.invoke(Method.java:585)\nat org.apache.catalina.startup.Bootstrap.start(Bootstrap.java:294)\nat org.apache.catalina.startup.Bootstrap.main(Bootstrap.java:432)\n\n\n\n---\n\n**Affects:** 2.0.1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453313268","453313269"], "labels":["in: core","type: bug"]}