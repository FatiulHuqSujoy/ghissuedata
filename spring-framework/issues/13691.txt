{"id":"13691", "title":"NamedParameterUtils,substituteNamedParameters() should consider that a namedParameterValue could be a SqlParameterValue  [SPR-9052]", "body":"**[Pengling Qian](https://jira.spring.io/secure/ViewProfile.jspa?name=pqian)** opened **[SPR-9052](https://jira.spring.io/browse/SPR-9052?redirect=false)** and commented

h2.Use Case:
I have a sql: WHERE clause contains **id in (:ids)**, column \"id\" is type integer in Mysql.
But ids given is a collect of strings, I don't want to generate such result: **id in ('1','2','3',...)**, although it could be executed. **id in (1,2,3,...)** is expected by me. So I tried the code below;

```
Map m = new Map();
m.put(\"ids\", new SqlParameterValue(Types.INTEGER, ids));
source = new MapSqlParameterSource(m);
query(sql, source, rowmapper);
```

I got an exception because of WHERE clause in parsed sql is: **id in (\\?)**, not **id in (?,?,?,...)**

NamedParameterUtils,substituteNamedParameters() didn't consider a collection could be wrapped in SqlParameterValue:

```
Object value = paramSource.getValue(paramName);
if (value instanceof Collection) { // <-- need (value instanceof SqlParameterValue and value.getValue() instanceof Collection)
  Iterator entryIter = ((Collection) value).iterator();
  ...
}
```

But PreparedStatementCreatorFactory.setValues(PreparedStatement ps) considered this use case:

```
int sqlColIndx = 1;
for (int i = 0; i < this.parameters.size(); i++) {
  Object in = this.parameters.get(i);
  SqlParameter declaredParameter = null;
  // SqlParameterValue overrides declared parameter metadata, in particular for
  // independence from the declared parameter position in case of named parameters.
  if (in instanceof SqlParameterValue) {
    SqlParameterValue paramValue = (SqlParameterValue) in;
    in = paramValue.getValue();
    declaredParameter = paramValue;
  }
  else {
    if (declaredParameters.size() <= i) {
      throw new InvalidDataAccessApiUsageException(
	\"SQL [\" + sql + \"]: unable to access parameter number \" + (i + 1) +
	\" given only \" + declaredParameters.size() + \" parameters\");
    }
    declaredParameter = (SqlParameter) declaredParameters.get(i);
  }
  if (in instanceof Collection && declaredParameter.getSqlType() != Types.ARRAY) {
    Collection entries = (Collection) in;
    for (Iterator it = entries.iterator(); it.hasNext();) {
      Object entry = it.next();
      if (entry instanceof Object[]) {
        Object[] valueArray = ((Object[])entry);
        for (int k = 0; k < valueArray.length; k++) {
          Object argValue = valueArray[k];
          StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, argValue);
        }
      }
      else {
        StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, entry);
      }
    }
  }
  else {
    StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, in);
  }
}
```

h2.Temporary solution:

```
source = new MapSqlParameterSource();
source.addValue(\"ids\", ids, Types.INTEGER);
query(sql, source, rowmapper);
```



---

**Affects:** 2.5.6, 3.0 GA, 3.1 GA

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1e1f8c912b3a1c79d040db9608f0e6890cbd2679
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13691","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13691/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13691/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/13691/events","html_url":"https://github.com/spring-projects/spring-framework/issues/13691","id":398116823,"node_id":"MDU6SXNzdWUzOTgxMTY4MjM=","number":13691,"title":"NamedParameterUtils,substituteNamedParameters() should consider that a namedParameterValue could be a SqlParameterValue  [SPR-9052]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2012-01-24T07:14:20Z","updated_at":"2019-01-11T22:24:01Z","closed_at":"2012-02-09T07:04:24Z","author_association":"COLLABORATOR","body":"**[Pengling Qian](https://jira.spring.io/secure/ViewProfile.jspa?name=pqian)** opened **[SPR-9052](https://jira.spring.io/browse/SPR-9052?redirect=false)** and commented\n\nh2.Use Case:\nI have a sql: WHERE clause contains **id in (:ids)**, column \"id\" is type integer in Mysql.\nBut ids given is a collect of strings, I don't want to generate such result: **id in ('1','2','3',...)**, although it could be executed. **id in (1,2,3,...)** is expected by me. So I tried the code below;\n\n```\nMap m = new Map();\nm.put(\"ids\", new SqlParameterValue(Types.INTEGER, ids));\nsource = new MapSqlParameterSource(m);\nquery(sql, source, rowmapper);\n```\n\nI got an exception because of WHERE clause in parsed sql is: **id in (\\?)**, not **id in (?,?,?,...)**\n\nNamedParameterUtils,substituteNamedParameters() didn't consider a collection could be wrapped in SqlParameterValue:\n\n```\nObject value = paramSource.getValue(paramName);\nif (value instanceof Collection) { // <-- need (value instanceof SqlParameterValue and value.getValue() instanceof Collection)\n  Iterator entryIter = ((Collection) value).iterator();\n  ...\n}\n```\n\nBut PreparedStatementCreatorFactory.setValues(PreparedStatement ps) considered this use case:\n\n```\nint sqlColIndx = 1;\nfor (int i = 0; i < this.parameters.size(); i++) {\n  Object in = this.parameters.get(i);\n  SqlParameter declaredParameter = null;\n  // SqlParameterValue overrides declared parameter metadata, in particular for\n  // independence from the declared parameter position in case of named parameters.\n  if (in instanceof SqlParameterValue) {\n    SqlParameterValue paramValue = (SqlParameterValue) in;\n    in = paramValue.getValue();\n    declaredParameter = paramValue;\n  }\n  else {\n    if (declaredParameters.size() <= i) {\n      throw new InvalidDataAccessApiUsageException(\n\t\"SQL [\" + sql + \"]: unable to access parameter number \" + (i + 1) +\n\t\" given only \" + declaredParameters.size() + \" parameters\");\n    }\n    declaredParameter = (SqlParameter) declaredParameters.get(i);\n  }\n  if (in instanceof Collection && declaredParameter.getSqlType() != Types.ARRAY) {\n    Collection entries = (Collection) in;\n    for (Iterator it = entries.iterator(); it.hasNext();) {\n      Object entry = it.next();\n      if (entry instanceof Object[]) {\n        Object[] valueArray = ((Object[])entry);\n        for (int k = 0; k < valueArray.length; k++) {\n          Object argValue = valueArray[k];\n          StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, argValue);\n        }\n      }\n      else {\n        StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, entry);\n      }\n    }\n  }\n  else {\n    StatementCreatorUtils.setParameterValue(psToUse, sqlColIndx++, declaredParameter, in);\n  }\n}\n```\n\nh2.Temporary solution:\n\n```\nsource = new MapSqlParameterSource();\nsource.addValue(\"ids\", ids, Types.INTEGER);\nquery(sql, source, rowmapper);\n```\n\n\n\n---\n\n**Affects:** 2.5.6, 3.0 GA, 3.1 GA\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/1e1f8c912b3a1c79d040db9608f0e6890cbd2679\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453365046"], "labels":["in: core"]}