{"id":"17900", "title":"Spring Transactions - Bug. PROPAGATION REQUIRED FAILS TO BEHAVE [SPR-13315]", "body":"**[moe](https://jira.spring.io/secure/ViewProfile.jspa?name=moe1234512345)** opened **[SPR-13315](https://jira.spring.io/browse/SPR-13315?redirect=false)** and commented

EDIT:
PLEASE DO NOT RECOMMEND THAT I USE TRANSACTION ANNOTAIONS FOR THIS. THIS IS NOT WHAT THIS BUG REPORT IS ABOUT.
Related: http://stackoverflow.com/questions/31817664/spring-hibernate-manually-creating-transactions-propagation-required-fails-b

I have run into what appears to be a bug, related to Spring handling of transactions.

Please have a look at these two test cases, comments are in the code:

Entity class for example:

```java
@Entity
public class Person{
    @Id
    String name;
}
```

Some methods used:

```java
    public TransactionStatus requireTransaction() {
            TransactionTemplate template = new TransactionTemplate();
            template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            return getTransactionManager().getTransaction(template);
    }
    
    public Session session() {
            return getRepository().session();
    }
    
    public PlatformTransactionManager getTransactionManager() {
            return getRepository().getTransactionManager();
    }

```

Here is the first test, testA();

```java
    @Test
    public void testA() throws InterruptedException {
            // We create the first transaction
            TransactionStatus statusOne = requireTransaction();
    
            // Create person one
            Person pOne = new Person();
            pOne.name = \"PersonOne\";
            session().persist(pOne);
    
            // ---> 111) NOTE! We do not commit! Intentionally!
    
            // We requireTransaction again. We should be getting the same transaction status.
    
            TransactionStatus statusTwo = requireTransaction();
            if ( !statusTwo.isNewTransaction() ) {
                    System.out.println(\"isNewTransaction: false! As expected! Meaning we are getting the original transaction status!\");
            }
    
    
            // Create person two
            Person pTwo = new Person();
            pTwo.name = \"PersonTwo\";
            session().persist(pTwo);
    
            // We will now be committing statusTwo which should actually be the first one, statusOne,
            // since we are using propagation required and the previous transaction was never committed
            // or rolledback or completed in any other fashion!
    
            getTransactionManager().commit(statusTwo);
    
            // !!!!!!! However !!!!!! Nothing is actually written to the database here!
    
            // This must be a bug. It existed on Spring 4.0.4 and I have upgraded to 4.2.0 and still the same thing happens!
    
            // Lets go on to the next test. testB() below.
    
            // If we now, at 111) instead do, let me repeat the entire logic:
    }

```

Here is the second test, testA();

```java
@Test
    public void testB() throws InterruptedException {
            // We create the first transaction
            TransactionStatus statusOne = requireTransaction();
    
            Person pOne = new Person();
            pOne.name = \"PersonOne\";
            session().persist(pOne);
    
            // -----> 111) NOW WE ARE COMMITTING INSTEAD, SINCE WE ARE ALMOST FORCED TO BUT DO NOT WANT TO
            getTransactionManager().commit(statusOne);
    
            // ----> 222) HOWEVER, NOW WE WILL NOT BE ABLE TO ROLLBACK THIS AT A LATER POINT
    
            // We requireTransaction again. We should be getting A NEW transaction status.
    
            TransactionStatus statusTwo = requireTransaction();
            if ( statusTwo.isNewTransaction() ) {
                    System.out.println(\"isNewTransaction: true! As expected! Meaning we are getting a new transaction status!\");
            }
    
            Person pTwo = new Person();
            pTwo.name = \"PersonTwo\";
            session().persist(pTwo);
    
            getTransactionManager().commit(statusTwo);
    
            // Now we will have two instances in the database, as expected.
    
            // If we instead of committing statusTwo would have done:
            // getTransactionManager().rollback(statusTwo)
            // then only the last one will be rolledback which is not desired!
    
            // Why are we forced to commit the first one to have any effect on future transactions!
            // Delegation will not work like this!
    }
```

Was that clear?

This is obviously a bug, is it not?

Why purpose would requireTransaction with PROPAGATION_REQUIRED have other than destroy future commits by the same thread?

Why is the commit on **statusTwo** in **testA()** not sufficient to commit the work on the first one as well?

Should this be done some other way? I think not, right? Bug!


---

**Affects:** 4.2 GA

**Reference URL:** http://stackoverflow.com/questions/31817664/spring-hibernate-manually-creating-transactions-propagation-required-fails-b
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17900","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17900/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17900/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17900/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17900","id":398182565,"node_id":"MDU6SXNzdWUzOTgxODI1NjU=","number":17900,"title":"Spring Transactions - Bug. PROPAGATION REQUIRED FAILS TO BEHAVE [SPR-13315]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2015-08-04T12:37:33Z","updated_at":"2019-01-12T16:46:04Z","closed_at":"2015-08-05T03:08:46Z","author_association":"COLLABORATOR","body":"**[moe](https://jira.spring.io/secure/ViewProfile.jspa?name=moe1234512345)** opened **[SPR-13315](https://jira.spring.io/browse/SPR-13315?redirect=false)** and commented\n\nEDIT:\nPLEASE DO NOT RECOMMEND THAT I USE TRANSACTION ANNOTAIONS FOR THIS. THIS IS NOT WHAT THIS BUG REPORT IS ABOUT.\nRelated: http://stackoverflow.com/questions/31817664/spring-hibernate-manually-creating-transactions-propagation-required-fails-b\n\nI have run into what appears to be a bug, related to Spring handling of transactions.\n\nPlease have a look at these two test cases, comments are in the code:\n\nEntity class for example:\n\n```java\n@Entity\npublic class Person{\n    @Id\n    String name;\n}\n```\n\nSome methods used:\n\n```java\n    public TransactionStatus requireTransaction() {\n            TransactionTemplate template = new TransactionTemplate();\n            template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);\n            return getTransactionManager().getTransaction(template);\n    }\n    \n    public Session session() {\n            return getRepository().session();\n    }\n    \n    public PlatformTransactionManager getTransactionManager() {\n            return getRepository().getTransactionManager();\n    }\n\n```\n\nHere is the first test, testA();\n\n```java\n    @Test\n    public void testA() throws InterruptedException {\n            // We create the first transaction\n            TransactionStatus statusOne = requireTransaction();\n    \n            // Create person one\n            Person pOne = new Person();\n            pOne.name = \"PersonOne\";\n            session().persist(pOne);\n    \n            // ---> 111) NOTE! We do not commit! Intentionally!\n    \n            // We requireTransaction again. We should be getting the same transaction status.\n    \n            TransactionStatus statusTwo = requireTransaction();\n            if ( !statusTwo.isNewTransaction() ) {\n                    System.out.println(\"isNewTransaction: false! As expected! Meaning we are getting the original transaction status!\");\n            }\n    \n    \n            // Create person two\n            Person pTwo = new Person();\n            pTwo.name = \"PersonTwo\";\n            session().persist(pTwo);\n    \n            // We will now be committing statusTwo which should actually be the first one, statusOne,\n            // since we are using propagation required and the previous transaction was never committed\n            // or rolledback or completed in any other fashion!\n    \n            getTransactionManager().commit(statusTwo);\n    \n            // !!!!!!! However !!!!!! Nothing is actually written to the database here!\n    \n            // This must be a bug. It existed on Spring 4.0.4 and I have upgraded to 4.2.0 and still the same thing happens!\n    \n            // Lets go on to the next test. testB() below.\n    \n            // If we now, at 111) instead do, let me repeat the entire logic:\n    }\n\n```\n\nHere is the second test, testA();\n\n```java\n@Test\n    public void testB() throws InterruptedException {\n            // We create the first transaction\n            TransactionStatus statusOne = requireTransaction();\n    \n            Person pOne = new Person();\n            pOne.name = \"PersonOne\";\n            session().persist(pOne);\n    \n            // -----> 111) NOW WE ARE COMMITTING INSTEAD, SINCE WE ARE ALMOST FORCED TO BUT DO NOT WANT TO\n            getTransactionManager().commit(statusOne);\n    \n            // ----> 222) HOWEVER, NOW WE WILL NOT BE ABLE TO ROLLBACK THIS AT A LATER POINT\n    \n            // We requireTransaction again. We should be getting A NEW transaction status.\n    \n            TransactionStatus statusTwo = requireTransaction();\n            if ( statusTwo.isNewTransaction() ) {\n                    System.out.println(\"isNewTransaction: true! As expected! Meaning we are getting a new transaction status!\");\n            }\n    \n            Person pTwo = new Person();\n            pTwo.name = \"PersonTwo\";\n            session().persist(pTwo);\n    \n            getTransactionManager().commit(statusTwo);\n    \n            // Now we will have two instances in the database, as expected.\n    \n            // If we instead of committing statusTwo would have done:\n            // getTransactionManager().rollback(statusTwo)\n            // then only the last one will be rolledback which is not desired!\n    \n            // Why are we forced to commit the first one to have any effect on future transactions!\n            // Delegation will not work like this!\n    }\n```\n\nWas that clear?\n\nThis is obviously a bug, is it not?\n\nWhy purpose would requireTransaction with PROPAGATION_REQUIRED have other than destroy future commits by the same thread?\n\nWhy is the commit on **statusTwo** in **testA()** not sufficient to commit the work on the first one as well?\n\nShould this be done some other way? I think not, right? Bug!\n\n\n---\n\n**Affects:** 4.2 GA\n\n**Reference URL:** http://stackoverflow.com/questions/31817664/spring-hibernate-manually-creating-transactions-propagation-required-fails-b\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453428660","453428662","453428663","453428665","453428666","453428667"], "labels":["in: data","status: duplicate"]}