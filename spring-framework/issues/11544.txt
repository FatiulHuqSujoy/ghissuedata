{"id":"11544", "title":"An optional @RequestParam parameter of an @InitBinder method is filled with garbarge instead of null, if the corresponding parameter is missing on the URL [SPR-6878]", "body":"**[Hans Desmet](https://jira.spring.io/secure/ViewProfile.jspa?name=desmethans)** opened **[SPR-6878](https://jira.spring.io/browse/SPR-6878?redirect=false)** and commented

A controller method can contain a method with `@InitBinder`
This method can contain a parameter with `@RequestParam`
`@RequestParam` can contain required=false, to indicate that corresponding URL parameter CAN be present, but MUST NOT be present
If the corresponding URL parameter IS NOT present, the java parameter on which you apply `@RequestParam` SHOULD BE filled with null
NOW it is filled with garbage (a series of characters, most of them tabs and newlines)

Command class:

---

package org.example.entities;

public class Person {
private String firstName;
private String lastName;

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getFirstName() {
return firstName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getLastName() {
return lastName;
}
}

Controller class:

---

package org.example.web;

import org.example.entities.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

`@Controller`
public class PersonController {
`@RequestMapping`(value = \"/person.htm\", method = RequestMethod.GET)
public String edit(`@RequestParam`(required = false) String wizzardStep,
Model model) {
model.addAttribute(new Person());
return \"person.jsp\";
}

`@RequestMapping`(value = \"/person.htm\", method = RequestMethod.POST)
public String edit(`@ModelAttribute` Person person) {
return \"person.jsp\";
}

`@InitBinder`(\"person\")
public void initBinder(
`@RequestParam`(value = \"wizzardStep\", required = false) String wizzardStep) {
System.out.print(\"wizzardStep==null:\");
System.out.println(wizzardStep == null);
if (wizzardStep != null) {
for (char kar : wizzardStep.toCharArray()) {
System.out.println(Character.isWhitespace(kar));
}
}
}

person.jsp

---

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

<%`@page` contentType=\"text/html\" pageEncoding=\"UTF-8\" session=\"false\"%>
<%`@taglib` prefix=\"c\" uri=\"http://java.sun.com/jsp/jstl/core\"%>
<%`@taglib` prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%>

\\<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"
\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
\\<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"nl\" lang=\"nl\">
\\<head>
\\<title>Continent example\\</title>
\\<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
\\</head>
\\<body>
\\<form:form commandName=\"person\">
\\<div>
\\<form:input path=\"firstName\"/>
\\</div>
\\<div>
\\</div>
\\<form:input path=\"lastName\"/>
\\<div>\\<input type=\"submit\" />\\</div>
\\</form:form>
\\</body>
\\</html>

When you submit the form, the output on the console (from the initBinder method):

---

wizzardStep==null:false
true
true
true
true
true
true
true
false
false
false
true
true
true
true
true
true
}

If you describe the `@RequestParam` java parameter as Integer, instead of String,
the problem gets more serious (HTTP 400 error), because the garbage cannot be converted to Integer.

P.S.

---

`@RequestParam` is very useful in an `@InitBinder` method, when you have multiple forms in wizzard style
and there is a URL parameter which indicates the current step in the wizzard.

---

**Affects:** 3.0.1

**Issue Links:**
- #11457 `@RequestParam`, `@CookieValue` and `@RequestHeader` default-value and required are not in sync

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11544","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11544/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11544/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11544/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11544","id":398103175,"node_id":"MDU6SXNzdWUzOTgxMDMxNzU=","number":11544,"title":"An optional @RequestParam parameter of an @InitBinder method is filled with garbarge instead of null, if the corresponding parameter is missing on the URL [SPR-6878]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/69","html_url":"https://github.com/spring-projects/spring-framework/milestone/69","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/69/labels","id":3960842,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mg==","number":69,"title":"3.0.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":98,"state":"closed","created_at":"2019-01-10T22:03:15Z","updated_at":"2019-01-11T02:52:19Z","due_on":"2010-03-31T07:00:00Z","closed_at":"2019-01-10T22:03:15Z"},"comments":1,"created_at":"2010-02-19T17:17:25Z","updated_at":"2019-01-13T07:55:18Z","closed_at":"2012-06-19T03:43:12Z","author_association":"COLLABORATOR","body":"**[Hans Desmet](https://jira.spring.io/secure/ViewProfile.jspa?name=desmethans)** opened **[SPR-6878](https://jira.spring.io/browse/SPR-6878?redirect=false)** and commented\n\nA controller method can contain a method with `@InitBinder`\nThis method can contain a parameter with `@RequestParam`\n`@RequestParam` can contain required=false, to indicate that corresponding URL parameter CAN be present, but MUST NOT be present\nIf the corresponding URL parameter IS NOT present, the java parameter on which you apply `@RequestParam` SHOULD BE filled with null\nNOW it is filled with garbage (a series of characters, most of them tabs and newlines)\n\nCommand class:\n\n---\n\npackage org.example.entities;\n\npublic class Person {\nprivate String firstName;\nprivate String lastName;\n\npublic void setFirstName(String firstName) {\nthis.firstName = firstName;\n}\n\npublic String getFirstName() {\nreturn firstName;\n}\n\npublic void setLastName(String lastName) {\nthis.lastName = lastName;\n}\n\npublic String getLastName() {\nreturn lastName;\n}\n}\n\nController class:\n\n---\n\npackage org.example.web;\n\nimport org.example.entities.Person;\nimport org.springframework.stereotype.Controller;\nimport org.springframework.ui.Model;\nimport org.springframework.web.bind.annotation.InitBinder;\nimport org.springframework.web.bind.annotation.ModelAttribute;\nimport org.springframework.web.bind.annotation.RequestMapping;\nimport org.springframework.web.bind.annotation.RequestMethod;\nimport org.springframework.web.bind.annotation.RequestParam;\n\n`@Controller`\npublic class PersonController {\n`@RequestMapping`(value = \"/person.htm\", method = RequestMethod.GET)\npublic String edit(`@RequestParam`(required = false) String wizzardStep,\nModel model) {\nmodel.addAttribute(new Person());\nreturn \"person.jsp\";\n}\n\n`@RequestMapping`(value = \"/person.htm\", method = RequestMethod.POST)\npublic String edit(`@ModelAttribute` Person person) {\nreturn \"person.jsp\";\n}\n\n`@InitBinder`(\"person\")\npublic void initBinder(\n`@RequestParam`(value = \"wizzardStep\", required = false) String wizzardStep) {\nSystem.out.print(\"wizzardStep==null:\");\nSystem.out.println(wizzardStep == null);\nif (wizzardStep != null) {\nfor (char kar : wizzardStep.toCharArray()) {\nSystem.out.println(Character.isWhitespace(kar));\n}\n}\n}\n\nperson.jsp\n\n---\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<%`@page` contentType=\"text/html\" pageEncoding=\"UTF-8\" session=\"false\"%>\n<%`@taglib` prefix=\"c\" uri=\"http://java.sun.com/jsp/jstl/core\"%>\n<%`@taglib` prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%>\n\n\\<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n\\<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"nl\" lang=\"nl\">\n\\<head>\n\\<title>Continent example\\</title>\n\\<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n\\</head>\n\\<body>\n\\<form:form commandName=\"person\">\n\\<div>\n\\<form:input path=\"firstName\"/>\n\\</div>\n\\<div>\n\\</div>\n\\<form:input path=\"lastName\"/>\n\\<div>\\<input type=\"submit\" />\\</div>\n\\</form:form>\n\\</body>\n\\</html>\n\nWhen you submit the form, the output on the console (from the initBinder method):\n\n---\n\nwizzardStep==null:false\ntrue\ntrue\ntrue\ntrue\ntrue\ntrue\ntrue\nfalse\nfalse\nfalse\ntrue\ntrue\ntrue\ntrue\ntrue\ntrue\n}\n\nIf you describe the `@RequestParam` java parameter as Integer, instead of String,\nthe problem gets more serious (HTTP 400 error), because the garbage cannot be converted to Integer.\n\nP.S.\n\n---\n\n`@RequestParam` is very useful in an `@InitBinder` method, when you have multiple forms in wizzard style\nand there is a URL parameter which indicates the current step in the wizzard.\n\n---\n\n**Affects:** 3.0.1\n\n**Issue Links:**\n- #11457 `@RequestParam`, `@CookieValue` and `@RequestHeader` default-value and required are not in sync\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453348447"], "labels":["in: web","type: bug"]}