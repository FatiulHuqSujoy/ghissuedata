{"id":"8827", "title":"AnnotationMethodHandlerAdapter exception [SPR-4149]", "body":"**[Andrei Tudose](https://jira.spring.io/secure/ViewProfile.jspa?name=andrei.tudose)** opened **[SPR-4149](https://jira.spring.io/browse/SPR-4149?redirect=false)** and commented

I have my DispatcherServlet application context

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:p=\"http://www.springframework.org/schema/p\"
xmlns:context=\"http://www.springframework.org/schema/context\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-2.5.xsd\">

    <context:component-scan base-package=\"ro.test.app.web\" />
        <bean name=\"/welcome.do\"
    	class=\"org.springframework.web.servlet.mvc.ParameterizableViewController\">
    	<property name=\"viewName\" value=\"welcome\" />
    </bean>

\\</beans>

and it works fine, but then I try to register a binder like in the Spring MVC docs by adding to DispatcherServlet application context the following bean

         <bean class=\"org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter\">
    	<property name=\"webBindingInitializer\">
    		<bean class=\"ro.test.app.util.IndexBindingInitializer\" />
    	</property>
    </bean>

When I try to load /welcome.do I get

javax.servlet.ServletException: No adapter for handler [org.springframework.web.servlet.mvc.ParameterizableViewController@234265]: Does your handler implement a supported interface like Controller?
org.springframework.web.servlet.DispatcherServlet.getHandlerAdapter(DispatcherServlet.java:1086)
org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:873)
org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:808)
org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:476)
org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:431)
javax.servlet.http.HttpServlet.service(HttpServlet.java:690)
javax.servlet.http.HttpServlet.service(HttpServlet.java:803)

My IndexBindingInitializer class looks like this

public class IndexBindingInitializer implements WebBindingInitializer {

    private static Log log = LogFactory.getLog(IndexBindingInitializer.class);
    
    public void initBinder(WebDataBinder binder, WebRequest request) {
    	log.info(\"binding\");
    	SimpleDateFormat dateFormat = new SimpleDateFormat(\"yyyy-MM-dd\");
    	dateFormat.setLenient(false);		
    	binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    	binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));        
    }

}


---

**Affects:** 2.5 final
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8827","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8827/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8827/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/8827/events","html_url":"https://github.com/spring-projects/spring-framework/issues/8827","id":398082981,"node_id":"MDU6SXNzdWUzOTgwODI5ODE=","number":8827,"title":"AnnotationMethodHandlerAdapter exception [SPR-4149]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2007-11-21T17:58:19Z","updated_at":"2019-01-12T16:39:34Z","closed_at":"2007-11-22T01:25:58Z","author_association":"COLLABORATOR","body":"**[Andrei Tudose](https://jira.spring.io/secure/ViewProfile.jspa?name=andrei.tudose)** opened **[SPR-4149](https://jira.spring.io/browse/SPR-4149?redirect=false)** and commented\n\nI have my DispatcherServlet application context\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:p=\"http://www.springframework.org/schema/p\"\nxmlns:context=\"http://www.springframework.org/schema/context\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd\nhttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-2.5.xsd\">\n\n    <context:component-scan base-package=\"ro.test.app.web\" />\n        <bean name=\"/welcome.do\"\n    \tclass=\"org.springframework.web.servlet.mvc.ParameterizableViewController\">\n    \t<property name=\"viewName\" value=\"welcome\" />\n    </bean>\n\n\\</beans>\n\nand it works fine, but then I try to register a binder like in the Spring MVC docs by adding to DispatcherServlet application context the following bean\n\n         <bean class=\"org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter\">\n    \t<property name=\"webBindingInitializer\">\n    \t\t<bean class=\"ro.test.app.util.IndexBindingInitializer\" />\n    \t</property>\n    </bean>\n\nWhen I try to load /welcome.do I get\n\njavax.servlet.ServletException: No adapter for handler [org.springframework.web.servlet.mvc.ParameterizableViewController@234265]: Does your handler implement a supported interface like Controller?\norg.springframework.web.servlet.DispatcherServlet.getHandlerAdapter(DispatcherServlet.java:1086)\norg.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:873)\norg.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:808)\norg.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:476)\norg.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:431)\njavax.servlet.http.HttpServlet.service(HttpServlet.java:690)\njavax.servlet.http.HttpServlet.service(HttpServlet.java:803)\n\nMy IndexBindingInitializer class looks like this\n\npublic class IndexBindingInitializer implements WebBindingInitializer {\n\n    private static Log log = LogFactory.getLog(IndexBindingInitializer.class);\n    \n    public void initBinder(WebDataBinder binder, WebRequest request) {\n    \tlog.info(\"binding\");\n    \tSimpleDateFormat dateFormat = new SimpleDateFormat(\"yyyy-MM-dd\");\n    \tdateFormat.setLenient(false);\t\t\n    \tbinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));\n    \tbinder.registerCustomEditor(String.class, new StringTrimmerEditor(false));        \n    }\n\n}\n\n\n---\n\n**Affects:** 2.5 final\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453324036"], "labels":["in: web","status: declined"]}