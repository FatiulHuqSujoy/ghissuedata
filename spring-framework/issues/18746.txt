{"id":"18746", "title":"Let @Scheduled work with a non-void method how is possible with <task:scheduled> [SPR-14175]", "body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-14175](https://jira.spring.io/browse/SPR-14175?redirect=false)** and commented

I am doing some experiments with `Task Execution and Scheduling`

I have two projects with this code:

```java
public class ProcessServiceImpl implements ProcessService, Runnable, Callable<String> {

	private static final Logger logger = LoggerFactory.getLogger(ProcessServiceImpl.class.getSimpleName());
	
	@Override
	public void executeRunnable() {
		new MessageRunnableWithSleep().run();
	}

	@Override
	public String executeCallable() {
		String result = \"\";
		try {
			result = new MessageCallableWithSleep().call(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
...
```

With XML I have:

```java
	<task:scheduler id=\"scheduler\" pool-size=\"#{T(com.manuel.jordan.support.Constants).FIXED_THREAD_POOL}\" />

	<task:scheduled-tasks scheduler=\"scheduler\" >
	
		<task:scheduled ref=\"processServiceImpl\" 
						method=\"executeRunnable\" 
						initial-delay=\"#{T(com.manuel.jordan.support.Constants).INITIAL_DELAY}\"						 
						fixed-rate=\"#{T(com.manuel.jordan.support.Constants).PERIOD}\"   
						/>						
		<task:scheduled ref=\"processServiceImpl\" 
						method=\"executeCallable\" 
						initial-delay=\"#{T(com.manuel.jordan.support.Constants).INITIAL_DELAY}\"						 
						fixed-rate=\"#{T(com.manuel.jordan.support.Constants).PERIOD}\"   
						/>
						
	</task:scheduled-tasks>
```

The code works fine. Until here I can confirm `<task:scheduled>` works for a method with a return type, therefore I can use a method where it returns something with schedule in peace.

But if in the other project I use:

```java
@Service
@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ProcessServiceImpl implements ProcessService, Runnable, Callable<String> {

	private static final Logger logger = LoggerFactory.getLogger(ProcessServiceImpl.class.getSimpleName());
	
	@Override
	@Scheduled(initialDelay=Constants.INITIAL_DELAY, fixedRate=Constants.PERIOD)
	public void executeRunnable() {
		new MessageRunnableWithSleep().run();
	}

	@Override
	@Scheduled(initialDelay=Constants.INITIAL_DELAY, fixedRate=Constants.PERIOD)
	public String executeCallable() {
		String result = \"\";
		try {
			result = new MessageCallableWithSleep().call(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
```

When I execute the code I get:

> Initialization of bean failed; nested exception is java.lang.IllegalStateException:
> Encountered invalid `@Scheduled` method 'executeCallable':
> Only void-returning methods may be annotated with `@Scheduled`

Therefore is not possible do a complete migration from XML to annotations.

The reason about have a scheduled method with a return value would be give the option to call the method manually (if need it) or leave in peace working within the scheduled process.

Let's consider the scenario about the sendNotificationEmail method with a Boolean how a return type. I am able to call the method manually and get the boolean value how a successful confirmation or put a schedule to work around that method.

Thanks

---

**Affects:** 4.2 GA, 4.2.5, 4.3 RC1

**Issue Links:**
- #18768 Add note constrain/restriction for <task:scheduled> about method signature such as `@Scheduled`

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18746","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18746/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18746/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/18746/events","html_url":"https://github.com/spring-projects/spring-framework/issues/18746","id":398192194,"node_id":"MDU6SXNzdWUzOTgxOTIxOTQ=","number":18746,"title":"Let @Scheduled work with a non-void method how is possible with <task:scheduled> [SPR-14175]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/148","html_url":"https://github.com/spring-projects/spring-framework/milestone/148","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/148/labels","id":3960921,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyMQ==","number":148,"title":"4.3 RC2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":48,"state":"closed","created_at":"2019-01-10T22:04:49Z","updated_at":"2019-01-11T09:03:03Z","due_on":"2016-05-05T07:00:00Z","closed_at":"2019-01-10T22:04:49Z"},"comments":6,"created_at":"2016-04-14T18:08:00Z","updated_at":"2019-01-13T05:26:22Z","closed_at":"2017-02-07T09:25:19Z","author_association":"COLLABORATOR","body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-14175](https://jira.spring.io/browse/SPR-14175?redirect=false)** and commented\n\nI am doing some experiments with `Task Execution and Scheduling`\n\nI have two projects with this code:\n\n```java\npublic class ProcessServiceImpl implements ProcessService, Runnable, Callable<String> {\n\n\tprivate static final Logger logger = LoggerFactory.getLogger(ProcessServiceImpl.class.getSimpleName());\n\t\n\t@Override\n\tpublic void executeRunnable() {\n\t\tnew MessageRunnableWithSleep().run();\n\t}\n\n\t@Override\n\tpublic String executeCallable() {\n\t\tString result = \"\";\n\t\ttry {\n\t\t\tresult = new MessageCallableWithSleep().call(); \n\t\t} catch (Exception e) {\n\t\t\te.printStackTrace();\n\t\t}\n\t\treturn result;\n\t}\n...\n```\n\nWith XML I have:\n\n```java\n\t<task:scheduler id=\"scheduler\" pool-size=\"#{T(com.manuel.jordan.support.Constants).FIXED_THREAD_POOL}\" />\n\n\t<task:scheduled-tasks scheduler=\"scheduler\" >\n\t\n\t\t<task:scheduled ref=\"processServiceImpl\" \n\t\t\t\t\t\tmethod=\"executeRunnable\" \n\t\t\t\t\t\tinitial-delay=\"#{T(com.manuel.jordan.support.Constants).INITIAL_DELAY}\"\t\t\t\t\t\t \n\t\t\t\t\t\tfixed-rate=\"#{T(com.manuel.jordan.support.Constants).PERIOD}\"   \n\t\t\t\t\t\t/>\t\t\t\t\t\t\n\t\t<task:scheduled ref=\"processServiceImpl\" \n\t\t\t\t\t\tmethod=\"executeCallable\" \n\t\t\t\t\t\tinitial-delay=\"#{T(com.manuel.jordan.support.Constants).INITIAL_DELAY}\"\t\t\t\t\t\t \n\t\t\t\t\t\tfixed-rate=\"#{T(com.manuel.jordan.support.Constants).PERIOD}\"   \n\t\t\t\t\t\t/>\n\t\t\t\t\t\t\n\t</task:scheduled-tasks>\n```\n\nThe code works fine. Until here I can confirm `<task:scheduled>` works for a method with a return type, therefore I can use a method where it returns something with schedule in peace.\n\nBut if in the other project I use:\n\n```java\n@Service\n@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)\npublic class ProcessServiceImpl implements ProcessService, Runnable, Callable<String> {\n\n\tprivate static final Logger logger = LoggerFactory.getLogger(ProcessServiceImpl.class.getSimpleName());\n\t\n\t@Override\n\t@Scheduled(initialDelay=Constants.INITIAL_DELAY, fixedRate=Constants.PERIOD)\n\tpublic void executeRunnable() {\n\t\tnew MessageRunnableWithSleep().run();\n\t}\n\n\t@Override\n\t@Scheduled(initialDelay=Constants.INITIAL_DELAY, fixedRate=Constants.PERIOD)\n\tpublic String executeCallable() {\n\t\tString result = \"\";\n\t\ttry {\n\t\t\tresult = new MessageCallableWithSleep().call(); \n\t\t} catch (Exception e) {\n\t\t\te.printStackTrace();\n\t\t}\n\t\treturn result;\n\t}\n```\n\nWhen I execute the code I get:\n\n> Initialization of bean failed; nested exception is java.lang.IllegalStateException:\n> Encountered invalid `@Scheduled` method 'executeCallable':\n> Only void-returning methods may be annotated with `@Scheduled`\n\nTherefore is not possible do a complete migration from XML to annotations.\n\nThe reason about have a scheduled method with a return value would be give the option to call the method manually (if need it) or leave in peace working within the scheduled process.\n\nLet's consider the scenario about the sendNotificationEmail method with a Boolean how a return type. I am able to call the method manually and get the boolean value how a successful confirmation or put a schedule to work around that method.\n\nThanks\n\n---\n\n**Affects:** 4.2 GA, 4.2.5, 4.3 RC1\n\n**Issue Links:**\n- #18768 Add note constrain/restriction for <task:scheduled> about method signature such as `@Scheduled`\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453438805","453438807","453438812","453438814","453438816","453438819"], "labels":["in: core","type: enhancement"]}