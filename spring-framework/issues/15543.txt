{"id":"15543", "title":"SpEL can't evaluate value if properties contain self quotes [SPR-10915]", "body":"**[Tuz Pavel](https://jira.spring.io/secure/ViewProfile.jspa?name=lostoverflow)** opened **[SPR-10915](https://jira.spring.io/browse/SPR-10915?redirect=false)** and commented

I'm using **org.springframework.beans.factory.config.PropertyPlaceholderConfigurer**
class to work with my properties.
I'm trying to evaluate the value by concatenation of several props in such way:

```xml
<property name=\"regex\" 
value=\"#{'${reader.regexp.header.room}'+
         '${reader.regexp.header.isZoom}'+
         '${reader.regexp.header.egnore}'}\" />
```

It works fine if props does not contain single quote: '
If it has one - I am having an exception: org.springframework.expression.ParseException: Found closing ')' at position 109 without an opening '('
Want to notice: if I try a similar expression in SINGLE line:

```xml
<property name=\"regex\" value=\"${reader.regexp.header.room}${reader.regexp.header.isZoom}${reader.regexp.header.hand}${reader.regexp.header.pokerType}${reader.regexp.header.limitType}${reader.regexp.header.blinds}${reader.regexp.header.date}${reader.regexp.header.systemTime}${reader.regexp.header.egnore}\" />
```

it works fine TOO even if it contaqns quoters!

Step to reproduce:
try to evaluate SpEL expression:

```xml
<property name=\"prop\" value=\"#{'${prop1}'+'${prop2}'}\" />
```

with props values:

```
 
prop1=abc 
prop2=cd'e
```

If you can't reproduce the bug, try my original xml config and props files.
problem property is

```
reader.regexp.header.pokerType=(Hold'em|Omaha)\\\\s+
```



---

**Attachments:**
- [cfg_props.zip](https://jira.spring.io/secure/attachment/21386/cfg_props.zip) (_1.47 kB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15543","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15543/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15543/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15543/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15543","id":398161505,"node_id":"MDU6SXNzdWUzOTgxNjE1MDU=","number":15543,"title":"SpEL can't evaluate value if properties contain self quotes [SPR-10915]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false},"assignees":[{"login":"philwebb","id":519772,"node_id":"MDQ6VXNlcjUxOTc3Mg==","avatar_url":"https://avatars1.githubusercontent.com/u/519772?v=4","gravatar_id":"","url":"https://api.github.com/users/philwebb","html_url":"https://github.com/philwebb","followers_url":"https://api.github.com/users/philwebb/followers","following_url":"https://api.github.com/users/philwebb/following{/other_user}","gists_url":"https://api.github.com/users/philwebb/gists{/gist_id}","starred_url":"https://api.github.com/users/philwebb/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/philwebb/subscriptions","organizations_url":"https://api.github.com/users/philwebb/orgs","repos_url":"https://api.github.com/users/philwebb/repos","events_url":"https://api.github.com/users/philwebb/events{/privacy}","received_events_url":"https://api.github.com/users/philwebb/received_events","type":"User","site_admin":false}],"milestone":null,"comments":1,"created_at":"2013-09-13T15:54:23Z","updated_at":"2019-01-13T21:02:32Z","closed_at":"2013-09-24T16:34:14Z","author_association":"COLLABORATOR","body":"**[Tuz Pavel](https://jira.spring.io/secure/ViewProfile.jspa?name=lostoverflow)** opened **[SPR-10915](https://jira.spring.io/browse/SPR-10915?redirect=false)** and commented\n\nI'm using **org.springframework.beans.factory.config.PropertyPlaceholderConfigurer**\nclass to work with my properties.\nI'm trying to evaluate the value by concatenation of several props in such way:\n\n```xml\n<property name=\"regex\" \nvalue=\"#{'${reader.regexp.header.room}'+\n         '${reader.regexp.header.isZoom}'+\n         '${reader.regexp.header.egnore}'}\" />\n```\n\nIt works fine if props does not contain single quote: '\nIf it has one - I am having an exception: org.springframework.expression.ParseException: Found closing ')' at position 109 without an opening '('\nWant to notice: if I try a similar expression in SINGLE line:\n\n```xml\n<property name=\"regex\" value=\"${reader.regexp.header.room}${reader.regexp.header.isZoom}${reader.regexp.header.hand}${reader.regexp.header.pokerType}${reader.regexp.header.limitType}${reader.regexp.header.blinds}${reader.regexp.header.date}${reader.regexp.header.systemTime}${reader.regexp.header.egnore}\" />\n```\n\nit works fine TOO even if it contaqns quoters!\n\nStep to reproduce:\ntry to evaluate SpEL expression:\n\n```xml\n<property name=\"prop\" value=\"#{'${prop1}'+'${prop2}'}\" />\n```\n\nwith props values:\n\n```\n \nprop1=abc \nprop2=cd'e\n```\n\nIf you can't reproduce the bug, try my original xml config and props files.\nproblem property is\n\n```\nreader.regexp.header.pokerType=(Hold'em|Omaha)\\\\s+\n```\n\n\n\n---\n\n**Attachments:**\n- [cfg_props.zip](https://jira.spring.io/secure/attachment/21386/cfg_props.zip) (_1.47 kB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453405577"], "labels":["in: core","status: declined"]}