{"id":"11637", "title":"Constructor injection is not working properly for collections unless the type of collection is specified [SPR-6972]", "body":"**[Abdurehim Ablimit](https://jira.spring.io/secure/ViewProfile.jspa?name=carawan)** opened **[SPR-6972](https://jira.spring.io/browse/SPR-6972?redirect=false)** and commented

**I have a very simple test class and it has two constructors as shown below. I also have bean definition as below.**

public class TestSpringBean {
private boolean bool;
private Map<String,String> prop;

    public TestSpringBean(boolean b,Map<String,String> prop) {
           //doing the constructor job
    }
    
    public TestSpringBean(Map<String,String> prop) {
    	this(true,prop);
    }

}

\\<bean id=\"mytestbean\" class=\"org.uytech.bugtrack.appclient.TestSpringBean\">
\\<constructor-arg type=\"java.util.Map\" index=\"0\">
\\<map>
\\<entry>\\<key>\\<value>yup an entry\\</value>\\</key>\\<value>just some string\\</value>\\</entry>
\\<entry>\\<key>\\<value>yup an entry2\\</value>\\</key>\\<value>just some string2\\</value>\\</entry>
\\<entry>\\<key>\\<value>yup an entry3\\</value>\\</key>\\<value>just some string3\\</value>\\</entry>
\\<entry>\\<key>\\<value>yup an entry4\\</value>\\</key>\\<value>just some string4\\</value>\\</entry>
\\</map>
\\</constructor-arg>
\\</bean>

**Unless I specify the constructor-arg type (java.util.Map in this case), Spring throws exception as shown below. The same is true for java.util.Properties class. Fortunately, this is not showstopper, users can still continue.**

**One workaround for this issue is to make the Map first parameter, but this not better than specifying the constructor-arg type in app context.**

**I did some investigation and found this issue comes from protected Map convertToTypedMap(Map original, String propertyName, Class requiredType, TypeDescriptor typeDescriptor) in TypeConvertorDelegate class. I don't understand why this class tries to convert a map to boolean.**

java.lang.InstantiationException: boolean
at java.lang.Class.newInstance0(Class.java:340)
at java.lang.Class.newInstance(Class.java:308)
at org.springframework.beans.TypeConverterDelegate.convertToTypedMap(TypeConverterDelegate.java:634)
at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:236)
at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:104)
at org.springframework.beans.BeanWrapperImpl.convertIfNecessary(BeanWrapperImpl.java:419)
at org.springframework.beans.factory.support.ConstructorResolver.createArgumentArray(ConstructorResolver.java:657)
at org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:191)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.autowireConstructor(AbstractAutowireCapableBeanFactory.java:984)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBeanInstance(AbstractAutowireCapableBeanFactory.java:888)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:479)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:450)
at org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:290)
at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:222)
at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:287)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:189)
at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:562)
at org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:871)
at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:423)


---

**Affects:** 3.0.1

**Attachments:**
- [repro.patch](https://jira.spring.io/secure/attachment/16330/repro.patch) (_2.36 kB_)

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/c13e5f9f5b764e80e52d4e4166652c64ca4d34e6, https://github.com/spring-projects/spring-framework/commit/77bb68b9675f1acb62053b52f0886ded3d0201ea, https://github.com/spring-projects/spring-framework/commit/c38c09bc351e222a9674d48c0bf74ba0a5bc63ad
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11637","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11637/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11637/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11637/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11637","id":398103746,"node_id":"MDU6SXNzdWUzOTgxMDM3NDY=","number":11637,"title":"Constructor injection is not working properly for collections unless the type of collection is specified [SPR-6972]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2010-03-11T06:14:43Z","updated_at":"2019-01-11T16:35:01Z","closed_at":"2012-06-19T02:28:09Z","author_association":"COLLABORATOR","body":"**[Abdurehim Ablimit](https://jira.spring.io/secure/ViewProfile.jspa?name=carawan)** opened **[SPR-6972](https://jira.spring.io/browse/SPR-6972?redirect=false)** and commented\n\n**I have a very simple test class and it has two constructors as shown below. I also have bean definition as below.**\n\npublic class TestSpringBean {\nprivate boolean bool;\nprivate Map<String,String> prop;\n\n    public TestSpringBean(boolean b,Map<String,String> prop) {\n           //doing the constructor job\n    }\n    \n    public TestSpringBean(Map<String,String> prop) {\n    \tthis(true,prop);\n    }\n\n}\n\n\\<bean id=\"mytestbean\" class=\"org.uytech.bugtrack.appclient.TestSpringBean\">\n\\<constructor-arg type=\"java.util.Map\" index=\"0\">\n\\<map>\n\\<entry>\\<key>\\<value>yup an entry\\</value>\\</key>\\<value>just some string\\</value>\\</entry>\n\\<entry>\\<key>\\<value>yup an entry2\\</value>\\</key>\\<value>just some string2\\</value>\\</entry>\n\\<entry>\\<key>\\<value>yup an entry3\\</value>\\</key>\\<value>just some string3\\</value>\\</entry>\n\\<entry>\\<key>\\<value>yup an entry4\\</value>\\</key>\\<value>just some string4\\</value>\\</entry>\n\\</map>\n\\</constructor-arg>\n\\</bean>\n\n**Unless I specify the constructor-arg type (java.util.Map in this case), Spring throws exception as shown below. The same is true for java.util.Properties class. Fortunately, this is not showstopper, users can still continue.**\n\n**One workaround for this issue is to make the Map first parameter, but this not better than specifying the constructor-arg type in app context.**\n\n**I did some investigation and found this issue comes from protected Map convertToTypedMap(Map original, String propertyName, Class requiredType, TypeDescriptor typeDescriptor) in TypeConvertorDelegate class. I don't understand why this class tries to convert a map to boolean.**\n\njava.lang.InstantiationException: boolean\nat java.lang.Class.newInstance0(Class.java:340)\nat java.lang.Class.newInstance(Class.java:308)\nat org.springframework.beans.TypeConverterDelegate.convertToTypedMap(TypeConverterDelegate.java:634)\nat org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:236)\nat org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:104)\nat org.springframework.beans.BeanWrapperImpl.convertIfNecessary(BeanWrapperImpl.java:419)\nat org.springframework.beans.factory.support.ConstructorResolver.createArgumentArray(ConstructorResolver.java:657)\nat org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:191)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.autowireConstructor(AbstractAutowireCapableBeanFactory.java:984)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBeanInstance(AbstractAutowireCapableBeanFactory.java:888)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:479)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:450)\nat org.springframework.beans.factory.support.AbstractBeanFactory$1.getObject(AbstractBeanFactory.java:290)\nat org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:222)\nat org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:287)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:189)\nat org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:562)\nat org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:871)\nat org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:423)\n\n\n---\n\n**Affects:** 3.0.1\n\n**Attachments:**\n- [repro.patch](https://jira.spring.io/secure/attachment/16330/repro.patch) (_2.36 kB_)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/c13e5f9f5b764e80e52d4e4166652c64ca4d34e6, https://github.com/spring-projects/spring-framework/commit/77bb68b9675f1acb62053b52f0886ded3d0201ea, https://github.com/spring-projects/spring-framework/commit/c38c09bc351e222a9674d48c0bf74ba0a5bc63ad\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453349183"], "labels":["in: core"]}