{"id":"22793", "title":"Different property resolution if property value is empty", "body":"**Affects:** 5.1.6 / 4.3.23 + Spring Tools 4.2.0

```java
@Component
public class PropertiesTest {

	@Value(\"${key}\")
	String property;

        @Value(\"${key}\")
	String[] propertyArr;
	
}
```

If a property `key` is defined without a value in application.properties (i.e. `key=`) the variable `property` is initialized with an empty String and `propertyArr` with an empty String array.

If `key` is defined without a value in a 'Spring Boot App' Eclipse launch configuration instead (\"Override Properties\" feature) the start of the application fails:

```
java.lang.IllegalArgumentException: Could not resolve placeholder 'key' in value \"${key}\"
````

Spring should *always* resolve a property with an empty value as an empty String / String array.
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22793","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22793/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22793/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22793/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22793","id":432956489,"node_id":"MDU6SXNzdWU0MzI5NTY0ODk=","number":22793,"title":"Different property resolution if property value is empty","user":{"login":"ReneTrefft","id":8194552,"node_id":"MDQ6VXNlcjgxOTQ1NTI=","avatar_url":"https://avatars1.githubusercontent.com/u/8194552?v=4","gravatar_id":"","url":"https://api.github.com/users/ReneTrefft","html_url":"https://github.com/ReneTrefft","followers_url":"https://api.github.com/users/ReneTrefft/followers","following_url":"https://api.github.com/users/ReneTrefft/following{/other_user}","gists_url":"https://api.github.com/users/ReneTrefft/gists{/gist_id}","starred_url":"https://api.github.com/users/ReneTrefft/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReneTrefft/subscriptions","organizations_url":"https://api.github.com/users/ReneTrefft/orgs","repos_url":"https://api.github.com/users/ReneTrefft/repos","events_url":"https://api.github.com/users/ReneTrefft/events{/privacy}","received_events_url":"https://api.github.com/users/ReneTrefft/received_events","type":"User","site_admin":false},"labels":[{"id":1225703104,"node_id":"MDU6TGFiZWwxMjI1NzAzMTA0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/for:%20external-project","name":"for: external-project","color":"c5def5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2019-04-14T10:27:44Z","updated_at":"2019-04-17T09:19:43Z","closed_at":"2019-04-14T11:08:35Z","author_association":"NONE","body":"**Affects:** 5.1.6 / 4.3.23 + Spring Tools 4.2.0\r\n\r\n```java\r\n@Component\r\npublic class PropertiesTest {\r\n\r\n\t@Value(\"${key}\")\r\n\tString property;\r\n\r\n        @Value(\"${key}\")\r\n\tString[] propertyArr;\r\n\t\r\n}\r\n```\r\n\r\nIf a property `key` is defined without a value in application.properties (i.e. `key=`) the variable `property` is initialized with an empty String and `propertyArr` with an empty String array.\r\n\r\nIf `key` is defined without a value in a 'Spring Boot App' Eclipse launch configuration instead (\"Override Properties\" feature) the start of the application fails:\r\n\r\n```\r\njava.lang.IllegalArgumentException: Could not resolve placeholder 'key' in value \"${key}\"\r\n````\r\n\r\nSpring should *always* resolve a property with an empty value as an empty String / String array.\r\n","closed_by":{"login":"sbrannen","id":104798,"node_id":"MDQ6VXNlcjEwNDc5OA==","avatar_url":"https://avatars0.githubusercontent.com/u/104798?v=4","gravatar_id":"","url":"https://api.github.com/users/sbrannen","html_url":"https://github.com/sbrannen","followers_url":"https://api.github.com/users/sbrannen/followers","following_url":"https://api.github.com/users/sbrannen/following{/other_user}","gists_url":"https://api.github.com/users/sbrannen/gists{/gist_id}","starred_url":"https://api.github.com/users/sbrannen/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sbrannen/subscriptions","organizations_url":"https://api.github.com/users/sbrannen/orgs","repos_url":"https://api.github.com/users/sbrannen/repos","events_url":"https://api.github.com/users/sbrannen/events{/privacy}","received_events_url":"https://api.github.com/users/sbrannen/received_events","type":"User","site_admin":false}}", "commentIds":["482949214","483204385","483217903","484006437"], "labels":["for: external-project"]}