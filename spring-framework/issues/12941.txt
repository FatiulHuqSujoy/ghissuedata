{"id":"12941", "title":"DelegatingFilterProxy should hide its parameters to proxied filter [SPR-8293]", "body":"**[Nicolas Guillaumin](https://jira.spring.io/secure/ViewProfile.jspa?name=nguillaumin)** opened **[SPR-8293](https://jira.spring.io/browse/SPR-8293?redirect=false)** and commented

When `org.springframework.web.filter.DelegatingFilterProxy` is setup with the `targetFilterLifecycle` init-param set to `true`, the `Filter.init()` method is called on the proxied filter.

That's great, however the proxied filter receive this `targetFilterLifecycle` parameter too. That's problematic with some filters that throws an Exception if an init-parameter is invalid / unrecognized.

Ex:

```
<filter>
  <filter-name>MyFilter</filter-name>
  <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
  <init-param>
    <param-name>targetFilterLifecycle</param-name>
    <param-value>true</param-value>
  </init-param>
  <init-param>
    <param-name>myParam</param-name>
    <param-value>myValue</param-value>
  </init-param> 
</filter>
```

With the above example `MyFilter.init()` will be called with both `MyParam=MyValue` and `targetFilterLifecycle=true`.

`DelegatingFilterProxy` shouldn't pass along its own parameters to the proxied filters.


---

**Affects:** 3.0.5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12941","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12941/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12941/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12941/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12941","id":398112080,"node_id":"MDU6SXNzdWUzOTgxMTIwODA=","number":12941,"title":"DelegatingFilterProxy should hide its parameters to proxied filter [SPR-8293]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2011-05-05T00:00:28Z","updated_at":"2019-01-12T16:25:34Z","closed_at":"2018-12-28T10:42:03Z","author_association":"COLLABORATOR","body":"**[Nicolas Guillaumin](https://jira.spring.io/secure/ViewProfile.jspa?name=nguillaumin)** opened **[SPR-8293](https://jira.spring.io/browse/SPR-8293?redirect=false)** and commented\n\nWhen `org.springframework.web.filter.DelegatingFilterProxy` is setup with the `targetFilterLifecycle` init-param set to `true`, the `Filter.init()` method is called on the proxied filter.\n\nThat's great, however the proxied filter receive this `targetFilterLifecycle` parameter too. That's problematic with some filters that throws an Exception if an init-parameter is invalid / unrecognized.\n\nEx:\n\n```\n<filter>\n  <filter-name>MyFilter</filter-name>\n  <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>\n  <init-param>\n    <param-name>targetFilterLifecycle</param-name>\n    <param-value>true</param-value>\n  </init-param>\n  <init-param>\n    <param-name>myParam</param-name>\n    <param-value>myValue</param-value>\n  </init-param> \n</filter>\n```\n\nWith the above example `MyFilter.init()` will be called with both `MyParam=MyValue` and `targetFilterLifecycle=true`.\n\n`DelegatingFilterProxy` shouldn't pass along its own parameters to the proxied filters.\n\n\n---\n\n**Affects:** 3.0.5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453359183"], "labels":["in: web","status: declined"]}