{"id":"15042", "title":"Properties from @Import-ed Configuration override properties in @Import-ing configuration [SPR-10409]", "body":"**[Paul Brabban](https://jira.spring.io/secure/ViewProfile.jspa?name=brabster)** opened **[SPR-10409](https://jira.spring.io/browse/SPR-10409?redirect=false)** and commented

I have a property `test=default` in class DefaultConfig, and I'm making them available using `@PropertySource` annotation.

```
@Configuration
@PropertySource(\"classpath:default.properties\")
public class DefaultConfig {}
```

I then want to be able to override to `test=override`, which is in a different properties file in class OverrideConfig, so I again use `@PropertySource`.

```
@Configuration
@Import(DefaultConfig.class)
@PropertySource(\"classpath:override.properties\")
public class OverrideConfig {}
```

I configure a test to prove that it works.

```
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={OverrideConfig.class})
public class TestPropertyOverride {
    
    @Autowired
    private Environment env;
    
    @Test
    public void propertyIsOverridden() {
    	assertEquals(\"override\", env.getProperty(\"test\"));
    }
    
}
```

Except of course it does not.

> org.junit.ComparisonFailure: expected:<[override]> but was:<[default]>

Maxing out debug, I can see what's happening:

> StandardEnvironment:107 - Adding [class path resource [default.properties]] PropertySource with lowest search precedence
> StandardEnvironment:107 - Adding [class path resource [override.properties]] PropertySource with lowest search precedence

I can't provide default property values in base configurations and then override them in others.

Am I making a simple mistake or misthinking this, or would you expect the properties defined by an `@PropertySource` in an `@Import-ed` configuration class to be overridden by properties defined in am `@PropertySource` in the `@Import-ing` class?


---

**Affects:** 3.2 GA

**Reference URL:** http://stackoverflow.com/questions/15577125/overriding-spring-propertysource-by-import

4 votes, 7 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15042","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15042/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15042/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15042/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15042","id":398158097,"node_id":"MDU6SXNzdWUzOTgxNTgwOTc=","number":15042,"title":"Properties from @Import-ed Configuration override properties in @Import-ing configuration [SPR-10409]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2013-03-23T01:40:59Z","updated_at":"2019-08-07T17:26:55Z","closed_at":"2018-12-27T12:01:33Z","author_association":"COLLABORATOR","body":"**[Paul Brabban](https://jira.spring.io/secure/ViewProfile.jspa?name=brabster)** opened **[SPR-10409](https://jira.spring.io/browse/SPR-10409?redirect=false)** and commented\n\nI have a property `test=default` in class DefaultConfig, and I'm making them available using `@PropertySource` annotation.\n\n```\n@Configuration\n@PropertySource(\"classpath:default.properties\")\npublic class DefaultConfig {}\n```\n\nI then want to be able to override to `test=override`, which is in a different properties file in class OverrideConfig, so I again use `@PropertySource`.\n\n```\n@Configuration\n@Import(DefaultConfig.class)\n@PropertySource(\"classpath:override.properties\")\npublic class OverrideConfig {}\n```\n\nI configure a test to prove that it works.\n\n```\n@RunWith(SpringJUnit4ClassRunner.class)\n@ContextConfiguration(classes={OverrideConfig.class})\npublic class TestPropertyOverride {\n    \n    @Autowired\n    private Environment env;\n    \n    @Test\n    public void propertyIsOverridden() {\n    \tassertEquals(\"override\", env.getProperty(\"test\"));\n    }\n    \n}\n```\n\nExcept of course it does not.\n\n> org.junit.ComparisonFailure: expected:<[override]> but was:<[default]>\n\nMaxing out debug, I can see what's happening:\n\n> StandardEnvironment:107 - Adding [class path resource [default.properties]] PropertySource with lowest search precedence\n> StandardEnvironment:107 - Adding [class path resource [override.properties]] PropertySource with lowest search precedence\n\nI can't provide default property values in base configurations and then override them in others.\n\nAm I making a simple mistake or misthinking this, or would you expect the properties defined by an `@PropertySource` in an `@Import-ed` configuration class to be overridden by properties defined in am `@PropertySource` in the `@Import-ing` class?\n\n\n---\n\n**Affects:** 3.2 GA\n\n**Reference URL:** http://stackoverflow.com/questions/15577125/overriding-spring-propertysource-by-import\n\n4 votes, 7 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453401970","453401972","453401975","453401976","453401977","519193456"], "labels":["in: core","status: declined"]}