{"id":"22557", "title":"AbstractAdvisingBeanPostProcessor add advisor does not consider aspectJ advisor order", "body":"
```java
// in postProcessAfterInitialization function:
	if (bean instanceof Advised) {
			Advised advised = (Advised) bean;
			if (!advised.isFrozen() && isEligible(AopUtils.getTargetClass(bean))) {
				// Add our local Advisor to the existing proxy's Advisor chain...
				if (this.beforeExistingAdvisors) {
					advised.addAdvisor(0, this.advisor);
				}
				else {
					advised.addAdvisor(this.advisor);
				}
				return bean;
			}
		}
```
this piece of code does not consider advisor order value.
change code to: 
```java
      if (bean instanceof Advised) {
            Advised advised = (Advised)bean;
            if (!advised.isFrozen() && isEligible(AopUtils.getTargetClass(bean))) {
                // Add our local Advisor to the existing proxy's Advisor chain...
                //re-order advisors according to their order value

                //if (this.beforeExistingAdvisors) {
                //    advised.addAdvisor(0, this.advisor);
                //} else {
                //    advised.addAdvisor(this.advisor);
                //}
                List<Advisor> advisorList = Lists.newArrayList(advised.getAdvisors());
                advisorList.add(this.advisor);
                List <Advisor> orderedAdvisorList = sortAdvisors(advisorList);
                int rightPos = orderedAdvisorList.indexOf(this.advisor);
                advised.addAdvisor(rightPos, this.advisor);
                return bean;
            }
        }
```
solved my problem.
---
<!--
Thanks for taking the time to create an issue. Please read the following:

- Questions should be asked on Stack Overflow.
- For bugs, specify affected versions and explain what you are trying to do.
- For enhancements, provide context and describe the problem.

Issue or Pull Request? Create only one, not both. GitHub treats them as the same.
If unsure, start with an issue, and if you submit a pull request later, the
issue will be closed as superseded.
-->", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22557","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22557/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22557/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22557/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22557","id":419267881,"node_id":"MDU6SXNzdWU0MTkyNjc4ODE=","number":22557,"title":"AbstractAdvisingBeanPostProcessor add advisor does not consider aspectJ advisor order","user":{"login":"laohou","id":2869706,"node_id":"MDQ6VXNlcjI4Njk3MDY=","avatar_url":"https://avatars1.githubusercontent.com/u/2869706?v=4","gravatar_id":"","url":"https://api.github.com/users/laohou","html_url":"https://github.com/laohou","followers_url":"https://api.github.com/users/laohou/followers","following_url":"https://api.github.com/users/laohou/following{/other_user}","gists_url":"https://api.github.com/users/laohou/gists{/gist_id}","starred_url":"https://api.github.com/users/laohou/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/laohou/subscriptions","organizations_url":"https://api.github.com/users/laohou/orgs","repos_url":"https://api.github.com/users/laohou/repos","events_url":"https://api.github.com/users/laohou/events{/privacy}","received_events_url":"https://api.github.com/users/laohou/received_events","type":"User","site_admin":false},"labels":[{"id":1188511975,"node_id":"MDU6TGFiZWwxMTg4NTExOTc1","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20invalid","name":"status: invalid","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-03-11T02:19:08Z","updated_at":"2019-03-11T13:50:31Z","closed_at":"2019-03-11T10:46:38Z","author_association":"NONE","body":"\r\n```java\r\n// in postProcessAfterInitialization function:\r\n\tif (bean instanceof Advised) {\r\n\t\t\tAdvised advised = (Advised) bean;\r\n\t\t\tif (!advised.isFrozen() && isEligible(AopUtils.getTargetClass(bean))) {\r\n\t\t\t\t// Add our local Advisor to the existing proxy's Advisor chain...\r\n\t\t\t\tif (this.beforeExistingAdvisors) {\r\n\t\t\t\t\tadvised.addAdvisor(0, this.advisor);\r\n\t\t\t\t}\r\n\t\t\t\telse {\r\n\t\t\t\t\tadvised.addAdvisor(this.advisor);\r\n\t\t\t\t}\r\n\t\t\t\treturn bean;\r\n\t\t\t}\r\n\t\t}\r\n```\r\nthis piece of code does not consider advisor order value.\r\nchange code to: \r\n```java\r\n      if (bean instanceof Advised) {\r\n            Advised advised = (Advised)bean;\r\n            if (!advised.isFrozen() && isEligible(AopUtils.getTargetClass(bean))) {\r\n                // Add our local Advisor to the existing proxy's Advisor chain...\r\n                //re-order advisors according to their order value\r\n\r\n                //if (this.beforeExistingAdvisors) {\r\n                //    advised.addAdvisor(0, this.advisor);\r\n                //} else {\r\n                //    advised.addAdvisor(this.advisor);\r\n                //}\r\n                List<Advisor> advisorList = Lists.newArrayList(advised.getAdvisors());\r\n                advisorList.add(this.advisor);\r\n                List <Advisor> orderedAdvisorList = sortAdvisors(advisorList);\r\n                int rightPos = orderedAdvisorList.indexOf(this.advisor);\r\n                advised.addAdvisor(rightPos, this.advisor);\r\n                return bean;\r\n            }\r\n        }\r\n```\r\nsolved my problem.\r\n---\r\n<!--\r\nThanks for taking the time to create an issue. Please read the following:\r\n\r\n- Questions should be asked on Stack Overflow.\r\n- For bugs, specify affected versions and explain what you are trying to do.\r\n- For enhancements, provide context and describe the problem.\r\n\r\nIssue or Pull Request? Create only one, not both. GitHub treats them as the same.\r\nIf unsure, start with an issue, and if you submit a pull request later, the\r\nissue will be closed as superseded.\r\n-->","closed_by":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}}", "commentIds":["471490666"], "labels":["status: invalid"]}