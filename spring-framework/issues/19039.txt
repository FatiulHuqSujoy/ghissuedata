{"id":"19039", "title":"Jackson 2.6.x -> 2.7 deserialize generic fails when calling Spring controller [SPR-14470]", "body":"**[Wagner Michael](https://jira.spring.io/secure/ViewProfile.jspa?name=maffelbaffel)** opened **[SPR-14470](https://jira.spring.io/browse/SPR-14470?redirect=false)** and commented

I'm just trying to upgrade (Spring 4.3 project) from jackson 2.6.3 to 2.7 and getting an error.

This is my simplyfied project:

Abstract controller class with a http post method which accepts a requestBody of a generic list:

```
public abstract class AbstractController<T extends Entity> {

    @RequestMapping(value = \"/\", method = POST)
    public void method1(@RequestBody final List<T> entities){
        System.out.println(\"AbstractController called\");
    }
}
```

Controller implementation with a simple Entity class

```
@RestController
@RequestMapping(\"/impl1\")
public class ControllerImpl extends AbstractController<EntityImpl> {
}
```

second controller with method1 overriden

```
@RestController
@RequestMapping(\"/impl2\")
public class ControllerImpl2 extends AbstractController<EntityImpl2> {

    @Override
    public void method1(@RequestBody List<EntityImpl2> entities) {
        System.out.println(\"ControllerImpl2 called\");
    }
}
```

My Entity:

```
public class EntityImpl implements Entity<Integer> {

    private String name;

    public EntityImpl() {
    }

    public EntityImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }
}
```

Ok, so this setup works fine with Jackson 2.6.x and Spring 4.2.x, but i need to upgrade to at least 2.7.
But with 2.7 every post request to \"/impl1/\" gives me this error:

```
Failed to read HTTP message: org.springframework.http.converter.HttpMessageNotReadableException: Could not read document: Can not construct instance of jackson.entity.Entity, problem: abstract types either need to be mapped to concrete types, have custom deserializer, or be instantiated with additional type information
     at [Source: java.io.PushbackInputStream@1aa5c56a; line: 1, column: 2] (through reference chain: java.util.ArrayList[0]); nested exception is com.fasterxml.jackson.databind.JsonMappingException: Can not construct instance of jackson.entity.Entity, problem: abstract types either need to be mapped to concrete types, have custom deserializer, or be instantiated with additional type information
     at [Source: java.io.PushbackInputStream@1aa5c56a; line: 1, column: 2] (through reference chain: java.util.ArrayList[0])
```

Thought posts to /impl2/ workwith both versions ... but i really dont want to override every method there.

Is there a way to solve this?
I have created a sample project with some pretty easy tests showing this behavior.
Just run

```
./gradlew build
```

If you open gradle.build and remove these lines:

```
// uncomment both for failing tests!!
    compile \"com.fasterxml.jackson.core:jackson-databind:2.7.0\"
    compile \"org.springframework:spring-webmvc:4.3.0.RELEASE\"
```

Tests are working fine then. (Jackson 2.6 and Spring 4.2 are being used then).
Am i missing any configuration there, which was not needed prior jackson 2.7?

Hope someone can help me here :)

---

**Affects:** 4.3.1

**Attachments:**
- [spring-jackson.zip](https://jira.spring.io/secure/attachment/23440/spring-jackson.zip) (_56.52 kB_)

**Issue Links:**
- #18301 Revise AbstractJackson2HttpMessageConverter's generic type adaptation
- #19928 Generic type is not used sometimes
- #19089 StackOverflowError at AbstractJackson2HttpMessageConverter.resolveVariable

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19039","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19039/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19039/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19039/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19039","id":398195450,"node_id":"MDU6SXNzdWUzOTgxOTU0NTA=","number":19039,"title":"Jackson 2.6.x -> 2.7 deserialize generic fails when calling Spring controller [SPR-14470]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false},"assignees":[{"login":"sdeleuze","id":141109,"node_id":"MDQ6VXNlcjE0MTEwOQ==","avatar_url":"https://avatars2.githubusercontent.com/u/141109?v=4","gravatar_id":"","url":"https://api.github.com/users/sdeleuze","html_url":"https://github.com/sdeleuze","followers_url":"https://api.github.com/users/sdeleuze/followers","following_url":"https://api.github.com/users/sdeleuze/following{/other_user}","gists_url":"https://api.github.com/users/sdeleuze/gists{/gist_id}","starred_url":"https://api.github.com/users/sdeleuze/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/sdeleuze/subscriptions","organizations_url":"https://api.github.com/users/sdeleuze/orgs","repos_url":"https://api.github.com/users/sdeleuze/repos","events_url":"https://api.github.com/users/sdeleuze/events{/privacy}","received_events_url":"https://api.github.com/users/sdeleuze/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/151","html_url":"https://github.com/spring-projects/spring-framework/milestone/151","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/151/labels","id":3960924,"node_id":"MDk6TWlsZXN0b25lMzk2MDkyNA==","number":151,"title":"4.3.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":37,"state":"closed","created_at":"2019-01-10T22:04:53Z","updated_at":"2019-01-11T09:11:26Z","due_on":"2016-07-27T07:00:00Z","closed_at":"2019-01-10T22:04:53Z"},"comments":3,"created_at":"2016-07-15T06:50:39Z","updated_at":"2019-01-11T16:22:13Z","closed_at":"2017-03-21T07:53:22Z","author_association":"COLLABORATOR","body":"**[Wagner Michael](https://jira.spring.io/secure/ViewProfile.jspa?name=maffelbaffel)** opened **[SPR-14470](https://jira.spring.io/browse/SPR-14470?redirect=false)** and commented\n\nI'm just trying to upgrade (Spring 4.3 project) from jackson 2.6.3 to 2.7 and getting an error.\n\nThis is my simplyfied project:\n\nAbstract controller class with a http post method which accepts a requestBody of a generic list:\n\n```\npublic abstract class AbstractController<T extends Entity> {\n\n    @RequestMapping(value = \"/\", method = POST)\n    public void method1(@RequestBody final List<T> entities){\n        System.out.println(\"AbstractController called\");\n    }\n}\n```\n\nController implementation with a simple Entity class\n\n```\n@RestController\n@RequestMapping(\"/impl1\")\npublic class ControllerImpl extends AbstractController<EntityImpl> {\n}\n```\n\nsecond controller with method1 overriden\n\n```\n@RestController\n@RequestMapping(\"/impl2\")\npublic class ControllerImpl2 extends AbstractController<EntityImpl2> {\n\n    @Override\n    public void method1(@RequestBody List<EntityImpl2> entities) {\n        System.out.println(\"ControllerImpl2 called\");\n    }\n}\n```\n\nMy Entity:\n\n```\npublic class EntityImpl implements Entity<Integer> {\n\n    private String name;\n\n    public EntityImpl() {\n    }\n\n    public EntityImpl(String name) {\n        this.name = name;\n    }\n\n    @Override\n    public String getName() {\n        return name;\n    }\n\n    @Override\n    public void setName(final String name) {\n        this.name = name;\n    }\n}\n```\n\nOk, so this setup works fine with Jackson 2.6.x and Spring 4.2.x, but i need to upgrade to at least 2.7.\nBut with 2.7 every post request to \"/impl1/\" gives me this error:\n\n```\nFailed to read HTTP message: org.springframework.http.converter.HttpMessageNotReadableException: Could not read document: Can not construct instance of jackson.entity.Entity, problem: abstract types either need to be mapped to concrete types, have custom deserializer, or be instantiated with additional type information\n     at [Source: java.io.PushbackInputStream@1aa5c56a; line: 1, column: 2] (through reference chain: java.util.ArrayList[0]); nested exception is com.fasterxml.jackson.databind.JsonMappingException: Can not construct instance of jackson.entity.Entity, problem: abstract types either need to be mapped to concrete types, have custom deserializer, or be instantiated with additional type information\n     at [Source: java.io.PushbackInputStream@1aa5c56a; line: 1, column: 2] (through reference chain: java.util.ArrayList[0])\n```\n\nThought posts to /impl2/ workwith both versions ... but i really dont want to override every method there.\n\nIs there a way to solve this?\nI have created a sample project with some pretty easy tests showing this behavior.\nJust run\n\n```\n./gradlew build\n```\n\nIf you open gradle.build and remove these lines:\n\n```\n// uncomment both for failing tests!!\n    compile \"com.fasterxml.jackson.core:jackson-databind:2.7.0\"\n    compile \"org.springframework:spring-webmvc:4.3.0.RELEASE\"\n```\n\nTests are working fine then. (Jackson 2.6 and Spring 4.2 are being used then).\nAm i missing any configuration there, which was not needed prior jackson 2.7?\n\nHope someone can help me here :)\n\n---\n\n**Affects:** 4.3.1\n\n**Attachments:**\n- [spring-jackson.zip](https://jira.spring.io/secure/attachment/23440/spring-jackson.zip) (_56.52 kB_)\n\n**Issue Links:**\n- #18301 Revise AbstractJackson2HttpMessageConverter's generic type adaptation\n- #19928 Generic type is not used sometimes\n- #19089 StackOverflowError at AbstractJackson2HttpMessageConverter.resolveVariable\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453442168","453442169","453442170"], "labels":["type: bug"]}