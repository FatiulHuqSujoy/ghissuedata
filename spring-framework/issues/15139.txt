{"id":"15139", "title":"AbstractAutowireCapableBeanFactory.predictBeanType returns parent type instead of class=\"<real class>\" value [SPR-10507]", "body":"**[pascal gehl](https://jira.spring.io/secure/ViewProfile.jspa?name=paskos)** opened **[SPR-10507](https://jira.spring.io/browse/SPR-10507?redirect=false)** and commented

Introduction:
I have some code that used to work since 2-3 years.
I just moved from version 3.1.3.RELEASE to 3.2.2.RELEASE.
And suddenly it stopped working.

Here are my beans:

```xml
 
<bean id=\"abstractIsAliveResource\" abstract=\"true\" class=\"com.cie.isalive.server.rest.AbstractIsAliveResource\">
      <property name=\"enabled\" ref=\"isAliveEnabled\" />
</bean>

<bean id=\"simpleIsAliveResource\" parent=\"abstractIsAliveResource\" class=\"com.cie.isalive.server.rest.SimpleIsAliveResource\" />

<bean id=\"isAliveResource\" parent=\"abstractIsAliveResource\" class=\"com.cie.isalive.server.rest.IsAliveResource\">
      <property name=\"showall\" value=\"${status.isalive.showall}\" />
</bean>
```

Both isAliveResource and simpleIsAliveResource are JAX-RS annotated classes.

```
@Path(IS_ALIVE_URI)
@Produces(APPLICATION_XML)
public class IsAliveResource extends AbstractIsAliveResource {
```

```
@Path(IS_SIMPLE_ALIVE_URI)
public class SimpleIsAliveResource extends AbstractIsAliveResource {
```

Somewhere else in my code I do:

```
Set<Object> resources = getApplicationContext().getBeansWithAnnotation(Path.class);
```

In 3.1.3.RELEASE both isAliveResource and simpleIsAliveResource where found.
Since moving to 3.2.2.RELEASE none of them are found.
I debugged and finally found that AbstractAutowireCapableBeanFactory.predictBeanType changed and now returns
com.cie.isalive.server.rest.AbstractIsAliveResource class instead of respective beans com.cie.isalive.server.rest.SimpleIsAliveResource and com.cie.isalive.server.rest.IsAliveResource.

Since AbstractIsAliveResource is not annotated then the beans are not found.

It is a major issue since we cannot use abstract beans for default annotated beans configuration anymore.

Thanks in advance.

Pascal

---

**Affects:** 3.2.2

**Issue Links:**
- #15007 Type detection fails for child bean if parent bean has been resolved before (_**\"duplicates\"**_)

", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15139","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15139/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15139/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/15139/events","html_url":"https://github.com/spring-projects/spring-framework/issues/15139","id":398158708,"node_id":"MDU6SXNzdWUzOTgxNTg3MDg=","number":15139,"title":"AbstractAutowireCapableBeanFactory.predictBeanType returns parent type instead of class=\"<real class>\" value [SPR-10507]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/91","html_url":"https://github.com/spring-projects/spring-framework/milestone/91","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/91/labels","id":3960864,"node_id":"MDk6TWlsZXN0b25lMzk2MDg2NA==","number":91,"title":"3.2.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":35,"state":"closed","created_at":"2019-01-10T22:03:40Z","updated_at":"2019-01-11T07:10:59Z","due_on":"2013-05-20T07:00:00Z","closed_at":"2019-01-10T22:03:40Z"},"comments":0,"created_at":"2013-04-30T13:58:03Z","updated_at":"2019-01-11T22:18:51Z","closed_at":"2013-05-16T08:33:37Z","author_association":"COLLABORATOR","body":"**[pascal gehl](https://jira.spring.io/secure/ViewProfile.jspa?name=paskos)** opened **[SPR-10507](https://jira.spring.io/browse/SPR-10507?redirect=false)** and commented\n\nIntroduction:\nI have some code that used to work since 2-3 years.\nI just moved from version 3.1.3.RELEASE to 3.2.2.RELEASE.\nAnd suddenly it stopped working.\n\nHere are my beans:\n\n```xml\n \n<bean id=\"abstractIsAliveResource\" abstract=\"true\" class=\"com.cie.isalive.server.rest.AbstractIsAliveResource\">\n      <property name=\"enabled\" ref=\"isAliveEnabled\" />\n</bean>\n\n<bean id=\"simpleIsAliveResource\" parent=\"abstractIsAliveResource\" class=\"com.cie.isalive.server.rest.SimpleIsAliveResource\" />\n\n<bean id=\"isAliveResource\" parent=\"abstractIsAliveResource\" class=\"com.cie.isalive.server.rest.IsAliveResource\">\n      <property name=\"showall\" value=\"${status.isalive.showall}\" />\n</bean>\n```\n\nBoth isAliveResource and simpleIsAliveResource are JAX-RS annotated classes.\n\n```\n@Path(IS_ALIVE_URI)\n@Produces(APPLICATION_XML)\npublic class IsAliveResource extends AbstractIsAliveResource {\n```\n\n```\n@Path(IS_SIMPLE_ALIVE_URI)\npublic class SimpleIsAliveResource extends AbstractIsAliveResource {\n```\n\nSomewhere else in my code I do:\n\n```\nSet<Object> resources = getApplicationContext().getBeansWithAnnotation(Path.class);\n```\n\nIn 3.1.3.RELEASE both isAliveResource and simpleIsAliveResource where found.\nSince moving to 3.2.2.RELEASE none of them are found.\nI debugged and finally found that AbstractAutowireCapableBeanFactory.predictBeanType changed and now returns\ncom.cie.isalive.server.rest.AbstractIsAliveResource class instead of respective beans com.cie.isalive.server.rest.SimpleIsAliveResource and com.cie.isalive.server.rest.IsAliveResource.\n\nSince AbstractIsAliveResource is not annotated then the beans are not found.\n\nIt is a major issue since we cannot use abstract beans for default annotated beans configuration anymore.\n\nThanks in advance.\n\nPascal\n\n---\n\n**Affects:** 3.2.2\n\n**Issue Links:**\n- #15007 Type detection fails for child bean if parent bean has been resolved before (_**\"duplicates\"**_)\n\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["in: core","type: bug"]}