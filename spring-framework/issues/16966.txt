{"id":"16966", "title":"Add support for DELETE with body to RestTemplate through exchange method [SPR-12361]", "body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-12361](https://jira.spring.io/browse/SPR-12361?redirect=false)** and commented

If I delete a resource through an **id** and with the **delete** method, it works fine

```java
restTemplate.delete(\"http://localhost:8080/spring-utility/person/{id}/one\", id);
```

But **delete** method is void, _therefore_ I can't get reply and status from the server.

Just playing, through the **exchange** method together with HttpMethod.PUT, I can do the following:

```java
ResponseEntity<String> response = 
			restTemplate.exchange(\"http://localhost:8080/spring-utility/person/{id}\", 
							   	   HttpMethod.PUT, 
							      new HttpEntity<Person>(person), 
							         String.class, 
							         person.getId());
```

And works fine, until here two points
1. I can get a a reply and status from the server
2. I can send a request body

Thinking in the same idea, I have tried use the **exchange** method together with HttpMethod.DELETE

```java
ResponseEntity<String> response =
		restTemplate.exchange(\"http://localhost:8080/spring-utility/person/{id}/two\", 
						HttpMethod.DELETE, 
					        new HttpEntity<Person>(person), 
					        String.class, 
					        person.getId());	
```

It with the intention to
1. get a a reply and status from the server
2. send a request body - better control against send an **id**

**Observe:** I want send an object to be deleted, not an id.

But always I get the following:

```
DELETE request for \"http://localhost:8080/spring-utility/person/1/two\" resulted in 400 (Bad Request); invoking error handler
Exception in thread \"main\" org.springframework.web.client.HttpClientErrorException: 400 Bad Request
```

I did a research:

* [Add support for DELETE with body to RestTemplate](#12524) it for Spring 3, I don't know if Spring 4 has other intentions.
* [Spring RestTemplate calling the Delete method with a request body (Delete With Request Body)](http://knowledgebrowse.blogspot.com/2013/08/spring-resttemplate-calling-delete.html) he has created its own solution
* [HttpMethod.Delete not working with RestTemplate of Spring-Android](http://stackoverflow.com/questions/18532240/httpmethod-delete-not-working-with-resttemplate-of-spring-android) he has direct access to the API

According with the links: Delete has no support for request body. Wondered why this behaviour
Could be improved it the API?

---

**Affects:** 4.0.7, 4.1.1

**Issue Links:**
- #18637 RestTemplate doesnt support DELETE with RequestBody (_**\"is duplicated by\"**_)
- #12524 Add support for DELETE with body to RestTemplate
- #16896 Add support for DELETE with body to AsyncRestTemplate
- #18665 AsyncRestTemplate should trigger no-output HTTP requests immediately as well

0 votes, 6 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16966","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16966/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16966/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16966/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16966","id":398173258,"node_id":"MDU6SXNzdWUzOTgxNzMyNTg=","number":16966,"title":"Add support for DELETE with body to RestTemplate through exchange method [SPR-12361]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/126","html_url":"https://github.com/spring-projects/spring-framework/milestone/126","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/126/labels","id":3960899,"node_id":"MDk6TWlsZXN0b25lMzk2MDg5OQ==","number":126,"title":"4.1.2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":108,"state":"closed","created_at":"2019-01-10T22:04:23Z","updated_at":"2019-01-11T08:05:48Z","due_on":"2014-11-10T08:00:00Z","closed_at":"2019-01-10T22:04:23Z"},"comments":10,"created_at":"2014-10-22T07:30:15Z","updated_at":"2019-01-14T05:14:29Z","closed_at":"2016-03-29T09:48:07Z","author_association":"COLLABORATOR","body":"**[Manuel Jordan](https://jira.spring.io/secure/ViewProfile.jspa?name=dr_pompeii)** opened **[SPR-12361](https://jira.spring.io/browse/SPR-12361?redirect=false)** and commented\n\nIf I delete a resource through an **id** and with the **delete** method, it works fine\n\n```java\nrestTemplate.delete(\"http://localhost:8080/spring-utility/person/{id}/one\", id);\n```\n\nBut **delete** method is void, _therefore_ I can't get reply and status from the server.\n\nJust playing, through the **exchange** method together with HttpMethod.PUT, I can do the following:\n\n```java\nResponseEntity<String> response = \n\t\t\trestTemplate.exchange(\"http://localhost:8080/spring-utility/person/{id}\", \n\t\t\t\t\t\t\t   \t   HttpMethod.PUT, \n\t\t\t\t\t\t\t      new HttpEntity<Person>(person), \n\t\t\t\t\t\t\t         String.class, \n\t\t\t\t\t\t\t         person.getId());\n```\n\nAnd works fine, until here two points\n1. I can get a a reply and status from the server\n2. I can send a request body\n\nThinking in the same idea, I have tried use the **exchange** method together with HttpMethod.DELETE\n\n```java\nResponseEntity<String> response =\n\t\trestTemplate.exchange(\"http://localhost:8080/spring-utility/person/{id}/two\", \n\t\t\t\t\t\tHttpMethod.DELETE, \n\t\t\t\t\t        new HttpEntity<Person>(person), \n\t\t\t\t\t        String.class, \n\t\t\t\t\t        person.getId());\t\n```\n\nIt with the intention to\n1. get a a reply and status from the server\n2. send a request body - better control against send an **id**\n\n**Observe:** I want send an object to be deleted, not an id.\n\nBut always I get the following:\n\n```\nDELETE request for \"http://localhost:8080/spring-utility/person/1/two\" resulted in 400 (Bad Request); invoking error handler\nException in thread \"main\" org.springframework.web.client.HttpClientErrorException: 400 Bad Request\n```\n\nI did a research:\n\n* [Add support for DELETE with body to RestTemplate](#12524) it for Spring 3, I don't know if Spring 4 has other intentions.\n* [Spring RestTemplate calling the Delete method with a request body (Delete With Request Body)](http://knowledgebrowse.blogspot.com/2013/08/spring-resttemplate-calling-delete.html) he has created its own solution\n* [HttpMethod.Delete not working with RestTemplate of Spring-Android](http://stackoverflow.com/questions/18532240/httpmethod-delete-not-working-with-resttemplate-of-spring-android) he has direct access to the API\n\nAccording with the links: Delete has no support for request body. Wondered why this behaviour\nCould be improved it the API?\n\n---\n\n**Affects:** 4.0.7, 4.1.1\n\n**Issue Links:**\n- #18637 RestTemplate doesnt support DELETE with RequestBody (_**\"is duplicated by\"**_)\n- #12524 Add support for DELETE with body to RestTemplate\n- #16896 Add support for DELETE with body to AsyncRestTemplate\n- #18665 AsyncRestTemplate should trigger no-output HTTP requests immediately as well\n\n0 votes, 6 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453418748","453418750","453418752","453418753","453418754","453418757","453418758","453418759","453418760","453418763"], "labels":["in: web","type: enhancement"]}