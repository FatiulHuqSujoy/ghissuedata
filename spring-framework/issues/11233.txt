{"id":"11233", "title":"Annotation configuration based TestContext [SPR-6567]", "body":"**[Keesun Baik](https://jira.spring.io/secure/ViewProfile.jspa?name=keesun)** opened **[SPR-6567](https://jira.spring.io/browse/SPR-6567?redirect=false)** and commented

Why don't you add AnnotationContextLoader that supports annotation configuration based TestContext.

for example. If there is an annotation configuration like this

`@Configuration`
public class SpringAnnotationConfigTestAppConfig {

    @Bean
    public String name(){
        return \"keesun\";
    }

}

and, at the same package, I can make test like this.

`@RunWith`(SpringJUnit4ClassRunner.class)
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\".\"})
public class SpringAnnotationConfigTest {

    @Autowired ApplicationContext ac;
    @Autowired String name;
    
    @Test
    public void di(){
        assertNotNull(ac);
        assertThat(name, is(\"keesun\"));
    }

}

then, conventionally SpringJUnit4ClassRunner finds SpringAnnotationConfigTest + \"AppConfig.java\" for an annotation configuration.

Of course, It makes sence that we sould use 'locations' attribute of `@ContextConfigruation` like these.

`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample/SpringAnnotationConfigTestAppConfig.java\"})
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample/\"})
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample\"})
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"./SpringAnnotationConfigTestAppConfig.java\"})
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"./\"})
`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\".\"})

with '.Java' locations can be used to AnnotationConfigurationApplicationContext.regist(), and without '.java' locations will be used to AnnotationConfigurationApplicationContext.scan().

What do you think about this?

---

**Affects:** 3.0 RC3

**Attachments:**
- [AnnotationContextLoader.java](https://jira.spring.io/secure/attachment/16009/AnnotationContextLoader.java) (_5.89 kB_)
- [AnnotationContextLoaderTest.java](https://jira.spring.io/secure/attachment/16010/AnnotationContextLoaderTest.java) (_1.95 kB_)
- [SpringAnnotationConfigTest.java](https://jira.spring.io/secure/attachment/16011/SpringAnnotationConfigTest.java) (_1.61 kB_)
- [SpringAnnotationConfigTestAppConfig.java](https://jira.spring.io/secure/attachment/16012/SpringAnnotationConfigTestAppConfig.java) (_476 bytes_)

**Issue Links:**
- #10852 Provide TestContext support for `@Configuration` classes (_**\"duplicates\"**_)

3 votes, 5 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11233","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11233/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11233/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11233/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11233","id":398100929,"node_id":"MDU6SXNzdWUzOTgxMDA5Mjk=","number":11233,"title":"Annotation configuration based TestContext [SPR-6567]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188511877,"node_id":"MDU6TGFiZWwxMTg4NTExODc3","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20duplicate","name":"status: duplicate","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2009-12-15T15:12:31Z","updated_at":"2019-01-13T07:56:24Z","closed_at":"2010-08-24T08:01:55Z","author_association":"COLLABORATOR","body":"**[Keesun Baik](https://jira.spring.io/secure/ViewProfile.jspa?name=keesun)** opened **[SPR-6567](https://jira.spring.io/browse/SPR-6567?redirect=false)** and commented\n\nWhy don't you add AnnotationContextLoader that supports annotation configuration based TestContext.\n\nfor example. If there is an annotation configuration like this\n\n`@Configuration`\npublic class SpringAnnotationConfigTestAppConfig {\n\n    @Bean\n    public String name(){\n        return \"keesun\";\n    }\n\n}\n\nand, at the same package, I can make test like this.\n\n`@RunWith`(SpringJUnit4ClassRunner.class)\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\".\"})\npublic class SpringAnnotationConfigTest {\n\n    @Autowired ApplicationContext ac;\n    @Autowired String name;\n    \n    @Test\n    public void di(){\n        assertNotNull(ac);\n        assertThat(name, is(\"keesun\"));\n    }\n\n}\n\nthen, conventionally SpringJUnit4ClassRunner finds SpringAnnotationConfigTest + \"AppConfig.java\" for an annotation configuration.\n\nOf course, It makes sence that we sould use 'locations' attribute of `@ContextConfigruation` like these.\n\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample/SpringAnnotationConfigTestAppConfig.java\"})\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample/\"})\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"/sandbox/springtest/sample\"})\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"./SpringAnnotationConfigTestAppConfig.java\"})\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\"./\"})\n`@ContextConfiguration`(loader = AnnotationContextLoader.class, locations = {\".\"})\n\nwith '.Java' locations can be used to AnnotationConfigurationApplicationContext.regist(), and without '.java' locations will be used to AnnotationConfigurationApplicationContext.scan().\n\nWhat do you think about this?\n\n---\n\n**Affects:** 3.0 RC3\n\n**Attachments:**\n- [AnnotationContextLoader.java](https://jira.spring.io/secure/attachment/16009/AnnotationContextLoader.java) (_5.89 kB_)\n- [AnnotationContextLoaderTest.java](https://jira.spring.io/secure/attachment/16010/AnnotationContextLoaderTest.java) (_1.95 kB_)\n- [SpringAnnotationConfigTest.java](https://jira.spring.io/secure/attachment/16011/SpringAnnotationConfigTest.java) (_1.61 kB_)\n- [SpringAnnotationConfigTestAppConfig.java](https://jira.spring.io/secure/attachment/16012/SpringAnnotationConfigTestAppConfig.java) (_476 bytes_)\n\n**Issue Links:**\n- #10852 Provide TestContext support for `@Configuration` classes (_**\"duplicates\"**_)\n\n3 votes, 5 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453345681","453345683","453345684"], "labels":["in: test","status: duplicate","type: enhancement"]}