{"id":"9606", "title":"Spring inconsistently resolves an overloaded setter method [SPR-4931]", "body":"**[Fred Muhlenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=fmuhlenberg)** opened **[SPR-4931](https://jira.spring.io/browse/SPR-4931?redirect=false)** and commented

Reference: http://forum.springframework.org/showthread.php?t=47051

I'm running into an issue with an overloaded set method.  
I get different method resolutions depending on the environment in which I run.

I am using the  MimeMessageHelper class and am setting the replyTo property and have sample code demonstrating my issue when configuring.

If I run in eclipse, the property requires the String version of the setter.
If I debug in eclipse, the property requires the InternetAddress version of the setter.

If I run from the command line, the property requires the InternetAddress version of the setter.
If I run on our (ancient) 1.4 Oracle App server  I need the String version of the setter.

Same source files, same config files, different results.

I assert that if I pass in a bean reference, Spring ought to resolve to an overloaded method to the type of the reference.

My sample test:

---

Directory Hierarchy

---

./.classpath
./.project
./build.xml
./lib/activation.jar
./lib/commons-logging-1.0.4.jar
./lib/j2ee.jar
./lib/junit-3.8.1.jar
./lib/junit-4.4.jar
./lib/log4j-1.2.14.jar
./lib/mail.jar
./lib/spring-mock.jar
./lib/spring.jar
./test/java/context-one.xml
./test/java/context-two.xml
./test/java/log4j.properties
./test/java/mderf/MockMimeMessageHelper.java
./test/java/mderf/OneTest.java
./test/java/mderf/TwoTest.java

---

build.xml
\\<project name=\"mailproto\" default=\"all\" basedir=\".\" >

    <property name=\"lib.dir\"         location=\"lib\" />
    
    <property name=\"build.dir\"           location=\"build\" />
    <property name=\"build.classes.dir\"   location=\"${build.dir}\" />
    <property name=\"build.report.dir\"    location=\"${build.dir}/report\" />
    
    <property name=\"test.dir\"         location=\"test\" />
    <property name=\"test.java.dir\"    location=\"${test.dir}/java\" />
    
    <path id=\"compile.classpath\">
        <fileset dir=\"${lib.dir}\">
            <include name=\"**/*.jar\"/>

\\<!--            \\<exclude name=\"**/junit-4.4.jar\"/> -->

            <exclude name=\"**/junit-3.8.1.jar\"/>
        </fileset>
    </path>    
    
    <path id=\"test.classpath\">
        <path refid=\"compile.classpath\" />
        <path location=\"${build.classes.dir}\" />
    </path>
    
    
    <target name=\"init\">
        <mkdir dir=\"${build.classes.dir}\"/>
        <mkdir dir=\"${build.report.dir}\"/>
    </target>
    
    
    <target name=\"all\" depends=\"run\">
    
    </target>
        
    <target name=\"compile\" depends=\"init\">
        
        <javac
            destdir=\"${build.classes.dir}\"
            srcdir=\"${test.java.dir}\"
        >
            <classpath refid=\"compile.classpath\" />
        </javac>
        
        <copy todir=\"${build.classes.dir}\">
            <fileset dir=\"${test.java.dir}\">
                <include name=\"*.xml\"/>
            </fileset>
        </copy>
    
    </target>
    
    <target name=\"run\" depends=\"compile\">
        
        <junit
            printsummary=\"true\"
            fork=\"yes\" 
            haltonerror=\"false\"
            showoutput=\"true\"
        >
            <classpath refid=\"test.classpath\"/>
            <formatter type=\"xml\"/>
            <batchtest todir=\"${build.report.dir}\">
                <fileset dir=\"${build.dir}\">
                    <include name=\"**/*Test.class\"/>
                </fileset>
            </batchtest>
    
        </junit>
        
        <junitreport todir=\"${build.report.dir}\">
            <fileset dir=\"${build.report.dir}\">
                <include name=\"TEST-*.xml\"/>
            </fileset>
            <report format=\"frames\" todir=\"${build.report.dir}\"/>
        </junitreport>
        
        <echo>

The JUnit report can be found in ${build.report.dir}/index.html

        </echo>
        
    </target>
    
    <target name=\"clean\">
    
        <delete dir=\"${build.dir}\"/>
        
    </target>

\\</project>

---

context-one.xml

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>
\\<!DOCTYPE beans PUBLIC \"-//SPRING/DTD BEAN/EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">
\\<beans>
\\<!--!-- **************************************** -->

    <!-- Beans associated with email notification -->
    <!-- **************************************** -->
    
    <!-- Address of email server -->
    <bean id=\"javaMailSender\" class=\"org.springframework.mail.javamail.JavaMailSenderImpl\">
        <property name=\"host\"><value>localhost</value></property>
    </bean>
    
    <bean id=\"mimeMessage\"
        factory-bean=\"javaMailSender\"
        factory-method=\"createMimeMessage\"/>
    
    <bean id=\"toMimeMessageHelper\" class=\"mderf.MockMimeMessageHelper\"> 
        <constructor-arg type=\"javax.mail.internet.MimeMessage\">
            <ref bean=\"mimeMessage\"/>
        </constructor-arg>
        <property name=\"replyTo\" ref=\"replyTo\"/>

\\<!--           \\<property name=\"replyTo\" value=\"no-reply@localhost\"/> -->

        <property name=\"subject\" value=\"Attention:  Change Request\"/>
    </bean>
    
    <!-- Reply address for automated messages -->
    <bean id=\"replyTo\" class=\"javax.mail.internet.InternetAddress\">
        <property name=\"address\" value=\"no-reply@localhost\"/>
        <property name=\"personal\" value=\"Do Not Reply (Automated Message)\"/>
    </bean>

\\</beans>

---

context-two.xml

\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>
\\<!DOCTYPE beans PUBLIC \"-//SPRING/DTD BEAN/EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">
\\<beans>
\\<!--!-- **************************************** -->

    <!-- Beans associated with email notification -->
    <!-- **************************************** -->
    
    <!-- Address of email server -->
    <bean id=\"javaMailSender\" class=\"org.springframework.mail.javamail.JavaMailSenderImpl\">
        <property name=\"host\"><value>localhost</value></property>
    </bean>
    
    <bean id=\"mimeMessage\"
        factory-bean=\"javaMailSender\"
        factory-method=\"createMimeMessage\"/>
    
    <bean id=\"toMimeMessageHelper\" class=\"mderf.MockMimeMessageHelper\"> 
        <constructor-arg type=\"javax.mail.internet.MimeMessage\">
            <ref bean=\"mimeMessage\"/>
        </constructor-arg>

\\<!--        \\<property name=\"replyTo\" ref=\"replyTo\"/> -->

           <property name=\"replyTo\" value=\"no-reply@localhost\"/>
        <property name=\"subject\" value=\"Attention:  Change Request\"/>
    </bean>
    
    <!-- Reply address for automated messages -->
    <bean id=\"replyTo\" class=\"javax.mail.internet.InternetAddress\">
        <property name=\"address\" value=\"no-reply@localhost\"/>
        <property name=\"personal\" value=\"Do Not Reply (Automated Message)\"/>
    </bean>

\\</beans>

---

log4j.properties

    1. direct log messages to stdout ###

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n

    1. direct messages to file hibernate.log ###

#log4j.appender.file=org.apache.log4j.FileAppender
#log4j.appender.file.File=hibernate.log
#log4j.appender.file.layout=org.apache.log4j.PatternLayout
#log4j.appender.file.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n

    1. set log levels - for more verbose logging change 'info' to 'debug' ###

log4j.rootLogger=warn, stdout

---

mderf/MockMimeMessageHelper.java
/*
* MockMimeMessageHelper.java
* 
* Created: Dec 11, 2007
* Version: 1.0
  */
  package mderf;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

/**
* This class

* 

* `@author` Fred Muhlenberg, High Performance Technologies, Inc.
  \\*/
  public class MockMimeMessageHelper extends MimeMessageHelper
  {
  /**

  * `@param` arg0
    */
    public MockMimeMessageHelper( MimeMessage arg0 )
    {
    super( arg0 );
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setFrom(javax.mail.internet.InternetAddress)
    */
    `@Override`
    public void setFrom( InternetAddress arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setFrom(java.lang.String)
    */
    `@Override`
    public void setFrom( String arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setReplyTo(javax.mail.internet.InternetAddress)
    */
    `@Override`
    public void setReplyTo( InternetAddress arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setReplyTo(java.lang.String)
    */
    `@Override`
    public void setReplyTo( String arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setSubject(java.lang.String)
    */
    `@Override`
    public void setSubject( String arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@param` arg1
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String, boolean)
    */
    `@Override`
    public void setText( String arg0, boolean arg1 ) throws MessagingException
    {
    setText( arg0 );
    }

  /**

  * `@param` arg0
  * `@param` arg1
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String, java.lang.String)
    */
    `@Override`
    public void setText( String arg0, String arg1 ) throws MessagingException
    {
    setText( arg0 );
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String)
    */
    `@Override`
    public void setText( String arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(javax.mail.internet.InternetAddress)
    */
    `@Override`
    public void setTo( InternetAddress arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(javax.mail.internet.InternetAddress[])
    */
    `@Override`
    public void setTo( InternetAddress[] arg0 ) throws MessagingException
    {
    for( InternetAddress ia : arg0 )
    {
    setTo( ia );
    }
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(java.lang.String)
    */
    `@Override`
    public void setTo( String arg0 ) throws MessagingException
    {
    }

  /**

  * `@param` arg0
  * `@throws` MessagingException
  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(java.lang.String[])
    */
    `@Override`
    public void setTo( String[] arg0 ) throws MessagingException
    {
    for( String str : arg0 )
    {
    setTo( str );
    }
    }

}

---

mderf/OneTest.java
package mderf;

import org.junit.Test;
import org.springframework.test.AbstractSingleSpringContextTests;

public class OneTest extends AbstractSingleSpringContextTests
{

    protected String[] getConfigLocations()
    {
        return new String[]{ \"context-one.xml\" };
    }
            
    @Test
    public void testDummy()
    {
        System.out.println( \"Dummy test\" );
    }

}

---

mderf/TwoTest.java
package mderf;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TwoTest
{

    protected String[] getConfigLocations()
    {
        return new String[]{ \"context-two.xml\" };
    }
            
    @Test
    public void testDummy()
    {
        new ClassPathXmlApplicationContext( getConfigLocations() );
        System.out.println( \"Dummy output\" );
    }

}

---

---

**Affects:** 2.0.2, 2.5 final, 2.5.4

**Attachments:**
- [mailproto.jar](https://jira.spring.io/secure/attachment/14194/mailproto.jar) (_3.42 MB_)

**Issue Links:**
- #17933 Avoid ambiguous property warning for setter methods with multiple parameters
- #11065 GenericTypeAwarePropertyDescriptor warns when creating java.security.SecureRandom bean

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4a25e2dde0adf856114d8112330f167eac769e89
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9606","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9606/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9606/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/9606/events","html_url":"https://github.com/spring-projects/spring-framework/issues/9606","id":398089230,"node_id":"MDU6SXNzdWUzOTgwODkyMzA=","number":9606,"title":"Spring inconsistently resolves an overloaded setter method [SPR-4931]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/65","html_url":"https://github.com/spring-projects/spring-framework/milestone/65","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/65/labels","id":3960838,"node_id":"MDk6TWlsZXN0b25lMzk2MDgzOA==","number":65,"title":"3.0 RC2","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":118,"state":"closed","created_at":"2019-01-10T22:03:10Z","updated_at":"2019-01-11T02:38:19Z","due_on":"2009-11-11T08:00:00Z","closed_at":"2019-01-10T22:03:10Z"},"comments":6,"created_at":"2008-06-18T06:18:28Z","updated_at":"2019-01-13T08:03:39Z","closed_at":"2015-08-21T04:44:04Z","author_association":"COLLABORATOR","body":"**[Fred Muhlenberg](https://jira.spring.io/secure/ViewProfile.jspa?name=fmuhlenberg)** opened **[SPR-4931](https://jira.spring.io/browse/SPR-4931?redirect=false)** and commented\n\nReference: http://forum.springframework.org/showthread.php?t=47051\n\nI'm running into an issue with an overloaded set method.  \nI get different method resolutions depending on the environment in which I run.\n\nI am using the  MimeMessageHelper class and am setting the replyTo property and have sample code demonstrating my issue when configuring.\n\nIf I run in eclipse, the property requires the String version of the setter.\nIf I debug in eclipse, the property requires the InternetAddress version of the setter.\n\nIf I run from the command line, the property requires the InternetAddress version of the setter.\nIf I run on our (ancient) 1.4 Oracle App server  I need the String version of the setter.\n\nSame source files, same config files, different results.\n\nI assert that if I pass in a bean reference, Spring ought to resolve to an overloaded method to the type of the reference.\n\nMy sample test:\n\n---\n\nDirectory Hierarchy\n\n---\n\n./.classpath\n./.project\n./build.xml\n./lib/activation.jar\n./lib/commons-logging-1.0.4.jar\n./lib/j2ee.jar\n./lib/junit-3.8.1.jar\n./lib/junit-4.4.jar\n./lib/log4j-1.2.14.jar\n./lib/mail.jar\n./lib/spring-mock.jar\n./lib/spring.jar\n./test/java/context-one.xml\n./test/java/context-two.xml\n./test/java/log4j.properties\n./test/java/mderf/MockMimeMessageHelper.java\n./test/java/mderf/OneTest.java\n./test/java/mderf/TwoTest.java\n\n---\n\nbuild.xml\n\\<project name=\"mailproto\" default=\"all\" basedir=\".\" >\n\n    <property name=\"lib.dir\"         location=\"lib\" />\n    \n    <property name=\"build.dir\"           location=\"build\" />\n    <property name=\"build.classes.dir\"   location=\"${build.dir}\" />\n    <property name=\"build.report.dir\"    location=\"${build.dir}/report\" />\n    \n    <property name=\"test.dir\"         location=\"test\" />\n    <property name=\"test.java.dir\"    location=\"${test.dir}/java\" />\n    \n    <path id=\"compile.classpath\">\n        <fileset dir=\"${lib.dir}\">\n            <include name=\"**/*.jar\"/>\n\n\\<!--            \\<exclude name=\"**/junit-4.4.jar\"/> -->\n\n            <exclude name=\"**/junit-3.8.1.jar\"/>\n        </fileset>\n    </path>    \n    \n    <path id=\"test.classpath\">\n        <path refid=\"compile.classpath\" />\n        <path location=\"${build.classes.dir}\" />\n    </path>\n    \n    \n    <target name=\"init\">\n        <mkdir dir=\"${build.classes.dir}\"/>\n        <mkdir dir=\"${build.report.dir}\"/>\n    </target>\n    \n    \n    <target name=\"all\" depends=\"run\">\n    \n    </target>\n        \n    <target name=\"compile\" depends=\"init\">\n        \n        <javac\n            destdir=\"${build.classes.dir}\"\n            srcdir=\"${test.java.dir}\"\n        >\n            <classpath refid=\"compile.classpath\" />\n        </javac>\n        \n        <copy todir=\"${build.classes.dir}\">\n            <fileset dir=\"${test.java.dir}\">\n                <include name=\"*.xml\"/>\n            </fileset>\n        </copy>\n    \n    </target>\n    \n    <target name=\"run\" depends=\"compile\">\n        \n        <junit\n            printsummary=\"true\"\n            fork=\"yes\" \n            haltonerror=\"false\"\n            showoutput=\"true\"\n        >\n            <classpath refid=\"test.classpath\"/>\n            <formatter type=\"xml\"/>\n            <batchtest todir=\"${build.report.dir}\">\n                <fileset dir=\"${build.dir}\">\n                    <include name=\"**/*Test.class\"/>\n                </fileset>\n            </batchtest>\n    \n        </junit>\n        \n        <junitreport todir=\"${build.report.dir}\">\n            <fileset dir=\"${build.report.dir}\">\n                <include name=\"TEST-*.xml\"/>\n            </fileset>\n            <report format=\"frames\" todir=\"${build.report.dir}\"/>\n        </junitreport>\n        \n        <echo>\n\nThe JUnit report can be found in ${build.report.dir}/index.html\n\n        </echo>\n        \n    </target>\n    \n    <target name=\"clean\">\n    \n        <delete dir=\"${build.dir}\"/>\n        \n    </target>\n\n\\</project>\n\n---\n\ncontext-one.xml\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\\<!DOCTYPE beans PUBLIC \"-//SPRING/DTD BEAN/EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">\n\\<beans>\n\\<!--!-- **************************************** -->\n\n    <!-- Beans associated with email notification -->\n    <!-- **************************************** -->\n    \n    <!-- Address of email server -->\n    <bean id=\"javaMailSender\" class=\"org.springframework.mail.javamail.JavaMailSenderImpl\">\n        <property name=\"host\"><value>localhost</value></property>\n    </bean>\n    \n    <bean id=\"mimeMessage\"\n        factory-bean=\"javaMailSender\"\n        factory-method=\"createMimeMessage\"/>\n    \n    <bean id=\"toMimeMessageHelper\" class=\"mderf.MockMimeMessageHelper\"> \n        <constructor-arg type=\"javax.mail.internet.MimeMessage\">\n            <ref bean=\"mimeMessage\"/>\n        </constructor-arg>\n        <property name=\"replyTo\" ref=\"replyTo\"/>\n\n\\<!--           \\<property name=\"replyTo\" value=\"no-reply@localhost\"/> -->\n\n        <property name=\"subject\" value=\"Attention:  Change Request\"/>\n    </bean>\n    \n    <!-- Reply address for automated messages -->\n    <bean id=\"replyTo\" class=\"javax.mail.internet.InternetAddress\">\n        <property name=\"address\" value=\"no-reply@localhost\"/>\n        <property name=\"personal\" value=\"Do Not Reply (Automated Message)\"/>\n    </bean>\n\n\\</beans>\n\n---\n\ncontext-two.xml\n\n\\<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\\<!DOCTYPE beans PUBLIC \"-//SPRING/DTD BEAN/EN\" \"http://www.springframework.org/dtd/spring-beans.dtd\">\n\\<beans>\n\\<!--!-- **************************************** -->\n\n    <!-- Beans associated with email notification -->\n    <!-- **************************************** -->\n    \n    <!-- Address of email server -->\n    <bean id=\"javaMailSender\" class=\"org.springframework.mail.javamail.JavaMailSenderImpl\">\n        <property name=\"host\"><value>localhost</value></property>\n    </bean>\n    \n    <bean id=\"mimeMessage\"\n        factory-bean=\"javaMailSender\"\n        factory-method=\"createMimeMessage\"/>\n    \n    <bean id=\"toMimeMessageHelper\" class=\"mderf.MockMimeMessageHelper\"> \n        <constructor-arg type=\"javax.mail.internet.MimeMessage\">\n            <ref bean=\"mimeMessage\"/>\n        </constructor-arg>\n\n\\<!--        \\<property name=\"replyTo\" ref=\"replyTo\"/> -->\n\n           <property name=\"replyTo\" value=\"no-reply@localhost\"/>\n        <property name=\"subject\" value=\"Attention:  Change Request\"/>\n    </bean>\n    \n    <!-- Reply address for automated messages -->\n    <bean id=\"replyTo\" class=\"javax.mail.internet.InternetAddress\">\n        <property name=\"address\" value=\"no-reply@localhost\"/>\n        <property name=\"personal\" value=\"Do Not Reply (Automated Message)\"/>\n    </bean>\n\n\\</beans>\n\n---\n\nlog4j.properties\n\n    1. direct log messages to stdout ###\n\nlog4j.appender.stdout=org.apache.log4j.ConsoleAppender\nlog4j.appender.stdout.Target=System.out\nlog4j.appender.stdout.layout=org.apache.log4j.PatternLayout\nlog4j.appender.stdout.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n\n\n    1. direct messages to file hibernate.log ###\n\n#log4j.appender.file=org.apache.log4j.FileAppender\n#log4j.appender.file.File=hibernate.log\n#log4j.appender.file.layout=org.apache.log4j.PatternLayout\n#log4j.appender.file.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n\n\n    1. set log levels - for more verbose logging change 'info' to 'debug' ###\n\nlog4j.rootLogger=warn, stdout\n\n---\n\nmderf/MockMimeMessageHelper.java\n/*\n* MockMimeMessageHelper.java\n* \n* Created: Dec 11, 2007\n* Version: 1.0\n  */\n  package mderf;\n\nimport javax.mail.MessagingException;\nimport javax.mail.internet.InternetAddress;\nimport javax.mail.internet.MimeMessage;\n\nimport org.springframework.mail.javamail.MimeMessageHelper;\n\n/**\n* This class\n\n* \n\n* `@author` Fred Muhlenberg, High Performance Technologies, Inc.\n  \\*/\n  public class MockMimeMessageHelper extends MimeMessageHelper\n  {\n  /**\n\n  * `@param` arg0\n    */\n    public MockMimeMessageHelper( MimeMessage arg0 )\n    {\n    super( arg0 );\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setFrom(javax.mail.internet.InternetAddress)\n    */\n    `@Override`\n    public void setFrom( InternetAddress arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setFrom(java.lang.String)\n    */\n    `@Override`\n    public void setFrom( String arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setReplyTo(javax.mail.internet.InternetAddress)\n    */\n    `@Override`\n    public void setReplyTo( InternetAddress arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setReplyTo(java.lang.String)\n    */\n    `@Override`\n    public void setReplyTo( String arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setSubject(java.lang.String)\n    */\n    `@Override`\n    public void setSubject( String arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@param` arg1\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String, boolean)\n    */\n    `@Override`\n    public void setText( String arg0, boolean arg1 ) throws MessagingException\n    {\n    setText( arg0 );\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@param` arg1\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String, java.lang.String)\n    */\n    `@Override`\n    public void setText( String arg0, String arg1 ) throws MessagingException\n    {\n    setText( arg0 );\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setText(java.lang.String)\n    */\n    `@Override`\n    public void setText( String arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(javax.mail.internet.InternetAddress)\n    */\n    `@Override`\n    public void setTo( InternetAddress arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(javax.mail.internet.InternetAddress[])\n    */\n    `@Override`\n    public void setTo( InternetAddress[] arg0 ) throws MessagingException\n    {\n    for( InternetAddress ia : arg0 )\n    {\n    setTo( ia );\n    }\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(java.lang.String)\n    */\n    `@Override`\n    public void setTo( String arg0 ) throws MessagingException\n    {\n    }\n\n  /**\n\n  * `@param` arg0\n  * `@throws` MessagingException\n  * `@see` org.springframework.mail.javamail.MimeMessageHelper#setTo(java.lang.String[])\n    */\n    `@Override`\n    public void setTo( String[] arg0 ) throws MessagingException\n    {\n    for( String str : arg0 )\n    {\n    setTo( str );\n    }\n    }\n\n}\n\n---\n\nmderf/OneTest.java\npackage mderf;\n\nimport org.junit.Test;\nimport org.springframework.test.AbstractSingleSpringContextTests;\n\npublic class OneTest extends AbstractSingleSpringContextTests\n{\n\n    protected String[] getConfigLocations()\n    {\n        return new String[]{ \"context-one.xml\" };\n    }\n            \n    @Test\n    public void testDummy()\n    {\n        System.out.println( \"Dummy test\" );\n    }\n\n}\n\n---\n\nmderf/TwoTest.java\npackage mderf;\n\nimport org.junit.Test;\nimport org.springframework.context.support.ClassPathXmlApplicationContext;\n\npublic class TwoTest\n{\n\n    protected String[] getConfigLocations()\n    {\n        return new String[]{ \"context-two.xml\" };\n    }\n            \n    @Test\n    public void testDummy()\n    {\n        new ClassPathXmlApplicationContext( getConfigLocations() );\n        System.out.println( \"Dummy output\" );\n    }\n\n}\n\n---\n\n---\n\n**Affects:** 2.0.2, 2.5 final, 2.5.4\n\n**Attachments:**\n- [mailproto.jar](https://jira.spring.io/secure/attachment/14194/mailproto.jar) (_3.42 MB_)\n\n**Issue Links:**\n- #17933 Avoid ambiguous property warning for setter methods with multiple parameters\n- #11065 GenericTypeAwarePropertyDescriptor warns when creating java.security.SecureRandom bean\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4a25e2dde0adf856114d8112330f167eac769e89\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453331694","453331696","453331698","453331700","453331701","453331702"], "labels":["in: core","type: enhancement"]}