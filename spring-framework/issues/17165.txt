{"id":"17165", "title":"JDBC Connetion will not be managed by Spring(like mybatis or hibernate) [SPR-12564]", "body":"**[jerryscott2014](https://jira.spring.io/secure/ViewProfile.jspa?name=jerry2014)** opened **[SPR-12564](https://jira.spring.io/browse/SPR-12564?redirect=false)** and commented

Hi, spring team.
1. when integration with mybatis (with mybatis-spring), it seems that the transaction does not make any sense which I could leave any use message.

2. From the reference URL,it might be the spring transaction should do some improvement.
   2.1  My configuration:
   2.1  I have some appContext (like parent and sub context), the parent context(suppose it to be \"appContext_channel.xml\")is like this (others omitted

```xml
<import resource=\"appContext_dao.xml\" />

<!--enable annotation support --><context:annotation-config />

<!--enable mbean export -->
<context:mbean-export />
```

    2.2  the appContext_dao.xml  like this:

```xml
   <bean id=\"dataSource\" class=\"com.alibaba.druid.pool.DruidDataSource\"
		init-method=\"init\" destroy-method=\"close\">
   </bean>

  <bean id=\"sqlSessionFactory\" class=\"org.mybatis.spring.SqlSessionFactoryBean\">
		<property name=\"dataSource\" ref=\"dataSource\" />
		<property name=\"plugins\">
			<list>
				<ref bean=\"pageQueryInterceptor\" />
				<ref bean=\"optimistLockInterceptor\" />
			</list>
		</property>
		<!-- 引入mapper资源文件 -->
		<property name=\"mapperLocations\" value=\"${DEFAULT_MAPPER_LOCATION}\"></property>
	</bean>

	<!--构造sqlSessionTemplate(支持批量) -->
	<bean id=\"sqlSessionTemplate\" class=\"org.mybatis.spring.SqlSessionTemplate\">
		<constructor-arg index=\"0\" ref=\"sqlSessionFactory\" />
		<constructor-arg index=\"1\" value=\"BATCH\" />
	</bean>

	<!-- 引入支持jdbc的事务管理器 -->
	<bean id=\"transactionManager\"
		class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">
		<property name=\"dataSource\" ref=\"dataSource\" />
	</bean>

	<!-- 引入jta的关联支持(userTransactionManager以及userTransaction) -->
	<bean id=\"userTransManager\" class=\"com.zjht.channel.server.tx.xa.JtaTransManagerHelper\"
		factory-method=\"createUserTransactionManager\" init-method=\"init\"
		destroy-method=\"close\">
		<constructor-arg value=\"${DEFAULT_JTA_STARUP_SERVICE}\" />
		<constructor-arg value=\"${DEFAULT_JTA_FORCE_SHUTDOWN}\" />
	</bean>

	<bean id=\"userTransaction\" class=\"com.zjht.channel.server.tx.xa.JtaTransManagerHelper\"
		factory-method=\"createUserTransaction\">
		<constructor-arg value=\"${DEFAULT_JTA_TRANS_TIMEOUT}\" />
	</bean>

	<!-- 缺省jta事务管理器(由应用引用决定) -->
	<bean id=\"jtaTransactionManager\"
		class=\"org.springframework.transaction.jta.JtaTransactionManager\">
		<constructor-arg ref=\"userTransaction\" />
		<constructor-arg ref=\"userTransManager\" />
	</bean>

	<!-- 引入annotation方式的事务注解 (针对jdbc|jta方式) -->
	<tx:annotation-driven transaction-manager=\"transactionManager\"
		mode=\"aspectj\" />

	<tx:annotation-driven transaction-manager=\"jtaTransactionManager\"
		mode=\"aspectj\" />

	<!-- 引入aspectj行为 -->
	<aop:aspectj-autoproxy proxy-target-class=\"true\" />
```

2.3. then  I use com class as the `@Configuration` defined to start the appContext_channel like this:

```java
@ImportResource({ \"classpath:appContext_channel.xml\" })
@EnableReactor
@MapperScan(basePackages = { \"com.zjht.channel.platform.mapper\" })
@ComponentScan(basePackages = { \"com.zjht.channel.platform.service\" })
@Configuration
@Import({ ChannelEnvConfiguration.class })
public class ChannelPlatformConfiguration {
   //omitted
}
```

3. from 2.3. the configuration or main class is located in package \"com.zjht.channel.platform.service\"\". and it would call the component-scan
   annotation to complete the reference under such package. but to my surprise, the transaction does not bind the spring transaction itself. while tracing the code to `DataSourceUtils.isConnectionTransactional()`  such
   method called and returned false. so the mybatis spring transaction bind process is fail like this:

```
2014-12-22 11:37:59.810 DEBUG [main] o.m.s.SqlSessionUtils [SqlSessionUtils.java:104] Creating a new SqlSession
2014-12-22 11:37:59.857 DEBUG [main] o.m.s.SqlSessionUtils [SqlSessionUtils.java:140] SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@1e3e097] was not registered for synchronization because synchronization is not active
2014-12-22 11:38:00.185 DEBUG [main] c.z.c.p.m.ChannelAppServiceMapper [LoggingCache.java:62] Cache Hit Ratio [com.zjht.channel.platform.mapper.ChannelAppServiceMapper]: 0.0
2014-12-22 11:38:00.248 DEBUG [main] o.m.s.t.SpringManagedTransaction [SpringManagedTransaction.java:86] JDBC Connection [com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl@1aa85ab] will not be managed by Spring
```

4. conclusion:
   4.1 I use AnnotationConfigApplicationContext with the configuration class(listed above) to start the appContext (the subContext defined the transaction support)--- with <tx:annotation-driven /> enabled

   4.2  the class with `@Configuration` also use the `@ComponentScan` to scan the annotation under the package to complete the bean initialization

   4.3  I use the `@Transactional(\"transactionManager\") `  marked in the service for mapper(the mybatis mapper interface) calling like this:
   {```java
   `@Service`
   `@Transactional`(\"transactionManager\")
   public class ChannelAppServiceCfgCreateImpl extends DefaultChannelAppServiceCfgCreateImpl {
   `@Autowired`
   private ChannelAppConfigMapper channelAppConfigMapper;

   `@Autowired`
   private ChannelAppServiceMapper channelAppServiceMapper;

   `@Autowired`
   private ChannelTerminalServiceMapper channelTerminalServiceMapper;

       //omitted

}

```

after the calling completed, the debug log showed that the transaction of spring was not active. 
   
5. suggestion:
    5.1 the way to start appContext listed above is very common with parent appContext (with resources import )and sub-appContext (like mvc or dao configuration) 

    5.2  could spring provide further support for transaction (with annotation enhancement)  for those bean with `@Service` or `@Controller` under componet-scan enabled.  we found it was hard to locate where the problem just like why the transaction does not effect for the configuration listed like  `<tx:annotation-driven />` has been marked. but in fact I could not 
make the service around transaction.

so would spring team add such for enhancement ,once those marked with @Transactional  as the sub-appContext defined and with component-scan, spring should make sure of the calling around the service should be under transaction.
```



---

**Reference URL:** http://jinnianshilongnian.iteye.com/blog/1850432

**Attachments:**
- [channelserver.log](https://jira.spring.io/secure/attachment/22440/channelserver.log) (_1.56 kB_)
- [TestMybatis.7z](https://jira.spring.io/secure/attachment/22446/TestMybatis.7z) (_4.65 MB_)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17165","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17165/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17165/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/17165/events","html_url":"https://github.com/spring-projects/spring-framework/issues/17165","id":398175028,"node_id":"MDU6SXNzdWUzOTgxNzUwMjg=","number":17165,"title":"JDBC Connetion will not be managed by Spring(like mybatis or hibernate) [SPR-12564]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2014-12-21T22:12:40Z","updated_at":"2019-01-12T03:30:43Z","closed_at":"2019-01-12T03:30:43Z","author_association":"COLLABORATOR","body":"**[jerryscott2014](https://jira.spring.io/secure/ViewProfile.jspa?name=jerry2014)** opened **[SPR-12564](https://jira.spring.io/browse/SPR-12564?redirect=false)** and commented\n\nHi, spring team.\n1. when integration with mybatis (with mybatis-spring), it seems that the transaction does not make any sense which I could leave any use message.\n\n2. From the reference URL,it might be the spring transaction should do some improvement.\n   2.1  My configuration:\n   2.1  I have some appContext (like parent and sub context), the parent context(suppose it to be \"appContext_channel.xml\")is like this (others omitted\n\n```xml\n<import resource=\"appContext_dao.xml\" />\n\n<!--enable annotation support --><context:annotation-config />\n\n<!--enable mbean export -->\n<context:mbean-export />\n```\n\n    2.2  the appContext_dao.xml  like this:\n\n```xml\n   <bean id=\"dataSource\" class=\"com.alibaba.druid.pool.DruidDataSource\"\n\t\tinit-method=\"init\" destroy-method=\"close\">\n   </bean>\n\n  <bean id=\"sqlSessionFactory\" class=\"org.mybatis.spring.SqlSessionFactoryBean\">\n\t\t<property name=\"dataSource\" ref=\"dataSource\" />\n\t\t<property name=\"plugins\">\n\t\t\t<list>\n\t\t\t\t<ref bean=\"pageQueryInterceptor\" />\n\t\t\t\t<ref bean=\"optimistLockInterceptor\" />\n\t\t\t</list>\n\t\t</property>\n\t\t<!-- 引入mapper资源文件 -->\n\t\t<property name=\"mapperLocations\" value=\"${DEFAULT_MAPPER_LOCATION}\"></property>\n\t</bean>\n\n\t<!--构造sqlSessionTemplate(支持批量) -->\n\t<bean id=\"sqlSessionTemplate\" class=\"org.mybatis.spring.SqlSessionTemplate\">\n\t\t<constructor-arg index=\"0\" ref=\"sqlSessionFactory\" />\n\t\t<constructor-arg index=\"1\" value=\"BATCH\" />\n\t</bean>\n\n\t<!-- 引入支持jdbc的事务管理器 -->\n\t<bean id=\"transactionManager\"\n\t\tclass=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">\n\t\t<property name=\"dataSource\" ref=\"dataSource\" />\n\t</bean>\n\n\t<!-- 引入jta的关联支持(userTransactionManager以及userTransaction) -->\n\t<bean id=\"userTransManager\" class=\"com.zjht.channel.server.tx.xa.JtaTransManagerHelper\"\n\t\tfactory-method=\"createUserTransactionManager\" init-method=\"init\"\n\t\tdestroy-method=\"close\">\n\t\t<constructor-arg value=\"${DEFAULT_JTA_STARUP_SERVICE}\" />\n\t\t<constructor-arg value=\"${DEFAULT_JTA_FORCE_SHUTDOWN}\" />\n\t</bean>\n\n\t<bean id=\"userTransaction\" class=\"com.zjht.channel.server.tx.xa.JtaTransManagerHelper\"\n\t\tfactory-method=\"createUserTransaction\">\n\t\t<constructor-arg value=\"${DEFAULT_JTA_TRANS_TIMEOUT}\" />\n\t</bean>\n\n\t<!-- 缺省jta事务管理器(由应用引用决定) -->\n\t<bean id=\"jtaTransactionManager\"\n\t\tclass=\"org.springframework.transaction.jta.JtaTransactionManager\">\n\t\t<constructor-arg ref=\"userTransaction\" />\n\t\t<constructor-arg ref=\"userTransManager\" />\n\t</bean>\n\n\t<!-- 引入annotation方式的事务注解 (针对jdbc|jta方式) -->\n\t<tx:annotation-driven transaction-manager=\"transactionManager\"\n\t\tmode=\"aspectj\" />\n\n\t<tx:annotation-driven transaction-manager=\"jtaTransactionManager\"\n\t\tmode=\"aspectj\" />\n\n\t<!-- 引入aspectj行为 -->\n\t<aop:aspectj-autoproxy proxy-target-class=\"true\" />\n```\n\n2.3. then  I use com class as the `@Configuration` defined to start the appContext_channel like this:\n\n```java\n@ImportResource({ \"classpath:appContext_channel.xml\" })\n@EnableReactor\n@MapperScan(basePackages = { \"com.zjht.channel.platform.mapper\" })\n@ComponentScan(basePackages = { \"com.zjht.channel.platform.service\" })\n@Configuration\n@Import({ ChannelEnvConfiguration.class })\npublic class ChannelPlatformConfiguration {\n   //omitted\n}\n```\n\n3. from 2.3. the configuration or main class is located in package \"com.zjht.channel.platform.service\"\". and it would call the component-scan\n   annotation to complete the reference under such package. but to my surprise, the transaction does not bind the spring transaction itself. while tracing the code to `DataSourceUtils.isConnectionTransactional()`  such\n   method called and returned false. so the mybatis spring transaction bind process is fail like this:\n\n```\n2014-12-22 11:37:59.810 DEBUG [main] o.m.s.SqlSessionUtils [SqlSessionUtils.java:104] Creating a new SqlSession\n2014-12-22 11:37:59.857 DEBUG [main] o.m.s.SqlSessionUtils [SqlSessionUtils.java:140] SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@1e3e097] was not registered for synchronization because synchronization is not active\n2014-12-22 11:38:00.185 DEBUG [main] c.z.c.p.m.ChannelAppServiceMapper [LoggingCache.java:62] Cache Hit Ratio [com.zjht.channel.platform.mapper.ChannelAppServiceMapper]: 0.0\n2014-12-22 11:38:00.248 DEBUG [main] o.m.s.t.SpringManagedTransaction [SpringManagedTransaction.java:86] JDBC Connection [com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl@1aa85ab] will not be managed by Spring\n```\n\n4. conclusion:\n   4.1 I use AnnotationConfigApplicationContext with the configuration class(listed above) to start the appContext (the subContext defined the transaction support)--- with <tx:annotation-driven /> enabled\n\n   4.2  the class with `@Configuration` also use the `@ComponentScan` to scan the annotation under the package to complete the bean initialization\n\n   4.3  I use the `@Transactional(\"transactionManager\") `  marked in the service for mapper(the mybatis mapper interface) calling like this:\n   {```java\n   `@Service`\n   `@Transactional`(\"transactionManager\")\n   public class ChannelAppServiceCfgCreateImpl extends DefaultChannelAppServiceCfgCreateImpl {\n   `@Autowired`\n   private ChannelAppConfigMapper channelAppConfigMapper;\n\n   `@Autowired`\n   private ChannelAppServiceMapper channelAppServiceMapper;\n\n   `@Autowired`\n   private ChannelTerminalServiceMapper channelTerminalServiceMapper;\n\n       //omitted\n\n}\n\n```\n\nafter the calling completed, the debug log showed that the transaction of spring was not active. \n   \n5. suggestion:\n    5.1 the way to start appContext listed above is very common with parent appContext (with resources import )and sub-appContext (like mvc or dao configuration) \n\n    5.2  could spring provide further support for transaction (with annotation enhancement)  for those bean with `@Service` or `@Controller` under componet-scan enabled.  we found it was hard to locate where the problem just like why the transaction does not effect for the configuration listed like  `<tx:annotation-driven />` has been marked. but in fact I could not \nmake the service around transaction.\n\nso would spring team add such for enhancement ,once those marked with @Transactional  as the sub-appContext defined and with component-scan, spring should make sure of the calling around the service should be under transaction.\n```\n\n\n\n---\n\n**Reference URL:** http://jinnianshilongnian.iteye.com/blog/1850432\n\n**Attachments:**\n- [channelserver.log](https://jira.spring.io/secure/attachment/22440/channelserver.log) (_1.56 kB_)\n- [TestMybatis.7z](https://jira.spring.io/secure/attachment/22446/TestMybatis.7z) (_4.65 MB_)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453420753","453420754","453420756","453420758","453420760","453420761","453420762","453420763","453420766","453420768","453420769","453420770","453715736"], "labels":["in: core","in: data","status: bulk-closed"]}