{"id":"11744", "title":"IllegalArgumentException thrown for simple java.lang.String prototype bean  [SPR-7084]", "body":"**[diwakar](https://jira.spring.io/secure/ViewProfile.jspa?name=diwakar)** opened **[SPR-7084](https://jira.spring.io/browse/SPR-7084?redirect=false)** and commented

Code

---

import org.springframework.context.support.GenericApplicationContext;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;

/**
*/
public class IllegalArgumentProblem {

    public static void main(String[] args) {
         GenericApplicationContext parent = new GenericApplicationContext();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(parent);
        reader.loadBeanDefinitions(new ClassPathResource(\"app.xml\", IllegalArgumentProblem.class));
        parent.refresh();
    
        String bean = (String) parent.getBean(\"x\");
        String bean2 = (String) parent.getBean(\"x\"); // second access throws the exception.
    }

}

Configuration

---

\\<beans xmlns=\"http://www.springframework.org/schema/beans\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xsi:schemaLocation=\"http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans-2.5.xsd\">

    <bean id=\"x\" class=\"java.lang.String\" scope=\"prototype\">
        <constructor-arg><value>PARENT value</value></constructor-arg>
    </bean>

\\</beans>

Error

---

log4j:WARN No appenders could be found for logger (org.springframework.core.CollectionFactory).
log4j:WARN Please initialize the log4j system properly.
Exception in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'x' defined in class path resource [app.xml]: Instantiation of bean failed; nested exception is org.springframework.beans.BeanInstantiationException: Could not instantiate bean class [java.lang.String]: Illegal arguments for constructor; nested exception is java.lang.IllegalArgumentException: argument type mismatch
at org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:254)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.autowireConstructor(AbstractAutowireCapableBeanFactory.java:925)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBeanInstance(AbstractAutowireCapableBeanFactory.java:823)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:440)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory$1.run(AbstractAutowireCapableBeanFactory.java:409)
at java.security.AccessController.doPrivileged(Native Method)
at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:380)
at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:283)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:185)
at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:164)
at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:881)
at IllegalArgumentProblem.main(IllegalArgumentProblem.java:21)
at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
at java.lang.reflect.Method.invoke(Method.java:597)
at com.intellij.rt.execution.application.AppMain.main(AppMain.java:90)
Caused by: org.springframework.beans.BeanInstantiationException: Could not instantiate bean class [java.lang.String]: Illegal arguments for constructor; nested exception is java.lang.IllegalArgumentException: argument type mismatch
at org.springframework.beans.BeanUtils.instantiateClass(BeanUtils.java:111)
at org.springframework.beans.factory.support.SimpleInstantiationStrategy.instantiate(SimpleInstantiationStrategy.java:87)
at org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:248)
... 16 more
Caused by: java.lang.IllegalArgumentException: argument type mismatch
at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:39)
at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:27)
at java.lang.reflect.Constructor.newInstance(Constructor.java:513)
at org.springframework.beans.BeanUtils.instantiateClass(BeanUtils.java:100)
... 18 more

Process finished with exit code 1

---

**Affects:** 2.5.5, 2.5.6

**Issue Links:**
- #10240 not singleton StringBeans (_**\"is duplicated by\"**_)

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/12ce250c6ce911774a7983905fd6e006b5a1eac1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11744","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11744/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11744/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/11744/events","html_url":"https://github.com/spring-projects/spring-framework/issues/11744","id":398104452,"node_id":"MDU6SXNzdWUzOTgxMDQ0NTI=","number":11744,"title":"IllegalArgumentException thrown for simple java.lang.String prototype bean  [SPR-7084]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70","html_url":"https://github.com/spring-projects/spring-framework/milestone/70","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/70/labels","id":3960843,"node_id":"MDk6TWlsZXN0b25lMzk2MDg0Mw==","number":70,"title":"3.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":117,"state":"closed","created_at":"2019-01-10T22:03:16Z","updated_at":"2019-01-11T02:35:54Z","due_on":"2010-06-13T07:00:00Z","closed_at":"2019-01-10T22:03:16Z"},"comments":1,"created_at":"2010-04-09T06:17:47Z","updated_at":"2019-01-13T21:46:26Z","closed_at":"2012-06-19T03:43:45Z","author_association":"COLLABORATOR","body":"**[diwakar](https://jira.spring.io/secure/ViewProfile.jspa?name=diwakar)** opened **[SPR-7084](https://jira.spring.io/browse/SPR-7084?redirect=false)** and commented\n\nCode\n\n---\n\nimport org.springframework.context.support.GenericApplicationContext;\nimport org.springframework.beans.factory.xml.XmlBeanDefinitionReader;\nimport org.springframework.core.io.ClassPathResource;\n\n/**\n*/\npublic class IllegalArgumentProblem {\n\n    public static void main(String[] args) {\n         GenericApplicationContext parent = new GenericApplicationContext();\n        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(parent);\n        reader.loadBeanDefinitions(new ClassPathResource(\"app.xml\", IllegalArgumentProblem.class));\n        parent.refresh();\n    \n        String bean = (String) parent.getBean(\"x\");\n        String bean2 = (String) parent.getBean(\"x\"); // second access throws the exception.\n    }\n\n}\n\nConfiguration\n\n---\n\n\\<beans xmlns=\"http://www.springframework.org/schema/beans\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxsi:schemaLocation=\"http://www.springframework.org/schema/beans\nhttp://www.springframework.org/schema/beans/spring-beans-2.5.xsd\">\n\n    <bean id=\"x\" class=\"java.lang.String\" scope=\"prototype\">\n        <constructor-arg><value>PARENT value</value></constructor-arg>\n    </bean>\n\n\\</beans>\n\nError\n\n---\n\nlog4j:WARN No appenders could be found for logger (org.springframework.core.CollectionFactory).\nlog4j:WARN Please initialize the log4j system properly.\nException in thread \"main\" org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'x' defined in class path resource [app.xml]: Instantiation of bean failed; nested exception is org.springframework.beans.BeanInstantiationException: Could not instantiate bean class [java.lang.String]: Illegal arguments for constructor; nested exception is java.lang.IllegalArgumentException: argument type mismatch\nat org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:254)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.autowireConstructor(AbstractAutowireCapableBeanFactory.java:925)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBeanInstance(AbstractAutowireCapableBeanFactory.java:823)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:440)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory$1.run(AbstractAutowireCapableBeanFactory.java:409)\nat java.security.AccessController.doPrivileged(Native Method)\nat org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:380)\nat org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:283)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:185)\nat org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:164)\nat org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:881)\nat IllegalArgumentProblem.main(IllegalArgumentProblem.java:21)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\nat java.lang.reflect.Method.invoke(Method.java:597)\nat com.intellij.rt.execution.application.AppMain.main(AppMain.java:90)\nCaused by: org.springframework.beans.BeanInstantiationException: Could not instantiate bean class [java.lang.String]: Illegal arguments for constructor; nested exception is java.lang.IllegalArgumentException: argument type mismatch\nat org.springframework.beans.BeanUtils.instantiateClass(BeanUtils.java:111)\nat org.springframework.beans.factory.support.SimpleInstantiationStrategy.instantiate(SimpleInstantiationStrategy.java:87)\nat org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:248)\n... 16 more\nCaused by: java.lang.IllegalArgumentException: argument type mismatch\nat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\nat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:39)\nat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:27)\nat java.lang.reflect.Constructor.newInstance(Constructor.java:513)\nat org.springframework.beans.BeanUtils.instantiateClass(BeanUtils.java:100)\n... 18 more\n\nProcess finished with exit code 1\n\n---\n\n**Affects:** 2.5.5, 2.5.6\n\n**Issue Links:**\n- #10240 not singleton StringBeans (_**\"is duplicated by\"**_)\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/12ce250c6ce911774a7983905fd6e006b5a1eac1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453349995"], "labels":["in: core","type: bug"]}