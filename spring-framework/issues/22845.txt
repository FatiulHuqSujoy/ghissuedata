{"id":"22845", "title":"CrudRepository add support for Optional", "body":"Currently I use CrudRepository this way
```java
@Repository
public interface StuffRepository extends CrudRepository<Stuff, Long> { 
    public Stuff findStuffByOtherId(String otherId);
}
```
would be great if this were possible
```java
@Repository
public interface StuffRepository extends CrudRepository<Stuff, Long> { 
    public Optional<Stuff> findStuffByOtherId(String otherId);
}
```

This would enable more compact code like this
```java
@Autowired StuffRepository stuffRepository;

public Stuff getOrCreateStuff(String otherId) {
  return stuffRepository.findByOtherId(otherId)
      .orElse(stuffRepository.save(new Stuff(otherId)));
}
```
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22845","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22845/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22845/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/22845/events","html_url":"https://github.com/spring-projects/spring-framework/issues/22845","id":437943734,"node_id":"MDU6SXNzdWU0Mzc5NDM3MzQ=","number":22845,"title":"CrudRepository add support for Optional","user":{"login":"amalic","id":3675484,"node_id":"MDQ6VXNlcjM2NzU0ODQ=","avatar_url":"https://avatars2.githubusercontent.com/u/3675484?v=4","gravatar_id":"","url":"https://api.github.com/users/amalic","html_url":"https://github.com/amalic","followers_url":"https://api.github.com/users/amalic/followers","following_url":"https://api.github.com/users/amalic/following{/other_user}","gists_url":"https://api.github.com/users/amalic/gists{/gist_id}","starred_url":"https://api.github.com/users/amalic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/amalic/subscriptions","organizations_url":"https://api.github.com/users/amalic/orgs","repos_url":"https://api.github.com/users/amalic/repos","events_url":"https://api.github.com/users/amalic/events{/privacy}","received_events_url":"https://api.github.com/users/amalic/received_events","type":"User","site_admin":false},"labels":[{"id":1225703104,"node_id":"MDU6TGFiZWwxMjI1NzAzMTA0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/for:%20external-project","name":"for: external-project","color":"c5def5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2019-04-27T13:28:33Z","updated_at":"2019-04-27T13:36:18Z","closed_at":"2019-04-27T13:36:07Z","author_association":"NONE","body":"Currently I use CrudRepository this way\r\n```java\r\n@Repository\r\npublic interface StuffRepository extends CrudRepository<Stuff, Long> { \r\n    public Stuff findStuffByOtherId(String otherId);\r\n}\r\n```\r\nwould be great if this were possible\r\n```java\r\n@Repository\r\npublic interface StuffRepository extends CrudRepository<Stuff, Long> { \r\n    public Optional<Stuff> findStuffByOtherId(String otherId);\r\n}\r\n```\r\n\r\nThis would enable more compact code like this\r\n```java\r\n@Autowired StuffRepository stuffRepository;\r\n\r\npublic Stuff getOrCreateStuff(String otherId) {\r\n  return stuffRepository.findByOtherId(otherId)\r\n      .orElse(stuffRepository.save(new Stuff(otherId)));\r\n}\r\n```\r\n","closed_by":{"login":"snicoll","id":490484,"node_id":"MDQ6VXNlcjQ5MDQ4NA==","avatar_url":"https://avatars0.githubusercontent.com/u/490484?v=4","gravatar_id":"","url":"https://api.github.com/users/snicoll","html_url":"https://github.com/snicoll","followers_url":"https://api.github.com/users/snicoll/followers","following_url":"https://api.github.com/users/snicoll/following{/other_user}","gists_url":"https://api.github.com/users/snicoll/gists{/gist_id}","starred_url":"https://api.github.com/users/snicoll/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/snicoll/subscriptions","organizations_url":"https://api.github.com/users/snicoll/orgs","repos_url":"https://api.github.com/users/snicoll/repos","events_url":"https://api.github.com/users/snicoll/events{/privacy}","received_events_url":"https://api.github.com/users/snicoll/received_events","type":"User","site_admin":false}}", "commentIds":["487286739"], "labels":["for: external-project"]}