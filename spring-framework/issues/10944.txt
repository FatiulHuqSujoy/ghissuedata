{"id":"10944", "title":"RmiServiceExporter fails to export a UnicastRemoteObject [SPR-6277]", "body":"**[Jean Hominal](https://jira.spring.io/secure/ViewProfile.jspa?name=jhominal)** opened **[SPR-6277](https://jira.spring.io/browse/SPR-6277?redirect=false)** and commented

When setting a org.springframework.remoting.rmi.RmiServiceExporter with a bean that extends java.rmi.server.UnicastRemoteObject, the invocation fails (with an exception saying that the object is already exported via RMI).

The problem comes from the UnicastRemoteObject constructor that automatically exports the bean, by default on port 0 (and the nature of UnicastRemoteObject forbids an instance to be exported more than once).

There are two possible workarounds for that in the forum reference, but they both boil down to a simple thing: calling the UnicastRemoteObject.unexportObject(Remote, boolean) on the bean before attempting to export it with the parameters from Spring.

For a true fix, I would put the additional lines in the RmiServiceExporter.prepare() method.

I don't believe it would be useful to add a configuration parameter for that.

Here is a basic testcase that should trigger the bug on Spring 2.5.6:

The java class:

package testcase;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class BasicUnicastRemoteObject extends UnicastRemoteObject
{
protected BasicUnicastRemoteObject() throws RemoteException
{
super();
}
}

Beans configuration:

\\<bean id=\"basic-unicast\" class=\"testcase.BasicUnicastRemoteObject\"/>
\\<bean id=\"basic-rmi-exporter\" class=\"org.springframework.remoting.rmi.RmiServiceExporter\">
\\<property name=\"serviceName\" value=\"basicUnicast\"/>
\\<property name=\"service\" ref=\"basic-unicast\"/>
\\<property name=\"registryPort\" value=\"1099\"/>
\\</bean>


---

**Affects:** 2.5.6

**Reference URL:** http://forum.springsource.org/showthread.php?p=266531#post266531
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10944","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10944/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10944/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/10944/events","html_url":"https://github.com/spring-projects/spring-framework/issues/10944","id":398098715,"node_id":"MDU6SXNzdWUzOTgwOTg3MTU=","number":10944,"title":"RmiServiceExporter fails to export a UnicastRemoteObject [SPR-6277]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2009-10-26T23:36:08Z","updated_at":"2019-01-12T16:38:54Z","closed_at":"2018-12-28T10:46:09Z","author_association":"COLLABORATOR","body":"**[Jean Hominal](https://jira.spring.io/secure/ViewProfile.jspa?name=jhominal)** opened **[SPR-6277](https://jira.spring.io/browse/SPR-6277?redirect=false)** and commented\n\nWhen setting a org.springframework.remoting.rmi.RmiServiceExporter with a bean that extends java.rmi.server.UnicastRemoteObject, the invocation fails (with an exception saying that the object is already exported via RMI).\n\nThe problem comes from the UnicastRemoteObject constructor that automatically exports the bean, by default on port 0 (and the nature of UnicastRemoteObject forbids an instance to be exported more than once).\n\nThere are two possible workarounds for that in the forum reference, but they both boil down to a simple thing: calling the UnicastRemoteObject.unexportObject(Remote, boolean) on the bean before attempting to export it with the parameters from Spring.\n\nFor a true fix, I would put the additional lines in the RmiServiceExporter.prepare() method.\n\nI don't believe it would be useful to add a configuration parameter for that.\n\nHere is a basic testcase that should trigger the bug on Spring 2.5.6:\n\nThe java class:\n\npackage testcase;\n\nimport java.rmi.RemoteException;\nimport java.rmi.server.UnicastRemoteObject;\n\npublic class BasicUnicastRemoteObject extends UnicastRemoteObject\n{\nprotected BasicUnicastRemoteObject() throws RemoteException\n{\nsuper();\n}\n}\n\nBeans configuration:\n\n\\<bean id=\"basic-unicast\" class=\"testcase.BasicUnicastRemoteObject\"/>\n\\<bean id=\"basic-rmi-exporter\" class=\"org.springframework.remoting.rmi.RmiServiceExporter\">\n\\<property name=\"serviceName\" value=\"basicUnicast\"/>\n\\<property name=\"service\" ref=\"basic-unicast\"/>\n\\<property name=\"registryPort\" value=\"1099\"/>\n\\</bean>\n\n\n---\n\n**Affects:** 2.5.6\n\n**Reference URL:** http://forum.springsource.org/showthread.php?p=266531#post266531\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453343204"], "labels":["in: web","status: declined"]}