{"id":"6346", "title":"Proposal for an unique JaxRpcPortProxyFactoryBean based on reflection [SPR-1649]", "body":"**[Vinicius Carvalho](https://jira.spring.io/secure/ViewProfile.jspa?name=viniciuscarvalho)** opened **[SPR-1649](https://jira.spring.io/browse/SPR-1649?redirect=false)** and commented

Hello this is a proposal for Spring have an unique JaxRpcPortProxyFactoryBean. As found on the docs we need to implement our own version of it every time we have a complex bean mapping. My proposal is to have an unique reflection based class that would handle this for us, by adding only two extra properties:
A map of the class name and the Qname used on the WSDL
and a namespace of the beans.

Here's one example of the class:

public class ReflectionJaxPortProxyFactoryBean extends
JaxRpcPortProxyFactoryBean {
private Map registredClasses;
private String namespace;

    @Override
    protected void postProcessJaxRpcService(Service service) {
    	TypeMappingRegistry tmr = service.getTypeMappingRegistry();
    	TypeMapping tm = tmr.createTypeMapping();
    	Set keySet = registredClasses.keySet();
    	Iterator it = keySet.iterator();
    	while(it.hasNext()){
    		String name = (String) it.next();
    		String className = (String) registredClasses.get(name);
    		QName qName = new QName(namespace,name);
    		Class clazz;
    		try {
    			clazz = Class.forName(className);
    		} catch (ClassNotFoundException e) {
    			throw new RuntimeException(\"Class not found: \" + className,e);
    		}
    		tm.register(clazz,qName,new BeanSerializerFactory(clazz,qName),new BeanDeserializerFactory(clazz,qName));
    		
    	}
    	tmr.register(\"http://schemas.xmlsoap.org/soap/encoding/\",tm);
    }
    public String getNamespace() {
    	return namespace;
    }
    
    public void setNamespace(String namespace) {
    	this.namespace = namespace;
    }
    
    public Map getRegistredClasses() {
    	return registredClasses;
    }
    
    public void setRegistredClasses(Map registredClasses) {
    	this.registredClasses = registredClasses;
    }

Using it would be would just require the user to pass the classnames and the namespace:
\\<bean id=\"WSLocalInterface\" class=\"net.vcc.eai.remoting.ReflectionJaxPortProxyFactoryBean\">
\\<property name=\"serviceInterface\">
\\<value>net.vcc.spring.remote.service.UserService\\</value>
\\</property>
\\<property name=\"wsdlDocumentUrl\">
\\<value>http://localhost:8080/springremoting/services/UserServiceImpl?wsdl\\</value>
\\</property>
\\<property name=\"namespaceUri\">
\\<value>http://impl.service.remote.spring.vcc.net\\</value>
\\</property>
\\<property name=\"serviceFactoryClass\">
\\<value>org.apache.axis.client.ServiceFactory\\</value>
\\</property>
\\<property name=\"serviceName\">
\\<value>UserServiceImplService\\</value>
\\</property>
\\<property name=\"portName\">
\\<value>UserServiceImpl\\</value>
\\</property>
\\<property name=\"namespace\">
\\<value>http://beans.remote.spring.vcc.net\\</value>
\\</property>
\\<property name=\"registredClasses\">
\\<map>
\\<entry>
\\<key>User\\</key>
\\<value>net.vcc.spring.remote.beans.User\\</value>
\\</entry>
\\<entry>
\\<key>Car\\</key>
\\<value>net.vcc.spring.remote.beans.Car\\</value>
\\</entry>			
\\</map>
\\</property>
\\</bean>


---

**Affects:** 1.2.6
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6346","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6346/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6346/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/6346/events","html_url":"https://github.com/spring-projects/spring-framework/issues/6346","id":398062996,"node_id":"MDU6SXNzdWUzOTgwNjI5OTY=","number":6346,"title":"Proposal for an unique JaxRpcPortProxyFactoryBean based on reflection [SPR-1649]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false,"description":"Issues in web modules (web, webmvc, webflux, websocket)"},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false,"description":"A suggestion or change that we don't feel we should currently apply"},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false,"description":"A general enhancement"}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2006-01-30T00:54:28Z","updated_at":"2012-06-19T09:37:29Z","closed_at":"2012-06-19T09:37:29Z","author_association":"COLLABORATOR","body":"**[Vinicius Carvalho](https://jira.spring.io/secure/ViewProfile.jspa?name=viniciuscarvalho)** opened **[SPR-1649](https://jira.spring.io/browse/SPR-1649?redirect=false)** and commented\n\nHello this is a proposal for Spring have an unique JaxRpcPortProxyFactoryBean. As found on the docs we need to implement our own version of it every time we have a complex bean mapping. My proposal is to have an unique reflection based class that would handle this for us, by adding only two extra properties:\nA map of the class name and the Qname used on the WSDL\nand a namespace of the beans.\n\nHere's one example of the class:\n\npublic class ReflectionJaxPortProxyFactoryBean extends\nJaxRpcPortProxyFactoryBean {\nprivate Map registredClasses;\nprivate String namespace;\n\n    @Override\n    protected void postProcessJaxRpcService(Service service) {\n    \tTypeMappingRegistry tmr = service.getTypeMappingRegistry();\n    \tTypeMapping tm = tmr.createTypeMapping();\n    \tSet keySet = registredClasses.keySet();\n    \tIterator it = keySet.iterator();\n    \twhile(it.hasNext()){\n    \t\tString name = (String) it.next();\n    \t\tString className = (String) registredClasses.get(name);\n    \t\tQName qName = new QName(namespace,name);\n    \t\tClass clazz;\n    \t\ttry {\n    \t\t\tclazz = Class.forName(className);\n    \t\t} catch (ClassNotFoundException e) {\n    \t\t\tthrow new RuntimeException(\"Class not found: \" + className,e);\n    \t\t}\n    \t\ttm.register(clazz,qName,new BeanSerializerFactory(clazz,qName),new BeanDeserializerFactory(clazz,qName));\n    \t\t\n    \t}\n    \ttmr.register(\"http://schemas.xmlsoap.org/soap/encoding/\",tm);\n    }\n    public String getNamespace() {\n    \treturn namespace;\n    }\n    \n    public void setNamespace(String namespace) {\n    \tthis.namespace = namespace;\n    }\n    \n    public Map getRegistredClasses() {\n    \treturn registredClasses;\n    }\n    \n    public void setRegistredClasses(Map registredClasses) {\n    \tthis.registredClasses = registredClasses;\n    }\n\nUsing it would be would just require the user to pass the classnames and the namespace:\n\\<bean id=\"WSLocalInterface\" class=\"net.vcc.eai.remoting.ReflectionJaxPortProxyFactoryBean\">\n\\<property name=\"serviceInterface\">\n\\<value>net.vcc.spring.remote.service.UserService\\</value>\n\\</property>\n\\<property name=\"wsdlDocumentUrl\">\n\\<value>http://localhost:8080/springremoting/services/UserServiceImpl?wsdl\\</value>\n\\</property>\n\\<property name=\"namespaceUri\">\n\\<value>http://impl.service.remote.spring.vcc.net\\</value>\n\\</property>\n\\<property name=\"serviceFactoryClass\">\n\\<value>org.apache.axis.client.ServiceFactory\\</value>\n\\</property>\n\\<property name=\"serviceName\">\n\\<value>UserServiceImplService\\</value>\n\\</property>\n\\<property name=\"portName\">\n\\<value>UserServiceImpl\\</value>\n\\</property>\n\\<property name=\"namespace\">\n\\<value>http://beans.remote.spring.vcc.net\\</value>\n\\</property>\n\\<property name=\"registredClasses\">\n\\<map>\n\\<entry>\n\\<key>User\\</key>\n\\<value>net.vcc.spring.remote.beans.User\\</value>\n\\</entry>\n\\<entry>\n\\<key>Car\\</key>\n\\<value>net.vcc.spring.remote.beans.Car\\</value>\n\\</entry>\t\t\t\n\\</map>\n\\</property>\n\\</bean>\n\n\n---\n\n**Affects:** 1.2.6\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453301239"], "labels":["in: web","status: declined","type: enhancement"]}