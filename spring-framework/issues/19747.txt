{"id":"19747", "title":"MockMvcClientHttpRequestFactory should implement AsyncClientHttpRequestFactory as well [SPR-15181]", "body":"**[Artem Bilan](https://jira.spring.io/secure/ViewProfile.jspa?name=abilan)** opened **[SPR-15181](https://jira.spring.io/browse/SPR-15181?redirect=false)** and commented

The use-case is like:

```java
AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate(new MockMvcClientHttpRequestFactory(this.mockMvc));

new DirectFieldAccessor(this.serviceAsyncGatewayHandler)
		.setPropertyValue(\"asyncRestTemplate\", asyncRestTemplate);

		this.mockMvc.perform(...);
```

To check the component based on the `AsyncRestTemplate` against some MVC environment.

But `MockMvcClientHttpRequestFactory` doesn't implement `AsyncClientHttpRequestFactory` and looks like there is no any other alternative unless home-cooked solution, for example:

```java
private final class MockMvcAsyncClientHttpRequestFactory extends MockMvcClientHttpRequestFactory
		implements AsyncClientHttpRequestFactory {

	MockMvcAsyncClientHttpRequestFactory(MockMvc mockMvc) {
		super(mockMvc);
	}

	@Override
	public AsyncClientHttpRequest createAsyncRequest(URI uri, HttpMethod httpMethod) throws IOException {
		final ClientHttpRequest syncDelegate = createRequest(uri, httpMethod);
		return new MockAsyncClientHttpRequest(httpMethod, uri) {

			@Override
			public HttpHeaders getHeaders() {
				return syncDelegate.getHeaders();
			}

			@Override
			public OutputStream getBody() throws IOException {
				return syncDelegate.getBody();
			}

			@Override
			protected ClientHttpResponse executeInternal() throws IOException {
				return syncDelegate.execute();
			}

		};
	}

}
```

Not sure that it will work for all use-case, but at least it meets my requirements even with the Basic Authentication header.

For out-of-the-box solution I suggest this modification to the `MockMvcClientHttpRequestFactory`:

```java
public class MockMvcClientHttpRequestFactory implements ClientHttpRequestFactory, AsyncClientHttpRequestFactory {

		private final MockMvc mockMvc;


		public MockMvcClientHttpRequestFactory(MockMvc mockMvc) {
			Assert.notNull(mockMvc, \"MockMvc must not be null\");
			this.mockMvc = mockMvc;
		}


		@Override
		public ClientHttpRequest createRequest(final URI uri, final HttpMethod httpMethod) throws IOException {
			return new MockClientHttpRequest(httpMethod, uri) {

				@Override
				public ClientHttpResponse executeInternal() throws IOException {
					return doExecute(httpMethod, uri.toString(), getHeaders(), getBodyAsBytes());
				}

			};
		}

		@Override
		public AsyncClientHttpRequest createAsyncRequest(URI uri, HttpMethod httpMethod) throws IOException {
			return new MockAsyncClientHttpRequest(httpMethod, uri) {

				@Override
				protected ClientHttpResponse executeInternal() throws IOException {
					return doExecute(httpMethod, uri.toString(), getHeaders(), getBodyAsBytes());
				}

			};

		}

		private ClientHttpResponse doExecute(HttpMethod httpMethod, String uri, HttpHeaders httpHeaders, byte[] body) {
			try {
				MockHttpServletRequestBuilder requestBuilder = request(httpMethod, uri);
				requestBuilder.content(body);
				requestBuilder.headers(httpHeaders);
				MvcResult mvcResult = MockMvcClientHttpRequestFactory.this.mockMvc.perform(requestBuilder).andReturn();
				MockHttpServletResponse servletResponse = mvcResult.getResponse();
				HttpStatus status = HttpStatus.valueOf(servletResponse.getStatus());
				byte[] reply = servletResponse.getContentAsByteArray();
				HttpHeaders headers = getResponseHeaders(servletResponse);
				MockClientHttpResponse clientResponse = new MockClientHttpResponse(reply, status);
				clientResponse.getHeaders().putAll(headers);
				return clientResponse;
			}
			catch (Exception ex) {
				byte[] reply = ex.toString().getBytes(StandardCharsets.UTF_8);
				return new MockClientHttpResponse(reply, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		private HttpHeaders getResponseHeaders(MockHttpServletResponse response) {
			HttpHeaders headers = new HttpHeaders();
			for (String name : response.getHeaderNames()) {
				List<String> values = response.getHeaders(name);
				for (String value : values) {
					headers.add(name, value);
				}
			}
			return headers;
		}

	}
```

Would be glad to see the fix in the `5.0`, we have a PR in Spring Integration which now doesn't use `MockMvc` because of this limitation.

Thanks


---

**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4da4f2be315e92a10da76a90ef434b0bb3c41d60
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19747","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19747/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19747/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/19747/events","html_url":"https://github.com/spring-projects/spring-framework/issues/19747","id":398204477,"node_id":"MDU6SXNzdWUzOTgyMDQ0Nzc=","number":19747,"title":"MockMvcClientHttpRequestFactory should implement AsyncClientHttpRequestFactory as well [SPR-15181]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511816,"node_id":"MDU6TGFiZWwxMTg4NTExODE2","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20test","name":"in: test","color":"e8f9de","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false},"assignees":[{"login":"rstoyanchev","id":401908,"node_id":"MDQ6VXNlcjQwMTkwOA==","avatar_url":"https://avatars1.githubusercontent.com/u/401908?v=4","gravatar_id":"","url":"https://api.github.com/users/rstoyanchev","html_url":"https://github.com/rstoyanchev","followers_url":"https://api.github.com/users/rstoyanchev/followers","following_url":"https://api.github.com/users/rstoyanchev/following{/other_user}","gists_url":"https://api.github.com/users/rstoyanchev/gists{/gist_id}","starred_url":"https://api.github.com/users/rstoyanchev/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rstoyanchev/subscriptions","organizations_url":"https://api.github.com/users/rstoyanchev/orgs","repos_url":"https://api.github.com/users/rstoyanchev/repos","events_url":"https://api.github.com/users/rstoyanchev/events{/privacy}","received_events_url":"https://api.github.com/users/rstoyanchev/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/177","html_url":"https://github.com/spring-projects/spring-framework/milestone/177","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/177/labels","id":3960950,"node_id":"MDk6TWlsZXN0b25lMzk2MDk1MA==","number":177,"title":"5.0 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":101,"state":"closed","created_at":"2019-01-10T22:05:24Z","updated_at":"2019-01-11T11:16:33Z","due_on":"2017-05-07T07:00:00Z","closed_at":"2019-01-10T22:05:24Z"},"comments":0,"created_at":"2017-01-24T03:20:48Z","updated_at":"2017-05-08T08:41:48Z","closed_at":"2017-05-08T08:41:48Z","author_association":"COLLABORATOR","body":"**[Artem Bilan](https://jira.spring.io/secure/ViewProfile.jspa?name=abilan)** opened **[SPR-15181](https://jira.spring.io/browse/SPR-15181?redirect=false)** and commented\n\nThe use-case is like:\n\n```java\nAsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate(new MockMvcClientHttpRequestFactory(this.mockMvc));\n\nnew DirectFieldAccessor(this.serviceAsyncGatewayHandler)\n\t\t.setPropertyValue(\"asyncRestTemplate\", asyncRestTemplate);\n\n\t\tthis.mockMvc.perform(...);\n```\n\nTo check the component based on the `AsyncRestTemplate` against some MVC environment.\n\nBut `MockMvcClientHttpRequestFactory` doesn't implement `AsyncClientHttpRequestFactory` and looks like there is no any other alternative unless home-cooked solution, for example:\n\n```java\nprivate final class MockMvcAsyncClientHttpRequestFactory extends MockMvcClientHttpRequestFactory\n\t\timplements AsyncClientHttpRequestFactory {\n\n\tMockMvcAsyncClientHttpRequestFactory(MockMvc mockMvc) {\n\t\tsuper(mockMvc);\n\t}\n\n\t@Override\n\tpublic AsyncClientHttpRequest createAsyncRequest(URI uri, HttpMethod httpMethod) throws IOException {\n\t\tfinal ClientHttpRequest syncDelegate = createRequest(uri, httpMethod);\n\t\treturn new MockAsyncClientHttpRequest(httpMethod, uri) {\n\n\t\t\t@Override\n\t\t\tpublic HttpHeaders getHeaders() {\n\t\t\t\treturn syncDelegate.getHeaders();\n\t\t\t}\n\n\t\t\t@Override\n\t\t\tpublic OutputStream getBody() throws IOException {\n\t\t\t\treturn syncDelegate.getBody();\n\t\t\t}\n\n\t\t\t@Override\n\t\t\tprotected ClientHttpResponse executeInternal() throws IOException {\n\t\t\t\treturn syncDelegate.execute();\n\t\t\t}\n\n\t\t};\n\t}\n\n}\n```\n\nNot sure that it will work for all use-case, but at least it meets my requirements even with the Basic Authentication header.\n\nFor out-of-the-box solution I suggest this modification to the `MockMvcClientHttpRequestFactory`:\n\n```java\npublic class MockMvcClientHttpRequestFactory implements ClientHttpRequestFactory, AsyncClientHttpRequestFactory {\n\n\t\tprivate final MockMvc mockMvc;\n\n\n\t\tpublic MockMvcClientHttpRequestFactory(MockMvc mockMvc) {\n\t\t\tAssert.notNull(mockMvc, \"MockMvc must not be null\");\n\t\t\tthis.mockMvc = mockMvc;\n\t\t}\n\n\n\t\t@Override\n\t\tpublic ClientHttpRequest createRequest(final URI uri, final HttpMethod httpMethod) throws IOException {\n\t\t\treturn new MockClientHttpRequest(httpMethod, uri) {\n\n\t\t\t\t@Override\n\t\t\t\tpublic ClientHttpResponse executeInternal() throws IOException {\n\t\t\t\t\treturn doExecute(httpMethod, uri.toString(), getHeaders(), getBodyAsBytes());\n\t\t\t\t}\n\n\t\t\t};\n\t\t}\n\n\t\t@Override\n\t\tpublic AsyncClientHttpRequest createAsyncRequest(URI uri, HttpMethod httpMethod) throws IOException {\n\t\t\treturn new MockAsyncClientHttpRequest(httpMethod, uri) {\n\n\t\t\t\t@Override\n\t\t\t\tprotected ClientHttpResponse executeInternal() throws IOException {\n\t\t\t\t\treturn doExecute(httpMethod, uri.toString(), getHeaders(), getBodyAsBytes());\n\t\t\t\t}\n\n\t\t\t};\n\n\t\t}\n\n\t\tprivate ClientHttpResponse doExecute(HttpMethod httpMethod, String uri, HttpHeaders httpHeaders, byte[] body) {\n\t\t\ttry {\n\t\t\t\tMockHttpServletRequestBuilder requestBuilder = request(httpMethod, uri);\n\t\t\t\trequestBuilder.content(body);\n\t\t\t\trequestBuilder.headers(httpHeaders);\n\t\t\t\tMvcResult mvcResult = MockMvcClientHttpRequestFactory.this.mockMvc.perform(requestBuilder).andReturn();\n\t\t\t\tMockHttpServletResponse servletResponse = mvcResult.getResponse();\n\t\t\t\tHttpStatus status = HttpStatus.valueOf(servletResponse.getStatus());\n\t\t\t\tbyte[] reply = servletResponse.getContentAsByteArray();\n\t\t\t\tHttpHeaders headers = getResponseHeaders(servletResponse);\n\t\t\t\tMockClientHttpResponse clientResponse = new MockClientHttpResponse(reply, status);\n\t\t\t\tclientResponse.getHeaders().putAll(headers);\n\t\t\t\treturn clientResponse;\n\t\t\t}\n\t\t\tcatch (Exception ex) {\n\t\t\t\tbyte[] reply = ex.toString().getBytes(StandardCharsets.UTF_8);\n\t\t\t\treturn new MockClientHttpResponse(reply, HttpStatus.INTERNAL_SERVER_ERROR);\n\t\t\t}\n\t\t}\n\n\t\tprivate HttpHeaders getResponseHeaders(MockHttpServletResponse response) {\n\t\t\tHttpHeaders headers = new HttpHeaders();\n\t\t\tfor (String name : response.getHeaderNames()) {\n\t\t\t\tList<String> values = response.getHeaders(name);\n\t\t\t\tfor (String value : values) {\n\t\t\t\t\theaders.add(name, value);\n\t\t\t\t}\n\t\t\t}\n\t\t\treturn headers;\n\t\t}\n\n\t}\n```\n\nWould be glad to see the fix in the `5.0`, we have a PR in Spring Integration which now doesn't use `MockMvc` because of this limitation.\n\nThanks\n\n\n---\n\n**Referenced from:** commits https://github.com/spring-projects/spring-framework/commit/4da4f2be315e92a10da76a90ef434b0bb3c41d60\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":[], "labels":["in: test","type: enhancement"]}