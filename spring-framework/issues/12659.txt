{"id":"12659", "title":"Tiles ELAttributeElevator is not properly instanciated. [SPR-8004]", "body":"**[Benoit Heurter](https://jira.spring.io/secure/ViewProfile.jspa?name=bheurter)** opened **[SPR-8004](https://jira.spring.io/browse/SPR-8004?redirect=false)** and commented

Note : This issue is NOT related to #11910 !

in TilesConfigurator class, the ELAttributeElevator class is directly instanciated instead of using the property ATTRIBUTE_EVALUATOR_INIT_PARAM

```
public void registerEvaluator(BasicTilesContainer container) {
  logger.debug(\"Registering Tiles 2.2 AttributeEvaluatorFactory for JSP 2.1\");
  try {
	...
	ELAttributeEvaluator evaluator = new ELAttributeEvaluator();
	...	
  }catch(...)
}
```

Hence, the property is defined in tilesPropertyMap :

```
public void afterPropertiesSet() throws TilesException {
	boolean activateEl = false;
	if (tilesElPresent) {
		activateEl = new JspExpressionChecker().isExpressionFactoryAvailable();
		if (!this.tilesPropertyMap.containsKey(TilesContainerFactory.ATTRIBUTE_EVALUATOR_INIT_PARAM)) {
			this.tilesPropertyMap.put(TilesContainerFactory.ATTRIBUTE_EVALUATOR_INIT_PARAM, activateEl ?
					\"org.apache.tiles.evaluator.el.ELAttributeEvaluator\" : DirectAttributeEvaluator.class.getName());
		}
	}
	....
}
```

Result is that we can't subclass ELAttributeEvaluator to add a FunctionMapper.

( my goal is to be able to use expression=\"${fn:length(myArray)}\" in tiles definition)

---

**Affects:** 3.0.5
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12659","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12659/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12659/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/12659/events","html_url":"https://github.com/spring-projects/spring-framework/issues/12659","id":398110476,"node_id":"MDU6SXNzdWUzOTgxMTA0NzY=","number":12659,"title":"Tiles ELAttributeElevator is not properly instanciated. [SPR-8004]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511910,"node_id":"MDU6TGFiZWwxMTg4NTExOTEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20web","name":"in: web","color":"e8f9de","default":false},{"id":1188511944,"node_id":"MDU6TGFiZWwxMTg4NTExOTQ0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20declined","name":"status: declined","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2011-02-28T07:51:31Z","updated_at":"2019-01-12T16:26:05Z","closed_at":"2018-12-28T11:39:08Z","author_association":"COLLABORATOR","body":"**[Benoit Heurter](https://jira.spring.io/secure/ViewProfile.jspa?name=bheurter)** opened **[SPR-8004](https://jira.spring.io/browse/SPR-8004?redirect=false)** and commented\n\nNote : This issue is NOT related to #11910 !\n\nin TilesConfigurator class, the ELAttributeElevator class is directly instanciated instead of using the property ATTRIBUTE_EVALUATOR_INIT_PARAM\n\n```\npublic void registerEvaluator(BasicTilesContainer container) {\n  logger.debug(\"Registering Tiles 2.2 AttributeEvaluatorFactory for JSP 2.1\");\n  try {\n\t...\n\tELAttributeEvaluator evaluator = new ELAttributeEvaluator();\n\t...\t\n  }catch(...)\n}\n```\n\nHence, the property is defined in tilesPropertyMap :\n\n```\npublic void afterPropertiesSet() throws TilesException {\n\tboolean activateEl = false;\n\tif (tilesElPresent) {\n\t\tactivateEl = new JspExpressionChecker().isExpressionFactoryAvailable();\n\t\tif (!this.tilesPropertyMap.containsKey(TilesContainerFactory.ATTRIBUTE_EVALUATOR_INIT_PARAM)) {\n\t\t\tthis.tilesPropertyMap.put(TilesContainerFactory.ATTRIBUTE_EVALUATOR_INIT_PARAM, activateEl ?\n\t\t\t\t\t\"org.apache.tiles.evaluator.el.ELAttributeEvaluator\" : DirectAttributeEvaluator.class.getName());\n\t\t}\n\t}\n\t....\n}\n```\n\nResult is that we can't subclass ELAttributeEvaluator to add a FunctionMapper.\n\n( my goal is to be able to use expression=\"${fn:length(myArray)}\" in tiles definition)\n\n---\n\n**Affects:** 3.0.5\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453357345","453357346"], "labels":["in: web","status: declined"]}