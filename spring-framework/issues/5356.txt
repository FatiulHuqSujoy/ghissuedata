{"id":"5356", "title":"[sandbox] Spring-JMX:  [SPR-628]", "body":"**[Jean Carriere](https://jira.spring.io/secure/ViewProfile.jspa?name=jcarriere)** opened **[SPR-628](https://jira.spring.io/browse/SPR-628?redirect=false)** and commented

I've tried to use the jmx package, the MetadataModelMBeanInfoAssembler, which is in the sandbox, with jboss.

My spring context is the following:
\\<bean id=\"ehcacheMonitoring\"
class=\"mypackage.service.mbean.EHCacheMonitoring\">
\\</bean>

<bean id=\"jmxAdapter\"
class=\"org.springframework.jmx.JmxMBeanAdapter\"

> \\<property name=\"assembler\">
> 
>> \\<ref bean=\"metadataAssembler\"/>
>> \\</property>
>> \\<property name=\"namingStrategy\">
>> \\<ref bean=\"metadataNaming\"/>
>> \\</property>
>> \\</bean>
> 
> \\<bean id=\"metadataAssembler\"
> class=\"org.springframework.jmx.assemblers.metadata.MetadataModelMBeanInfoAssembler\">
> \\<property name=\"attributeSource\">
> \\<ref bean=\"attributesImpl\"/>
> \\</property>
> \\</bean>
> 
> \\<bean id=\"metadataNaming\"
> class=\"org.springframework.jmx.naming.MetadataNamingStrategy\">
> \\<property name=\"attributeSource\">
> \\<ref bean=\"attributesImpl\"/>
> \\</property>
> \\</bean>
> 
> \\<bean id=\"attributesImpl\"
> class=\"org.springframework.jmx.metadata.support.commons.CommonsAttributesJmxAttributeSource\"/>
> 
> My bean is
> *
> * @`@org`.springframework.jmx.metadata.support.ManagedResource(...)
>   */
>   public class EHCacheMonitoring implements EHCacheMonitoringMBean
>   {
> 
> /**
> * @`@org`.springframework.jmx.metadata.support.ManagedOperation(...)
>   */
>   public String showCachesInfo() throws CacheException
>   {
>   .....
>   }
> 
> }
> 
> The exception is
> 10:01:59,859 ERROR [ContextLoader] Context initialization failed
> org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'jmxAdapter' defined in ServletContext resource [/WEB-INF/classes/serviceContext.xml]: Initialization of bean failed; nested exception is org.springframework.jmx.exceptions.MBeanAssemblyException: A JMX error occured when trying to assemble the management interface metadata.; nested exception is javax.management.MBeanRegistrationException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]
> org.springframework.jmx.exceptions.MBeanAssemblyException: A JMX error occured when trying to assemble the management interface metadata.; nested exception is javax.management.MBeanRegistrationException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]
> MBeanException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]
> Cause: java.lang.ClassCastException
> at org.jboss.mx.server.registry.BasicMBeanRegistry.registerMBean(BasicMBeanRegistry.java:160)
> at sun.reflect.GeneratedMethodAccessor1.invoke(Unknown Source)
> at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
> at java.lang.reflect.Method.invoke(Method.java:324)
> at org.jboss.mx.capability.ReflectedMBeanDispatcher.invoke(ReflectedMBeanDispatcher.java:284)
> at org.jboss.mx.interceptor.ObjectReferenceInterceptor.invoke(ObjectReferenceInterceptor.java:59)
> at org.jboss.mx.interceptor.MBeanAttributeInterceptor.invoke(MBeanAttributeInterceptor.java:43)
> at org.jboss.mx.interceptor.PersistenceInterceptor2.invoke(PersistenceInterceptor2.java:93)
> at org.jboss.mx.server.MBeanInvoker.invoke(MBeanInvoker.java:76)
> at javax.management.modelmbean.RequiredModelMBean.invoke(RequiredModelMBean.java:144)
> at org.jboss.mx.server.MBeanServerImpl.invoke(MBeanServerImpl.java:546)
> at org.jboss.mx.server.MBeanServerImpl.registerMBean(MBeanServerImpl.java:997)
> at org.jboss.mx.server.MBeanServerImpl.registerMBean(MBeanServerImpl.java:327)
> at org.springframework.jmx.JmxMBeanAdapter.registerSimpleBean(JmxMBeanAdapter.java:295)
> at org.springframework.jmx.JmxMBeanAdapter.registerBean(JmxMBeanAdapter.java:272)
> at org.springframework.jmx.JmxMBeanAdapter.registerBeans(JmxMBeanAdapter.java:218)
> at org.springframework.jmx.JmxMBeanAdapter.afterPropertiesSet(JmxMBeanAdapter.java:117)
> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1037)
> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:305)
> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:223)
> at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:236)
> at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:159)
> at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:261)
> at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)
> at org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:131)
> at org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:177)
> at org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:105)
> at org.springframework.web.context.ContextLoaderListener.contextInitialized(ContextLoaderListener.java:4
> at org.apache.catalina.core.StandardContext.listenerStart(StandardContext.java:3270)
> at org.apache.catalina.core.StandardContext.start(StandardContext.java:3599)
> at org.apache.catalina.core.ContainerBase.addChildInternal(ContainerBase.java:821)
> at org.apache.catalina.core.ContainerBase.addChild(ContainerBase.java:807)
> at org.apache.catalina.core.StandardHost.addChild(StandardHost.java:579)
> at org.jboss.web.tomcat.tc4.EmbeddedTomcatService.createWebContext(EmbeddedTomcatService.java:530)
> at org.jboss.web.tomcat.tc4.EmbeddedTomcatService.performDeploy(EmbeddedTomcatService.java:309)
> at org.jboss.web.AbstractWebContainer.start(AbstractWebContainer.java:428)
> at org.jboss.deployment.MainDeployer.start(MainDeployer.java:832)
> at org.jboss.deployment.MainDeployer.deploy(MainDeployer.java:642)
> at org.jboss.deployment.MainDeployer.deploy(MainDeployer.java:605)
> at sun.reflect.GeneratedMethodAccessor22.invoke(Unknown Source)
> at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
> at java.lang.reflect.Method.invoke(Method.java:324)
> at org.jboss.mx.capability.ReflectedMBeanDispatcher.invoke(ReflectedMBeanDispatcher.java:284)
> at org.jboss.mx.server.MBeanServerImpl.invoke(MBeanServerImpl.java:546)
> at org.jboss.jmx.adaptor.control.Server.invokeOpByName(Server.java:229)
> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.invokeOpByName(HtmlAdaptorServlet.java:266)
> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.processRequest(HtmlAdaptorServlet.java:81)
> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.doGet(HtmlAdaptorServlet.java:56)
> at javax.servlet.http.HttpServlet.service(HttpServlet.java:740)
> at javax.servlet.http.HttpServlet.service(HttpServlet.java:853)
> at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:247)
> at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:193)
> at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:256)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)
> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)
> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)
> at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:191)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)
> at org.jboss.web.tomcat.security.JBossSecurityMgrRealm.invoke(JBossSecurityMgrRealm.java:220)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.apache.catalina.valves.CertificatesValve.invoke(CertificatesValve.java:246)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.jboss.web.tomcat.tc4.statistics.ContainerStatsValve.invoke(ContainerStatsValve.java:76)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)
> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)
> at org.apache.catalina.core.StandardContext.invoke(StandardContext.java:2416)
> at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:180)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)
> at org.apache.catalina.valves.ErrorDispatcherValve.invoke(ErrorDispatcherValve.java:171)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:172)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.jboss.web.tomcat.security.SecurityAssociationValve.invoke(SecurityAssociationValve.java:65)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:577)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)
> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)
> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)
> at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:174)
> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)
> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)
> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)
> at org.apache.coyote.tomcat4.CoyoteAdapter.service(CoyoteAdapter.java:223)
> at org.apache.coyote.http11.Http11Processor.process(Http11Processor.java:601)
> at org.apache.coyote.http11.Http11Protocol$Http11ConnectionHandler.processConnection(Http11Protocol.java:392)
> at org.apache.tomcat.util.net.TcpWorkerThread.runIt(PoolTcpEndpoint.java:565)
> at org.apache.tomcat.util.threads.ThreadPool$ControlRunnable.run(ThreadPool.java:619)
> at java.lang.Thread.run(Thread.java:534)
> 
> nb: I've checked out the sandbox on 2005/12/01

---

**Affects:** 1.2 RC1
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5356","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5356/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5356/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/5356/events","html_url":"https://github.com/spring-projects/spring-framework/issues/5356","id":398054283,"node_id":"MDU6SXNzdWUzOTgwNTQyODM=","number":5356,"title":"[sandbox] Spring-JMX:  [SPR-628]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1188511953,"node_id":"MDU6TGFiZWwxMTg4NTExOTUz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20bug","name":"type: bug","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/16","html_url":"https://github.com/spring-projects/spring-framework/milestone/16","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/16/labels","id":3960786,"node_id":"MDk6TWlsZXN0b25lMzk2MDc4Ng==","number":16,"title":"1.2 RC1","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":79,"state":"closed","created_at":"2019-01-10T22:02:10Z","updated_at":"2019-01-10T23:08:29Z","due_on":null,"closed_at":"2019-01-10T22:02:10Z"},"comments":6,"created_at":"2005-01-17T03:57:29Z","updated_at":"2019-01-13T08:58:41Z","closed_at":"2005-02-02T20:02:50Z","author_association":"COLLABORATOR","body":"**[Jean Carriere](https://jira.spring.io/secure/ViewProfile.jspa?name=jcarriere)** opened **[SPR-628](https://jira.spring.io/browse/SPR-628?redirect=false)** and commented\n\nI've tried to use the jmx package, the MetadataModelMBeanInfoAssembler, which is in the sandbox, with jboss.\n\nMy spring context is the following:\n\\<bean id=\"ehcacheMonitoring\"\nclass=\"mypackage.service.mbean.EHCacheMonitoring\">\n\\</bean>\n\n<bean id=\"jmxAdapter\"\nclass=\"org.springframework.jmx.JmxMBeanAdapter\"\n\n> \\<property name=\"assembler\">\n> \n>> \\<ref bean=\"metadataAssembler\"/>\n>> \\</property>\n>> \\<property name=\"namingStrategy\">\n>> \\<ref bean=\"metadataNaming\"/>\n>> \\</property>\n>> \\</bean>\n> \n> \\<bean id=\"metadataAssembler\"\n> class=\"org.springframework.jmx.assemblers.metadata.MetadataModelMBeanInfoAssembler\">\n> \\<property name=\"attributeSource\">\n> \\<ref bean=\"attributesImpl\"/>\n> \\</property>\n> \\</bean>\n> \n> \\<bean id=\"metadataNaming\"\n> class=\"org.springframework.jmx.naming.MetadataNamingStrategy\">\n> \\<property name=\"attributeSource\">\n> \\<ref bean=\"attributesImpl\"/>\n> \\</property>\n> \\</bean>\n> \n> \\<bean id=\"attributesImpl\"\n> class=\"org.springframework.jmx.metadata.support.commons.CommonsAttributesJmxAttributeSource\"/>\n> \n> My bean is\n> *\n> * @`@org`.springframework.jmx.metadata.support.ManagedResource(...)\n>   */\n>   public class EHCacheMonitoring implements EHCacheMonitoringMBean\n>   {\n> \n> /**\n> * @`@org`.springframework.jmx.metadata.support.ManagedOperation(...)\n>   */\n>   public String showCachesInfo() throws CacheException\n>   {\n>   .....\n>   }\n> \n> }\n> \n> The exception is\n> 10:01:59,859 ERROR [ContextLoader] Context initialization failed\n> org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'jmxAdapter' defined in ServletContext resource [/WEB-INF/classes/serviceContext.xml]: Initialization of bean failed; nested exception is org.springframework.jmx.exceptions.MBeanAssemblyException: A JMX error occured when trying to assemble the management interface metadata.; nested exception is javax.management.MBeanRegistrationException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]\n> org.springframework.jmx.exceptions.MBeanAssemblyException: A JMX error occured when trying to assemble the management interface metadata.; nested exception is javax.management.MBeanRegistrationException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]\n> MBeanException: preRegister() failed [ObjectName='spring:bean=EHCacheMonitoring', Class=javax.management.modelmbean.RequiredModelMBean (javax.management.modelmbean.RequiredModelMBean@f1b95f)]\n> Cause: java.lang.ClassCastException\n> at org.jboss.mx.server.registry.BasicMBeanRegistry.registerMBean(BasicMBeanRegistry.java:160)\n> at sun.reflect.GeneratedMethodAccessor1.invoke(Unknown Source)\n> at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\n> at java.lang.reflect.Method.invoke(Method.java:324)\n> at org.jboss.mx.capability.ReflectedMBeanDispatcher.invoke(ReflectedMBeanDispatcher.java:284)\n> at org.jboss.mx.interceptor.ObjectReferenceInterceptor.invoke(ObjectReferenceInterceptor.java:59)\n> at org.jboss.mx.interceptor.MBeanAttributeInterceptor.invoke(MBeanAttributeInterceptor.java:43)\n> at org.jboss.mx.interceptor.PersistenceInterceptor2.invoke(PersistenceInterceptor2.java:93)\n> at org.jboss.mx.server.MBeanInvoker.invoke(MBeanInvoker.java:76)\n> at javax.management.modelmbean.RequiredModelMBean.invoke(RequiredModelMBean.java:144)\n> at org.jboss.mx.server.MBeanServerImpl.invoke(MBeanServerImpl.java:546)\n> at org.jboss.mx.server.MBeanServerImpl.registerMBean(MBeanServerImpl.java:997)\n> at org.jboss.mx.server.MBeanServerImpl.registerMBean(MBeanServerImpl.java:327)\n> at org.springframework.jmx.JmxMBeanAdapter.registerSimpleBean(JmxMBeanAdapter.java:295)\n> at org.springframework.jmx.JmxMBeanAdapter.registerBean(JmxMBeanAdapter.java:272)\n> at org.springframework.jmx.JmxMBeanAdapter.registerBeans(JmxMBeanAdapter.java:218)\n> at org.springframework.jmx.JmxMBeanAdapter.afterPropertiesSet(JmxMBeanAdapter.java:117)\n> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1037)\n> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:305)\n> at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:223)\n> at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:236)\n> at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:159)\n> at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:261)\n> at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:317)\n> at org.springframework.web.context.support.AbstractRefreshableWebApplicationContext.refresh(AbstractRefreshableWebApplicationContext.java:131)\n> at org.springframework.web.context.ContextLoader.createWebApplicationContext(ContextLoader.java:177)\n> at org.springframework.web.context.ContextLoader.initWebApplicationContext(ContextLoader.java:105)\n> at org.springframework.web.context.ContextLoaderListener.contextInitialized(ContextLoaderListener.java:4\n> at org.apache.catalina.core.StandardContext.listenerStart(StandardContext.java:3270)\n> at org.apache.catalina.core.StandardContext.start(StandardContext.java:3599)\n> at org.apache.catalina.core.ContainerBase.addChildInternal(ContainerBase.java:821)\n> at org.apache.catalina.core.ContainerBase.addChild(ContainerBase.java:807)\n> at org.apache.catalina.core.StandardHost.addChild(StandardHost.java:579)\n> at org.jboss.web.tomcat.tc4.EmbeddedTomcatService.createWebContext(EmbeddedTomcatService.java:530)\n> at org.jboss.web.tomcat.tc4.EmbeddedTomcatService.performDeploy(EmbeddedTomcatService.java:309)\n> at org.jboss.web.AbstractWebContainer.start(AbstractWebContainer.java:428)\n> at org.jboss.deployment.MainDeployer.start(MainDeployer.java:832)\n> at org.jboss.deployment.MainDeployer.deploy(MainDeployer.java:642)\n> at org.jboss.deployment.MainDeployer.deploy(MainDeployer.java:605)\n> at sun.reflect.GeneratedMethodAccessor22.invoke(Unknown Source)\n> at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\n> at java.lang.reflect.Method.invoke(Method.java:324)\n> at org.jboss.mx.capability.ReflectedMBeanDispatcher.invoke(ReflectedMBeanDispatcher.java:284)\n> at org.jboss.mx.server.MBeanServerImpl.invoke(MBeanServerImpl.java:546)\n> at org.jboss.jmx.adaptor.control.Server.invokeOpByName(Server.java:229)\n> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.invokeOpByName(HtmlAdaptorServlet.java:266)\n> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.processRequest(HtmlAdaptorServlet.java:81)\n> at org.jboss.jmx.adaptor.html.HtmlAdaptorServlet.doGet(HtmlAdaptorServlet.java:56)\n> at javax.servlet.http.HttpServlet.service(HttpServlet.java:740)\n> at javax.servlet.http.HttpServlet.service(HttpServlet.java:853)\n> at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:247)\n> at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:193)\n> at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:256)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)\n> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)\n> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)\n> at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:191)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)\n> at org.jboss.web.tomcat.security.JBossSecurityMgrRealm.invoke(JBossSecurityMgrRealm.java:220)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.apache.catalina.valves.CertificatesValve.invoke(CertificatesValve.java:246)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.jboss.web.tomcat.tc4.statistics.ContainerStatsValve.invoke(ContainerStatsValve.java:76)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)\n> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)\n> at org.apache.catalina.core.StandardContext.invoke(StandardContext.java:2416)\n> at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:180)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)\n> at org.apache.catalina.valves.ErrorDispatcherValve.invoke(ErrorDispatcherValve.java:171)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:172)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.jboss.web.tomcat.security.SecurityAssociationValve.invoke(SecurityAssociationValve.java:65)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:577)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:641)\n> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)\n> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)\n> at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:174)\n> at org.apache.catalina.core.StandardPipeline$StandardPipelineValveContext.invokeNext(StandardPipeline.java:643)\n> at org.apache.catalina.core.StandardPipeline.invoke(StandardPipeline.java:480)\n> at org.apache.catalina.core.ContainerBase.invoke(ContainerBase.java:995)\n> at org.apache.coyote.tomcat4.CoyoteAdapter.service(CoyoteAdapter.java:223)\n> at org.apache.coyote.http11.Http11Processor.process(Http11Processor.java:601)\n> at org.apache.coyote.http11.Http11Protocol$Http11ConnectionHandler.processConnection(Http11Protocol.java:392)\n> at org.apache.tomcat.util.net.TcpWorkerThread.runIt(PoolTcpEndpoint.java:565)\n> at org.apache.tomcat.util.threads.ThreadPool$ControlRunnable.run(ThreadPool.java:619)\n> at java.lang.Thread.run(Thread.java:534)\n> \n> nb: I've checked out the sandbox on 2005/12/01\n\n---\n\n**Affects:** 1.2 RC1\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453291002","453291003","453291004","453291008","453291009","453291010"], "labels":["in: core","type: bug"]}