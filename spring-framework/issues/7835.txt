{"id":"7835", "title":"SpringJtaSynchronizationAdapter setRollbackOnly hides causing exception within Weblogic 8.1 [SPR-3149]", "body":"**[Karl Baum](https://jira.spring.io/secure/ViewProfile.jspa?name=kbaum)** opened **[SPR-3149](https://jira.spring.io/browse/SPR-3149?redirect=false)** and commented

In previous releases,  Spring was swallowing Hibernate exceptions within the TransactionSynchronization beforeCompletion method.  For more detail see the following link:

http://opensource.atlassian.com/projects/spring/browse/SPR-2270

As the JIRA issue states, the issue was addressed by letting the runtime exception through, but still calling setRollbackOnly.

The issue is that with this fix within Weblogic 8.1, even though Spring lets the exception propogate up, Weblogic will always output a vague AppSetRollbackOnlyException cause whenever setRollbackOnly is invoked.  Hence, the underlying exception is still hidden from the developer.

We applied the Spring 2.X fix to our version of Spring 1.2.5.

public void beforeCompletion() {
try {
boolean readOnly =
TransactionSynchronizationManager.isCurrentTransactionReadOnly();
this.springSessionSynchronization.beforeCommit(readOnly);
} catch (RuntimeException ex) {
setRollbackOnlyIfPossible();
throw ex;
} catch (Error err) {
setRollbackOnlyIfPossible();
throw err;
} finally {
// Unbind the SessionHolder from the thread early, to avoid issues
// with strict JTA implementations that issue warnings when doing JDBC
// operations after transaction completion (e.g. Connection.getWarnings).
this.beforeCompletionCalled = true;
this.springSessionSynchronization.beforeCompletion();
}
}

Notice the code above calls setRollbackOnly and rethrows the exception.  With this in place, however, we get the following vague exception from Weblogic:

<Feb 8, 2007 11:51:08 AM EST> \\<Info> \\<EJB> \\<BEA-010051> <EJB Exception occurred during invocation from home: com.mycompany.MyRemoteEJB_inpik2_HomeImpl@bdfdb4 threw exception: javax.ejb.TransactionRolledbackLocalException: Error
committing transaction:; nested exception is: weblogic.transaction.internal.AppSetRollbackOnlyException
javax.ejb.TransactionRolledbackLocalException: Error committing transaction:; nested exception is: weblogic.transaction.internal.AppSetRollb
ackOnlyException
weblogic.transaction.internal.AppSetRollbackOnlyException
at weblogic.transaction.internal.TransactionImpl.setRollbackOnly()V(TransactionImpl.java:504)
at weblogic.transaction.internal.TransactionManagerImpl.setRollbackOnly()V(TransactionManagerImpl.java:337)
at weblogic.transaction.internal.TransactionManagerImpl.setRollbackOnly()V(TransactionManagerImpl.java:331)
at org.springframework.transaction.jta.UserTransactionAdapter.setRollbackOnly()V(UserTransactionAdapter.java:86)
at org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.setRollbackOnlyIfPossible()V(SessionFactoryUtils
.java:1030)
at org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.beforeCompletion()V(SessionFactoryUtils.java:100
8)
at weblogic.transaction.internal.ServerSCInfo.callBeforeCompletions(Lweblogic.transaction.internal.TransactionImpl;)V(ServerSCInfo.j
ava:1010)
at weblogic.transaction.internal.ServerSCInfo.startPrePrepareAndChain(Lweblogic.transaction.internal.ServerTransactionImpl;I)V(Serve
rSCInfo.java:115)
at weblogic.transaction.internal.ServerTransactionImpl.localPrePrepareAndChain()V(ServerTransactionImpl.java:1216)
at weblogic.transaction.internal.ServerTransactionImpl.globalPrePrepare()V(ServerTransactionImpl.java:1990)
at weblogic.transaction.internal.ServerTransactionImpl.internalCommit()V(ServerTransactionImpl.java:275)
at weblogic.transaction.internal.ServerTransactionImpl.commit()V(ServerTransactionImpl.java:246)
at weblogic.ejb20.internal.BaseEJBLocalObject.postInvoke(Lweblogic.ejb20.internal.InvocationWrapper;Ljava.lang.Throwable;)V(BaseEJBL
ocalObject.java:363)
at
...
...

We changed the previous patch to not call setRollbackOnly and allow the exception to propogate:

public void beforeCompletion() {
try {
boolean readOnly =
TransactionSynchronizationManager.isCurrentTransactionReadOnly();
this.springSessionSynchronization.beforeCommit(readOnly);
} finally {
this.beforeCompletionCalled = true;
this.springSessionSynchronization.beforeCompletion();
}
}

We now get the underlying exception within our stack trace:

<Feb 8, 2007 12:03:19 PM EST> \\<Info> \\<EJB> \\<BEA-010051> <EJB Exception occurred during invocation from home: om.mycompany.MyRemoteEJB_inpik2_HomeImpl@acc47a threw exception: javax.ejb.TransactionRolledbackLocalException: Error
committing transaction:; nested exception is: org.springframework.jdbc.UncategorizedSQLException: Hibernate transaction synchronization; un
categorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001]; error code [-911]; DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exce
ption is com.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2
javax.ejb.TransactionRolledbackLocalException: Error committing transaction:; nested exception is: org.springframework.jdbc.UncategorizedSQL
Exception: Hibernate transaction synchronization; uncategorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001]; error code [-911]; DB2 SQL error: SQLC
ODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exception is com.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001,
SQLERRMC: 2
org.springframework.jdbc.UncategorizedSQLException: Hibernate transaction synchronization; uncategorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001
]; error code [-911]; DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exception is com.ibm.db2.jcc.a.SqlException: DB2 SQ
L error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2
com.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2
at com.ibm.db2.jcc.a.cq.e(Lcom.ibm.db2.jcc.a.da;)I(cq.java:1482)
at com.ibm.db2.jcc.c.bc.s(Lcom.ibm.db2.jcc.a.ct;)V(bc.java:721)
at com.ibm.db2.jcc.c.bc.k(Lcom.ibm.db2.jcc.a.ct;)V(bc.java:375)
at com.ibm.db2.jcc.c.bc.a(Lcom.ibm.db2.jcc.a.cg;)V(bc.java:63)
at com.ibm.db2.jcc.c.q.a(Lcom.ibm.db2.jcc.a.cg;)V(q.java:64)
at com.ibm.db2.jcc.c.bp.c()V(bp.java:266)
at com.ibm.db2.jcc.a.cr.V()V(cr.java:1412)
at com.ibm.db2.jcc.a.cr.d(IZ)V(cr.java:1939)
at com.ibm.db2.jcc.a.cr.R()I(cr.java:440)
at com.ibm.db2.jcc.a.cr.executeUpdate()I(cr.java:423)
at weblogic.jdbc.wrapper.PreparedStatement.executeUpdate()I(PreparedStatement.java:147)
at org.hibernate.persister.entity.BasicEntityPersister.insert(Ljava.io.Serializable;[Ljava.lang.Object;[ZILjava.lang.String;Ljava.la
ng.Object;Lorg.hibernate.engine.SessionImplementor;)V(BasicEntityPersister.java:1856)
at org.hibernate.persister.entity.BasicEntityPersister.insert(Ljava.io.Serializable;[Ljava.lang.Object;Ljava.lang.Object;Lorg.hibern
ate.engine.SessionImplementor;)V(BasicEntityPersister.java:2200)
at org.hibernate.action.EntityInsertAction.execute()V(EntityInsertAction.java:46)
at org.hibernate.engine.ActionQueue.execute(Lorg.hibernate.action.Executable;)V(ActionQueue.java:239)
at org.hibernate.engine.ActionQueue.executeActions(Ljava.util.List;)V(ActionQueue.java:223)
at org.hibernate.engine.ActionQueue.executeActions()V(ActionQueue.java:136)
at org.hibernate.event.def.AbstractFlushingEventListener.performExecutions(Lorg.hibernate.engine.SessionImplementor;)V(AbstractFlush
ingEventListener.java:274)
at org.hibernate.event.def.DefaultFlushEventListener.onFlush(Lorg.hibernate.event.FlushEvent;)V(DefaultFlushEventListener.java:27)
at org.hibernate.impl.SessionImpl.flush()V(SessionImpl.java:730)
at org.springframework.orm.hibernate3.SessionFactoryUtils$SpringSessionSynchronization.beforeCommit(Z)V(SessionFactoryUtils.java:881
)
at org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.beforeCompletion()V(SessionFactoryUtils.java:100
5)
at weblogic.transaction.internal.ServerSCInfo.callBeforeCompletions(Lweblogic.transaction.internal.TransactionImpl;)V(ServerSCInfo.j
ava:1010)
at weblogic.transaction.internal.ServerSCInfo.startPrePrepareAndChain(Lweblogic.transaction.internal.ServerTransactionImpl;I)V(Serve
rSCInfo.java:115)
at weblogic.transaction.internal.ServerTransactionImpl.localPrePrepareAndChain()V(ServerTransactionImpl.java:1216)
at weblogic.transaction.internal.ServerTransactionImpl.globalPrePrepare()V(ServerTransactionImpl.java:1990)
at weblogic.transaction.internal.ServerTransactionImpl.internalCommit()V(ServerTransactionImpl.java:275)
at weblogic.transaction.internal.ServerTransactionImpl.commit()V(ServerTransactionImpl.java:246)
at weblogic.ejb20.internal.BaseEJBLocalObject.postInvoke(Lweblogic.ejb20.internal.InvocationWrapper;Ljava.lang.Throwable;)V(BaseEJBL
ocalObject.java:363)
...
...

I know the JTA 1.0.1 specification does not define the behaviour for exceptions thrown from before completion and this is why spring included the call to setRollbackOnly in addition to rethrowing the exception.  The issue is that calling setRollbackOnly yields undesirable exception masking within Weblogic 8.1 even if the exception is rethrown.  Since we cannot predict the behaviour of every JTA 1.0.1 container, I think this functionality should be configurable.  The problem is that SessionFactoryUtils instanciates the SpringJtaSynchronizationAdapter within a static method making the transaction synchronization behaviour not very customizable.

Thx.

-karl


---

**Affects:** 1.2.8, 2.0.2

**Backported to:** [1.2.9](https://github.com/spring-projects/spring-framework/milestone/39?closed=1)
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7835","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7835/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7835/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/7835/events","html_url":"https://github.com/spring-projects/spring-framework/issues/7835","id":398075389,"node_id":"MDU6SXNzdWUzOTgwNzUzODk=","number":7835,"title":"SpringJtaSynchronizationAdapter setRollbackOnly hides causing exception within Weblogic 8.1 [SPR-3149]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188511809,"node_id":"MDU6TGFiZWwxMTg4NTExODA5","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20data","name":"in: data","color":"e8f9de","default":false},{"id":1188511834,"node_id":"MDU6TGFiZWwxMTg4NTExODM0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20backported","name":"status: backported","color":"fef2c0","default":false},{"id":1188512010,"node_id":"MDU6TGFiZWwxMTg4NTEyMDEw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/type:%20enhancement","name":"type: enhancement","color":"e3d9fc","default":false}],"state":"closed","locked":false,"assignee":{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false},"assignees":[{"login":"jhoeller","id":1263688,"node_id":"MDQ6VXNlcjEyNjM2ODg=","avatar_url":"https://avatars1.githubusercontent.com/u/1263688?v=4","gravatar_id":"","url":"https://api.github.com/users/jhoeller","html_url":"https://github.com/jhoeller","followers_url":"https://api.github.com/users/jhoeller/followers","following_url":"https://api.github.com/users/jhoeller/following{/other_user}","gists_url":"https://api.github.com/users/jhoeller/gists{/gist_id}","starred_url":"https://api.github.com/users/jhoeller/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jhoeller/subscriptions","organizations_url":"https://api.github.com/users/jhoeller/orgs","repos_url":"https://api.github.com/users/jhoeller/repos","events_url":"https://api.github.com/users/jhoeller/events{/privacy}","received_events_url":"https://api.github.com/users/jhoeller/received_events","type":"User","site_admin":false}],"milestone":{"url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/40","html_url":"https://github.com/spring-projects/spring-framework/milestone/40","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/milestones/40/labels","id":3960811,"node_id":"MDk6TWlsZXN0b25lMzk2MDgxMQ==","number":40,"title":"2.0.3","description":null,"creator":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"open_issues":0,"closed_issues":140,"state":"closed","created_at":"2019-01-10T22:02:40Z","updated_at":"2019-01-11T00:55:00Z","due_on":null,"closed_at":"2019-01-10T22:02:40Z"},"comments":14,"created_at":"2007-02-11T23:53:03Z","updated_at":"2012-06-19T03:50:21Z","closed_at":"2012-06-19T03:50:21Z","author_association":"COLLABORATOR","body":"**[Karl Baum](https://jira.spring.io/secure/ViewProfile.jspa?name=kbaum)** opened **[SPR-3149](https://jira.spring.io/browse/SPR-3149?redirect=false)** and commented\n\nIn previous releases,  Spring was swallowing Hibernate exceptions within the TransactionSynchronization beforeCompletion method.  For more detail see the following link:\n\nhttp://opensource.atlassian.com/projects/spring/browse/SPR-2270\n\nAs the JIRA issue states, the issue was addressed by letting the runtime exception through, but still calling setRollbackOnly.\n\nThe issue is that with this fix within Weblogic 8.1, even though Spring lets the exception propogate up, Weblogic will always output a vague AppSetRollbackOnlyException cause whenever setRollbackOnly is invoked.  Hence, the underlying exception is still hidden from the developer.\n\nWe applied the Spring 2.X fix to our version of Spring 1.2.5.\n\npublic void beforeCompletion() {\ntry {\nboolean readOnly =\nTransactionSynchronizationManager.isCurrentTransactionReadOnly();\nthis.springSessionSynchronization.beforeCommit(readOnly);\n} catch (RuntimeException ex) {\nsetRollbackOnlyIfPossible();\nthrow ex;\n} catch (Error err) {\nsetRollbackOnlyIfPossible();\nthrow err;\n} finally {\n// Unbind the SessionHolder from the thread early, to avoid issues\n// with strict JTA implementations that issue warnings when doing JDBC\n// operations after transaction completion (e.g. Connection.getWarnings).\nthis.beforeCompletionCalled = true;\nthis.springSessionSynchronization.beforeCompletion();\n}\n}\n\nNotice the code above calls setRollbackOnly and rethrows the exception.  With this in place, however, we get the following vague exception from Weblogic:\n\n<Feb 8, 2007 11:51:08 AM EST> \\<Info> \\<EJB> \\<BEA-010051> <EJB Exception occurred during invocation from home: com.mycompany.MyRemoteEJB_inpik2_HomeImpl@bdfdb4 threw exception: javax.ejb.TransactionRolledbackLocalException: Error\ncommitting transaction:; nested exception is: weblogic.transaction.internal.AppSetRollbackOnlyException\njavax.ejb.TransactionRolledbackLocalException: Error committing transaction:; nested exception is: weblogic.transaction.internal.AppSetRollb\nackOnlyException\nweblogic.transaction.internal.AppSetRollbackOnlyException\nat weblogic.transaction.internal.TransactionImpl.setRollbackOnly()V(TransactionImpl.java:504)\nat weblogic.transaction.internal.TransactionManagerImpl.setRollbackOnly()V(TransactionManagerImpl.java:337)\nat weblogic.transaction.internal.TransactionManagerImpl.setRollbackOnly()V(TransactionManagerImpl.java:331)\nat org.springframework.transaction.jta.UserTransactionAdapter.setRollbackOnly()V(UserTransactionAdapter.java:86)\nat org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.setRollbackOnlyIfPossible()V(SessionFactoryUtils\n.java:1030)\nat org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.beforeCompletion()V(SessionFactoryUtils.java:100\n8)\nat weblogic.transaction.internal.ServerSCInfo.callBeforeCompletions(Lweblogic.transaction.internal.TransactionImpl;)V(ServerSCInfo.j\nava:1010)\nat weblogic.transaction.internal.ServerSCInfo.startPrePrepareAndChain(Lweblogic.transaction.internal.ServerTransactionImpl;I)V(Serve\nrSCInfo.java:115)\nat weblogic.transaction.internal.ServerTransactionImpl.localPrePrepareAndChain()V(ServerTransactionImpl.java:1216)\nat weblogic.transaction.internal.ServerTransactionImpl.globalPrePrepare()V(ServerTransactionImpl.java:1990)\nat weblogic.transaction.internal.ServerTransactionImpl.internalCommit()V(ServerTransactionImpl.java:275)\nat weblogic.transaction.internal.ServerTransactionImpl.commit()V(ServerTransactionImpl.java:246)\nat weblogic.ejb20.internal.BaseEJBLocalObject.postInvoke(Lweblogic.ejb20.internal.InvocationWrapper;Ljava.lang.Throwable;)V(BaseEJBL\nocalObject.java:363)\nat\n...\n...\n\nWe changed the previous patch to not call setRollbackOnly and allow the exception to propogate:\n\npublic void beforeCompletion() {\ntry {\nboolean readOnly =\nTransactionSynchronizationManager.isCurrentTransactionReadOnly();\nthis.springSessionSynchronization.beforeCommit(readOnly);\n} finally {\nthis.beforeCompletionCalled = true;\nthis.springSessionSynchronization.beforeCompletion();\n}\n}\n\nWe now get the underlying exception within our stack trace:\n\n<Feb 8, 2007 12:03:19 PM EST> \\<Info> \\<EJB> \\<BEA-010051> <EJB Exception occurred during invocation from home: om.mycompany.MyRemoteEJB_inpik2_HomeImpl@acc47a threw exception: javax.ejb.TransactionRolledbackLocalException: Error\ncommitting transaction:; nested exception is: org.springframework.jdbc.UncategorizedSQLException: Hibernate transaction synchronization; un\ncategorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001]; error code [-911]; DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exce\nption is com.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2\njavax.ejb.TransactionRolledbackLocalException: Error committing transaction:; nested exception is: org.springframework.jdbc.UncategorizedSQL\nException: Hibernate transaction synchronization; uncategorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001]; error code [-911]; DB2 SQL error: SQLC\nODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exception is com.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001,\nSQLERRMC: 2\norg.springframework.jdbc.UncategorizedSQLException: Hibernate transaction synchronization; uncategorized SQLException for SQL [insert into MYTABLE (COLUMN1, COLUMN2, COLUMN3, COLUMN4) values (?, ?, ?, ?)]; SQL state [40001\n]; error code [-911]; DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2; nested exception is com.ibm.db2.jcc.a.SqlException: DB2 SQ\nL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2\ncom.ibm.db2.jcc.a.SqlException: DB2 SQL error: SQLCODE: -911, SQLSTATE: 40001, SQLERRMC: 2\nat com.ibm.db2.jcc.a.cq.e(Lcom.ibm.db2.jcc.a.da;)I(cq.java:1482)\nat com.ibm.db2.jcc.c.bc.s(Lcom.ibm.db2.jcc.a.ct;)V(bc.java:721)\nat com.ibm.db2.jcc.c.bc.k(Lcom.ibm.db2.jcc.a.ct;)V(bc.java:375)\nat com.ibm.db2.jcc.c.bc.a(Lcom.ibm.db2.jcc.a.cg;)V(bc.java:63)\nat com.ibm.db2.jcc.c.q.a(Lcom.ibm.db2.jcc.a.cg;)V(q.java:64)\nat com.ibm.db2.jcc.c.bp.c()V(bp.java:266)\nat com.ibm.db2.jcc.a.cr.V()V(cr.java:1412)\nat com.ibm.db2.jcc.a.cr.d(IZ)V(cr.java:1939)\nat com.ibm.db2.jcc.a.cr.R()I(cr.java:440)\nat com.ibm.db2.jcc.a.cr.executeUpdate()I(cr.java:423)\nat weblogic.jdbc.wrapper.PreparedStatement.executeUpdate()I(PreparedStatement.java:147)\nat org.hibernate.persister.entity.BasicEntityPersister.insert(Ljava.io.Serializable;[Ljava.lang.Object;[ZILjava.lang.String;Ljava.la\nng.Object;Lorg.hibernate.engine.SessionImplementor;)V(BasicEntityPersister.java:1856)\nat org.hibernate.persister.entity.BasicEntityPersister.insert(Ljava.io.Serializable;[Ljava.lang.Object;Ljava.lang.Object;Lorg.hibern\nate.engine.SessionImplementor;)V(BasicEntityPersister.java:2200)\nat org.hibernate.action.EntityInsertAction.execute()V(EntityInsertAction.java:46)\nat org.hibernate.engine.ActionQueue.execute(Lorg.hibernate.action.Executable;)V(ActionQueue.java:239)\nat org.hibernate.engine.ActionQueue.executeActions(Ljava.util.List;)V(ActionQueue.java:223)\nat org.hibernate.engine.ActionQueue.executeActions()V(ActionQueue.java:136)\nat org.hibernate.event.def.AbstractFlushingEventListener.performExecutions(Lorg.hibernate.engine.SessionImplementor;)V(AbstractFlush\ningEventListener.java:274)\nat org.hibernate.event.def.DefaultFlushEventListener.onFlush(Lorg.hibernate.event.FlushEvent;)V(DefaultFlushEventListener.java:27)\nat org.hibernate.impl.SessionImpl.flush()V(SessionImpl.java:730)\nat org.springframework.orm.hibernate3.SessionFactoryUtils$SpringSessionSynchronization.beforeCommit(Z)V(SessionFactoryUtils.java:881\n)\nat org.springframework.orm.hibernate3.SessionFactoryUtils$JtaSessionSynchronization.beforeCompletion()V(SessionFactoryUtils.java:100\n5)\nat weblogic.transaction.internal.ServerSCInfo.callBeforeCompletions(Lweblogic.transaction.internal.TransactionImpl;)V(ServerSCInfo.j\nava:1010)\nat weblogic.transaction.internal.ServerSCInfo.startPrePrepareAndChain(Lweblogic.transaction.internal.ServerTransactionImpl;I)V(Serve\nrSCInfo.java:115)\nat weblogic.transaction.internal.ServerTransactionImpl.localPrePrepareAndChain()V(ServerTransactionImpl.java:1216)\nat weblogic.transaction.internal.ServerTransactionImpl.globalPrePrepare()V(ServerTransactionImpl.java:1990)\nat weblogic.transaction.internal.ServerTransactionImpl.internalCommit()V(ServerTransactionImpl.java:275)\nat weblogic.transaction.internal.ServerTransactionImpl.commit()V(ServerTransactionImpl.java:246)\nat weblogic.ejb20.internal.BaseEJBLocalObject.postInvoke(Lweblogic.ejb20.internal.InvocationWrapper;Ljava.lang.Throwable;)V(BaseEJBL\nocalObject.java:363)\n...\n...\n\nI know the JTA 1.0.1 specification does not define the behaviour for exceptions thrown from before completion and this is why spring included the call to setRollbackOnly in addition to rethrowing the exception.  The issue is that calling setRollbackOnly yields undesirable exception masking within Weblogic 8.1 even if the exception is rethrown.  Since we cannot predict the behaviour of every JTA 1.0.1 container, I think this functionality should be configurable.  The problem is that SessionFactoryUtils instanciates the SpringJtaSynchronizationAdapter within a static method making the transaction synchronization behaviour not very customizable.\n\nThx.\n\n-karl\n\n\n---\n\n**Affects:** 1.2.8, 2.0.2\n\n**Backported to:** [1.2.9](https://github.com/spring-projects/spring-framework/milestone/39?closed=1)\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453315211","453315213","453315214","453315216","453315217","453315218","453315219","453315220","453315221","453315222","453315224","453315225","453315226","453315228"], "labels":["in: data","status: backported","type: enhancement"]}