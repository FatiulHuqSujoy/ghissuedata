{"id":"16381", "title":"@Value annotation should be able to inject List<String> from YAML properties [SPR-11759]", "body":"**[Adam Berlin](https://jira.spring.io/secure/ViewProfile.jspa?name=aberlin@pivotallabs.com)** opened **[SPR-11759](https://jira.spring.io/browse/SPR-11759?redirect=false)** and commented

Yaml file
&mdash;

```
 
foobar:  
  ignoredUserIds:
    - 57016311
    - 22588218
```

Class
&mdash;

```
 
    
public class Foobar {
    @Value(\"${foobar.ignoredUserIds}\")
    List<String> ignoredUserIds;
}
 
```

Error
&mdash;

```
 
Caused by: org.springframework.beans.factory.BeanCreationException: Could not autowire field: java.util.List foobar.Foobar.ignoredUserIds; nested exception is java.lang.IllegalArgumentException: Could not resolve placeholder 'foobar.ignoredUserIds' in string value \"${foobar.ignoredUserIds}\"

```



---

**Reference URL:** https://github.com/spring-projects/spring-boot/issues/501

21 votes, 21 watchers
", "json":"{"url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16381","repository_url":"https://api.github.com/repos/spring-projects/spring-framework","labels_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16381/labels{/name}","comments_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16381/comments","events_url":"https://api.github.com/repos/spring-projects/spring-framework/issues/16381/events","html_url":"https://github.com/spring-projects/spring-framework/issues/16381","id":398168077,"node_id":"MDU6SXNzdWUzOTgxNjgwNzc=","number":16381,"title":"@Value annotation should be able to inject List<String> from YAML properties [SPR-11759]","user":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false},"labels":[{"id":1188512024,"node_id":"MDU6TGFiZWwxMTg4NTEyMDI0","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/has:%20votes-jira","name":"has: votes-jira","color":"dfdfdf","default":false},{"id":1188512000,"node_id":"MDU6TGFiZWwxMTg4NTEyMDAw","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/in:%20core","name":"in: core","color":"e8f9de","default":false},{"id":1189449303,"node_id":"MDU6TGFiZWwxMTg5NDQ5MzAz","url":"https://api.github.com/repos/spring-projects/spring-framework/labels/status:%20bulk-closed","name":"status: bulk-closed","color":"fef2c0","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":14,"created_at":"2014-05-05T10:05:44Z","updated_at":"2019-05-30T09:38:35Z","closed_at":"2019-01-12T00:19:47Z","author_association":"COLLABORATOR","body":"**[Adam Berlin](https://jira.spring.io/secure/ViewProfile.jspa?name=aberlin@pivotallabs.com)** opened **[SPR-11759](https://jira.spring.io/browse/SPR-11759?redirect=false)** and commented\n\nYaml file\n&mdash;\n\n```\n \nfoobar:  \n  ignoredUserIds:\n    - 57016311\n    - 22588218\n```\n\nClass\n&mdash;\n\n```\n \n    \npublic class Foobar {\n    @Value(\"${foobar.ignoredUserIds}\")\n    List<String> ignoredUserIds;\n}\n \n```\n\nError\n&mdash;\n\n```\n \nCaused by: org.springframework.beans.factory.BeanCreationException: Could not autowire field: java.util.List foobar.Foobar.ignoredUserIds; nested exception is java.lang.IllegalArgumentException: Could not resolve placeholder 'foobar.ignoredUserIds' in string value \"${foobar.ignoredUserIds}\"\n\n```\n\n\n\n---\n\n**Reference URL:** https://github.com/spring-projects/spring-boot/issues/501\n\n21 votes, 21 watchers\n","closed_by":{"login":"spring-issuemaster","id":16028288,"node_id":"MDQ6VXNlcjE2MDI4Mjg4","avatar_url":"https://avatars2.githubusercontent.com/u/16028288?v=4","gravatar_id":"","url":"https://api.github.com/users/spring-issuemaster","html_url":"https://github.com/spring-issuemaster","followers_url":"https://api.github.com/users/spring-issuemaster/followers","following_url":"https://api.github.com/users/spring-issuemaster/following{/other_user}","gists_url":"https://api.github.com/users/spring-issuemaster/gists{/gist_id}","starred_url":"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spring-issuemaster/subscriptions","organizations_url":"https://api.github.com/users/spring-issuemaster/orgs","repos_url":"https://api.github.com/users/spring-issuemaster/repos","events_url":"https://api.github.com/users/spring-issuemaster/events{/privacy}","received_events_url":"https://api.github.com/users/spring-issuemaster/received_events","type":"User","site_admin":false}}", "commentIds":["453412880","453412883","453412884","453412885","453412887","453412889","453412890","453412893","453412894","453412896","453412897","453412900","453699127","497270109"], "labels":["has: votes-jira","in: core","status: bulk-closed"]}