{"id":"453329175","body":"**[David Pedowitz](https://jira.spring.io/secure/ViewProfile.jspa?name\u003ddpedowitz)** commented\n\nAttempt to add properties available in PropertiesLoaderSupport PropertyResourceConfigurer to bean parser\n","date":"May 5, 2008, 6:56:01 AM","json":"{\"url\":\"https://api.github.com/repos/spring-projects/spring-framework/issues/comments/453329175\",\"html_url\":\"https://github.com/spring-projects/spring-framework/issues/9338#issuecomment-453329175\",\"issue_url\":\"https://api.github.com/repos/spring-projects/spring-framework/issues/9338\",\"id\":453329175,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDQ1MzMyOTE3NQ\u003d\u003d\",\"user\":{\"login\":\"spring-issuemaster\",\"id\":16028288,\"node_id\":\"MDQ6VXNlcjE2MDI4Mjg4\",\"avatar_url\":\"https://avatars2.githubusercontent.com/u/16028288?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/spring-issuemaster\",\"html_url\":\"https://github.com/spring-issuemaster\",\"followers_url\":\"https://api.github.com/users/spring-issuemaster/followers\",\"following_url\":\"https://api.github.com/users/spring-issuemaster/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/spring-issuemaster/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/spring-issuemaster/subscriptions\",\"organizations_url\":\"https://api.github.com/users/spring-issuemaster/orgs\",\"repos_url\":\"https://api.github.com/users/spring-issuemaster/repos\",\"events_url\":\"https://api.github.com/users/spring-issuemaster/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/spring-issuemaster/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2008-05-05T06:56:01Z\",\"updated_at\":\"2019-01-11T01:12:00Z\",\"author_association\":\"COLLABORATOR\",\"body\":\"**[David Pedowitz](https://jira.spring.io/secure/ViewProfile.jspa?name\u003ddpedowitz)** commented\\n\\nAttempt to add properties available in PropertiesLoaderSupport PropertyResourceConfigurer to bean parser\\n\"}"}