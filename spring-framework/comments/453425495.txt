{"id":"453425495","body":"**[cemo koc](https://jira.spring.io/secure/ViewProfile.jspa?name\u003dcemokoc)** commented\n\nI was considering resolving OptionalMethodArgumentResolver first than resolving generic parameter to resolve second part. In this second part you are free to implement your logic. If your resolver does not let you to return null values, as PathVariable, you are free to throw exception. OptionalMethodArgumentResolver does not restrict to return a value from a MethodArgumentResolver. It does only improve semantic and lets you warn other developers to not fall in pitfall by suggesting an optional can also be Empty.\n","date":"May 14, 2015, 1:45:05 PM","json":"{\"url\":\"https://api.github.com/repos/spring-projects/spring-framework/issues/comments/453425495\",\"html_url\":\"https://github.com/spring-projects/spring-framework/issues/17589#issuecomment-453425495\",\"issue_url\":\"https://api.github.com/repos/spring-projects/spring-framework/issues/17589\",\"id\":453425495,\"node_id\":\"MDEyOklzc3VlQ29tbWVudDQ1MzQyNTQ5NQ\u003d\u003d\",\"user\":{\"login\":\"spring-issuemaster\",\"id\":16028288,\"node_id\":\"MDQ6VXNlcjE2MDI4Mjg4\",\"avatar_url\":\"https://avatars2.githubusercontent.com/u/16028288?v\u003d4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/spring-issuemaster\",\"html_url\":\"https://github.com/spring-issuemaster\",\"followers_url\":\"https://api.github.com/users/spring-issuemaster/followers\",\"following_url\":\"https://api.github.com/users/spring-issuemaster/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/spring-issuemaster/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/spring-issuemaster/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/spring-issuemaster/subscriptions\",\"organizations_url\":\"https://api.github.com/users/spring-issuemaster/orgs\",\"repos_url\":\"https://api.github.com/users/spring-issuemaster/repos\",\"events_url\":\"https://api.github.com/users/spring-issuemaster/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/spring-issuemaster/received_events\",\"type\":\"User\",\"site_admin\":false},\"created_at\":\"2015-05-14T13:45:05Z\",\"updated_at\":\"2019-01-11T08:22:56Z\",\"author_association\":\"COLLABORATOR\",\"body\":\"**[cemo koc](https://jira.spring.io/secure/ViewProfile.jspa?name\u003dcemokoc)** commented\\n\\nI was considering resolving OptionalMethodArgumentResolver first than resolving generic parameter to resolve second part. In this second part you are free to implement your logic. If your resolver does not let you to return null values, as PathVariable, you are free to throw exception. OptionalMethodArgumentResolver does not restrict to return a value from a MethodArgumentResolver. It does only improve semantic and lets you warn other developers to not fall in pitfall by suggesting an optional can also be Empty.\\n\"}"}