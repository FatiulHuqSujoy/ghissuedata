{"id":"32336", "title":"Documents will randomly be returned in results", "body":"**Elasticsearch version**: 6.2.4

**Plugins installed**: []

**JVM version**: 1.8.0_172

**OS version**: MacOS (Darwin Kernel Version 15.6.0)

**Description of the problem including expected versus actual behavior**:
On my index with around 80 million documents, there seems to be about 100 documents that will randomly not show up in queries. The inconsistent documents are spread across the shards, which means that it is probably not related to https://github.com/elastic/elasticsearch/issues/753.

Ex:
```
POST my_index/_search?preference=_shards:1
{
  \"query\": {
    \"term\": {
      \"id\": \"006eb480-bf2c-4770-aa34-b2fccc418905\"
    }
  }
}
```
This will return the document about 75% of the time.

Index and Cluster settings:
* AWS Hosted Elasticsearch.
* 4 data nodes, no dedicated master nodes.
* refresh_interval=\"1s\"
* replicas=0
* shards=4
* Manual routing.
* Automatic document versioning.

I'm not sure what other information I need to give in order to track down this problem.", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/32336","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/32336/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/32336/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/32336/events","html_url":"https://github.com/elastic/elasticsearch/issues/32336","id":344119711,"node_id":"MDU6SXNzdWUzNDQxMTk3MTE=","number":32336,"title":"Documents will randomly be returned in results","user":{"login":"kylelyk","id":3401359,"node_id":"MDQ6VXNlcjM0MDEzNTk=","avatar_url":"https://avatars1.githubusercontent.com/u/3401359?v=4","gravatar_id":"","url":"https://api.github.com/users/kylelyk","html_url":"https://github.com/kylelyk","followers_url":"https://api.github.com/users/kylelyk/followers","following_url":"https://api.github.com/users/kylelyk/following{/other_user}","gists_url":"https://api.github.com/users/kylelyk/gists{/gist_id}","starred_url":"https://api.github.com/users/kylelyk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kylelyk/subscriptions","organizations_url":"https://api.github.com/users/kylelyk/orgs","repos_url":"https://api.github.com/users/kylelyk/repos","events_url":"https://api.github.com/users/kylelyk/events{/privacy}","received_events_url":"https://api.github.com/users/kylelyk/received_events","type":"User","site_admin":false},"labels":[{"id":836504707,"node_id":"MDU6TGFiZWw4MzY1MDQ3MDc=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Distributed/Distributed","name":":Distributed/Distributed","color":"0e8a16","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2018-07-24T16:48:39Z","updated_at":"2018-07-24T18:54:21Z","closed_at":"2018-07-24T18:54:20Z","author_association":"NONE","body":"**Elasticsearch version**: 6.2.4\r\n\r\n**Plugins installed**: []\r\n\r\n**JVM version**: 1.8.0_172\r\n\r\n**OS version**: MacOS (Darwin Kernel Version 15.6.0)\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nOn my index with around 80 million documents, there seems to be about 100 documents that will randomly not show up in queries. The inconsistent documents are spread across the shards, which means that it is probably not related to https://github.com/elastic/elasticsearch/issues/753.\r\n\r\nEx:\r\n```\r\nPOST my_index/_search?preference=_shards:1\r\n{\r\n  \"query\": {\r\n    \"term\": {\r\n      \"id\": \"006eb480-bf2c-4770-aa34-b2fccc418905\"\r\n    }\r\n  }\r\n}\r\n```\r\nThis will return the document about 75% of the time.\r\n\r\nIndex and Cluster settings:\r\n* AWS Hosted Elasticsearch.\r\n* 4 data nodes, no dedicated master nodes.\r\n* refresh_interval=\"1s\"\r\n* replicas=0\r\n* shards=4\r\n* Manual routing.\r\n* Automatic document versioning.\r\n\r\nI'm not sure what other information I need to give in order to track down this problem.","closed_by":{"login":"kylelyk","id":3401359,"node_id":"MDQ6VXNlcjM0MDEzNTk=","avatar_url":"https://avatars1.githubusercontent.com/u/3401359?v=4","gravatar_id":"","url":"https://api.github.com/users/kylelyk","html_url":"https://github.com/kylelyk","followers_url":"https://api.github.com/users/kylelyk/followers","following_url":"https://api.github.com/users/kylelyk/following{/other_user}","gists_url":"https://api.github.com/users/kylelyk/gists{/gist_id}","starred_url":"https://api.github.com/users/kylelyk/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/kylelyk/subscriptions","organizations_url":"https://api.github.com/users/kylelyk/orgs","repos_url":"https://api.github.com/users/kylelyk/repos","events_url":"https://api.github.com/users/kylelyk/events{/privacy}","received_events_url":"https://api.github.com/users/kylelyk/received_events","type":"User","site_admin":false}}", "commentIds":["407475825","407494315","407497506","407505031","407505747","407514584"], "labels":[":Distributed/Distributed"]}