{"id":"25871", "title":"Remove `--path.conf` from bin/elasticsearch help text", "body":"#25815 removed support for `--path.conf` from the command line, but the argument is still listed in the help text and consumed when run.

Extra credit: can we make the error message when using `--path.conf` match the error message of other unrecognized flags?

Current: `Found multiple arguments for option c/path.conf, but you asked for only one`
Ideal:  `path.conf is not a recognized option`

original issue text:

---
**Elasticsearch version**: master
**Plugins installed**: none
**JVM version**: `1.8.0_121-b13`
**OS version**: macOS
**Description of the problem including expected versus actual behavior**:

calling `bin/elasticsearch --path.conf=...` with anything causes an error to be logged:

```sh
elasticsearch-6.0.0-beta1-SNAPSHOT  $ ./bin/elasticsearch --path.conf=...
Option                Description
------                -----------
-E <KeyValuePair>     Configure a setting
-V, --version         Prints elasticsearch version information and exits
-c, --path.conf       Configure config path
-d, --daemonize       Starts Elasticsearch in the background
-h, --help            show help
-p, --pidfile <Path>  Creates a pid file in the specified path on start
-q, --quiet           Turns off standard ouput/error streams logging in console
-s, --silent          show minimal output
-v, --verbose         show verbose output
ERROR: Found multiple arguments for option c/path.conf, but you asked for only one
```
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/25871","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25871/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25871/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25871/events","html_url":"https://github.com/elastic/elasticsearch/issues/25871","id":245243265,"node_id":"MDU6SXNzdWUyNDUyNDMyNjU=","number":25871,"title":"Remove `--path.conf` from bin/elasticsearch help text","user":{"login":"spalger","id":1329312,"node_id":"MDQ6VXNlcjEzMjkzMTI=","avatar_url":"https://avatars1.githubusercontent.com/u/1329312?v=4","gravatar_id":"","url":"https://api.github.com/users/spalger","html_url":"https://github.com/spalger","followers_url":"https://api.github.com/users/spalger/followers","following_url":"https://api.github.com/users/spalger/following{/other_user}","gists_url":"https://api.github.com/users/spalger/gists{/gist_id}","starred_url":"https://api.github.com/users/spalger/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/spalger/subscriptions","organizations_url":"https://api.github.com/users/spalger/orgs","repos_url":"https://api.github.com/users/spalger/repos","events_url":"https://api.github.com/users/spalger/events{/privacy}","received_events_url":"https://api.github.com/users/spalger/received_events","type":"User","site_admin":false},"labels":[{"id":144797810,"node_id":"MDU6TGFiZWwxNDQ3OTc4MTA=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Infra/Core","name":":Core/Infra/Core","color":"0e8a16","default":false},{"id":654498781,"node_id":"MDU6TGFiZWw2NTQ0OTg3ODE=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/v6.0.0-beta1","name":"v6.0.0-beta1","color":"dddddd","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2017-07-24T23:45:52Z","updated_at":"2017-07-31T23:05:20Z","closed_at":"2017-07-28T08:35:08Z","author_association":"MEMBER","body":"#25815 removed support for `--path.conf` from the command line, but the argument is still listed in the help text and consumed when run.\r\n\r\nExtra credit: can we make the error message when using `--path.conf` match the error message of other unrecognized flags?\r\n\r\nCurrent: `Found multiple arguments for option c/path.conf, but you asked for only one`\r\nIdeal:  `path.conf is not a recognized option`\r\n\r\noriginal issue text:\r\n\r\n---\r\n**Elasticsearch version**: master\r\n**Plugins installed**: none\r\n**JVM version**: `1.8.0_121-b13`\r\n**OS version**: macOS\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\ncalling `bin/elasticsearch --path.conf=...` with anything causes an error to be logged:\r\n\r\n```sh\r\nelasticsearch-6.0.0-beta1-SNAPSHOT  $ ./bin/elasticsearch --path.conf=...\r\nOption                Description\r\n------                -----------\r\n-E <KeyValuePair>     Configure a setting\r\n-V, --version         Prints elasticsearch version information and exits\r\n-c, --path.conf       Configure config path\r\n-d, --daemonize       Starts Elasticsearch in the background\r\n-h, --help            show help\r\n-p, --pidfile <Path>  Creates a pid file in the specified path on start\r\n-q, --quiet           Turns off standard ouput/error streams logging in console\r\n-s, --silent          show minimal output\r\n-v, --verbose         show verbose output\r\nERROR: Found multiple arguments for option c/path.conf, but you asked for only one\r\n```\r\n","closed_by":{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false}}", "commentIds":["317617595","317795874","318368048","318595017","319100820","319102196","319112751","319150736","319216381","319218515","319219029","319220854","319221290"], "labels":[":Core/Infra/Core","v6.0.0-beta1"]}