{"id":"35440", "title":"[CI] Broken packaging tests on Centos6", "body":"https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+packaging-tests/77/console

Reproduces reliably
```
./gradlew :qa:vagrant:vagrantCentos6#batsPackagingTest
```

```
04:25:49 # Caused by: java.nio.file.NoSuchFileException: /usr/lib/os-release
04:25:49 # 	at sun.nio.fs.UnixException.translateToIOException(UnixException.java:86) ~[?:?]
04:25:49 # 	at sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:102) ~[?:?]
04:25:49 # 	at sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:107) ~[?:?]
04:25:49 # 	at sun.nio.fs.UnixFileSystemProvider.newByteChannel(UnixFileSystemProvider.java:214) ~[?:?]
```

Seems to be triggered by #35352 (730ec1d)
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/35440","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/35440/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/35440/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/35440/events","html_url":"https://github.com/elastic/elasticsearch/issues/35440","id":379615040,"node_id":"MDU6SXNzdWUzNzk2MTUwNDA=","number":35440,"title":"[CI] Broken packaging tests on Centos6","user":{"login":"tvernum","id":2244393,"node_id":"MDQ6VXNlcjIyNDQzOTM=","avatar_url":"https://avatars0.githubusercontent.com/u/2244393?v=4","gravatar_id":"","url":"https://api.github.com/users/tvernum","html_url":"https://github.com/tvernum","followers_url":"https://api.github.com/users/tvernum/followers","following_url":"https://api.github.com/users/tvernum/following{/other_user}","gists_url":"https://api.github.com/users/tvernum/gists{/gist_id}","starred_url":"https://api.github.com/users/tvernum/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tvernum/subscriptions","organizations_url":"https://api.github.com/users/tvernum/orgs","repos_url":"https://api.github.com/users/tvernum/repos","events_url":"https://api.github.com/users/tvernum/events{/privacy}","received_events_url":"https://api.github.com/users/tvernum/received_events","type":"User","site_admin":false},"labels":[{"id":114977275,"node_id":"MDU6TGFiZWwxMTQ5NzcyNzU=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Infra/Packaging","name":":Core/Infra/Packaging","color":"0e8a16","default":false},{"id":148612629,"node_id":"MDU6TGFiZWwxNDg2MTI2Mjk=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Etest-failure","name":">test-failure","color":"207de5","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-11-12T04:20:21Z","updated_at":"2018-11-13T00:31:12Z","closed_at":"2018-11-13T00:31:12Z","author_association":"CONTRIBUTOR","body":"https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+packaging-tests/77/console\r\n\r\nReproduces reliably\r\n```\r\n./gradlew :qa:vagrant:vagrantCentos6#batsPackagingTest\r\n```\r\n\r\n```\r\n04:25:49 # Caused by: java.nio.file.NoSuchFileException: /usr/lib/os-release\r\n04:25:49 # \tat sun.nio.fs.UnixException.translateToIOException(UnixException.java:86) ~[?:?]\r\n04:25:49 # \tat sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:102) ~[?:?]\r\n04:25:49 # \tat sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:107) ~[?:?]\r\n04:25:49 # \tat sun.nio.fs.UnixFileSystemProvider.newByteChannel(UnixFileSystemProvider.java:214) ~[?:?]\r\n```\r\n\r\nSeems to be triggered by #35352 (730ec1d)\r\n","closed_by":{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false}}", "commentIds":["437749526","437792367","437812460","437843743","437893876"], "labels":[":Core/Infra/Packaging",">test-failure"]}