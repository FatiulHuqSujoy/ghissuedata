{"id":"27343", "title":"`must not exist` doesn't work on the second level of nested query", "body":"<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`): 2.4.3

**Plugins installed**: [delete-by-query]

**JVM version** (`java -version`): 1.8.0_151-b12

**OS version** (`uname -a` if on a Unix-like system): Windows 10 x64

**Description of the problem including expected versus actual behavior**:
When I use `exists` query inside `must not` on a second level of `nested` query (nested inside nested) it doesn't match documents that has no sub-nested documents. If I remove `must_not`, it works fine.

**Steps to reproduce**:

 1. create index
```
PUT http://localhost:9200/sample
{
    \"mappings\": {
        \"root\": {
            \"properties\": {
                \"id\": {
                    \"type\": \"integer\"
                },
                \"secondLvl\": {
                    \"type\": \"nested\",
                    \"properties\": {
                        \"thirdLvl\": {
                            \"type\": \"nested\",
                            \"properties\": {
                                \"name\": {
                                    \"type\": \"string\"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
```
 2. insert entites
```
POST http://localhost:9200/sample/_bulk
{\"index\":{\"_type\":\"root\",\"_id\":\"1\"}}
{\"id\":1,\"secondLvl\":[{\"thirdLvl\":[{\"name\":\"test\"}]}]}
{\"index\":{\"_type\":\"root\",\"_id\":\"2\"}}
{\"id\":2,\"secondLvl\":[{}]}
```
 3. search
```
POST http://localhost:9200/sample/root/_search
{
    \"query\": {
        \"nested\": {
            \"query\": {
                \"nested\": {
                    \"query\": {
                        \"bool\": {
                            \"must_not\": [
                                {
                                    \"exists\": {
                                        \"field\": \"secondLvl.thirdLvl\"
                                    }
                                }
                            ]
                        }
                    },
                    \"path\": \"secondLvl.thirdLvl\"
                }
            },
            \"path\": \"secondLvl\"
        }
    }
}
```
Expected to match the second document. Actual - no results.
If I search without `must_not` query, it works fine, the first document matches. But the opposite query returns nothing instead of the second document.
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/27343","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27343/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27343/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27343/events","html_url":"https://github.com/elastic/elasticsearch/issues/27343","id":272936571,"node_id":"MDU6SXNzdWUyNzI5MzY1NzE=","number":27343,"title":"`must not exist` doesn't work on the second level of nested query","user":{"login":"llRandom","id":2345582,"node_id":"MDQ6VXNlcjIzNDU1ODI=","avatar_url":"https://avatars3.githubusercontent.com/u/2345582?v=4","gravatar_id":"","url":"https://api.github.com/users/llRandom","html_url":"https://github.com/llRandom","followers_url":"https://api.github.com/users/llRandom/followers","following_url":"https://api.github.com/users/llRandom/following{/other_user}","gists_url":"https://api.github.com/users/llRandom/gists{/gist_id}","starred_url":"https://api.github.com/users/llRandom/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/llRandom/subscriptions","organizations_url":"https://api.github.com/users/llRandom/orgs","repos_url":"https://api.github.com/users/llRandom/repos","events_url":"https://api.github.com/users/llRandom/events{/privacy}","received_events_url":"https://api.github.com/users/llRandom/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":7,"created_at":"2017-11-10T13:44:15Z","updated_at":"2017-11-13T10:00:34Z","closed_at":"2017-11-13T10:00:34Z","author_association":"NONE","body":"<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`): 2.4.3\r\n\r\n**Plugins installed**: [delete-by-query]\r\n\r\n**JVM version** (`java -version`): 1.8.0_151-b12\r\n\r\n**OS version** (`uname -a` if on a Unix-like system): Windows 10 x64\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nWhen I use `exists` query inside `must not` on a second level of `nested` query (nested inside nested) it doesn't match documents that has no sub-nested documents. If I remove `must_not`, it works fine.\r\n\r\n**Steps to reproduce**:\r\n\r\n 1. create index\r\n```\r\nPUT http://localhost:9200/sample\r\n{\r\n    \"mappings\": {\r\n        \"root\": {\r\n            \"properties\": {\r\n                \"id\": {\r\n                    \"type\": \"integer\"\r\n                },\r\n                \"secondLvl\": {\r\n                    \"type\": \"nested\",\r\n                    \"properties\": {\r\n                        \"thirdLvl\": {\r\n                            \"type\": \"nested\",\r\n                            \"properties\": {\r\n                                \"name\": {\r\n                                    \"type\": \"string\"\r\n                                }\r\n                            }\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n```\r\n 2. insert entites\r\n```\r\nPOST http://localhost:9200/sample/_bulk\r\n{\"index\":{\"_type\":\"root\",\"_id\":\"1\"}}\r\n{\"id\":1,\"secondLvl\":[{\"thirdLvl\":[{\"name\":\"test\"}]}]}\r\n{\"index\":{\"_type\":\"root\",\"_id\":\"2\"}}\r\n{\"id\":2,\"secondLvl\":[{}]}\r\n```\r\n 3. search\r\n```\r\nPOST http://localhost:9200/sample/root/_search\r\n{\r\n    \"query\": {\r\n        \"nested\": {\r\n            \"query\": {\r\n                \"nested\": {\r\n                    \"query\": {\r\n                        \"bool\": {\r\n                            \"must_not\": [\r\n                                {\r\n                                    \"exists\": {\r\n                                        \"field\": \"secondLvl.thirdLvl\"\r\n                                    }\r\n                                }\r\n                            ]\r\n                        }\r\n                    },\r\n                    \"path\": \"secondLvl.thirdLvl\"\r\n                }\r\n            },\r\n            \"path\": \"secondLvl\"\r\n        }\r\n    }\r\n}\r\n```\r\nExpected to match the second document. Actual - no results.\r\nIf I search without `must_not` query, it works fine, the first document matches. But the opposite query returns nothing instead of the second document.\r\n","closed_by":{"login":"llRandom","id":2345582,"node_id":"MDQ6VXNlcjIzNDU1ODI=","avatar_url":"https://avatars3.githubusercontent.com/u/2345582?v=4","gravatar_id":"","url":"https://api.github.com/users/llRandom","html_url":"https://github.com/llRandom","followers_url":"https://api.github.com/users/llRandom/followers","following_url":"https://api.github.com/users/llRandom/following{/other_user}","gists_url":"https://api.github.com/users/llRandom/gists{/gist_id}","starred_url":"https://api.github.com/users/llRandom/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/llRandom/subscriptions","organizations_url":"https://api.github.com/users/llRandom/orgs","repos_url":"https://api.github.com/users/llRandom/repos","events_url":"https://api.github.com/users/llRandom/events{/privacy}","received_events_url":"https://api.github.com/users/llRandom/received_events","type":"User","site_admin":false}}", "commentIds":["343506741","343509748","343511328","343523926","343594588","343853155","343869486"], "labels":[]}