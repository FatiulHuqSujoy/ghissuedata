{"id":"31967", "title":"CORS - elasticsearch not available", "body":"**Elasticsearch version:** docker.elastic.co/elasticsearch/elasticsearch:6.3.1

**Description of the problem including expected versus actual behavior**:
Adding CORS parameters to the elasticsearch.yml file stops elasticsearch from coming up without errors in log.

**Steps to reproduce**:
1.  Use the following docker-compose.yml:
```
version: '3.6'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.1
    container_name: elasticsearch
    environment:
      - cluster.name=schedule
    networks:
      - elastic_net
    ports:
      - 9200:9200
      - 9300:9300
    volumes:
      - esdata1:/usr/share/elasticsearch/data
      - /path/to/local/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
  kibana:
    image: docker.elastic.co/kibana/kibana:6.3.1
    container_name: kibana
    networks:
      - elastic_net
    ports:
      - 5601:5601
volumes:
  esdata1:
    driver: local

networks:
  elastic_net:
```
2.  The /path/to/local/elasticsearch.yml has all lines commented out except for the following:
```
http.cors.enabled: true
http.cors.allow-origin: /http?://localhost(:[0-9]+)?/
http.cors.allow-methods: OPTIONS, HEAD, GET, POST, PUT, DELETE
http.cors.allow-headers: X-Requested-With,X-Auth-Token,Content-Type, Content-Length
```
3.  Bring up docker instances
`docker-compose up`
 
In the logs you will see that Kibana can't connect to Elasticsearch:

`kibana           | {\"type\":\"log\",\"@timestamp\":\"2018-07-11T14:10:46Z\",\"tags\":[\"warning\",\"elasticsearch\",\"admin\"],\"pid\":1,\"message\":\"Unable to revive connection: http://elasticsearch:9200/\"}
kibana           | {\"type\":\"log\",\"@timestamp\":\"2018-07-11T14:10:46Z\",\"tags\":[\"warning\",\"elasticsearch\",\"admin\"],\"pid\":1,\"message\":\"No living connections\"}`

Also, trying to connect from a browser won't work:
http://localhost:9200/myindex/mytype/_search
returns ERR_CONNECTION_REFUSED

", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/31967","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31967/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31967/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31967/events","html_url":"https://github.com/elastic/elasticsearch/issues/31967","id":340268698,"node_id":"MDU6SXNzdWUzNDAyNjg2OTg=","number":31967,"title":"CORS - elasticsearch not available","user":{"login":"wardv","id":8428549,"node_id":"MDQ6VXNlcjg0Mjg1NDk=","avatar_url":"https://avatars3.githubusercontent.com/u/8428549?v=4","gravatar_id":"","url":"https://api.github.com/users/wardv","html_url":"https://github.com/wardv","followers_url":"https://api.github.com/users/wardv/followers","following_url":"https://api.github.com/users/wardv/following{/other_user}","gists_url":"https://api.github.com/users/wardv/gists{/gist_id}","starred_url":"https://api.github.com/users/wardv/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wardv/subscriptions","organizations_url":"https://api.github.com/users/wardv/orgs","repos_url":"https://api.github.com/users/wardv/repos","events_url":"https://api.github.com/users/wardv/events{/privacy}","received_events_url":"https://api.github.com/users/wardv/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-07-11T14:30:31Z","updated_at":"2018-07-11T22:06:17Z","closed_at":"2018-07-11T22:06:17Z","author_association":"NONE","body":"**Elasticsearch version:** docker.elastic.co/elasticsearch/elasticsearch:6.3.1\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nAdding CORS parameters to the elasticsearch.yml file stops elasticsearch from coming up without errors in log.\r\n\r\n**Steps to reproduce**:\r\n1.  Use the following docker-compose.yml:\r\n```\r\nversion: '3.6'\r\nservices:\r\n  elasticsearch:\r\n    image: docker.elastic.co/elasticsearch/elasticsearch:6.3.1\r\n    container_name: elasticsearch\r\n    environment:\r\n      - cluster.name=schedule\r\n    networks:\r\n      - elastic_net\r\n    ports:\r\n      - 9200:9200\r\n      - 9300:9300\r\n    volumes:\r\n      - esdata1:/usr/share/elasticsearch/data\r\n      - /path/to/local/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml\r\n  kibana:\r\n    image: docker.elastic.co/kibana/kibana:6.3.1\r\n    container_name: kibana\r\n    networks:\r\n      - elastic_net\r\n    ports:\r\n      - 5601:5601\r\nvolumes:\r\n  esdata1:\r\n    driver: local\r\n\r\nnetworks:\r\n  elastic_net:\r\n```\r\n2.  The /path/to/local/elasticsearch.yml has all lines commented out except for the following:\r\n```\r\nhttp.cors.enabled: true\r\nhttp.cors.allow-origin: /http?://localhost(:[0-9]+)?/\r\nhttp.cors.allow-methods: OPTIONS, HEAD, GET, POST, PUT, DELETE\r\nhttp.cors.allow-headers: X-Requested-With,X-Auth-Token,Content-Type, Content-Length\r\n```\r\n3.  Bring up docker instances\r\n`docker-compose up`\r\n \r\nIn the logs you will see that Kibana can't connect to Elasticsearch:\r\n\r\n`kibana           | {\"type\":\"log\",\"@timestamp\":\"2018-07-11T14:10:46Z\",\"tags\":[\"warning\",\"elasticsearch\",\"admin\"],\"pid\":1,\"message\":\"Unable to revive connection: http://elasticsearch:9200/\"}\r\nkibana           | {\"type\":\"log\",\"@timestamp\":\"2018-07-11T14:10:46Z\",\"tags\":[\"warning\",\"elasticsearch\",\"admin\"],\"pid\":1,\"message\":\"No living connections\"}`\r\n\r\nAlso, trying to connect from a browser won't work:\r\nhttp://localhost:9200/myindex/mytype/_search\r\nreturns ERR_CONNECTION_REFUSED\r\n\r\n","closed_by":{"login":"wardv","id":8428549,"node_id":"MDQ6VXNlcjg0Mjg1NDk=","avatar_url":"https://avatars3.githubusercontent.com/u/8428549?v=4","gravatar_id":"","url":"https://api.github.com/users/wardv","html_url":"https://github.com/wardv","followers_url":"https://api.github.com/users/wardv/followers","following_url":"https://api.github.com/users/wardv/following{/other_user}","gists_url":"https://api.github.com/users/wardv/gists{/gist_id}","starred_url":"https://api.github.com/users/wardv/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wardv/subscriptions","organizations_url":"https://api.github.com/users/wardv/orgs","repos_url":"https://api.github.com/users/wardv/repos","events_url":"https://api.github.com/users/wardv/events{/privacy}","received_events_url":"https://api.github.com/users/wardv/received_events","type":"User","site_admin":false}}", "commentIds":["404325671"], "labels":[]}