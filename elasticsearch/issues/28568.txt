{"id":"28568", "title":"[CI] TikaDocTests and AttachmentProcessorTests failing on oracle-java10-periodic", "body":"The following tests fail reproducibly for the java-10 builds on master and 6.x:

I can reproduce this locally using Oracle Corporation 10-ea [Java HotSpot(TM) 64-Bit Server VM 10-ea+42] on master with

```
REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\
  -Dtests.seed=1E6696FD299DB332 \\
  -Dtests.class=org.elasticsearch.ingest.attachment.TikaDocTests \\
  -Dtests.method=\"testFiles\" \\
  -Dtests.security.manager=true \\
  -Dtests.locale=so-ET \\
  -Dtests.timezone=Europe/Vatican

REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\
  -Dtests.seed=1E6696FD299DB332 \\
  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\
  -Dtests.method=\"testWordDocumentWithVisioSchema\" \\
  -Dtests.security.manager=true \\
  -Dtests.locale=fr-NE \\
  -Dtests.timezone=US/Central

REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\
  -Dtests.seed=1E6696FD299DB332 \\
  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\
  -Dtests.method=\"testVisioIsExcluded\" \\
  -Dtests.security.manager=true \\
  -Dtests.locale=fr-NE \\
  -Dtests.timezone=US/Central

REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\
  -Dtests.seed=1E6696FD299DB332 \\
  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\
  -Dtests.method=\"testWordDocument\" \\
  -Dtests.security.manager=true \\
  -Dtests.locale=fr-NE \\
  -Dtests.timezone=US/Central
```

The failures look like:
```
1> [2018-02-08T14:38:51,961][INFO ][o.e.i.a.TikaDocTests     ] [testFiles]: after test
  2> REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test -Dtests.seed=D3DABC16A6A59FDE -Dtests.class=org.elasticsearch.ingest.attachment.TikaDocTests -Dtests.method=\"testFiles\" -Dtests.security.manager=true -Dtests.locale=uk-UA -Dtests.timezone=Asia/Tehran
FAILURE 5.01s | TikaDocTests.testFiles <<< FAILURES!
   > Throwable #1: java.lang.AssertionError
   >    at __randomizedtesting.SeedInfo.seed([D3DABC16A6A59FDE:BAC7D990F4786C5C]:0)
   >    at org.elasticsearch.ingest.attachment.TikaDocTests.assertParseable(TikaDocTests.java:66)
   >    at org.elasticsearch.ingest.attachment.TikaDocTests.testFiles(TikaDocTests.java:56)
   >    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
   >    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
   >    at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
   >    at java.base/java.lang.reflect.Method.invoke(Method.java:564)
   >    at java.base/java.lang.Thread.run(Thread.java:844)
```

Which just seems the documents are not parsed at all. Also I see several warning in the beginning of the test run:
```
  2> SLF4J: Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".
  2> SLF4J: Defaulting to no-operation (NOP) logger implementation
  2> SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
  2> лют. 08, 2018 2:38:49 ПП org.apache.tika.config.InitializableProblemHandler$3 handleInitializableProblem
  2> WARNING: JBIG2ImageReader not loaded. jbig2 files will be ignored
  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io
  2> for optional dependencies.
  2> TIFFImageWriter not loaded. tiff files will not be processed
  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io
  2> for optional dependencies.
  2> J2KImageReader not loaded. JPEG2000 files will not be processed.
  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io
  2> for optional dependencies.
  2> лют. 08, 2018 2:38:49 ПП org.apache.tika.config.InitializableProblemHandler$3 handleInitializableProblem
  2> WARNING: org.xerial's sqlite-jdbc is not loaded.
  2> Please provide the jar on your classpath to parse sqlite files.
  2> See tika-parsers/pom.xml for the correct version.
```

Instances of recent failures:
https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+oracle-java10-periodic/69/console
https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+6.x+oracle-java10-periodic/68/console


", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/28568","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28568/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28568/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28568/events","html_url":"https://github.com/elastic/elasticsearch/issues/28568","id":295475087,"node_id":"MDU6SXNzdWUyOTU0NzUwODc=","number":28568,"title":"[CI] TikaDocTests and AttachmentProcessorTests failing on oracle-java10-periodic","user":{"login":"cbuescher","id":10398885,"node_id":"MDQ6VXNlcjEwMzk4ODg1","avatar_url":"https://avatars0.githubusercontent.com/u/10398885?v=4","gravatar_id":"","url":"https://api.github.com/users/cbuescher","html_url":"https://github.com/cbuescher","followers_url":"https://api.github.com/users/cbuescher/followers","following_url":"https://api.github.com/users/cbuescher/following{/other_user}","gists_url":"https://api.github.com/users/cbuescher/gists{/gist_id}","starred_url":"https://api.github.com/users/cbuescher/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cbuescher/subscriptions","organizations_url":"https://api.github.com/users/cbuescher/orgs","repos_url":"https://api.github.com/users/cbuescher/repos","events_url":"https://api.github.com/users/cbuescher/events{/privacy}","received_events_url":"https://api.github.com/users/cbuescher/received_events","type":"User","site_admin":false},"labels":[{"id":268963484,"node_id":"MDU6TGFiZWwyNjg5NjM0ODQ=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Features/Ingest","name":":Core/Features/Ingest","color":"0e8a16","default":false},{"id":148612629,"node_id":"MDU6TGFiZWwxNDg2MTI2Mjk=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Etest-failure","name":">test-failure","color":"207de5","default":false},{"id":782448523,"node_id":"MDU6TGFiZWw3ODI0NDg1MjM=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/jdk10","name":"jdk10","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2018-02-08T11:21:21Z","updated_at":"2018-02-13T20:44:41Z","closed_at":"2018-02-09T13:41:25Z","author_association":"MEMBER","body":"The following tests fail reproducibly for the java-10 builds on master and 6.x:\r\n\r\nI can reproduce this locally using Oracle Corporation 10-ea [Java HotSpot(TM) 64-Bit Server VM 10-ea+42] on master with\r\n\r\n```\r\nREPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\\r\n  -Dtests.seed=1E6696FD299DB332 \\\r\n  -Dtests.class=org.elasticsearch.ingest.attachment.TikaDocTests \\\r\n  -Dtests.method=\"testFiles\" \\\r\n  -Dtests.security.manager=true \\\r\n  -Dtests.locale=so-ET \\\r\n  -Dtests.timezone=Europe/Vatican\r\n\r\nREPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\\r\n  -Dtests.seed=1E6696FD299DB332 \\\r\n  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\\r\n  -Dtests.method=\"testWordDocumentWithVisioSchema\" \\\r\n  -Dtests.security.manager=true \\\r\n  -Dtests.locale=fr-NE \\\r\n  -Dtests.timezone=US/Central\r\n\r\nREPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\\r\n  -Dtests.seed=1E6696FD299DB332 \\\r\n  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\\r\n  -Dtests.method=\"testVisioIsExcluded\" \\\r\n  -Dtests.security.manager=true \\\r\n  -Dtests.locale=fr-NE \\\r\n  -Dtests.timezone=US/Central\r\n\r\nREPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test \\\r\n  -Dtests.seed=1E6696FD299DB332 \\\r\n  -Dtests.class=org.elasticsearch.ingest.attachment.AttachmentProcessorTests \\\r\n  -Dtests.method=\"testWordDocument\" \\\r\n  -Dtests.security.manager=true \\\r\n  -Dtests.locale=fr-NE \\\r\n  -Dtests.timezone=US/Central\r\n```\r\n\r\nThe failures look like:\r\n```\r\n1> [2018-02-08T14:38:51,961][INFO ][o.e.i.a.TikaDocTests     ] [testFiles]: after test\r\n  2> REPRODUCE WITH: ./gradlew :plugins:ingest-attachment:test -Dtests.seed=D3DABC16A6A59FDE -Dtests.class=org.elasticsearch.ingest.attachment.TikaDocTests -Dtests.method=\"testFiles\" -Dtests.security.manager=true -Dtests.locale=uk-UA -Dtests.timezone=Asia/Tehran\r\nFAILURE 5.01s | TikaDocTests.testFiles <<< FAILURES!\r\n   > Throwable #1: java.lang.AssertionError\r\n   >    at __randomizedtesting.SeedInfo.seed([D3DABC16A6A59FDE:BAC7D990F4786C5C]:0)\r\n   >    at org.elasticsearch.ingest.attachment.TikaDocTests.assertParseable(TikaDocTests.java:66)\r\n   >    at org.elasticsearch.ingest.attachment.TikaDocTests.testFiles(TikaDocTests.java:56)\r\n   >    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n   >    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n   >    at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n   >    at java.base/java.lang.reflect.Method.invoke(Method.java:564)\r\n   >    at java.base/java.lang.Thread.run(Thread.java:844)\r\n```\r\n\r\nWhich just seems the documents are not parsed at all. Also I see several warning in the beginning of the test run:\r\n```\r\n  2> SLF4J: Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".\r\n  2> SLF4J: Defaulting to no-operation (NOP) logger implementation\r\n  2> SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.\r\n  2> лют. 08, 2018 2:38:49 ПП org.apache.tika.config.InitializableProblemHandler$3 handleInitializableProblem\r\n  2> WARNING: JBIG2ImageReader not loaded. jbig2 files will be ignored\r\n  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io\r\n  2> for optional dependencies.\r\n  2> TIFFImageWriter not loaded. tiff files will not be processed\r\n  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io\r\n  2> for optional dependencies.\r\n  2> J2KImageReader not loaded. JPEG2000 files will not be processed.\r\n  2> See https://pdfbox.apache.org/2.0/dependencies.html#jai-image-io\r\n  2> for optional dependencies.\r\n  2> лют. 08, 2018 2:38:49 ПП org.apache.tika.config.InitializableProblemHandler$3 handleInitializableProblem\r\n  2> WARNING: org.xerial's sqlite-jdbc is not loaded.\r\n  2> Please provide the jar on your classpath to parse sqlite files.\r\n  2> See tika-parsers/pom.xml for the correct version.\r\n```\r\n\r\nInstances of recent failures:\r\nhttps://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+oracle-java10-periodic/69/console\r\nhttps://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+6.x+oracle-java10-periodic/68/console\r\n\r\n\r\n","closed_by":{"login":"cbuescher","id":10398885,"node_id":"MDQ6VXNlcjEwMzk4ODg1","avatar_url":"https://avatars0.githubusercontent.com/u/10398885?v=4","gravatar_id":"","url":"https://api.github.com/users/cbuescher","html_url":"https://github.com/cbuescher","followers_url":"https://api.github.com/users/cbuescher/followers","following_url":"https://api.github.com/users/cbuescher/following{/other_user}","gists_url":"https://api.github.com/users/cbuescher/gists{/gist_id}","starred_url":"https://api.github.com/users/cbuescher/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/cbuescher/subscriptions","organizations_url":"https://api.github.com/users/cbuescher/orgs","repos_url":"https://api.github.com/users/cbuescher/repos","events_url":"https://api.github.com/users/cbuescher/events{/privacy}","received_events_url":"https://api.github.com/users/cbuescher/received_events","type":"User","site_admin":false}}", "commentIds":["364119328","364330895"], "labels":[":Core/Features/Ingest",">test-failure","jdk10"]}