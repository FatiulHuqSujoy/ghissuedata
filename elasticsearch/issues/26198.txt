{"id":"26198", "title":"Dockerized Elasticsearch instance crashes when receiving request", "body":"<!--

** Please read the guidelines below. **

Issues that do not follow these guidelines are likely to be closed.

1.  GitHub is reserved for bug reports and feature requests. The best place to
    ask a general question is at the Elastic [forums](https://discuss.elastic.co).
    GitHub is not the place for general questions.

2.  Is this bug report or feature request for a supported OS? If not, it
    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os

3.  Please fill out EITHER the feature request block or the bug report block
    below, and delete the other block.

-->

<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`):

Version: 6.0.0-beta1, Build: 896afa4/2017-08-03T23:14:26.258Z, JVM: 1.8.0_144

**Plugins installed**: []

**JVM version** (`java -version`):

java version \"1.8.0_144\"
Java(TM) SE Runtime Environment (build 1.8.0_144-b01)
Java HotSpot(TM) 64-Bit Server VM (build 25.144-b01, mixed mode)

**OS version** (`uname -a` if on a Unix-like system):

Linux 816fd3d99829 4.4.0-91-generic #114-Ubuntu SMP Tue Aug 8 11:56:56 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

**Description of the problem including expected versus actual behavior**:

I'm running Elasticsearch within a docker container (Docker version 17.05.0-ce, build 89658be). Have used older versions up to Elasticsearch 5.5.1 with the same setup without any problems.

When testing to run Elasticsearch 6.0.0-beta1 I have faced an issue that I haven't seem before. The issue is that Elasticsearch crashes when receiving a request.

When starting up the Elasticsearch container, it starts up and Elasticsearch health status seems ok by looking in the logs as it states `Cluster health status changed from [RED] to [YELLOW]` and only a single Elasticsearch instance is running.

Running a simple API  towards the elasticsearch instance works fine:

```
user@host:/elasticsearch# curl localhost:9200/
{
  \"name\" : \"dvphuFi\",
  \"cluster_name\" : \"elasticsearch-logs\",
  \"cluster_uuid\" : \"k8QHY2SRTsyZn8sLo5vNRw\",
  \"version\" : {
    \"number\" : \"6.0.0-beta1\",
    \"build_hash\" : \"896afa4\",
    \"build_date\" : \"2017-08-03T23:14:26.258Z\",
    \"build_snapshot\" : false,
    \"lucene_version\" : \"7.0.0\",
    \"minimum_wire_compatibility_version\" : \"5.6.0\",
    \"minimum_index_compatibility_version\" : \"5.0.0\"
  },
  \"tagline\" : \"You Know, for Search\"
}
```

However, running any query will cause the Elasticsearch instance to crash:

```
user@host:/elasticsearch# curl localhost:9200/_search?q=test
curl: (52) Empty reply from server
```

The following steps was done in order to get the system working again after the error started happening:
1. Stop, remove and restart the docker image -> Error still occurred
2. Restarting the Docker deamon (via `service docker restart` -> Error still occurred
3. Restarting the Linux host -> Error still occurred
4. Deleting the Elasticsearch data dir -> System works ok again, however data needs to be re-indexed

More logs will be attached as a comment to this issue.

Below is an excerpt from the logs:

```
[2017-08-14T11:20:27,152][INFO ][o.e.n.Node               ] [] initializing ...
[2017-08-14T11:20:27,257][INFO ][o.e.e.NodeEnvironment    ] [dvphuFi] using [1] data paths, mounts [[/data (/dev/sda1)]], net usable_space [14.5gb], net total_space [31.3gb], types [ext4]
[2017-08-14T11:20:27,257][INFO ][o.e.e.NodeEnvironment    ] [dvphuFi] heap size [3.8gb], compressed ordinary object pointers [true]
[2017-08-14T11:20:27,591][INFO ][o.e.n.Node               ] node name [dvphuFi] derived from node ID [dvphuFiRT8aEJicgCiXprg]; set [node.name] to override
[2017-08-14T11:20:27,591][INFO ][o.e.n.Node               ] version[6.0.0-beta1], pid[14], build[896afa4/2017-08-03T23:14:26.258Z], OS[Linux/4.4.0-91-generic/amd64], JVM[Oracle Corporation/Java HotSpot(TM) 64-Bit Server VM/1.8.0_144/25.144-b01]
[2017-08-14T11:20:27,592][INFO ][o.e.n.Node               ] JVM arguments [-Xms4g, -Xmx4g, -Dlog4j2.disable.jmx=true, -Des.path.home=/elasticsearch, -Des.path.conf=/elasticsearch/config]
[2017-08-14T11:20:27,592][WARN ][o.e.n.Node               ] version [6.0.0-beta1] is a pre-release version of Elasticsearch and is not suitable for production
[2017-08-14T11:20:28,200][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [aggs-matrix-stats]
[2017-08-14T11:20:28,200][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [analysis-common]
[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [ingest-common]
[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-expression]
[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-mustache]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-painless]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [parent-join]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [percolator]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [reindex]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [repository-url]
[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [transport-netty4]
[2017-08-14T11:20:28,203][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [tribe]
[2017-08-14T11:20:28,203][INFO ][o.e.p.PluginsService     ] [dvphuFi] no plugins loaded
[2017-08-14T11:20:29,526][INFO ][o.e.d.DiscoveryModule    ] [dvphuFi] using discovery type [zen]
[2017-08-14T11:20:30,304][INFO ][o.e.n.Node               ] initialized
[2017-08-14T11:20:30,305][INFO ][o.e.n.Node               ] [dvphuFi] starting ...
[2017-08-14T11:20:30,336][INFO ][i.n.u.i.PlatformDependent] Your platform does not provide complete low-level API for accessing direct buffers reliably. Unless explicitly requested, heap buffer will always be preferred to avoid potential system instability.
[2017-08-14T11:20:30,431][INFO ][o.e.t.TransportService   ] [dvphuFi] publish_address {172.18.0.10:9300}, bound_addresses {0.0.0.0:9300}
[2017-08-14T11:20:30,440][INFO ][o.e.b.BootstrapChecks    ] [dvphuFi] bound or publishing to a non-loopback or non-link-local address, enforcing bootstrap checks
[2017-08-14T11:20:33,486][INFO ][o.e.c.s.MasterService    ] [dvphuFi] zen-disco-elected-as-master ([0] nodes joined), reason: new_master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300}
[2017-08-14T11:20:33,492][INFO ][o.e.c.s.ClusterApplierService] [dvphuFi] new_master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300}, reason: apply cluster state (from master [master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300} committed version [1] source [zen-disco-elected-as-master ([0] nodes joined)]])
[2017-08-14T11:20:33,526][INFO ][o.e.h.n.Netty4HttpServerTransport] [dvphuFi] publish_address {172.18.0.10:9200}, bound_addresses {0.0.0.0:9200}
[2017-08-14T11:20:33,526][INFO ][o.e.n.Node               ] [dvphuFi] started
[2017-08-14T11:20:35,029][INFO ][o.e.g.GatewayService     ] [dvphuFi] recovered [125] indices into cluster_state
[2017-08-14T11:20:45,558][INFO ][o.e.c.r.a.AllocationService] [dvphuFi] Cluster health status changed from [RED] to [YELLOW] (reason: [shards started [[logstash-logs-2017.08.09][2], [logstash-logs-2017.08.09][3]] ...]).
[2017-08-14T11:31:18,825][ERROR][o.e.t.n.Netty4Utils      ] fatal error on the network layer
        at org.elasticsearch.transport.netty4.Netty4Utils.maybeDie(Netty4Utils.java:179)
        at org.elasticsearch.http.netty4.Netty4HttpRequestHandler.exceptionCaught(Netty4HttpRequestHandler.java:81)
        at io.netty.channel.AbstractChannelHandlerContext.invokeExceptionCaught(AbstractChannelHandlerContext.java:285)
        at io.netty.channel.AbstractChannelHandlerContext.notifyHandlerException(AbstractChannelHandlerContext.java:850)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:364)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at org.elasticsearch.http.netty4.pipelining.HttpPipeliningHandler.channelRead(HttpPipeliningHandler.java:63)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)
        at io.netty.handler.codec.MessageToMessageCodec.channelRead(MessageToMessageCodec.java:111)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:310)
        at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:284)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.channel.ChannelInboundHandlerAdapter.channelRead(ChannelInboundHandlerAdapter.java:86)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.channel.DefaultChannelPipeline$HeadContext.channelRead(DefaultChannelPipeline.java:1334)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:926)
        at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:134)
        at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:644)
        at io.netty.channel.nio.NioEventLoop.processSelectedKeysPlain(NioEventLoop.java:544)
        at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:498)
        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:458)
        at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:858)
        at java.lang.Thread.run(Thread.java:748)
[2017-08-14T11:31:18,839][WARN ][o.e.h.n.Netty4HttpServerTransport] [dvphuFi] caught exception while handling client http traffic, closing connection [id: 0x423a8b89, L:/127.0.0.1:9200 - R:/127.0.0.1:44298]
java.lang.StackOverflowError: null
        at com.carrotsearch.hppc.ObjectObjectHashMap.<init>(ObjectObjectHashMap.java:123) ~[hppc-0.7.1.jar:?]
...
...
...
```

Full log is attached below.

**Steps to reproduce**:

Please include a *minimal* but *complete* recreation of the problem, including
(e.g.) index creation, mappings, settings, query etc.  The easier you make for
us to reproduce it, the more likely that somebody will take the time to look at it.

 1. Start and run Elasticsearch within a Docker container
 2. Run a query against the Elasticsearch instance such as `/_search?q=test`
 3. Elasticsearch crashes with a stacktrace in the logs (see below)

**Provide logs (if relevant)**:

Logs will be added as a comment.
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/26198","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/26198/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/26198/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/26198/events","html_url":"https://github.com/elastic/elasticsearch/issues/26198","id":250010557,"node_id":"MDU6SXNzdWUyNTAwMTA1NTc=","number":26198,"title":"Dockerized Elasticsearch instance crashes when receiving request","user":{"login":"algestam","id":133108,"node_id":"MDQ6VXNlcjEzMzEwOA==","avatar_url":"https://avatars2.githubusercontent.com/u/133108?v=4","gravatar_id":"","url":"https://api.github.com/users/algestam","html_url":"https://github.com/algestam","followers_url":"https://api.github.com/users/algestam/followers","following_url":"https://api.github.com/users/algestam/following{/other_user}","gists_url":"https://api.github.com/users/algestam/gists{/gist_id}","starred_url":"https://api.github.com/users/algestam/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/algestam/subscriptions","organizations_url":"https://api.github.com/users/algestam/orgs","repos_url":"https://api.github.com/users/algestam/repos","events_url":"https://api.github.com/users/algestam/events{/privacy}","received_events_url":"https://api.github.com/users/algestam/received_events","type":"User","site_admin":false},"labels":[{"id":146832564,"node_id":"MDU6TGFiZWwxNDY4MzI1NjQ=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Search/Search","name":":Search/Search","color":"0e8a16","default":false},{"id":23173,"node_id":"MDU6TGFiZWwyMzE3Mw==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Ebug","name":">bug","color":"b60205","default":false},{"id":356863226,"node_id":"MDU6TGFiZWwzNTY4NjMyMjY=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/Pioneer%20Program","name":"Pioneer Program","color":"b60205","default":false}],"state":"closed","locked":false,"assignee":{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false},"assignees":[{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false}],"milestone":null,"comments":14,"created_at":"2017-08-14T12:01:39Z","updated_at":"2017-09-13T10:16:28Z","closed_at":"2017-09-13T10:16:28Z","author_association":"NONE","body":"<!--\r\n\r\n** Please read the guidelines below. **\r\n\r\nIssues that do not follow these guidelines are likely to be closed.\r\n\r\n1.  GitHub is reserved for bug reports and feature requests. The best place to\r\n    ask a general question is at the Elastic [forums](https://discuss.elastic.co).\r\n    GitHub is not the place for general questions.\r\n\r\n2.  Is this bug report or feature request for a supported OS? If not, it\r\n    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os\r\n\r\n3.  Please fill out EITHER the feature request block or the bug report block\r\n    below, and delete the other block.\r\n\r\n-->\r\n\r\n<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`):\r\n\r\nVersion: 6.0.0-beta1, Build: 896afa4/2017-08-03T23:14:26.258Z, JVM: 1.8.0_144\r\n\r\n**Plugins installed**: []\r\n\r\n**JVM version** (`java -version`):\r\n\r\njava version \"1.8.0_144\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_144-b01)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.144-b01, mixed mode)\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\n\r\nLinux 816fd3d99829 4.4.0-91-generic #114-Ubuntu SMP Tue Aug 8 11:56:56 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\nI'm running Elasticsearch within a docker container (Docker version 17.05.0-ce, build 89658be). Have used older versions up to Elasticsearch 5.5.1 with the same setup without any problems.\r\n\r\nWhen testing to run Elasticsearch 6.0.0-beta1 I have faced an issue that I haven't seem before. The issue is that Elasticsearch crashes when receiving a request.\r\n\r\nWhen starting up the Elasticsearch container, it starts up and Elasticsearch health status seems ok by looking in the logs as it states `Cluster health status changed from [RED] to [YELLOW]` and only a single Elasticsearch instance is running.\r\n\r\nRunning a simple API  towards the elasticsearch instance works fine:\r\n\r\n```\r\nuser@host:/elasticsearch# curl localhost:9200/\r\n{\r\n  \"name\" : \"dvphuFi\",\r\n  \"cluster_name\" : \"elasticsearch-logs\",\r\n  \"cluster_uuid\" : \"k8QHY2SRTsyZn8sLo5vNRw\",\r\n  \"version\" : {\r\n    \"number\" : \"6.0.0-beta1\",\r\n    \"build_hash\" : \"896afa4\",\r\n    \"build_date\" : \"2017-08-03T23:14:26.258Z\",\r\n    \"build_snapshot\" : false,\r\n    \"lucene_version\" : \"7.0.0\",\r\n    \"minimum_wire_compatibility_version\" : \"5.6.0\",\r\n    \"minimum_index_compatibility_version\" : \"5.0.0\"\r\n  },\r\n  \"tagline\" : \"You Know, for Search\"\r\n}\r\n```\r\n\r\nHowever, running any query will cause the Elasticsearch instance to crash:\r\n\r\n```\r\nuser@host:/elasticsearch# curl localhost:9200/_search?q=test\r\ncurl: (52) Empty reply from server\r\n```\r\n\r\nThe following steps was done in order to get the system working again after the error started happening:\r\n1. Stop, remove and restart the docker image -> Error still occurred\r\n2. Restarting the Docker deamon (via `service docker restart` -> Error still occurred\r\n3. Restarting the Linux host -> Error still occurred\r\n4. Deleting the Elasticsearch data dir -> System works ok again, however data needs to be re-indexed\r\n\r\nMore logs will be attached as a comment to this issue.\r\n\r\nBelow is an excerpt from the logs:\r\n\r\n```\r\n[2017-08-14T11:20:27,152][INFO ][o.e.n.Node               ] [] initializing ...\r\n[2017-08-14T11:20:27,257][INFO ][o.e.e.NodeEnvironment    ] [dvphuFi] using [1] data paths, mounts [[/data (/dev/sda1)]], net usable_space [14.5gb], net total_space [31.3gb], types [ext4]\r\n[2017-08-14T11:20:27,257][INFO ][o.e.e.NodeEnvironment    ] [dvphuFi] heap size [3.8gb], compressed ordinary object pointers [true]\r\n[2017-08-14T11:20:27,591][INFO ][o.e.n.Node               ] node name [dvphuFi] derived from node ID [dvphuFiRT8aEJicgCiXprg]; set [node.name] to override\r\n[2017-08-14T11:20:27,591][INFO ][o.e.n.Node               ] version[6.0.0-beta1], pid[14], build[896afa4/2017-08-03T23:14:26.258Z], OS[Linux/4.4.0-91-generic/amd64], JVM[Oracle Corporation/Java HotSpot(TM) 64-Bit Server VM/1.8.0_144/25.144-b01]\r\n[2017-08-14T11:20:27,592][INFO ][o.e.n.Node               ] JVM arguments [-Xms4g, -Xmx4g, -Dlog4j2.disable.jmx=true, -Des.path.home=/elasticsearch, -Des.path.conf=/elasticsearch/config]\r\n[2017-08-14T11:20:27,592][WARN ][o.e.n.Node               ] version [6.0.0-beta1] is a pre-release version of Elasticsearch and is not suitable for production\r\n[2017-08-14T11:20:28,200][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [aggs-matrix-stats]\r\n[2017-08-14T11:20:28,200][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [analysis-common]\r\n[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [ingest-common]\r\n[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-expression]\r\n[2017-08-14T11:20:28,201][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-mustache]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [lang-painless]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [parent-join]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [percolator]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [reindex]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [repository-url]\r\n[2017-08-14T11:20:28,202][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [transport-netty4]\r\n[2017-08-14T11:20:28,203][INFO ][o.e.p.PluginsService     ] [dvphuFi] loaded module [tribe]\r\n[2017-08-14T11:20:28,203][INFO ][o.e.p.PluginsService     ] [dvphuFi] no plugins loaded\r\n[2017-08-14T11:20:29,526][INFO ][o.e.d.DiscoveryModule    ] [dvphuFi] using discovery type [zen]\r\n[2017-08-14T11:20:30,304][INFO ][o.e.n.Node               ] initialized\r\n[2017-08-14T11:20:30,305][INFO ][o.e.n.Node               ] [dvphuFi] starting ...\r\n[2017-08-14T11:20:30,336][INFO ][i.n.u.i.PlatformDependent] Your platform does not provide complete low-level API for accessing direct buffers reliably. Unless explicitly requested, heap buffer will always be preferred to avoid potential system instability.\r\n[2017-08-14T11:20:30,431][INFO ][o.e.t.TransportService   ] [dvphuFi] publish_address {172.18.0.10:9300}, bound_addresses {0.0.0.0:9300}\r\n[2017-08-14T11:20:30,440][INFO ][o.e.b.BootstrapChecks    ] [dvphuFi] bound or publishing to a non-loopback or non-link-local address, enforcing bootstrap checks\r\n[2017-08-14T11:20:33,486][INFO ][o.e.c.s.MasterService    ] [dvphuFi] zen-disco-elected-as-master ([0] nodes joined), reason: new_master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300}\r\n[2017-08-14T11:20:33,492][INFO ][o.e.c.s.ClusterApplierService] [dvphuFi] new_master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300}, reason: apply cluster state (from master [master {dvphuFi}{dvphuFiRT8aEJicgCiXprg}{riLrClxxSlugm8Ks7PMClg}{172.18.0.10}{172.18.0.10:9300} committed version [1] source [zen-disco-elected-as-master ([0] nodes joined)]])\r\n[2017-08-14T11:20:33,526][INFO ][o.e.h.n.Netty4HttpServerTransport] [dvphuFi] publish_address {172.18.0.10:9200}, bound_addresses {0.0.0.0:9200}\r\n[2017-08-14T11:20:33,526][INFO ][o.e.n.Node               ] [dvphuFi] started\r\n[2017-08-14T11:20:35,029][INFO ][o.e.g.GatewayService     ] [dvphuFi] recovered [125] indices into cluster_state\r\n[2017-08-14T11:20:45,558][INFO ][o.e.c.r.a.AllocationService] [dvphuFi] Cluster health status changed from [RED] to [YELLOW] (reason: [shards started [[logstash-logs-2017.08.09][2], [logstash-logs-2017.08.09][3]] ...]).\r\n[2017-08-14T11:31:18,825][ERROR][o.e.t.n.Netty4Utils      ] fatal error on the network layer\r\n        at org.elasticsearch.transport.netty4.Netty4Utils.maybeDie(Netty4Utils.java:179)\r\n        at org.elasticsearch.http.netty4.Netty4HttpRequestHandler.exceptionCaught(Netty4HttpRequestHandler.java:81)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeExceptionCaught(AbstractChannelHandlerContext.java:285)\r\n        at io.netty.channel.AbstractChannelHandlerContext.notifyHandlerException(AbstractChannelHandlerContext.java:850)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:364)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at org.elasticsearch.http.netty4.pipelining.HttpPipeliningHandler.channelRead(HttpPipeliningHandler.java:63)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)\r\n        at io.netty.handler.codec.MessageToMessageCodec.channelRead(MessageToMessageCodec.java:111)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.handler.codec.MessageToMessageDecoder.channelRead(MessageToMessageDecoder.java:102)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:310)\r\n        at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:284)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.channel.ChannelInboundHandlerAdapter.channelRead(ChannelInboundHandlerAdapter.java:86)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)\r\n        at io.netty.channel.DefaultChannelPipeline$HeadContext.channelRead(DefaultChannelPipeline.java:1334)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)\r\n        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)\r\n        at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:926)\r\n        at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:134)\r\n        at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:644)\r\n        at io.netty.channel.nio.NioEventLoop.processSelectedKeysPlain(NioEventLoop.java:544)\r\n        at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:498)\r\n        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:458)\r\n        at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:858)\r\n        at java.lang.Thread.run(Thread.java:748)\r\n[2017-08-14T11:31:18,839][WARN ][o.e.h.n.Netty4HttpServerTransport] [dvphuFi] caught exception while handling client http traffic, closing connection [id: 0x423a8b89, L:/127.0.0.1:9200 - R:/127.0.0.1:44298]\r\njava.lang.StackOverflowError: null\r\n        at com.carrotsearch.hppc.ObjectObjectHashMap.<init>(ObjectObjectHashMap.java:123) ~[hppc-0.7.1.jar:?]\r\n...\r\n...\r\n...\r\n```\r\n\r\nFull log is attached below.\r\n\r\n**Steps to reproduce**:\r\n\r\nPlease include a *minimal* but *complete* recreation of the problem, including\r\n(e.g.) index creation, mappings, settings, query etc.  The easier you make for\r\nus to reproduce it, the more likely that somebody will take the time to look at it.\r\n\r\n 1. Start and run Elasticsearch within a Docker container\r\n 2. Run a query against the Elasticsearch instance such as `/_search?q=test`\r\n 3. Elasticsearch crashes with a stacktrace in the logs (see below)\r\n\r\n**Provide logs (if relevant)**:\r\n\r\nLogs will be added as a comment.\r\n","closed_by":{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false}}", "commentIds":["322173236","322175703","322177666","322178274","322181250","322190256","322191258","322212257","322215311","322215899","322406261","322520651","326828674","326830139"], "labels":[":Search/Search",">bug","Pioneer Program"]}