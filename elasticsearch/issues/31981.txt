{"id":"31981", "title":"Search with \"cowan\" results in no matches with default_operator AND but if using AND between terms returns match", "body":"**Elasticsearch version** (`bin/elasticsearch --version`):

Docker Elastic `docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4`

**OS version** (`uname -a` if on a Unix-like system):

Microsoft Windows [Version 10.0.17134.165]

**Description of the problem including expected versus actual behavior**:

After adding a document, searching for \"cowan nsw seaplane\" produces no results. However searching for \"cowan AND nsw AND seaplane\" produces the expected result. Playing around with the terms shows that the text cowan appears to be causing issues? Is this a reserved word or some such? Steps to reproduce below.

**Steps to reproduce**:

 1.

```
docker run -p 9200:9200 -p 9300:9300 -e \"discovery.type=single-node\" -d docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4
```

 2.
```
POST http://localhost:9200/metadata/meta/
{
  \"meta\": {
    \"place\": [
      \"COWAN,NSW,AUSTRALIA\"
    ],
    \"description\": \"SEAPLANE CRASHES INTO HAWKESBURY RIVER AT JERUSALEM BAY, POLICE CONFIRM THE 6 PEOPLE ON BOARD DID NOT SURVIVE, 3 BODIES HAVE BEEN RECOVERED SO FAR\"
  }
}

RESPONSE
{
    \"_index\": \"metadata\",
    \"_type\": \"meta\",
    \"_id\": \"W31qjGQBqyAQ5SdCNt4J\",
    \"_version\": 1,
    \"result\": \"created\",
    \"_shards\": {
        \"total\": 2,
        \"successful\": 1,
        \"failed\": 0
    },
    \"_seq_no\": 0,
    \"_primary_term\": 1
}
```
 3.
```
POST http://localhost:9200/metadata/meta/_search/
{
  \"query\": {
    \"bool\": {
      \"must\": [
        {
          \"query_string\": {
            \"query\": \"seaplane hawkesbury cowan\",
            \"default_operator\": \"AND\",
            \"fields\": [
		    	\"meta.place\",
		        \"meta.description\"
            ]
          }
        }
      ]
    }
  }
}

RESPONSE
{
    \"took\": 5,
    \"timed_out\": false,
    \"_shards\": {
        \"total\": 5,
        \"successful\": 5,
        \"skipped\": 0,
        \"failed\": 0
    },
    \"hits\": {
        \"total\": 0,
        \"max_score\": null,
        \"hits\": []
    }
}
```
4. 
```
POST http://localhost:9200/metadata/meta/_search/
{
  \"query\": {
    \"bool\": {
      \"must\": [
        {
          \"query_string\": {
            \"query\": \"seaplane AND hawkesbury AND cowan\",
            \"default_operator\": \"AND\",
            \"fields\": [
		    	\"meta.place\",
		        \"meta.description\"
            ]
          }
        }
      ]
    }
  }
}

RESPONSE
{
    \"took\": 4,
    \"timed_out\": false,
    \"_shards\": {
        \"total\": 5,
        \"successful\": 5,
        \"skipped\": 0,
        \"failed\": 0
    },
    \"hits\": {
        \"total\": 1,
        \"max_score\": 0.8630463,
        \"hits\": [
            {
                \"_index\": \"metadata\",
                \"_type\": \"meta\",
                \"_id\": \"W31qjGQBqyAQ5SdCNt4J\",
                \"_score\": 0.8630463,
                \"_source\": {
                    \"meta\": {
                        \"place\": [
                            \"COWAN,NSW,AUSTRALIA\"
                        ],
                        \"description\": \"SEAPLANE CRASHES INTO HAWKESBURY RIVER AT JERUSALEM BAY, POLICE CONFIRM THE 6 PEOPLE ON BOARD DID NOT SURVIVE, 3 BODIES HAVE BEEN RECOVERED SO FAR\"
                    }
                }
            }
        ]
    }
}
```
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/31981","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31981/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31981/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31981/events","html_url":"https://github.com/elastic/elasticsearch/issues/31981","id":340472580,"node_id":"MDU6SXNzdWUzNDA0NzI1ODA=","number":31981,"title":"Search with \"cowan\" results in no matches with default_operator AND but if using AND between terms returns match","user":{"login":"boyter","id":612151,"node_id":"MDQ6VXNlcjYxMjE1MQ==","avatar_url":"https://avatars1.githubusercontent.com/u/612151?v=4","gravatar_id":"","url":"https://api.github.com/users/boyter","html_url":"https://github.com/boyter","followers_url":"https://api.github.com/users/boyter/followers","following_url":"https://api.github.com/users/boyter/following{/other_user}","gists_url":"https://api.github.com/users/boyter/gists{/gist_id}","starred_url":"https://api.github.com/users/boyter/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/boyter/subscriptions","organizations_url":"https://api.github.com/users/boyter/orgs","repos_url":"https://api.github.com/users/boyter/repos","events_url":"https://api.github.com/users/boyter/events{/privacy}","received_events_url":"https://api.github.com/users/boyter/received_events","type":"User","site_admin":false},"labels":[{"id":146832564,"node_id":"MDU6TGFiZWwxNDY4MzI1NjQ=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Search/Search","name":":Search/Search","color":"0e8a16","default":false},{"id":23173,"node_id":"MDU6TGFiZWwyMzE3Mw==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Ebug","name":">bug","color":"b60205","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-07-12T03:02:13Z","updated_at":"2018-07-13T02:07:21Z","closed_at":"2018-07-12T16:06:24Z","author_association":"NONE","body":"**Elasticsearch version** (`bin/elasticsearch --version`):\r\n\r\nDocker Elastic `docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4`\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\n\r\nMicrosoft Windows [Version 10.0.17134.165]\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\nAfter adding a document, searching for \"cowan nsw seaplane\" produces no results. However searching for \"cowan AND nsw AND seaplane\" produces the expected result. Playing around with the terms shows that the text cowan appears to be causing issues? Is this a reserved word or some such? Steps to reproduce below.\r\n\r\n**Steps to reproduce**:\r\n\r\n 1.\r\n\r\n```\r\ndocker run -p 9200:9200 -p 9300:9300 -e \"discovery.type=single-node\" -d docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4\r\n```\r\n\r\n 2.\r\n```\r\nPOST http://localhost:9200/metadata/meta/\r\n{\r\n  \"meta\": {\r\n    \"place\": [\r\n      \"COWAN,NSW,AUSTRALIA\"\r\n    ],\r\n    \"description\": \"SEAPLANE CRASHES INTO HAWKESBURY RIVER AT JERUSALEM BAY, POLICE CONFIRM THE 6 PEOPLE ON BOARD DID NOT SURVIVE, 3 BODIES HAVE BEEN RECOVERED SO FAR\"\r\n  }\r\n}\r\n\r\nRESPONSE\r\n{\r\n    \"_index\": \"metadata\",\r\n    \"_type\": \"meta\",\r\n    \"_id\": \"W31qjGQBqyAQ5SdCNt4J\",\r\n    \"_version\": 1,\r\n    \"result\": \"created\",\r\n    \"_shards\": {\r\n        \"total\": 2,\r\n        \"successful\": 1,\r\n        \"failed\": 0\r\n    },\r\n    \"_seq_no\": 0,\r\n    \"_primary_term\": 1\r\n}\r\n```\r\n 3.\r\n```\r\nPOST http://localhost:9200/metadata/meta/_search/\r\n{\r\n  \"query\": {\r\n    \"bool\": {\r\n      \"must\": [\r\n        {\r\n          \"query_string\": {\r\n            \"query\": \"seaplane hawkesbury cowan\",\r\n            \"default_operator\": \"AND\",\r\n            \"fields\": [\r\n\t\t    \t\"meta.place\",\r\n\t\t        \"meta.description\"\r\n            ]\r\n          }\r\n        }\r\n      ]\r\n    }\r\n  }\r\n}\r\n\r\nRESPONSE\r\n{\r\n    \"took\": 5,\r\n    \"timed_out\": false,\r\n    \"_shards\": {\r\n        \"total\": 5,\r\n        \"successful\": 5,\r\n        \"skipped\": 0,\r\n        \"failed\": 0\r\n    },\r\n    \"hits\": {\r\n        \"total\": 0,\r\n        \"max_score\": null,\r\n        \"hits\": []\r\n    }\r\n}\r\n```\r\n4. \r\n```\r\nPOST http://localhost:9200/metadata/meta/_search/\r\n{\r\n  \"query\": {\r\n    \"bool\": {\r\n      \"must\": [\r\n        {\r\n          \"query_string\": {\r\n            \"query\": \"seaplane AND hawkesbury AND cowan\",\r\n            \"default_operator\": \"AND\",\r\n            \"fields\": [\r\n\t\t    \t\"meta.place\",\r\n\t\t        \"meta.description\"\r\n            ]\r\n          }\r\n        }\r\n      ]\r\n    }\r\n  }\r\n}\r\n\r\nRESPONSE\r\n{\r\n    \"took\": 4,\r\n    \"timed_out\": false,\r\n    \"_shards\": {\r\n        \"total\": 5,\r\n        \"successful\": 5,\r\n        \"skipped\": 0,\r\n        \"failed\": 0\r\n    },\r\n    \"hits\": {\r\n        \"total\": 1,\r\n        \"max_score\": 0.8630463,\r\n        \"hits\": [\r\n            {\r\n                \"_index\": \"metadata\",\r\n                \"_type\": \"meta\",\r\n                \"_id\": \"W31qjGQBqyAQ5SdCNt4J\",\r\n                \"_score\": 0.8630463,\r\n                \"_source\": {\r\n                    \"meta\": {\r\n                        \"place\": [\r\n                            \"COWAN,NSW,AUSTRALIA\"\r\n                        ],\r\n                        \"description\": \"SEAPLANE CRASHES INTO HAWKESBURY RIVER AT JERUSALEM BAY, POLICE CONFIRM THE 6 PEOPLE ON BOARD DID NOT SURVIVE, 3 BODIES HAVE BEEN RECOVERED SO FAR\"\r\n                    }\r\n                }\r\n            }\r\n        ]\r\n    }\r\n}\r\n```\r\n","closed_by":{"login":"jimczi","id":15977469,"node_id":"MDQ6VXNlcjE1OTc3NDY5","avatar_url":"https://avatars0.githubusercontent.com/u/15977469?v=4","gravatar_id":"","url":"https://api.github.com/users/jimczi","html_url":"https://github.com/jimczi","followers_url":"https://api.github.com/users/jimczi/followers","following_url":"https://api.github.com/users/jimczi/following{/other_user}","gists_url":"https://api.github.com/users/jimczi/gists{/gist_id}","starred_url":"https://api.github.com/users/jimczi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimczi/subscriptions","organizations_url":"https://api.github.com/users/jimczi/orgs","repos_url":"https://api.github.com/users/jimczi/repos","events_url":"https://api.github.com/users/jimczi/events{/privacy}","received_events_url":"https://api.github.com/users/jimczi/received_events","type":"User","site_admin":false}}", "commentIds":["404548259","404564834","404677489"], "labels":[":Search/Search",">bug"]}