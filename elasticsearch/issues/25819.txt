{"id":"25819", "title":"Indexing a value of \"1.0\" to byte field with coerce=true fails on 5.5.0", "body":"<!-- Bug report -->

**Elasticsearch version**:
\"version\" : {
    \"number\" : \"5.5.0\",
    \"build_hash\" : \"260387d\",
    \"build_date\" : \"2017-06-30T23:16:05.735Z\",
    \"build_snapshot\" : false,
    \"lucene_version\" : \"6.6.0\"
  }

**Plugins installed**: none

**JVM version** (`java -version`):
java version \"1.8.0_131\"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)

**OS version** 
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=14.04
DISTRIB_CODENAME=trusty
DISTRIB_DESCRIPTION=\"Ubuntu 14.04.5 LTS\"

**Description of the problem including expected versus actual behavior**:

Attempting to index a value of \"1.0\" to byte field mapped with coerce=true fails with the following error: number_format_exception

**Steps to reproduce**:

curl -X PUT localhost:9200/testing -d '{ \"mappings\" : { \"coerceme\" : { \"properties\" : { \"test\" : { \"type\" : \"byte\", \"coerce\" : true}}}}}'

curl -X PUT localhost:9200/testing/coerceme/1 -d '{ \"test\" : \"1.0\" }'

Response:
{\"error\":{\"root_cause\":[{\"type\":\"mapper_parsing_exception\",\"reason\":\"failed to parse [test]\"}],\"type\":\"mapper_parsing_exception\",\"reason\":\"failed to parse [test]\",\"caused_by\":{\"type\":\"number_format_exception\",\"reason\":\"For input string: \\\"1.0\\\"\"}},\"status\":400}




", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/25819","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25819/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25819/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25819/events","html_url":"https://github.com/elastic/elasticsearch/issues/25819","id":244496865,"node_id":"MDU6SXNzdWUyNDQ0OTY4NjU=","number":25819,"title":"Indexing a value of \"1.0\" to byte field with coerce=true fails on 5.5.0","user":{"login":"PaulieMac","id":12311389,"node_id":"MDQ6VXNlcjEyMzExMzg5","avatar_url":"https://avatars1.githubusercontent.com/u/12311389?v=4","gravatar_id":"","url":"https://api.github.com/users/PaulieMac","html_url":"https://github.com/PaulieMac","followers_url":"https://api.github.com/users/PaulieMac/followers","following_url":"https://api.github.com/users/PaulieMac/following{/other_user}","gists_url":"https://api.github.com/users/PaulieMac/gists{/gist_id}","starred_url":"https://api.github.com/users/PaulieMac/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/PaulieMac/subscriptions","organizations_url":"https://api.github.com/users/PaulieMac/orgs","repos_url":"https://api.github.com/users/PaulieMac/repos","events_url":"https://api.github.com/users/PaulieMac/events{/privacy}","received_events_url":"https://api.github.com/users/PaulieMac/received_events","type":"User","site_admin":false},"labels":[{"id":141145460,"node_id":"MDU6TGFiZWwxNDExNDU0NjA=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Search/Mapping","name":":Search/Mapping","color":"0e8a16","default":false},{"id":23173,"node_id":"MDU6TGFiZWwyMzE3Mw==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Ebug","name":">bug","color":"b60205","default":false},{"id":110815527,"node_id":"MDU6TGFiZWwxMTA4MTU1Mjc=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/help%20wanted","name":"help wanted","color":"207de5","default":true}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2017-07-20T21:06:15Z","updated_at":"2017-07-26T06:21:43Z","closed_at":"2017-07-26T06:21:43Z","author_association":"NONE","body":"<!-- Bug report -->\r\n\r\n**Elasticsearch version**:\r\n\"version\" : {\r\n    \"number\" : \"5.5.0\",\r\n    \"build_hash\" : \"260387d\",\r\n    \"build_date\" : \"2017-06-30T23:16:05.735Z\",\r\n    \"build_snapshot\" : false,\r\n    \"lucene_version\" : \"6.6.0\"\r\n  }\r\n\r\n**Plugins installed**: none\r\n\r\n**JVM version** (`java -version`):\r\njava version \"1.8.0_131\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_131-b11)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)\r\n\r\n**OS version** \r\nDISTRIB_ID=Ubuntu\r\nDISTRIB_RELEASE=14.04\r\nDISTRIB_CODENAME=trusty\r\nDISTRIB_DESCRIPTION=\"Ubuntu 14.04.5 LTS\"\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\nAttempting to index a value of \"1.0\" to byte field mapped with coerce=true fails with the following error: number_format_exception\r\n\r\n**Steps to reproduce**:\r\n\r\ncurl -X PUT localhost:9200/testing -d '{ \"mappings\" : { \"coerceme\" : { \"properties\" : { \"test\" : { \"type\" : \"byte\", \"coerce\" : true}}}}}'\r\n\r\ncurl -X PUT localhost:9200/testing/coerceme/1 -d '{ \"test\" : \"1.0\" }'\r\n\r\nResponse:\r\n{\"error\":{\"root_cause\":[{\"type\":\"mapper_parsing_exception\",\"reason\":\"failed to parse [test]\"}],\"type\":\"mapper_parsing_exception\",\"reason\":\"failed to parse [test]\",\"caused_by\":{\"type\":\"number_format_exception\",\"reason\":\"For input string: \\\"1.0\\\"\"}},\"status\":400}\r\n\r\n\r\n\r\n\r\n","closed_by":{"login":"jpountz","id":299848,"node_id":"MDQ6VXNlcjI5OTg0OA==","avatar_url":"https://avatars2.githubusercontent.com/u/299848?v=4","gravatar_id":"","url":"https://api.github.com/users/jpountz","html_url":"https://github.com/jpountz","followers_url":"https://api.github.com/users/jpountz/followers","following_url":"https://api.github.com/users/jpountz/following{/other_user}","gists_url":"https://api.github.com/users/jpountz/gists{/gist_id}","starred_url":"https://api.github.com/users/jpountz/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jpountz/subscriptions","organizations_url":"https://api.github.com/users/jpountz/orgs","repos_url":"https://api.github.com/users/jpountz/repos","events_url":"https://api.github.com/users/jpountz/events{/privacy}","received_events_url":"https://api.github.com/users/jpountz/received_events","type":"User","site_admin":false}}", "commentIds":["316833939","316842355","316846605","316848935","316986665","317005583"], "labels":[":Search/Mapping",">bug","help wanted"]}