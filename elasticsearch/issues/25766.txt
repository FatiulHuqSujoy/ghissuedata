{"id":"25766", "title":"elasticsearch exclude._ip did not work", "body":"Kibana Version: 4.5.1
Elasticsearch Version: 2.3.3
OS: CentOS release 6.4 
Plugins installed: [marvel]
java version \"1.8.0_74\"

I was trying to decommission a node ip[x.x.x.x],node name is [Mountjoy]:
```
curl -XPUT 'localhost:9200/_cluster/settings?pretty' -H 'Content-Type: application/json' -d'
{
  \"transient\" : {
    \"cluster.routing.allocation.exclude._ip\" : \"x.x.x.x\"
  }
}
'
```
after several hours,the Marvel show node's shards didn't decrease and elasticsearch v2.3.3 did not have any explaination apis. then i try other exclude attributes below,still didn't work!
```
\"_host\" : \"x.x.x.x\",
\"_name\" : \"Mountjoy\",
\"_ip\" : \"x.x.x.x\"
```
then i try open `    \"cluster.routing.rebalance.enable\": \"none\" ` to `\"cluster.routing.rebalance.enable\": \"all\"`,seems didn't work,just rebalancing.



the node setting and the info:
```
curl -XGET 'http://localhost:9200/_cluster/settings?pretty'
{
  \"persistent\" : { },
  \"transient\" : {
    \"cluster\" : {
      \"routing\" : {
        \"rebalance\" : {
          \"enable\" : \"primaries\"
        },
        \"allocation\" : {
          \"cluster_concurrent_rebalance\" : \"12\",
          \"disk\" : {
            \"include_relocations\" : \"false\"
          },
          \"exclude\" : {
            \"_host\" : \"x.x.x.x\",
            \"_name\" : \"Mountjoy\",
            \"_ip\" : \"x.x.x.x\"
          },
          \"node_initial_primaries_recoveries\" : \"32\",
          \"balance\" : {
            \"index\" : \"1.00f\",
            \"threshold\" : \"1.00f\",
            \"shard\" : \"0.00f\"
          }
        }
      }
    }

curl -XGET 'http://localhost:9200/_nodes?pretty'|less
nodes\" : {
    \"VnvVx5eZTVqG3WMoJfgFgA\" : {
      \"name\" : \"Mountjoy\",
      \"transport_address\" : \"x.x.x.x:9300\",
      \"host\" : \"x.x.x.x\",
      \"ip\" : \"x.x.x.x\",

}

````", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/25766","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25766/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25766/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25766/events","html_url":"https://github.com/elastic/elasticsearch/issues/25766","id":243656467,"node_id":"MDU6SXNzdWUyNDM2NTY0Njc=","number":25766,"title":"elasticsearch exclude._ip did not work","user":{"login":"mars00772","id":987990,"node_id":"MDQ6VXNlcjk4Nzk5MA==","avatar_url":"https://avatars3.githubusercontent.com/u/987990?v=4","gravatar_id":"","url":"https://api.github.com/users/mars00772","html_url":"https://github.com/mars00772","followers_url":"https://api.github.com/users/mars00772/followers","following_url":"https://api.github.com/users/mars00772/following{/other_user}","gists_url":"https://api.github.com/users/mars00772/gists{/gist_id}","starred_url":"https://api.github.com/users/mars00772/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mars00772/subscriptions","organizations_url":"https://api.github.com/users/mars00772/orgs","repos_url":"https://api.github.com/users/mars00772/repos","events_url":"https://api.github.com/users/mars00772/events{/privacy}","received_events_url":"https://api.github.com/users/mars00772/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2017-07-18T09:36:40Z","updated_at":"2017-08-04T02:56:46Z","closed_at":"2017-07-18T10:44:19Z","author_association":"NONE","body":"Kibana Version: 4.5.1\r\nElasticsearch Version: 2.3.3\r\nOS: CentOS release 6.4 \r\nPlugins installed: [marvel]\r\njava version \"1.8.0_74\"\r\n\r\nI was trying to decommission a node ip[x.x.x.x],node name is [Mountjoy]:\r\n```\r\ncurl -XPUT 'localhost:9200/_cluster/settings?pretty' -H 'Content-Type: application/json' -d'\r\n{\r\n  \"transient\" : {\r\n    \"cluster.routing.allocation.exclude._ip\" : \"x.x.x.x\"\r\n  }\r\n}\r\n'\r\n```\r\nafter several hours,the Marvel show node's shards didn't decrease and elasticsearch v2.3.3 did not have any explaination apis. then i try other exclude attributes below,still didn't work!\r\n```\r\n\"_host\" : \"x.x.x.x\",\r\n\"_name\" : \"Mountjoy\",\r\n\"_ip\" : \"x.x.x.x\"\r\n```\r\nthen i try open `    \"cluster.routing.rebalance.enable\": \"none\" ` to `\"cluster.routing.rebalance.enable\": \"all\"`,seems didn't work,just rebalancing.\r\n\r\n\r\n\r\nthe node setting and the info:\r\n```\r\ncurl -XGET 'http://localhost:9200/_cluster/settings?pretty'\r\n{\r\n  \"persistent\" : { },\r\n  \"transient\" : {\r\n    \"cluster\" : {\r\n      \"routing\" : {\r\n        \"rebalance\" : {\r\n          \"enable\" : \"primaries\"\r\n        },\r\n        \"allocation\" : {\r\n          \"cluster_concurrent_rebalance\" : \"12\",\r\n          \"disk\" : {\r\n            \"include_relocations\" : \"false\"\r\n          },\r\n          \"exclude\" : {\r\n            \"_host\" : \"x.x.x.x\",\r\n            \"_name\" : \"Mountjoy\",\r\n            \"_ip\" : \"x.x.x.x\"\r\n          },\r\n          \"node_initial_primaries_recoveries\" : \"32\",\r\n          \"balance\" : {\r\n            \"index\" : \"1.00f\",\r\n            \"threshold\" : \"1.00f\",\r\n            \"shard\" : \"0.00f\"\r\n          }\r\n        }\r\n      }\r\n    }\r\n\r\ncurl -XGET 'http://localhost:9200/_nodes?pretty'|less\r\nnodes\" : {\r\n    \"VnvVx5eZTVqG3WMoJfgFgA\" : {\r\n      \"name\" : \"Mountjoy\",\r\n      \"transport_address\" : \"x.x.x.x:9300\",\r\n      \"host\" : \"x.x.x.x\",\r\n      \"ip\" : \"x.x.x.x\",\r\n\r\n}\r\n\r\n````","closed_by":{"login":"ywelsch","id":3718355,"node_id":"MDQ6VXNlcjM3MTgzNTU=","avatar_url":"https://avatars3.githubusercontent.com/u/3718355?v=4","gravatar_id":"","url":"https://api.github.com/users/ywelsch","html_url":"https://github.com/ywelsch","followers_url":"https://api.github.com/users/ywelsch/followers","following_url":"https://api.github.com/users/ywelsch/following{/other_user}","gists_url":"https://api.github.com/users/ywelsch/gists{/gist_id}","starred_url":"https://api.github.com/users/ywelsch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ywelsch/subscriptions","organizations_url":"https://api.github.com/users/ywelsch/orgs","repos_url":"https://api.github.com/users/ywelsch/repos","events_url":"https://api.github.com/users/ywelsch/events{/privacy}","received_events_url":"https://api.github.com/users/ywelsch/received_events","type":"User","site_admin":false}}", "commentIds":["316026609","320140284","320143261"], "labels":[]}