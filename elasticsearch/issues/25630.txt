{"id":"25630", "title":"Bug Span near query: \"Less than 2 subSpans.size():1\"", "body":"**Elasticsearch version**: 5.5.0 (docker.elastic.co/elasticsearch/elasticsearch:5.5.0)

**Description**:

The documentation for the [span near query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-span-near-query.html) states:

> The clauses element is a list of **one or more** other span type queries 

(emphasis mine)

Just supplying *one* clause though, causes an error to be returned ~*iff the HTTP request method is `POST` instead of `GET`*~. E.g.

```
...
    \"failures\": [
      {
        \"shard\": 0,
        \"index\": \"test\",
        \"node\": \"...\",
        \"reason\": {
          \"type\": \"illegal_argument_exception\",
          \"reason\": \"Less than 2 subSpans.size():1\"
        }
      }
    ]
```
---


**Steps to reproduce**:

With ES 5.5.0 running:

 1. Create a mapping:

```
curl -X \"PUT\" \"http://localhost:9200/test\" \\
     -H \"Content-Type: text/plain; charset=utf-8\" \\
     -u elastic:changeme \\
     -d $'{
  \"settings\": {
    \"analysis\": {
      \"analyzer\": {
        \"my_analyzer\": {
          \"tokenizer\": \"my_pattern_tokenizer\"
        }
      },
      \"tokenizer\": {
        \"my_pattern_tokenizer\": {
          \"type\": \"pattern\",
          \"pattern\": \"\\\\\\\\/\"
        }
      }
    }
  },
  \"mappings\": {
    \"bar\": {
      \"properties\": {
        \"path\": {
          \"type\": \"text\",
          \"analyzer\": \"my_analyzer\"
        }
      }
    }
  }
}'
```

 2. Add an example document:

```
curl -X \"POST\" \"http://localhost:9200/test/bar\" \\
     -H \"Content-Type: application/json\" \\
     -u elastic:changeme \\
     -d $'{
  \"path\": \"foo/bar/baz\"
}'
```

 3. Run a search using a span near query with a single clause:

```
curl -X \"POST\" \"http://localhost:9200/test/bar/_search\" \\
     -H \"Content-Type: application/json\" \\
     -u elastic:changeme \\
     -d $'{
  \"query\": {
    \"span_near\": {
      \"clauses\": [
        {
          \"span_first\": {
            \"match\": {
              \"span_term\": {
                \"path\": \"foo\"
              }
            },
            \"end\": 1
          }
        }
      ],
      \"slop\": 2,
      \"in_order\": true
    }
  }
}'

```

~If using an HTTP **`POST`**~ this fails with:

```
...
    \"failures\": [
      {
        \"shard\": 1,
        \"index\": \"test\",
        \"node\": \"24CCgJNwRbCoxUlUbFXiuw\",
        \"reason\": {
          \"type\": \"illegal_argument_exception\",
          \"reason\": \"Less than 2 subSpans.size():1\"
        }
      }
    ]
...
```

~The same request using a **`GET`** request method succeeds.~

~According to https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html it should be possible to use either `POST` or `GET`.~

`GET` vs. `POST` was just a red herring. This fails in either case.

", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/25630","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25630/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25630/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/25630/events","html_url":"https://github.com/elastic/elasticsearch/issues/25630","id":241695795,"node_id":"MDU6SXNzdWUyNDE2OTU3OTU=","number":25630,"title":"Bug Span near query: \"Less than 2 subSpans.size():1\"","user":{"login":"juretta","id":746,"node_id":"MDQ6VXNlcjc0Ng==","avatar_url":"https://avatars0.githubusercontent.com/u/746?v=4","gravatar_id":"","url":"https://api.github.com/users/juretta","html_url":"https://github.com/juretta","followers_url":"https://api.github.com/users/juretta/followers","following_url":"https://api.github.com/users/juretta/following{/other_user}","gists_url":"https://api.github.com/users/juretta/gists{/gist_id}","starred_url":"https://api.github.com/users/juretta/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/juretta/subscriptions","organizations_url":"https://api.github.com/users/juretta/orgs","repos_url":"https://api.github.com/users/juretta/repos","events_url":"https://api.github.com/users/juretta/events{/privacy}","received_events_url":"https://api.github.com/users/juretta/received_events","type":"User","site_admin":false},"labels":[{"id":146832564,"node_id":"MDU6TGFiZWwxNDY4MzI1NjQ=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Search/Search","name":":Search/Search","color":"0e8a16","default":false},{"id":23174,"node_id":"MDU6TGFiZWwyMzE3NA==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Eenhancement","name":">enhancement","color":"4a4ea8","default":false}],"state":"closed","locked":false,"assignee":{"login":"jimczi","id":15977469,"node_id":"MDQ6VXNlcjE1OTc3NDY5","avatar_url":"https://avatars0.githubusercontent.com/u/15977469?v=4","gravatar_id":"","url":"https://api.github.com/users/jimczi","html_url":"https://github.com/jimczi","followers_url":"https://api.github.com/users/jimczi/followers","following_url":"https://api.github.com/users/jimczi/following{/other_user}","gists_url":"https://api.github.com/users/jimczi/gists{/gist_id}","starred_url":"https://api.github.com/users/jimczi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimczi/subscriptions","organizations_url":"https://api.github.com/users/jimczi/orgs","repos_url":"https://api.github.com/users/jimczi/repos","events_url":"https://api.github.com/users/jimczi/events{/privacy}","received_events_url":"https://api.github.com/users/jimczi/received_events","type":"User","site_admin":false},"assignees":[{"login":"jimczi","id":15977469,"node_id":"MDQ6VXNlcjE1OTc3NDY5","avatar_url":"https://avatars0.githubusercontent.com/u/15977469?v=4","gravatar_id":"","url":"https://api.github.com/users/jimczi","html_url":"https://github.com/jimczi","followers_url":"https://api.github.com/users/jimczi/followers","following_url":"https://api.github.com/users/jimczi/following{/other_user}","gists_url":"https://api.github.com/users/jimczi/gists{/gist_id}","starred_url":"https://api.github.com/users/jimczi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimczi/subscriptions","organizations_url":"https://api.github.com/users/jimczi/orgs","repos_url":"https://api.github.com/users/jimczi/repos","events_url":"https://api.github.com/users/jimczi/events{/privacy}","received_events_url":"https://api.github.com/users/jimczi/received_events","type":"User","site_admin":false}],"milestone":null,"comments":3,"created_at":"2017-07-10T12:29:52Z","updated_at":"2018-02-14T13:28:52Z","closed_at":"2017-07-24T11:24:29Z","author_association":"NONE","body":"**Elasticsearch version**: 5.5.0 (docker.elastic.co/elasticsearch/elasticsearch:5.5.0)\r\n\r\n**Description**:\r\n\r\nThe documentation for the [span near query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-span-near-query.html) states:\r\n\r\n> The clauses element is a list of **one or more** other span type queries \r\n\r\n(emphasis mine)\r\n\r\nJust supplying *one* clause though, causes an error to be returned ~*iff the HTTP request method is `POST` instead of `GET`*~. E.g.\r\n\r\n```\r\n...\r\n    \"failures\": [\r\n      {\r\n        \"shard\": 0,\r\n        \"index\": \"test\",\r\n        \"node\": \"...\",\r\n        \"reason\": {\r\n          \"type\": \"illegal_argument_exception\",\r\n          \"reason\": \"Less than 2 subSpans.size():1\"\r\n        }\r\n      }\r\n    ]\r\n```\r\n---\r\n\r\n\r\n**Steps to reproduce**:\r\n\r\nWith ES 5.5.0 running:\r\n\r\n 1. Create a mapping:\r\n\r\n```\r\ncurl -X \"PUT\" \"http://localhost:9200/test\" \\\r\n     -H \"Content-Type: text/plain; charset=utf-8\" \\\r\n     -u elastic:changeme \\\r\n     -d $'{\r\n  \"settings\": {\r\n    \"analysis\": {\r\n      \"analyzer\": {\r\n        \"my_analyzer\": {\r\n          \"tokenizer\": \"my_pattern_tokenizer\"\r\n        }\r\n      },\r\n      \"tokenizer\": {\r\n        \"my_pattern_tokenizer\": {\r\n          \"type\": \"pattern\",\r\n          \"pattern\": \"\\\\\\\\/\"\r\n        }\r\n      }\r\n    }\r\n  },\r\n  \"mappings\": {\r\n    \"bar\": {\r\n      \"properties\": {\r\n        \"path\": {\r\n          \"type\": \"text\",\r\n          \"analyzer\": \"my_analyzer\"\r\n        }\r\n      }\r\n    }\r\n  }\r\n}'\r\n```\r\n\r\n 2. Add an example document:\r\n\r\n```\r\ncurl -X \"POST\" \"http://localhost:9200/test/bar\" \\\r\n     -H \"Content-Type: application/json\" \\\r\n     -u elastic:changeme \\\r\n     -d $'{\r\n  \"path\": \"foo/bar/baz\"\r\n}'\r\n```\r\n\r\n 3. Run a search using a span near query with a single clause:\r\n\r\n```\r\ncurl -X \"POST\" \"http://localhost:9200/test/bar/_search\" \\\r\n     -H \"Content-Type: application/json\" \\\r\n     -u elastic:changeme \\\r\n     -d $'{\r\n  \"query\": {\r\n    \"span_near\": {\r\n      \"clauses\": [\r\n        {\r\n          \"span_first\": {\r\n            \"match\": {\r\n              \"span_term\": {\r\n                \"path\": \"foo\"\r\n              }\r\n            },\r\n            \"end\": 1\r\n          }\r\n        }\r\n      ],\r\n      \"slop\": 2,\r\n      \"in_order\": true\r\n    }\r\n  }\r\n}'\r\n\r\n```\r\n\r\n~If using an HTTP **`POST`**~ this fails with:\r\n\r\n```\r\n...\r\n    \"failures\": [\r\n      {\r\n        \"shard\": 1,\r\n        \"index\": \"test\",\r\n        \"node\": \"24CCgJNwRbCoxUlUbFXiuw\",\r\n        \"reason\": {\r\n          \"type\": \"illegal_argument_exception\",\r\n          \"reason\": \"Less than 2 subSpans.size():1\"\r\n        }\r\n      }\r\n    ]\r\n...\r\n```\r\n\r\n~The same request using a **`GET`** request method succeeds.~\r\n\r\n~According to https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html it should be possible to use either `POST` or `GET`.~\r\n\r\n`GET` vs. `POST` was just a red herring. This fails in either case.\r\n\r\n","closed_by":{"login":"jimczi","id":15977469,"node_id":"MDQ6VXNlcjE1OTc3NDY5","avatar_url":"https://avatars0.githubusercontent.com/u/15977469?v=4","gravatar_id":"","url":"https://api.github.com/users/jimczi","html_url":"https://github.com/jimczi","followers_url":"https://api.github.com/users/jimczi/followers","following_url":"https://api.github.com/users/jimczi/following{/other_user}","gists_url":"https://api.github.com/users/jimczi/gists{/gist_id}","starred_url":"https://api.github.com/users/jimczi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimczi/subscriptions","organizations_url":"https://api.github.com/users/jimczi/orgs","repos_url":"https://api.github.com/users/jimczi/repos","events_url":"https://api.github.com/users/jimczi/events{/privacy}","received_events_url":"https://api.github.com/users/jimczi/received_events","type":"User","site_admin":false}}", "commentIds":["314137213","314159710","314165827"], "labels":[":Search/Search",">enhancement"]}