{"id":"27293", "title":"Bug: Sum_bucket result ordering does not work", "body":"Using docker hub image:

REPOSITORY                      TAG                 IMAGE ID            CREATED             SIZE
elasticsearch                   5                   7516701e4922        11 weeks ago        315.5 MB


**Elasticsearch version** (`bin/elasticsearch --version`):
```
root@836e3b7e7622:/usr/share/elasticsearch# elasticsearch --version
Version: 5.5.2, Build: b2f0c09/2017-08-14T12:33:14.154Z, JVM: 1.8.0_141
```


**Plugins installed**: []

**JVM version** (`java -version`):
```
root@836e3b7e7622:/usr/share/elasticsearch# java -version
openjdk version \"1.8.0_141\"
OpenJDK Runtime Environment (build 1.8.0_141-8u141-b15-1~deb9u1-b15)
OpenJDK 64-Bit Server VM (build 25.141-b15, mixed mode)
```

Cant order on sum_bucket results:

The following query works
```

	\"aggs\": {
		\"sources\": {
			\"terms\": {
				\"field\": \"@source\",
				\"size\": 20,

				\"min_doc_count\": 1
			},
			\"aggs\": {
				\"sources_aggs\": {
					\"sum_bucket\":{
						 \"buckets_path\": \"sources_nested>sources_targets\"
					}
				},
				\"sources_nested\": {
					\"terms\": {
						\"field\": \"@target\",
						\"size\": 20,
						\"order\": {
							\"sources_targets\": \"desc\"
						},
						\"min_doc_count\": 1
					},
					\"aggs\": {
						\"sources_targets\": {
						
							\"avg\": {
								\"field\": \"@value\"
							}
						}
					}
				}
			}
		}
	}
}
```

Sample data that is written with curl is:

```
      {
          \"key\" : \"4507\",
          \"doc_count\" : 120,
          \"sources_nested\" : {
            \"doc_count_error_upper_bound\" : -1,
            \"sum_other_doc_count\" : 100,
            \"buckets\" : [
              {
                \"key\" : \"4506\",
                \"doc_count\" : 1,
                \"sources_targets\" : {
                  \"value\" : 0.9067630767822266
                }
              }

              ....
              {
                \"key\" : \"3870\",
                \"doc_count\" : 1,
                \"sources_targets\" : {
                  \"value\" : 0.29410073161125183
                }
              }
            ]
          },
          \"sources_aggs\" : {
            \"value\" : 10.384261131286621
          }
        }
```

But when I add order by the sources_aggs, ES gives error:

```

	\"aggs\": {
		\"sources\": {
			\"terms\": {
				\"field\": \"@source\",
				\"size\": 20,
				\"order\": {
					\"sources_aggs\": \"desc\"
				},
				\"min_doc_count\": 1
			},
			\"aggs\": {
				\"sources_aggs\": {
					\"sum_bucket\":{
						 \"buckets_path\": \"sources_nested>sources_targets\"
					}
				},
				\"sources_nested\": {
					\"terms\": {
						\"field\": \"@target\",
						\"size\": 20,
						\"order\": {
							\"sources_targets\": \"desc\"
						},
						\"min_doc_count\": 1
					},
					\"aggs\": {
						\"sources_targets\": {
						
							\"avg\": {
								\"field\": \"@value\"
							}
						}
					}
				}
			}
		}
	}
}
```
The error I get is:

```
        \"reason\" : \"Invalid term-aggregator order path [sources_aggs]. Unknown aggregation [sources_aggs]\"
```


sources_aggs.value does not work too.

Should I add reverse_nested similar to this bug?  #16838 

It should not be necessary.


Could this be because the sum_bucket aggregation is calculated at the very end? I do not know the implementation details, just a guess


", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/27293","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27293/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27293/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27293/events","html_url":"https://github.com/elastic/elasticsearch/issues/27293","id":271639009,"node_id":"MDU6SXNzdWUyNzE2MzkwMDk=","number":27293,"title":"Bug: Sum_bucket result ordering does not work","user":{"login":"ali-bugdayci","id":1422344,"node_id":"MDQ6VXNlcjE0MjIzNDQ=","avatar_url":"https://avatars1.githubusercontent.com/u/1422344?v=4","gravatar_id":"","url":"https://api.github.com/users/ali-bugdayci","html_url":"https://github.com/ali-bugdayci","followers_url":"https://api.github.com/users/ali-bugdayci/followers","following_url":"https://api.github.com/users/ali-bugdayci/following{/other_user}","gists_url":"https://api.github.com/users/ali-bugdayci/gists{/gist_id}","starred_url":"https://api.github.com/users/ali-bugdayci/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ali-bugdayci/subscriptions","organizations_url":"https://api.github.com/users/ali-bugdayci/orgs","repos_url":"https://api.github.com/users/ali-bugdayci/repos","events_url":"https://api.github.com/users/ali-bugdayci/events{/privacy}","received_events_url":"https://api.github.com/users/ali-bugdayci/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-11-06T22:11:18Z","updated_at":"2017-11-07T07:49:01Z","closed_at":"2017-11-07T07:41:09Z","author_association":"NONE","body":"Using docker hub image:\r\n\r\nREPOSITORY                      TAG                 IMAGE ID            CREATED             SIZE\r\nelasticsearch                   5                   7516701e4922        11 weeks ago        315.5 MB\r\n\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`):\r\n```\r\nroot@836e3b7e7622:/usr/share/elasticsearch# elasticsearch --version\r\nVersion: 5.5.2, Build: b2f0c09/2017-08-14T12:33:14.154Z, JVM: 1.8.0_141\r\n```\r\n\r\n\r\n**Plugins installed**: []\r\n\r\n**JVM version** (`java -version`):\r\n```\r\nroot@836e3b7e7622:/usr/share/elasticsearch# java -version\r\nopenjdk version \"1.8.0_141\"\r\nOpenJDK Runtime Environment (build 1.8.0_141-8u141-b15-1~deb9u1-b15)\r\nOpenJDK 64-Bit Server VM (build 25.141-b15, mixed mode)\r\n```\r\n\r\nCant order on sum_bucket results:\r\n\r\nThe following query works\r\n```\r\n\r\n\t\"aggs\": {\r\n\t\t\"sources\": {\r\n\t\t\t\"terms\": {\r\n\t\t\t\t\"field\": \"@source\",\r\n\t\t\t\t\"size\": 20,\r\n\r\n\t\t\t\t\"min_doc_count\": 1\r\n\t\t\t},\r\n\t\t\t\"aggs\": {\r\n\t\t\t\t\"sources_aggs\": {\r\n\t\t\t\t\t\"sum_bucket\":{\r\n\t\t\t\t\t\t \"buckets_path\": \"sources_nested>sources_targets\"\r\n\t\t\t\t\t}\r\n\t\t\t\t},\r\n\t\t\t\t\"sources_nested\": {\r\n\t\t\t\t\t\"terms\": {\r\n\t\t\t\t\t\t\"field\": \"@target\",\r\n\t\t\t\t\t\t\"size\": 20,\r\n\t\t\t\t\t\t\"order\": {\r\n\t\t\t\t\t\t\t\"sources_targets\": \"desc\"\r\n\t\t\t\t\t\t},\r\n\t\t\t\t\t\t\"min_doc_count\": 1\r\n\t\t\t\t\t},\r\n\t\t\t\t\t\"aggs\": {\r\n\t\t\t\t\t\t\"sources_targets\": {\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\"avg\": {\r\n\t\t\t\t\t\t\t\t\"field\": \"@value\"\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}\r\n```\r\n\r\nSample data that is written with curl is:\r\n\r\n```\r\n      {\r\n          \"key\" : \"4507\",\r\n          \"doc_count\" : 120,\r\n          \"sources_nested\" : {\r\n            \"doc_count_error_upper_bound\" : -1,\r\n            \"sum_other_doc_count\" : 100,\r\n            \"buckets\" : [\r\n              {\r\n                \"key\" : \"4506\",\r\n                \"doc_count\" : 1,\r\n                \"sources_targets\" : {\r\n                  \"value\" : 0.9067630767822266\r\n                }\r\n              }\r\n\r\n              ....\r\n              {\r\n                \"key\" : \"3870\",\r\n                \"doc_count\" : 1,\r\n                \"sources_targets\" : {\r\n                  \"value\" : 0.29410073161125183\r\n                }\r\n              }\r\n            ]\r\n          },\r\n          \"sources_aggs\" : {\r\n            \"value\" : 10.384261131286621\r\n          }\r\n        }\r\n```\r\n\r\nBut when I add order by the sources_aggs, ES gives error:\r\n\r\n```\r\n\r\n\t\"aggs\": {\r\n\t\t\"sources\": {\r\n\t\t\t\"terms\": {\r\n\t\t\t\t\"field\": \"@source\",\r\n\t\t\t\t\"size\": 20,\r\n\t\t\t\t\"order\": {\r\n\t\t\t\t\t\"sources_aggs\": \"desc\"\r\n\t\t\t\t},\r\n\t\t\t\t\"min_doc_count\": 1\r\n\t\t\t},\r\n\t\t\t\"aggs\": {\r\n\t\t\t\t\"sources_aggs\": {\r\n\t\t\t\t\t\"sum_bucket\":{\r\n\t\t\t\t\t\t \"buckets_path\": \"sources_nested>sources_targets\"\r\n\t\t\t\t\t}\r\n\t\t\t\t},\r\n\t\t\t\t\"sources_nested\": {\r\n\t\t\t\t\t\"terms\": {\r\n\t\t\t\t\t\t\"field\": \"@target\",\r\n\t\t\t\t\t\t\"size\": 20,\r\n\t\t\t\t\t\t\"order\": {\r\n\t\t\t\t\t\t\t\"sources_targets\": \"desc\"\r\n\t\t\t\t\t\t},\r\n\t\t\t\t\t\t\"min_doc_count\": 1\r\n\t\t\t\t\t},\r\n\t\t\t\t\t\"aggs\": {\r\n\t\t\t\t\t\t\"sources_targets\": {\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\"avg\": {\r\n\t\t\t\t\t\t\t\t\"field\": \"@value\"\r\n\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}\r\n```\r\nThe error I get is:\r\n\r\n```\r\n        \"reason\" : \"Invalid term-aggregator order path [sources_aggs]. Unknown aggregation [sources_aggs]\"\r\n```\r\n\r\n\r\nsources_aggs.value does not work too.\r\n\r\nShould I add reverse_nested similar to this bug?  #16838 \r\n\r\nIt should not be necessary.\r\n\r\n\r\nCould this be because the sum_bucket aggregation is calculated at the very end? I do not know the implementation details, just a guess\r\n\r\n\r\n","closed_by":{"login":"colings86","id":236731,"node_id":"MDQ6VXNlcjIzNjczMQ==","avatar_url":"https://avatars0.githubusercontent.com/u/236731?v=4","gravatar_id":"","url":"https://api.github.com/users/colings86","html_url":"https://github.com/colings86","followers_url":"https://api.github.com/users/colings86/followers","following_url":"https://api.github.com/users/colings86/following{/other_user}","gists_url":"https://api.github.com/users/colings86/gists{/gist_id}","starred_url":"https://api.github.com/users/colings86/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/colings86/subscriptions","organizations_url":"https://api.github.com/users/colings86/orgs","repos_url":"https://api.github.com/users/colings86/repos","events_url":"https://api.github.com/users/colings86/events{/privacy}","received_events_url":"https://api.github.com/users/colings86/received_events","type":"User","site_admin":false}}", "commentIds":["342400372","342401866"], "labels":[]}