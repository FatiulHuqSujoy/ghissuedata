{"id":"34700", "title":"Improve documentation", "body":"<!--

** Please read the guidelines below. **

Issues that do not follow these guidelines are likely to be closed.

1.  GitHub is reserved for bug reports and feature requests. The best place to
    ask a general question is at the Elastic [forums](https://discuss.elastic.co).
    GitHub is not the place for general questions.

2.  Is this bug report or feature request for a supported OS? If not, it
    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os

3.  Please fill out EITHER the feature request block or the bug report block
    below, and delete the other block.

-->

<!-- Feature request -->

**Describe the feature**:
As the document is shown on elastic website below:
 [https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low-usage-requests.html#java-rest-low-usage-request-options](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low-usage-requests.html#java-rest-low-usage-request-options)

Using the code below , we should be able to create a `RequestOptions`,but it had a compile error because of `HeapBufferedResponseConsumerFactory`.

The source in [RestClientDocumentation.java](https://github.com/elastic/elasticsearch/blob/master/client/rest/src/test/java/org/elasticsearch/client/documentation/RestClientDocumentation.java) is right of course because it imported the class with`import HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory`  

But for developers it will not be automatically imported in Eclipse. and I need to read the ElasticSearch java resource to find and import the class or go to github to read the sample. 

So, I want to improve it and make it more developer friendly that every beginner can use it with copy simply.

**Code on website**
```
private static final RequestOptions COMMON_OPTIONS;
static {
    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); 
    builder.setHttpAsyncResponseConsumerFactory(           
        new HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
    COMMON_OPTIONS = builder.build();
}
```
**Code in RestClientDocumentation.java**
```
import org.elasticsearch.client.HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory;
...
...
private static final RequestOptions COMMON_OPTIONS;
static {
    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); 
    builder.setHttpAsyncResponseConsumerFactory(           
        new HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
    COMMON_OPTIONS = builder.build();
}
```

**Code modified**
```
import org.elasticsearch.client.HttpAsyncResponseConsumerFactory.;
...
...
private static final RequestOptions COMMON_OPTIONS;
static {
    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); 
    builder.setHttpAsyncResponseConsumerFactory(           
        new HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
    COMMON_OPTIONS = builder.build();
}
```

", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/34700","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/34700/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/34700/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/34700/events","html_url":"https://github.com/elastic/elasticsearch/issues/34700","id":372434018,"node_id":"MDU6SXNzdWUzNzI0MzQwMTg=","number":34700,"title":"Improve documentation","user":{"login":"wangzhenhui1992","id":16408480,"node_id":"MDQ6VXNlcjE2NDA4NDgw","avatar_url":"https://avatars1.githubusercontent.com/u/16408480?v=4","gravatar_id":"","url":"https://api.github.com/users/wangzhenhui1992","html_url":"https://github.com/wangzhenhui1992","followers_url":"https://api.github.com/users/wangzhenhui1992/followers","following_url":"https://api.github.com/users/wangzhenhui1992/following{/other_user}","gists_url":"https://api.github.com/users/wangzhenhui1992/gists{/gist_id}","starred_url":"https://api.github.com/users/wangzhenhui1992/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/wangzhenhui1992/subscriptions","organizations_url":"https://api.github.com/users/wangzhenhui1992/orgs","repos_url":"https://api.github.com/users/wangzhenhui1992/repos","events_url":"https://api.github.com/users/wangzhenhui1992/events{/privacy}","received_events_url":"https://api.github.com/users/wangzhenhui1992/received_events","type":"User","site_admin":false},"labels":[{"id":23715,"node_id":"MDU6TGFiZWwyMzcxNQ==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Edocs","name":">docs","color":"db755e","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":4,"created_at":"2018-10-22T08:29:31Z","updated_at":"2018-10-23T00:37:37Z","closed_at":"2018-10-22T17:47:08Z","author_association":"CONTRIBUTOR","body":"<!--\r\n\r\n** Please read the guidelines below. **\r\n\r\nIssues that do not follow these guidelines are likely to be closed.\r\n\r\n1.  GitHub is reserved for bug reports and feature requests. The best place to\r\n    ask a general question is at the Elastic [forums](https://discuss.elastic.co).\r\n    GitHub is not the place for general questions.\r\n\r\n2.  Is this bug report or feature request for a supported OS? If not, it\r\n    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os\r\n\r\n3.  Please fill out EITHER the feature request block or the bug report block\r\n    below, and delete the other block.\r\n\r\n-->\r\n\r\n<!-- Feature request -->\r\n\r\n**Describe the feature**:\r\nAs the document is shown on elastic website below:\r\n [https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low-usage-requests.html#java-rest-low-usage-request-options](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low-usage-requests.html#java-rest-low-usage-request-options)\r\n\r\nUsing the code below , we should be able to create a `RequestOptions`,but it had a compile error because of `HeapBufferedResponseConsumerFactory`.\r\n\r\nThe source in [RestClientDocumentation.java](https://github.com/elastic/elasticsearch/blob/master/client/rest/src/test/java/org/elasticsearch/client/documentation/RestClientDocumentation.java) is right of course because it imported the class with`import HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory`  \r\n\r\nBut for developers it will not be automatically imported in Eclipse. and I need to read the ElasticSearch java resource to find and import the class or go to github to read the sample. \r\n\r\nSo, I want to improve it and make it more developer friendly that every beginner can use it with copy simply.\r\n\r\n**Code on website**\r\n```\r\nprivate static final RequestOptions COMMON_OPTIONS;\r\nstatic {\r\n    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();\r\n    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); \r\n    builder.setHttpAsyncResponseConsumerFactory(           \r\n        new HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));\r\n    COMMON_OPTIONS = builder.build();\r\n}\r\n```\r\n**Code in RestClientDocumentation.java**\r\n```\r\nimport org.elasticsearch.client.HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory;\r\n...\r\n...\r\nprivate static final RequestOptions COMMON_OPTIONS;\r\nstatic {\r\n    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();\r\n    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); \r\n    builder.setHttpAsyncResponseConsumerFactory(           \r\n        new HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));\r\n    COMMON_OPTIONS = builder.build();\r\n}\r\n```\r\n\r\n**Code modified**\r\n```\r\nimport org.elasticsearch.client.HttpAsyncResponseConsumerFactory.;\r\n...\r\n...\r\nprivate static final RequestOptions COMMON_OPTIONS;\r\nstatic {\r\n    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();\r\n    builder.addHeader(\"Authorization\", \"Bearer \" + TOKEN); \r\n    builder.setHttpAsyncResponseConsumerFactory(           \r\n        new HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));\r\n    COMMON_OPTIONS = builder.build();\r\n}\r\n```\r\n\r\n","closed_by":{"login":"albertzaharovits","id":4568420,"node_id":"MDQ6VXNlcjQ1Njg0MjA=","avatar_url":"https://avatars2.githubusercontent.com/u/4568420?v=4","gravatar_id":"","url":"https://api.github.com/users/albertzaharovits","html_url":"https://github.com/albertzaharovits","followers_url":"https://api.github.com/users/albertzaharovits/followers","following_url":"https://api.github.com/users/albertzaharovits/following{/other_user}","gists_url":"https://api.github.com/users/albertzaharovits/gists{/gist_id}","starred_url":"https://api.github.com/users/albertzaharovits/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/albertzaharovits/subscriptions","organizations_url":"https://api.github.com/users/albertzaharovits/orgs","repos_url":"https://api.github.com/users/albertzaharovits/repos","events_url":"https://api.github.com/users/albertzaharovits/events{/privacy}","received_events_url":"https://api.github.com/users/albertzaharovits/received_events","type":"User","site_admin":false}}", "commentIds":["431912522","432036179","432038550","432040640"], "labels":[">docs"]}