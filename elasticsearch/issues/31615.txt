{"id":"31615", "title":"Introduce vector field, vector query and rescoring based on them", "body":"Introduce a new field of type `vector` on which vector calculations can be done during rescoring phase

```json
PUT my_index
{
  \"mappings\": {
    \"_doc\": {
      \"properties\": {
        \"my_feature\": {
          \"type\": \"vector\"   
      }
    }
  }
}
```

## Indexing
Allow only a single value per document
Allow to index both dense and sparse vectors?

Dense form:
```json
PUT my_index/_doc/1
{
  \"my_feature\":   [11.5, 10.4, 23.0]
}
```
Sparse form (represented as  list of dimension names and values for corresponding dimensions):
```json
PUT my_index/_doc/1
{
  \"my_feature\": {\"1\": 11.5, \"5\": 10.5,  \"101\": 23.0}
}
```

## Query and Rescoring
Introduce a special type of `vector` query:
```json
\"vector\" : {
   \"field\" : \"my_feature\",
    \"query_vector\": {\"1\": 3, \"5\": 10.5,  \"101\": 12}
}
```
This query **can only be used in the rescoring context**.
This query produces a score for every document in the rescoring context in the following way:
1) If a document doesn't have a vector value for `field`, 0 value will be returned
2) If a document does have a vector value  for `field` : doc_vector, the cosine similarity between doc_vector and `query_vector` is calculated:
`dotProduct(doc_vector, query_vector)  /  (sqrt(doc_vector) * sqrt(query_vector))`


```json
POST /_search
{
   \"query\" : {\"<user-query>\"},
   \"rescore\" : {
      \"window_size\" : 50,
      \"query\" : {
         \"rescore_query\" : {
            \"vector\" : {
               \"field\" : \"my_feature\",
               \"query_vector\": {\"1\": 3, \"5\": 10.5,  \"101\": 12}
            }
         }
      }
   }
}
```


## Internal encoding

1. Encoding of vectors:
Internally both dense and sparse vectors are encoded as sorted hash?
Thus dense array is transformed:
[4, 12] -> {0: 4, 1: 12}
Keys are sorted, so we can iterate over them instead of calculating hash

2.  What should be values in vectors?
    - floats?
    - smaller than floats?  (lost some precision here, but less index size)

3. Vectors are encoded as binaries.



", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/31615","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31615/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31615/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31615/events","html_url":"https://github.com/elastic/elasticsearch/issues/31615","id":336264895,"node_id":"MDU6SXNzdWUzMzYyNjQ4OTU=","number":31615,"title":"Introduce vector field, vector query and rescoring based on them","user":{"login":"mayya-sharipova","id":5738841,"node_id":"MDQ6VXNlcjU3Mzg4NDE=","avatar_url":"https://avatars1.githubusercontent.com/u/5738841?v=4","gravatar_id":"","url":"https://api.github.com/users/mayya-sharipova","html_url":"https://github.com/mayya-sharipova","followers_url":"https://api.github.com/users/mayya-sharipova/followers","following_url":"https://api.github.com/users/mayya-sharipova/following{/other_user}","gists_url":"https://api.github.com/users/mayya-sharipova/gists{/gist_id}","starred_url":"https://api.github.com/users/mayya-sharipova/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mayya-sharipova/subscriptions","organizations_url":"https://api.github.com/users/mayya-sharipova/orgs","repos_url":"https://api.github.com/users/mayya-sharipova/repos","events_url":"https://api.github.com/users/mayya-sharipova/events{/privacy}","received_events_url":"https://api.github.com/users/mayya-sharipova/received_events","type":"User","site_admin":false},"labels":[{"id":418189364,"node_id":"MDU6TGFiZWw0MTgxODkzNjQ=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Search/Ranking","name":":Search/Ranking","color":"0e8a16","default":false}],"state":"closed","locked":false,"assignee":{"login":"mayya-sharipova","id":5738841,"node_id":"MDQ6VXNlcjU3Mzg4NDE=","avatar_url":"https://avatars1.githubusercontent.com/u/5738841?v=4","gravatar_id":"","url":"https://api.github.com/users/mayya-sharipova","html_url":"https://github.com/mayya-sharipova","followers_url":"https://api.github.com/users/mayya-sharipova/followers","following_url":"https://api.github.com/users/mayya-sharipova/following{/other_user}","gists_url":"https://api.github.com/users/mayya-sharipova/gists{/gist_id}","starred_url":"https://api.github.com/users/mayya-sharipova/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mayya-sharipova/subscriptions","organizations_url":"https://api.github.com/users/mayya-sharipova/orgs","repos_url":"https://api.github.com/users/mayya-sharipova/repos","events_url":"https://api.github.com/users/mayya-sharipova/events{/privacy}","received_events_url":"https://api.github.com/users/mayya-sharipova/received_events","type":"User","site_admin":false},"assignees":[{"login":"mayya-sharipova","id":5738841,"node_id":"MDQ6VXNlcjU3Mzg4NDE=","avatar_url":"https://avatars1.githubusercontent.com/u/5738841?v=4","gravatar_id":"","url":"https://api.github.com/users/mayya-sharipova","html_url":"https://github.com/mayya-sharipova","followers_url":"https://api.github.com/users/mayya-sharipova/followers","following_url":"https://api.github.com/users/mayya-sharipova/following{/other_user}","gists_url":"https://api.github.com/users/mayya-sharipova/gists{/gist_id}","starred_url":"https://api.github.com/users/mayya-sharipova/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mayya-sharipova/subscriptions","organizations_url":"https://api.github.com/users/mayya-sharipova/orgs","repos_url":"https://api.github.com/users/mayya-sharipova/repos","events_url":"https://api.github.com/users/mayya-sharipova/events{/privacy}","received_events_url":"https://api.github.com/users/mayya-sharipova/received_events","type":"User","site_admin":false}],"milestone":null,"comments":25,"created_at":"2018-06-27T14:53:36Z","updated_at":"2019-10-03T23:49:10Z","closed_at":"2019-02-20T12:01:18Z","author_association":"CONTRIBUTOR","body":"Introduce a new field of type `vector` on which vector calculations can be done during rescoring phase\r\n\r\n```json\r\nPUT my_index\r\n{\r\n  \"mappings\": {\r\n    \"_doc\": {\r\n      \"properties\": {\r\n        \"my_feature\": {\r\n          \"type\": \"vector\"   \r\n      }\r\n    }\r\n  }\r\n}\r\n```\r\n\r\n## Indexing\r\nAllow only a single value per document\r\nAllow to index both dense and sparse vectors?\r\n\r\nDense form:\r\n```json\r\nPUT my_index/_doc/1\r\n{\r\n  \"my_feature\":   [11.5, 10.4, 23.0]\r\n}\r\n```\r\nSparse form (represented as  list of dimension names and values for corresponding dimensions):\r\n```json\r\nPUT my_index/_doc/1\r\n{\r\n  \"my_feature\": {\"1\": 11.5, \"5\": 10.5,  \"101\": 23.0}\r\n}\r\n```\r\n\r\n## Query and Rescoring\r\nIntroduce a special type of `vector` query:\r\n```json\r\n\"vector\" : {\r\n   \"field\" : \"my_feature\",\r\n    \"query_vector\": {\"1\": 3, \"5\": 10.5,  \"101\": 12}\r\n}\r\n```\r\nThis query **can only be used in the rescoring context**.\r\nThis query produces a score for every document in the rescoring context in the following way:\r\n1) If a document doesn't have a vector value for `field`, 0 value will be returned\r\n2) If a document does have a vector value  for `field` : doc_vector, the cosine similarity between doc_vector and `query_vector` is calculated:\r\n`dotProduct(doc_vector, query_vector)  /  (sqrt(doc_vector) * sqrt(query_vector))`\r\n\r\n\r\n```json\r\nPOST /_search\r\n{\r\n   \"query\" : {\"<user-query>\"},\r\n   \"rescore\" : {\r\n      \"window_size\" : 50,\r\n      \"query\" : {\r\n         \"rescore_query\" : {\r\n            \"vector\" : {\r\n               \"field\" : \"my_feature\",\r\n               \"query_vector\": {\"1\": 3, \"5\": 10.5,  \"101\": 12}\r\n            }\r\n         }\r\n      }\r\n   }\r\n}\r\n```\r\n\r\n\r\n## Internal encoding\r\n\r\n1. Encoding of vectors:\r\nInternally both dense and sparse vectors are encoded as sorted hash?\r\nThus dense array is transformed:\r\n[4, 12] -> {0: 4, 1: 12}\r\nKeys are sorted, so we can iterate over them instead of calculating hash\r\n\r\n2.  What should be values in vectors?\r\n    - floats?\r\n    - smaller than floats?  (lost some precision here, but less index size)\r\n\r\n3. Vectors are encoded as binaries.\r\n\r\n\r\n\r\n","closed_by":{"login":"mayya-sharipova","id":5738841,"node_id":"MDQ6VXNlcjU3Mzg4NDE=","avatar_url":"https://avatars1.githubusercontent.com/u/5738841?v=4","gravatar_id":"","url":"https://api.github.com/users/mayya-sharipova","html_url":"https://github.com/mayya-sharipova","followers_url":"https://api.github.com/users/mayya-sharipova/followers","following_url":"https://api.github.com/users/mayya-sharipova/following{/other_user}","gists_url":"https://api.github.com/users/mayya-sharipova/gists{/gist_id}","starred_url":"https://api.github.com/users/mayya-sharipova/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/mayya-sharipova/subscriptions","organizations_url":"https://api.github.com/users/mayya-sharipova/orgs","repos_url":"https://api.github.com/users/mayya-sharipova/repos","events_url":"https://api.github.com/users/mayya-sharipova/events{/privacy}","received_events_url":"https://api.github.com/users/mayya-sharipova/received_events","type":"User","site_admin":false}}", "commentIds":["400702221","400712095","402009216","402188187","402294657","408330958","408545322","408549273","449494652","450142434","450454307","458033969","458360808","460665091","470266031","470475661","471443814","494601250","494800612","505896948","506519244","517151412","517250483","538106040","538171576"], "labels":[":Search/Ranking"]}