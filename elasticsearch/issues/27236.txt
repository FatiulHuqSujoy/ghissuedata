{"id":"27236", "title":"Test sysctl vm.max_map_count before failing init script", "body":"
<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`): <= current master

**Plugins installed**: n/a

**JVM version** (`java -version`): any

**OS version** (`uname -a` if on a Unix-like system): Linux xxxx 4.10.0-37-generic #41~16.04.1-Ubuntu SMP Fri Oct 6 22:42:59 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

**Description of the problem including expected versus actual behavior**:
elasticsearch daemon always fails to start when run inside a container.
Assuming value of vm.max_map_count >= MAX_MAP_COUNT, I expect the daemon to start when the init script is run.

The elasticsearch daemon requires that the value of /proc/sys/vm/max_map_count be equal to or greater than MAX_MAP_COUNT. The daemon init script attempts to set this value with sysctl.
Unfortunately, since this value cannot be set from inside a container, the init script always fails, even when the current value is perfectly good.

Issue was encountered in LXC containers, it also applies to LXD, Docker and other container environments.

Given that the smarter DevOps practitioner will set this value on the host PRIOR to starting the container, would it be reasonable to TEST the value before refusing to start the daemon?

I proposes a minor change to distribution/deb/src/main/packaging/init.d/elasticsearch (and other OS scripts as applicable) as follows:
```
126c126,128
< 		sysctl -q -w vm.max_map_count=$MAX_MAP_COUNT
---
> 		if [ `sysctl -n vm.max_map_count` -lt $MAX_MAP_COUNT ];then
> 			sysctl -q -w vm.max_map_count=$MAX_MAP_COUNT
> 		fi
```
As a sysadmin, it \"feels wrong\" to change a kernel parameter, potentially affecting other applications on the host, without first checking if the change is required. This patch adds a test of the current value of vm.max_map_count against MAX_MAP_COUNT. If the current value equals or exceeds the required value, no action is taken and the daemon will start. Only if the current value is insufficient, will the script attempt to make the change and fail.

My only concern is for the portability of the test construct, in case it is too \"BASH-centric\".

I will submit a pull request including patches for all the supported OS init scripts if this would be acceptable.

More background info at https://github.com/elastic/puppet-elasticsearch/issues/887

regards,

Drew


**Steps to reproduce**:
1. Install elasticsearch in a container
2. Start with the init script


", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/27236","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27236/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27236/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27236/events","html_url":"https://github.com/elastic/elasticsearch/issues/27236","id":270706998,"node_id":"MDU6SXNzdWUyNzA3MDY5OTg=","number":27236,"title":"Test sysctl vm.max_map_count before failing init script","user":{"login":"biggreenogre","id":3988067,"node_id":"MDQ6VXNlcjM5ODgwNjc=","avatar_url":"https://avatars0.githubusercontent.com/u/3988067?v=4","gravatar_id":"","url":"https://api.github.com/users/biggreenogre","html_url":"https://github.com/biggreenogre","followers_url":"https://api.github.com/users/biggreenogre/followers","following_url":"https://api.github.com/users/biggreenogre/following{/other_user}","gists_url":"https://api.github.com/users/biggreenogre/gists{/gist_id}","starred_url":"https://api.github.com/users/biggreenogre/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/biggreenogre/subscriptions","organizations_url":"https://api.github.com/users/biggreenogre/orgs","repos_url":"https://api.github.com/users/biggreenogre/repos","events_url":"https://api.github.com/users/biggreenogre/events{/privacy}","received_events_url":"https://api.github.com/users/biggreenogre/received_events","type":"User","site_admin":false},"labels":[{"id":114977275,"node_id":"MDU6TGFiZWwxMTQ5NzcyNzU=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Infra/Packaging","name":":Core/Infra/Packaging","color":"0e8a16","default":false},{"id":23174,"node_id":"MDU6TGFiZWwyMzE3NA==","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Eenhancement","name":">enhancement","color":"4a4ea8","default":false},{"id":1009148120,"node_id":"MDU6TGFiZWwxMDA5MTQ4MTIw","url":"https://api.github.com/repos/elastic/elasticsearch/labels/v6.4.1","name":"v6.4.1","color":"DDDDDD","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":6,"created_at":"2017-11-02T15:54:28Z","updated_at":"2019-01-16T07:12:56Z","closed_at":"2019-01-16T07:12:55Z","author_association":"NONE","body":"\r\n<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`): <= current master\r\n\r\n**Plugins installed**: n/a\r\n\r\n**JVM version** (`java -version`): any\r\n\r\n**OS version** (`uname -a` if on a Unix-like system): Linux xxxx 4.10.0-37-generic #41~16.04.1-Ubuntu SMP Fri Oct 6 22:42:59 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nelasticsearch daemon always fails to start when run inside a container.\r\nAssuming value of vm.max_map_count >= MAX_MAP_COUNT, I expect the daemon to start when the init script is run.\r\n\r\nThe elasticsearch daemon requires that the value of /proc/sys/vm/max_map_count be equal to or greater than MAX_MAP_COUNT. The daemon init script attempts to set this value with sysctl.\r\nUnfortunately, since this value cannot be set from inside a container, the init script always fails, even when the current value is perfectly good.\r\n\r\nIssue was encountered in LXC containers, it also applies to LXD, Docker and other container environments.\r\n\r\nGiven that the smarter DevOps practitioner will set this value on the host PRIOR to starting the container, would it be reasonable to TEST the value before refusing to start the daemon?\r\n\r\nI proposes a minor change to distribution/deb/src/main/packaging/init.d/elasticsearch (and other OS scripts as applicable) as follows:\r\n```\r\n126c126,128\r\n< \t\tsysctl -q -w vm.max_map_count=$MAX_MAP_COUNT\r\n---\r\n> \t\tif [ `sysctl -n vm.max_map_count` -lt $MAX_MAP_COUNT ];then\r\n> \t\t\tsysctl -q -w vm.max_map_count=$MAX_MAP_COUNT\r\n> \t\tfi\r\n```\r\nAs a sysadmin, it \"feels wrong\" to change a kernel parameter, potentially affecting other applications on the host, without first checking if the change is required. This patch adds a test of the current value of vm.max_map_count against MAX_MAP_COUNT. If the current value equals or exceeds the required value, no action is taken and the daemon will start. Only if the current value is insufficient, will the script attempt to make the change and fail.\r\n\r\nMy only concern is for the portability of the test construct, in case it is too \"BASH-centric\".\r\n\r\nI will submit a pull request including patches for all the supported OS init scripts if this would be acceptable.\r\n\r\nMore background info at https://github.com/elastic/puppet-elasticsearch/issues/887\r\n\r\nregards,\r\n\r\nDrew\r\n\r\n\r\n**Steps to reproduce**:\r\n1. Install elasticsearch in a container\r\n2. Start with the init script\r\n\r\n\r\n","closed_by":{"login":"danielmitterdorfer","id":1699576,"node_id":"MDQ6VXNlcjE2OTk1NzY=","avatar_url":"https://avatars3.githubusercontent.com/u/1699576?v=4","gravatar_id":"","url":"https://api.github.com/users/danielmitterdorfer","html_url":"https://github.com/danielmitterdorfer","followers_url":"https://api.github.com/users/danielmitterdorfer/followers","following_url":"https://api.github.com/users/danielmitterdorfer/following{/other_user}","gists_url":"https://api.github.com/users/danielmitterdorfer/gists{/gist_id}","starred_url":"https://api.github.com/users/danielmitterdorfer/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/danielmitterdorfer/subscriptions","organizations_url":"https://api.github.com/users/danielmitterdorfer/orgs","repos_url":"https://api.github.com/users/danielmitterdorfer/repos","events_url":"https://api.github.com/users/danielmitterdorfer/events{/privacy}","received_events_url":"https://api.github.com/users/danielmitterdorfer/received_events","type":"User","site_admin":false}}", "commentIds":["341595445","350317919","367367241","372483023","375652669","454675885"], "labels":[":Core/Infra/Packaging",">enhancement","v6.4.1"]}