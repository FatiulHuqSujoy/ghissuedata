{"id":"29827", "title":"Java high-level REST client completeness for xpack APIs", "body":"*Original comment by @javanna:*

This is the xpack side of https://github.com/elastic/elasticsearch/issues/27205 . This is a meta issue to track completeness of the Java REST high-level Client in terms of supported xpack API. The following list includes all the REST API that xpack exposes to date, and that are also exposed by the Transport Client. The ones marked as done are already supported by the high-level REST client, while the others need to be added. Every group is sorted based on an estimation around how important the API is, from more important to less important. Each API is also assigned a rank from 1 to 3 that expresses how difficult adding support for it is expected to be.

*Please Note* The existing actions are in x-pack/ which is governed by the Elastic License, not the Apache License. Engineers who take these should strive to pull the Actions into Apache License controlled territory, so the high level rest client does not use any Elastic License bits. These should live in `x-pack/protocol`, which is OSS.

*Please also note* The APIs themselves should follow [the spec](https://github.com/elastic/elasticsearch/tree/master/x-pack/plugin/src/test/resources/rest-api-spec/api) for how they are named. This means they should be in the XPackClient, not the HighLevelRestClient. This would mean you end up with `hlRestClient.xpack().watcher()` as an example

*Please Please also also note* Prepend your commit msgs with `HLRC:`

## Watcher API
- [x] put watch * #32026 @spinscale 
- [x] get watch * #35531 @jimczi
- [x] delete watch * #32337 @ywelsch
- [x] execute watch * #35868 @romseygeek
- [x] ack watch * #33962 @jtibshirani
- [x] activate watch * #33988 @iverase
- [x] deactivate watch * #34192 @not-napoleon
- [x] watcher stats ** @imotov 
- [x] watcher stop * #34317 @jdconrad 
- [x] watcher start * #34317 @jdconrad  

## Machine Learning API
- [x] ml info action @vladimirdolzhenko #35777
- [x] Create calendar ** (highest prio) @davidkyle #33362
- [x] Delete calendar * @davidkyle #33775
- [x] Add job to calendar * [ @benwtrent |  #35666 ]
- [x] Delete job from calendar * [ @benwtrent | #35713 ]
- [x] Add scheduled events to calendar * [ @benwtrent | #35704 ]
- [x] Delete scheduled events from calendar *  [ @benwtrent | #35760 ]
- [x] Get calendars * @davidkyle #33760
- [x] Get scheduled events * [ @benwtrent | #35747 ]
- [x] Create datafeed ** (highest prio) #33603 @dimitris-athanasiou
- [x] Delete datafeed * #33667  @dimitris-athanasiou
- [x] Start datafeed * [ @benwtrent | #33898 ]
- [x] Stop datafeed * [ @benwtrent | #33946 ]
- [x] Get datafeed info * #33715 @dimitris-athanasiou
- [x] Get datafeed statistics ** [ @benwtrent | #34271 ]
- [x] Preview datafeed * [ @benwtrent | #34284 ]
- [x] Update datafeed * [ @benwtrent | #34882 ]
- [x] Create job ** (highest prio) [ @droberts195, @benwtrent | #32726 ]
- [x] Delete job * [ @benwtrent | #32820 ]
- [x] Open job * [ @benwtrent | #32860 ]
- [x] Close job * [ @benwtrent | #32943 ]
- [x] Get job info * [ @benwtrent | #32960 ]
- [x] Get job statistics ** [ @benwtrent | #33183 ]
- [x] Flush job * [ @benwtrent | #33187]
- [x] Post data to job ** [ @benwtrent | #33443 ]
- [x] Update job * (done after put job API) [ @benwtrent | #33392 ]
- [x] Forecast job *  [ @benwtrent | #33506 ]
- [x] Delete Forecast *  [ @benwtrent | #33526 ]
- [x] Delete model snapshot * @edsavage #35537 
- [x] Get model snapshot info * @edsavage #35487 
- [x] Revert model snapshot * @edsavage #35750
- [x] Update model snapshot  ** @edsavage #35694 
- [x] Get buckets * #33056 @dimitris-athanasiou
- [x] Get overall buckets * #33297 @dimitris-athanasiou
- [x] Get categories * #33465 @edsavage
- [x] Get influencers * #33389 @dimitris-athanasiou
- [x] Get records * #33085 @dimitris-athanasiou
- [x] Find file structure * #35833 @droberts195
- [x] Put filter * [ @benwtrent | #35175 ]
- [x] Get filters * [ @benwtrent | #35502  ]
- [x] Update filters * [ @benwtrent | #35522 ]
- [x] Delete filters * #35382 @iverase 

### Machine Learning REST only API
- [x] delete expired data * @edsavage #35906

## Security API
- [x] clear cache * #35163 @jaymode
- [x] has privileges ** #35479 @tvernum 
- [x] put role ** #36209 @albertzaharovits
- [x] get roles ** 35787 @jkakavas 
- [x] clear role cache * @bleskes, #34187
- [x] delete role * #34620 @ywelsch - Has no API docs in the api ref
- [x] create role mapping ** #34171 @bizybot
- [x] modify role mapping ** #34171 @bizybot
- [x] delete role mapping * #34531 @bizybot
- [x] get role mappings ** #34637 @bizybot
- [x] get ssl certificates ** #34135 @jkakavas 
- [x] post create token * #34791 @tvernum
- [x] delete invalidate token * #35114 @tvernum
- [x] put user ** (highest prio) #32332 @jaymode
- [x] get users ** #36332 @nknize
- [x] delete user * #35294 @iverase 
- [x] disable user #33481 @jaymode
- [x] enable user #33481 @jaymode
- [x] change password #33509 * @jkakavas
- [x] xpack.security.get_privileges #35556 @jkakavas 
- [x] xpack.security.delete_privileges. #35454 @tlrx 
- [x] xpack.security.put_privileges #35679 @bizybot 
- [x] xpack.security.get_user_privileges #36292 @tvernum

### Security REST only API
- [x] authenticate * (highest prio) #33552 @albertzaharovits

## Miscellaneous API
- [x] xpack info * #31870 @nik9000
- [x] xpack usage ** #31975 @rjernst

## Graph API
- [x] graph explore ***  #32366 @markharwood

## Licensing API
- [x] put license ** #32214 @imotov
- [x] get license ** @imotov
- [x] delete license * #32586 @ywelsch
- [x] post start trial * #33406 @andyb-elastic
- [x] get trial status *  #33176 @javanna
- [x] post start basic * #33606 @vladimirdolzhenko
- [x] get basic status * #33176 @javanna

## Rollup
- [x] Create Job #33521 @tlrx
- [x] Delete Job #34066 @pcsanwald
- [x] Start Job #34623 (@cbuescher) 
- [x] Stop Job #34702 (@cbuescher)
- [x] Get Job * #33921 (@nik9000)
- [x] Get Rollup Capabilities #32880 (@polyfractal)
- [x] Get Rollup Index Capabilities #35102 * (@polyfractal)
- [x] Rollup Search ** #36334 (@nik9000)

## Migration
- [x] get Migration Assistance * #32744 @javanna 
- [x] upgrade Migration ** #34898 @pgomulka
- [x] get Deprecation Info * #36279 @hub-cap 

## Monitoring API
- ~~monitoring bulk~~

## Index Lifecycle Management
See https://github.com/elastic/elasticsearch/issues/33100

## CCR
See #33824", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/29827","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29827/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29827/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29827/events","html_url":"https://github.com/elastic/elasticsearch/issues/29827","id":317448378,"node_id":"MDU6SXNzdWUzMTc0NDgzNzg=","number":29827,"title":"Java high-level REST client completeness for xpack APIs","user":{"login":"elasticmachine","id":15837671,"node_id":"MDQ6VXNlcjE1ODM3Njcx","avatar_url":"https://avatars3.githubusercontent.com/u/15837671?v=4","gravatar_id":"","url":"https://api.github.com/users/elasticmachine","html_url":"https://github.com/elasticmachine","followers_url":"https://api.github.com/users/elasticmachine/followers","following_url":"https://api.github.com/users/elasticmachine/following{/other_user}","gists_url":"https://api.github.com/users/elasticmachine/gists{/gist_id}","starred_url":"https://api.github.com/users/elasticmachine/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elasticmachine/subscriptions","organizations_url":"https://api.github.com/users/elasticmachine/orgs","repos_url":"https://api.github.com/users/elasticmachine/repos","events_url":"https://api.github.com/users/elasticmachine/events{/privacy}","received_events_url":"https://api.github.com/users/elasticmachine/received_events","type":"User","site_admin":false},"labels":[{"id":493198109,"node_id":"MDU6TGFiZWw0OTMxOTgxMDk=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Features/Java%20High%20Level%20REST%20Client","name":":Core/Features/Java High Level REST Client","color":"0e8a16","default":false},{"id":158399402,"node_id":"MDU6TGFiZWwxNTgzOTk0MDI=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/Meta","name":"Meta","color":"e11d21","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":13,"created_at":"2017-11-02T17:37:40Z","updated_at":"2019-04-15T17:48:06Z","closed_at":"2019-04-15T17:48:06Z","author_association":"COLLABORATOR","body":"*Original comment by @javanna:*\r\n\r\nThis is the xpack side of https://github.com/elastic/elasticsearch/issues/27205 . This is a meta issue to track completeness of the Java REST high-level Client in terms of supported xpack API. The following list includes all the REST API that xpack exposes to date, and that are also exposed by the Transport Client. The ones marked as done are already supported by the high-level REST client, while the others need to be added. Every group is sorted based on an estimation around how important the API is, from more important to less important. Each API is also assigned a rank from 1 to 3 that expresses how difficult adding support for it is expected to be.\r\n\r\n*Please Note* The existing actions are in x-pack/ which is governed by the Elastic License, not the Apache License. Engineers who take these should strive to pull the Actions into Apache License controlled territory, so the high level rest client does not use any Elastic License bits. These should live in `x-pack/protocol`, which is OSS.\r\n\r\n*Please also note* The APIs themselves should follow [the spec](https://github.com/elastic/elasticsearch/tree/master/x-pack/plugin/src/test/resources/rest-api-spec/api) for how they are named. This means they should be in the XPackClient, not the HighLevelRestClient. This would mean you end up with `hlRestClient.xpack().watcher()` as an example\r\n\r\n*Please Please also also note* Prepend your commit msgs with `HLRC:`\r\n\r\n## Watcher API\r\n- [x] put watch * #32026 @spinscale \r\n- [x] get watch * #35531 @jimczi\r\n- [x] delete watch * #32337 @ywelsch\r\n- [x] execute watch * #35868 @romseygeek\r\n- [x] ack watch * #33962 @jtibshirani\r\n- [x] activate watch * #33988 @iverase\r\n- [x] deactivate watch * #34192 @not-napoleon\r\n- [x] watcher stats ** @imotov \r\n- [x] watcher stop * #34317 @jdconrad \r\n- [x] watcher start * #34317 @jdconrad  \r\n\r\n## Machine Learning API\r\n- [x] ml info action @vladimirdolzhenko #35777\r\n- [x] Create calendar ** (highest prio) @davidkyle #33362\r\n- [x] Delete calendar * @davidkyle #33775\r\n- [x] Add job to calendar * [ @benwtrent |  #35666 ]\r\n- [x] Delete job from calendar * [ @benwtrent | #35713 ]\r\n- [x] Add scheduled events to calendar * [ @benwtrent | #35704 ]\r\n- [x] Delete scheduled events from calendar *  [ @benwtrent | #35760 ]\r\n- [x] Get calendars * @davidkyle #33760\r\n- [x] Get scheduled events * [ @benwtrent | #35747 ]\r\n- [x] Create datafeed ** (highest prio) #33603 @dimitris-athanasiou\r\n- [x] Delete datafeed * #33667  @dimitris-athanasiou\r\n- [x] Start datafeed * [ @benwtrent | #33898 ]\r\n- [x] Stop datafeed * [ @benwtrent | #33946 ]\r\n- [x] Get datafeed info * #33715 @dimitris-athanasiou\r\n- [x] Get datafeed statistics ** [ @benwtrent | #34271 ]\r\n- [x] Preview datafeed * [ @benwtrent | #34284 ]\r\n- [x] Update datafeed * [ @benwtrent | #34882 ]\r\n- [x] Create job ** (highest prio) [ @droberts195, @benwtrent | #32726 ]\r\n- [x] Delete job * [ @benwtrent | #32820 ]\r\n- [x] Open job * [ @benwtrent | #32860 ]\r\n- [x] Close job * [ @benwtrent | #32943 ]\r\n- [x] Get job info * [ @benwtrent | #32960 ]\r\n- [x] Get job statistics ** [ @benwtrent | #33183 ]\r\n- [x] Flush job * [ @benwtrent | #33187]\r\n- [x] Post data to job ** [ @benwtrent | #33443 ]\r\n- [x] Update job * (done after put job API) [ @benwtrent | #33392 ]\r\n- [x] Forecast job *  [ @benwtrent | #33506 ]\r\n- [x] Delete Forecast *  [ @benwtrent | #33526 ]\r\n- [x] Delete model snapshot * @edsavage #35537 \r\n- [x] Get model snapshot info * @edsavage #35487 \r\n- [x] Revert model snapshot * @edsavage #35750\r\n- [x] Update model snapshot  ** @edsavage #35694 \r\n- [x] Get buckets * #33056 @dimitris-athanasiou\r\n- [x] Get overall buckets * #33297 @dimitris-athanasiou\r\n- [x] Get categories * #33465 @edsavage\r\n- [x] Get influencers * #33389 @dimitris-athanasiou\r\n- [x] Get records * #33085 @dimitris-athanasiou\r\n- [x] Find file structure * #35833 @droberts195\r\n- [x] Put filter * [ @benwtrent | #35175 ]\r\n- [x] Get filters * [ @benwtrent | #35502  ]\r\n- [x] Update filters * [ @benwtrent | #35522 ]\r\n- [x] Delete filters * #35382 @iverase \r\n\r\n### Machine Learning REST only API\r\n- [x] delete expired data * @edsavage #35906\r\n\r\n## Security API\r\n- [x] clear cache * #35163 @jaymode\r\n- [x] has privileges ** #35479 @tvernum \r\n- [x] put role ** #36209 @albertzaharovits\r\n- [x] get roles ** 35787 @jkakavas \r\n- [x] clear role cache * @bleskes, #34187\r\n- [x] delete role * #34620 @ywelsch - Has no API docs in the api ref\r\n- [x] create role mapping ** #34171 @bizybot\r\n- [x] modify role mapping ** #34171 @bizybot\r\n- [x] delete role mapping * #34531 @bizybot\r\n- [x] get role mappings ** #34637 @bizybot\r\n- [x] get ssl certificates ** #34135 @jkakavas \r\n- [x] post create token * #34791 @tvernum\r\n- [x] delete invalidate token * #35114 @tvernum\r\n- [x] put user ** (highest prio) #32332 @jaymode\r\n- [x] get users ** #36332 @nknize\r\n- [x] delete user * #35294 @iverase \r\n- [x] disable user #33481 @jaymode\r\n- [x] enable user #33481 @jaymode\r\n- [x] change password #33509 * @jkakavas\r\n- [x] xpack.security.get_privileges #35556 @jkakavas \r\n- [x] xpack.security.delete_privileges. #35454 @tlrx \r\n- [x] xpack.security.put_privileges #35679 @bizybot \r\n- [x] xpack.security.get_user_privileges #36292 @tvernum\r\n\r\n### Security REST only API\r\n- [x] authenticate * (highest prio) #33552 @albertzaharovits\r\n\r\n## Miscellaneous API\r\n- [x] xpack info * #31870 @nik9000\r\n- [x] xpack usage ** #31975 @rjernst\r\n\r\n## Graph API\r\n- [x] graph explore ***  #32366 @markharwood\r\n\r\n## Licensing API\r\n- [x] put license ** #32214 @imotov\r\n- [x] get license ** @imotov\r\n- [x] delete license * #32586 @ywelsch\r\n- [x] post start trial * #33406 @andyb-elastic\r\n- [x] get trial status *  #33176 @javanna\r\n- [x] post start basic * #33606 @vladimirdolzhenko\r\n- [x] get basic status * #33176 @javanna\r\n\r\n## Rollup\r\n- [x] Create Job #33521 @tlrx\r\n- [x] Delete Job #34066 @pcsanwald\r\n- [x] Start Job #34623 (@cbuescher) \r\n- [x] Stop Job #34702 (@cbuescher)\r\n- [x] Get Job * #33921 (@nik9000)\r\n- [x] Get Rollup Capabilities #32880 (@polyfractal)\r\n- [x] Get Rollup Index Capabilities #35102 * (@polyfractal)\r\n- [x] Rollup Search ** #36334 (@nik9000)\r\n\r\n## Migration\r\n- [x] get Migration Assistance * #32744 @javanna \r\n- [x] upgrade Migration ** #34898 @pgomulka\r\n- [x] get Deprecation Info * #36279 @hub-cap \r\n\r\n## Monitoring API\r\n- ~~monitoring bulk~~\r\n\r\n## Index Lifecycle Management\r\nSee https://github.com/elastic/elasticsearch/issues/33100\r\n\r\n## CCR\r\nSee #33824","closed_by":{"login":"hub-cap","id":613352,"node_id":"MDQ6VXNlcjYxMzM1Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/613352?v=4","gravatar_id":"","url":"https://api.github.com/users/hub-cap","html_url":"https://github.com/hub-cap","followers_url":"https://api.github.com/users/hub-cap/followers","following_url":"https://api.github.com/users/hub-cap/following{/other_user}","gists_url":"https://api.github.com/users/hub-cap/gists{/gist_id}","starred_url":"https://api.github.com/users/hub-cap/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hub-cap/subscriptions","organizations_url":"https://api.github.com/users/hub-cap/orgs","repos_url":"https://api.github.com/users/hub-cap/repos","events_url":"https://api.github.com/users/hub-cap/events{/privacy}","received_events_url":"https://api.github.com/users/hub-cap/received_events","type":"User","site_admin":false}}", "commentIds":["384130883","384130884","384130885","384130886","384130887","384130888","384130889","403555284","412535920","412547459","412548228","416245526","417342005"], "labels":[":Core/Features/Java High Level REST Client","Meta"]}