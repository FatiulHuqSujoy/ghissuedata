{"id":"31478", "title":"ElasticSearch 6.3 - Binding to Docker0 instead of eth0", "body":"**Elasticsearch version 6.3.0** 

**Plugins installed**: None

**JVM version** (`java -version`):
```
java version \"1.8.0_65\"
Java(TM) SE Runtime Environment (build 1.8.0_65-b17)
Java HotSpot(TM) Client VM (build 25.65-b01, mixed mode)
```

**OS version** (`uname -a` if on a Unix-like system):
```
Linux rpi-1 4.14.44-v7+ #1117 SMP Thu May 31 16:57:56 BST 2018 armv7l GNU/Linux
```

**Description of the problem including expected versus actual behavior**:
ES is trying to bind to docker0's IP, not the machines IP
```
[2018-06-20T12:21:14,679][INFO ][o.e.t.TransportService   ] [rpi-1] publish_address {172.17.0.1:9300}, bound_addresses {0.0.0.0:9300}
[2018-06-20T12:21:14,752][INFO ][o.e.b.BootstrapChecks    ] [rpi-1] bound or publishing to a non-loopback address, enforcing bootstrap checks
ERROR: [1] bootstrap checks failed
```
I tried this outside of docker and installed ES directly on the host to see why the cluster is not joining.
Here is my config:
```
xpack.ml.enabled: false
node.name:  rpi-1
discovery.zen.ping.unicast.hosts: ['live', 'rpi-1', 'rpi-3', 'rpi-4']
http.cors.enabled: true
http.cors.allow-origin: \"*\"
node.master: true
node.data: true
http.port: 9201
network.host: 0.0.0.0
network.bind_host: 0.0.0.0
```
Specifiying an IP instead of 0.0.0.0 results in the same issue

On an x86 box, my config is the following and everything works fine:
```
network.host:  761db145dc8d
node.name:  761db145dc8d
discovery.zen.ping.unicast.hosts: [ 192.168.0.108, 192.168.0.103, 192.168.0.104]
http.cors.enabled: true
http.cors.allow-origin: \"*\"
node.master: true
network.publish_host:  192.168.0.108
http.port:  9201
```

When copying the config over to the arm node, I get this error:
```
[2018-06-20T12:29:39,436][INFO ][o.e.d.DiscoveryModule    ] [rpi-1] using discovery type [zen]
[2018-06-20T12:29:44,744][INFO ][o.e.n.Node               ] [rpi-1] initialized
[2018-06-20T12:29:44,745][INFO ][o.e.n.Node               ] [rpi-1] starting ...
[2018-06-20T12:29:45,467][INFO ][o.e.t.TransportService   ] [rpi-1] publish_address {192.168.0.101:9300}, bound_addresses {192.168.0.101:9300}
[2018-06-20T12:29:45,548][INFO ][o.e.b.BootstrapChecks    ] [rpi-1] bound or publishing to a non-loopback address, enforcing bootstrap checks
ERROR: [1] bootstrap checks failed
```
I have never had any issues with ES 6.1.0 and prior on ARMv7, only 6.2.4 and 6.3.0. ", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/31478","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31478/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31478/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31478/events","html_url":"https://github.com/elastic/elasticsearch/issues/31478","id":334216749,"node_id":"MDU6SXNzdWUzMzQyMTY3NDk=","number":31478,"title":"ElasticSearch 6.3 - Binding to Docker0 instead of eth0","user":{"login":"rusher81572","id":5395507,"node_id":"MDQ6VXNlcjUzOTU1MDc=","avatar_url":"https://avatars2.githubusercontent.com/u/5395507?v=4","gravatar_id":"","url":"https://api.github.com/users/rusher81572","html_url":"https://github.com/rusher81572","followers_url":"https://api.github.com/users/rusher81572/followers","following_url":"https://api.github.com/users/rusher81572/following{/other_user}","gists_url":"https://api.github.com/users/rusher81572/gists{/gist_id}","starred_url":"https://api.github.com/users/rusher81572/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/rusher81572/subscriptions","organizations_url":"https://api.github.com/users/rusher81572/orgs","repos_url":"https://api.github.com/users/rusher81572/repos","events_url":"https://api.github.com/users/rusher81572/events{/privacy}","received_events_url":"https://api.github.com/users/rusher81572/received_events","type":"User","site_admin":false},"labels":[{"id":114977275,"node_id":"MDU6TGFiZWwxMTQ5NzcyNzU=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Infra/Packaging","name":":Core/Infra/Packaging","color":"0e8a16","default":false}],"state":"closed","locked":false,"assignee":{"login":"hub-cap","id":613352,"node_id":"MDQ6VXNlcjYxMzM1Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/613352?v=4","gravatar_id":"","url":"https://api.github.com/users/hub-cap","html_url":"https://github.com/hub-cap","followers_url":"https://api.github.com/users/hub-cap/followers","following_url":"https://api.github.com/users/hub-cap/following{/other_user}","gists_url":"https://api.github.com/users/hub-cap/gists{/gist_id}","starred_url":"https://api.github.com/users/hub-cap/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hub-cap/subscriptions","organizations_url":"https://api.github.com/users/hub-cap/orgs","repos_url":"https://api.github.com/users/hub-cap/repos","events_url":"https://api.github.com/users/hub-cap/events{/privacy}","received_events_url":"https://api.github.com/users/hub-cap/received_events","type":"User","site_admin":false},"assignees":[{"login":"hub-cap","id":613352,"node_id":"MDQ6VXNlcjYxMzM1Mg==","avatar_url":"https://avatars2.githubusercontent.com/u/613352?v=4","gravatar_id":"","url":"https://api.github.com/users/hub-cap","html_url":"https://github.com/hub-cap","followers_url":"https://api.github.com/users/hub-cap/followers","following_url":"https://api.github.com/users/hub-cap/following{/other_user}","gists_url":"https://api.github.com/users/hub-cap/gists{/gist_id}","starred_url":"https://api.github.com/users/hub-cap/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/hub-cap/subscriptions","organizations_url":"https://api.github.com/users/hub-cap/orgs","repos_url":"https://api.github.com/users/hub-cap/repos","events_url":"https://api.github.com/users/hub-cap/events{/privacy}","received_events_url":"https://api.github.com/users/hub-cap/received_events","type":"User","site_admin":false}],"milestone":null,"comments":4,"created_at":"2018-06-20T19:25:42Z","updated_at":"2018-06-25T16:14:43Z","closed_at":"2018-06-25T16:14:43Z","author_association":"NONE","body":"**Elasticsearch version 6.3.0** \r\n\r\n**Plugins installed**: None\r\n\r\n**JVM version** (`java -version`):\r\n```\r\njava version \"1.8.0_65\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_65-b17)\r\nJava HotSpot(TM) Client VM (build 25.65-b01, mixed mode)\r\n```\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\n```\r\nLinux rpi-1 4.14.44-v7+ #1117 SMP Thu May 31 16:57:56 BST 2018 armv7l GNU/Linux\r\n```\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nES is trying to bind to docker0's IP, not the machines IP\r\n```\r\n[2018-06-20T12:21:14,679][INFO ][o.e.t.TransportService   ] [rpi-1] publish_address {172.17.0.1:9300}, bound_addresses {0.0.0.0:9300}\r\n[2018-06-20T12:21:14,752][INFO ][o.e.b.BootstrapChecks    ] [rpi-1] bound or publishing to a non-loopback address, enforcing bootstrap checks\r\nERROR: [1] bootstrap checks failed\r\n```\r\nI tried this outside of docker and installed ES directly on the host to see why the cluster is not joining.\r\nHere is my config:\r\n```\r\nxpack.ml.enabled: false\r\nnode.name:  rpi-1\r\ndiscovery.zen.ping.unicast.hosts: ['live', 'rpi-1', 'rpi-3', 'rpi-4']\r\nhttp.cors.enabled: true\r\nhttp.cors.allow-origin: \"*\"\r\nnode.master: true\r\nnode.data: true\r\nhttp.port: 9201\r\nnetwork.host: 0.0.0.0\r\nnetwork.bind_host: 0.0.0.0\r\n```\r\nSpecifiying an IP instead of 0.0.0.0 results in the same issue\r\n\r\nOn an x86 box, my config is the following and everything works fine:\r\n```\r\nnetwork.host:  761db145dc8d\r\nnode.name:  761db145dc8d\r\ndiscovery.zen.ping.unicast.hosts: [ 192.168.0.108, 192.168.0.103, 192.168.0.104]\r\nhttp.cors.enabled: true\r\nhttp.cors.allow-origin: \"*\"\r\nnode.master: true\r\nnetwork.publish_host:  192.168.0.108\r\nhttp.port:  9201\r\n```\r\n\r\nWhen copying the config over to the arm node, I get this error:\r\n```\r\n[2018-06-20T12:29:39,436][INFO ][o.e.d.DiscoveryModule    ] [rpi-1] using discovery type [zen]\r\n[2018-06-20T12:29:44,744][INFO ][o.e.n.Node               ] [rpi-1] initialized\r\n[2018-06-20T12:29:44,745][INFO ][o.e.n.Node               ] [rpi-1] starting ...\r\n[2018-06-20T12:29:45,467][INFO ][o.e.t.TransportService   ] [rpi-1] publish_address {192.168.0.101:9300}, bound_addresses {192.168.0.101:9300}\r\n[2018-06-20T12:29:45,548][INFO ][o.e.b.BootstrapChecks    ] [rpi-1] bound or publishing to a non-loopback address, enforcing bootstrap checks\r\nERROR: [1] bootstrap checks failed\r\n```\r\nI have never had any issues with ES 6.1.0 and prior on ARMv7, only 6.2.4 and 6.3.0. ","closed_by":{"login":"jasontedor","id":4744941,"node_id":"MDQ6VXNlcjQ3NDQ5NDE=","avatar_url":"https://avatars3.githubusercontent.com/u/4744941?v=4","gravatar_id":"","url":"https://api.github.com/users/jasontedor","html_url":"https://github.com/jasontedor","followers_url":"https://api.github.com/users/jasontedor/followers","following_url":"https://api.github.com/users/jasontedor/following{/other_user}","gists_url":"https://api.github.com/users/jasontedor/gists{/gist_id}","starred_url":"https://api.github.com/users/jasontedor/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jasontedor/subscriptions","organizations_url":"https://api.github.com/users/jasontedor/orgs","repos_url":"https://api.github.com/users/jasontedor/repos","events_url":"https://api.github.com/users/jasontedor/events{/privacy}","received_events_url":"https://api.github.com/users/jasontedor/received_events","type":"User","site_admin":false}}", "commentIds":["399069804","399242520","399243854","400009033"], "labels":[":Core/Infra/Packaging"]}