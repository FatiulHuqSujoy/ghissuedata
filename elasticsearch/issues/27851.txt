{"id":"27851", "title":"bengali_normalization is not found", "body":"**Elasticsearch version** (`bin/elasticsearch --version`): 6.0.1

**Plugins installed**: []

**JVM version** (`java -version`): Docker image docker.elastic.co/elasticsearch/elasticsearch:6.0.1

**OS version** (`uname -a` if on a Unix-like system): Docker image docker.elastic.co/elasticsearch/elasticsearch:6.0.1

**Description of the problem including expected versus actual behavior**:
Cannot use bengali_normalization filter.



**Steps to reproduce**:
```
PUT doc_bn
{
  \"settings\": {
    \"number_of_shards\": 1,
    \"number_of_replicas\": 0,
    \"refresh_interval\": \"10s\",
    \"analysis\": {
      \"filter\": {
        \"my_stop\": {
          \"type\": \"stop\",
          \"stopwords\": \"_bengali_\"
        },
        \"my_stemmer\": {
          \"type\": \"stemmer\",
          \"language\": \"bengali\"
        }
      },
      \"analyzer\": {
        \"my_html\": {
          \"tokenizer\": \"standard\",
          \"char_filter\": [
            \"html_strip\"
          ],
          \"filter\": [
            \"lowercase\",
            \"indic_normalization\",
            \"bengali_normalization\",
            \"my_stop\",
            \"my_stemmer\"
          ]
        }
    }
  },
  \"mappings\": {
    ...
  }
}

```
The result is:
Custom Analyzer [my_html] failed to find filter under name [bengali_normalization]
It should be available as mentioned in this link: https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html#bengali-analyzer

https://discuss.elastic.co/t/bengali-normalization-not-found/112113/3
", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/27851","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27851/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27851/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/27851/events","html_url":"https://github.com/elastic/elasticsearch/issues/27851","id":282696614,"node_id":"MDU6SXNzdWUyODI2OTY2MTQ=","number":27851,"title":"bengali_normalization is not found","user":{"login":"amerzad","id":12209863,"node_id":"MDQ6VXNlcjEyMjA5ODYz","avatar_url":"https://avatars2.githubusercontent.com/u/12209863?v=4","gravatar_id":"","url":"https://api.github.com/users/amerzad","html_url":"https://github.com/amerzad","followers_url":"https://api.github.com/users/amerzad/followers","following_url":"https://api.github.com/users/amerzad/following{/other_user}","gists_url":"https://api.github.com/users/amerzad/gists{/gist_id}","starred_url":"https://api.github.com/users/amerzad/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/amerzad/subscriptions","organizations_url":"https://api.github.com/users/amerzad/orgs","repos_url":"https://api.github.com/users/amerzad/repos","events_url":"https://api.github.com/users/amerzad/events{/privacy}","received_events_url":"https://api.github.com/users/amerzad/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2017-12-17T14:15:02Z","updated_at":"2017-12-18T09:35:45Z","closed_at":"2017-12-18T09:06:41Z","author_association":"NONE","body":"**Elasticsearch version** (`bin/elasticsearch --version`): 6.0.1\r\n\r\n**Plugins installed**: []\r\n\r\n**JVM version** (`java -version`): Docker image docker.elastic.co/elasticsearch/elasticsearch:6.0.1\r\n\r\n**OS version** (`uname -a` if on a Unix-like system): Docker image docker.elastic.co/elasticsearch/elasticsearch:6.0.1\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nCannot use bengali_normalization filter.\r\n\r\n\r\n\r\n**Steps to reproduce**:\r\n```\r\nPUT doc_bn\r\n{\r\n  \"settings\": {\r\n    \"number_of_shards\": 1,\r\n    \"number_of_replicas\": 0,\r\n    \"refresh_interval\": \"10s\",\r\n    \"analysis\": {\r\n      \"filter\": {\r\n        \"my_stop\": {\r\n          \"type\": \"stop\",\r\n          \"stopwords\": \"_bengali_\"\r\n        },\r\n        \"my_stemmer\": {\r\n          \"type\": \"stemmer\",\r\n          \"language\": \"bengali\"\r\n        }\r\n      },\r\n      \"analyzer\": {\r\n        \"my_html\": {\r\n          \"tokenizer\": \"standard\",\r\n          \"char_filter\": [\r\n            \"html_strip\"\r\n          ],\r\n          \"filter\": [\r\n            \"lowercase\",\r\n            \"indic_normalization\",\r\n            \"bengali_normalization\",\r\n            \"my_stop\",\r\n            \"my_stemmer\"\r\n          ]\r\n        }\r\n    }\r\n  },\r\n  \"mappings\": {\r\n    ...\r\n  }\r\n}\r\n\r\n```\r\nThe result is:\r\nCustom Analyzer [my_html] failed to find filter under name [bengali_normalization]\r\nIt should be available as mentioned in this link: https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html#bengali-analyzer\r\n\r\nhttps://discuss.elastic.co/t/bengali-normalization-not-found/112113/3\r\n","closed_by":{"login":"colings86","id":236731,"node_id":"MDQ6VXNlcjIzNjczMQ==","avatar_url":"https://avatars0.githubusercontent.com/u/236731?v=4","gravatar_id":"","url":"https://api.github.com/users/colings86","html_url":"https://github.com/colings86","followers_url":"https://api.github.com/users/colings86/followers","following_url":"https://api.github.com/users/colings86/following{/other_user}","gists_url":"https://api.github.com/users/colings86/gists{/gist_id}","starred_url":"https://api.github.com/users/colings86/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/colings86/subscriptions","organizations_url":"https://api.github.com/users/colings86/orgs","repos_url":"https://api.github.com/users/colings86/repos","events_url":"https://api.github.com/users/colings86/events{/privacy}","received_events_url":"https://api.github.com/users/colings86/received_events","type":"User","site_admin":false}}", "commentIds":["352366004","352373103"], "labels":[]}