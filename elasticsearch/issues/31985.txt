{"id":"31985", "title":"Java high-level REST client  support mdc", "body":"hi,  sorry to bother you 
 in my work when i use this client , i met some difficulties , i want the log in in the client  , just the log in the below code , i want it support MDC . but i can not find the way .

is in the ``` org.elasticsearch.client.RestClient```
```

    private void performRequestAsync(final long startTime, final HostTuple<Iterator<HttpHost>> hostTuple, final HttpRequestBase request,
                                     final Set<Integer> ignoreErrorCodes,
                                     final HttpAsyncResponseConsumerFactory httpAsyncResponseConsumerFactory,
                                     final FailureTrackingResponseListener listener) {
        final HttpHost host = hostTuple.hosts.next();
       ...
        client.execute(requestProducer, asyncResponseConsumer, context, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse httpResponse) {
                try {
                    RequestLogger.logResponse(logger, request, host, httpResponse);//   i want the log of this  with MDC 
                 ........
                } catch(Exception e) {
                    listener.onDefinitiveFailure(e);
                }
            }
```
i use slf4j+logback and i want the code like this behind  to support slf4-MDC

```
  String sessionId = MDC.get(\"SessionId\");//get sessionID from current thread 
  client.execute(requestProducer, asyncResponseConsumer, context, new FutureCallback<HttpResponse>() {
            @Override
            public void completed(HttpResponse httpResponse) {
                try {
                  MDC.put(\"SessionId\", sessionId);  // put the sessionId to the thread-pool thread so that logback logger can find it .
                    RequestLogger.logResponse(logger, request, host, httpResponse);//   i want the log of this  with MDC 
                 ........
                } catch(Exception e) {
                    listener.onDefinitiveFailure(e);
                }
            }
```

## the way i use the client 
create:
```

    @Bean
    public RestHighLevelClient esOriginClient() {
        List<String> hostList = Arrays.asList(host.split(\",\"));
        HttpHost[] httpHosts = new HttpHost[hostList.size()];
        for (int i = 0; i < hostList.size(); i++) {
            httpHosts[i] = new HttpHost(hostList.get(i), port, \"http\");
        }
        RestClientBuilder builder = RestClient.builder(httpHosts);
        return new RestHighLevelClient(builder);
    }


```
use:
```
 SearchResponse searchResponse = client.search(request);
```
just the method in ```org.elasticsearch.client.RestHighLevelClient```
```

  public final SearchResponse search(SearchRequest searchRequest, Header... headers) throws IOException {
        return performRequestAndParseEntity(searchRequest, Request::search, SearchResponse::fromXContent, emptySet(), headers);
    }

```

## the way i have try to support mdc 
1. myRestClient  extends RestClient  but i find the RestClient cannot be extended because of the constructor is not public 
2. i try to extend ```org.elasticsearch.client.RequestLogger``` but i find it is final ,and alos the method is static .

so i want is there any way can help me? sorry for bother you .













", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/31985","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31985/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31985/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/31985/events","html_url":"https://github.com/elastic/elasticsearch/issues/31985","id":340508754,"node_id":"MDU6SXNzdWUzNDA1MDg3NTQ=","number":31985,"title":"Java high-level REST client  support mdc","user":{"login":"chenchuangc","id":13183268,"node_id":"MDQ6VXNlcjEzMTgzMjY4","avatar_url":"https://avatars2.githubusercontent.com/u/13183268?v=4","gravatar_id":"","url":"https://api.github.com/users/chenchuangc","html_url":"https://github.com/chenchuangc","followers_url":"https://api.github.com/users/chenchuangc/followers","following_url":"https://api.github.com/users/chenchuangc/following{/other_user}","gists_url":"https://api.github.com/users/chenchuangc/gists{/gist_id}","starred_url":"https://api.github.com/users/chenchuangc/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/chenchuangc/subscriptions","organizations_url":"https://api.github.com/users/chenchuangc/orgs","repos_url":"https://api.github.com/users/chenchuangc/repos","events_url":"https://api.github.com/users/chenchuangc/events{/privacy}","received_events_url":"https://api.github.com/users/chenchuangc/received_events","type":"User","site_admin":false},"labels":[{"id":493198109,"node_id":"MDU6TGFiZWw0OTMxOTgxMDk=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Core/Features/Java%20High%20Level%20REST%20Client","name":":Core/Features/Java High Level REST Client","color":"0e8a16","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":5,"created_at":"2018-07-12T06:45:27Z","updated_at":"2018-08-31T07:37:18Z","closed_at":"2018-08-31T07:37:18Z","author_association":"NONE","body":"hi,  sorry to bother you \r\n in my work when i use this client , i met some difficulties , i want the log in in the client  , just the log in the below code , i want it support MDC . but i can not find the way .\r\n\r\nis in the ``` org.elasticsearch.client.RestClient```\r\n```\r\n\r\n    private void performRequestAsync(final long startTime, final HostTuple<Iterator<HttpHost>> hostTuple, final HttpRequestBase request,\r\n                                     final Set<Integer> ignoreErrorCodes,\r\n                                     final HttpAsyncResponseConsumerFactory httpAsyncResponseConsumerFactory,\r\n                                     final FailureTrackingResponseListener listener) {\r\n        final HttpHost host = hostTuple.hosts.next();\r\n       ...\r\n        client.execute(requestProducer, asyncResponseConsumer, context, new FutureCallback<HttpResponse>() {\r\n            @Override\r\n            public void completed(HttpResponse httpResponse) {\r\n                try {\r\n                    RequestLogger.logResponse(logger, request, host, httpResponse);//   i want the log of this  with MDC \r\n                 ........\r\n                } catch(Exception e) {\r\n                    listener.onDefinitiveFailure(e);\r\n                }\r\n            }\r\n```\r\ni use slf4j+logback and i want the code like this behind  to support slf4-MDC\r\n\r\n```\r\n  String sessionId = MDC.get(\"SessionId\");//get sessionID from current thread \r\n  client.execute(requestProducer, asyncResponseConsumer, context, new FutureCallback<HttpResponse>() {\r\n            @Override\r\n            public void completed(HttpResponse httpResponse) {\r\n                try {\r\n                  MDC.put(\"SessionId\", sessionId);  // put the sessionId to the thread-pool thread so that logback logger can find it .\r\n                    RequestLogger.logResponse(logger, request, host, httpResponse);//   i want the log of this  with MDC \r\n                 ........\r\n                } catch(Exception e) {\r\n                    listener.onDefinitiveFailure(e);\r\n                }\r\n            }\r\n```\r\n\r\n## the way i use the client \r\ncreate:\r\n```\r\n\r\n    @Bean\r\n    public RestHighLevelClient esOriginClient() {\r\n        List<String> hostList = Arrays.asList(host.split(\",\"));\r\n        HttpHost[] httpHosts = new HttpHost[hostList.size()];\r\n        for (int i = 0; i < hostList.size(); i++) {\r\n            httpHosts[i] = new HttpHost(hostList.get(i), port, \"http\");\r\n        }\r\n        RestClientBuilder builder = RestClient.builder(httpHosts);\r\n        return new RestHighLevelClient(builder);\r\n    }\r\n\r\n\r\n```\r\nuse:\r\n```\r\n SearchResponse searchResponse = client.search(request);\r\n```\r\njust the method in ```org.elasticsearch.client.RestHighLevelClient```\r\n```\r\n\r\n  public final SearchResponse search(SearchRequest searchRequest, Header... headers) throws IOException {\r\n        return performRequestAndParseEntity(searchRequest, Request::search, SearchResponse::fromXContent, emptySet(), headers);\r\n    }\r\n\r\n```\r\n\r\n## the way i have try to support mdc \r\n1. myRestClient  extends RestClient  but i find the RestClient cannot be extended because of the constructor is not public \r\n2. i try to extend ```org.elasticsearch.client.RequestLogger``` but i find it is final ,and alos the method is static .\r\n\r\nso i want is there any way can help me? sorry for bother you .\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n","closed_by":{"login":"chenchuangc","id":13183268,"node_id":"MDQ6VXNlcjEzMTgzMjY4","avatar_url":"https://avatars2.githubusercontent.com/u/13183268?v=4","gravatar_id":"","url":"https://api.github.com/users/chenchuangc","html_url":"https://github.com/chenchuangc","followers_url":"https://api.github.com/users/chenchuangc/followers","following_url":"https://api.github.com/users/chenchuangc/following{/other_user}","gists_url":"https://api.github.com/users/chenchuangc/gists{/gist_id}","starred_url":"https://api.github.com/users/chenchuangc/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/chenchuangc/subscriptions","organizations_url":"https://api.github.com/users/chenchuangc/orgs","repos_url":"https://api.github.com/users/chenchuangc/repos","events_url":"https://api.github.com/users/chenchuangc/events{/privacy}","received_events_url":"https://api.github.com/users/chenchuangc/received_events","type":"User","site_admin":false}}", "commentIds":["404705652","404706277","406547515","406629459","417579680"], "labels":[":Core/Features/Java High Level REST Client"]}