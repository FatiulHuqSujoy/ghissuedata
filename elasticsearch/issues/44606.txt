{"id":"44606", "title":"Why I got error - function score query returned an invalid score (negative score)?", "body":"<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`):
```
Version: 7.1.0, Build: default/tar/606a173/2019-05-16T00:43:15.323135Z, JVM: 1.8.0_112
```
**Plugins installed**: []
- `analysis-nori`

**JVM version** (`java -version`):
```
java version \"1.8.0_112\"
Java(TM) SE Runtime Environment (build 1.8.0_112-b16)
Java HotSpot(TM) 64-Bit Server VM (build 25.112-b16, mixed mode)
```

**OS version** (`uname -a` if on a Unix-like system):
- Mac OS high sierra 10.13.2(17C88)

**Description of the problem including expected versus actual behavior**:

I have below like query and elasticsearch complain with exception 

```
function score query returned an invalid score: -1.3092874 for doc: 6
```
but I have filtered with age with 1, so I don't understand negative score.
(and all result docs age is 1)


```
{
  \"query\": {
    \"function_score\": {
      \"query\": {
        \"bool\": {
          \"must\": [
            {
              \"multi_match\": {
                \"query\": \"hello\",
                \"type\": \"cross_fields\",
                \"fields\": [
                  \"introduction.text^3\"                  
                ],
                \"slop\": 10
              }
            }
          ],
          \"filter\": [
            {
              \"range\": {
                \"age\": {
                  \"gte\": 1
                }
              }
            },
            {
              \"range\": {
                \"age\": {
                  \"lte\": 1
                }
              }
            }
          ]
        }
      },
      \"boost\": 5,
      \"score_mode\": \"multiply\",
      \"boost_mode\": \"multiply\",
      \"functions\": [
        {
          \"filter\": {
            \"range\": {
              \"age\": {
                \"gte\": 1
              }
            }
          },
          \"weight\": 1.2
        }
      ]
    }
  },
  \"from\": 0,
  \"size\": 30,
  \"sort\": [
    \"_score\"
  ]
}
```", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/44606","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/44606/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/44606/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/44606/events","html_url":"https://github.com/elastic/elasticsearch/issues/44606","id":470107079,"node_id":"MDU6SXNzdWU0NzAxMDcwNzk=","number":44606,"title":"Why I got error - function score query returned an invalid score (negative score)?","user":{"login":"HanguChoi","id":12369225,"node_id":"MDQ6VXNlcjEyMzY5MjI1","avatar_url":"https://avatars2.githubusercontent.com/u/12369225?v=4","gravatar_id":"","url":"https://api.github.com/users/HanguChoi","html_url":"https://github.com/HanguChoi","followers_url":"https://api.github.com/users/HanguChoi/followers","following_url":"https://api.github.com/users/HanguChoi/following{/other_user}","gists_url":"https://api.github.com/users/HanguChoi/gists{/gist_id}","starred_url":"https://api.github.com/users/HanguChoi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/HanguChoi/subscriptions","organizations_url":"https://api.github.com/users/HanguChoi/orgs","repos_url":"https://api.github.com/users/HanguChoi/repos","events_url":"https://api.github.com/users/HanguChoi/events{/privacy}","received_events_url":"https://api.github.com/users/HanguChoi/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":2,"created_at":"2019-07-19T02:28:55Z","updated_at":"2019-07-19T06:22:28Z","closed_at":"2019-07-19T03:33:25Z","author_association":"NONE","body":"<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`):\r\n```\r\nVersion: 7.1.0, Build: default/tar/606a173/2019-05-16T00:43:15.323135Z, JVM: 1.8.0_112\r\n```\r\n**Plugins installed**: []\r\n- `analysis-nori`\r\n\r\n**JVM version** (`java -version`):\r\n```\r\njava version \"1.8.0_112\"\r\nJava(TM) SE Runtime Environment (build 1.8.0_112-b16)\r\nJava HotSpot(TM) 64-Bit Server VM (build 25.112-b16, mixed mode)\r\n```\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\n- Mac OS high sierra 10.13.2(17C88)\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\nI have below like query and elasticsearch complain with exception \r\n\r\n```\r\nfunction score query returned an invalid score: -1.3092874 for doc: 6\r\n```\r\nbut I have filtered with age with 1, so I don't understand negative score.\r\n(and all result docs age is 1)\r\n\r\n\r\n```\r\n{\r\n  \"query\": {\r\n    \"function_score\": {\r\n      \"query\": {\r\n        \"bool\": {\r\n          \"must\": [\r\n            {\r\n              \"multi_match\": {\r\n                \"query\": \"hello\",\r\n                \"type\": \"cross_fields\",\r\n                \"fields\": [\r\n                  \"introduction.text^3\"                  \r\n                ],\r\n                \"slop\": 10\r\n              }\r\n            }\r\n          ],\r\n          \"filter\": [\r\n            {\r\n              \"range\": {\r\n                \"age\": {\r\n                  \"gte\": 1\r\n                }\r\n              }\r\n            },\r\n            {\r\n              \"range\": {\r\n                \"age\": {\r\n                  \"lte\": 1\r\n                }\r\n              }\r\n            }\r\n          ]\r\n        }\r\n      },\r\n      \"boost\": 5,\r\n      \"score_mode\": \"multiply\",\r\n      \"boost_mode\": \"multiply\",\r\n      \"functions\": [\r\n        {\r\n          \"filter\": {\r\n            \"range\": {\r\n              \"age\": {\r\n                \"gte\": 1\r\n              }\r\n            }\r\n          },\r\n          \"weight\": 1.2\r\n        }\r\n      ]\r\n    }\r\n  },\r\n  \"from\": 0,\r\n  \"size\": 30,\r\n  \"sort\": [\r\n    \"_score\"\r\n  ]\r\n}\r\n```","closed_by":{"login":"HanguChoi","id":12369225,"node_id":"MDQ6VXNlcjEyMzY5MjI1","avatar_url":"https://avatars2.githubusercontent.com/u/12369225?v=4","gravatar_id":"","url":"https://api.github.com/users/HanguChoi","html_url":"https://github.com/HanguChoi","followers_url":"https://api.github.com/users/HanguChoi/followers","following_url":"https://api.github.com/users/HanguChoi/following{/other_user}","gists_url":"https://api.github.com/users/HanguChoi/gists{/gist_id}","starred_url":"https://api.github.com/users/HanguChoi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/HanguChoi/subscriptions","organizations_url":"https://api.github.com/users/HanguChoi/orgs","repos_url":"https://api.github.com/users/HanguChoi/repos","events_url":"https://api.github.com/users/HanguChoi/events{/privacy}","received_events_url":"https://api.github.com/users/HanguChoi/received_events","type":"User","site_admin":false}}", "commentIds":["513077545","513107547"], "labels":[]}