{"id":"30986", "title":"Query starting with - fails after building mapping", "body":"**Elasticsearch version** (`bin/elasticsearch --version`):

Docker Elastic `docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4`

**OS version** (`uname -a` if on a Unix-like system):

Linux SurfaceBook2 4.4.0-17134-Microsoft #48-Microsoft Fri Apr 27 18:06:00 PST 2018 x86_64 x86_64 x86_64 GNU/Linux

Running WSL on Windows, but verified on Ubuntu 16:04 system as well.

**Description of the problem including expected versus actual behavior**:

When doing a search for `--1` without adding mapping the search works as expected. When the mapping is applied a search for `--1` will throw an exception. This seems to apply to any search term that starts with `-`

**Steps to reproduce**:
1. 

```
docker run -p 9200:9200 -p 9300:9300 -e \"discovery.type=single-node\" -d docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4
```

2.

```
GET http://localhost:9200/_search?q=--1

RESPONSE
{
    \"took\": 6,
    \"timed_out\": false,
    \"_shards\": {
        \"total\": 0,
        \"successful\": 0,
        \"skipped\": 0,
        \"failed\": 0
    },
    \"hits\": {
        \"total\": 0,
        \"max_score\": 0,
        \"hits\": []
    }
}
```

3. 

```
PUT http://localhost:9200/metadata/
{
  \"mappings\": {
    \"meta\": { 
      \"properties\": { 
        \"asset.format\": { 
          \"type\": \"keyword\"
        },
        \"firstPublishDate\": {
          \"type\": \"date\",
          \"format\": \"yyyy-MM-dd\",
          \"ignore_malformed\": true
        },
        \"recordDate\": {
          \"type\": \"date\",
          \"format\": \"yyyy-MM-dd\",
          \"ignore_malformed\": true
        },
        \"status.type\": {
          \"type\": \"keyword\"
        },
        \"status.digitised\": {
          \"type\": \"keyword\"
        }
      }
    }
  }
}

RESPONSE
{
    \"acknowledged\": true,
    \"shards_acknowledged\": true,
    \"index\": \"metadata\"
}
```

4.

```
GET http://localhost:9200/_search?q=--1

RESPONSE
{
    \"error\": {
        \"root_cause\": [
            {
                \"type\": \"query_shard_exception\",
                \"reason\": \"Failed to parse query [--1]\",
                \"index_uuid\": \"StMwN9PhSIucajHJNwgFcQ\",
                \"index\": \"metadata\"
            }
        ],
        \"type\": \"search_phase_execution_exception\",
        \"reason\": \"all shards failed\",
        \"phase\": \"query\",
        \"grouped\": true,
        \"failed_shards\": [
            {
                \"shard\": 0,
                \"index\": \"metadata\",
                \"node\": \"65QeKxouS1OFf8-SxprHNw\",
                \"reason\": {
                    \"type\": \"query_shard_exception\",
                    \"reason\": \"Failed to parse query [--1]\",
                    \"index_uuid\": \"StMwN9PhSIucajHJNwgFcQ\",
                    \"index\": \"metadata\",
                    \"caused_by\": {
                        \"type\": \"parse_exception\",
                        \"reason\": \"Cannot parse '--1': Encountered \\\" \\\"-\\\" \\\"- \\\"\\\" at line 1, column 1.\\nWas expecting one of:\\n    <BAREOPER> ...\\n    \\\"(\\\" ...\\n    \\\"*\\\" ...\\n    <QUOTED> ...\\n    <TERM> ...\\n    <PREFIXTERM> ...\\n    <WILDTERM> ...\\n    <REGEXPTERM> ...\\n    \\\"[\\\" ...\\n    \\\"{\\\" ...\\n    <NUMBER> ...\\n    <TERM> ...\\n    \\\"*\\\" ...\\n    \",
                        \"caused_by\": {
                            \"type\": \"parse_exception\",
                            \"reason\": \"Encountered \\\" \\\"-\\\" \\\"- \\\"\\\" at line 1, column 1.\\nWas expecting one of:\\n    <BAREOPER> ...\\n    \\\"(\\\" ...\\n    \\\"*\\\" ...\\n    <QUOTED> ...\\n    <TERM> ...\\n    <PREFIXTERM> ...\\n    <WILDTERM> ...\\n    <REGEXPTERM> ...\\n    \\\"[\\\" ...\\n    \\\"{\\\" ...\\n    <NUMBER> ...\\n    <TERM> ...\\n    \\\"*\\\" ...\\n    \"
                        }
                    }
                }
            }
        ]
    },
    \"status\": 400
}
``` 

", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/30986","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/30986/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/30986/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/30986/events","html_url":"https://github.com/elastic/elasticsearch/issues/30986","id":327977515,"node_id":"MDU6SXNzdWUzMjc5Nzc1MTU=","number":30986,"title":"Query starting with - fails after building mapping","user":{"login":"boyter","id":612151,"node_id":"MDQ6VXNlcjYxMjE1MQ==","avatar_url":"https://avatars1.githubusercontent.com/u/612151?v=4","gravatar_id":"","url":"https://api.github.com/users/boyter","html_url":"https://github.com/boyter","followers_url":"https://api.github.com/users/boyter/followers","following_url":"https://api.github.com/users/boyter/following{/other_user}","gists_url":"https://api.github.com/users/boyter/gists{/gist_id}","starred_url":"https://api.github.com/users/boyter/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/boyter/subscriptions","organizations_url":"https://api.github.com/users/boyter/orgs","repos_url":"https://api.github.com/users/boyter/repos","events_url":"https://api.github.com/users/boyter/events{/privacy}","received_events_url":"https://api.github.com/users/boyter/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-05-31T03:09:51Z","updated_at":"2018-05-31T07:32:17Z","closed_at":"2018-05-31T07:32:17Z","author_association":"NONE","body":"**Elasticsearch version** (`bin/elasticsearch --version`):\r\n\r\nDocker Elastic `docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4`\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\n\r\nLinux SurfaceBook2 4.4.0-17134-Microsoft #48-Microsoft Fri Apr 27 18:06:00 PST 2018 x86_64 x86_64 x86_64 GNU/Linux\r\n\r\nRunning WSL on Windows, but verified on Ubuntu 16:04 system as well.\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\nWhen doing a search for `--1` without adding mapping the search works as expected. When the mapping is applied a search for `--1` will throw an exception. This seems to apply to any search term that starts with `-`\r\n\r\n**Steps to reproduce**:\r\n1. \r\n\r\n```\r\ndocker run -p 9200:9200 -p 9300:9300 -e \"discovery.type=single-node\" -d docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.4\r\n```\r\n\r\n2.\r\n\r\n```\r\nGET http://localhost:9200/_search?q=--1\r\n\r\nRESPONSE\r\n{\r\n    \"took\": 6,\r\n    \"timed_out\": false,\r\n    \"_shards\": {\r\n        \"total\": 0,\r\n        \"successful\": 0,\r\n        \"skipped\": 0,\r\n        \"failed\": 0\r\n    },\r\n    \"hits\": {\r\n        \"total\": 0,\r\n        \"max_score\": 0,\r\n        \"hits\": []\r\n    }\r\n}\r\n```\r\n\r\n3. \r\n\r\n```\r\nPUT http://localhost:9200/metadata/\r\n{\r\n  \"mappings\": {\r\n    \"meta\": { \r\n      \"properties\": { \r\n        \"asset.format\": { \r\n          \"type\": \"keyword\"\r\n        },\r\n        \"firstPublishDate\": {\r\n          \"type\": \"date\",\r\n          \"format\": \"yyyy-MM-dd\",\r\n          \"ignore_malformed\": true\r\n        },\r\n        \"recordDate\": {\r\n          \"type\": \"date\",\r\n          \"format\": \"yyyy-MM-dd\",\r\n          \"ignore_malformed\": true\r\n        },\r\n        \"status.type\": {\r\n          \"type\": \"keyword\"\r\n        },\r\n        \"status.digitised\": {\r\n          \"type\": \"keyword\"\r\n        }\r\n      }\r\n    }\r\n  }\r\n}\r\n\r\nRESPONSE\r\n{\r\n    \"acknowledged\": true,\r\n    \"shards_acknowledged\": true,\r\n    \"index\": \"metadata\"\r\n}\r\n```\r\n\r\n4.\r\n\r\n```\r\nGET http://localhost:9200/_search?q=--1\r\n\r\nRESPONSE\r\n{\r\n    \"error\": {\r\n        \"root_cause\": [\r\n            {\r\n                \"type\": \"query_shard_exception\",\r\n                \"reason\": \"Failed to parse query [--1]\",\r\n                \"index_uuid\": \"StMwN9PhSIucajHJNwgFcQ\",\r\n                \"index\": \"metadata\"\r\n            }\r\n        ],\r\n        \"type\": \"search_phase_execution_exception\",\r\n        \"reason\": \"all shards failed\",\r\n        \"phase\": \"query\",\r\n        \"grouped\": true,\r\n        \"failed_shards\": [\r\n            {\r\n                \"shard\": 0,\r\n                \"index\": \"metadata\",\r\n                \"node\": \"65QeKxouS1OFf8-SxprHNw\",\r\n                \"reason\": {\r\n                    \"type\": \"query_shard_exception\",\r\n                    \"reason\": \"Failed to parse query [--1]\",\r\n                    \"index_uuid\": \"StMwN9PhSIucajHJNwgFcQ\",\r\n                    \"index\": \"metadata\",\r\n                    \"caused_by\": {\r\n                        \"type\": \"parse_exception\",\r\n                        \"reason\": \"Cannot parse '--1': Encountered \\\" \\\"-\\\" \\\"- \\\"\\\" at line 1, column 1.\\nWas expecting one of:\\n    <BAREOPER> ...\\n    \\\"(\\\" ...\\n    \\\"*\\\" ...\\n    <QUOTED> ...\\n    <TERM> ...\\n    <PREFIXTERM> ...\\n    <WILDTERM> ...\\n    <REGEXPTERM> ...\\n    \\\"[\\\" ...\\n    \\\"{\\\" ...\\n    <NUMBER> ...\\n    <TERM> ...\\n    \\\"*\\\" ...\\n    \",\r\n                        \"caused_by\": {\r\n                            \"type\": \"parse_exception\",\r\n                            \"reason\": \"Encountered \\\" \\\"-\\\" \\\"- \\\"\\\" at line 1, column 1.\\nWas expecting one of:\\n    <BAREOPER> ...\\n    \\\"(\\\" ...\\n    \\\"*\\\" ...\\n    <QUOTED> ...\\n    <TERM> ...\\n    <PREFIXTERM> ...\\n    <WILDTERM> ...\\n    <REGEXPTERM> ...\\n    \\\"[\\\" ...\\n    \\\"{\\\" ...\\n    <NUMBER> ...\\n    <TERM> ...\\n    \\\"*\\\" ...\\n    \"\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n        ]\r\n    },\r\n    \"status\": 400\r\n}\r\n``` \r\n\r\n","closed_by":{"login":"jimczi","id":15977469,"node_id":"MDQ6VXNlcjE1OTc3NDY5","avatar_url":"https://avatars0.githubusercontent.com/u/15977469?v=4","gravatar_id":"","url":"https://api.github.com/users/jimczi","html_url":"https://github.com/jimczi","followers_url":"https://api.github.com/users/jimczi/followers","following_url":"https://api.github.com/users/jimczi/following{/other_user}","gists_url":"https://api.github.com/users/jimczi/gists{/gist_id}","starred_url":"https://api.github.com/users/jimczi/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/jimczi/subscriptions","organizations_url":"https://api.github.com/users/jimczi/orgs","repos_url":"https://api.github.com/users/jimczi/repos","events_url":"https://api.github.com/users/jimczi/events{/privacy}","received_events_url":"https://api.github.com/users/jimczi/received_events","type":"User","site_admin":false}}", "commentIds":["393437285"], "labels":[]}