{"id":"29443", "title":"Email issue.", "body":"<!--


** Please read the guidelines below. **

Issues that do not follow these guidelines are likely to be closed.

1.  GitHub is reserved for bug reports and feature requests. The best place to
    ask a general question is at the Elastic [forums](https://discuss.elastic.co).
    GitHub is not the place for general questions.

2.  Is this bug report or feature request for a supported OS? If not, it
    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os

3.  Please fill out EITHER the feature request block or the bug report block
    below, and delete the other block.

-->

<!-- Feature request -->

**Describe the feature**:
In-spite  Elastics watcher getting successful executed not reviving email.

**Email host detail in elasticsserach.yml**
*xpack.notification.email.account:
    socgen_account:
        profile: fidessa
        smtp:
            auth: true
            starttls.enable: true
            host: smtp-*******        
            user: *********
            password: *******
*
**Watcher response**
*{
  \"watch_id\": \"system_process_watch\",
  \"node\": \"TEfIfaz8R16BbkeH9ah1TA\",
  \"state\": \"execution_not_needed\",
  \"status\": {
    \"state\": {
      \"active\": true,
      \"timestamp\": \"2018-04-10T07:28:36.198Z\"
    },
    \"last_checked\": \"2018-04-10T07:32:36.390Z\",
    \"actions\": {
      \"email_administrator\": {
        \"ack\": {
          \"timestamp\": \"2018-04-10T07:28:36.198Z\",
          \"state\": \"awaits_successful_execution\"
        }
      }
    },
    \"execution_state\": \"execution_not_needed\",
    \"version\": -1
  },
  \"trigger_event\": {
    \"type\": \"schedule\",
    \"triggered_time\": \"2018-04-10T07:32:36.390Z\",
    \"schedule\": {
      \"scheduled_time\": \"2018-04-10T07:32:36.343Z\"
    }
  },
  \"input\": {
    \"search\": {
      \"request\": {
        \"search_type\": \"query_then_fetch\",
        \"indices\": [
          \"metricbeat-*\"
        ],
        \"types\": [],
        \"body\": {
          \"size\": 0,
          \"query\": {
            \"match\": {
              \"metricset.name\": \"process\"
            }
          }
        }
      }
    }
  },
  \"condition\": {
    \"compare\": {
      \"system.process.cpu.total.norm.pct\": {
        \"gt\": 0
      }
    }
  },
  \"result\": {
    \"execution_time\": \"2018-04-10T07:32:36.390Z\",
    \"execution_duration\": 2,
    \"input\": {
      \"type\": \"search\",
      \"status\": \"success\",
      \"payload\": {
        \"_shards\": {
          \"total\": 2,
          \"failed\": 0,
          \"successful\": 2,
          \"skipped\": 0
        },
        \"hits\": {
          \"hits\": [],
          \"total\": 11746,
          \"max_score\": 0
        },
        \"took\": 0,
        \"timed_out\": false
      },
      \"search\": {
        \"request\": {
          \"search_type\": \"query_then_fetch\",
          \"indices\": [
            \"metricbeat-*\"
          ],
          \"types\": [],
          \"body\": {
            \"size\": 0,
            \"query\": {
              \"match\": {
                \"metricset.name\": \"process\"
              }
            }
          }
        }
      }
    },
    \"condition\": {
      \"type\": \"compare\",
      \"status\": \"success\",
      \"met\": false,
      \"compare\": {
        \"resolved_values\": {
          \"system.process.cpu.total.norm.pct\": null
        }
      }
    },
    \"actions\": []
  },
  \"messages\": []
}*   

<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`):
6.2.2
**Plugins installed**: [X-pack]

**JVM version** (`java -version`):
1.8.0_101-b13
**OS version** (`uname -a` if on a Unix-like system):
Linux ******** 3.10.0-514.el7.x86_64 #1 SMP Wed Oct 19 11:24:13 EDT 2086_64 x86_64 x86_64 GNU/Linux

**Description of the problem including expected versus actual behavior**:

**Steps to reproduce**:

Please include a *minimal* but *complete* recreation of the problem, including
(e.g.) index creation, mappings, settings, query etc.  The easier you make for
us to reproduce it, the more likely that somebody will take the time to look at it.

 1.
 2.
 3.

**Provide logs (if relevant)**:

", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/29443","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29443/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29443/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/29443/events","html_url":"https://github.com/elastic/elasticsearch/issues/29443","id":312806809,"node_id":"MDU6SXNzdWUzMTI4MDY4MDk=","number":29443,"title":"Email issue.","user":{"login":"naliniranjan82","id":5726338,"node_id":"MDQ6VXNlcjU3MjYzMzg=","avatar_url":"https://avatars3.githubusercontent.com/u/5726338?v=4","gravatar_id":"","url":"https://api.github.com/users/naliniranjan82","html_url":"https://github.com/naliniranjan82","followers_url":"https://api.github.com/users/naliniranjan82/followers","following_url":"https://api.github.com/users/naliniranjan82/following{/other_user}","gists_url":"https://api.github.com/users/naliniranjan82/gists{/gist_id}","starred_url":"https://api.github.com/users/naliniranjan82/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/naliniranjan82/subscriptions","organizations_url":"https://api.github.com/users/naliniranjan82/orgs","repos_url":"https://api.github.com/users/naliniranjan82/repos","events_url":"https://api.github.com/users/naliniranjan82/events{/privacy}","received_events_url":"https://api.github.com/users/naliniranjan82/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":1,"created_at":"2018-04-10T07:41:50Z","updated_at":"2018-04-10T07:53:49Z","closed_at":"2018-04-10T07:53:49Z","author_association":"NONE","body":"<!--\r\n\r\n\r\n** Please read the guidelines below. **\r\n\r\nIssues that do not follow these guidelines are likely to be closed.\r\n\r\n1.  GitHub is reserved for bug reports and feature requests. The best place to\r\n    ask a general question is at the Elastic [forums](https://discuss.elastic.co).\r\n    GitHub is not the place for general questions.\r\n\r\n2.  Is this bug report or feature request for a supported OS? If not, it\r\n    is likely to be closed.  See https://www.elastic.co/support/matrix#show_os\r\n\r\n3.  Please fill out EITHER the feature request block or the bug report block\r\n    below, and delete the other block.\r\n\r\n-->\r\n\r\n<!-- Feature request -->\r\n\r\n**Describe the feature**:\r\nIn-spite  Elastics watcher getting successful executed not reviving email.\r\n\r\n**Email host detail in elasticsserach.yml**\r\n*xpack.notification.email.account:\r\n    socgen_account:\r\n        profile: fidessa\r\n        smtp:\r\n            auth: true\r\n            starttls.enable: true\r\n            host: smtp-*******        \r\n            user: *********\r\n            password: *******\r\n*\r\n**Watcher response**\r\n*{\r\n  \"watch_id\": \"system_process_watch\",\r\n  \"node\": \"TEfIfaz8R16BbkeH9ah1TA\",\r\n  \"state\": \"execution_not_needed\",\r\n  \"status\": {\r\n    \"state\": {\r\n      \"active\": true,\r\n      \"timestamp\": \"2018-04-10T07:28:36.198Z\"\r\n    },\r\n    \"last_checked\": \"2018-04-10T07:32:36.390Z\",\r\n    \"actions\": {\r\n      \"email_administrator\": {\r\n        \"ack\": {\r\n          \"timestamp\": \"2018-04-10T07:28:36.198Z\",\r\n          \"state\": \"awaits_successful_execution\"\r\n        }\r\n      }\r\n    },\r\n    \"execution_state\": \"execution_not_needed\",\r\n    \"version\": -1\r\n  },\r\n  \"trigger_event\": {\r\n    \"type\": \"schedule\",\r\n    \"triggered_time\": \"2018-04-10T07:32:36.390Z\",\r\n    \"schedule\": {\r\n      \"scheduled_time\": \"2018-04-10T07:32:36.343Z\"\r\n    }\r\n  },\r\n  \"input\": {\r\n    \"search\": {\r\n      \"request\": {\r\n        \"search_type\": \"query_then_fetch\",\r\n        \"indices\": [\r\n          \"metricbeat-*\"\r\n        ],\r\n        \"types\": [],\r\n        \"body\": {\r\n          \"size\": 0,\r\n          \"query\": {\r\n            \"match\": {\r\n              \"metricset.name\": \"process\"\r\n            }\r\n          }\r\n        }\r\n      }\r\n    }\r\n  },\r\n  \"condition\": {\r\n    \"compare\": {\r\n      \"system.process.cpu.total.norm.pct\": {\r\n        \"gt\": 0\r\n      }\r\n    }\r\n  },\r\n  \"result\": {\r\n    \"execution_time\": \"2018-04-10T07:32:36.390Z\",\r\n    \"execution_duration\": 2,\r\n    \"input\": {\r\n      \"type\": \"search\",\r\n      \"status\": \"success\",\r\n      \"payload\": {\r\n        \"_shards\": {\r\n          \"total\": 2,\r\n          \"failed\": 0,\r\n          \"successful\": 2,\r\n          \"skipped\": 0\r\n        },\r\n        \"hits\": {\r\n          \"hits\": [],\r\n          \"total\": 11746,\r\n          \"max_score\": 0\r\n        },\r\n        \"took\": 0,\r\n        \"timed_out\": false\r\n      },\r\n      \"search\": {\r\n        \"request\": {\r\n          \"search_type\": \"query_then_fetch\",\r\n          \"indices\": [\r\n            \"metricbeat-*\"\r\n          ],\r\n          \"types\": [],\r\n          \"body\": {\r\n            \"size\": 0,\r\n            \"query\": {\r\n              \"match\": {\r\n                \"metricset.name\": \"process\"\r\n              }\r\n            }\r\n          }\r\n        }\r\n      }\r\n    },\r\n    \"condition\": {\r\n      \"type\": \"compare\",\r\n      \"status\": \"success\",\r\n      \"met\": false,\r\n      \"compare\": {\r\n        \"resolved_values\": {\r\n          \"system.process.cpu.total.norm.pct\": null\r\n        }\r\n      }\r\n    },\r\n    \"actions\": []\r\n  },\r\n  \"messages\": []\r\n}*   \r\n\r\n<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`):\r\n6.2.2\r\n**Plugins installed**: [X-pack]\r\n\r\n**JVM version** (`java -version`):\r\n1.8.0_101-b13\r\n**OS version** (`uname -a` if on a Unix-like system):\r\nLinux ******** 3.10.0-514.el7.x86_64 #1 SMP Wed Oct 19 11:24:13 EDT 2086_64 x86_64 x86_64 GNU/Linux\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\n\r\n**Steps to reproduce**:\r\n\r\nPlease include a *minimal* but *complete* recreation of the problem, including\r\n(e.g.) index creation, mappings, settings, query etc.  The easier you make for\r\nus to reproduce it, the more likely that somebody will take the time to look at it.\r\n\r\n 1.\r\n 2.\r\n 3.\r\n\r\n**Provide logs (if relevant)**:\r\n\r\n","closed_by":{"login":"tvernum","id":2244393,"node_id":"MDQ6VXNlcjIyNDQzOTM=","avatar_url":"https://avatars0.githubusercontent.com/u/2244393?v=4","gravatar_id":"","url":"https://api.github.com/users/tvernum","html_url":"https://github.com/tvernum","followers_url":"https://api.github.com/users/tvernum/followers","following_url":"https://api.github.com/users/tvernum/following{/other_user}","gists_url":"https://api.github.com/users/tvernum/gists{/gist_id}","starred_url":"https://api.github.com/users/tvernum/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/tvernum/subscriptions","organizations_url":"https://api.github.com/users/tvernum/orgs","repos_url":"https://api.github.com/users/tvernum/repos","events_url":"https://api.github.com/users/tvernum/events{/privacy}","received_events_url":"https://api.github.com/users/tvernum/received_events","type":"User","site_admin":false}}", "commentIds":["380008616"], "labels":[]}