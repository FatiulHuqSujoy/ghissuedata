{"id":"37268", "title":"[CI] UnicastZenPingTests.testInvalidHosts", "body":"## Example build failure
https://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+release-tests/309/console

## Reproduction line 
does not reproduce locally
```
REPRODUCE WITH: ./gradlew :server:unitTest \\
  -Dtests.seed=86FB7F4B57514FBE \\
  -Dtests.class=org.elasticsearch.discovery.zen.UnicastZenPingTests \\
  -Dtests.method=\"testInvalidHosts\" \\
  -Dtests.security.manager=true \\
  -Dbuild.snapshot=false \\
  -Dtests.jvm.argline=\"-Dbuild.snapshot=false\" \\
  -Dtests.locale=ar-YE \\
  -Dtests.timezone=Asia/Brunei \\
  -Dcompiler.java=11 \\
  -Druntime.java=8
```

## Example relevant log:
```
FAILURE 21.5s J2 | UnicastZenPingTests.testInvalidHosts <<< FAILURES!
   > Throwable #1: java.lang.AssertionError: 
   > Expected: a collection with size <1>
   >      but: collection size was <0>
   > 	at __randomizedtesting.SeedInfo.seed([86FB7F4B57514FBE:97CE97BE380F6D4A]:0)
   > 	at org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)
   > 	at org.elasticsearch.discovery.zen.UnicastZenPingTests.testInvalidHosts(UnicastZenPingTests.java:726)
   > 	at java.lang.Thread.run(Thread.java:748)
```

## Frequency
4 times Today. 

Related: #23738", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/37268","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/37268/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/37268/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/37268/events","html_url":"https://github.com/elastic/elasticsearch/issues/37268","id":397434248,"node_id":"MDU6SXNzdWUzOTc0MzQyNDg=","number":37268,"title":"[CI] UnicastZenPingTests.testInvalidHosts","user":{"login":"atorok","id":2565652,"node_id":"MDQ6VXNlcjI1NjU2NTI=","avatar_url":"https://avatars1.githubusercontent.com/u/2565652?v=4","gravatar_id":"","url":"https://api.github.com/users/atorok","html_url":"https://github.com/atorok","followers_url":"https://api.github.com/users/atorok/followers","following_url":"https://api.github.com/users/atorok/following{/other_user}","gists_url":"https://api.github.com/users/atorok/gists{/gist_id}","starred_url":"https://api.github.com/users/atorok/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/atorok/subscriptions","organizations_url":"https://api.github.com/users/atorok/orgs","repos_url":"https://api.github.com/users/atorok/repos","events_url":"https://api.github.com/users/atorok/events{/privacy}","received_events_url":"https://api.github.com/users/atorok/received_events","type":"User","site_admin":false},"labels":[{"id":881394071,"node_id":"MDU6TGFiZWw4ODEzOTQwNzE=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/:Distributed/Cluster%20Coordination","name":":Distributed/Cluster Coordination","color":"0e8a16","default":false},{"id":148612629,"node_id":"MDU6TGFiZWwxNDg2MTI2Mjk=","url":"https://api.github.com/repos/elastic/elasticsearch/labels/%3Etest-failure","name":">test-failure","color":"207de5","default":false},{"id":1163688906,"node_id":"MDU6TGFiZWwxMTYzNjg4OTA2","url":"https://api.github.com/repos/elastic/elasticsearch/labels/v6.7.0","name":"v6.7.0","color":"dddddd","default":false},{"id":1223177445,"node_id":"MDU6TGFiZWwxMjIzMTc3NDQ1","url":"https://api.github.com/repos/elastic/elasticsearch/labels/v7.0.0-beta1","name":"v7.0.0-beta1","color":"dddddd","default":false}],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2019-01-09T15:53:28Z","updated_at":"2019-02-07T10:02:18Z","closed_at":"2019-01-11T10:35:48Z","author_association":"CONTRIBUTOR","body":"## Example build failure\r\nhttps://elasticsearch-ci.elastic.co/job/elastic+elasticsearch+master+release-tests/309/console\r\n\r\n## Reproduction line \r\ndoes not reproduce locally\r\n```\r\nREPRODUCE WITH: ./gradlew :server:unitTest \\\r\n  -Dtests.seed=86FB7F4B57514FBE \\\r\n  -Dtests.class=org.elasticsearch.discovery.zen.UnicastZenPingTests \\\r\n  -Dtests.method=\"testInvalidHosts\" \\\r\n  -Dtests.security.manager=true \\\r\n  -Dbuild.snapshot=false \\\r\n  -Dtests.jvm.argline=\"-Dbuild.snapshot=false\" \\\r\n  -Dtests.locale=ar-YE \\\r\n  -Dtests.timezone=Asia/Brunei \\\r\n  -Dcompiler.java=11 \\\r\n  -Druntime.java=8\r\n```\r\n\r\n## Example relevant log:\r\n```\r\nFAILURE 21.5s J2 | UnicastZenPingTests.testInvalidHosts <<< FAILURES!\r\n   > Throwable #1: java.lang.AssertionError: \r\n   > Expected: a collection with size <1>\r\n   >      but: collection size was <0>\r\n   > \tat __randomizedtesting.SeedInfo.seed([86FB7F4B57514FBE:97CE97BE380F6D4A]:0)\r\n   > \tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n   > \tat org.elasticsearch.discovery.zen.UnicastZenPingTests.testInvalidHosts(UnicastZenPingTests.java:726)\r\n   > \tat java.lang.Thread.run(Thread.java:748)\r\n```\r\n\r\n## Frequency\r\n4 times Today. \r\n\r\nRelated: #23738","closed_by":{"login":"ywelsch","id":3718355,"node_id":"MDQ6VXNlcjM3MTgzNTU=","avatar_url":"https://avatars3.githubusercontent.com/u/3718355?v=4","gravatar_id":"","url":"https://api.github.com/users/ywelsch","html_url":"https://github.com/ywelsch","followers_url":"https://api.github.com/users/ywelsch/followers","following_url":"https://api.github.com/users/ywelsch/following{/other_user}","gists_url":"https://api.github.com/users/ywelsch/gists{/gist_id}","starred_url":"https://api.github.com/users/ywelsch/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ywelsch/subscriptions","organizations_url":"https://api.github.com/users/ywelsch/orgs","repos_url":"https://api.github.com/users/ywelsch/repos","events_url":"https://api.github.com/users/ywelsch/events{/privacy}","received_events_url":"https://api.github.com/users/ywelsch/received_events","type":"User","site_admin":false}}", "commentIds":["452746402","453143625","453473740"], "labels":[":Distributed/Cluster Coordination",">test-failure","v6.7.0","v7.0.0-beta1"]}