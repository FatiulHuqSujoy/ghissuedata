{"id":"28439", "title":"WordDelimiterTokenFilter does not produce expected tokens with split_on_case_change and preserve_original set to true", "body":"<!-- Bug report -->

**Elasticsearch version** (`bin/elasticsearch --version`):
Version: 5.4.1, Build: 2cfe0df/2017-05-29T16:05:51.443Z

**Plugins installed**: []

**JVM version** (`java -version`):
 JVM: 1.8.0_151

**OS version** (`uname -a` if on a Unix-like system):
Windows 10

**Description of the problem including expected versus actual behavior**:
When using word delimiter token filter some expected tokens are not generated.

When I try to analyze the text \"ElasticSearch.TestProject\"

I expect the tokens elastic, search, test, project, elasticsearch, testproject, elasticsearch.testproject to be generated since I have split_on_case_change, split_on_numerics on and using a whitespace tokenizer. 

But Actually I only see following tokens -
elasticsearch.testproject, elastic, search, test, project

**Steps to reproduce**:
 **1. Create index** 
_PUT testindex
 {
    \"settings\" : {
        \"index\" : {
            \"number_of_shards\" : 2, 
            \"number_of_replicas\" : 2 
        },
    \"analysis\": {
      \"filter\": {
        \"wordDelimiter\": {
          \"type\": \"word_delimiter\",
          \"generate_word_parts\": \"true\",
          \"generate_number_parts\": \"true\",
          \"catenate_words\": \"false\",
          \"catenate_numbers\": \"false\",
          \"catenate_all\": \"false\",
          \"split_on_case_change\": \"true\",
          \"preserve_original\": \"true\",
          \"split_on_numerics\": \"true\",
          \"stem_english_possessive\": \"true\"
        }
      },
      \"analyzer\": {
		\"content_analyzer\": {
          \"type\": \"custom\",
          \"tokenizer\": \"whitespace\",
          \"filter\": [
            \"asciifolding\",
            \"wordDelimiter\",
            \"lowercase\"
          ]
        }
      }
    }
	}
 }_

 **2. Analyze Text-**
_POST testindex/_analyze 
{
  \"analyzer\": \"content_analyzer\",
  \"text\": \"ElasticSearch.TestProject\"
}_

**Following tokens are generated-**

{
\"token\": \"elasticsearch-testproject\",
\"start_offset\": 0,
\"end_offset\": 25,
\"type\": \"word\",
\"position\": 0
}
,
{
\"token\": \"elastic\",
\"start_offset\": 0,
\"end_offset\": 7,
\"type\": \"word\",
\"position\": 0
}
,
{
\"token\": \"search\",
\"start_offset\": 7,
\"end_offset\": 13,
\"type\": \"word\",
\"position\": 1
}
,
{
\"token\": \"test\",
\"start_offset\": 14,
\"end_offset\": 18,
\"type\": \"word\",
\"position\": 2
}
,
{
\"token\": \"project\",
\"start_offset\": 18,
\"end_offset\": 25,
\"type\": \"word\",
\"position\": 3
}

**Expected Result:**
Besides the above tokens even elasticsearch and testproject should be generated. such that the phrase query \"elasticsearch testproject\" should also match.", "json":"{"url":"https://api.github.com/repos/elastic/elasticsearch/issues/28439","repository_url":"https://api.github.com/repos/elastic/elasticsearch","labels_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28439/labels{/name}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28439/comments","events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/28439/events","html_url":"https://github.com/elastic/elasticsearch/issues/28439","id":292735539,"node_id":"MDU6SXNzdWUyOTI3MzU1Mzk=","number":28439,"title":"WordDelimiterTokenFilter does not produce expected tokens with split_on_case_change and preserve_original set to true","user":{"login":"atbagga","id":11888714,"node_id":"MDQ6VXNlcjExODg4NzE0","avatar_url":"https://avatars1.githubusercontent.com/u/11888714?v=4","gravatar_id":"","url":"https://api.github.com/users/atbagga","html_url":"https://github.com/atbagga","followers_url":"https://api.github.com/users/atbagga/followers","following_url":"https://api.github.com/users/atbagga/following{/other_user}","gists_url":"https://api.github.com/users/atbagga/gists{/gist_id}","starred_url":"https://api.github.com/users/atbagga/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/atbagga/subscriptions","organizations_url":"https://api.github.com/users/atbagga/orgs","repos_url":"https://api.github.com/users/atbagga/repos","events_url":"https://api.github.com/users/atbagga/events{/privacy}","received_events_url":"https://api.github.com/users/atbagga/received_events","type":"User","site_admin":false},"labels":[],"state":"closed","locked":false,"assignee":null,"assignees":[],"milestone":null,"comments":3,"created_at":"2018-01-30T10:46:26Z","updated_at":"2018-01-31T05:27:47Z","closed_at":"2018-01-30T12:45:44Z","author_association":"NONE","body":"<!-- Bug report -->\r\n\r\n**Elasticsearch version** (`bin/elasticsearch --version`):\r\nVersion: 5.4.1, Build: 2cfe0df/2017-05-29T16:05:51.443Z\r\n\r\n**Plugins installed**: []\r\n\r\n**JVM version** (`java -version`):\r\n JVM: 1.8.0_151\r\n\r\n**OS version** (`uname -a` if on a Unix-like system):\r\nWindows 10\r\n\r\n**Description of the problem including expected versus actual behavior**:\r\nWhen using word delimiter token filter some expected tokens are not generated.\r\n\r\nWhen I try to analyze the text \"ElasticSearch.TestProject\"\r\n\r\nI expect the tokens elastic, search, test, project, elasticsearch, testproject, elasticsearch.testproject to be generated since I have split_on_case_change, split_on_numerics on and using a whitespace tokenizer. \r\n\r\nBut Actually I only see following tokens -\r\nelasticsearch.testproject, elastic, search, test, project\r\n\r\n**Steps to reproduce**:\r\n **1. Create index** \r\n_PUT testindex\r\n {\r\n    \"settings\" : {\r\n        \"index\" : {\r\n            \"number_of_shards\" : 2, \r\n            \"number_of_replicas\" : 2 \r\n        },\r\n    \"analysis\": {\r\n      \"filter\": {\r\n        \"wordDelimiter\": {\r\n          \"type\": \"word_delimiter\",\r\n          \"generate_word_parts\": \"true\",\r\n          \"generate_number_parts\": \"true\",\r\n          \"catenate_words\": \"false\",\r\n          \"catenate_numbers\": \"false\",\r\n          \"catenate_all\": \"false\",\r\n          \"split_on_case_change\": \"true\",\r\n          \"preserve_original\": \"true\",\r\n          \"split_on_numerics\": \"true\",\r\n          \"stem_english_possessive\": \"true\"\r\n        }\r\n      },\r\n      \"analyzer\": {\r\n\t\t\"content_analyzer\": {\r\n          \"type\": \"custom\",\r\n          \"tokenizer\": \"whitespace\",\r\n          \"filter\": [\r\n            \"asciifolding\",\r\n            \"wordDelimiter\",\r\n            \"lowercase\"\r\n          ]\r\n        }\r\n      }\r\n    }\r\n\t}\r\n }_\r\n\r\n **2. Analyze Text-**\r\n_POST testindex/_analyze \r\n{\r\n  \"analyzer\": \"content_analyzer\",\r\n  \"text\": \"ElasticSearch.TestProject\"\r\n}_\r\n\r\n**Following tokens are generated-**\r\n\r\n{\r\n\"token\": \"elasticsearch-testproject\",\r\n\"start_offset\": 0,\r\n\"end_offset\": 25,\r\n\"type\": \"word\",\r\n\"position\": 0\r\n}\r\n,\r\n{\r\n\"token\": \"elastic\",\r\n\"start_offset\": 0,\r\n\"end_offset\": 7,\r\n\"type\": \"word\",\r\n\"position\": 0\r\n}\r\n,\r\n{\r\n\"token\": \"search\",\r\n\"start_offset\": 7,\r\n\"end_offset\": 13,\r\n\"type\": \"word\",\r\n\"position\": 1\r\n}\r\n,\r\n{\r\n\"token\": \"test\",\r\n\"start_offset\": 14,\r\n\"end_offset\": 18,\r\n\"type\": \"word\",\r\n\"position\": 2\r\n}\r\n,\r\n{\r\n\"token\": \"project\",\r\n\"start_offset\": 18,\r\n\"end_offset\": 25,\r\n\"type\": \"word\",\r\n\"position\": 3\r\n}\r\n\r\n**Expected Result:**\r\nBesides the above tokens even elasticsearch and testproject should be generated. such that the phrase query \"elasticsearch testproject\" should also match.","closed_by":{"login":"romseygeek","id":1347065,"node_id":"MDQ6VXNlcjEzNDcwNjU=","avatar_url":"https://avatars0.githubusercontent.com/u/1347065?v=4","gravatar_id":"","url":"https://api.github.com/users/romseygeek","html_url":"https://github.com/romseygeek","followers_url":"https://api.github.com/users/romseygeek/followers","following_url":"https://api.github.com/users/romseygeek/following{/other_user}","gists_url":"https://api.github.com/users/romseygeek/gists{/gist_id}","starred_url":"https://api.github.com/users/romseygeek/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/romseygeek/subscriptions","organizations_url":"https://api.github.com/users/romseygeek/orgs","repos_url":"https://api.github.com/users/romseygeek/repos","events_url":"https://api.github.com/users/romseygeek/events{/privacy}","received_events_url":"https://api.github.com/users/romseygeek/received_events","type":"User","site_admin":false}}", "commentIds":["361559177","361582741","361828003"], "labels":[]}